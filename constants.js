// export const HOST = "http://stg.zastlogisolutions.com/api";
// export const M_HOST = "http://stg.zastlogisolutions.com/m-api";
export const M_HOST = "https://admin.zastlogisolutions.com/m-api";

// import * as BuildConfig from "./src/config";
// import {API_HOST} from "./src/config";

// export const M_HOST = API_HOST;

export const REQUEST_TYPE = {
  POST: "post",
  GET: "get",
  PUT: "put",
  DELETE: "delete"
};

export const STACK = {
  APP: "APP",
  AUTH: "AUTH",
  AUTH_LOADING: "AUTH_LOADING"
};

export const NAVIGATE = {
  SIGN_IN: "SIGN_IN",
  SIGN_OUT: "SIGN_OUT",
  OTP_SCREEN: "OTP_SCREEN",
  HOME: "HOME",
  DASHBOARD: "DASHBOARD",
  ALL_TRIPS: "ALL_TRIPS",
  ALL_BOOKINGS: "ALL_BOOKINGS",
  CREATE_BOOKING: "CREATE_BOOKING",
  MY_BOOKINGS: "MY_BOOKINGS",
  SEARCH: "SEARCH",
  AVAILABLE_VEHICLES: "AVAILABLE_VEHICLES",
  BOOKING_ONE_WAY: "BOOKING_ONE_WAY",
  BOOKING_TWO_WAY: "BOOKING_TWO_WAY",
  DRY_RUN: "DRY_RUN",
  TRIPS_NEW: "TRIPS_NEW",
  TRIPS_IN_TRANSIT: "TRIPS_IN_TRANSIT",
  TRIPS_AT_DESTINATION: "TRIPS_AT_DESTINATION",
  TRIP_DETAILS: "TRIP_DETAILS",
  TRIP_COMMENTS: "TRIP_COMMENTS",
  EDIT_TRIP: "EDIT_TRIP",
  EDIT_TRIP_ROUTE: "EDIT_TRIP_ROUTE",
  EDIT_TRIP_CUSTOMER: "EDIT_TRIP_CUSTOMER",
  HALT_TRIP: "HALT_TRIP",
  TRIP_HISTORY: "TRIP_HISTORY",
  TRIP_RUNNING_STATUS: "TRIP_RUNNING_STATUS",
  BOOKINGS_NEW_REQUEST: "BOOKINGS_NEW_REQUEST",
  BOOKINGS_ACCEPTED: "BOOKINGS_ACCEPTED",
  BOOKINGS_ASSIGNED: "BOOKINGS_ASSIGNED",
  BOOKINGS_DOCKED: "BOOKINGS_DOCKED",
  SEARCH_RESULT: "SEARCH_RESULT",
  EDIT_BOOKING_TWO_WAY: "EDIT_BOOKING_TWO_WAY",
  EDIT_BOOKING_ONE_WAY: "EDIT_BOOKING_ONE_WAY",
  EDIT_DRY_RUN: "EDIT_DRY_RUN",
  TRIP_COMMENTS_SEARCH: "TRIP_COMMENTS_SEARCH",
  CROSS_DOCK_TRIP: "CROSS_DOCK_TRIP",
  LOCATION_SEARCH: "LOCATION_SEARCH",
  FORCED_UPDATE: "FORCED_UPDATE",
  VENDOR_APPROVAL: "VENDOR_APPROVAL",
  VENDOR_EDIT: "VENDOR_EDIT",
  VENDOR_APPROVAL_PENDING: "VENDOR_APPROVAL_PENDING",
  REQUEST_TRIP_ADVANCE: "REQUEST_TRIP_ADVANCE",
  ADD_TRIP_CHARGES: "ADD_TRIP_CHARGES",
  ADVANCE_PAYMENTS: "ADVANCE_PAYMENTS",
  VENDOR_DETAILS: "VENDOR_DETAILS",
  VEHICLE_APPROVAL_PENDING: "VEHICLE_APPROVAL_PENDING",
  VEHICLE_EDIT: "VEHICLE_EDIT",
  TRACKING_SEARCH: "TRACKING_SEARCH",
  TRACKING_SEARCH_RESULT: "TRACKING_SEARCH_RESULT"
};

const NORTH = "North";
const SOUTH = "South";
const EAST = "East";
const WEST = "West";
const CENTRAL = "Central";
const ALL = "All";

export const ZONES = [ALL, NORTH, SOUTH, EAST, WEST, CENTRAL];

export const BOOKING_TYPE = {
  AD_HOC_ONE_WAY: "AD_HOC_ONE_WAY",
  AD_HOC_TWO_WAY: "AD_HOC_TWO_WAY",
  DRY_RUN: "DRY_RUN"
};

export const TRIP_STATUS_TABLE = {
  CREATED: "CR",
  IN_TRANSIT: "IN",
  TOUCHING: "AI",
  ARRIVED: "AR",
  CLOSED: "CL",
  HALTED: "HL",
  TERMINATED: "TM"
};

export const TRIP_STATUS_TABLE_NAME = {
  [TRIP_STATUS_TABLE.CREATED]: "Created",
  [TRIP_STATUS_TABLE.IN_TRANSIT]: "In Transit",
  [TRIP_STATUS_TABLE.TOUCHING]: "Touching",
  [TRIP_STATUS_TABLE.ARRIVED]: "Arrived",
  [TRIP_STATUS_TABLE.CLOSED]: "Closed",
  [TRIP_STATUS_TABLE.HALTED]: "Halted",
  [TRIP_STATUS_TABLE.TERMINATED]: "Terminated"
};

export const VEHICLE_CATEGORY = {
  ZAS: "ZAS",
  CON: "CON"
};

export const SEARCH_REQUEST_DATA = {
  BOOKING_ID: "booking_code"
};

export const TRIP_STATUS = {
  NEW: "created",
  IN_TRANSIT: "all-in-transit",
  AT_DESTINATION: "arrived-destination"
};

export const BOOKING_API_STATUS = {
  NEW_REQUEST: "requested",
  ACCEPTED: "accepted",
  ASSIGNED: "assigned",
  DOCKED: "docked"
};

export const BOOKING_STATUS = {
  RQ: "REQUESTED",
  AC: "ACCEPTED",
  AS: "ASSIGNED",
  RJ: "REJECTED",
  DK: "DOCKED",
  CL: "CLOSED"
};

export const TRIP_ENTITIES = {
  COMMENT: "comments",
  HISTORY: "history",
  RUNNING_STATUS: "running_status"
};

export const TERMINATE_REASONS = {
  VEHICLE_BREAKDOWN: "Vehicle Breakdown",
  CUSTOMER_TERMINATED: "Customer terminated the trip",
  VEHICLE_UNAVAILABLE: "Vehicle Unavailable"
};

export const TERMINATE_REASONS_ARRAY = [
  TERMINATE_REASONS.VEHICLE_BREAKDOWN,
  TERMINATE_REASONS.CUSTOMER_TERMINATED,
  TERMINATE_REASONS.VEHICLE_UNAVAILABLE
];

export const USER_RIGHTS = {
  CREATE_BOOKING: "create-booking",
  ACCEPT_BOOKING: "accept-booking",
  ASSIGN_BOOKING: "assign-booking",
  EDIT_BOOKING: "edit-booking",
  EDIT_TAT: "edit-tat",
  DOCK_BOOKING: "dock-booking",
  REJECT_BOOKING: "reject-booking",
  SCHEDULED_BOOKING_ACTIVATION: "scheduled-booking-activation",
  DOWNLOAD_BOOKING: "download-booking",
  EDIT_FROM_SEARCH: "edit-from-search",
  EDIT_REFERENCE: "edit-reference",
  VIEW_USER: "view-user",
  ADD_USER: "add-user",
  EDIT_USER: "edit-user",
  CHANGE_PASSWORD: "change-password",
  VIEW_USER_LOGS: "view-user-logs",
  CHANGE_OWN_PASSWORD: "change-own-password",
  SET_CUSTOMERS: "set-customers",
  ADD_FACILITY_ADDRESS: "add-facility-address",
  MODIFY_CITY: "modify-city",
  MODIFY_CONSIGNORS: "modify-consignors",
  UPDATE_ROUTE_TAT: "update-route-tat",
  MODIFY_DISTANCE_MAP: "modify-distance-map",
  ADD_VENDOR: "add-vendor",
  EDIT_VENDOR: "edit-vendor",
  ADD_EDIT_VENDOR_DETAILS: "add-edit-vendor-details",
  DOWNLOAD_VENDOR: "download-vendor",
  ADD_CUSTOMER: "add-customer",
  EDIT_CUSTOMER: "edit-customer",
  ADD_EDIT_CUSTOMER_DETAILS: "add-edit-customer-details",
  DOWNLOAD_CUSTOMER: "download-customer",
  ADD_VEHICLE: "add-vehicle",
  EDIT_VEHICLE: "edit-vehicle",
  ADD_EDIT_VEHICLE_DETAILS: "add-edit-vehicle-details",
  DOWNLOAD_VEHICLE: "download-vehicle",
  EDIT_VEHICLE_PHONE: "edit-vehicle-phone",
  ADD_DRIVER: "add-driver",
  EDIT_DRIVER: "edit-driver",
  ADD_EDIT_DRIVER_DETAILS: "add-edit-driver-details",
  DOWNLOAD_DRIVER: "download-driver",
  UPLOAD_DOCUMENT: "upload-document",
  MODIFY_DOCUMENTS: "modify-documents",
  BEGIN_TRIP: "begin-trip",
  TERMINATE_TRIP: "terminate-trip",
  HALT_TRIP: "halt-trip",
  TOUCHING_IN: "touching-in",
  TOUCHING_OUT: "touching-out",
  REACHED_DESTINATION: "reached-destination",
  CLOSE_TRIP: "close-trip",
  ADD_RUNNING_STATUS: "add-running-status",
  ADD_TRIP_COMMENT: "add-trip-comment",
  EDIT_TRIP_ROUTE: "edit-trip-route",
  EDIT_TRIP_VEHICLE: "edit-trip-vehicle",
  ADD_MANUAL_LOCATION: "add-manual-location",
  DOWNLOAD_TRIP: "download-trip",
  UPDATE_LR: "update-lr",
  DOWNLOAD_TRACKING: "download-tracking",
  VIEW_GPS_FEED: "view-gps-feed",
  VIEW_VODAFONE_NUMBERS: "view-vodafone-numbers",
  UPDATE_VODAFONE_NUMBERS: "update-vodafone-numbers",
  POD_TRACK: "pod-track",
  ASSIGN_POC_POD: "assign-poc-pod",
  UPDATE_POD_STATUS: "update-pod-status",
  UPLOAD_POD: "upload-pod",
  BULK_UPLOAD_POD: "bulk-upload-pod",
  UPDATE_VENDOR_BILL: "update-vendor-bill",
  CREATE_ADVANCE: "create-advance",
  VIEW_ADVANCE: "view-advance",
  SEARCH_ADVANCE: "search-advance",
  EDIT_TRIP_HISTORY: "edit-trip-history",
  DELETE_TRIP_HISTORY: "delete-trip-history",
  VIEW_ADVANCE_REQUESTS: "view-advance-requests",
  PROCESS_ADVANCE: "process-advance",
  TRIP_CHARGES: "trip-charges",
  REQUEST_TRIP_ADVANCE: "request-trip-advance",
  CREATE_ADHOC_ADVANCE: "create-adhoc-advance",
  VIEW_CUSTOMER_REPORT: "view-customer-report",
  APPROVE_VENDOR_OPS: "approve-vendor-ops",
  EDIT_VEHICLE_CATEGORY: "edit-vehicle-category"
};

export const TRIP_FLAG = {
  IN: "IN",
  OUT: "OUT",
  ARRIVED: "ARRIVED"
};

export const RUNNING_STATUS = {
  ON_TIME: "ontime",
  DELAYED: "delayed"
};

export const AUTO_FILL_TAT = {
  CREATE_BOOKING: "CREATE_BOOKING",
  EDIT_ROUTE: "EDIT_ROUTE",
  EDIT_BOOKING: "EDIT_BOOKING"
};

export const ROLES = {
  ADM_IT: "ADM-IT",
  ADM_OPS: "ADM-OPS",
  FIN_OPS: "FIN-OPS",
  TRK_ADM: "TRK-ADM",
  TRK_OPS: "TRK-OPS",
  TRK_OPS_LR: "TRK-OPS-LR",
  TRF_ADM: "TRF-ADM",
  TRF_OPS: "TRF-OPS",
  GRN_OPS: "GRN-OPS",
  DOC_OPS: "DOC-OPS",
  HR: "HR",
  CS: "CS",
  VEN: "VEN"
};

const SELECT_ADVANCE_TYPE = "Select Advance Type";
const ADV_ZAST_NOW = "ADV ZAST NOW";
const FNF = "FNF";

export const ADVANCE_TYPES = [SELECT_ADVANCE_TYPE, ADV_ZAST_NOW, FNF];

const SELECT_VEHICLE_CATEGORY = "SELECT VEHICLE CATEGORY";
const CONTRACT = "Contract";
const ZASTNOW = "ZastNow";

export const VEHICLE_CATEGORY_APPROVE = [
  SELECT_VEHICLE_CATEGORY,
  CONTRACT,
  ZASTNOW
];

export const OS = {
  ANDROID: "android",
  IOS: "ios"
};

export const VEHICLE_RUNNING_STATUS = {
  RUNNING: 1,
  STOPPED: 0,
  NOT_IN_CONTACT: -1,
  UNKNOWN: -2
};
