import React from "react";
import { Text } from "react-native";
import { createBottomTabNavigator } from "react-navigation";
import { Icon } from "react-native-elements";
import NewTrips from "../../containers/AllTrips/newTrip";
import InTransitTrips from "../../containers/AllTrips/inTransit";
import AtDestinationTrips from "../../containers/AllTrips/atDestination";
import { WrapInSafeArea } from "../appStack";
import colors from "../../components/common/Colors";
import { NAVIGATE } from "../../../constants";

const TextView = ({ text, tintColor }) => {
  return (
    <Text style={{ color: tintColor, fontSize: 12, alignSelf: "center" }}>
      {text}
    </Text>
  );
};

const IconView = ({ name, tintColor }) => {
  return <Icon name={name} type="material-community" color={tintColor} />;
};

const AllTripsScreen = createBottomTabNavigator(
  {
    [NAVIGATE.TRIPS_NEW]: props => WrapInSafeArea(NewTrips, props),
    [NAVIGATE.TRIPS_IN_TRANSIT]: props => WrapInSafeArea(InTransitTrips, props),
    [NAVIGATE.TRIPS_AT_DESTINATION]: props =>
      WrapInSafeArea(AtDestinationTrips, props)
  },
  {
    initialRouteName: NAVIGATE.TRIPS_NEW,
    tabBarOptions: {
      activeTintColor: colors.darkblue,
      inactiveTintColor: colors.black65,
      style: {
        backgroundColor: colors.white,
        borderBottomWidth: 0,
        borderBottomColor: colors.blueColor
      },
      indicatorStyle: {
        backgroundColor: colors.redColor
      }
    },
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        if (routeName === NAVIGATE.TRIPS_NEW) {
          return IconView({ name: "truck-fast", tintColor });
        } else if (routeName === NAVIGATE.TRIPS_IN_TRANSIT) {
          return IconView({ name: "transit-connection-variant", tintColor });
        } else if (routeName === NAVIGATE.TRIPS_AT_DESTINATION) {
          return IconView({ name: "map-marker", tintColor });
        }
      },
      tabBarLabel: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        if (routeName === NAVIGATE.TRIPS_NEW) {
          return TextView({ tintColor, text: "New" });
        } else if (routeName === NAVIGATE.TRIPS_IN_TRANSIT) {
          return TextView({ tintColor, text: "In Transit" });
        } else if (routeName === NAVIGATE.TRIPS_AT_DESTINATION) {
          return TextView({ tintColor, text: "At Destination" });
        }
      }
    })
  }
);

export default AllTripsScreen;
