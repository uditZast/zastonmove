import React from "react";
import { Text } from "react-native";
import { createBottomTabNavigator } from "react-navigation";
import colors from "../../components/common/Colors";
import { Icon } from "react-native-elements";
import { WrapInSafeArea } from "../appStack";
import { NAVIGATE } from "../../../constants";
import NewRequestBookings from "../../containers/BookingList/NewRequestBookings";
import AcceptedBookings from "../../containers/BookingList/AcceptedBookings";
import AssignedBookings from "../../containers/BookingList/AssignedBookings";
import DockedBookings from "../../containers/BookingList/DockedBookings";

const TextView = ({ tintColor, text }) => {
  return (
    <Text
      style={{
        color: tintColor,
        fontSize: 12,
        alignSelf: "center"
      }}
    >
      {text}
    </Text>
  );
};
const IconView = ({ tintColor, name }) => {
  return <Icon name={name} type="material-community" color={tintColor} />;
};
const AllBookingScreen = createBottomTabNavigator(
  {
    [NAVIGATE.BOOKINGS_NEW_REQUEST]: props =>
      WrapInSafeArea(NewRequestBookings, props),
    [NAVIGATE.BOOKINGS_ACCEPTED]: props =>
      WrapInSafeArea(AcceptedBookings, props),
    [NAVIGATE.BOOKINGS_ASSIGNED]: props =>
      WrapInSafeArea(AssignedBookings, props),
    [NAVIGATE.BOOKINGS_DOCKED]: props => WrapInSafeArea(DockedBookings, props)
  },
  {
    initialRouteName: NAVIGATE.BOOKINGS_NEW_REQUEST,
    tabBarOptions: {
      activeTintColor: colors.darkblue,
      inactiveTintColor: colors.black65,
      style: {
        backgroundColor: colors.white,
        borderBottomWidth: 0,
        borderBottomColor: colors.blueColor
      },
      indicatorStyle: {
        backgroundColor: colors.redColor
      }
    },
    headerMode: null,
    navigationOptions: { header: null },
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName = "";
        if (routeName === NAVIGATE.BOOKINGS_NEW_REQUEST) {
          return IconView({ tintColor, name: "file-outline" });
        } else if (routeName === NAVIGATE.BOOKINGS_ACCEPTED) {
          return IconView({ tintColor, name: "file-check-outline" });
        } else if (routeName === NAVIGATE.BOOKINGS_ASSIGNED) {
          return IconView({ tintColor, name: "truck-check" });
        } else if (routeName === NAVIGATE.BOOKINGS_DOCKED) {
          return IconView({ tintColor, name: "bus-clock" });
        }
        return <Icon name={iconName} size={25} color={colors.blueColor} />;
      },
      tabBarLabel: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        if (routeName === NAVIGATE.BOOKINGS_NEW_REQUEST) {
          return TextView({ tintColor, text: "New Request" });
        } else if (routeName === NAVIGATE.BOOKINGS_ACCEPTED) {
          return TextView({ tintColor, text: "Accepted" });
        } else if (routeName === NAVIGATE.BOOKINGS_ASSIGNED) {
          return TextView({ tintColor, text: "Assigned" });
        } else if (routeName === NAVIGATE.BOOKINGS_DOCKED) {
          return TextView({ tintColor, text: "Docked" });
        }
      }
    })
  }
);
export default AllBookingScreen;
