import { createStackNavigator } from "react-navigation";
import SignIn from "../containers/SignIn";
import OtpScreen from "../containers/OtpLogin";
import ForcedUpdate from "../containers/ForcedUpdate";
import { NAVIGATE } from "../../constants";
import { WrapInSafeArea } from "./appStack";

const authStack = createStackNavigator(
  {
    [NAVIGATE.SIGN_IN]: {
      screen: props => WrapInSafeArea(SignIn, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.OTP_SCREEN]: {
      screen: props => WrapInSafeArea(OtpScreen, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.FORCED_UPDATE]: {
      screen: props => WrapInSafeArea(ForcedUpdate, props),
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: NAVIGATE.SIGN_IN
  }
);

export default authStack;
