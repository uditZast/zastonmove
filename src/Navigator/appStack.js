import React, { Fragment } from "react";
import { createStackNavigator } from "react-navigation";
import { SafeAreaView } from "react-native";
import { DrawerNav } from "./Drawer/drawerNavigator";
import TripDetailsPage from "../components/trips/TripDetailsPage";
import CreateBookingForms from "../components/bookings/forms/CreateBookingForms";
import BookingHistory from "../components/bookings/bookingactions/BookingHistory";
import CrossDockModal from "../containers/CrossDockModal";
import RunningStatusModal from "../containers/RunningStatusModal";
import SearchResultScreen from "../components/bookings/forms/SearchResultScreen";
import LocationMapScreen from "../components/LocationMapScreen";
import BookingOneWay from "../containers/Screens/Bookings/bookingOneWay.js";
import BookingTwoWay from "../containers/Screens/Bookings/bookingTwoWay.js";
import DryRun from "../containers/Screens/Bookings/dryRun.js";
import EditDryRun from "../containers/Screens/EditBooking/editDryRun";
import TripDetails from "../containers/TripDetails";
import TripComments from "../containers/TripDetails/comments";
import EditTrip from "../containers/EditTrip";
import EditTripRoute from "../containers/EditTrip/editRoute";
import EditTripCustomer from "../containers/EditTrip/editCustomer";
import HaltTrip from "../containers/TripDetails/haltTrip";
import TripHistory from "../containers/TripDetails/history";
import TripRunningStatus from "../containers/TripDetails/runningStatus";
import { NAVIGATE } from "../../constants";
import colors from "../components/common/Colors";
import SearchResult from "../containers/SearchResult";
import EditBookingTwoWay from "../containers/Screens/EditBooking/editBookingTwoWay";
import EditBookingOneWay from "../containers/Screens/EditBooking/editBookingOneWay";
import TripCommentsSearch from "../containers/TripCommentsSearch";
import CrossDockTrip from "../containers/CrossDock";
import LocationSearch from "../containers/LocationSearch";
import VendorApprovalForm from "../containers/VendorApprovalForm";
import TripCharges from "../containers/TripCharges";
import AdvancePayments from "../containers/AdvancePayments";
import VendorDetails from "../containers/VendorDetails";
import VehicleApprovalForm from "../containers/VehicleApprovalForm";
import TrackingSearchResultsPage from "../containers/TrackingSearchResult";

export const WrapInSafeArea = (Comp, props) => {
  return (
    <Fragment>
      {/*<SafeAreaView style={{ flex: 1 }}>*/}
      {/*  <Comp {...props} />*/}
      {/*</SafeAreaView>*/}
      <SafeAreaView style={{ flex: 0, backgroundColor: colors.blue }} />
      <SafeAreaView style={{ flex: 1 }}>
        <Comp {...props} style={{ flex: 1, backgroundColor: colors.white }} />
      </SafeAreaView>
    </Fragment>
  );
};

const appStack = createStackNavigator(
  {
    [NAVIGATE.HOME]: {
      screen: DrawerNav,
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.BOOKING_ONE_WAY]: {
      screen: props => WrapInSafeArea(BookingOneWay, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.BOOKING_TWO_WAY]: {
      screen: props => WrapInSafeArea(BookingTwoWay, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.DRY_RUN]: {
      screen: props => WrapInSafeArea(DryRun, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.EDIT_DRY_RUN]: {
      screen: props => WrapInSafeArea(EditDryRun, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.TRIP_DETAILS]: {
      screen: props => WrapInSafeArea(TripDetails, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.TRIP_COMMENTS]: {
      screen: props => WrapInSafeArea(TripComments, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.TRIP_COMMENTS_SEARCH]: {
      screen: props => WrapInSafeArea(TripCommentsSearch, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.HALT_TRIP]: {
      screen: props => WrapInSafeArea(HaltTrip, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.EDIT_TRIP]: {
      screen: props => WrapInSafeArea(EditTrip, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.EDIT_TRIP_ROUTE]: {
      screen: props => WrapInSafeArea(EditTripRoute, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.EDIT_TRIP_CUSTOMER]: {
      screen: props => WrapInSafeArea(EditTripCustomer, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.TRIP_HISTORY]: {
      screen: props => WrapInSafeArea(TripHistory, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.TRIP_RUNNING_STATUS]: {
      screen: props => WrapInSafeArea(TripRunningStatus, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.SEARCH_RESULT]: {
      screen: props => WrapInSafeArea(SearchResult, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.EDIT_BOOKING_TWO_WAY]: {
      screen: props => WrapInSafeArea(EditBookingTwoWay, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.EDIT_BOOKING_ONE_WAY]: {
      screen: props => WrapInSafeArea(EditBookingOneWay, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.CROSS_DOCK_TRIP]: {
      screen: props => WrapInSafeArea(CrossDockTrip, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.LOCATION_SEARCH]: {
      screen: props => WrapInSafeArea(LocationSearch, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.VENDOR_EDIT]: {
      screen: props => WrapInSafeArea(VendorApprovalForm, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.ADD_TRIP_CHARGES]: {
      screen: props => WrapInSafeArea(TripCharges, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.ADVANCE_PAYMENTS]: {
      screen: props => WrapInSafeArea(AdvancePayments, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.VENDOR_DETAILS]: {
      screen: props => WrapInSafeArea(VendorDetails, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.VEHICLE_EDIT]: {
      screen: props => WrapInSafeArea(VehicleApprovalForm, props),
      navigationOptions: {
        header: null
      }
    },
    [NAVIGATE.TRACKING_SEARCH_RESULT]: {
      screen: props => WrapInSafeArea(TrackingSearchResultsPage, props),
      navigationOptions: {
        header: null
      }
    },

    TripDetails: {
      screen: TripDetailsPage,
      navigationOptions: {
        headerStyle: {
          backgroundColor: "#3096f3"
        }
      }
    },
    AdHocOne: {
      screen: CreateBookingForms
    },
    BookingHistory: {
      screen: BookingHistory
    },
    EditVehiclePhone: {
      screen: CrossDockModal
    },
    RunningStatus: {
      screen: RunningStatusModal
    },
    SearchResult: {
      screen: SearchResultScreen
    },
    LocationScreen: {
      screen: LocationMapScreen
    }
  },
  {
    initialRouteName: NAVIGATE.HOME
  }
);

export default appStack;
