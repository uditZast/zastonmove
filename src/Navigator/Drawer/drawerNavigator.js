import React from "react";
import { createDrawerNavigator, createStackNavigator } from "react-navigation";
import { Icon } from "react-native-elements";
import { WrapInSafeArea } from "../appStack";
import { NAVIGATE } from "../../../constants";
import AllTrips from "../BottomTab/allTrips";
import AllBookings from "../BottomTab/allBookings";
import drawerSideBar from "./drawerSideBar";
import Dashboard from "../../containers/Dashboard";
import MyBookings from "../../containers/MyBookings";
import AvailableVehicles from "../../containers/AvailableVehicles";
import VehicleApprovalPending from "../../containers/VehicleApprovalPending";
import colors from "../../colors/Colors";
import Search from "../../containers/Search";
import VendorApprovalPending from "../../containers/VendorApprovalPending";
import RequestTripAdvance from "../../containers/RequestTripAdvance";
import TrackingSearch from "../../containers/TrackingSearch";

const MenuIcon = navigation => {
  return (
    <Icon
      onPress={() => navigation.toggleDrawer()}
      containerStyle={{ marginLeft: 20 }}
      name="menu"
      underlayColor={"transparent"}
      color={colors.white}
    />
  );
};

const dashboard = createStackNavigator({
  [NAVIGATE.DASHBOARD]: {
    screen: props => WrapInSafeArea(Dashboard, props),
    navigationOptions: {
      header: null
    }
  }
});

const allTrips = createStackNavigator({
  [NAVIGATE.ALL_TRIPS]: {
    screen: AllTrips,
    navigationOptions: {
      header: null
    }
  }
});

const allBooking = createStackNavigator({
  [NAVIGATE.ALL_BOOKINGS]: {
    screen: AllBookings,
    navigationOptions: {
      header: null
    }
  }
});

// const allBooking = createStackNavigator({
//   [NAVIGATE.ALL_BOOKINGS]: {
//     screen: AllBookings,
//     navigationOptions: ({ navigation }) => ({
//       title: "All Bookings",
//       headerLeft: MenuIcon(navigation),
//       headerStyle: {
//         backgroundColor: colors.blueColor
//       },
//       headerTintColor: colors.white
//     })
//   }
// });

const myBookings = createStackNavigator({
  [NAVIGATE.MY_BOOKINGS]: {
    screen: props => WrapInSafeArea(MyBookings, props),
    navigationOptions: {
      header: null
    }
  }
});

const search = createStackNavigator({
  [NAVIGATE.SEARCH]: {
    screen: props => WrapInSafeArea(Search, props),
    navigationOptions: {
      header: null
    }
  }
});

const availableVehicles = createStackNavigator({
  [NAVIGATE.AVAILABLE_VEHICLES]: {
    screen: props => WrapInSafeArea(AvailableVehicles, props),
    navigationOptions: {
      header: null
    }
  }
});

const vendorApprovalPending = createStackNavigator({
  [NAVIGATE.VENDOR_APPROVAL_PENDING]: {
    screen: props => WrapInSafeArea(VendorApprovalPending, props),
    navigationOptions: {
      header: null
    }
  }
});

const requestTripAdvance = createStackNavigator({
  [NAVIGATE.REQUEST_TRIP_ADVANCE]: {
    screen: props => WrapInSafeArea(RequestTripAdvance, props),
    navigationOptions: {
      header: null
    }
  }
});

const vehicleApprovalPending = createStackNavigator({
  [NAVIGATE.VEHICLE_APPROVAL_PENDING]: {
    screen: props => WrapInSafeArea(VehicleApprovalPending, props),
    navigationOptions: {
      header: null
    }
  }
});

const trackingSearch = createStackNavigator({
  [NAVIGATE.TRACKING_SEARCH]: {
    screen: props => WrapInSafeArea(TrackingSearch, props),
    navigationOptions: {
      header: null
    }
  }
});

export const DrawerNav = createDrawerNavigator(
  {
    [NAVIGATE.DASHBOARD]: dashboard,
    [NAVIGATE.ALL_TRIPS]: allTrips,
    [NAVIGATE.ALL_BOOKINGS]: allBooking,
    [NAVIGATE.MY_BOOKINGS]: myBookings,
    [NAVIGATE.SEARCH]: search,
    [NAVIGATE.AVAILABLE_VEHICLES]: availableVehicles,
    [NAVIGATE.VENDOR_APPROVAL_PENDING]: vendorApprovalPending,
    [NAVIGATE.REQUEST_TRIP_ADVANCE]: requestTripAdvance,
    [NAVIGATE.VEHICLE_APPROVAL_PENDING]: vehicleApprovalPending,
    [NAVIGATE.TRACKING_SEARCH]: trackingSearch
  },
  {
    contentComponent: drawerSideBar
    // unmountInactiveRoutes: true
  }
);
