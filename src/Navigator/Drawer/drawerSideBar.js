import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, StyleSheet, Text, View } from "react-native";
import colors from "../../colors/Colors";
import man from "../../images/man.png";
import { Icon } from "react-native-elements";
import { NAVIGATE } from "../../../constants";
import { signOut } from "../../modules/auth";
import { connect } from "react-redux";
import { ScrollView } from "react-navigation";

class DrawerSideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  };

  logout = () => {
    const { signOut } = this.props;
    signOut().then(() => this.props.navigation.navigate(NAVIGATE.SIGN_IN));
  };

  render() {
    const { username, role } = this.props;

    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.headerContainer}>
            <View style={styles.headerStyle}>
              <Image source={man} style={styles.logoStyle}></Image>
              <Text style={styles.headingTextStyle}>Hi, {username} !</Text>
            </View>
          </View>

          <View style={styles.screenContainer}>
            <View
              style={[
                styles.screenStyle,
                this.props.activeItemKey === NAVIGATE.DASHBOARD
                  ? styles.activeBackgroundColor
                  : null
              ]}
            >
              <Icon
                name="home"
                type="feather"
                size={18}
                color={
                  this.props.activeItemKey === NAVIGATE.DASHBOARD
                    ? colors.blueColor
                    : colors.grey
                }
              />

              <Text
                style={[
                  styles.screenTextStyle,
                  this.props.activeItemKey === NAVIGATE.DASHBOARD
                    ? styles.selectedTextStyle
                    : null
                ]}
                onPress={this.navigateToScreen(NAVIGATE.DASHBOARD)}
              >
                Dashboard
              </Text>
            </View>

            <View style={styles.lineStyle} />

            <View
              style={[
                styles.screenStyle,
                this.props.activeItemKey === NAVIGATE.SEARCH
                  ? styles.activeBackgroundColor
                  : null
              ]}
            >
              <Icon
                name="search"
                type="feather"
                size={18}
                color={
                  this.props.activeItemKey === NAVIGATE.SEARCH
                    ? colors.blueColor
                    : colors.grey
                }
              />

              <Text
                style={[
                  styles.screenTextStyle,
                  this.props.activeItemKey === NAVIGATE.SEARCH
                    ? styles.selectedTextStyle
                    : null
                ]}
                onPress={this.navigateToScreen(NAVIGATE.SEARCH)}
              >
                Search
              </Text>
            </View>

            <View style={styles.lineStyle} />

            <View
              style={[
                styles.screenStyle,
                this.props.activeItemKey === NAVIGATE.AVAILABLE_VEHICLES
                  ? styles.activeBackgroundColor
                  : null
              ]}
            >
              <Icon
                name="book"
                type="feather"
                size={18}
                color={
                  this.props.activeItemKey === NAVIGATE.AVAILABLE_VEHICLES
                    ? colors.blueColor
                    : colors.grey
                }
              />

              <Text
                style={[
                  styles.screenTextStyle,
                  this.props.activeItemKey === NAVIGATE.AVAILABLE_VEHICLES
                    ? styles.selectedTextStyle
                    : null
                ]}
                onPress={this.navigateToScreen(NAVIGATE.AVAILABLE_VEHICLES)}
              >
                Available Vehicles
              </Text>
            </View>

            <View style={styles.lineStyle} />

            <View
              style={[
                styles.screenStyle,
                this.props.activeItemKey === NAVIGATE.ALL_BOOKINGS
                  ? styles.activeBackgroundColor
                  : null
              ]}
            >
              <Icon
                name="book"
                type="feather"
                size={18}
                color={
                  this.props.activeItemKey === NAVIGATE.ALL_BOOKINGS
                    ? colors.blueColor
                    : colors.grey
                }
              />

              <Text
                style={[
                  styles.screenTextStyle,
                  this.props.activeItemKey === NAVIGATE.ALL_BOOKINGS
                    ? styles.selectedTextStyle
                    : null
                ]}
                onPress={this.navigateToScreen(NAVIGATE.ALL_BOOKINGS)}
              >
                All Booking
              </Text>
            </View>

            <View style={styles.lineStyle} />

            {/*<View*/}
            {/*  style={[*/}
            {/*    styles.screenStyle,*/}
            {/*    this.props.activeItemKey === NAVIGATE.CREATE_BOOKING*/}
            {/*      ? styles.activeBackgroundColor*/}
            {/*      : null*/}
            {/*  ]}*/}
            {/*>*/}
            {/*  <Icon*/}
            {/*    name="book"*/}
            {/*    type="feather"*/}
            {/*    size={18}*/}
            {/*    color={*/}
            {/*      this.props.activeItemKey === NAVIGATE.CREATE_BOOKING*/}
            {/*        ? colors.blueColor*/}
            {/*        : colors.grey*/}
            {/*    }*/}
            {/*  />*/}

            {/*  <Text*/}
            {/*    style={[*/}
            {/*      styles.screenTextStyle,*/}
            {/*      this.props.activeItemKey === NAVIGATE.CREATE_BOOKING*/}
            {/*        ? styles.selectedTextStyle*/}
            {/*        : null*/}
            {/*    ]}*/}
            {/*    onPress={this.navigateToScreen(NAVIGATE.CREATE_BOOKING)}*/}
            {/*  >*/}
            {/*    Create Booking*/}
            {/*  </Text>*/}
            {/*</View>*/}

            {/*<View style={styles.lineStyle} />*/}

            <View
              style={[
                styles.screenStyle,
                this.props.activeItemKey === NAVIGATE.MY_BOOKINGS
                  ? styles.activeBackgroundColor
                  : null
              ]}
            >
              <Icon
                name="book-open"
                type="feather"
                size={18}
                color={
                  this.props.activeItemKey === NAVIGATE.MY_BOOKINGS
                    ? colors.blueColor
                    : colors.grey
                }
              />

              <Text
                style={[
                  styles.screenTextStyle,
                  this.props.activeItemKey === NAVIGATE.MY_BOOKINGS
                    ? styles.selectedTextStyle
                    : null
                ]}
                onPress={this.navigateToScreen(NAVIGATE.MY_BOOKINGS)}
              >
                My Bookings
              </Text>
            </View>

            <View style={styles.lineStyle} />

            <View
              style={[
                styles.screenStyle,
                this.props.activeItemKey === NAVIGATE.ALL_TRIPS
                  ? styles.activeBackgroundColor
                  : null
              ]}
            >
              <Icon
                name="truck"
                type="feather"
                size={18}
                color={
                  this.props.activeItemKey === NAVIGATE.ALL_TRIPS
                    ? colors.blueColor
                    : colors.grey
                }
              />

              <Text
                style={[
                  styles.screenTextStyle,
                  this.props.activeItemKey === NAVIGATE.ALL_TRIPS
                    ? styles.selectedTextStyle
                    : null
                ]}
                onPress={this.navigateToScreen(NAVIGATE.ALL_TRIPS)}
              >
                All Trips
              </Text>
            </View>

            <View style={styles.lineStyle} />

            {/*====================TRACKING SEARCH----------------*/}
            <View
              style={[
                styles.screenStyle,
                this.props.activeItemKey === NAVIGATE.TRACKING_SEARCH
                  ? styles.activeBackgroundColor
                  : null
              ]}
            >
              <Icon
                size={18}
                name="crosshairs-gps"
                type="material-community"
                underlayColor={"transparent"}
                color={
                  this.props.activeItemKey === NAVIGATE.TRACKING_SEARCH
                    ? colors.blueColor
                    : colors.grey
                }
              />

              <Text
                style={[
                  styles.screenTextStyle,
                  this.props.activeItemKey === NAVIGATE.TRACKING_SEARCH
                    ? styles.selectedTextStyle
                    : null
                ]}
                onPress={this.navigateToScreen(NAVIGATE.TRACKING_SEARCH)}
              >
                Tracking
              </Text>
            </View>

            <View style={styles.lineStyle} />

            <View
              style={[
                styles.screenStyle,
                this.props.activeItemKey === NAVIGATE.VENDOR_APPROVAL_PENDING
                  ? styles.activeBackgroundColor
                  : null
              ]}
            >
              <Icon
                name="book"
                type="feather"
                size={18}
                color={
                  this.props.activeItemKey === NAVIGATE.VENDOR_APPROVAL_PENDING
                    ? colors.blueColor
                    : colors.grey
                }
              />

              <Text
                style={[
                  styles.screenTextStyle,
                  this.props.activeItemKey === NAVIGATE.VENDOR_APPROVAL_PENDING
                    ? styles.selectedTextStyle
                    : null
                ]}
                onPress={this.navigateToScreen(
                  NAVIGATE.VENDOR_APPROVAL_PENDING
                )}
              >
                Vendor Approval List
              </Text>
            </View>

            {role === "FIN-ADM" ||
            role === "ADM-IT" ||
            role === "ADM-OPS" ||
            role === "TRF-ADM" ||
            role === "TRF-OPS" ? (
              <View>
                <View style={styles.lineStyle} />

                <View
                  style={[
                    styles.screenStyle,
                    this.props.activeItemKey === NAVIGATE.REQUEST_TRIP_ADVANCE
                      ? styles.activeBackgroundColor
                      : null
                  ]}
                >
                  <Icon
                    name="book"
                    type="feather"
                    size={18}
                    color={
                      this.props.activeItemKey === NAVIGATE.REQUEST_TRIP_ADVANCE
                        ? colors.blueColor
                        : colors.grey
                    }
                  />

                  <Text
                    style={[
                      styles.screenTextStyle,
                      this.props.activeItemKey === NAVIGATE.REQUEST_TRIP_ADVANCE
                        ? styles.selectedTextStyle
                        : null
                    ]}
                    onPress={this.navigateToScreen(
                      NAVIGATE.REQUEST_TRIP_ADVANCE
                    )}
                  >
                    Request Trip Advance
                  </Text>
                </View>
              </View>
            ) : null}

            <View style={styles.lineStyle} />

            <View
              style={[
                styles.screenStyle,
                this.props.activeItemKey === NAVIGATE.VEHICLE_APPROVAL_PENDING
                  ? styles.activeBackgroundColor
                  : null
              ]}
            >
              <Icon
                name="book"
                type="feather"
                size={18}
                color={
                  this.props.activeItemKey === NAVIGATE.VEHICLE_APPROVAL_PENDING
                    ? colors.blueColor
                    : colors.grey
                }
              />

              <Text
                style={[
                  styles.screenTextStyle,
                  this.props.activeItemKey === NAVIGATE.VEHICLE_APPROVAL_PENDING
                    ? styles.selectedTextStyle
                    : null
                ]}
                onPress={this.navigateToScreen(
                  NAVIGATE.VEHICLE_APPROVAL_PENDING
                )}
              >
                Vehicle Approval
              </Text>
            </View>

            <View style={styles.lineStyle} />

            <View
              style={[
                styles.screenStyle,
                this.props.activeItemKey === NAVIGATE.SIGN_OUT
                  ? styles.activeBackgroundColor
                  : null
              ]}
            >
              <Icon
                name="log-out"
                type="feather"
                size={18}
                color={
                  this.props.activeItemKey === NAVIGATE.SIGN_OUT
                    ? colors.blueColor
                    : colors.grey
                }
              />

              <Text
                style={[
                  styles.screenTextStyle,
                  this.props.activeItemKey === NAVIGATE.SIGN_OUT
                    ? styles.selectedTextStyle
                    : null
                ]}
                onPress={this.logout}
              >
                Logout
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center"
  },
  headerContainer: {
    height: 120,
    flexDirection: "row"
  },
  screenContainer: {
    paddingTop: 10,
    width: "100%"
  },
  screenStyle: {
    height: 40,
    marginTop: 2,
    paddingLeft: 25,
    flexDirection: "row",
    alignItems: "center",
    width: "100%"
  },
  screenTextStyle: {
    fontSize: 16,
    marginLeft: 15,
    padding: 5,
    textAlign: "left",
    flex: 1,
    fontFamily: "CircularStd-Book"
  },
  selectedTextStyle: {
    color: colors.blueColor,
    paddingTop: 5,
    fontFamily: "CircularStd-Book"
  },
  activeBackgroundColor: {
    fontWeight: "400",
    backgroundColor: colors.white
  },
  headerStyle: {
    flex: 1,
    width: 280,
    flexDirection: "row",
    backgroundColor: colors.blueColor,
    alignItems: "flex-end",
    paddingBottom: 10
  },
  headingTextStyle: {
    color: colors.white,
    fontFamily: "CircularStd-Book",
    fontSize: 18,
    padding: 5,
    marginLeft: 10,
    justifyContent: "center"
  },
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 5
  },
  logoStyle: {
    height: 35,
    width: 35,
    marginLeft: 20,
    resizeMode: "contain"
  }
});

const mapStateToProps = state => {
  const { auth, users } = state;
  const { authenticatedUser } = auth;
  const { role, basicInfo: { username = "User" } = {} } =
    users[authenticatedUser] || {};
  return { username, role };
};

const mapDispatchToProps = dispatch => {
  return {
    signOut: () => dispatch(signOut())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DrawerSideBar);
