import React from "react";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import AuthLoading from "../containers/Auth";
import AuthStack from "./authStack";
import AppStack from "./appStack";
import { STACK } from "../../constants";

const AppNavigator = createSwitchNavigator(
  {
    [STACK.AUTH_LOADING]: { screen: AuthLoading },
    [STACK.AUTH]: { screen: AuthStack },
    [STACK.APP]: { screen: AppStack }
  },
  {
    initialRouteName: STACK.AUTH_LOADING
  }
);

export default createAppContainer(AppNavigator);
