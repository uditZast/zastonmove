import React from "react";

const Colors = {
  white: "#fff",
  black: "#000",
  borderColorlogin: "#eff0f1",
  loginGradient1: "#c92e2a",
  loginGradient2: "#4b4b4b",
  loginGradient3: "#c894cc",
  loginButton: "#63deb8",
  textcolor: "#484848",
  light_grey: "#F8F3F3",
  border_grey: "#c7c7c7",
  listBackground: "#eeeeee",
  greenColor: "#08F713",
  redColor: "#F75649",
  blueColor: "#3096f3",
  bookedColor: "#e9960c",
  lightBlue: "#C1F8FE",
  light_purple: "#F4DDFC",
  light_yellow: "#FCF7DD",
  light_green: "#D3FA9A",
  light_pink: "#F6C7FD",
  dark_grey: "#777677",
  blue: "#F2F9FC",
  // blue: '#EBF5FB',
  red: "#F75649",
  warningColor: "#f89406",
  maroon: "#CD6155",
  grey: "#8A898A"
};

export default Colors;
