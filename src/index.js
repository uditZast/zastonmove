import React from "react";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";
import Navigation from "./Navigator";
import Store from "./configureStore";
import NavigationService from "./NavigationService";
import { subscribeToNetworkChange } from "./modules/network";

export default () => {
  const { store, persistor } = Store.configureStore();

  store.dispatch(subscribeToNetworkChange());

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Navigation
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </PersistGate>
    </Provider>
  );
};
