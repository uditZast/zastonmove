import React from "react";

class DeviceInfoHelper {
  constructor() {
    this.info = {};
  }

  getInfo(key) {
    if (
      !(Object.keys(this.info).length === 0 && this.info.constructor === Object)
    ) {
      if (!!key) {
        return this.info[key];
      }

      return this.info;
    }
    this.info = {};

    if (!!key) {
      return this.info[key];
    }

    return this.info;
  }

  setInfo(info) {
    this.info = Object.assign({}, this.info, info);
  }

  reset() {
    this.info = {};
  }
}

const DeviceInfo = new DeviceInfoHelper();

export default DeviceInfo;
