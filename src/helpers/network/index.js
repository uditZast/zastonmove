import { HOST, M_HOST, NAVIGATE } from "../../../constants";
import Deviceinfo from "../../Deviceinfo.js";
import urlParser from "url-parse";
import NavigationService from "../../NavigationService";

export const doRequest = async requestdata => {
  const accessToken = Deviceinfo.getInfo("accessToken");

  //create request config according to data
  let headers = {
    ...(accessToken && { Authorization: `Token ${accessToken}` }),
    Accept: "application/json",
    "Content-Type": requestdata.contentType
      ? "multipart/form-data"
      : "application/json"
    //"Content-Type": "application/json"
  };

  let queryParams = {};
  let postData = null;
  let queryMethod = null;
  const { params = {}, isMAPI = false } = requestdata;

  if (!requestdata.method || requestdata.method.toUpperCase() === "GET") {
    if (requestdata.params) {
      queryParams = Object.assign({}, requestdata.params, queryParams);
    }
    queryMethod = { method: "GET" };
  } else {
    if (requestdata.body) {
      let post_body = requestdata.body || {};
      postData = { body: post_body };
      queryMethod = { method: requestdata.method };
    } else {
      let post_body = requestdata.data || {};
      postData = { body: JSON.stringify(post_body) };
      queryMethod = { method: requestdata.method };
    }
  }

  const parsedUrl = requestdata.isMAPI
    ? new urlParser(`${M_HOST}${requestdata.url}`, true)
    : new urlParser(`${HOST}${requestdata.url}`, true);
  queryParams = Object.assign({}, parsedUrl.query, params, queryParams);
  //Forming the new URL
  parsedUrl.set("query", queryParams);

  if (!!requestdata.headers) {
    headers = Object.assign({}, requestdata.headers, headers);
  }

  let additional = Object.assign(
    {},
    { credentials: "same-origin" },
    queryMethod,
    { headers: headers },
    requestdata.extras,
    postData
  );

  console.log("parsedUrl, additional ----", parsedUrl, additional);
  try {
    const r = await fetch(parsedUrl.toString(), additional);
    const response = await r.json();
    const { status } = r;
    if (status === 401) {
      NavigationService.navigate(NAVIGATE.SIGN_IN);
    }
    return response;
  } catch (e) {
    console.log("error in fetch network", e);
    throw e;
  }
};
