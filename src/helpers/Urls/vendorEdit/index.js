export const fetchVendorEditURL = () => {
  return "/update-vendor";
};

export const fetchVehicleEditURL = () => {
  return "/update-vehicle";
};
