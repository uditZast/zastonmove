export const fetchAvailableVehiclesURL = () => {
  return "/available-vehicles";
};

export const fetchVersionCheckURL = () => {
  return "/available-vehicles";
};

// export const fetchBookingsByStatusURL = status => {
//   return `/get-bookings/requested`;
// };

export const fetchBookingsByStatusURL = status => {
  return `/get-bookings/` + status;
};

export const fetchDownloadReport = () => {
  return "/available-vehicle-report";
};

// export const fetchBookingsByStatusURL = status => {
//   console.log('status bookingggggg----', status);
//   return `/get-allBookings/${status}`;
// };
