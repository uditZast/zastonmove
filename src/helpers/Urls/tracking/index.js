export const getTrackingDetails = (page, vehicle_id, start_date, end_date) => {
  return `/track-vehicle/${vehicle_id}/${page}?start_date=${start_date}&end_date=${end_date}`;
};
