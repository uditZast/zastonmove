export const requestTripAdvanceUrl = () => {
  return "/get-charges-and-advance";
};

export const saveTripChargesUrl = () => {
  return "/save-trip-charges";
};

export const saveAdvanceTripUrl = () => {
  return "/create-advance-request";
};

export const sendOtpToVendorUrl = () => {
  return "/advance-otp-generate";
};

export const verifyOtpUrl = () => {
  return "/advance-otp-verify";
};
