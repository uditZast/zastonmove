export const getSignInURL = () => {
  return "/login";
};

export const fetchInitialDataURL = () => {
  return "/initial-data";
};

export const verifyOtpURL = () => {
  return "/verify-otp";
};

export const sendOtpURL = () => {
  return "/resend-otp";
};
