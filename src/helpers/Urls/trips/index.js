export const fetchAllTripsByStatusURL = status => {
  return `/get-trips/${status}`;
};

export const markTripAsBeginURL = () => {
  return `/begin-trip`;
};

export const markTripAsCloseURL = () => {
  return `/close-trip`;
};

export const markTripAsArrivedURL = () => {
  return `/arrived-destination`;
};

export const markTripAsTouchingInURL = () => {
  return `/arrived-intermediate`;
};

export const markTripAsHaltedURL = () => {
  return `/halt-trip`;
};

export const markTripAsTerminateURL = () => {
  return `/terminate-trip`;
};

export const editTripURL = () => {
  return "/edit-trip-vehicle";
};

export const editTripRouteURL = () => {
  return "/edit-trip-route";
};

export const editTripCustomerURL = () => {
  return "/edit-trip-customer";
};

export const fetchTripDetailsURL = () => {
  return "/history-comments";
};

export const addTripCommentsURL = () => {
  return `/add-trip-comment`;
};

export const addTripRunningStatusURL = () => {
  return `/add-trip-running-status`;
};

export const updateDriverPhoneURL = () => {
  return `/edit-phone`;
};

export const updateReferenceURL = () => {
  return `/edit-reference`;
};

export const updateTripTATURL = () => {
  return `/edit-tat`;
};

export const plotRouteURL = () => {
  return `/plot-route`;
};

export const crossDockTripURL = () => {
  return `/crossdock`;
};

export const reverseGeocodeURL = (lat, long) => {
  return `/reverse-geocode?lat=${lat}&long=${long}`;
};

export const fetchLocationsURL = q => {
  return `/map-search-results?search=${q}`;
};
