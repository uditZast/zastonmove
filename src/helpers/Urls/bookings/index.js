export const getBookingsHistoryURL = () => {
  return "/bookings-BookingHistory";
};

export const getMyBookingsURL = () => {
  return "/my-bookings";
};

export const getCreateBookingDetailsURL = () => {
  return "/create-booking-details-new";
};

export const createBookingURL = () => {
  return "/create-booking";
};

export const editBookingURL = () => {
  return "/edit-booking";
};

export const acceptBookingURL = () => {
  return "/accept-booking";
};

export const fetchExpectedBookingURL = () => {
  return "/autofill-tat-distance";
};

export const assignVehicleURL = () => {
  return "/assign-booking";
};

export const rejectBookingURL = () => {
  return "/reject-booking";
};

export const dockBookingURL = () => {
  return "/dock-booking";
};

export const fetchBookingHistoryURL = () => {
  return "/history-comments";
};

export const searchURL = () => {
  return "/search";
};

export const fetchBookingData = () => {
  return "/get-booking-data";
};
