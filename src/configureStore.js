import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { persistStore, persistReducer } from "redux-persist";
import AsyncStorage from "@react-native-community/async-storage";
//import storage from "redux-persist/lib/storage"; // defaults to localStorage for web and AsyncStorage for react-native
import rootReducer from "./modules/rootReducer";
import { composeWithDevTools } from "redux-devtools-extension";

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  blacklist: ["screens", "network", "tracking"]
};

const noop = () => {};

const persistedReducer = persistReducer(persistConfig, rootReducer);

class StoreHelper {
  constructor() {
    this.store = null;
    this.persistStore = null;
  }

  configureStore = (cb = noop) => {
    const enhancer = compose(composeWithDevTools(applyMiddleware(thunk)));
    const store = createStore(persistedReducer, enhancer);
    const persistor = persistStore(store, {}, () => {
      const state = store.getState();
      cb(state);
    });

    this.store = store;
    this.persistStore = persistor;

    return { store, persistor };
  };

  purge = async () => {
    await this.persistStore.purge();
  };
}

const Store = new StoreHelper();

export default Store;
