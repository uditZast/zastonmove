import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import {
  requestTripAdvanceUrl,
  saveAdvanceTripUrl,
  saveTripChargesUrl,
  sendOtpToVendorUrl,
  verifyOtpUrl
} from "../../helpers/Urls/tripAdvanceUrl";

const REQUEST_TRIP_ADVANCE = "REQUEST_TRIP_ADVANCE";
const REQUEST_TRIP_ADVANCE_COMPLETED = "REQUEST_TRIP_ADVANCE_COMPLETED";
const REQUEST_TRIP_ADVANCE_FAILED = "REQUEST_TRIP_ADVANCE_FAILED";

const ADD_TRIPS_CHARGES = "ADD_TRIPS_CHARGES";
const ADD_TRIPS_CHARGES_COMPLETED = "ADD_TRIPS_CHARGES_COMPLETED";
const ADD_TRIPS_CHARGES_FAILED = "ADD_TRIPS_CHARGES_FAILED";

export const SUBMIT_TRIP_ADVANCE = "SUBMIT_TRIP_ADVANCE";
export const SUBMIT_TRIP_ADVANCE_COMPLETED = "SUBMIT_TRIP_ADVANCE_COMPLETED";
export const SUBMIT_TRIP_ADVANCE_FAILED = "SUBMIT_TRIP_ADVANCE_FAILED";

const SEND_OTP_TO_VENDOR = "SEND_OTP_TO_VENDOR";
const SEND_OTP_TO_VENDOR_COMPLETED = "SEND_OTP_TO_VENDOR_COMPLETED";
const SEND_OTP_TO_VENDOR_FAILED = "SEND_OTP_TO_VENDOR_FAILED";

const VERIFY_OTP_TRIP_ADVANCE = "VERIFY_OTP_TRIP_ADVANCE";
const VERIFY_OTP_TRIP_ADVANCE_COMPLETED = "VERIFY_OTP_TRIP_ADVANCE_COMPLETED";
const VERIFY_OTP_TRIP_ADVANCE_FAILED = "VERIFY_OTP_TRIP_ADVANCE_COMPLETED";

export const requestTripAdvance = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: REQUEST_TRIP_ADVANCE });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: requestTripAdvanceUrl(),
        isMAPI: true,
        data: requestData
      });
      console.log("response --REQUEST_TRIP_ADVANCE--", response);
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: REQUEST_TRIP_ADVANCE_COMPLETED,
          payload: data
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: REQUEST_TRIP_ADVANCE_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: REQUEST_TRIP_ADVANCE_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const addTripCharges = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: ADD_TRIPS_CHARGES });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: saveTripChargesUrl(),
        isMAPI: true,
        data: requestData
      });
      console.log("response --ADD_TRIPS_CHARGES--", response);
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: ADD_TRIPS_CHARGES_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: ADD_TRIPS_CHARGES_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: ADD_TRIPS_CHARGES_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const submitTripAdvance = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: SUBMIT_TRIP_ADVANCE });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: saveAdvanceTripUrl(),
        isMAPI: true,
        data: requestData
      });

      console.log("response --------SUBMIT TRIP ADVANCE----", response);
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: SUBMIT_TRIP_ADVANCE_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: SUBMIT_TRIP_ADVANCE_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: SUBMIT_TRIP_ADVANCE_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const sendOtpToVendor = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: SEND_OTP_TO_VENDOR });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: sendOtpToVendorUrl(),
        isMAPI: true,
        data: requestData
      });
      console.log("response --REQUEST_TRIP_ADVANCE--", response);
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: SEND_OTP_TO_VENDOR_COMPLETED,
          payload: data
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: SEND_OTP_TO_VENDOR_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: SEND_OTP_TO_VENDOR_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const verifyOtpAdvance = requestData => {
  console.log("requestData --------------", requestData);
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: VERIFY_OTP_TRIP_ADVANCE });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: verifyOtpUrl(),
        isMAPI: true,
        data: requestData
      });
      console.log("response --VEERIFY OTP--", response);
      const { data, status } = response || {};

      console.log("data ------------------", data);
      console.log("status --------------------", status);
      if (status === true) {
        console.log("---- status if ----");
        dispatch({
          type: VERIFY_OTP_TRIP_ADVANCE_COMPLETED,
          payload: data
        });
      } else if (status === false) {
        console.log("---- status false else if  ----");
        const { message = "Error" } = response || {};
        dispatch({
          type: VERIFY_OTP_TRIP_ADVANCE_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      console.log("err -------CATCH----", err);
      dispatch({
        type: VERIFY_OTP_TRIP_ADVANCE_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = {}, action) => {
  const { type, payload } = action || {};

  switch (type) {
    case REQUEST_TRIP_ADVANCE:
      return {
        ...state
      };
    case REQUEST_TRIP_ADVANCE_COMPLETED:
      return {
        ...state,
        ...payload
      };
    case REQUEST_TRIP_ADVANCE_FAILED:
      return {
        ...state,
        error: payload.message
      };
    case ADD_TRIPS_CHARGES:
      return {
        ...state,
        error: ""
      };
    case ADD_TRIPS_CHARGES_COMPLETED:
      return {
        ...state,
        ...payload
      };
    case ADD_TRIPS_CHARGES_FAILED:
      return {
        ...state,
        error: payload.message
      };
    case SUBMIT_TRIP_ADVANCE:
      return {
        ...state,
        error: ""
      };
    case SUBMIT_TRIP_ADVANCE_COMPLETED:
      return {
        ...state,
        ...payload
      };
    case SUBMIT_TRIP_ADVANCE_FAILED:
      return {
        ...state,
        error: payload.message
      };
    case SEND_OTP_TO_VENDOR:
      return {
        ...state,
        error: ""
      };
    case SEND_OTP_TO_VENDOR_COMPLETED:
      return {
        ...state,
        ...payload
      };
    case SEND_OTP_TO_VENDOR_FAILED:
      return {
        ...state,
        error: payload.message
      };
    case VERIFY_OTP_TRIP_ADVANCE:
      return {
        ...state,
        error: ""
      };
    case VERIFY_OTP_TRIP_ADVANCE_COMPLETED:
      return {
        ...state,
        ...payload
      };
    case VERIFY_OTP_TRIP_ADVANCE_FAILED:
      return {
        ...state,
        error: payload.message
      };

    default:
      return state;
  }
};
