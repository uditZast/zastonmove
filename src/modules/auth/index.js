import { Auth, Vehicles } from "../../helpers/Urls";
import { NAVIGATE, REQUEST_TYPE } from "../../../constants";
import DeviceInfo from "../../Deviceinfo";
import NavigationService from "../../NavigationService";
import { doRequest } from "../../helpers/network";

export const SIGNING = "SIGNING";
export const SIGNING_COMPLETED = "SIGNING_COMPLETED";
export const SIGNING_COMPLETED_WITH_ERROR = "SIGNING_COMPLETED_WITH_ERROR";

export const RESET_STORE = "RESET_STORE";

const FETCH_INITIAL_DATA = "FETCH_INITIAL_DATA";
const FETCH_INITIAL_DATA_COMPLETED = "FETCH_INITIAL_DATA_COMPLETED";
const FETCH_INITIAL_DATA_FAILED = "FETCH_INITIAL_DATA_FAILED";

const VERIFY_OTP = "VERIFY_OTP";
const VERIFY_OTP_COMPLETED = "VERIFY_OTP_COMPLETED";
const VERIFY_OTP_FAILED = "VERIFY_OTP_FAILED";

const SEND_OTP = "SEND_OTP";
const SEND_OTP_COMPLETED = "SEND_OTP_COMPLETED";
const SEND_OTP_FAILED = "SEND_OTP_FAILED";

const initial_state = {
  authenticated: false,
  loading: false,
  isError: false,
  error: "",
  accessToken: "",
  authenticatedUser: null,
  role: "",
  rights: []
};

const onSignInComplete = (state, payload) => {
  const { token: accessToken, userId, users } = payload;
  console.log("payload --AUTH--", payload);
  const loggedInUser = users[userId] || {};
  const { role, rights } = loggedInUser;
  DeviceInfo.setInfo({
    accessToken
  });
  return {
    ...state,
    loading: false,
    authenticated: true,
    isError: false,
    error: "",
    accessToken: accessToken,
    authenticatedUser: userId,
    role,
    rights
  };
};

const onSignInFailed = (state, payload) => {
  const { error = "" } = payload;
  return {
    ...state,
    authenticated: false,
    authenticatedUser: false,
    isError: true,
    error: error,
    loading: false,
    role: "",
    rights: []
  };
};

export const signIn = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: SIGNING });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        data: requestData,
        url: Auth.getSignInURL(),
        isMAPI: true
      });

      const { data, status = false, message = "" } = response;
      if (status === false) {
        dispatch({
          type: SIGNING_COMPLETED_WITH_ERROR,
          payload: { error: message }
        });
      } else if (status === true) {
        dispatch({
          type: SIGNING_COMPLETED,
          payload: data
        });
      }
    } catch (err) {
      dispatch({
        type: SIGNING_COMPLETED_WITH_ERROR,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const signOut = () => {
  return async dispatch => {
    try {
      dispatch({ type: RESET_STORE });
      // NavigationService.navigate(NAVIGATE.SIGN_IN);
    } catch (err) {
      throw err;
    }
  };
};

export const fetchInitialData = () => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: FETCH_INITIAL_DATA });
      response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: Auth.fetchInitialDataURL(),
        isMAPI: true
      });
      console.log("response -FETCH_INITIAL_DATA--", response);
      const { data, status = false } = response || {};
      // const { data, error = true, message } = responseData;

      if (status === true) {
        dispatch({
          type: FETCH_INITIAL_DATA_COMPLETED,
          payload: data
        });
      } else if (status === false) {
        const { message = "" } = response || {};
        dispatch({
          type: FETCH_INITIAL_DATA_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: FETCH_INITIAL_DATA_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const verifyOtp = otpData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: VERIFY_OTP });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Auth.verifyOtpURL(),
        data: otpData,
        isMAPI: true
      });
      console.log("response --INITIAL_DATA--", response);
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: VERIFY_OTP_COMPLETED,
          payload: data
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: VERIFY_OTP_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: VERIFY_OTP_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const sendOtp = otpData => {
  return async dispatch => {
    let response = {};
    try {
      console.log("otpData ----", otpData);
      dispatch({ type: SEND_OTP });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Auth.sendOtpURL(),
        data: otpData,
        isMAPI: true
      });
      console.log("response --RESEND OTP--", response);
      const { data = {} } = response || {};

      if (status === true) {
        dispatch({
          type: SEND_OTP_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: SEND_OTP_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: SEND_OTP_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = initial_state, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    case SIGNING:
      return {
        ...state,
        loading: true,
        isError: false
      };
    case SIGNING_COMPLETED:
    case VERIFY_OTP_COMPLETED:
      return onSignInComplete(state, payload);
    case SIGNING_COMPLETED_WITH_ERROR:
      return onSignInFailed(state, payload);
    default: {
      return state;
    }
  }
};
