import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { fetchVehicleEditURL } from "../../helpers/Urls/vendorEdit";

const EDIT_VEHICLE_DETAILS = "EDIT_VEHICLE_DETAILS";
const EDIT_VEHICLE_DETAILS_COMPLETED = "EDIT_VEHICLE_DETAILS_COMPLETED";
const EDIT_VEHICLE_DETAILS_FAILED = "EDIT_VEHICLE_DETAILS_FAILED";

export const editVehicleDetails = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: EDIT_VEHICLE_DETAILS });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: fetchVehicleEditURL(),
        isMAPI: true,
        data: requestData
      });
      console.log("response --EDIT_VEHICLE_DETAILS--", response);
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: EDIT_VEHICLE_DETAILS_COMPLETED,
          payload: data
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: EDIT_VEHICLE_DETAILS_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: EDIT_VEHICLE_DETAILS_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};
