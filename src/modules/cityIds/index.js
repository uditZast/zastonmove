export default (state = [], action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.city_ids) {
        return payload.city_ids;
      }
      return state;
    }
  }
};
