import NetInfo from "@react-native-community/netinfo";

export const CHANGE_NETWORK_STATE = "CHANGE_NETWORK_STATE";

const initial_state = {
  isConnected: true
};

const changeNetworkStatus = payload => ({
  type: CHANGE_NETWORK_STATE,
  payload
});

export const subscribeToNetworkChange = () => {
  return (dispatch, state) => {
    NetInfo.fetch().then(state => {
      dispatch(changeNetworkStatus(state));

      NetInfo.addEventListener(state => {
        dispatch(changeNetworkStatus(state));
      });
    });
  };
};

export default (state = initial_state, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    case CHANGE_NETWORK_STATE:
      return { ...state, ...payload };

    default: {
      return state;
    }
  }
};
