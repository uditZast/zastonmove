import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { Bookings } from "../../helpers/Urls";

export const REJECT_BOOKING = "REJECT_BOOKING";
export const REJECT_BOOKING_COMPLETED = "REJECT_BOOKING_COMPLETED";
export const REJECT_BOOKING_FAILED = "REJECT_BOOKING_FAILED";

export const rejectBooking = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: REJECT_BOOKING });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Bookings.rejectBookingURL(),
        isMAPI: true,
        data: requestData
      });
      const { data, status } = response || {};

      if (status === true) {
        const { booking_id } = requestData;

        dispatch({
          type: REJECT_BOOKING_COMPLETED,
          payload: { ...data, booking_id }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: REJECT_BOOKING_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: REJECT_BOOKING_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.rejection_reasons) {
        return { ...state, ...payload.rejection_reasons };
      }
      return state;
    }
  }
};
