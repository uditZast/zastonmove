export default (state = [], action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.customer_ids) {
        return payload.customer_ids;
      }
      return state;
    }
  }
};
