export default (state = [], action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.vehicle_ids) {
        return payload.vehicle_ids;
      }
      return state;
    }
  }
};
