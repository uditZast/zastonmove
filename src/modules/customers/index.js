import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { Bookings } from "../../helpers/Urls";

export const FETCHING_CREATE_BOOKING_DETAILS =
  "FETCHING_CREATE_BOOKING_DETAILS";
export const FETCHING_CREATE_BOOKING_DETAILS_COMPLETED =
  "FETCHING_CREATE_BOOKING_DETAILS_COMPLETED";
export const FETCHING_CREATE_BOOKING_DETAILS_COMPLETED_WITH_ERROR =
  "FETCHING_CREATE_BOOKING_DETAILS_COMPLETED_WITH_ERROR";

export const fetchCreateBookingDetails = token => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: FETCHING_CREATE_BOOKING_DETAILS });
      response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: Bookings.getCreateBookingDetailsURL()
      });
      const { data: responseData = {} } = response || {};
      const { data, error = true, message } = responseData;

      if (error === false) {
        dispatch({
          type: FETCHING_CREATE_BOOKING_DETAILS_COMPLETED,
          payload: { ...data }
        });
      } else if (error === true) {
        dispatch({
          type: FETCHING_CREATE_BOOKING_DETAILS_COMPLETED_WITH_ERROR,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: FETCHING_CREATE_BOOKING_DETAILS_COMPLETED_WITH_ERROR,
        payload: { error: err.message }
      });
      //throw err;
    }
    return response;
  };
};

export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.customers) {
        return { ...state, ...payload.customers };
      }
      return state;
    }
  }
};
