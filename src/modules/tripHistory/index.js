export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.trip_history) {
        return { ...state, ...payload.trip_history };
      }
      return state;
    }
  }
};
