import { combineReducers } from "redux";
import auth from "./auth";
import bookings from "./bookings";
import customers from "./customers";
import customerIds from "./customerIds";
import vehicle_types from "./vehicle_types";
import tat_dict from "./tat_dict";
import rejectReasons from "./rejectBooking";
import vehicles from "./vehicles";
import vehicleIds from "./vehicleIds";
import vendors from "./vendors";
import cities from "./cities";
import cityIds from "./cityIds";
import haltReasons from "./haltReasons";
import delayReasons from "./delayReasons";
import vehicleChangeReasons from "./vehicleChangeReasons";
import engagedBy from "./engagedBy";
import users from "./users";
import network from "./network";
import screens from "./screens";
import vehicleTypes from "./vehicleTypes";
import tripComments from "./tripComments";
import tripHistory from "./tripHistory";
import tripRunningStatus from "./tripRunningStatus";
import versions from "./versions";
import tracking from "./tracking";
import bookingHistory from "./bookingHistory";
import trips from "./trips";
import Store from "../configureStore";
import DeviceInfo from "../Deviceinfo";
import downloadReport from "./sendDownloadReport";
import vendorIds from "./vendorIds";
import requestTripAdvance from "./requestTripAdvance";
import tripCharges from "./tripCharges";

const appReducer = combineReducers({
  customers,
  customerIds,
  rejectReasons,
  vehicles,
  vehicleIds,
  cities,
  cityIds,
  vendors,
  haltReasons,
  delayReasons,
  vehicleChangeReasons,
  engagedBy,
  vehicle_types,
  tat_dict,
  auth,
  bookings,
  users,
  network,
  vehicleTypes,
  tripComments,
  tripHistory,
  tripRunningStatus,
  screens,
  bookingHistory,
  versions,
  trips,
  downloadReport,
  vendorIds,
  requestTripAdvance,
  tracking
});

const rootReducer = (state, action) => {
  if (action.type === "RESET_STORE") {
    state = {};
    Store.purge();
    DeviceInfo.reset();
  }
  if (action.type === "persist/REHYDRATE") {
    const { payload: { auth: { accessToken = null } = {} } = {} } = action;
    if (accessToken) {
      DeviceInfo.setInfo({ accessToken });
    }
  }
  return appReducer(state, action);
};

export default rootReducer;
