import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { saveTripChargesUrl } from "../../helpers/Urls/tripAdvanceUrl";

const ADD_TRIPS_CHARGES = "ADD_TRIPS_CHARGES";
const ADD_TRIPS_CHARGES_COMPLETED = "ADD_TRIPS_CHARGES_COMPLETED";
const ADD_TRIPS_CHARGES_FAILED = "ADD_TRIPS_CHARGES_FAILED";

export const addTripCharges = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: ADD_TRIPS_CHARGES });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: saveTripChargesUrl(),
        isMAPI: true,
        data: requestData
      });
      console.log("response --ADD_TRIPS_CHARGES--", response);
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: ADD_TRIPS_CHARGES_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: ADD_TRIPS_CHARGES_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: ADD_TRIPS_CHARGES_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = {}, action) => {
  const { type, payload } = action || {};

  console.log("payload -----TRIP CHARGES-----------------", payload);
  switch (type) {
    case ADD_TRIPS_CHARGES:
      return {
        ...state,
        isFetching: true,
        hasError: false,
        error: ""
      };
    case ADD_TRIPS_CHARGES_COMPLETED:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        requestTripAdvance: { ...payload }
      };
    case ADD_TRIPS_CHARGES_FAILED:
      return {
        ...state,
        isFetching: false,
        hasError: true,
        error: payload.message
      };

    default:
      return state;
  }
};
