export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.trip_halt_reasons) {
        return { ...state, ...payload.trip_halt_reasons };
      }
      return state;
    }
  }
};
