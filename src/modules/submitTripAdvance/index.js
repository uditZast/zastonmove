import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { saveAdvanceTripUrl } from "../../helpers/Urls/tripAdvanceUrl";

export const SUBMIT_TRIP_ADVANCE = "SUBMIT_TRIP_ADVANCE";
export const SUBMIT_TRIP_ADVANCE_COMPLETED = "SUBMIT_TRIP_ADVANCE_COMPLETED";
export const SUBMIT_TRIP_ADVANCE_FAILED = "SUBMIT_TRIP_ADVANCE_FAILED";

export const submitTripAdvance = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: SUBMIT_TRIP_ADVANCE });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: saveAdvanceTripUrl(),
        isMAPI: true,
        data: requestData
      });

      console.log("response --------SUBMIT TRIP ADVANCE----", response);
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: SUBMIT_TRIP_ADVANCE_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: SUBMIT_TRIP_ADVANCE_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: SUBMIT_TRIP_ADVANCE_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;

  switch (type) {
    default: {
      if (payload.data) {
        return { ...state, ...payload.data };
      }
      return state;
    }
  }
};
