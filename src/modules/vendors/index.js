export default (state = {}, action = {}) => {
    const {type, payload = {}} = action;
    switch (type) {
        default: {
            if (payload.vendors) {
                return {...state, ...payload.vendors};
            }
            return state;
        }
    }
};