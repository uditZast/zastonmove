import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { Trips } from "../../helpers/Urls";

export const MARK_TRIP_AS_BEGIN = "MARK_TRIP_AS_BEGIN";
export const MARK_TRIP_AS_BEGIN_COMPLETED = "MARK_TRIP_AS_BEGIN_COMPLETED";
export const MARK_TRIP_AS_BEGIN_FAILED = "MARK_TRIP_AS_BEGIN_FAILED";

export const MARK_TRIP_AS_CLOSED = "MARK_TRIP_AS_CLOSED";
export const MARK_TRIP_AS_CLOSED_COMPLETED = "MARK_TRIP_AS_CLOSED_COMPLETED";
export const MARK_TRIP_AS_CLOSED_FAILED = "MARK_TRIP_AS_CLOSED_FAILED";

export const MARK_TRIP_AS_ARRIVED = "MARK_TRIP_AS_ARRIVED";
export const MARK_TRIP_AS_ARRIVED_COMPLETED = "MARK_TRIP_AS_ARRIVED_COMPLETED";
export const MARK_TRIP_AS_ARRIVED_FAILED = "MARK_TRIP_AS_ARRIVED_FAILED";

const MARK_TRIP_AS_TOUCHING_IN = "MARK_TRIP_AS_TOUCHING_IN";
const MARK_TRIP_AS_TOUCHING_IN_COMPLETED = "MARK_TRIP_AS_TOUCHING_IN_COMPLETED";
const MARK_TRIP_AS_TOUCHING_IN_FAILED = "MARK_TRIP_AS_TOUCHING_IN_FAILED";

const MARK_TRIP_AS_HALTED = "MARK_TRIP_AS_HALTED";
const MARK_TRIP_AS_HALTED_COMPLETED = "MARK_TRIP_AS_HALTED_COMPLETED";
const MARK_TRIP_AS_HALTED_FAILED = "MARK_TRIP_AS_HALTED_FAILED";

export const MARK_TRIP_AS_TERMINATE = "MARK_TRIP_AS_TERMINATE";
export const MARK_TRIP_AS_TERMINATE_COMPLETED =
  "MARK_TRIP_AS_TERMINATE_COMPLETED";
export const MARK_TRIP_AS_TERMINATE_FAILED = "MARK_TRIP_AS_TERMINATE_FAILED";

const EDIT_TRIP = "EDIT_TRIP";
const EDIT_TRIP_COMPLETED = "EDIT_TRIP_COMPLETED";
const EDIT_TRIP_FAILED = "EDIT_TRIP_FAILED";

const EDIT_TRIP_ROUTE = "EDIT_TRIP_ROUTE";
const EDIT_TRIP_ROUTE_COMPLETED = "EDIT_TRIP_ROUTE_COMPLETED";
const EDIT_TRIP_ROUTE_FAILED = "EDIT_TRIP_ROUTE_FAILED";

const EDIT_TRIP_CUSTOMER = "EDIT_TRIP_CUSTOMER";
const EDIT_TRIP_CUSTOMER_COMPLETED = "EDIT_TRIP_CUSTOMER_COMPLETED";
const EDIT_TRIP_CUSTOMER_FAILED = "EDIT_TRIP_CUSTOMER_FAILED";

const ADDING_TRIP_COMMENT = "ADDING_TRIP_COMMENT";
const ADDING_TRIP_COMMENT_COMPLETED = "ADDING_TRIP_COMMENT_COMPLETED";
const ADDING_TRIP_COMMENT_FAILED = "ADDING_TRIP_COMMENT_FAILED";

const FETCH_TRIP_DETAILS = "FETCH_TRIP_DETAILS";
const FETCH_TRIP_DETAILS_COMPLETED = "FETCH_TRIP_DETAILS_COMPLETED";
const FETCH_TRIP_DETAILS_FAILED = "FETCH_TRIP_DETAILS_FAILED";

const UPDATE_DRIVER_PHONE = "UPDATE_DRIVER_PHONE";
const UPDATE_DRIVER_PHONE_COMPLETED = "UPDATE_DRIVER_PHONE_COMPLETED";
const UPDATE_DRIVER_PHONE_FAILED = "UPDATE_DRIVER_PHONE_FAILED";

const UPDATE_REFERENCE = "UPDATE_REFERENCE";
const UPDATE_REFERENCE_COMPLETED = "UPDATE_REFERENCE_COMPLETED";
const UPDATE_REFERENCE_FAILED = "UPDATE_REFERENCE_FAILED";

const UPDATE_TAT = "UPDATE_TAT";
const UPDATE_TAT_COMPLETED = "UPDATE_TAT_COMPLETED";
const UPDATE_TAT_FAILED = "UPDATE_TAT_FAILED";

const FETCH_TRIP_PLOT_ROUTE = "FETCH_TRIP_PLOT_ROUTE";
const FETCH_TRIP_PLOT_ROUTE_COMPLETED = "FETCH_TRIP_PLOT_ROUTE_COMPLETED";
const FETCH_TRIP_PLOT_ROUTE_FAILED = "FETCH_TRIP_PLOT_ROUTE_FAILED";

const CROSS_DOCK_TRIP = "CROSS_DOCK_TRIP";
const CROSS_DOCK_TRIP_COMPLETED = "CROSS_DOCK_TRIP_COMPLETED";
const CROSS_DOCK_TRIP_FAILED = "CROSS_DOCK_TRIP_FAILED";

//{trip_id, date_time}
export const markTripAsBegin = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: MARK_TRIP_AS_BEGIN });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.markTripAsBeginURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --MARK_TRIP_AS_BEGIN--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: MARK_TRIP_AS_BEGIN_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: MARK_TRIP_AS_BEGIN_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: MARK_TRIP_AS_BEGIN_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const markTripAsClosed = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: MARK_TRIP_AS_CLOSED });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.markTripAsCloseURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --MARK_TRIP_AS_CLOSE--", response);
      const { data, status } = response || {};
      if (status === true) {
        const { trip_id } = requestData;
        dispatch({
          type: MARK_TRIP_AS_CLOSED_COMPLETED,
          payload: { ...data, trip_id }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: MARK_TRIP_AS_CLOSED_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: MARK_TRIP_AS_CLOSED_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const markTripAsTouchingIn = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: MARK_TRIP_AS_TOUCHING_IN });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.markTripAsTouchingInURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --MARK_TRIP_AS_TOUCHING_IN--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: MARK_TRIP_AS_TOUCHING_IN_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: MARK_TRIP_AS_TOUCHING_IN_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: MARK_TRIP_AS_TOUCHING_IN_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

//trip_id, date_time, reason=id, comment = "abc"
export const markTripAsHalted = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: MARK_TRIP_AS_HALTED });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.markTripAsHaltedURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --MARK_TRIP_AS_HALTED--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: MARK_TRIP_AS_HALTED_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: MARK_TRIP_AS_HALTED_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: MARK_TRIP_AS_HALTED_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const markTripAsArrived = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: MARK_TRIP_AS_ARRIVED });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.markTripAsArrivedURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --MARK_TRIP_AS_ARRIVED--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: MARK_TRIP_AS_ARRIVED_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: MARK_TRIP_AS_ARRIVED_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: MARK_TRIP_AS_ARRIVED_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const markTripAsTerminate = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: MARK_TRIP_AS_TERMINATE });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.markTripAsTerminateURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --MARK_TRIP_AS_TERMINATE--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: MARK_TRIP_AS_TERMINATE_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: MARK_TRIP_AS_TERMINATE_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: MARK_TRIP_AS_TERMINATE_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

//requestData=vendor_id, vehicle_id, phone, engaged_by_id
export const editTrip = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: EDIT_TRIP });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.editTripURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --EDIT_TRIP--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: EDIT_TRIP_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: EDIT_TRIP_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: EDIT_TRIP_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const editTripRoute = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: EDIT_TRIP_ROUTE });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.editTripRouteURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --EDIT_TRIP_ROUTE--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: EDIT_TRIP_ROUTE_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: EDIT_TRIP_ROUTE_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: EDIT_TRIP_ROUTE_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const editTripCustomer = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: EDIT_TRIP_CUSTOMER });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.editTripCustomerURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --EDIT_TRIP_CUSTOMER--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: EDIT_TRIP_CUSTOMER_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: EDIT_TRIP_CUSTOMER_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: EDIT_TRIP_CUSTOMER_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

//requestData = trip_id, entity="comments/history/running_status"
export const fetchTripDetails = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: FETCH_TRIP_DETAILS });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.fetchTripDetailsURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --ADDING_TRIP_COMMENT--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: FETCH_TRIP_DETAILS_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: FETCH_TRIP_DETAILS_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: FETCH_TRIP_DETAILS_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

//requestData={ trip_id, comment: "1123"}
export const addTripComment = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: ADDING_TRIP_COMMENT });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.addTripCommentsURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --ADDING_TRIP_COMMENT--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: ADDING_TRIP_COMMENT_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: ADDING_TRIP_COMMENT_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: ADDING_TRIP_COMMENT_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

//trip_id, phone
export const updateDriverPhone = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: UPDATE_DRIVER_PHONE });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.updateDriverPhoneURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --UPDATE_DRIVER_PHONE--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: UPDATE_DRIVER_PHONE_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: UPDATE_DRIVER_PHONE_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: UPDATE_DRIVER_PHONE_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

//trip_id, reference
export const updateReference = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: UPDATE_REFERENCE });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.updateReferenceURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --UPDATE_REFERENCE--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: UPDATE_REFERENCE_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: UPDATE_REFERENCE_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: UPDATE_REFERENCE_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

//trip_id, tat
export const updateTripTat = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: UPDATE_TAT });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.updateTripTATURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --UPDATE_TAT--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: UPDATE_TAT_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: UPDATE_TAT_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: UPDATE_TAT_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

//trip_id, tat
export const plotTripRoute = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: FETCH_TRIP_PLOT_ROUTE });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.plotRouteURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --UPDATE_TAT--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: FETCH_TRIP_PLOT_ROUTE_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: FETCH_TRIP_PLOT_ROUTE_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: FETCH_TRIP_PLOT_ROUTE_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

//requestData = trip_id, vehicle_id, vendor_id, engaged_by_id, lat, long, location,
//crossdock_city_id, reason (vehicleChangeReason - value), crossdock_time, distance, phone
export const crossDock = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: CROSS_DOCK_TRIP });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.crossDockTripURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --CROSS_DOCK_TRIP--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: CROSS_DOCK_TRIP_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: CROSS_DOCK_TRIP_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: CROSS_DOCK_TRIP_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.trips) {
        // return Object.assign({}, state, {
        //   ...payload.trips
        // });
        return { ...state, ...payload.trips };
      }
      return state;
    }
  }
};
