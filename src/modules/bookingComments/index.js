import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { Bookings } from "../../helpers/Urls";

export const FETCHING_BOOKING_COMMENTS_BY_ID =
  "FETCHING_BOOKING_COMMENTS_BY_ID";
export const FETCHING_BOOKING_COMMENTS_BY_ID_COMPLETED =
  "FETCHING_BOOKING_COMMENTS_BY_ID_COMPLETED";
export const FETCHING_BOOKING_COMMENTS_BY_ID_COMPLETED_WITH_ERROR =
  "FETCHING_BOOKING_COMMENTS_BY_ID_COMPLETED_WITH_ERROR";

export const fetchBookingCommentsById = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: FETCHING_BOOKING_COMMENTS_BY_ID });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Bookings.fetchBookingHistoryURL(),
        isMAPI: true,
        data: requestData
      });
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: FETCHING_BOOKING_COMMENTS_BY_ID_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        dispatch({
          type: FETCHING_BOOKING_COMMENTS_BY_ID_COMPLETED_WITH_ERROR,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: FETCHING_BOOKING_COMMENTS_BY_ID_COMPLETED_WITH_ERROR,
        payload: { error: err.message }
      });
      //throw err;
    }
    return response;
  };
};

export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;

  switch (type) {
    default: {
      if (payload.comments) {
        return { ...state, ...payload.comments };
      }
      return state;
    }
  }
};
