export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.vehicle_types) {
        return { ...state, ...payload.vehicle_types };
      }
      return state;
    }
  }
};
