import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { Vehicles } from "../../helpers/Urls";

const INITIAL_STATE = {};

const FETCH_VERSION_CHECK = "FETCH_VERSION_CHECK";
const FETCH_VERSION_CHECK_COMPLETED = "FETCH_VERSION_CHECK_COMPLETED";
const FETCH_VERSION_CHECK_FAILED = "FETCH_VERSION_CHECK_FAILED";

export const fetchVersionCheck = () => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: FETCH_VERSION_CHECK });
      response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: Vehicles.fetchVersionCheckURL(),
        isMAPI: true
      });
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: FETCH_VERSION_CHECK_COMPLETED,
          payload: data
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: FETCH_VERSION_CHECK_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: FETCH_VERSION_CHECK_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action || {};
  switch (type) {
    case FETCH_VERSION_CHECK:
      return {
        ...state,
        isFetching: true,
        hasError: false,
        error: ""
      };
    case FETCH_VERSION_CHECK_COMPLETED:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        data: payload
      };
    case FETCH_VERSION_CHECK_FAILED:
      return {
        ...state,
        isFetching: false,
        hasError: true,
        error: payload.message
      };
    default:
      return state;
  }
};
