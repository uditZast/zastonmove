import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { Bookings } from "../../helpers/Urls";

export const ASSIGN_VEHICLE = "ASSIGN_VEHICLE";
export const ASSIGN_VEHICLE_COMPLETED = "ASSIGN_VEHICLE_COMPLETED";
export const ASSIGN_VEHICLE_FAILED = "ASSIGN_VEHICLE_FAILED";

export const assignVehicle = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: ASSIGN_VEHICLE });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Bookings.assignVehicleURL(),
        isMAPI: true,
        data: requestData
      });
      const { data, status } = response || {};

      if (status === true) {
        const { booking_id } = requestData;
        dispatch({
          type: ASSIGN_VEHICLE_COMPLETED,
          payload: { ...data, booking_id }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: ASSIGN_VEHICLE_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: ASSIGN_VEHICLE_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;

  switch (type) {
    default: {
      if (payload.data) {
        return { ...state, ...payload.data };
      }
      return state;
    }
  }
};
