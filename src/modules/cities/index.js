export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.cities) {
        return { ...state, ...payload.cities };
      }
      return state;
    }
  }
};
