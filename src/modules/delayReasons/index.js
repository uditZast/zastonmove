export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.delay_reasons) {
        return { ...state, ...payload.delay_reasons };
      }
      return state;
    }
  }
};
