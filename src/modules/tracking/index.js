export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.tracking) {
        return { ...state, ...payload.tracking };
      }
      return state;
    }
  }
};
