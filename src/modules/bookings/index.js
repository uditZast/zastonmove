import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { Bookings } from "../../helpers/Urls";

const FETCH_MY_BOOKINGS = "FETCH_MY_BOOKINGS";
const FETCH_MY_BOOKINGS_COMPLETED = "FETCH_MY_BOOKINGS_COMPLETED";
const FETCH_MY_BOOKINGS_FAILED = "FETCH_MY_BOOKINGS_FAILED";

const CREATE_BOOKING = "CREATE_BOOKING";
export const CREATE_BOOKING_COMPLETED = "CREATE_BOOKING_COMPLETED";
const CREATE_BOOKING_FAILED = "CREATE_BOOKING_FAILED";

const EDIT_BOOKING = "EDIT_BOOKING";
const EDIT_BOOKING_COMPLETED = "EDIT_BOOKING_COMPLETED";
const EDIT_BOOKING_FAILED = "EDIT_BOOKING_FAILED";

export const ACCEPT_BOOKING = "ACCEPT_BOOKING";
export const ACCEPT_BOOKING_COMPLETED = "ACCEPT_BOOKING_COMPLETED";
export const ACCEPT_BOOKING_FAILED = "ACCEPT_BOOKING_FAILED";

const FETCH_BOOKING_BY_ID = "FETCH_BOOKING_BY_ID";
const FETCH_BOOKING_BY_ID_COMPLETED = "FETCH_BOOKING_BY_ID_COMPLETED";
const FETCH_BOOKING_BY_ID_FAILED = "FETCH_BOOKING_BY_ID_FAILED";

export const createBooking = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: CREATE_BOOKING });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Bookings.createBookingURL(),
        isMAPI: true,
        data: requestData
      });
      console.clear();
      console.log("response --CREATE_BOOKING--", response);
      const { data, status } = response || {};

      if (status === true) {
        const { bookings } = data;
        console.log("bookings ----", bookings);
        const booking_id = Object.keys(bookings)[0];
        console.log("booking_id --IN BOOKINGS--", booking_id);
        dispatch({
          type: CREATE_BOOKING_COMPLETED,
          payload: { ...data, booking_id }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: CREATE_BOOKING_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: CREATE_BOOKING_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const editBooking = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: EDIT_BOOKING });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Bookings.editBookingURL(),
        isMAPI: true,
        data: requestData
      });
      console.log("response --EDIT_BOOKING--", response);
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: EDIT_BOOKING_COMPLETED,
          payload: data
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: EDIT_BOOKING_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: EDIT_BOOKING_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const acceptBooking = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: ACCEPT_BOOKING });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Bookings.acceptBookingURL(),
        isMAPI: true,
        data: requestData
      });
      console.log("response --ACCEPT BOOKING--", response);
      const { data, status } = response || {};

      if (status === true) {
        const {
          request_data: { forward: { booking_id } = {} } = {}
        } = requestData;

        const { data = {} } = response;
        const { bookings } = data;
        const { basicInfo: { type } = {}, status } = bookings[booking_id];

        console.log("status ----------------", status);
        dispatch({
          type: ACCEPT_BOOKING_COMPLETED,
          payload: { ...data, booking_id, type, status }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: ACCEPT_BOOKING_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: ACCEPT_BOOKING_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export const fetchMyBookings = () => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: FETCH_MY_BOOKINGS });
      response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: Bookings.getMyBookingsURL()
      });
      const { status, payload = {} } = response;
      if (status === true) {
        dispatch({
          type: FETCH_MY_BOOKINGS_COMPLETED,
          payload: payload.data
        });
      }
    } catch (err) {
      dispatch({ type: FETCH_MY_BOOKINGS_FAILED });
    }
    return response;
  };
};

export const fetchBookingData = booking_id => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: FETCH_BOOKING_BY_ID });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Bookings.fetchBookingData(),
        isMAPI: true,
        data: { booking_id }
      });
      // console.log("response --FETCH_BOOKING_BY_ID--", response);
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: FETCH_BOOKING_BY_ID_COMPLETED,
          payload: data
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: FETCH_BOOKING_BY_ID_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: FETCH_BOOKING_BY_ID_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.bookings) {
        return { ...state, ...payload.bookings };
      }
      return state;
    }
  }
};
