export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.tat_dict) {
        return { ...state, ...payload.tat_dict };
      }
      return state;
    }
  }
};
