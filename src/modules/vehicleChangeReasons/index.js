export default (state = {}, action = {}) => {
    const {type, payload = {}} = action;
    switch (type) {
        default: {
            if (payload.vehicle_change_reasons) {
                return {...state, ...payload.vehicle_change_reasons};
            }
            return state;
        }
    }
};

