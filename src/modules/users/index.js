export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.users) {
        return { ...state, ...payload.users };
      }
      return state;
    }
  }
};
