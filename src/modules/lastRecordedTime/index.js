const INITIAL_STATE = {};

const LAST_RECORDED_TIME = "LAST_RECORDED_TIME";

export const lastRecordedTime = requestData => {
  console.log(
    "requestData --DOWNLOAD REPORT------------------------------------------------",
    requestData
  );
  const { last_requested_report } = requestData;

  return async dispatch => {
    let response = {};
    return response;
  };
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action || {};
  switch (type) {
    case LAST_RECORDED_TIME:
      return {
        ...state,
        isFetching: true,
        hasError: false,
        error: ""
      };
    default:
      return state;
  }
};
