import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { Vehicles } from "../../helpers/Urls";
import moment from "moment";

const INITIAL_STATE = {};

const FETCH_DOWNLOAD_REPORT = "FETCH_DOWNLOAD_REPORT";
const FETCH_DOWNLOAD_REPORT_COMPLETED = "FETCH_DOWNLOAD_REPORT_COMPLETED";
const FETCH_DOWNLOAD_REPORT_FAILED = "FETCH_DOWNLOAD_REPORT_FAILED";

export const sendDownloadReport = () => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: FETCH_DOWNLOAD_REPORT });
      response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: Vehicles.fetchDownloadReport(),
        isMAPI: true
      });
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: FETCH_DOWNLOAD_REPORT_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: FETCH_DOWNLOAD_REPORT_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: FETCH_DOWNLOAD_REPORT_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action || {};

  switch (type) {
    case FETCH_DOWNLOAD_REPORT:
      return {
        ...state,
        isFetching: true,
        hasError: false,
        error: ""
      };
    case FETCH_DOWNLOAD_REPORT_COMPLETED:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        sendDownloadReport: payload,
        lastDownloadReport: new moment()
      };
    case FETCH_DOWNLOAD_REPORT_FAILED:
      return {
        ...state,
        isFetching: false,
        hasError: true,
        error: payload.message
      };

    default:
      return state;
  }
};
