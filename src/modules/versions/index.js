export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.versions) {
        return { ...state, ...payload.versions };
      }
      return state;
    }
  }
};
