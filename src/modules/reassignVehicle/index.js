import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { Bookings } from "../../helpers/Urls";

export const REASSIGN_VEHICLE = "REASSIGN_VEHICLE";
export const REASSIGN_VEHICLE_COMPLETED = "REASSIGN_VEHICLE_COMPLETED";
export const REASSIGN_VEHICLE_FAILED = "REASSIGN_VEHICLE_FAILED";

export const reassignVehicle = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: REASSIGN_VEHICLE });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Bookings.assignVehicleURL(),
        isMAPI: true,
        data: requestData
      });
      console.log("response --REASSIGN_VEHICLE--", response);
      const { data, status } = response || {};

      if (status === true) {
        const { booking_id } = requestData;
        dispatch({
          type: REASSIGN_VEHICLE_COMPLETED,
          payload: { ...data, booking_id }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: REASSIGN_VEHICLE_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: REASSIGN_VEHICLE_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;

  switch (type) {
    default: {
      if (payload.data) {
        return { ...state, ...payload.data };
      }
      return state;
    }
  }
};
