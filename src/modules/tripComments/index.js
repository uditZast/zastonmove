export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.trip_comments) {
        return { ...state, ...payload.trip_comments };
      }
      return state;
    }
  }
};
