import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { Bookings } from "../../helpers/Urls";

export const DOCK_BOOKING = "DOCK_BOOKING";
export const DOCK_BOOKING_COMPLETED = "DOCK_BOOKING_COMPLETED";
export const DOCK_BOOKING_FAILED = "DOCK_BOOKING_FAILED";

export const dockBooking = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: DOCK_BOOKING });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Bookings.dockBookingURL(),
        isMAPI: true,
        data: requestData
      });
      console.log("response --DOCK_BOOKING--", response);
      const { data, status } = response || {};

      if (status === true) {
        const { booking_id } = requestData;
        dispatch({
          type: DOCK_BOOKING_COMPLETED,
          payload: { ...data, booking_id }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: DOCK_BOOKING_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: DOCK_BOOKING_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;

  switch (type) {
    default: {
      if (payload.data) {
        return { ...state, ...payload.data };
      }
      return state;
    }
  }
};
