import { doRequest } from "../../../helpers/network";
import { REQUEST_TYPE } from "../../../../constants";
import { Vehicles } from "../../../helpers/Urls";

const INITIAL_STATE = {};

const FETCH_AVAILABLE_VEHICLES = "FETCH_AVAILABLE_VEHICLES";
const FETCH_AVAILABLE_VEHICLES_COMPLETED = "FETCH_AVAILABLE_VEHICLES_COMPLETED";
const FETCH_AVAILABLE_VEHICLES_FAILED = "FETCH_AVAILABLE_VEHICLES_FAILED";

const REQUEST_AGAIN = "REQUEST_AGAIN";

export const fetchAvailableVehicles = () => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: FETCH_AVAILABLE_VEHICLES });
      response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: Vehicles.fetchAvailableVehiclesURL(),
        isMAPI: true
      });
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: FETCH_AVAILABLE_VEHICLES_COMPLETED,
          payload: data
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: FETCH_AVAILABLE_VEHICLES_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: FETCH_AVAILABLE_VEHICLES_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action || {};
  switch (type) {
    case FETCH_AVAILABLE_VEHICLES:
      return {
        ...state,
        isFetching: true,
        hasError: false,
        error: ""
      };
    case FETCH_AVAILABLE_VEHICLES_COMPLETED:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        available_vehicles: payload.available_vehicles
      };
    case FETCH_AVAILABLE_VEHICLES_FAILED:
      return {
        ...state,
        isFetching: false,
        hasError: true,
        error: payload.message
      };
    case REQUEST_AGAIN:
      return {
        ...state,
        updated_at: payload.updated_at
      };

    default:
      return state;
  }
};
