import { doRequest } from "../../../helpers/network";
import { REQUEST_TYPE } from "../../../../constants";
import { Bookings } from "../../../helpers/Urls";
import { CREATE_BOOKING_COMPLETED } from "../../bookings";

const INITIAL_STATE = {};
const FETCH_MY_BOOKINGS = "FETCH_MY_BOOKINGS";
const FETCH_MY_BOOKINGS_COMPLETED = "FETCH_MY_BOOKINGS_COMPLETED";
const FETCH_MY_BOOKINGS_FAILED = "FETCH_MY_BOOKINGS_FAILED";

export const fetchMyBookings = () => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: FETCH_MY_BOOKINGS });
      response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: Bookings.getMyBookingsURL(),
        isMAPI: true
      });
      const { status, data = {} } = response;
      if (status === true) {
        dispatch({
          type: FETCH_MY_BOOKINGS_COMPLETED,
          payload: { ...data }
        });
      } else {
        const { message = "Error" } = response || {};
        dispatch({
          type: FETCH_MY_BOOKINGS_COMPLETED,
          payload: { message: message }
        });
      }
    } catch (err) {
      dispatch({
        type: FETCH_MY_BOOKINGS_FAILED,
        payload: { message: `${err}` }
      });
    }
    return response;
  };
};

const addBookingToMyBookings = (state, payload) => {
  const { bookings } = payload;

  const newBookingId = Object.keys(bookings).map(Number)[0];
  const { basicInfo: { return_booking_id } = {} } = bookings[newBookingId];

  const { booking_ids = [] } = state;

  const newMyBookings = [...booking_ids];
  newMyBookings.unshift(newBookingId);

  return {
    ...state,
    booking_ids: newMyBookings
  };
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action || {};
  switch (type) {
    case FETCH_MY_BOOKINGS:
      return {
        ...state,
        isFetching: true,
        hasError: false,
        error: ""
      };
    case FETCH_MY_BOOKINGS_COMPLETED:
      return {
        ...state,
        isFetching: false,
        hasError: false,
        booking_ids: payload.booking_ids
      };
    case CREATE_BOOKING_COMPLETED:
      return addBookingToMyBookings(state, payload);
    case FETCH_MY_BOOKINGS_FAILED:
      return {
        ...state,
        isFetching: false,
        hasError: true,
        error: payload.message
      };
    default:
      return state;
  }
};
