import { doRequest } from "../../../helpers/network";
import { REQUEST_TYPE, TRIP_STATUS } from "../../../../constants";
import { Trips } from "../../../helpers/Urls";
import {
  MARK_TRIP_AS_ARRIVED_COMPLETED,
  MARK_TRIP_AS_BEGIN_COMPLETED,
  MARK_TRIP_AS_CLOSED_COMPLETED,
  MARK_TRIP_AS_TERMINATE_COMPLETED
} from "../../trips";

const INITIAL_STATE = {
  isFetching: false,
  hasError: false,
  error: "",
  new_trip_ids: [],
  in_transit_trip_ids: [],
  at_dest_trip_ids: []
};

const FETCH_TRIPS_BY_STATUS = "FETCH_TRIPS_BY_STATUS";
const FETCH_TRIPS_BY_STATUS_COMPLETED = "FETCH_TRIPS_BY_STATUS_COMPLETED";
const FETCH_TRIPS_BY_STATUS_FAILED = "FETCH_TRIPS_BY_STATUS_FAILED";

export const fetchTripsByStatus = tripStatus => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: FETCH_TRIPS_BY_STATUS });
      response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: Trips.fetchAllTripsByStatusURL(tripStatus),
        isMAPI: true
      });
      console.log("response --FETCH_TRIPS_BY_STATUS--", response);
      console.log("tripStatus ----", tripStatus);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: FETCH_TRIPS_BY_STATUS_COMPLETED,
          payload: { ...data, tripStatus }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: FETCH_TRIPS_BY_STATUS_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: FETCH_TRIPS_BY_STATUS_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

const onTripsFetchedSuccessfully = (state, payload) => {
  console.log("payload ----", payload);
  const { tripStatus, trip_ids = [] } = payload;
  if (tripStatus === TRIP_STATUS.NEW) {
    return {
      ...state,
      isFetching: false,
      hasError: false,
      new_trip_ids: trip_ids
    };
  } else if (tripStatus === TRIP_STATUS.IN_TRANSIT) {
    return {
      ...state,
      isFetching: false,
      hasError: false,
      in_transit_trip_ids: trip_ids
    };
  } else if (tripStatus === TRIP_STATUS.AT_DESTINATION) {
    return {
      ...state,
      isFetching: false,
      hasError: false,
      at_dest_trip_ids: trip_ids
    };
  }
};

const filterArray = (entry, array) => {
  const newArray = [...array];
  const index = newArray.indexOf(entry);
  if (index !== -1) {
    newArray.splice(index, 1);
  }
  return newArray;
};

const addEntryToArray = (entry, array) => {
  const newArray = [...array];
  const index = newArray.indexOf(entry);
  if (index === -1) {
    newArray.unshift(entry);
  }
  return newArray;
};

const removeTripId = (state, payload) => {
  const { trip_id } = payload;

  const {
    new_trip_ids = [],
    in_transit_trip_ids = [],
    at_dest_trip_ids = []
  } = state;
  const newTripArray = filterArray(trip_id, new_trip_ids);
  const inTransitArray = filterArray(trip_id, in_transit_trip_ids);
  const atDestArray = filterArray(trip_id, at_dest_trip_ids);
  return {
    ...state,
    new_trip_ids: newTripArray,
    in_transit_trip_ids: inTransitArray,
    at_dest_trip_ids: atDestArray
  };
};

const removeTripIdFromInTransitAndAddToReached = (state, payload) => {
  const { trip_id, trips } = payload;
  const { in_transit_trip_ids = [], at_dest_trip_ids = [] } = state;
  const inTransitArray = filterArray(trip_id, in_transit_trip_ids);
  let atDestArray = [...at_dest_trip_ids];

  const trip = trips[trip_id] || {};
  const { basicInfo: { customer_id } = {} } = trip;
  if (customer_id) {
    //IF It's Dry run
    atDestArray = addEntryToArray(trip_id, at_dest_trip_ids);
  }
  return {
    ...state,
    in_transit_trip_ids: inTransitArray,
    at_dest_trip_ids: atDestArray
  };
};

const addTripToInTransit = (state, payload) => {
  const { trip_id } = payload;
  const {
    new_trip_ids = [],
    in_transit_trip_ids = [],
    at_dest_trip_ids = []
  } = state;

  const newTripArray = filterArray(trip_id, new_trip_ids);
  const atDestArray = filterArray(trip_id, at_dest_trip_ids);
  const inTransitArray = addEntryToArray(trip_id, in_transit_trip_ids);

  return {
    ...state,
    new_trip_ids: newTripArray,
    in_transit_trip_ids: inTransitArray,
    at_dest_trip_ids: atDestArray
  };
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action || {};
  switch (type) {
    case FETCH_TRIPS_BY_STATUS:
      return {
        ...state,
        isFetching: true,
        hasError: false,
        error: ""
      };
    case FETCH_TRIPS_BY_STATUS_COMPLETED:
      return onTripsFetchedSuccessfully(state, payload);
    case MARK_TRIP_AS_TERMINATE_COMPLETED:
    case MARK_TRIP_AS_CLOSED_COMPLETED:
      return removeTripId(state, payload);
    case MARK_TRIP_AS_ARRIVED_COMPLETED:
      return removeTripIdFromInTransitAndAddToReached(state, payload);
    case MARK_TRIP_AS_BEGIN_COMPLETED:
      return addTripToInTransit(state, payload);
    case FETCH_TRIPS_BY_STATUS_FAILED:
      return {
        ...state,
        isFetching: false,
        hasError: true,
        new_trip_ids: [],
        in_transit_trip_ids: [],
        at_dest_trip_ids: [],
        error: payload.message
      };

    default:
      return state;
  }
};
