import { doRequest } from "../../../helpers/network";
import { REQUEST_TYPE } from "../../../../constants";
import { Vehicles } from "../../../helpers/Urls";

const INITIAL_STATE = {
  isFetching: false,
  hasError: false,
  error: "",
  data: []
};

const FETCH_VENDOR_APPROVAL = "FETCH_VENDOR_APPROVAL";
const FETCH_VENDOR_APPROVAL_COMPLETED = "FETCH_VENDOR_APPROVAL_COMPLETED";
const FETCH_VENDOR_APPROVAL_FAILED = "FETCH_VENDOR_APPROVAL_FAILED";

export const fetchVendorApprovalPending = () => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: FETCH_VENDOR_APPROVAL });
      response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: Vehicles.fetchVendorApprovalPendingURL(),
        isMAPI: true
      });

      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: FETCH_VENDOR_APPROVAL_COMPLETED,
          payload: { ...data, status }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: FETCH_VENDOR_APPROVAL_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: FETCH_VENDOR_APPROVAL_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action || {};
  switch (type) {
    case FETCH_VENDOR_APPROVAL:
      return {
        ...state,
        isFetching: true,
        hasError: false,
        error: ""
      };
    case FETCH_VENDOR_APPROVAL_COMPLETED:
      return {
        ...state,
        data: payload
      };
    case FETCH_VENDOR_APPROVAL_FAILED:
      return {
        ...state,
        isFetching: false,
        hasError: true,
        error: payload.message,
        data: []
      };

    default:
      return state;
  }
};
