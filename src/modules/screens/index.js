import { combineReducers } from "redux";
import availableVehicles from "./availableVehicles";
import allTrips from "./allTrips";
import myBookings from "./myBookings";
import allBookings from "./allBookings";
import search from "./search";
import trackingDetails from "./trackingDetails";

export default combineReducers({
  availableVehicles,
  allTrips,
  myBookings,
  allBookings,
  trackingDetails,
  search
});
