import { doRequest } from "../../../helpers/network";
import { BOOKING_API_STATUS, REQUEST_TYPE } from "../../../../constants";
import { Vehicles } from "../../../helpers/Urls";
import {
  ACCEPT_BOOKING_COMPLETED,
  CREATE_BOOKING_COMPLETED
} from "../../bookings";
import { REJECT_BOOKING_COMPLETED } from "../../rejectBooking";
import { ASSIGN_VEHICLE_COMPLETED } from "../../assignVehicle";
import { DOCK_BOOKING_COMPLETED } from "../../dockBooking";

const INITIAL_STATE = {
  isFetching: false,
  hasError: false,
  error: "",
  new_requests: [],
  accepted: [],
  assigned: [],
  docked: []
};

const FETCH_BOOKINGS_BY_STATUS = "FETCH_BOOKINGS_BY_STATUS";
const FETCH_BOOKINGS_BY_STATUS_COMPLETED = "FETCH_BOOKINGS_BY_STATUS_COMPLETED";
const FETCH_BOOKINGS_BY_STATUS_FAILED = "FETCH_BOOKINGS_BY_STATUS_FAILED";

export const fetchBookingsByStatus = bookingStatus => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: FETCH_BOOKINGS_BY_STATUS });
      response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: Vehicles.fetchBookingsByStatusURL(bookingStatus),
        isMAPI: true
      });

      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: FETCH_BOOKINGS_BY_STATUS_COMPLETED,
          payload: { ...data, bookingStatus }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: FETCH_BOOKINGS_BY_STATUS_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: FETCH_BOOKINGS_BY_STATUS_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

const onBookingsFetchedSuccessfully = (state, payload) => {
  const { bookingStatus, booking_ids = [] } = payload;

  if (bookingStatus === BOOKING_API_STATUS.NEW_REQUEST) {
    return {
      ...state,
      isFetching: false,
      hasError: false,
      new_requests: booking_ids
    };
  } else if (bookingStatus === BOOKING_API_STATUS.ACCEPTED) {
    return {
      ...state,
      isFetching: false,
      hasError: false,
      accepted: booking_ids
    };
  } else if (bookingStatus === BOOKING_API_STATUS.ASSIGNED) {
    return {
      ...state,
      isFetching: false,
      hasError: false,
      assigned: booking_ids
    };
  } else if (bookingStatus === BOOKING_API_STATUS.DOCKED) {
    return {
      ...state,
      isFetching: false,
      hasError: false,
      docked: booking_ids
    };
  }
};

const filterArray = (entry, array) => {
  const newArray = [...array];
  const index = newArray.indexOf(entry);
  if (index !== -1) {
    newArray.splice(index, 1);
  }
  return newArray;
};

const addEntryToArray = (entry, array) => {
  const newArray = [...array];
  const index = newArray.indexOf(entry);
  if (index !== -1) {
    newArray.unshift(entry);
  }
  return newArray;
};

const removeBookingId = (state, payload) => {
  const { booking_id } = payload;

  const { new_requests = [], accepted = [], assigned = [] } = state;
  const newRequestArray = filterArray(booking_id, new_requests);
  const acceptedArray = filterArray(booking_id, accepted);
  const assignedArray = filterArray(booking_id, assigned);

  return {
    ...state,
    new_requests: newRequestArray,
    acceptedArray: acceptedArray,
    assigned: assignedArray
  };
};

const removeBookingIdFromRequestedAndAddToAccepted = (state, payload) => {
  const { booking_id, type, status } = payload;
  console.clear();
  console.log("payload ----", payload);
  const { new_requests = [], accepted = [], docked = [] } = state;

  console.log("newRequestArray --111111--", new_requests);
  const newRequestArray = filterArray(booking_id, new_requests);
  console.log("newRequestArray ---22222222-", newRequestArray);

  if (newRequestArray.indexOf(booking_id) === -1) {
    if (type !== "DR") {
      // console.log("acceptedArray --000000000--", accepted);
      let acceptedArray = [...accepted];
      // console.log("acceptedArray --11111111111--", acceptedArray);
      acceptedArray.unshift(booking_id);
      // console.log("acceptedArray --222222222--", acceptedArray);
      return {
        ...state,
        new_requests: newRequestArray,
        accepted: acceptedArray
      };
    } else {
      let dockedArray = [...docked];
      dockedArray.unshift(booking_id);
      return {
        ...state,
        new_requests: newRequestArray,
        docked: dockedArray
      };
    }
  }
  return { ...state };
};

const removeBookingIdFromAcceptedAndAddToAssigned = (state, payload) => {
  const { booking_id } = payload;

  const { accepted = [], assigned = [] } = state;

  const acceptedArray = filterArray(booking_id, accepted);
  let assignedArray = [...assigned];

  assignedArray.unshift(booking_id);

  return {
    ...state,
    accepted: acceptedArray,
    assigned: assignedArray
  };
};

const removeBookingIdFromAssignedAndAddToDocked = (state, payload) => {
  const { booking_id } = payload;
  const { assigned = [], docked = [] } = state;

  const assignedArray = filterArray(booking_id, assigned);
  let dockedArray = [...docked];

  dockedArray.unshift(booking_id);

  return {
    ...state,
    assigned: assignedArray,
    docked: dockedArray
  };
};

const addNewlyCreatedBookingIdToList = (state, payload) => {
  const { booking_id } = payload;
  const { new_requests = [] } = state;
  const bookingId = Number(booking_id);
  if (bookingId) {
    let newRequestsArray = [...new_requests];
    newRequestsArray.unshift(bookingId);
    return {
      ...state,
      new_requests: newRequestsArray
    };
  }
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action || {};
  switch (type) {
    case FETCH_BOOKINGS_BY_STATUS:
      return {
        ...state,
        isFetching: true,
        hasError: false,
        error: ""
      };
    case CREATE_BOOKING_COMPLETED:
      return addNewlyCreatedBookingIdToList(state, payload);
    case FETCH_BOOKINGS_BY_STATUS_COMPLETED:
      return onBookingsFetchedSuccessfully(state, payload);
    case REJECT_BOOKING_COMPLETED:
      return removeBookingId(state, payload);
    case ACCEPT_BOOKING_COMPLETED:
      return removeBookingIdFromRequestedAndAddToAccepted(state, payload);
    case ASSIGN_VEHICLE_COMPLETED:
      return removeBookingIdFromAcceptedAndAddToAssigned(state, payload);
    case DOCK_BOOKING_COMPLETED:
      return removeBookingIdFromAssignedAndAddToDocked(state, payload);

    case FETCH_BOOKINGS_BY_STATUS_FAILED:
      return {
        ...state,
        isFetching: false,
        hasError: true,
        error: payload.message,
        new_requests: [],
        accepted: [],
        assigned: [],
        docked: []
      };

    default:
      return state;
  }
};
