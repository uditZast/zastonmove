import { doRequest } from "../../../helpers/network";
import { REQUEST_TYPE } from "../../../../constants";
import { Tracking } from "../../../helpers/Urls";

const INITIAL_STATE = {
  isFetching: false,
  tracking_ids: [],
  load_more: false,
  isRefreshing: false,
  hasError: false,
  error: ""
};

const FETCH_VEHICLE_TRACKING_DETAILS = "FETCH_VEHICLE_TRACKING_DETAILS";
const FETCH_VEHICLE_TRACKING_DETAILS_COMPLETED =
  "FETCH_VEHICLE_TRACKING_DETAILS_COMPLETED";
const FETCH_VEHICLE_TRACKING_DETAILS_FAILED =
  "FETCH_VEHICLE_TRACKING_DETAILS_FAILED";

export const fetchVehicleTrackingDetails = requestData => {
  return async dispatch => {
    const { page, vehicle_id, start_date = 0, end_date = 0 } = requestData;
    let response = {};
    try {
      dispatch({ type: FETCH_VEHICLE_TRACKING_DETAILS });
      response = await doRequest({
        method: REQUEST_TYPE.GET,
        url: Tracking.getTrackingDetails(
          page,
          vehicle_id,
          start_date,
          end_date
        ),
        isMAPI: true
      });
      console.log("response --FETCH_VEHICLE_TRACKING_DETAILS--", response);
      const { data, status = true } = response || {};

      if (status === true) {
        dispatch({
          type: FETCH_VEHICLE_TRACKING_DETAILS_COMPLETED,
          payload: { ...data, page }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: FETCH_VEHICLE_TRACKING_DETAILS_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: FETCH_VEHICLE_TRACKING_DETAILS_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

const finishedFetching = (state, payload) => {
  const { load_more, tracking_ids } = payload;
  const oldTrackingIds = [...state.tracking_ids];
  const newTrackingIds = oldTrackingIds.concat(tracking_ids);

  return {
    ...state,
    load_more,
    isFetching: false,
    hasError: false,
    tracking_ids: newTrackingIds
  };
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action || {};
  switch (type) {
    case FETCH_VEHICLE_TRACKING_DETAILS:
      return {
        ...state,
        isFetching: true,
        hasError: false,
        load_more: false,
        error: ""
      };
    case FETCH_VEHICLE_TRACKING_DETAILS_COMPLETED:
      return finishedFetching(state, payload);
    case FETCH_VEHICLE_TRACKING_DETAILS_FAILED:
      return {
        ...state,
        isFetching: false,
        load_more: false,
        hasError: true,
        error: payload.message
      };

    default:
      return state;
  }
};
