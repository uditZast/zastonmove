import { doRequest } from "../../../helpers/network";
import { REQUEST_TYPE } from "../../../../constants";
import { Bookings } from "../../../helpers/Urls";

const INITIAL_STATE = {};

const SEARCH = "SEARCH";
const SEARCH_COMPLETED = "SEARCH_COMPLETED";
const SEARCH_FAILED = "SEARCH_FAILED";

const clearSearchData = (state, payload) => {
  const { booking_ids = [], trip_ids = [] } = payload;
  return {
    ...state,
    isFetching: false,
    hasError: false,
    booking_ids: booking_ids,
    trip_ids: trip_ids
  };
};

export const fetchSearchResult = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: SEARCH });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Bookings.searchURL(),
        isMAPI: true,
        data: requestData
      });
      console.log("response --SEARCH_RESULT--", response);
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: SEARCH_COMPLETED,
          payload: data
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: SEARCH_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: SEARCH_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action || {};
  // const {booking_ids} = {};
  // const {trip_ids} = {};

  switch (type) {
    case SEARCH:
      console.log("---- Search ----");
      return {
        ...state,
        isFetching: true,
        hasError: false,
        error: ""
      };
    case SEARCH_COMPLETED:
      return clearSearchData(state, payload);
    case SEARCH_FAILED:
      console.log("---- Search Failed ----");
      return {
        ...state,
        isFetching: false,
        hasError: true,
        error: payload.message,
        booking_ids: [],
        trip_ids: []
      };

    default:
      return state;
  }
};
