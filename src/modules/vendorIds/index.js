export default (state = [], action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.vendor_ids) {
        return payload.vendor_ids;
      }
      return state;
    }
  }
};
