export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.vehicles) {
        return { ...state, ...payload.vehicles };
      }
      return state;
    }
  }
};
