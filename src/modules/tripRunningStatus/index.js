import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { Trips } from "../../helpers/Urls";

const ADDING_RUNNING_STATUS = "ADDING_RUNNING_STATUS";
const ADDING_RUNNING_STATUS_COMPLETED = "ADDING_RUNNING_STATUS_COMPLETED";
const ADDING_RUNNING_STATUS_FAILED = "ADDING_RUNNING_STATUS_FAILED";

//requestData = {trip_id, }
export const addRunningStatus = (requestData = {}) => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: ADDING_RUNNING_STATUS });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Trips.addTripRunningStatusURL(),
        data: requestData,
        isMAPI: true
      });
      console.log("response --ADDING_RUNNING_STATUS--", response);
      const { data, status } = response || {};
      if (status === true) {
        dispatch({
          type: ADDING_RUNNING_STATUS_COMPLETED,
          payload: { ...data }
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: ADDING_RUNNING_STATUS_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: ADDING_RUNNING_STATUS_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};

export default (state = {}, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    default: {
      if (payload.trip_running_status) {
        return { ...state, ...payload.trip_running_status };
      }
      return state;
    }
  }
};
