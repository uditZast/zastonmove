import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { fetchVendorEditURL } from "../../helpers/Urls/vendorEdit";

const EDIT_VENDOR_DETAILS = "EDIT_VENDOR_DETAILS";
const EDIT_VENDOR_DETAILS_COMPLETED = "EDIT_VENDOR_DETAILS_COMPLETED";
const EDIT_VENDOR_DETAILS_FAILED = "EDIT_VENDOR_DETAILS_FAILED";

export const editVendorDetails = requestData => {
  return async dispatch => {
    let response = {};
    try {
      dispatch({ type: EDIT_VENDOR_DETAILS });
      response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: fetchVendorEditURL(),
        isMAPI: true,
        data: requestData
      });
      console.log("response --EDIT_VENDOR_DETAILS--", response);
      const { data, status } = response || {};

      if (status === true) {
        dispatch({
          type: EDIT_VENDOR_DETAILS_COMPLETED,
          payload: data
        });
      } else if (status === false) {
        const { message = "Error" } = response || {};
        dispatch({
          type: EDIT_VENDOR_DETAILS_FAILED,
          payload: { error: message }
        });
      }
    } catch (err) {
      dispatch({
        type: EDIT_VENDOR_DETAILS_FAILED,
        payload: { error: err.message }
      });
    }
    return response;
  };
};
