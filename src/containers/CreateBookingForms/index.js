import { connect } from "react-redux";
import CreateBookingForms from "../../components/bookings/forms/CreateBookingForms";
import { fetchBookingHistoryById } from "../../modules/bookingHistory";

const mapStateToProps = state => {
  console.log(state, "state create booking form modal -------");
  const {
    vehicles,
    vendors,
    cities,
    customers,
    vehicle_types,
    tat_dict
  } = state;
  return {
    fetchBookingHistoryById: data => dispatch(fetchBookingHistoryById(data))
  };

  return {
    error: {},
    data: { ...state },
    vehicles,
    vendors,
    cities,
    customers,
    vehicle_types,
    tat_dict
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateBookingForms);
