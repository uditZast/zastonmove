import { connect } from "react-redux";
import TripHistorySearch from "../../components/TripHistorySearch";
import { fetchTripDetails } from "../../modules/trips";

const mapStateToProps = state => {
  const { trips, tripHistory, network: { isConnected } = {} } = state;

  return {
    isConnected,
    tripHistory
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchTripHistory: data => dispatch(fetchTripDetails(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TripHistorySearch);
