import { connect } from "react-redux";
import {
  acceptBooking,
  createBooking,
  editBooking,
  fetchBookingData
} from "../../../modules/bookings";
import EditDryRun from "../../../components/EditDryRun";

const mapStateToProps = (state, ownProps) => {
  const {
    customers,
    vehicles,
    network: { isConnected } = {},
    vehicleTypes,
    vendors,
    cities,
    cityIds,
    bookings,
    engagedBy
  } = state;

  const {
    navigation: { state: { params: { booking_id, action } = {} } = {} } = {}
  } = ownProps;

  return {
    customers,
    vehicles,
    isConnected,
    vehicleTypes,
    vendors,
    cities,
    cityIds,
    booking_id,
    action,
    bookings,
    engagedBy
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createBooking: data => dispatch(createBooking(data)),
    editBooking: data => dispatch(editBooking(data)),
    fetchBookingData: data => dispatch(fetchBookingData(data)),
    acceptBooking: data => dispatch(acceptBooking(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditDryRun);
