import { connect } from "react-redux";
import BookingTwoWay from "../../../components/NewBookings/bookingTwoWay.js";
import { createBooking, fetchBookingData } from "../../../modules/bookings";

const mapStateToProps = (state, ownProps) => {
  const {
    customers,
    customerIds,
    vehicles,
    network: { isConnected } = {},
    vehicleTypes,
    vendors,
    cities,
    cityIds,
    bookings
  } = state;

  const {
    navigation: { state: { params: { booking_id } = {} } = {} } = {}
  } = ownProps;

  return {
    customers,
    customerIds,
    vehicles,
    isConnected,
    vehicleTypes,
    vendors,
    cities,
    cityIds,
    booking_id,
    bookings
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createBooking: data => dispatch(createBooking(data)),
    fetchBookingData: data => dispatch(fetchBookingData(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookingTwoWay);
