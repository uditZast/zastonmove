import { connect } from "react-redux";
import DryRun from "../../../components/NewBookings/dryRun.js";
import { createBooking, fetchBookingData } from "../../../modules/bookings";

const mapStateToProps = (state, ownProps) => {
  const {
    customers,
    vehicles,
    network: { isConnected } = {},
    vehicleTypes,
    vendors,
    cities,
    cityIds,
    bookings
  } = state;
  const {
    navigation: { state: { params: { booking_id } = {} } = {} } = {}
  } = ownProps;

  return {
    customers,
    vehicles,
    isConnected,
    vehicleTypes,
    vendors,
    cities,
    cityIds,
    bookings,
    booking_id
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createBooking: data => dispatch(createBooking(data)),
    fetchBookingData: data => dispatch(fetchBookingData(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DryRun);
