import { connect } from "react-redux";
import BookingOneWay from "../../../components/NewBookings/bookingOneWay.js";
import { createBooking, fetchBookingData } from "../../../modules/bookings";

const mapStateToProps = (state, ownProps) => {
  const {
    customers,
    customerIds,
    vehicles,
    network: { isConnected } = {},
    vehicleTypes,
    vendors,
    cities,
    cityIds,
    bookings,
    engagedBy
  } = state;

  const {
    navigation: { state: { params: { booking_id, screen } = {} } = {} } = {}
  } = ownProps;

  return {
    customers,
    customerIds,
    vehicles,
    isConnected,
    vehicleTypes,
    vendors,
    cities,
    cityIds,
    booking_id,
    screen,
    bookings,
    engagedBy
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createBooking: data => dispatch(createBooking(data)),
    fetchBookingData: data => dispatch(fetchBookingData(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookingOneWay);
