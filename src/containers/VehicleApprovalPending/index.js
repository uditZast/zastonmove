import React from "react";
import { connect } from "react-redux";
import VehicleApprovalPending from "../../components/VehicleApprovalPending";

const mapStateToProps = state => {
  const {
    auth,
    users,
    vehicleTypes,
    network: { isConnected } = {},
    vehicles,
    vehicleIds,
    vendors,
    cities
  } = state;

  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};

  return {
    auth,
    users,
    vehicleTypes,
    isConnected,
    loggedInUser,
    vehicles,
    vehicleIds,
    vendors,
    cities
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VehicleApprovalPending);
