import { connect } from "react-redux";
import AvailableVehiclesScreen from "../../components/screens/AvailableVehiclesScreen";

const mapStateToProps = state => {
  console.log(state, "state assign modal -------");
  const { vehicle_types } = state;

  return {
    error: {},
    data: { ...state },
    vehicle_types
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AvailableVehiclesScreen);
