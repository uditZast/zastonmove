import { connect } from "react-redux";
import NewTripEditTripModal from "../../modal/NewTripEditTripModal";

const mapStateToProps = state => {
  console.log(state, "state NewTripEdit-------" + JSON.stringify(state));
  const { vendors, vehicles, engagedBy } = state;

  return {
    error: {},
    data: { ...state },
    vehicles,
    vendors,
    engagedBy
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewTripEditTripModal);
