import { connect } from "react-redux";
import ForcedUpdate from "../../components/ForcedUpdate";

const mapStateToProps = (state, ownProps) => {
  const { auth } = state;
  const {
    navigation: { state: { params: { user_id, username } = {} } = {} } = {}
  } = ownProps;
  return { auth, user_id, username };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForcedUpdate);
