import React from "react";
import { connect } from "react-redux";
import AdvanceTripDetails from "../../components/AdvanceTripDetails";

const mapStateToProps = state => {
  const {
    auth,
    users,
    vendors,
    vehicles,
    network: { isConnected } = {},
    customers,
    requestTripAdvance
  } = state;

  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};

  return {
    auth,
    users,
    isConnected,
    loggedInUser,
    vendors,
    vehicles,
    customers,
    requestTripAdvance
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdvanceTripDetails);
