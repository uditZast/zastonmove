import React from "react";
import { connect } from "react-redux";
import { fetchVendorApprovalPending } from "../../modules/screens/vendorApprovalPending";
import VendorApprovalPending from "../../components/VendorApprovalPending";

const mapStateToProps = state => {
  const {
    auth,
    users,
    vehicleTypes,
    network: { isConnected } = {},
    vendors,
    vendorIds
  } = state;

  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};

  return {
    auth,
    users,
    vehicleTypes,
    isConnected,
    loggedInUser,
    vendors,
    vendorIds
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchVendorApprovalPending: () => dispatch(fetchVendorApprovalPending())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VendorApprovalPending);
