import { connect } from "react-redux";
import OtpLogin from "../../components/OtpLogin";
import { verifyOtp, sendOtp } from "../../modules/auth";

const mapStateToProps = (state, ownProps) => {
  const { auth } = state;
  const {
    navigation: { state: { params: { user_id, username } = {} } = {} } = {}
  } = ownProps;
  return { auth, user_id, username };
};

const mapDispatchToProps = dispatch => {
  return {
    verifyOtp: data => dispatch(verifyOtp(data)),
    sendOtp: data => dispatch(sendOtp(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OtpLogin);
