import { connect } from "react-redux";
import AdvanceTripModal from "../../modal/AdvanceTripModal";
import {
  sendOtpToVendor,
  submitTripAdvance,
  verifyOtpAdvance
} from "../../modules/requestTripAdvance";

const mapStateToProps = state => {
  const { vehicles, vendors, requestTripAdvance } = state;

  return {
    error: {},
    data: { ...state },
    vehicles,
    vendors,
    requestTripAdvance
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sendOtpToVendor: data => dispatch(sendOtpToVendor(data)),
    verifyOtpAdvance: data => dispatch(verifyOtpAdvance(data)),
    submitTripAdvance: data => dispatch(submitTripAdvance(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdvanceTripModal);
