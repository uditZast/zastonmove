import { connect } from "react-redux";
import TripCommentsSearch from "../../components/TripCommentsSearch";
import { addTripComment, fetchTripDetails } from "../../modules/trips";

const mapStateToProps = state => {
  const {
    trips,
    tripComments,
    auth,
    users,
    network: { isConnected } = {}
  } = state;
  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};
  return {
    isConnected,
    tripComments,
    loggedInUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchTripComments: data => dispatch(fetchTripDetails(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TripCommentsSearch);
