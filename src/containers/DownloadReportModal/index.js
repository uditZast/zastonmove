import { connect } from "react-redux";
import { sendDownloadReport } from "../../modules/sendDownloadReport";
import DownloadReportModal from "../../components/common/DownloadReportModal";
import moment from "moment";

const mapStateToProps = state => {
  console.log("state ----------", state);

  const { downloadReport } = state;
  const { lastDownloadReport } = downloadReport;

  const lastRequestedTime = new moment(lastDownloadReport);

  const difference = moment.duration(moment().diff(lastRequestedTime));
  console.log("difference ----", difference.asMinutes());
  console.log("difference ----", difference.asMinutes() < 5);

  let canDownloadReport;
  {
    difference.asMinutes() < 5 && difference.asMinutes() !== 0
      ? (canDownloadReport = false)
      : (canDownloadReport = true);
  }

  return {
    error: {},
    data: { ...state },
    canDownloadReport
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sendDownloadReport: data => dispatch(sendDownloadReport(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DownloadReportModal);
