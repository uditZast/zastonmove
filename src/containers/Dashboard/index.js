import { connect } from "react-redux";
import { withNavigationFocus } from "react-navigation";
import Dashboard from "../../components/Dashboard";
import { fetchInitialData, signOut } from "../../modules/auth";
import { fetchAvailableVehicles } from "../../modules/screens/availableVehicles";
import { fetchVersionCheck } from "../../modules/versionCheck";

const mapStateToProps = state => {
  const {
    customers,
    vehicles,
    network: { isConnected } = {},
    screens: { availableVehicles: pageData } = {},
    auth
  } = state;
  return { customers, vehicles, isConnected, pageData, auth };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchInitialData: () => dispatch(fetchInitialData()),
    fetchAvailableVehicles: () => dispatch(fetchAvailableVehicles()),
    fetchVersionCheck: () => dispatch(fetchVersionCheck()),
    signOut: () => dispatch(signOut())
  };
};

export default withNavigationFocus(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Dashboard)
);
