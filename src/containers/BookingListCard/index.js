import { connect } from "react-redux";
import BookingListCard from "../../components/common/BookingListCard";

const mapStateToProps = state => {
  const {
    vehicles,
    vendors,
    customers,
    rejectReasons,
    vehicleTypes,
    bookings
  } = state;

  return {
    vehicles,
    vendors,
    customers,
    rejectReasons,
    vehicleTypes,
    bookings
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookingListCard);
