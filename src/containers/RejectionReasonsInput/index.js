import { connect } from "react-redux";
import RejectionReasonInput from "../../components/common/RejectionReasonsInput";
import { rejectBooking } from "../../modules/rejectBooking";

const mapStateToProps = state => {
  const { rejectReasons } = state;

  return {
    error: {},
    data: { ...state },
    rejectReasons
  };
};

const mapDispatchToProps = dispatch => {
  return { rejectBooking: data => dispatch(rejectBooking(data)) };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RejectionReasonInput);
