import { connect } from "react-redux";
import MyBookings from "../../components/MyBookings";
import { fetchMyBookings } from "../../modules/screens/myBookings";

const mapStateToProps = (state, ownProps) => {
  const {
    auth,
    bookings,
    customers,
    vendors,
    vehicles,
    vehicleTypes,
    users,
    network: { isConnected } = {},
    screens: { myBookings: pageData } = {},
    engagedBy,
    trips
  } = state;

  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};

  return {
    auth,
    bookings,
    isConnected,
    customers,
    vendors,
    vehicles,
    vehicleTypes,
    pageData,
    loggedInUser,
    engagedBy,
    trips
  };
};

const mapDispatchToProps = dispatch => {
  return { fetchMyBookings: () => dispatch(fetchMyBookings()) };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyBookings);
