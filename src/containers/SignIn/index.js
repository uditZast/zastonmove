import { connect } from "react-redux";
import SignIn from "../../components/SignIn";
import { signIn, signOut } from "../../modules/auth";

const mapStateToProps = state => {
  const { auth } = state;
  return { auth };
};

const mapDispatchToProps = dispatch => {
  return {
    signIn: data => dispatch(signIn(data)),
    signOut: () => dispatch(signOut())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignIn);
