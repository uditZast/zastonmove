import { connect } from "react-redux";
import SearchScreen from "../../components/bookings/forms/SearchScreen";

const mapStateToProps = state => {
  console.log(state, "state Search-------" + JSON.stringify(state));
  const {
    cities,
    vehicles,
    vendors,
    customers,
    rejectReasons,
    haltReason,
    delayReasons,
    vehicleChangeReasons
  } = state;

  return {
    cities,
    vehicles,
    vendors,
    customers,
    rejectReasons,
    haltReason,
    delayReasons,
    vehicleChangeReasons
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchScreen);
