import React from "react";
import { connect } from "react-redux";
import AssignedBookings from "../../../components/BookingList/AssignedBookings";
import { fetchBookingsByStatus } from "../../../modules/screens/allBookings";
import { dockBooking } from "../../../modules/dockBooking";

const mapStateToProps = state => {
  console.log("state ----", state);
  const {
    users,
    auth,
    customers,
    vehicles,
    vendors,
    vehicleTypes,
    network: { isConnected } = {},
    screens: { allBookings: pageData } = {},
    bookings,
    engagedBy,
    trips
  } = state;
  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};

  return {
    auth,
    users,
    customers,
    vehicles,
    vendors,
    vehicleTypes,
    isConnected,
    pageData,
    bookings,
    loggedInUser,
    engagedBy,
    trips
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchBookingsByStatus: () => dispatch(fetchBookingsByStatus("assigned"))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AssignedBookings);
