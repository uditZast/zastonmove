import React from "react";
import { connect } from "react-redux";
import AcceptedBookings from "../../../components/BookingList/AcceptedBookings";
import { fetchBookingsByStatus } from "../../../modules/screens/allBookings";

const mapStateToProps = state => {
  // console.log("state ----", state);
  const {
    auth,
    users,
    customers,
    vehicles,
    vendors,
    vehicleTypes,
    network: { isConnected } = {},
    screens: { allBookings: pageData } = {},
    bookings,
    engagedBy,
    trips
  } = state;
  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};

  return {
    auth,
    users,
    customers,
    vehicles,
    vendors,
    vehicleTypes,
    isConnected,
    pageData,
    bookings,
    loggedInUser,
    engagedBy,
    trips
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchBookingsByStatus: () => dispatch(fetchBookingsByStatus("accepted"))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AcceptedBookings);
