import React from "react";
import { connect } from "react-redux";
import NewRequestBookings from "../../../components/BookingList/NewRequestBookings";
import { fetchBookingsByStatus } from "../../../modules/screens/allBookings";

const mapStateToProps = state => {
  const {
    auth,
    users,
    customers,
    vehicles,
    vendors,
    vehicleTypes,
    network: { isConnected } = {},
    screens: { allBookings: pageData } = {},
    bookings,
    engagedBy,
    trips
  } = state;

  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};

  return {
    auth,
    users,
    customers,
    vehicles,
    vendors,
    vehicleTypes,
    isConnected,
    pageData,
    bookings,
    loggedInUser,
    engagedBy,
    trips
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchBookingsByStatus: () => dispatch(fetchBookingsByStatus("requested"))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewRequestBookings);
