import { connect } from "react-redux";
import TripCharges from "../../components/TripCharges";
import { addTripCharges } from "../../modules/requestTripAdvance";

const mapStateToProps = (state, ownProps) => {
  const {
    customers,
    vehicles,
    network: { isConnected } = {},
    vendors,
    requestTripAdvance
  } = state;

  // const {
  //   navigation: {state: {params: {tripCharges, tripData} = {}} = {}} = {}
  // } = ownProps;

  return {
    customers,
    vehicles,
    isConnected,
    vendors,
    // tripData,
    requestTripAdvance
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addTripCharges: data => dispatch(addTripCharges(data))
    // addTripCharges: data => dispatch(addTripCharges(data)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TripCharges);
