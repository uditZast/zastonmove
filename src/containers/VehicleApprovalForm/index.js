import { connect } from "react-redux";
import VehicleApprovalForm from "../../components/VehicleApprovalForm";
import { editVehicleDetails } from "../../modules/editVehicle";

const mapStateToProps = (state, ownProps) => {
  const {
    cities,
    cityIds,
    vendors,
    auth,
    users,
    vehicleTypes,
    vehicles,
    network: { isConnected } = {}
  } = state;

  const {
    navigation: { state: { params: { vehicle_id } = {} } = {} } = {}
  } = ownProps;

  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};

  return {
    auth,
    users,
    cities,
    cityIds,
    vendors,
    isConnected,
    vehicle_id,
    loggedInUser,
    vehicleTypes,
    vehicles
  };
};

const mapDispatchToProps = dispatch => {
  return {
    editVehicleDetails: data => dispatch(editVehicleDetails(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VehicleApprovalForm);
