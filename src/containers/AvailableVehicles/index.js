import { connect } from "react-redux";
import AvailableVehicles from "../../components/AvailableVehicles";
import { fetchAvailableVehicles } from "../../modules/screens/availableVehicles";

const mapStateToProps = state => {
  const {
    auth,
    vehicleTypes,
    vendors,
    cities,
    cityIds,
    customers,
    customerIds,
    network: { isConnected } = {},
    screens: { availableVehicles: pageData } = {}
  } = state;
  return {
    auth,
    isConnected,
    vehicleTypes,
    pageData,
    vendors,
    cities,
    cityIds,
    customers,
    customerIds
  };
};

const mapDispatchToProps = dispatch => {
  return { fetchAvailableVehicles: () => dispatch(fetchAvailableVehicles()) };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AvailableVehicles);
