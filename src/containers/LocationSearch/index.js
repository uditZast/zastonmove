import { connect } from "react-redux";
import LocationSearch from "../../components/LocationSearch";

const mapStateToProps = state => {
  const { auth, network: { isConnected } = {} } = state;
  return {
    auth,
    isConnected
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LocationSearch);
