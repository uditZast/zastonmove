import {connect} from "react-redux";
import CommonModal from "../../modal/CommonModal";


const mapStateToProps = state => {
    console.log(state, "state assign modal -------");
    const {vehicles, vendors, cities} = state;

    return {
        error: {},
        data: {...state},
        vehicles,
        vendors,
        cities
    }
};


const mapDispatchToProps = dispatch => {
    return {};
};


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CommonModal);