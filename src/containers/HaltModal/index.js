import {connect} from "react-redux";
import HaltModal from "../../modal/HaltModal";


const mapStateToProps = state => {
    console.log(state, "state Rejection-------" + JSON.stringify(state));
    const {haltReason} = state;

    return {
        error: {},
        data: {...state},
        haltReason
    }
};


const mapDispatchToProps = dispatch => {
    return {};
};


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HaltModal);