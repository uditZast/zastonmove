import { connect } from "react-redux";
import EditTrip from "../../components/EditTrip/editRoute";
import { editTripRoute } from "../../modules/trips";

const mapStateToProps = (state, ownProps) => {
  const { trips, cities, cityIds } = state;
  const {
    navigation: { state: { params: { trip_id } = {} } = {} } = {}
  } = ownProps;
  const trip = trips[trip_id] || {};
  return {
    cities,
    cityIds,
    trip_id,
    trip
  };
};

const mapDispatchToProps = dispatch => {
  return {
    editTrip: data => dispatch(editTripRoute(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditTrip);
