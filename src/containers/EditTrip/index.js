import { connect } from "react-redux";
import EditTrip from "../../components/EditTrip";
import { editTrip } from "../../modules/trips";

const mapStateToProps = (state, ownProps) => {
  const {
    trips,
    customers,
    vendors,
    vehicles,
    vehicleTypes,
    engagedBy
  } = state;
  const {
    navigation: { state: { params: { trip_id } = {} } = {} } = {}
  } = ownProps;
  const trip = trips[trip_id] || {};
  return {
    customers,
    vendors,
    vehicles,
    vehicleTypes,
    engagedBy,
    trip_id,
    trip
  };
};

const mapDispatchToProps = dispatch => {
  return {
    editTrip: data => dispatch(editTrip(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditTrip);
