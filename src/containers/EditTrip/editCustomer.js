import { connect } from "react-redux";
import EditTripCustomer from "../../components/EditTrip/editCustomer";
import { editTripCustomer } from "../../modules/trips";

const mapStateToProps = (state, ownProps) => {
  const {
    trips,
    cities,
    cityIds,
    customers,
    customerIds,
    vehicleTypes
  } = state;
  const {
    navigation: { state: { params: { trip_id } = {} } = {} } = {}
  } = ownProps;
  const trip = trips[trip_id] || {};
  return {
    cities,
    cityIds,
    customers,
    customerIds,
    vehicleTypes,
    trip_id,
    trip
  };
};

const mapDispatchToProps = dispatch => {
  return {
    editTripCustomer: data => dispatch(editTripCustomer(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditTripCustomer);
