import { connect } from "react-redux";
import CrossDockTrip from "../../components/CrossDock";
import { crossDock } from "../../modules/trips";

const mapStateToProps = (state, ownProps) => {
  const {
    auth,
    trips,
    customers,
    vendors,
    vehicles,
    vehicleTypes,
    users,
    cities,
    cityIds,
    vehicleChangeReasons,
    engagedBy,
    network: { isConnected } = {}
  } = state;
  const {
    navigation: { state: { params: { trip_id } = {} } = {} } = {}
  } = ownProps;
  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};
  const trip = trips[trip_id] || {};
  return {
    auth,
    isConnected,
    customers,
    vendors,
    vehicles,
    vehicleTypes,
    trip_id,
    trip,
    cities,
    cityIds,
    vehicleChangeReasons,
    engagedBy,
    loggedInUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    crossDock: data => dispatch(crossDock(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CrossDockTrip);
