import { connect } from "react-redux";
import EngagedByInput from "../../components/common/EngagedByInput";
import engagedBy from "../../modules/engagedBy";

const mapStateToProps = state => {
  const { engagedBy } = state;

  return {
    error: {},
    data: { ...state },
    engagedBy
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EngagedByInput);
