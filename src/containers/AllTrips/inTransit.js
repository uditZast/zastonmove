import { connect } from "react-redux";
import InTransitTrips from "../../components/AllTrips/inTransit";
import { fetchTripsByStatus } from "../../modules/screens/allTrips";

const mapStateToProps = state => {
  const {
    auth,
    trips,
    customers,
    vendors,
    vehicles,
    network: { isConnected } = {},
    screens: { allTrips: pageData } = {}
  } = state;
  return {
    auth,
    isConnected,
    trips,
    customers,
    vendors,
    vehicles,
    pageData
  };
};

const mapDispatchToProps = dispatch => {
  return { fetchTripsByStatus: status => dispatch(fetchTripsByStatus(status)) };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InTransitTrips);
