import React from "react";
import { connect } from "react-redux";
import VendorDetails from "../../components/VendorDetails";

const mapStateToProps = (state, ownProps) => {
  const {
    auth,
    users,
    network: { isConnected } = {},
    vendors,
    requestTripAdvance
  } = state;

  const {
    navigation: {
      state: { params: { vendorDetailsData, vendorData } = {} } = {}
    } = {}
  } = ownProps;

  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};

  return {
    auth,
    users,
    isConnected,
    loggedInUser,
    vendors,
    requestTripAdvance,
    vendorDetailsData,
    vendorData
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VendorDetails);
