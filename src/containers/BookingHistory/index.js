import { connect } from "react-redux";
import BookingHistory from "../../components/common/BookingHistory";
import { fetchBookingCommentsById } from "../../modules/bookingComments";

const mapStateToProps = (state, ownProps) => {
  const { bookingHistory, bookings } = state;
  const { bookingId } = ownProps;
  const booking = bookings[bookingId] || {};
  const { basicInfo: { booking_history_id } = {} } = booking;
  const bHistory = bookingHistory[booking_history_id] || {};

  return {
    bHistory,
    booking
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchBookingHistoryById: data => dispatch(fetchBookingCommentsById(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookingHistory);
