import { connect } from "react-redux";
import DatePickerInput from "../../components/common/DatePickerInput";
import { dockBooking } from "../../modules/dockBooking";

const mapStateToProps = state => {
  console.log(state, "state assign modal -------");

  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    dockBooking: data => dispatch(dockBooking(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DatePickerInput);
