import React from "react";
import { connect } from "react-redux";
import AdvancePayments from "../../components/AdvancePayments";
import vendors from "../../modules/vendors";

const mapStateToProps = (state, ownProps) => {
  const {
    auth,
    users,
    vehicles,
    network: { isConnected } = {},
    vendors,
    customers,
    requestTripAdvance
  } = state;

  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};

  // const {
  //   navigation: { state: { params: { tripAdvance, tripData } = {} } = {} } = {}
  // } = ownProps;

  return {
    auth,
    users,
    isConnected,
    loggedInUser,
    vendors,
    vehicles,
    customers,
    requestTripAdvance
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdvancePayments);
