import { connect } from "react-redux";
import RunningStatusModal from "../../modal/RunningStatusModal";

const mapStateToProps = state => {
  console.log(state, "state Delay reasons-------" + JSON.stringify(state));
  const { delayReasons } = state;

  return {
    error: {},
    data: { ...state },
    delayReasons
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RunningStatusModal);
