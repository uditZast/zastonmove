import { connect } from "react-redux";
import RequestTripAdvanceStatus from "../../components/RequestTripAdvanceStatus";

const mapStateToProps = (state, ownProps) => {
  const { requestTripAdvance } = state;

  return {
    requestTripAdvance
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestTripAdvanceStatus);
