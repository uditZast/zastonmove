import { connect } from "react-redux";
import TrackingSearchPage from "../../components/TrackingSearch";

const mapStateToProps = state => {
  const {
    customers,
    vendors,
    vehicles,
    vehicleTypes,
    cities,
    network: { isConnected } = {},
    screens: { trackingDetails: pageData } = {}
  } = state;

  return {
    isConnected,
    customers,
    vendors,
    vehicles,
    vehicleTypes,
    cities,
    pageData
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TrackingSearchPage);
