import { connect } from "react-redux";
import AssignModal from "../../components/common/AssignModal";
import { assignVehicle } from "../../modules/assignVehicle";
import { reassignVehicle } from "../../modules/reassignVehicle";

const mapStateToProps = state => {
  const { vehicles, vendors } = state;

  return {
    error: {},
    data: { ...state },
    vehicles,
    vendors
  };
};

const mapDispatchToProps = dispatch => {
  return {
    assignVehicle: data => dispatch(assignVehicle(data)),
    reassignVehicle: data => dispatch(reassignVehicle(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AssignModal);
