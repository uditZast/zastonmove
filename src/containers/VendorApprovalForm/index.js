import { connect } from "react-redux";
import VendorApprovalForm from "../../components/VendorApprovalForm";
import { editVendorDetails } from "../../modules/editVendor";

const mapStateToProps = (state, ownProps) => {
  const {
    cities,
    cityIds,
    vendors,
    auth,
    users,
    network: { isConnected } = {}
  } = state;

  const {
    navigation: { state: { params: { vendor_id } = {} } = {} } = {}
  } = ownProps;

  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};

  return {
    auth,
    users,
    cities,
    cityIds,
    vendors,
    isConnected,
    vendor_id,
    loggedInUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    editVendorDetails: data => dispatch(editVendorDetails(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VendorApprovalForm);
