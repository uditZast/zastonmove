import { connect } from "react-redux";
import AuthScreen from "../../components/Auth";

const mapStateToProps = state => {
  const { auth } = state;
  return {
    auth
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthScreen);
