import { connect } from "react-redux";
import TripDetails from "../../components/TripDetails";
import {
  markTripAsArrived,
  markTripAsBegin,
  markTripAsClosed,
  markTripAsTerminate,
  markTripAsTouchingIn,
  plotTripRoute,
  updateDriverPhone,
  updateReference,
  updateTripTat
} from "../../modules/trips";

const mapStateToProps = (state, ownProps) => {
  const {
    auth,
    trips,
    customers,
    customerIds,
    vendors,
    vehicles,
    vehicleTypes,
    users,
    cities,
    network: { isConnected } = {},
    engagedBy
  } = state;
  const {
    navigation: { state: { params: { trip_id } = {} } = {} } = {}
  } = ownProps;
  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};
  const trip = trips[trip_id] || {};
  return {
    auth,
    isConnected,
    customers,
    customerIds,
    vendors,
    vehicles,
    vehicleTypes,
    trip_id,
    trip,
    cities,
    loggedInUser,
    engagedBy
  };
};

const mapDispatchToProps = dispatch => {
  return {
    markTripAsBegin: data => dispatch(markTripAsBegin(data)),
    markTripAsClosed: data => dispatch(markTripAsClosed(data)),
    markTripAsArrived: data => dispatch(markTripAsArrived(data)),
    markTripAsTouchingIn: data => dispatch(markTripAsTouchingIn(data)),
    updateDriverPhone: data => dispatch(updateDriverPhone(data)),
    updateReference: data => dispatch(updateReference(data)),
    updateTripTat: data => dispatch(updateTripTat(data)),
    markTripAsTerminate: data => dispatch(markTripAsTerminate(data)),
    plotTripRoute: data => dispatch(plotTripRoute(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TripDetails);
