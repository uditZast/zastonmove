import { connect } from "react-redux";
import TripComments from "../../components/TripDetails/comments";
import { addTripComment, fetchTripDetails } from "../../modules/trips";

const mapStateToProps = (state, ownProps) => {
  const {
    trips,
    tripComments,
    auth,
    users,
    network: { isConnected } = {}
  } = state;
  const {
    navigation: { state: { params: { trip_id } = {} } = {} } = {}
  } = ownProps;
  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};
  const trip = trips[trip_id] || {};
  return {
    isConnected,
    tripComments,
    trip_id,
    trip,
    loggedInUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addTripComment: data => dispatch(addTripComment(data)),
    fetchTripComments: data => dispatch(fetchTripDetails(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TripComments);
