import { connect } from "react-redux";
import HaltTrip from "../../components/TripDetails/haltTrip";
import {
  addTripComment,
  fetchTripDetails,
  markTripAsHalted
} from "../../modules/trips";

const mapStateToProps = (state, ownProps) => {
  const {
    auth,
    users,
    trips,
    haltReasons,
    network: { isConnected } = {}
  } = state;
  const {
    navigation: { state: { params: { trip_id } = {} } = {} } = {}
  } = ownProps;
  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};
  const trip = trips[trip_id] || {};
  return {
    isConnected,
    haltReasons,
    trip_id,
    trip,
    loggedInUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    markTripAsHalted: data => dispatch(markTripAsHalted(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HaltTrip);
