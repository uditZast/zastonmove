import { connect } from "react-redux";
import TripRunningStatus from "../../components/TripDetails/runningStatus";
import { fetchTripDetails } from "../../modules/trips";
import { addRunningStatus } from "../../modules/tripRunningStatus";

const mapStateToProps = (state, ownProps) => {
  const {
    auth,
    users,
    trips,
    tripHistory,
    delayReasons,
    tripRunningStatus,
    network: { isConnected } = {}
  } = state;
  const {
    navigation: { state: { params: { trip_id } = {} } = {} } = {}
  } = ownProps;
  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};
  const trip = trips[trip_id] || {};
  return {
    isConnected,
    loggedInUser,
    tripHistory,
    delayReasons,
    tripRunningStatus,
    trip_id,
    trip
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchTripRunningStatus: data => dispatch(fetchTripDetails(data)),
    addRunningStatus: data => dispatch(addRunningStatus(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TripRunningStatus);
