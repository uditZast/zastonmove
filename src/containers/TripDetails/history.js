import { connect } from "react-redux";
import TripHistory from "../../components/TripDetails/history";
import { fetchTripDetails } from "../../modules/trips";

const mapStateToProps = (state, ownProps) => {
  const { trips, tripHistory, network: { isConnected } = {} } = state;
  const {
    navigation: { state: { params: { trip_id } = {} } = {} } = {}
  } = ownProps;
  const trip = trips[trip_id] || {};
  return {
    isConnected,
    tripHistory,
    trip_id,
    trip
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchTripHistory: data => dispatch(fetchTripDetails(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TripHistory);
