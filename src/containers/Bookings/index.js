import { connect } from "react-redux";
import { fetchMyBookings } from "../../modules/bookings";
import MyBookings from "../../components/bookings/MyBookingsScreen";

const mapStateToProps = state => {
  const { customers } = state;
  return { customers };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchMyBookings: token => dispatch(fetchMyBookings(token))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyBookings);
