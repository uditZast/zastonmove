import { connect } from "react-redux";
import SearchResult from "../../components/SearchResult";
import { fetchSearchResult } from "../../modules/screens/search";

const mapStateToProps = (state, ownProps) => {
  const {
    auth,
    users,
    cities,
    vehicles,
    vendors,
    customers,
    vehicleTypes,
    screens: { search: pageData } = {},
    bookings,
    trips,
    engagedBy
  } = state;
  const {
    navigation: { state: { params: { requestData } = {} } = {} } = {}
  } = ownProps;
  const { authenticatedUser } = auth || {};
  const loggedInUser = users[authenticatedUser] || {};

  return {
    auth,
    requestData,
    users,
    cities,
    vehicles,
    vendors,
    customers,
    vehicleTypes,
    pageData,
    bookings,
    trips,
    loggedInUser,
    engagedBy
  };
};

const mapDispatchToProps = dispatch => {
  return { fetchSearchResult: data => dispatch(fetchSearchResult(data)) };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchResult);
