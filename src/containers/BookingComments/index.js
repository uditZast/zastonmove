import { connect } from "react-redux";
import { fetchBookingHistoryById } from "../../modules/bookingHistory";
import BookingHistory from "../../components/common/BookingHistory";

const mapStateToProps = (state, ownProps) => {
  const { bookingComments, bookings } = state;
  const { bookingId } = ownProps;
  const booking = bookings[bookingId] || {};
  // const { basicInfo: { booking_history_id } = {} } = booking;
  const { basicInfo: { booking_history_id } = {} } = booking;
  const bHistory = bookingComments[booking_history_id] || {};

  return {
    bHistory,
    booking
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchBookingHistoryById: data => dispatch(fetchBookingHistoryById(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookingHistory);
