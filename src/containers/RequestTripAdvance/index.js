import { connect } from "react-redux";
import { requestTripAdvance } from "../../modules/requestTripAdvance";
import RequestTripAdvance from "../../components/RequestTripAdvance";

const mapStateToProps = (state, ownProps) => {
  const {
    customers,
    vehicles,
    network: { isConnected } = {},
    vendors,
    requestTripAdvance
  } = state;

  return {
    customers,
    vehicles,
    isConnected,
    vendors,
    requestTripAdvance
  };
};

const mapDispatchToProps = dispatch => {
  return {
    requestTripAdvance: data => dispatch(requestTripAdvance(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestTripAdvance);
