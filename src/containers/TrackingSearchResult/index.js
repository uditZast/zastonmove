import { connect } from "react-redux";
import TrackingSearchResultsPage from "../../components/TrackingSearchResult";
import { fetchVehicleTrackingDetails } from "../../modules/screens/trackingDetails";

const mapStateToProps = (state, ownProps) => {
  const {
    vehicles,
    vehicleTypes,
    tracking,
    network: { isConnected } = {},
    screens: { trackingDetails: pageData } = {}
  } = state;

  const { navigation: { state: { params = {} } = {} } = {} } = ownProps;
  const { vehicle_id, start_date = 0, end_date = 0 } = params || {};

  return {
    isConnected,
    vehicles,
    tracking,
    vehicleTypes,
    vehicle_id,
    start_date,
    end_date,
    pageData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchVehicleTrackingDetails: data =>
      dispatch(fetchVehicleTrackingDetails(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TrackingSearchResultsPage);
