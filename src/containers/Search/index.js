import { connect } from "react-redux";
import Search from "../../components/Search";
import { fetchSearchResult } from "../../modules/screens/search";

const mapStateToProps = state => {
  const {
    cities,
    cityIds,
    vehicles,
    vendors,
    customers,
    customerIds,
    vehicleTypes,
    rejectReasons,
    haltReason,
    delayReasons,
    vehicleChangeReasons,
    screens: { search: pageData } = {}
  } = state;

  return {
    cities,
    cityIds,
    vehicles,
    vendors,
    customers,
    customerIds,
    vehicleTypes,
    rejectReasons,
    haltReason,
    delayReasons,
    vehicleChangeReasons,
    pageData
  };
};

const mapDispatchToProps = dispatch => {
  return { fetchSearchResult: data => dispatch(fetchSearchResult(data)) };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);
