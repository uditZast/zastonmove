import { connect } from "react-redux";
import CrossDockModal from "../../modal/CrossDockModal";

const mapStateToProps = state => {
  console.log(state, "state assign modal -------");
  const { vehicles, vendors, cities, vehicleChangeReasons, engagedBy } = state;

  return {
    error: {},
    data: { ...state },
    vehicleChangeReasons,
    engagedBy,
    vehicles,
    vendors,
    cities
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CrossDockModal);
