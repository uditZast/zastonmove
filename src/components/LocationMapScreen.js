import React, {Component} from 'react';
import MapView from "react-native-maps";
import {StyleSheet} from 'react-native';
import MapViewDirections from "react-native-maps-directions";
import {Marker} from "react-native-maps";

export default class LocationMapScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            draggedCoordinate: {
                latitude: 0,
                longitude: 0
            }
        };

    }

    render() {
        return (
            <MapView
                initialRegion={{
                    latitude: 28.4595,
                    longitude: 77.0266,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421
                }}>

                <MapViewDirections
                    origin={{latitude: 28.4595, longitude: 77.0266}}
                    destination={{latitude: 28.7041, longitude: 77.1025}}
                    strokeWidth={3}
                    strokeColor="hotpink"
                    apikey={'AIzaSyAj-miEdT-tzK792VlaXkxkvEXd3e_1swA'}
                />

            </MapView>

        );
    }
}


const styles = StyleSheet.create({
    map: {
        flex: 1,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    mapViewStyle: {
        height: '100%'
    },
});