import React, { Component, Fragment } from "react";
import {
  FlatList,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../common/Colors";
import { ZONES } from "../../../constants";
import SelectInput from "react-native-select-input-ios";
import Header from "../common/header";
import VehicleTypeInput from "../common/VehicleTypeInput";
import customStyles from "../common/Styles";
import { Icon } from "react-native-elements";
import RBSheet from "react-native-raw-bottom-sheet";
import VendorInput from "../common/VendorsInput";
import CityInput from "../common/CityInput";
import CustomerInput from "../common/CustomerInput";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import FilterValue from "../common/FilterValue";
import { filter, groupBy, pickBy } from "lodash";
import AvailableVehicleRowData from "../common/AvailableVehicleRowData";
import DownloadReportModal from "../../containers/DownloadReportModal";

class AvailableVehicles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedVType: 0,
      selectedZone: 0,
      selectedVendor: 0,
      selectedVendorCode: null,
      selectedSourceCity: 0,
      selectedDestinationCity: 0,
      selectedSourceCityCode: null,
      selectedDestinationCityCode: null,
      booked: false,
      selectedCustomer: 0,
      selectedCustomerCode: null,
      leftSlider: 0,
      rightSlider: 0,
      contracted: false,
      zastnow: false,
      bookedCount: null,
      contractedCount: null,
      zastCount: null,
      zoneValue: null,
      vType: null,
      groupedKeys: [],
      sliderValue: null,
      multiSliderValue: [0, 7],
      showModal: false
    };
  }

  componentDidMount() {
    const { fetchAvailableVehicles } = this.props;
    fetchAvailableVehicles();
    this.setInitialCounts();
  }

  setInitialCounts = () => {
    const { pageData: { available_vehicles = [] } = {} } = this.props;

    if (available_vehicles.length > 0) {
      let bookedCount = 0,
        cCount = 0,
        zCount = 0;

      available_vehicles.forEach(item => {
        const { isBooked = false, category } = item;

        if (isBooked) {
          bookedCount++;
        }

        if (category === "con") {
          cCount++;
        }

        if (category !== "con") {
          zCount++;
        }
      });

      let groupedData = groupBy(available_vehicles, element => {
        const { route } = element;
        return route.split("-").pop(-1);
      });

      // let groupedKeys = Object.keys(groupedData);
      let tempArray = Object.keys(groupedData);
      let sortedArray = tempArray.sort();

      let groupedKeys = [...sortedArray];

      this.setState({
        bookedCount: bookedCount,
        contractedCount: cCount,
        zastCount: zCount,
        groupedKeys,
        groupedData
      });
    }
  };

  getZones = () => {
    const options = [];
    ZONES.forEach((zone, index) => {
      options.push({
        label: zone,
        value: index
      });
    });
    return options;
  };

  setZone = id => {
    let name = ZONES[id];
    this.setState({ selectedZone: id, zoneValue: name });
  };

  selectVehicle = vehicle => {
    const { vehicleTypes } = this.props;

    this.setState({ selectedVType: vehicle });

    if (vehicle === 0) {
      this.setState({ selectedVType: vehicle, vType: "All" });
    } else {
      const { basicInfo: { name } = {} } = vehicleTypes[vehicle];
      this.setState({ vType: name });
    }
  };

  selectVendor = (vendor, code) => {
    this.setState({ selectedVendor: vendor, selectedVendorCode: code });
  };

  selectSourceCity = (city, code) => {
    this.setState({ selectedSourceCity: city, selectedSourceCityCode: code });
  };

  selectDestinationCity = (city, code) => {
    this.setState({
      selectedDestinationCity: city,
      selectedDestinationCityCode: code
    });
  };

  selectCustomer = (id, code) => {
    this.setState({ selectedCustomer: id, selectedCustomerCode: code });
  };

  renderVehicleNew = ({ item: key, index }) => {
    const { groupedData = {} } = this.state;

    const items = groupedData[key] || {};

    const {
      selectedVType,
      selectedZone,
      selectedVendor,
      selectedSourceCity,
      selectedDestinationCity,
      booked,
      selectedCustomer,
      leftSlider,
      rightSlider,
      contracted,
      zastnow
    } = this.state;

    const { vehicleTypes, vendors, cities, customers } = this.props;

    let cityName;

    pickBy(cities, element => {
      const { basicInfo: { code, name } = {} } = element;
      if (code === key) {
        cityName = name;
      }
    });

    const { basicInfo: { name } = {} } = vehicleTypes[selectedVType] || {};

    const { basicInfo: { code, nick_name } = {} } =
      vendors[selectedVendor] || {};

    const { basicInfo: { code: cityCode } = {} } =
      cities[selectedSourceCity] || {};

    const { basicInfo: { code: destinationCode } = {} } =
      cities[selectedDestinationCity] || {};

    const { basicInfo: { code: customerCode } = {} } =
      customers[selectedCustomer] || {};

    let conFilteredData = filter(items, element => {
      const { category } = element;

      if (category === "con") {
        return category;
      }
    });

    if (selectedVType !== 0) {
      conFilteredData = conFilteredData.filter(vehicle => {
        const { vehicle_type } = vehicle || {};
        return `${name}` === `${vehicle_type}`;
      });
    }

    if (selectedZone !== 0) {
      const zoneChar = ZONES[selectedZone].charAt(0) || "";
      conFilteredData = conFilteredData.filter(vehicle => {
        const { zone } = vehicle || {};
        return `${zoneChar}` === `${zone}`;
      });
    }

    if (leftSlider !== 0 || rightSlider !== 0) {
      conFilteredData = conFilteredData.filter(vehicle => {
        const { free_since_seconds } = vehicle || {};

        let days = Math.floor(free_since_seconds / (3600 * 24));

        if (days >= leftSlider && days <= rightSlider) {
          return true;
        } else {
          return false;
        }
      });
    }

    if (selectedVendor !== 0) {
      conFilteredData = conFilteredData.filter(vCode => {
        const { vendor } = vCode || {};
        return `${code}` === `${vendor}`;
      });
    }

    if (selectedCustomer !== 0) {
      conFilteredData = conFilteredData.filter(vCode => {
        const { customer } = vCode || {};
        return `${customerCode}` === `${customer}`;
      });
    }

    if (selectedSourceCity !== 0) {
      conFilteredData = conFilteredData.filter(sCity => {
        const { route } = sCity || {};

        let cname = route.split("-")[0];

        return `${cityCode}` === `${cname}`;
      });
    }

    if (selectedDestinationCity !== 0) {
      conFilteredData = conFilteredData.filter(sCity => {
        const { route } = sCity || {};

        let cname = route.split("-");
        let a = cname[cname.length - 1];

        return `${destinationCode}` === `${a}`;
      });
    }

    if (booked) {
      conFilteredData = conFilteredData.filter(sCity => {
        const { isBooked = false } = sCity || {};

        return isBooked;
      });
    }

    if (zastnow) {
      return false;
    }

    return (
      <Fragment key={index}>
        <View style={{ flex: 1 }}>
          {conFilteredData.length > 0 && (
            <View style={{ flexDirection: "row" }}>
              <View
                style={[
                  styles.listContainer,
                  {
                    marginTop: index === 0 ? 5 : 10,
                    padding: index === 0 ? 5 : 5
                  }
                ]}
              >
                <Text style={styles.sectionTextStyle}>
                  {key} {"("}
                  {cityName}
                  {")"}
                </Text>
              </View>
              <View style={{ flex: 0.5 }} />
            </View>
          )}

          {conFilteredData.map(rowData => {
            return <AvailableVehicleRowData rowData={rowData} />;
          })}
        </View>
      </Fragment>
    );
  };

  renderVehicle = ({ item: key, index }) => {
    const { groupedData = {} } = this.state;

    const items = groupedData[key] || {};

    const {
      selectedVType,
      selectedZone,
      selectedVendor,
      selectedSourceCity,
      selectedDestinationCity,
      booked,
      selectedCustomer,
      leftSlider,
      rightSlider,
      contracted,
      zastnow
    } = this.state;

    const { vehicleTypes, vendors, cities, customers } = this.props;

    let cityName;

    pickBy(cities, element => {
      const { basicInfo: { code, name } = {} } = element;
      if (code === key) {
        cityName = name;
      }
    });

    const { basicInfo: { name } = {} } = vehicleTypes[selectedVType] || {};

    const { basicInfo: { code, nick_name } = {} } =
      vendors[selectedVendor] || {};

    const { basicInfo: { code: cityCode } = {} } =
      cities[selectedSourceCity] || {};

    const { basicInfo: { code: destinationCode } = {} } =
      cities[selectedDestinationCity] || {};

    const { basicInfo: { code: customerCode } = {} } =
      customers[selectedCustomer] || {};

    let zastFilteredData = filter(items, element => {
      const { category } = element;

      if (category === "zast") {
        return category;
      }
    });

    if (selectedVType !== 0) {
      zastFilteredData = zastFilteredData.filter(vehicle => {
        const { vehicle_type } = vehicle || {};
        return `${name}` === `${vehicle_type}`;
      });
    }

    if (selectedZone !== 0) {
      const zoneChar = ZONES[selectedZone].charAt(0) || "";
      zastFilteredData = zastFilteredData.filter(vehicle => {
        const { zone } = vehicle || {};
        return `${zoneChar}` === `${zone}`;
      });
    }

    if (leftSlider !== 0 || rightSlider !== 0) {
      zastFilteredData = zastFilteredData.filter(vehicle => {
        const { free_since_seconds } = vehicle || {};

        let days = Math.floor(free_since_seconds / (3600 * 24));

        if (days >= leftSlider && days <= rightSlider) {
          return true;
        } else {
          return false;
        }
      });
    }

    if (selectedVendor !== 0) {
      zastFilteredData = zastFilteredData.filter(vCode => {
        const { vendor } = vCode || {};
        return `${code}` === `${vendor}`;
      });
    }

    if (selectedCustomer !== 0) {
      zastFilteredData = zastFilteredData.filter(vCode => {
        const { customer } = vCode || {};
        return `${customerCode}` === `${customer}`;
      });
    }

    if (selectedSourceCity !== 0) {
      zastFilteredData = zastFilteredData.filter(sCity => {
        const { route } = sCity || {};

        let cname = route.split("-")[0];

        return `${cityCode}` === `${cname}`;
      });
    }

    if (selectedDestinationCity !== 0) {
      zastFilteredData = zastFilteredData.filter(sCity => {
        const { route } = sCity || {};

        let cname = route.split("-");
        let a = cname[cname.length - 1];

        return `${destinationCode}` === `${a}`;
      });
    }

    if (booked) {
      zastFilteredData = zastFilteredData.filter(sCity => {
        const { isBooked = false } = sCity || {};

        return isBooked;
      });
    }

    if (contracted) {
      return false;
    }

    return (
      <Fragment key={index}>
        <View style={{ flex: 1 }}>
          {zastFilteredData.length > 0 && (
            <View style={{ flexDirection: "row" }}>
              <View
                style={[
                  styles.listContainer,
                  {
                    marginTop: index === 0 ? 5 : 10,
                    padding: index === 0 ? 5 : 5
                  }
                ]}
              >
                <Text style={styles.sectionTextStyle}>
                  {key} {"("}
                  {cityName}
                  {")"}
                </Text>
              </View>
            </View>
          )}

          {zastFilteredData.map(rowData => {
            return <AvailableVehicleRowData rowData={rowData} />;
          })}
        </View>
      </Fragment>
    );
  };

  renderAvailableVehiclesListGroupBy = () => {
    const { pageData: { isFetching = false } = {} } = this.props;

    let data = this.state.groupedKeys;

    return (
      <Fragment>
        <ScrollView>
          <FlatList
            data={[...data]}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderVehicleNew}
            onRefresh={this.props.fetchAvailableVehicles}
            refreshing={isFetching}
            keyboardShouldPersistTaps={"handled"}
            showsVerticalScrollIndicator={false}
          />
          <View style={{ paddingBottom: 60 }}>
            <FlatList
              data={[...data]}
              keyExtractor={(item, index) => index.toString()}
              renderItem={this.renderVehicle}
              onRefresh={this.props.fetchAvailableVehicles}
              refreshing={isFetching}
              keyboardShouldPersistTaps={"handled"}
              showsVerticalScrollIndicator={false}
            />
          </View>
        </ScrollView>

        <View
          style={{
            position: "absolute",
            width: 56,
            height: 56,
            alignItems: "center",
            justifyContent: "center",
            right: 20,
            bottom: 20,
            elevation: 8,
            backgroundColor: colors.blue,
            padding: 5,
            borderRadius: 100 / 2
          }}
        >
          <TouchableOpacity onPress={this.openDownloadDialog}>
            <Icon
              name="download"
              type="material-community"
              color={colors.white}
              size={30}
            />
          </TouchableOpacity>
        </View>
      </Fragment>
    );
  };

  clearCustomer = () => {
    this.setState({ selectedCustomerCode: null, selectedCustomer: 0 });
  };

  clearDestinationCity = () => {
    this.setState({
      selectedDestinationCityCode: null,
      selectedDestinationCity: 0
    });
  };

  clearVendor = () => {
    this.setState({ selectedVendorCode: null, selectedVendor: 0 });
  };

  clearZone = () => {
    this.setState({ selectedZone: 0, zoneValue: null });
  };

  clearVehicleType = () => {
    this.setState({ selectedVType: 0, vType: null });
  };

  clearFreeSince = () => {
    this.setState({
      leftSlider: 0,
      rightSlider: 0,
      sliderValue: null,
      multiSliderValue: [0, 7]
    });
  };

  clearBooked = () => {
    this.setState({ booked: false });
  };

  clearContracted = () => {
    this.setState({ contracted: false });
  };

  clearZastNow = () => {
    this.setState({ zastnow: false });
  };

  setIsBooked = () => {
    if (this.state.booked) {
      this.setState({ booked: false });
    } else {
      this.setState({ booked: true });
    }
  };

  setConCategory = () => {
    if (this.state.contracted) {
      this.setState({
        contracted: false,
        zastnow: false
      });
    } else {
      this.setState({ contracted: true, zastnow: false });
    }
  };

  setZastnowCategory = () => {
    if (this.state.zastnow) {
      this.setState({ zastnow: false, contracted: false });
    } else {
      this.setState({ zastnow: true, contracted: false });
    }
  };

  leftCursor = e => {
    let a = e[0] + "days" + "-" + e[1] + "days";
    this.setState({ leftSlider: e[0], rightSlider: e[1], sliderValue: a });
  };

  clearAllFilters = () => {
    this.setState({
      selectedVType: 0,
      selectedZone: 0,
      selectedVendor: 0,
      selectedVendorCode: null,
      selectedSourceCity: 0,
      selectedDestinationCity: 0,
      selectedSourceCityCode: null,
      selectedDestinationCityCode: null,
      booked: false,
      selectedCustomer: 0,
      selectedCustomerCode: null,
      leftSlider: 0,
      rightSlider: 0,
      contracted: false,
      zastnow: false,
      zoneValue: null,
      vType: null,
      sliderValue: null,
      multiSliderValue: [0, 7]
    });
  };

  multiSliderValuesChange = values => {
    let a = values[0] + "days" + "-" + values[1] + "days";

    this.setState({
      multiSliderValue: values,
      leftSlider: values[0],
      rightSlider: values[1],
      sliderValue: a
    });
  };

  openDownloadDialog = () => {
    this.setState({ showModal: true });
  };

  closeDownloadDialog = async () => {
    this.setState({ showModal: false });
  };

  render() {
    const { vehicleTypes } = this.props;

    return (
      <Fragment>
        <Header
          name={"Available Vehicles"}
          navigation={this.props.navigation}
          menu={true}
          mapIconVisible={true}
        />

        <View style={styles.container}>
          <View style={{ backgroundColor: colors.white }}>
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 1 }}>
                <View style={[customStyles.border]}>
                  <Text style={customStyles.labelText}>Zone</Text>
                  <SelectInput
                    style={
                      Platform.OS === "ios"
                        ? customStyles.pickerView
                        : { height: 30 }
                    }
                    labelStyle={{
                      color: colors.black,
                      ...Platform.select({
                        android: {
                          height: 30
                        },
                        ios: {
                          alignSelf: "center"
                        }
                      })
                    }}
                    options={this.getZones()}
                    mode={"dropdown"}
                    value={this.state.selectedZone}
                    onSubmitEditing={this.setZone}
                  />
                </View>
              </View>

              <View style={{ flex: 1 }}>
                <VehicleTypeInput
                  vehiclesTypes={vehicleTypes}
                  selectedVehicle={this.state.selectedVType}
                  selectVehicle={this.selectVehicle}
                  textCenter={true}
                />
              </View>
            </View>

            <View
              style={{
                flexDirection: "row",
                borderColor: "#ddd",
                borderWidth: 1,
                borderBottomWidth: 1,
                borderTopWidth: 1,
                padding: 5
              }}
            >
              <ScrollView
                horizontal={true}
                elevation={5}
                style={{
                  flexDirection: "row"
                }}
                showsHorizontalScrollIndicator={false}
              >
                {this.state.selectedCustomerCode && (
                  <FilterValue
                    displayValue={this.state.selectedCustomerCode}
                    clearValue={this.clearCustomer}
                    iconName={"human-male"}
                  />
                )}

                {this.state.selectedDestinationCityCode && (
                  <FilterValue
                    displayValue={this.state.selectedDestinationCityCode}
                    clearValue={this.clearDestinationCity}
                    iconName={"crosshairs-gps"}
                  />
                )}

                {this.state.selectedVendorCode && (
                  <FilterValue
                    displayValue={this.state.selectedVendorCode}
                    clearValue={this.clearVendor}
                    iconName={"alpha-v-circle"}
                  />
                )}

                {this.state.zoneValue && (
                  <FilterValue
                    displayValue={this.state.zoneValue}
                    clearValue={this.clearZone}
                    iconName={"city"}
                  />
                )}

                {this.state.vType && (
                  <FilterValue
                    displayValue={this.state.vType}
                    clearValue={this.clearVehicleType}
                    iconName={"truck"}
                  />
                )}

                {this.state.sliderValue && (
                  <FilterValue
                    displayValue={this.state.sliderValue}
                    clearValue={this.clearFreeSince}
                    iconName={"truck"}
                  />
                )}

                {this.state.booked && (
                  <FilterValue
                    displayValue={"Booked"}
                    clearValue={this.clearBooked}
                    iconName={"bookmark"}
                  />
                )}

                {this.state.contracted && (
                  <FilterValue
                    displayValue={"Contracted"}
                    clearValue={this.clearContracted}
                    iconName={"alpha-c-circle"}
                  />
                )}
                {this.state.zastnow && (
                  <FilterValue
                    displayValue={"ZastNow"}
                    clearValue={this.clearZastNow}
                    iconName={"alpha-z-circle"}
                  />
                )}
              </ScrollView>

              <TouchableOpacity
                onPress={() => this.RBSheet.open()}
                style={{
                  alignItems: "flex-end",
                  justifyContent: "center",
                  padding: 8
                }}
              >
                <Text
                  style={{
                    color: colors.blue,
                    textDecorationLine: "underline"
                  }}
                >
                  More Filters
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.legendContainer}>
            <View style={{ flexDirection: "row", justifyContent: "center" }}>
              <Icon
                name={"bookmark"}
                type="material-community"
                size={20}
                color={colors.red}
              />
              <Text style={{ justifyContent: "center" }}>Booked</Text>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "center" }}>
              <View
                style={{ width: 15, height: 15, backgroundColor: colors.blue }}
              />
              <Text style={{ marginLeft: 3 }}>ZastNow</Text>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "center" }}>
              <View
                style={{ width: 15, height: 15, backgroundColor: colors.white }}
              />
              <Text style={{ marginLeft: 3 }}>Contracted</Text>
            </View>
          </View>

          {this.renderAvailableVehiclesListGroupBy()}

          {/*     FLOATING ICON     */}
          {this.state.showModal && (
            <DownloadReportModal
              isVisible={this.state.showModal}
              closeModal={this.closeDownloadDialog}
            />
          )}

          {/*BOTTOM SHEET*/}

          <RBSheet
            ref={ref => {
              this.RBSheet = ref;
            }}
            height={600}
            duration={250}
          >
            <View
              style={{
                marginBottom: 10,
                flexDirection: "row",
                justifyContent: "center"
              }}
            >
              <View
                style={{
                  flex: 0.8,
                  alignItems: "flex-end",
                  justifyContent: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: "bold",
                    marginTop: 10,
                    fontFamily: "CircularStd-Book"
                  }}
                >
                  Refine your Search
                </Text>
              </View>

              <View
                style={{
                  flex: 0.3,
                  marginLeft: 10,
                  marginTop: 10,
                  justifyContent: "center"
                }}
              >
                {this.state.selectedVendorCode ||
                this.state.selectedCustomerCode ||
                this.state.selectedDestinationCityCode ||
                this.state.booked ||
                this.state.contracted ||
                this.state.vType ||
                this.state.zoneValue ||
                this.state.leftSlider !== 0 ||
                this.state.rightSlider !== 0 ||
                this.state.zastnow ? (
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      paddingRight: 10
                    }}
                  >
                    <TouchableOpacity
                      style={{
                        alignSelf: "flex-end"
                      }}
                      onPress={this.clearAllFilters}
                    >
                      <Text style={{ justifyContent: "center", fontSize: 12 }}>
                        Clear
                      </Text>
                    </TouchableOpacity>
                    <Text style={{ justifyContent: "center", fontSize: 12 }}>
                      {" | "}
                    </Text>
                    <TouchableOpacity
                      style={{
                        alignSelf: "flex-end"
                      }}
                      onPress={() => this.RBSheet.close()}
                    >
                      <Text style={{ justifyContent: "center", fontSize: 12 }}>
                        Apply
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : null}
              </View>
            </View>

            <VendorInput
              vendors={this.props.vendors}
              selectedVendor={this.state.selectedVendor}
              selectVendor={this.selectVendor}
              disabled={false}
            />

            <CityInput
              cities={this.props.cities}
              cityIds={this.props.cityIds}
              selectedCity={this.state.selectedDestinationCity}
              selectCity={this.selectDestinationCity}
              message={"Last Location"}
            />

            <CustomerInput
              customers={this.props.customers}
              customerIds={this.props.customerIds}
              selectedCustomer={this.state.selectedCustomer}
              selectCustomer={this.selectCustomer}
              disabled={false}
            />

            <View style={[customStyles.border]}>
              <Text style={customStyles.labelText}>Zone</Text>
              <SelectInput
                style={
                  Platform.OS === "ios"
                    ? customStyles.pickerView
                    : { height: 30 }
                }
                labelStyle={{
                  color: colors.black,
                  ...Platform.select({
                    android: {
                      height: 30
                    },
                    ios: {
                      alignSelf: "flex-start"
                    }
                  })
                }}
                options={this.getZones()}
                mode={"dropdown"}
                value={this.state.selectedZone}
                onSubmitEditing={this.setZone}
              />
            </View>

            <VehicleTypeInput
              vehiclesTypes={vehicleTypes}
              selectedVehicle={this.state.selectedVType}
              selectVehicle={this.selectVehicle}
              textCenter={false}
            />

            <View
              style={{
                flexDirection: "row",
                marginLeft: 5,
                marginRight: 5,
                marginTop: 20
              }}
            >
              <TouchableOpacity style={{ flex: 1 }} onPress={this.setIsBooked}>
                <View
                  style={[
                    styles.cardStyle,
                    {
                      alignItems: "center",
                      padding: 10,
                      backgroundColor: this.state.booked
                        ? colors.blue
                        : colors.white
                    }
                  ]}
                >
                  <Icon
                    name="bookmark"
                    type="material-community"
                    size={25}
                    color={colors.red}
                  />
                  <Text
                    style={
                      this.state.booked
                        ? [styles.textStyle, { color: colors.white }]
                        : styles.textStyle
                    }
                  >
                    Booked
                  </Text>

                  <Text
                    style={
                      this.state.booked
                        ? [
                            styles.textStyle,
                            {
                              marginTop: 0,
                              color: colors.white
                            }
                          ]
                        : [styles.textStyle, { marginTop: 0 }]
                    }
                  >
                    {"( "}
                    {this.state.bookedCount ? this.state.bookedCount : "0"}
                    {" )"}
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{ flex: 1 }}
                onPress={this.setConCategory}
              >
                <View
                  style={[
                    styles.cardStyle,
                    {
                      alignItems: "center",
                      padding: 10,
                      backgroundColor: this.state.contracted
                        ? colors.blue
                        : colors.white
                    }
                  ]}
                >
                  <Icon
                    name="alpha-c-circle"
                    type="material-community"
                    size={25}
                    color={this.state.contracted ? colors.white : colors.blue}
                  />
                  <Text
                    style={
                      this.state.contracted
                        ? [styles.textStyle, { color: colors.white }]
                        : styles.textStyle
                    }
                  >
                    Contracted
                  </Text>

                  <Text
                    style={
                      this.state.contracted
                        ? [
                            styles.textStyle,
                            {
                              marginTop: 0,
                              color: colors.white
                            }
                          ]
                        : [styles.textStyle, { marginTop: 0 }]
                    }
                  >
                    {"( "}
                    {this.state.contractedCount
                      ? this.state.contractedCount
                      : "0"}
                    {" )"}
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{ flex: 1 }}
                onPress={this.setZastnowCategory}
              >
                <View
                  style={[
                    styles.cardStyle,
                    {
                      alignItems: "center",
                      padding: 10,
                      backgroundColor: this.state.zastnow
                        ? colors.blue
                        : colors.white
                    }
                  ]}
                >
                  <Icon
                    name="alpha-z-circle"
                    type="material-community"
                    size={25}
                    color={this.state.zastnow ? colors.white : colors.blue}
                  />
                  <Text
                    style={
                      this.state.zastnow
                        ? [styles.textStyle, { color: colors.white }]
                        : styles.textStyle
                    }
                  >
                    ZastNow
                  </Text>

                  <Text
                    style={[
                      styles.textStyle,
                      {
                        marginTop: 0,
                        color: this.state.zastnow ? colors.white : colors.blue
                      }
                    ]}
                  >
                    {"( "}
                    {this.state.zastCount ? this.state.zastCount : "0"}
                    {" )"}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>

            <View
              style={{
                marginLeft: 10,
                marginTop: 20
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: "CircularStd-Book",
                    flex: 0.4
                  }}
                >
                  Free Since
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: "CircularStd-Book",
                    flex: 0.6
                  }}
                >
                  {this.state.leftSlider}
                  {" days"} {" - "}
                  {this.state.rightSlider}
                  {" days"}
                </Text>
              </View>

              <MultiSlider
                containerStyle={{
                  marginLeft: 10,
                  flex: 1,
                  marginTop: 20,
                  justifyContent: "center",
                  alignSelf: "center"
                }}
                values={[
                  this.state.multiSliderValue[0],
                  this.state.multiSliderValue[1]
                ]}
                onValuesChange={this.multiSliderValuesChange}
                min={0}
                max={7}
                step={1}
                allowOverlap={false}
                snapped
              />
            </View>
          </RBSheet>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.listBackground
  },
  filterBorder: {
    borderWidth: 1,
    borderRadius: 3,
    borderColor: colors.blue,
    flex: 1,
    margin: 10,
    height: 40
  },
  leftVehicleContainer: {
    flex: 2,
    justifyContent: "space-between",
    padding: 6
  },
  rightVehicleContainer: {
    flex: 1,
    justifyContent: "space-around",
    borderWidth: 1,
    borderLeftColor: colors.border_grey,
    borderTopColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: "transparent",
    padding: 6
  },
  miniBox: {
    borderWidth: 1,
    borderColor: colors.blue,
    alignItems: "center",
    borderRadius: 2,
    margin: 2
  },
  pickerStyle: {
    fontSize: 14,
    color: colors.grey
  },
  cardContainer: {
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10,
    justifyContent: "space-between"
  },
  locationContainer: {
    marginLeft: 10,
    marginRight: 10,
    borderTopColor: colors.border_grey,
    borderWidth: 0.5,
    borderBottomColor: "transparent",
    borderRightColor: "transparent",
    borderLeftColor: "transparent",
    paddingTop: 6,
    paddingRight: 6,
    paddingBottom: 6,
    flexDirection: "row",
    marginBottom: 5
  },
  legendContainer: {
    backgroundColor: colors.listBackground,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 10
  },
  radioButtonStyle: {
    margin: 0
  },
  cardStyle: {
    borderColor: colors.blue,
    borderWidth: 1,
    borderRadius: 2,
    borderBottomWidth: 1,
    marginLeft: 5,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    borderTopLeftRadius: 3,
    backgroundColor: colors.white,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3
  },
  textStyle: {
    fontFamily: "CircularStd-Book",
    fontSize: 13,
    marginTop: 10,
    color: colors.black65
  },
  sectionTextStyle: {
    color: colors.white,
    fontWeight: "bold",
    padding: 5,
    textAlign: "center",
    fontFamily: "CircularStd-Book",
    fontSize: 14
  },
  listContainer: {
    marginLeft: 10,
    borderColor: colors.blue,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderWidth: 1,
    alignItems: "center",
    backgroundColor: colors.blue,
    flex: 0.5
  }
});

export default AvailableVehicles;
