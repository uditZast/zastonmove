import React, { Component } from "react";
import { FlatList, Image, Picker, StyleSheet, Text, View } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import locIcon from "../../images/placeholder.png";
import Card from "../Card";
import colors from "../../colors/Colors";
import { AVAILABLE_VEHICLES } from "../../constants/Constants";
import { connect } from "react-redux";
import { BarIndicator } from "react-native-indicators";
import NetInfo from "@react-native-community/netinfo";
import Snackbar from "react-native-snackbar";
import { Container } from "../common/Container";
import axios from "axios";
import SelectInput from "react-native-select-input-ios";

let vTypeArray = [],
  zoneArray = [],
  selectedValue,
  dataSource_filter;

class AvailableVehiclesScreen extends Component {
  state = { data: [], values: "" };

  constructor(props) {
    super(props);
    this.state = {
      values: "",
      isLoading: true,
      OriginalData: [],
      dataSource: [],
      filterData: [],
      selectedVType: "",
      selectedZone: ""
    };
  }

  componentDidMount() {
    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        AsyncStorage.getItem("token").then(value => {
          console.log("token:::::::" + value);
          this.setState({ values: value });
          axios
            .get(AVAILABLE_VEHICLES, {
              headers: {
                Authorization: "Token " + value,
                "Content-Type": "application/json"
              }
            })
            .then(responseJson => {
              console.log("data::::" + JSON.stringify(responseJson));
              this.setState(
                {
                  dataSource: responseJson.data.data,
                  OriginalData: responseJson.data.data,
                  isLoading: false
                },
                () => console.log(this.state.dataSource.length)
              );
            })
            .catch(error => {
              console.log("catch new request" + JSON.stringify(error));
              console.log("catch new request : " + error);
              if (error.response.status === 401) {
                AsyncStorage.removeItem("token");
                this.props.navigation.navigate("Auth");
              } else {
              }
            });
        });
      } else {
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });
  }

  checkUndefinedValues(value) {
    if (value !== "") {
      console.log("location::::");
      return (
        <View style={styles.rowTwoStyle}>
          <Image source={locIcon} style={styles.iconStyle} />
          <Text style={styles.normalTextStyle}>{value}</Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkUndefined(value) {
    if (value !== "") {
      return (
        <View style={styles.rowTwoStyle}>
          <Text style={styles.normalTextStyle}>{value}</Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkBooked(value) {
    if (value !== false) {
      return (
        <View>
          <View style={styles.triangleCorner} />
        </View>
      );
    } else {
      return null;
    }
  }

  conditionalMarginVehicleNum(isbooked, value) {
    if (isbooked !== false) {
      return <Text style={styles.boldTextStyle}>{value}</Text>;
    } else {
      return <Text style={styles.boldTextStyle1}>{value}</Text>;
    }
  }

  conditionalMarginRoute(isBooked, value, value1) {
    if (isBooked !== false) {
      return (
        <View style={styles.column1Style}>
          <Text style={styles.secTextStyle1}>
            {value} {" " + value1}
          </Text>
        </View>
      );
    } else {
      return (
        <Text style={styles.secTextStyle}>
          {value}
          {" " + value1}
        </Text>
      );
    }
  }

  conditionalMarginClosedon(isBooked, value) {
    if (isBooked !== false) {
      return (
        <View style={styles.column1Style}>
          <Text style={styles.secTextStyle1}>{value}</Text>
        </View>
      );
    } else {
      return <Text style={styles.secTextStyle}>{value}</Text>;
    }
  }

  rendorError() {
    if (this.state.error) {
      return <Text style={styles.errorStyle}>{this.state.error}</Text>;
    } else {
      console.log("location::::else");
      return null;
    }
  }

  openSecondActivity(rowData) {
    console.log("working");
    this.props.navigation.navigate("TripDetails", {
      ListViewClickHolder: rowData
    });
  }

  dataToSend(props) {
    return (
      <View>
        <Text>{props.vehicle}</Text>
      </View>
    );
  }

  filterSearch(text) {
    console.log("selected Value" + text);
    console.log("selected Zone" + this.state.selectedZone);
    console.log("selected VType" + this.state.selectedVType);

    if (this.state.selectedZone === "" && this.state.selectedVType === "") {
      console.log("zone= null && vType= null");
      this.setState({
        text: text,
        dataSource: this.state.OriginalData, // after filter we are setting users to new array
        isLoading: false
      });
    } else if (
      this.state.selectedZone !== "" &&
      this.state.selectedVType === ""
    ) {
      console.log("zone= not null && vType= null");

      dataSource_filter = this.state.OriginalData.filter(item => {
        const itemData = item.zone;
        const textData = this.state.selectedZone;
        return itemData.indexOf(textData) > -1;
      });

      this.setState({
        dataSource: dataSource_filter, // after filter we are setting users to new array
        isLoading: false
      });
    } else if (
      this.state.selectedZone === "" &&
      this.state.selectedVType !== ""
    ) {
      console.log("zone= null && vType= not null");
      const dataSource_filter = this.state.OriginalData.filter(item => {
        const itemData = item.vehicle_type;
        const textData = this.state.selectedVType;
        return itemData.indexOf(textData) > -1;
      });

      this.setState({
        dataSource: dataSource_filter, // after filter we are setting users to new array
        isLoading: false
      });
    } else {
      console.log("zone= not null && vType= not null");
      const dataSource_filter = this.state.OriginalData.filter(item => {
        const itemData = item.zone + " " + item.vehicle_type;
        const textData =
          this.state.selectedZone + " " + this.state.selectedVType;
        console.log(textData);
        console.log("itemdata of :::::::" + itemData.indexOf(textData));
        return itemData.indexOf(textData) > -1;
      });

      this.setState({
        dataSource: dataSource_filter, // after filter we are setting users to new array
        isLoading: false
      });
    }
  }

  render() {
    console.log("DataSource Render:" + this.state.dataSource.length);
    console.log("Original Render:" + this.state.OriginalData.length);

    if (this.props.products.products.length > 0) {
      vTypeArray = this.props.products.products[1]["vehicle_types"];
      let jsonDataDuplicate = [...vTypeArray];
      jsonDataDuplicate.splice(0, 0, { name: "All", length: 0, id: 0 });
      vTypeArray = jsonDataDuplicate;
    } else {
    }

    zoneArray = [
      { name: "All", id: 0 },
      { name: "N", id: 1 },
      { name: "S", id: 2 },
      { name: "W", id: 3 },
      { name: "E", id: 4 },
      { name: "C", id: 5 }
    ];

    console.log("Vehicle type array:  " + JSON.stringify(vTypeArray));

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <BarIndicator color={colors.blueColor} size={20} />
        </View>
      );
    }

    return (
      <Container>
        <View style={styles.container}>
          <View style={{ flexDirection: "row", backgroundColor: colors.white }}>
            <View style={styles.reasonBorderStyle}>
              <Text style={styles.phcustomerStyle}>Zone</Text>
              <Picker
                mode="dropdown"
                style={{ padding: 0, margin: 0, height: 35 }}
                selectedValue={this.state.selectedZone}
                itemStyle={{ padding: 0, height: 35 }}
                onValueChange={(itemValue, itemIndex) => {
                  itemValue === "All"
                    ? this.setState(
                        {
                          selectedZone: "",
                          isLoading: true
                        },
                        () => this.filterSearch(this.state.selectedZone)
                      )
                    : this.setState(
                        {
                          selectedZone: itemValue,
                          isLoading: true
                        },
                        () => this.filterSearch(this.state.selectedZone)
                      );
                }}
              >
                {zoneArray.map(item => {
                  return (
                    <Picker.Item
                      label={item.name}
                      value={item.name}
                      key={item.id}
                      color={colors.textcolor}
                    />
                  );
                })}
              </Picker>
            </View>

            <View style={styles.reasonBorderStyle}>
              <Text style={styles.phcustomerStyle}>Vehicle Type</Text>
              <Picker
                mode="dropdown"
                style={{ padding: 0, margin: 0, height: 35 }}
                selectedValue={this.state.selectedVType}
                itemStyle={{ padding: 0, height: 35 }}
                onValueChange={(itemValue, itemIndex) => {
                  itemValue === "All"
                    ? this.setState(
                        {
                          selectedVType: "",
                          isLoading: true
                        },
                        () => this.filterSearch(this.state.selectedVType)
                      )
                    : this.setState(
                        {
                          selectedVType: itemValue,
                          isLoading: true
                        },
                        () => this.filterSearch(this.state.selectedVType)
                      );
                }}
              >
                {vTypeArray.map(item => {
                  return (
                    <Picker.Item
                      label={item.name}
                      value={item.name}
                      key={item.id}
                      color={colors.textcolor}
                    />
                  );
                })}
              </Picker>
            </View>
          </View>

          <FlatList
            data={this.state.dataSource}
            keyboardShouldPersistTaps="always"
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => {
              return (
                <Card>
                  <View
                    style={
                      item === "zast"
                        ? [
                            styles.maincontainer,
                            { backgroundColor: colors.blue }
                          ]
                        : styles.maincontainer
                    }
                  >
                    {this.checkBooked(item.isBooked)}

                    <View style={{ flex: 1 }}>
                      <View style={styles.column1Style}>
                        {this.conditionalMarginVehicleNum(
                          item.isBooked,
                          item.number
                        )}
                        <Text style={[styles.lightTextStyle]}>
                          {item.vehicle_type}
                        </Text>
                      </View>
                      <View style={styles.column1Style}>
                        {this.conditionalMarginClosedon(
                          item.isBooked,
                          item.trip_status
                        )}
                      </View>
                      <View style={styles.column1Style}>
                        {this.conditionalMarginRoute(
                          item.isBooked,
                          item.route,
                          item.zone
                        )}
                      </View>
                    </View>

                    <View style={styles.lineStyle1} />

                    <View style={styles.column2Style}>
                      <View style={styles.borderStyle}>
                        <Text style={styles.tripIdStyle}>{item.trip_code}</Text>
                      </View>
                      <View style={styles.borderStyle1}>
                        <Text style={styles.borderTextStyle}>
                          {item.customer} ({item.booking_type})
                        </Text>
                      </View>
                      <View style={styles.borderStyle1}>
                        <Text style={styles.borderTextStyle}>
                          {item.vendor}
                        </Text>
                      </View>
                    </View>
                  </View>
                </Card>
              );
            }}
            keyExtractor={item => item.trip_code.toString()}
          />

          {/*<View style={{flexDirection: 'row'}}>*/}
          {/*    <TouchableOpacity onPress={() => this.filterSearch('All')}>*/}
          {/*        <Text style={{flex: 1}}>All</Text>*/}
          {/*    </TouchableOpacity>*/}
          {/*    <TouchableOpacity onPress={() => this.filterSearch('N')}>*/}
          {/*        <Text style={{flex: 1}}>N</Text>*/}
          {/*    </TouchableOpacity>*/}
          {/*    <TouchableOpacity onPress={() => this.filterSearch('S')}>*/}
          {/*        <Text style={{flex: 1}}>S</Text>*/}
          {/*    </TouchableOpacity>*/}
          {/*    <TouchableOpacity onPress={() => this.filterSearch('E')}>*/}
          {/*        <Text style={{flex: 1}}>E</Text>*/}
          {/*    </TouchableOpacity>*/}
          {/*    <TouchableOpacity onPress={() => this.filterSearch('W')}>*/}
          {/*        <Text style={{flex: 1}}>W</Text>*/}
          {/*    </TouchableOpacity>*/}
          {/*    <TouchableOpacity onPress={() => this.filterSearch('C')}>*/}
          {/*        <Text style={{flex: 1}}>C</Text>*/}
          {/*    </TouchableOpacity>*/}
          {/*</View>*/}
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.listbackgroundcolor
  },
  rowOneStyle: {
    flexDirection: "column",
    flex: 1
  },
  boldTextStyle: {
    fontWeight: "bold",
    fontSize: 17,
    color: colors.blueColor,
    fontFamily: "CircularStd-Book"
  },
  rowTwoStyle: {
    flexDirection: "row",
    marginLeft: 10,
    marginTop: 5
  },
  iconStyle: {
    height: 18,
    width: 18
  },
  normalTextStyle: {
    color: colors.textcolor,
    fontSize: 12,
    marginLeft: 10,
    marginTop: 5,
    fontFamily: "CircularStd-Book"
  },
  listStyle: {
    flex: 1,
    flexDirection: "column",
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: colors.listbackgroundcolor
  },
  errorStyle: {
    fontSize: 20,
    alignSelf: "center",
    color: "red",
    marginBottom: 10,
    fontFamily: "CircularStd-Book"
  },
  circleStyle: {
    width: 15,
    height: 15,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10
  },
  columnStyle: {
    flexDirection: "column"
  },
  rowStyle: {
    flexDirection: "row",
    flex: 1
  },
  innerView: {
    flexDirection: "column",
    padding: 5
  },
  circle: {
    width: 18,
    height: 18,
    borderRadius: 100 / 2,
    backgroundColor: colors.loginGradient1,
    marginTop: 3
  },
  circletextStyle: {
    color: colors.white,
    fontSize: 13,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    fontFamily: "CircularStd-Book"
  },
  toolbar: {
    height: 60,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    backgroundColor: colors.blueColor,
    elevation: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  textWhite: {
    fontSize: 16,
    color: colors.white,
    fontFamily: "CircularStd-Book"
  },
  secTextStyle: {
    color: colors.textcolor,
    fontSize: 12,
    marginLeft: 10,
    fontFamily: "CircularStd-Book"
  },
  secTextStyle1: {
    color: colors.textcolor,
    fontSize: 12,
    fontFamily: "CircularStd-Book"
  },
  imageStyle: {
    width: 17,
    height: 17
  },
  maincontainer: {
    flex: 1,
    flexDirection: "row"
  },
  column1Style: {
    flex: 0.6,
    flexDirection: "row",
    marginLeft: 0
  },
  column2Style: {
    flex: 0.4,
    flexDirection: "column",
    alignSelf: "flex-end",
    marginBottom: 5,
    marginRight: 5
  },
  lineStyle1: {
    height: 70,
    borderLeftColor: colors.light_grey,
    borderLeftWidth: 1,
    marginRight: 5
  },
  borderStyle: {
    borderWidth: 0.5,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor
  },
  borderStyle1: {
    borderWidth: 0.5,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 3
  },
  borderTextStyle: {
    color: colors.textcolor,
    alignSelf: "center",
    fontSize: 12,
    fontFamily: "CircularStd-Book"
  },
  tripIdStyle: {
    fontWeight: "bold",
    fontSize: 12,
    color: colors.textcolor,
    marginLeft: 10,
    alignSelf: "center",
    fontFamily: "CircularStd-Book"
  },
  triangleCorner: {
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid",
    borderRightWidth: 10,
    borderTopWidth: 10,
    borderRightColor: "transparent",
    borderTopColor: colors.bookedColor
  },
  lightTextStyle: {
    color: colors.dark_grey,
    fontSize: 12,
    marginLeft: 10,
    marginTop: 3,
    fontFamily: "CircularStd-Book"
  },
  columnMarginStyle: {
    flex: 0.6,
    flexDirection: "row",
    marginLeft: 5
  },
  boldTextStyle1: {
    fontWeight: "bold",
    fontSize: 17,
    color: colors.blueColor,
    marginLeft: 10,
    fontFamily: "CircularStd-Book"
  },
  reasonBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 10,
    paddingLeft: 5,
    paddingRight: 5,
    marginLeft: 10,
    marginRight: 10,
    flex: 0.5,
    marginBottom: 10
  },
  phcustomerStyle: {
    marginTop: -10,
    color: colors.dark_grey,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  }
});

export default AvailableVehiclesScreen;
// const mapStateToProps = state => ({
//     state: state,
//     error: state.error,
//     products: state.productsReducer.products,
//     pending: state.productsReducer.pending,
// });
//
//
// export default connect(
//     mapStateToProps,
//     null
// )(AvailableVehiclesScreen);
