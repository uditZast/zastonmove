import {Component} from 'react-native';
import LoginScreen from "../LoginScreen";
import colors from "../../colors/Colors";
import {Navigation} from "react-native-navigation";
import React from "react";



// const resetAction=StackActions.reset({
//     index:0,
//     actions:[NavigationActions.navigate({routeName:'LoginScreen'})]
// })

export default class SplashScreenPage extends Component{

    constructor(props){
        super(props);
        this.state={loading:true}
    }

    performTimeConsumingTask = async() => {
        return new Promise((resolve) =>
            setTimeout(
                () => { resolve('result') },
                2000
            )
        );
    }


    async componentDidMount() {
        // Preload data from an external API
        // Preload data using AsyncStorage
        const data = await this.performTimeConsumingTask();

        if (data !== null) {
            this.setState({ isLoading: false });
        }
    }

    // render(){
    //     return(
    //         <View style={{backgroundColor:colors.loginGradient1,flex:1}}>
    //             <Text>Hello splash!!</Text>
    //         </View>
    //     );
    // }

    render() {
        if (this.state.isLoading) {
            return <SplashScreenComponent />;
        }

        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Welcome to React Native!
                </Text>
                <Text style={styles.instructions}>
                    Reload the App to see a splash screen
                </Text>
            </View>
        );
    }
}