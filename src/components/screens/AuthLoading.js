import React, {Component} from 'react'
import AsyncStorage from '@react-native-community/async-storage';
import {BarIndicator} from "react-native-indicators";
import colors from "../../colors/Colors";

class AuthLoading extends Component {
    constructor(props) {
        super(props);
        this.bootstrapAsync();
    }

    bootstrapAsync = async () => {
        const data = await AsyncStorage.multiGet(['token', 'type']);
        let token = null;
        let type = null;
        if (data) {
            token = data[0][1];
            type = data[1][1];
        }

        this.props.navigation.navigate(token ? type === '1' ? 'HubHome' : 'VendorHome' : 'Auth');
    };

    render() {
        return (
            <BarIndicator color={colors.blueColor}
                          size={20}/>

        )
    }
}

export default AuthLoading