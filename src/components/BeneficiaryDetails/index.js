import React, { Component, Fragment } from "react";
import { StyleSheet, Text, View } from "react-native";
import colors from "../../components/common/Colors";

class BeneficiaryDetails extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { beneficiaryDetails } = this.props;

    const { vendor_details } = beneficiaryDetails;

    const {
      benef_name: benefName,
      bank_name: bank,
      account,
      ifsc
    } = vendor_details;

    return (
      <Fragment>
        <View style={styles.cardContainerStyle}>
          <View
            style={{ flexDirection: "row", paddingLeft: 5, paddingRight: 5 }}
          >
            <Text
              style={[
                styles.labelTextStyle,
                {
                  fontSize: 16,
                  color: colors.black85,
                  fontWeight: "bold"
                }
              ]}
            >
              {benefName}
            </Text>
          </View>

          <View
            style={{ flexDirection: "row", paddingLeft: 5, paddingRight: 5 }}
          >
            <Text
              style={[
                styles.labelTextStyle,
                { fontSize: 14, color: colors.black40 }
              ]}
            >
              {bank}
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              paddingLeft: 5,
              paddingRight: 5,
              paddingBottom: 5
            }}
          >
            <Text
              style={[
                styles.labelTextStyle,
                { fontSize: 14, color: colors.black40 }
              ]}
            >
              {account}
            </Text>
            <Text
              style={[
                styles.labelTextStyle,
                { fontSize: 14, color: colors.black40 }
              ]}
            >
              {" ("}
              {ifsc}
              {")"}
            </Text>
          </View>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  labelTextStyle: {
    fontFamily: "CircularStd-Book",
    paddingTop: 5,
    paddingRight: 5,
    paddingLeft: 5
  },
  cardContainerStyle: {
    borderColor: "#ddd",
    borderWidth: 1,
    borderRadius: 2,
    borderBottomWidth: 1,
    marginBottom: 5,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: colors.white,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderTopRightRadius: 3,
    borderTopLeftRadius: 3
  }
});

export default BeneficiaryDetails;
