import React, { Component, Fragment } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import CustomStyles from "../Styles";
import colors from "../Colors";
import moment from "moment";
import { NAVIGATE, USER_RIGHTS } from "../../../../constants";
import { Icon } from "react-native-elements";
import { hasRights } from "../checkRights";

class VendorApprovalCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rejectModal: false,
      showRejectModal: false,
      assignModal: false,
      historyModal: false,
      isVisible: false,
      selectedReason: null,
      showDockModal: false,
      selectedDateTime: new moment(),
      reassignModal: false,
      selectedIndex: 0
    };
  }

  goToVendorApprovalForm = () => {
    console.log("---- VENDOR APPROVAL FORM ----");
    const { id } = this.props;
    const { loggedInUser } = this.props;
    {
      (hasRights(USER_RIGHTS.APPROVE_VENDOR_OPS, loggedInUser) ||
        hasRights(USER_RIGHTS.EDIT_VENDOR, loggedInUser)) &&
        this.props.navigation.navigate(NAVIGATE.VENDOR_EDIT, {
          vendor_id: id
        });
    }
  };

  render() {
    const {
      code,
      nickName,
      name,
      phone1,
      phone2,
      email,
      is_active,
      is_otpVerified,
      address,
      is_approved,
      poc_name
    } = this.props;

    return (
      <Fragment>
        <TouchableOpacity
          style={styles.cardContainer}
          onPress={this.goToVendorApprovalForm}
        >
          <View style={{ flex: 1, padding: 0 }}>
            <View
              style={{
                padding: 10,
                flex: 1
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: "600",
                  color: colors.blue
                }}
              >
                {name}
                {nickName && (
                  <Text
                    style={{
                      color: colors.black65,
                      fontWeight: "500",
                      fontSize: 14
                    }}
                  >
                    {nickName}
                  </Text>
                )}
              </Text>

              <View style={{ flexDirection: "row", flex: 1, marginTop: 5 }}>
                {poc_name && (
                  <Text
                    style={{
                      color: colors.black65,
                      fontWeight: "500",
                      fontSize: 14,
                      flex: 0.5
                    }}
                  >
                    POC : {poc_name}
                  </Text>
                )}

                {code && (
                  <Text
                    style={{
                      color: colors.black65,
                      fontWeight: "500",
                      fontSize: 14,
                      flex: 0.5
                    }}
                  >
                    CODE : {code}
                  </Text>
                )}
              </View>
              <View style={styles.headingContainer} />
              {/*=================LINE====================*/}
              <View style={CustomStyles.dashedLine} />
              {/*=================LINE====================*/}

              <View>
                {address && (
                  <View
                    style={{
                      alignSelf: "flex-start",
                      flexDirection: "row",
                      alignItems: "center",
                      paddingTop: 4,
                      paddingBottom: 4,
                      marginRight: 10
                    }}
                  >
                    <Icon
                      size={20}
                      name={"office-building"}
                      type="material-community"
                      containerStyle={{ marginRight: 10 }}
                      color={colors.darkblue}
                    />
                    <Text
                      style={{ color: colors.black65, fontSize: 14 }}
                      numberOfLines={1}
                    >
                      {address}
                    </Text>
                  </View>
                )}

                <View style={{ flexDirection: "row" }}>
                  {phone1 && (
                    <View
                      style={{
                        alignSelf: "flex-start",
                        flexDirection: "row",
                        alignItems: "center",
                        paddingTop: 4,
                        paddingBottom: 4,
                        flex: 0.5
                      }}
                    >
                      <Icon
                        size={20}
                        name={"phone-in-talk"}
                        type="material-community"
                        containerStyle={{ marginRight: 10 }}
                        color={colors.darkblue}
                      />
                      <Text
                        style={{ color: colors.black65, fontSize: 14 }}
                        numberOfLines={1}
                      >
                        {phone1}
                      </Text>
                    </View>
                  )}

                  {phone2 && (
                    <View
                      style={{
                        alignSelf: "flex-start",
                        flexDirection: "row",
                        alignItems: "center",
                        paddingTop: 4,
                        paddingBottom: 4,
                        flex: 0.5
                      }}
                    >
                      <Icon
                        size={20}
                        name={"phone-in-talk"}
                        type="material-community"
                        containerStyle={{ marginRight: 10 }}
                        color={colors.darkblue}
                      />
                      <Text
                        style={{ color: colors.black65, fontSize: 14 }}
                        numberOfLines={1}
                      >
                        {phone2 && <Text>{"  " + phone2}</Text>}
                      </Text>
                    </View>
                  )}
                </View>
                {email && (
                  <View
                    style={{
                      alignSelf: "flex-start",
                      flexDirection: "row",
                      alignItems: "center",
                      paddingTop: 4,
                      paddingBottom: 4
                    }}
                  >
                    <Icon
                      size={20}
                      name={"email"}
                      type="material-community"
                      containerStyle={{ marginRight: 10 }}
                      color={colors.darkblue}
                    />
                    <Text
                      style={{ color: colors.black65, fontSize: 14 }}
                      numberOfLines={1}
                    >
                      {email}
                    </Text>
                  </View>
                )}

                <View style={{ flexDirection: "row" }}>
                  <View
                    style={{
                      flex: 0.5,
                      alignSelf: "flex-start",
                      flexDirection: "row",
                      alignItems: "center",
                      paddingTop: 4,
                      paddingBottom: 4
                    }}
                  >
                    <Icon
                      size={20}
                      name={is_approved ? "check" : "close"}
                      type="material-community"
                      containerStyle={{ marginRight: 10 }}
                      color={is_approved ? colors.darkGreen : colors.red}
                    />
                    <Text
                      style={{ color: colors.black65, fontSize: 14 }}
                      numberOfLines={1}
                    >
                      {is_approved ? "Approved" : "Not Approved"}
                    </Text>
                  </View>

                  <View
                    style={{
                      alignSelf: "flex-start",
                      flexDirection: "row",
                      alignItems: "center",
                      paddingTop: 4,
                      paddingBottom: 4,
                      flex: 0.5
                    }}
                  >
                    <Icon
                      size={20}
                      name={is_otpVerified ? "check" : "close"}
                      type="material-community"
                      containerStyle={{ marginRight: 10 }}
                      color={
                        is_otpVerified ? colors.darkGreen : colors.redColor
                      }
                    />
                    <Text
                      style={{ color: colors.black65, fontSize: 14 }}
                      numberOfLines={1}
                    >
                      {is_otpVerified ? "OTP Verified" : "OTP Not Verified"}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    margin: 10,
    marginTop: 0,
    flexDirection: "row",
    borderWidth: 1,
    backgroundColor: colors.black02,
    borderColor: colors.black15,
    borderRadius: 6
  },
  headingContainer: {
    flex: 1,
    paddingBottom: 4,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start"
  },
  routeContainer: {
    paddingTop: 4,
    paddingBottom: 4
  },
  addressContainer: {
    flexDirection: "row",
    alignItems: "flex-start",
    paddingTop: 4
  }
});

export default VendorApprovalCard;
