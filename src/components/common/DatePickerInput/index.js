import React, { Component, Fragment } from "react";
import CustomStyles from "../Styles";
import moment from "moment";
import DatePicker from "react-native-datepicker";
import colors from "../Colors";
import Snackbar from "react-native-snackbar";
import { View } from "react-native-animatable";
import { StyleSheet, Text } from "react-native";
import { Button } from "react-native-elements";

class DatePickerInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dateTime: null
    };
  }

  componentDidMount() {}

  setDateTime = (date, dateTime) => {
    const { selectDateTime } = this.props;
    const momentDate = new moment(dateTime).format("DD-MM-YYYY HH:mm");
    this.setState({ dateTime: momentDate });
  };

  // setDateTime = (date, dateTime) => {
  //   const { selectDateTime } = this.props;
  //   const momentDate = new moment(dateTime).format("DD-MM-YYYY HH:mm");
  //   this.setState({ dateTime: momentDate }, () => this.submitDockTime());
  // };

  submitDockTime = () => {
    console.log("---- submit dock time  ----");
    const { dockBooking } = this.props;
    const { dateTime } = this.state;
    const { bookingId, close } = this.props;

    console.log("bookingId ----", dateTime);

    if (dateTime === null) {
      Snackbar.show({
        title: "Select Dock Date and time",
        duration: Snackbar.LENGTH_SHORT
      });
    } else {
      const requestData = {
        booking_id: bookingId,
        date_time: dateTime
      };

      dockBooking(requestData)
        .then(response => {
          console.log("response ----", response);
          if (response.status === true) {
            Snackbar.show({
              title: "" + response.message,
              duration: Snackbar.LENGTH_LONG
            });
            close();
          } else {
            Snackbar.show({
              title: "" + response.message,
              duration: Snackbar.LENGTH_LONG
            });
            close();
          }
        })
        .catch(error => {
          console.log("---- error ----" + error);
        });
    }
  };

  render() {
    const { message, selectedDate } = this.props;

    return (
      <Fragment>
        <View style={[CustomStyles.border, { marginTop: 15 }]}>
          <View style={styles.labelText}>
            <Text
              style={[{ backgroundColor: "#00000003", color: colors.blue }]}
            >
              Dock Time
            </Text>
          </View>
          <DatePicker
            date={this.state.dateTime}
            mode="datetime"
            placeholder={"Select Date and Time"}
            showIcon={false}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            onDateChange={this.setDateTime}
            style={[
              CustomStyles.pickerView,
              { width: "100%", alignItems: "flex-start" }
            ]}
            hideText={!moment(selectedDate).isValid()}
            customStyles={{
              dateInput: {
                borderWidth: 0,
                backgroundColor: "transparent"
                // paddingTop: 0,
                // paddingBottom: 0
              },
              disabled: {
                backgroundColor: "transparent"
              },
              dateText: {
                color: "#000",
                alignSelf: "flex-start"
              },
              placeholderText: {
                alignSelf: "flex-start",
                color: colors.black25
              }
              //For Dark Mode
              // datePickerCon: { backgroundColor: "black" }
            }}
            getDateStr={date => {
              return new moment(date).format("LLL");
            }}
          />
        </View>

        <Button
          title={"Submit"}
          titleStyle={{
            fontSize: 15,
            fontFamily: "CircularStd-Book"
          }}
          onPress={this.submitDockTime}
          loading={this.state.isLoading}
          buttonStyle={{
            marginTop: 10,
            marginBottom: 10,
            marginLeft: 10,
            marginRight: 10
          }}
        />
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  labelText: {
    marginTop: -10,
    color: colors.blue,
    marginLeft: 10,
    backgroundColor: colors.white,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  }
});

export default DatePickerInput;
