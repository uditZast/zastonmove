import React, { Component, Fragment } from "react";
import { FloatingAction } from "react-native-floating-action";
import { withNavigation } from "react-navigation";
import { Avatar, Icon } from "react-native-elements";
import colors from "../../../colors/Colors";
import { Text } from "react-native";
import { NAVIGATE } from "../../../../constants";

const AD_HOC_ONE_WAY = "AD_HOC_ONE_WAY";
const AD_HOC_TWO_WAY = "AD_HOC_TWO_WAY";
const DRY_RUN = "DRY_RUN";
const actions = [
  {
    text: "AdHoc One Way",
    icon: (
      <Icon
        type="material"
        size={30}
        name="arrow-forward"
        color={colors.white}
      />
    ),
    name: AD_HOC_ONE_WAY,
    position: 1,
    textBackground: "transparent",
    textColor: colors.white,
    color: colors.blueColor,
    textElevation: 0,
    buttonSize: 50
    // render: () => {
    //   return <Text style={{ fontSize: 40 }}>AD_HOC_ONE_WAY</Text>;
    // }
  },
  {
    text: "AdHoc Two Way",
    icon: (
      <Icon
        type="material"
        size={30}
        name="compare-arrows"
        color={colors.white}
      />
    ),
    name: AD_HOC_TWO_WAY,
    position: 2,
    textBackground: "transparent",
    textColor: colors.white,
    color: colors.blueColor,
    textElevation: 0,
    buttonSize: 50
  },
  {
    text: "Dry Run",
    icon: (
      <Text style={{ fontSize: 22, color: colors.white, fontWeight: "600" }}>
        D
      </Text>
    ),
    name: DRY_RUN,
    position: 3,
    textBackground: "transparent",
    textColor: colors.white,
    color: colors.blueColor,
    textElevation: 0,
    buttonSize: 50
  }
];

class FloatingActionButton extends Component {
  gotoAdHocOneWayScreen = () => {
    this.props.navigation.navigate(NAVIGATE.BOOKING_ONE_WAY);
  };

  gotoAdHocTwoWayScreen = () => {
    this.props.navigation.navigate(NAVIGATE.BOOKING_TWO_WAY);
  };

  gotoDryRunScreen = () => {
    this.props.navigation.navigate(NAVIGATE.DRY_RUN);
  };

  render() {
    return (
      <Fragment>
        <FloatingAction
          actions={actions}
          color={colors.blueColor}
          onPressItem={name => {
            if (name === AD_HOC_ONE_WAY) {
              this.gotoAdHocOneWayScreen();
            } else if (name === AD_HOC_TWO_WAY) {
              this.gotoAdHocTwoWayScreen();
            } else if (name === DRY_RUN) {
              this.gotoDryRunScreen();
            }
          }}
        />
      </Fragment>
    );
  }
}

export default withNavigation(FloatingActionButton);
