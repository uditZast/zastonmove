import React, { Component, Fragment } from "react";
import { Platform, StyleSheet, Text, TextInput, View } from "react-native";
import CustomStyles from "../Styles";
import colors from "../Colors";

class NumberInput extends Component {
  constructor(props) {
    super(props);
    const { initialValue, disableDistance = false } = props;
    this.state = {
      text: initialValue,
      disable: disableDistance
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.initialValue !== this.props.initialValue ||
      prevProps.disableDistance !== this.props.disableDistance
    ) {
      this.setState((prevState, prevProps) => {
        let newState = { text: prevProps.initialValue };
        if (prevProps.disableDistance) {
          newState = { ...newState, disable: prevProps.disableDistance };
        }
        return newState;
      });
    }
  }

  onEndEditing = () => {
    const { updateText } = this.props;
    updateText(this.state.text);
  };

  render() {
    const {
      message,
      placeholder,
      numericProp,
      styleFlag = false,
      maxLengthProp = false,
      otpLengthProp = false
    } = this.props;

    return (
      <Fragment>
        <View
          style={
            this.state.disable
              ? CustomStyles.borderDisabled
              : CustomStyles.border
          }
        >
          <View style={styles.labelText}>
            <Text
              style={
                styleFlag
                  ? {
                      color: colors.blue,
                      backgroundColor: "#00000003"
                    }
                  : this.state.disable
                  ? { color: colors.black25 }
                  : { color: colors.blue }
              }
            >
              {message}
            </Text>
          </View>
          <TextInput
            placeholder={placeholder}
            style={
              this.state.disable
                ? [styles.textInputStyle, { color: colors.black25 }]
                : styles.textInputStyle
            }
            keyboardType={numericProp === true ? "numeric" : "default"}
            onChangeText={text =>
              this.setState({ text: text }, this.onEndEditing)
            }
            value={this.state.text ? String(this.state.text) : ""}
            placeholderTextColor={colors.black25}
            onEndEditing={this.onEndEditing}
            editable={!this.state.disable}
            maxLength={maxLengthProp ? 20 : otpLengthProp ? 6 : null}
          />
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  textInputStyle: {
    flex: 1,
    alignItems: "flex-start",
    paddingLeft: 10,
    justifyContent: "center",
    ...Platform.select({
      ios: { maxHeight: 30, paddingBottom: 10 },
      android: {
        maxHeight: 30,
        paddingTop: 0,
        paddingBottom: 0,
        textAlignVertical: "center"
      }
    })
  },
  labelText: {
    marginTop: -10,
    color: colors.blue,
    marginLeft: 10,
    backgroundColor: colors.white,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  }
});

export default NumberInput;
