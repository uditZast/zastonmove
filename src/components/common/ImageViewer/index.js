import React, { Component, Fragment } from "react";
import {
  Alert,
  Dimensions,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../Colors";
import { Icon } from "react-native-elements";
import Swiper from "react-native-swiper";
import Modal from "react-native-modal";
import { DocumentDirectoryPath, downloadFile, exists } from "react-native-fs";

const prevIcon = (
  <Icon
    size={27}
    name="chevron-left-circle"
    type="material-community"
    underlayColor={"transparent"}
    color={colors.white}
  />
);

const nextIcon = (
  <Icon
    size={27}
    name="chevron-right-circle"
    type="material-community"
    underlayColor={"transparent"}
    color={colors.white}
  />
);

class ImageViewer extends Component {
  constructor(props) {
    super(props);
    this.state = { openAlert: false, index: props.index ? props.index : 0 };
  }

  updateParent = () => {
    this.closeAlert();
    const { removeImage } = this.props;
    const { index } = this.state;
    if (removeImage) {
      removeImage(index);
    }
  };

  openAlert = () => {
    this.setState({ openAlert: true });
  };

  closeAlert = () => {
    this.setState({ openAlert: false });
  };

  downloadImage = () => {
    const { url } = this.props;

    downloadFile({
      fromUrl: url,
      toFile: `${DocumentDirectoryPath}`
    }).promise.then(r => {
      console.log("r ----", r);
      console.log("------- check the downloaded image ----");
      console.log(
        "`${DocumentDirectoryPath}/react-native.png` ------------------",
        `${DocumentDirectoryPath}/react-native.png`
      );

      this.setState({ isDone: true });
    });
  };

  downloadAndGetImageUrl(name: string, source_url: string) {
    let fileName = this.getFileName(name);
    return exists(fileName)
      .then(response => {
        if (response) {
          return { uri: fileName };
        } else {
          let destination_path = "/" + name + ".jpg";
          return downloadFile({
            fromUrl: source_url,
            toFile: DocumentDirectoryPath + destination_path
          })
            .promise.then(response => {
              console.log("response ------------------", response);
              return { uri: fileName };
            })
            .catch(error => {
              console.log("error ----------------", error);
              return { uri: source_url };
            });
        }
      })
      .catch(error => {
        console.log("error external----------------", error);
        return { uri: source_url };
      });
  }

  getFileName(name: string) {
    const FILE = Platform.OS === "ios" ? "" : "file://";
    return FILE + DocumentDirectoryPath + "/" + name + ".png";
  }

  render() {
    const { openAlert = false, index } = this.state;
    const { visible, closeModal, showDelete, images, url } = this.props;

    console.log("url ----------------------", url);
    return (
      <Fragment>
        <Modal
          isVisible={visible}
          onBackButtonPress={closeModal}
          onBackdropPress={closeModal}
          animationIn={"slideInUp"}
          style={styles.modalContainer}
        >
          <View style={styles.modalView}>
            <View style={styles.closeContainer}>
              <Icon
                size={30}
                name="close"
                type="material-community"
                underlayColor={"transparent"}
                color={colors.white}
                onPress={closeModal}
              />
            </View>
            <Swiper
              style={styles.wrapper}
              showsButtons={true}
              prevButton={prevIcon}
              nextButton={nextIcon}
              loop={false}
              index={index}
              onIndexChanged={newIndex => this.setState({ index: newIndex })}
            >
              {images.map((image, index) => {
                let width = "100%";
                let height = "100%";
                Image.getSize(
                  image,
                  (srcWidth, srcHeight) => {
                    const maxHeight = Dimensions.get("window").height;
                    const maxWidth = Dimensions.get("window").width;

                    const ratio = Math.min(
                      maxWidth / srcWidth,
                      maxHeight / srcHeight
                    );
                    width = srcWidth * ratio;
                    height = srcHeight * ratio;
                  },
                  error => {
                    console.log("error:", error);
                  }
                );
                return (
                  <View key={`${image}_${index}`} style={styles.slides}>
                    <Image
                      style={{ width: `${width}`, height: `${height}` }}
                      source={{ uri: image }}
                    />
                  </View>
                );
              })}
            </Swiper>

            <TouchableOpacity
              style={styles.deleteContainer}
              onPress={showDelete ? this.openAlert : null}
            >
              {showDelete && (
                <Fragment>
                  <Icon
                    size={30}
                    name="delete"
                    type="material-community"
                    underlayColor={"transparent"}
                    color={"#fc6c85"}
                    onPress={closeModal}
                  />
                  <Text style={styles.text}>DELETE</Text>
                </Fragment>
              )}
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.deleteContainer}
              onPress={showDelete ? this.downloadImage : null}
            >
              {showDelete && (
                <Fragment>
                  <Icon
                    size={30}
                    name="delete"
                    type="material-community"
                    underlayColor={"transparent"}
                    color={"#fc6c85"}
                    onPress={closeModal}
                  />
                  <Text style={styles.text}>DOWNLOAD</Text>
                </Fragment>
              )}
            </TouchableOpacity>
          </View>
        </Modal>
        {openAlert &&
          Alert.alert(
            "Delete Image",
            "Are you sure?",
            [
              {
                text: "Cancel",
                onPress: this.closeAlert,
                style: "cancel"
              },
              { text: "OK", onPress: this.updateParent }
            ],
            { cancelable: false }
          )}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    position: "absolute",
    margin: 0,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  closeContainer: {
    padding: 40,
    paddingRight: 20,
    alignItems: "flex-end",
    justifyContent: "center"
  },
  wrapper: {},
  imageContainer: {
    // width: "100%",
    // maxHeight:"100%",
    // aspectRatio: 1
  },
  slides: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
    // backgroundColor: "#92BBD9"
  },
  modalView: {
    width: "100%",
    flex: 1,
    borderRadius: 4,
    position: "relative",
    backgroundColor: "#2f3b52",
    alignSelf: "center"
  },
  text: {
    color: "#fc6c85",
    fontSize: 16,
    fontWeight: "bold",
    marginLeft: 4
  },
  deleteContainer: {
    flexDirection: "row",
    padding: 40,
    paddingLeft: 20,
    paddingRight: 20,
    alignItems: "center"
  }
});

export default ImageViewer;
