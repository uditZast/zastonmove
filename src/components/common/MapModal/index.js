import React, { Component, Fragment } from "react";
import { StyleSheet, Text, View } from "react-native";
import Modal from "react-native-modal";
import colors from "../Colors";
import MapView, { Marker } from "react-native-maps";
import { Icon } from "react-native-elements";
class MapModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { lat, long, location, closeModal, isVisible = false } = this.props;

    return (
      <Fragment>
        <Modal
          isVisible={isVisible}
          onBackButtonPress={closeModal}
          onBackdropPress={closeModal}
          animationIn={"fadeIn"}
          animationOut={"fadeOut"}
          style={{ margin: 0 }}
        >
          <View style={styles.modalView}>
            <View style={{ flex: 1 }}>
              <View style={{ flexDirection: "row", padding: 8 }}>
                <Icon
                  size={20}
                  name={"map-marker-circle"}
                  type="material-community"
                  containerStyle={{ marginRight: 8 }}
                  color={colors.black65}
                />
                <Text style={{ color: colors.black85 }}>{location}</Text>
              </View>
              <MapView
                ref={ref => {
                  this.map = ref;
                }}
                provider={this.props.provider}
                style={{ flex: 1 }}
                initialRegion={{
                  latitude: lat,
                  longitude: long,
                  latitudeDelta: 0.04,
                  longitudeDelta: 0.04
                }}
              >
                {lat && long && (
                  <Marker
                    coordinate={{
                      latitude: lat,
                      longitude: long
                    }}
                  />
                )}
              </MapView>
            </View>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalView: {
    width: "90%",
    height: "80%",
    borderRadius: 4,
    backgroundColor: colors.white,
    alignSelf: "center"
  },
  searchBarContainer: {
    backgroundColor: colors.white,
    borderWidth: 0,
    padding: 16
  }
});

export default MapModal;
