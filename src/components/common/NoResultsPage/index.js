import React, { Component, Fragment } from "react";
import { Image, View } from "react-native";
import NoResultImage from "../../../images/no_result.jpg";
import colors from "../../../colors/Colors";

class NoResult extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Fragment>
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            alignSelf: "center",
            backgroundColor: colors.white
          }}
        >
          <Image
            source={NoResultImage}
            style={{
              width: "100%",
              height: undefined,
              aspectRatio: 1,
              alignSelf: "center"
            }}
          />
        </View>
      </Fragment>
    );
  }
}

export default NoResult;
