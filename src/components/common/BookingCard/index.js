import React, { Component, Fragment } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import CustomStyles from "../Styles";
import colors from "../Colors";
import { Icon } from "react-native-elements";
import { BOOKING_STATUS, NAVIGATE } from "../../../../constants";
import CardOptionsNew from "../../bookings/bookingactions/CardOptionsNew";
import RejectionReasonsInput from "../../../containers/RejectionReasonsInput";
import AssignModal from "../../../containers/AssignModal";
import BookingHistory from "../../../containers/BookingHistory";
import DatePickerInput from "../../../containers/DatePickerInput";
import moment from "moment";

class BookingCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rejectModal: false,
      showRejectModal: false,
      assignModal: false,
      historyModal: false,
      isVisible: false,
      selectedReason: null,
      showDockModal: false,
      selectedDateTime: new moment(),
      reassignModal: false,
      selectedIndex: 0
    };
  }

  checkReturnBookingType = return_booking_id => {
    const { bookings, vehicleTypes, vehicles, screen } = this.props;
    const { basicInfo: { vehicle_id, vehicle_type_id, code, type } = {} } =
      bookings[return_booking_id] || {};

    const vehicle = vehicles[vehicle_id] || {};
    const vehicleType = vehicleTypes[vehicle_type_id] || {};

    const { basicInfo: { name: vehicleTypeName } = {} } = vehicleType;
    const { basicInfo: { vehicle_number } = {} } = vehicle;

    if (return_booking_id !== undefined && screen === "new_request") {
      return (
        <View style={{ flex: 0.5 }}>
          <Text
            style={{
              fontSize: 14,
              fontWeight: "600",
              color: colors.blue
            }}
          >
            {code} ({type})
          </Text>
        </View>
      );
    }
  };

  renderListActions = () => {
    this.closeRejectLayout();
    this.closeDock();
    this.props.selectIndex(this.props.index);
  };

  visibleRejectModal = () => {
    if (this.state.rejectModal === false) {
      this.setState({
        rejectModal: true,
        showDockModal: false
      });
    } else {
      this.setState({
        rejectModal: false,
        showDockModal: false
      });
    }
  };

  assignModalVisible = () => {
    this.setState({ assignModal: true, isVisible: true });
  };

  reAssignModalVisible = () => {
    this.setState({ reassignModal: true, isVisible: true });
  };

  visibleBookingHistory = () => {
    this.setState({ historyModal: true, isVisible: true });
  };

  closeModal = () => {
    this.setState({ isVisible: false });
  };

  closeAssignModal = async () => {
    this.setState({ reassignModal: false, isVisible: false });
  };

  selectRejectReason = code => {
    this.setState({ selectedReason: code });
  };

  closeRejectLayout = () => {
    console.log("---- close reject modal ----");
    this.setState({ rejectModal: false });
  };

  visibleDockModal = () => {
    if (this.state.showDockModal === false) {
      this.setState({ showDockModal: true, rejectModal: false });
    } else {
      this.setState({ showDockModal: false, rejectModal: false });
    }
  };

  setDate = dateTime => {
    this.setState({ selectedDateTime: dateTime });
  };

  onEditBooking = () => {
    const { screen, booking } = this.props;

    const { basicInfo: { type: bookingType, id } = {} } = booking || {};

    console.log("bookingType onEdit Click----", bookingType);
    if (screen === "new_request") {
      if (bookingType === "AH" || bookingType === "SCH") {
        this.props.navigation.navigate(NAVIGATE.EDIT_BOOKING_ONE_WAY, {
          booking_id: id,
          action: "edit"
        });
      } else if (bookingType === "AHF" || bookingType === "SCHF") {
        this.props.navigation.navigate(NAVIGATE.EDIT_BOOKING_TWO_WAY, {
          booking_id: id,
          action: "edit"
        });
      } else if (bookingType === "DR") {
        this.props.navigation.navigate(NAVIGATE.EDIT_DRY_RUN, {
          booking_id: id,
          action: "edit"
        });
      }
    } else if (screen === "accepted") {
      if (
        bookingType === "AH" ||
        bookingType === "AHF" ||
        bookingType === "AHR" ||
        bookingType === "SCHF" ||
        bookingType === "SCHR" ||
        bookingType === "SCH"
      ) {
        this.props.navigation.navigate(NAVIGATE.EDIT_BOOKING_ONE_WAY, {
          booking_id: id,
          action: "edit"
        });
      } else {
      }
    } else if (screen === "assigned") {
      if (
        bookingType === "AH" ||
        bookingType === "AHF" ||
        bookingType === "AHR" ||
        bookingType === "SCHF" ||
        bookingType === "SCHR" ||
        bookingType === "SCH"
      ) {
        this.props.navigation.navigate(NAVIGATE.EDIT_BOOKING_ONE_WAY, {
          booking_id: id,
          action: "edit"
        });
      } else {
      }
    }
  };

  onAcceptBooking = () => {
    // const {Crashlytics} = Fabric;
    // Crashlytics.crash();

    const { screen, booking } = this.props;
    const { basicInfo: { type: bookingType, id } = {} } = booking || {};

    if (bookingType === "AH" || bookingType === "SCH") {
      this.props.navigation.navigate(NAVIGATE.EDIT_BOOKING_ONE_WAY, {
        booking_id: id,
        action: "accept"
      });
    } else if (bookingType === "AHF" || bookingType === "SCHF") {
      this.props.navigation.navigate(NAVIGATE.EDIT_BOOKING_TWO_WAY, {
        booking_id: id,
        action: "accept"
      });
    } else if (bookingType === "DR") {
      this.props.navigation.navigate(NAVIGATE.EDIT_DRY_RUN, {
        booking_id: id,
        action: "accept"
      });
    } else {
    }
  };

  closeDock = () => {
    this.setState({ showDockModal: false });
  };

  render() {
    const { openBookingHistory } = this;
    const {
      booking,
      customers,
      vendors,
      vehicles,
      vehicleTypes,
      engagedBy,
      trips
    } = this.props;

    const { screen } = this.props;

    const {
      rejectModal = false,
      showRejectModal = false,
      assignModal = false,
      historyModal = false,
      showDockModal = false,
      reassignModal = false
    } = this.state;

    const {
      selectedIndex = 0,
      index = 0,
      statusVisibility = false
    } = this.props;

    const {
      basicInfo: {
        code: bookingCode,
        type,
        reference,
        expected_tat,
        route,
        vehicle_type_id,
        customer_id,
        vendor_id,
        vehicle_id,
        return_booking_id,
        id,
        trip_time,
        engaged_by_id,
        trip_id
      } = {},
      status,
      comments
    } = booking || {};

    const { basicInfo: { vehicle_number } = {} } = vehicles[vehicle_id] || {};
    const { basicInfo: { code: vendor_name, nick_name } = {} } =
      vendors[vendor_id] || {};
    const { basicInfo: { name: vehicleType } = {} } =
      vehicleTypes[vehicle_type_id] || {};
    const { basicInfo: { code: customerName } = {} } =
      customers[customer_id] || {};
    const { basicInfo: { name: engaged_by } = {} } =
      engagedBy[engaged_by_id] || {};

    const { basicInfo: { code: tripCode } = {} } = trips[trip_id] || {};

    return (
      <Fragment>
        <View
          style={{
            flex: 1,
            borderWidth: 1,
            backgroundColor: colors.black02,
            borderColor: colors.black15,
            borderRadius: 6,
            margin: 10,
            marginTop: 0
          }}
        >
          <TouchableOpacity
            style={styles.cardContainer}
            onPress={this.renderListActions}
          >
            <View style={{ flex: 1, padding: 0 }}>
              <View
                style={{
                  padding: 10,
                  flex: 1
                }}
              >
                <View style={styles.headingContainer}>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    {bookingCode && (
                      <View
                        style={return_booking_id ? { flex: 0.5 } : { flex: 1 }}
                      >
                        {screen === "my_bookings" && tripCode ? (
                          <Text
                            style={{
                              fontSize: 14,
                              fontWeight: "600",
                              color: colors.blue
                            }}
                          >
                            {bookingCode} {" / "} {tripCode} ({type})
                          </Text>
                        ) : (
                          <Text
                            style={{
                              fontSize: 14,
                              fontWeight: "600",
                              color: colors.blue
                            }}
                          >
                            {bookingCode} ({type})
                          </Text>
                        )}
                      </View>
                    )}

                    {this.checkReturnBookingType(return_booking_id)}
                  </View>
                </View>
                {vehicle_number && vendor_name ? (
                  <View style={[styles.headingContainer, { paddingTop: 4 }]}>
                    <View style={{ flex: 0.5 }}>
                      <Text
                        style={{
                          color: colors.black85,
                          fontWeight: "600",
                          fontSize: 14
                        }}
                      >
                        {vehicle_number} ({vehicleType})
                      </Text>
                    </View>

                    {/*<View*/}
                    {/*  style={{*/}
                    {/*    flex: 0.5,*/}
                    {/*    alignItems: "center",*/}
                    {/*    justifyContent: "center"*/}
                    {/*  }}*/}
                    {/*>*/}
                    {/*  <Text*/}
                    {/*    style={{*/}
                    {/*      color: colors.black65,*/}
                    {/*      fontWeight: "400",*/}
                    {/*      fontSize: 14*/}
                    {/*    }}*/}
                    {/*  >*/}
                    {/*    Vendor: {vendor_name}*/}
                    {/*  </Text>*/}
                    {/*</View>*/}
                  </View>
                ) : (
                  <View style={[styles.headingContainer, { paddingTop: 4 }]}>
                    <View style={{ flex: 0.5 }}>
                      <Text
                        style={{
                          color: colors.black85,
                          fontWeight: "600",
                          fontSize: 14
                        }}
                      >
                        {vehicleType}
                      </Text>
                    </View>
                  </View>
                )}
                {/*=================LINE====================*/}
                <View style={CustomStyles.dashedLine} />
                {/*=================LINE====================*/}
                <View style={styles.routeContainer}>
                  <View style={{ flexDirection: "row", flex: 1 }}>
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                        flex: 0.5
                      }}
                    >
                      <Icon
                        size={24}
                        name={"map-marker-path"}
                        type="material-community"
                        containerStyle={{ marginRight: 8 }}
                        color={colors.darkblue}
                      />
                      <Text
                        style={{
                          color: colors.darkblue,
                          fontSize: 14,
                          marginRight: 5
                        }}
                      >
                        {route}
                      </Text>
                    </View>

                    <View
                      style={{
                        flex: 0.5,
                        flexDirection: "row",
                        alignItems: "flex-start",
                        justifyContent: "center"
                      }}
                    >
                      {customerName && (
                        <Text
                          style={{
                            fontSize: 14,
                            color: colors.black65
                          }}
                        >
                          Cust : {customerName}
                        </Text>
                      )}
                      {!customerName && (
                        <Text
                          style={{
                            fontSize: 14,
                            fontWeight: "500",
                            color: colors.black65
                          }}
                        >
                          Dry Run
                        </Text>
                      )}
                    </View>
                  </View>

                  <View style={{ flex: 1, marginTop: 10 }}>
                    <Text
                      style={{ color: colors.black65, fontSize: 14 }}
                      numberOfLines={1}
                    >
                      Scheduled Time: {new moment(trip_time).format("LLL")}
                    </Text>
                  </View>

                  {nick_name && (
                    <View
                      style={{
                        flex: 1,
                        justifyContent: "center",
                        marginTop: 10
                      }}
                    >
                      <Text
                        style={{
                          color: colors.black65,
                          fontWeight: "400",
                          fontSize: 14
                        }}
                      >
                        Vendor: {nick_name}
                      </Text>
                    </View>
                  )}

                  {selectedIndex === index && (
                    <View>
                      {reference && (
                        <Text
                          style={{
                            color: colors.black65,
                            fontSize: 14,
                            marginTop: 10
                          }}
                          numberOfLines={1}
                        >
                          Reference: {reference}
                        </Text>
                      )}
                      {engaged_by && (
                        <Text
                          style={{
                            color: colors.black65,
                            fontSize: 14,
                            marginTop: 10
                          }}
                          numberOfLines={1}
                        >
                          Vehicle Engaged By: {engaged_by}
                        </Text>
                      )}
                    </View>
                  )}

                  <View style={{ flexDirection: "row" }}>
                    <View style={{ flex: 1, marginTop: 10 }}>
                      {expected_tat !== 0 && (
                        <Text
                          style={{ color: colors.black65, fontSize: 14 }}
                          numberOfLines={1}
                        >
                          Expected Tat: {expected_tat}
                        </Text>
                      )}
                    </View>

                    {statusVisibility ? (
                      <View
                        style={{
                          flex: 1,
                          marginTop: 10,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        {status && (
                          <Text
                            style={{
                              color: colors.black85,
                              fontWeight: "600",
                              fontSize: 14
                            }}
                          >
                            {BOOKING_STATUS[status]}
                          </Text>
                        )}
                      </View>
                    ) : null}
                  </View>
                </View>
              </View>
              {selectedIndex === index && (
                <CardOptionsNew
                  onAcceptTap={this.onAcceptBooking}
                  onEditTap={this.onEditBooking}
                  onRejectTap={this.visibleRejectModal}
                  onHistoryTap={this.visibleBookingHistory}
                  screen={this.props.screen}
                  change="1"
                  onAssignTap={this.assignModalVisible}
                  id={this.props.id}
                  onReAssignTap={this.reAssignModalVisible}
                  onDockTap={this.visibleDockModal}
                  {...this.props}
                />
              )}
            </View>
          </TouchableOpacity>

          {rejectModal &&
            showDockModal === false &&
            selectedIndex === index && (
              <RejectionReasonsInput
                visible={this.closeRejectLayout}
                bookingId={id}
                rejectReasons={this.props.rejectReasons}
                selectedReason={this.state.selectedReason}
                selectReason={this.selectRejectReason}
              />
            )}

          {showDockModal &&
            rejectModal === false &&
            selectedIndex === index && (
              <DatePickerInput
                close={this.closeDock}
                bookingId={id}
                message={"Select Dock Time"}
                selectedDateTime={this.state.selectedDateTime}
                selectDateTime={this.setDate}
              />
            )}
        </View>

        {assignModal && (
          <AssignModal
            bookingCode={bookingCode}
            bookingId={id}
            isVisible={this.state.isVisible}
            vehicles={this.props.vehicles}
            vendors={this.props.vendors}
            vehicleTypes={this.props.vehicleTypes}
            closeModal={this.closeAssignModal}
            text={"Assign Vehicle"}
            apiFlag={0}
          />
        )}

        {reassignModal && (
          <AssignModal
            bookingCode={bookingCode}
            bookingId={id}
            isVisible={this.state.isVisible}
            vehicles={this.props.vehicles}
            vendors={this.props.vendors}
            vehicleTypes={this.props.vehicleTypes}
            closeModal={this.closeAssignModal}
            text={"Re-Assign Vehicle"}
            apiFlag={1}
          />
        )}

        {historyModal && (
          <BookingHistory
            bookingId={id}
            tripId={null}
            isVisible={this.state.isVisible}
            closeModal={this.closeModal}
          />
        )}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    flexDirection: "row"
  },
  headingContainer: {
    flex: 1,
    paddingBottom: 4,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start"
  },
  routeContainer: {
    // flexDirection: "row",
    // alignItems: "center",
    // justifyContent: "space-between",
    paddingTop: 4,
    paddingBottom: 4
  },
  addressContainer: {
    flexDirection: "row",
    alignItems: "flex-start",
    paddingTop: 4
  }
});

export default BookingCard;
