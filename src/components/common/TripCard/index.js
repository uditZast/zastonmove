import React, { Component, Fragment } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import CustomStyles from "../Styles";
import colors from "../Colors";
import { Icon } from "react-native-elements";
import { NAVIGATE, TRIP_STATUS_TABLE_NAME } from "../../../../constants";
import { callNumber } from "../callNumber";
import moment from "moment";
import CardOptionsNew from "../../bookings/bookingactions/CardOptionsNew";
import TripCommentsSearch from "../../../containers/TripCommentsSearch";
import TripHistorySearch from "../../../containers/TripHistorySearch";

class TripCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      historyModal: false,
      commentModal: false,
      isVisibleHistory: false,
      isVisibleComment: false
    };
  }

  componentDidMount() {}

  goToTripDetails = () => {
    const { trip } = this.props;
    const { basicInfo: { id } = {} } = trip;
    this.props.navigation.navigate(NAVIGATE.TRIP_DETAILS, { trip_id: id });
  };

  makeCall = () => {
    const { trip } = this.props;
    const { basicInfo: { phone } = {} } = trip || {};
    if (phone) {
      callNumber(phone);
    }
  };

  visibleTripHistory = () => {
    this.setState({ historyModal: true, isVisibleHistory: true });
  };

  visibleTripComment = () => {
    this.setState({ commentModal: true, isVisibleComment: true });
  };

  closeModal = () => {
    if (this.state.isVisibleHistory) {
      this.setState({ isVisibleHistory: false });
    } else {
      this.setState({ isVisibleComment: false });
    }
  };

  renderListActions = () => {
    this.props.selectIndex(this.props.index);
  };

  render() {
    const { goToTripDetails } = this;
    const {
      trip,
      customers,
      vendors,
      vehicles,
      showStatus = false
    } = this.props;
    const { index, selectedIndex, actionsVisiblity = false } = this.props;

    const { commentModal = false, historyModal = false } = this.state;

    const {
      basicInfo: {
        vehicle_id,
        vendor_id,
        code,
        customer_id,
        route,
        location,
        phone,
        recorded_at,
        id,
        trip_comment_id,
        trip_history_id
      } = {},
      status
    } = trip || {};

    const recordedLast = moment(recorded_at);

    const { basicInfo: { vehicle_number } = {} } = vehicles[vehicle_id] || {};
    const { basicInfo: { code: vendor_name, nick_name } = {} } =
      vendors[vendor_id] || {};
    const { basicInfo: { code: customerName } = {} } =
      customers[customer_id] || {};

    return (
      <Fragment>
        <TouchableOpacity
          style={styles.cardContainer}
          onPress={actionsVisiblity ? this.renderListActions : goToTripDetails}
          // onPress={goToTripDetails}
        >
          <View style={{ flex: 1 }}>
            <View style={styles.headingContainer}>
              <View style={{ flex: 1 }}>
                {vehicle_number && (
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "600",
                      color: colors.blue
                    }}
                  >
                    {vehicle_number}
                  </Text>
                )}
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                {customerName && (
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "500",
                      color: colors.black65
                    }}
                  >
                    Customer : {customerName}
                  </Text>
                )}

                {!customerName && (
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "500",
                      color: colors.black65
                    }}
                  >
                    Dry Run
                  </Text>
                )}
              </View>
            </View>
            <View style={[styles.headingContainer, { paddingTop: 4 }]}>
              <View style={{ flex: 1 }}>
                <Text
                  style={{
                    color: colors.black85,
                    fontWeight: "600",
                    fontSize: 14
                  }}
                >
                  {code}
                </Text>
              </View>
              {/*<View style={{flex: 1}}>*/}
              {/*  {vendor_name && (*/}
              {/*    <Text*/}
              {/*      style={{*/}
              {/*        color: colors.black65,*/}
              {/*        fontWeight: "500",*/}
              {/*        fontSize: 14*/}
              {/*      }}*/}
              {/*    >*/}
              {/*      Vendor: {vendor_name}*/}
              {/*    </Text>*/}
              {/*  )}*/}
              {/*</View>*/}
            </View>
            {/*=================LINE====================*/}
            <View style={CustomStyles.dashedLine} />
            {/*=================LINE====================*/}
            <View style={styles.routeContainer}>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingBottom: 4
                }}
              >
                <Icon
                  size={24}
                  name={"map-marker-path"}
                  type="material-community"
                  containerStyle={{ marginRight: 8 }}
                  color={colors.darkblue}
                />
                <Text style={{ color: colors.darkblue, fontSize: 14 }}>
                  {route}
                </Text>
              </View>

              {phone && (
                <TouchableOpacity
                  style={{
                    alignSelf: "flex-start",
                    flexDirection: "row",
                    alignItems: "center",
                    paddingTop: 4,
                    paddingBottom: 4
                  }}
                  onPress={this.makeCall}
                >
                  <Icon
                    size={20}
                    name={"phone-in-talk"}
                    type="material-community"
                    containerStyle={{ marginRight: 10 }}
                    color={colors.darkblue}
                  />
                  <Text
                    style={{ color: colors.darkblue, fontSize: 14 }}
                    numberOfLines={1}
                  >
                    {phone}
                  </Text>
                </TouchableOpacity>
              )}

              {nick_name && (
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    paddingBottom: 4
                  }}
                >
                  <Icon
                    size={24}
                    name={"account"}
                    type="material-community"
                    containerStyle={{ marginRight: 8 }}
                    color={colors.darkblue}
                  />
                  <Text
                    style={{
                      color: colors.black65,
                      fontWeight: "500",
                      fontSize: 14
                    }}
                  >
                    {nick_name}
                  </Text>
                </View>
              )}
            </View>
            {location && (
              <View style={styles.addressContainer}>
                <Icon
                  size={20}
                  name={"map-marker-circle"}
                  type="material-community"
                  containerStyle={{ marginRight: 8 }}
                  color={colors.black65}
                />

                <Text
                  style={{
                    color: colors.black65,
                    fontSize: 13,
                    flexWrap: "wrap",
                    flexShrink: 1
                  }}
                >
                  {location}
                  {recordedLast.isValid() && (
                    <Text
                      style={{
                        color: new moment(recordedLast)
                          .add(10, "minutes")
                          .isAfter(new moment())
                          ? colors.darkGreen
                          : colors.red
                      }}
                    >
                      {" "}
                      ({recordedLast.fromNow()})
                    </Text>
                  )}
                </Text>
              </View>
            )}

            {selectedIndex === index && actionsVisiblity && (
              <CardOptionsNew
                change="1"
                screen={"trip_search"}
                id={this.props.id}
                onHistoryTap={this.visibleTripHistory}
                onCommentTap={this.visibleTripComment}
                {...this.props}
              />
            )}
          </View>
        </TouchableOpacity>

        {commentModal ? (
          <TripCommentsSearch
            trip_id={id}
            trip_comment_id={trip_comment_id}
            isVisible={this.state.isVisibleComment}
            closeModal={this.closeModal}
          />
        ) : null}
        {historyModal ? (
          <TripHistorySearch
            trip_id={id}
            trip_history_id={trip_history_id}
            isVisible={this.state.isVisibleHistory}
            closeModal={this.closeModal}
          />
        ) : null}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    margin: 10,
    marginTop: 0,
    padding: 10,
    flexDirection: "row",
    borderWidth: 1,
    backgroundColor: colors.black02,
    borderColor: colors.black15,
    borderRadius: 6
  },
  headingContainer: {
    flex: 1,
    paddingBottom: 4,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start"
  },
  routeContainer: {
    // flexDirection: "row",
    // alignItems: "center",
    // justifyContent: "space-between",
    paddingTop: 4,
    paddingBottom: 4
  },
  addressContainer: {
    flexDirection: "row",
    alignItems: "flex-start",
    paddingTop: 4
  }
});

export default TripCard;
