import React, { Component } from "react";
import { Platform, Text, TouchableOpacity, View } from "react-native";
import { Button, Icon } from "react-native-elements";
import VehicleInput from "../VehicleInput";
import NumberInput from "../NumberInput";
import colors from "../../../colors/Colors";
import VendorsInput from "../VendorsInput";
import { VEHICLE_CATEGORY } from "../../../../constants";
import Modal from "react-native-modal";
import Snackbar from "react-native-snackbar";

class AssignModal extends Component {
  state = {
    selectedVendor: 0,
    selectedVehicle: 0,
    isLoading: false,
    isVisible: false,
    driverPhoneNumber: null,
    vendorDisabled: false,
    showModal: false,
    showError: false
  };

  closeModal = () => {
    this.setState({ isVisible: false, input: null });
  };

  selectVehicle = vehicle => {
    const { vehicles } = this.props;
    const { basicInfo: { vehicle_type_id, vehicle_category, vendor_id } = {} } =
      vehicles[vehicle] || {};
    if (VEHICLE_CATEGORY.CON === vehicle_category) {
      this.setState({ selectedVendor: vendor_id, vendorDisabled: true });
    } else {
      this.setState({ selectedVendor: 0, vendorDisabled: false });
    }
    this.setState({
      selectedVehicle: vehicle,
      selectedVehicleType: vehicle_type_id
    });
  };

  selectVendor = vendor => {
    this.setState({ selectedVendor: vendor });
  };

  updateDriverNumber = phoneNumber => {
    this.setState({ driverPhoneNumber: phoneNumber });
  };

  submitData = () => {
    this.setState({ isLoading: true });

    const { bookingId, closeModal, apiFlag } = this.props;
    const { selectedVehicle, selectedVendor, driverPhoneNumber } = this.state;

    if (selectedVehicle === 0) {
      this.setState({ isLoading: false });
      Snackbar.show({
        title: "Select Vehicle",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (selectedVendor === 0) {
      this.setState({ isLoading: false });
      Snackbar.show({
        title: "Select Vendor",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (driverPhoneNumber === null) {
      this.setState({ isLoading: false });
      Snackbar.show({
        title: "Add Driver Phone Number",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      const requestData = {
        booking_id: bookingId,
        vehicle_id: selectedVehicle,
        vendor_id: selectedVendor,
        phone: driverPhoneNumber
      };

      const { assignVehicle, reassignVehicle } = this.props;

      if (apiFlag === 0) {
        assignVehicle(requestData)
          .then(response => {
            if (response.status === true) {
              this.setState({ isLoading: false });
              closeModal().then(
                Snackbar.show({
                  title: "" + response.message,
                  duration: Snackbar.LENGTH_LONG
                })
              );
            } else {
              this.setState({
                isLoading: false,
                showError: true,
                errorMessage: response.message
              });
            }
          })
          .catch(error => {
            console.log("error===============" + error);
          });
      } else {
        reassignVehicle(requestData)
          .then(response => {
            if (response.status === true) {
              this.setState({ isLoading: false });
              closeModal().then(
                Snackbar.show({
                  title: "" + response.message,
                  duration: Snackbar.LENGTH_LONG
                })
              );
            } else {
              this.setState({
                isLoading: false,
                showError: true,
                errorMessage: response.message
              });
            }
          })
          .catch(error => {
            console.log("error===============" + error);
          });
      }
    }
  };

  close = () => {
    const { closeModal } = this.props;
    this.setState({ showError: false, errorMessage: null });
    closeModal();
  };

  render() {
    const { bookingCode, closeModal, isVisible, text } = this.props;

    return (
      <Modal
        isVisible={isVisible}
        onBackButtonPress={this.close}
        onBackdropPress={this.close}
      >
        <View style={styles.modalOuterStyle}>
          <View style={styles.modalInnerStyle}>
            <View>
              <TouchableOpacity
                style={styles.imageStyle}
                hitSlop={{ top: 30, bottom: 30, left: 70, right: 70 }}
                onPress={this.close}
              >
                <Icon
                  name="cancel"
                  type="material"
                  size={25}
                  color={colors.blueColor}
                />
              </TouchableOpacity>

              <Text style={styles.textStyle}>{bookingCode}</Text>

              <VehicleInput
                vehicles={this.props.vehicles}
                vehicleTypes={this.props.vehicleTypes}
                selectedVehicle={this.state.selectedVehicle}
                selectVehicle={this.selectVehicle}
              />
              <VendorsInput
                vendors={this.props.vendors}
                selectedVendor={this.state.selectedVendor}
                selectVendor={this.selectVendor}
                disabled={
                  !this.state.selectedVehicle || this.state.vendorDisabled
                }
              />

              <NumberInput
                numericProp={true}
                message={"Driver Phone"}
                updateText={this.updateDriverNumber}
                placeholder={"Enter Driver Phone Number"}
              />

              <Button
                onPress={this.submitData}
                title={text}
                buttonStyle={{ margin: 10 }}
                loading={this.state.isLoading}
              />

              {this.state.showError && (
                <Text
                  style={{
                    fontSize: 12,
                    color: colors.redColor,
                    padding: 10,
                    justifyContent: "center",
                    alignSelf: "center"
                  }}
                >
                  {this.state.errorMessage}
                </Text>
              )}
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = {
  imageStyle: {
    width: 23,
    height: 22,
    marginTop: -18,
    marginLeft: -18
  },
  customerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    paddingLeft: 5,
    paddingRight: 5,
    alignItems: "flex-start"
  },
  phcustomerStyle: {
    marginTop: -10,
    color: colors.dark_grey,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  buttonStyle: {
    color: colors.white,
    backgroundColor: colors.blueColor,
    fontSize: 15,
    justifyContent: "flex-end",
    marginTop: 20,
    marginLeft: 2,
    marginRight: 2,
    marginBottom: 10,
    textAlign: "center",
    padding: 10
  },
  customerStyleInput: {
    fontSize: 12,
    color: colors.border_grey,
    padding: 5,
    marginLeft: 5,
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center"
  },
  selectedTextColor: {
    fontSize: 12,
    color: colors.dark_grey,
    marginLeft: 5,
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
    fontFamily: "CircularStd-Book",
    ...Platform.select({
      ios: {
        padding: 5,
        overflow: "hidden"
      },
      android: {
        padding: 10
      }
    })
  },
  modalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0)"
  },
  modalInnerStyle: {
    backgroundColor: "#FFF",
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    padding: 10,
    width: "90%"
  },
  BorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    flex: 1,
    paddingLeft: 5,
    paddingRight: 5
  },
  StyleInput: {
    fontSize: 12,
    color: colors.dark_grey,
    paddingLeft: 5,
    marginLeft: 5,
    width: "100%",
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center",
    ...Platform.select({
      ios: {
        padding: 5,
        overflow: "hidden"
      },
      android: {
        padding: 0
      }
    })
  },
  textStyle: {
    fontSize: 13,
    color: colors.black,
    textAlign: "center",
    marginBottom: 10,
    fontFamily: "CircularStd-Book"
  }
};

// const mapStateToProps = state => ({
//     state: state,
//     error: state.error,
//     products: state.productsReducer.products,
//     pending: state.productsReducer.pending,
// });
//
//
// export default connect(
//     mapStateToProps,
//     null
// )(AssignModal);

export default AssignModal;
