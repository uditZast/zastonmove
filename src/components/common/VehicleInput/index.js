import React, { Component, Fragment } from "react";
import {
  FlatList,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import Modal from "react-native-modal";
import CustomStyles from "../Styles";
import colors from "../Colors";
import { SearchBar } from "react-native-elements";

class VehicleInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      input: null
    };
  }

  setVehicle = id => () => {
    console.log("id selectedVehicle input----", id);
    const { selectVehicle } = this.props;
    selectVehicle(id);
    this.closeModal();
  };

  openModal = () => {
    this.setState({ isVisible: true });
  };

  closeModal = () => {
    this.setState({ isVisible: false, input: null });
  };

  setSearch = text => {
    this.setState({ input: text });
  };

  clearInput = () => {
    this.setState({ input: null });
  };

  renderVehicle = ({ index, item: vehicle_id }) => {
    const { vehicles, vehicleTypes } = this.props;
    const { basicInfo: { vehicle_number, vehicle_type_id } = {} } =
      vehicles[vehicle_id] || {};
    const { basicInfo: { name } = {} } = vehicleTypes[vehicle_type_id] || {};
    return (
      <Fragment key={index}>
        <TouchableOpacity
          style={{
            paddingTop: 10,
            paddingBottom: 10,
            flexDirection: "row",
            borderWidth: 1,
            borderColor: colors.white,
            borderBottomColor: colors.grey
          }}
          onPress={this.setVehicle(vehicle_id)}
        >
          <View style={{ flex: 1 }}>
            <Text style={{ color: colors.blue }}>{name}</Text>
          </View>
          <Text style={{ color: colors.blue }}>{":"}</Text>
          <View style={{ flex: 4, paddingLeft: 16 }}>
            <Text>{vehicle_number}</Text>
          </View>
        </TouchableOpacity>
      </Fragment>
    );
  };

  renderVehicleList = () => {
    const { vehicles } = this.props;
    let list = Object.keys(vehicles);
    const { input } = this.state;
    if (input && input.toUpperCase()) {
      const u_input = input.toUpperCase();
      list = list.filter(vehicle_id => {
        const { basicInfo: { vehicle_number = "", code, type } = {} } =
          vehicles[vehicle_id] || {};
        return vehicle_number.toUpperCase().includes(u_input);
      });
    }
    return (
      <FlatList
        data={[...list]}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderVehicle}
        keyboardShouldPersistTaps={"handled"}
        showsVerticalScrollIndicator={false}
      />
    );
  };

  render() {
    const { closeModal, openModal } = this;
    const { selectedVehicle = 0, vehicles, disabled = false } = this.props;
    const { basicInfo: { vehicle_number = "Select Vehicle", id } = {} } =
      vehicles[selectedVehicle] || {};

    const { isVisible } = this.state;

    return (
      <Fragment>
        <TouchableOpacity
          style={disabled ? CustomStyles.borderDisabled : CustomStyles.border}
          onPress={disabled ? null : openModal}
        >
          <Text style={CustomStyles.labelText}>Vehicle</Text>
          <View
            style={{
              ...Platform.select({
                ios: CustomStyles.pickerView,
                android: {
                  flexGrow: 1,
                  justifyContent: "center",
                  paddingLeft: 10
                }
              })
            }}
          >
            <Text style={id ? {} : { color: colors.black25 }}>
              {vehicle_number}
            </Text>
          </View>
        </TouchableOpacity>
        <Modal
          isVisible={isVisible}
          onBackButtonPress={closeModal}
          onBackdropPress={closeModal}
          animationIn={"slideInUp"}
          // coverScreen={true}
          style={{
            position: "absolute",
            margin: 0,
            top: 95,
            bottom: 0,
            left: 0,
            right: 0
          }}
          //onSwipeComplete={closeModal}
          //swipeDirection="down"
        >
          <View style={styles.modalView}>
            <SearchBar
              placeholder="Search Vehicles..."
              onChangeText={this.setSearch}
              lightTheme
              containerStyle={styles.searchBarContainer}
              value={this.state.input}
              onCancel={this.clearInput}
              onClear={this.clearInput}
            />
            <View style={{ padding: 16, paddingTop: 0, flex: 1 }}>
              {this.renderVehicleList()}
            </View>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalView: {
    width: "100%",
    flex: 1,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    alignSelf: "center",
    paddingBottom: 30
  },
  searchBarContainer: {
    backgroundColor: colors.white,
    borderWidth: 0,
    padding: 16
  }
});

export default VehicleInput;
