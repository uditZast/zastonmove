import React, { Component, Fragment } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import Modal from "react-native-modal";
import CustomStyles from "../Styles";
import colors from "../Colors";
import { SearchBar } from "react-native-elements";

class CustomerInputNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      input: null
    };
  }

  setCustomer = id => () => {
    const { selectCustomer } = this.props;
    selectCustomer(id);
    this.closeModal();
  };

  openModal = () => {
    this.setState({ isVisible: true });
  };

  closeModal = () => {
    this.setState({ isVisible: false, input: null });
  };

  setSearch = text => {
    this.setState({ input: text });
  };

  clearInput = () => {
    this.setState({ input: null });
  };

  renderCustomer = ({ index, item: customer_id }) => {
    const { customers } = this.props;
    const { basicInfo: { name, code } = {} } = customers[customer_id] || {};
    return (
      <Fragment key={index}>
        <TouchableOpacity
          style={{
            paddingTop: 10,
            paddingBottom: 10,
            flexDirection: "row",
            borderWidth: 1,
            borderColor: colors.white,
            borderBottomColor: colors.grey
          }}
          onPress={this.setCustomer(customer_id)}
        >
          <View style={{ flex: 1 }}>
            <Text style={{ color: colors.blue }}>{code}</Text>
          </View>
          <Text style={{ color: colors.blue }}>{":"}</Text>
          <View style={{ flex: 4, paddingLeft: 16 }}>
            <Text>{name}</Text>
          </View>
        </TouchableOpacity>
      </Fragment>
    );
  };

  renderCustomerList = () => {
    const { customers } = this.props;
    let list = Object.keys(customers);
    const { input } = this.state;

    console.log("input ----", input);

    if (input && input.toUpperCase()) {
      const u_input = input.toUpperCase();
      list = list.filter(customer_id => {
        const { basicInfo: { name = "", code, type } = {} } =
          customers[customer_id] || {};
        return (
          name.toUpperCase().includes(u_input) ||
          code.toUpperCase().includes(u_input) ||
          type.toUpperCase().includes(u_input)
        );
      });
    }
    return (
      <FlatList
        data={[...list]}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderCustomer}
        keyboardShouldPersistTaps={"handled"}
        showsVerticalScrollIndicator={false}
      />
    );
  };

  render() {
    const { closeModal, openModal } = this;
    const { selectedCustomer = 0, customers, disabled } = this.props;
    const { basicInfo: { name = "Select Customer", id } = {} } =
      customers[selectedCustomer] || {};
    const { isVisible } = this.state;

    return (
      <Fragment>
        <TouchableOpacity
          style={disabled ? CustomStyles.borderDisabled : CustomStyles.border}
          onPress={disabled ? null : openModal}
        >
          <Text style={CustomStyles.labelText}>Customer</Text>
          <View style={CustomStyles.pickerView}>
            <Text style={id ? {} : { color: colors.black25 }}>{name}</Text>
          </View>
        </TouchableOpacity>
        <Modal
          isVisible={isVisible}
          onBackButtonPress={closeModal}
          onBackdropPress={closeModal}
          animationIn={"slideInUp"}
          // coverScreen={true}
          style={{
            position: "absolute",
            margin: 0,
            top: 95,
            bottom: 0,
            left: 0,
            right: 0
          }}
          //onSwipeComplete={closeModal}
          //swipeDirection="down"
        >
          <View style={styles.modalView}>
            <SearchBar
              placeholder="Search Customers..."
              onChangeText={this.setSearch}
              lightTheme
              containerStyle={styles.searchBarContainer}
              value={this.state.input}
              onCancel={this.clearInput}
              onClear={this.clearInput}
            />
            <View style={{ padding: 16, paddingTop: 0, flex: 1 }}>
              {this.renderCustomerList()}
            </View>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalView: {
    width: "100%",
    flex: 1,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    alignSelf: "center",
    paddingBottom: 30
  },
  searchBarContainer: {
    backgroundColor: colors.white,
    borderWidth: 0,
    padding: 16
  }
});

export default CustomerInputNew;
