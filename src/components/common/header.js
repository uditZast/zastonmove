import React from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { Icon } from "react-native-elements";
import colors from "./Colors";
import { NAVIGATE } from "../../../constants";

export default ({
  name,
  goBack,
  menu,
  navigation,
  iconVisible = false,
  mapIconVisible = false
}) => {
  const toggleDrawer = () => {
    if (navigation && navigation.toggleDrawer) {
      navigation.toggleDrawer();
    }
  };

  const goToPreviousScreen = () => {
    if (navigation && navigation.goBack) {
      navigation.goBack();
    }
  };

  const goToAvailableListScreen = () => {
    navigation.navigate(NAVIGATE.AVAILABLE_VEHICLES);
  };

  const goToDashboard = () => {
    navigation.navigate(NAVIGATE.DASHBOARD);
  };

  return (
    <View
      style={{
        top: 0,
        height: 50,
        alignItems: "center",
        justifyContent: "space-between",
        flexDirection: "row",
        backgroundColor: colors.blue
      }}
    >
      <TouchableOpacity
        style={{
          left: 16,
          justifyContent: "flex-start",
          width: 27,
          height: 27
        }}
        onPress={goBack ? goToPreviousScreen : toggleDrawer}
      >
        <Icon
          name={goBack ? "arrow-back" : "menu"}
          size={27}
          color={colors.white}
        />
      </TouchableOpacity>
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <Text style={{ fontSize: 16, fontWeight: "500", color: colors.white }}>
          {name}
        </Text>
      </View>
      <View
        style={{ right: 16, justifyContent: "center", width: 27, height: 27 }}
      />

      {iconVisible && (
        <View style={{ paddingRight: 10 }}>
          <TouchableOpacity
            onPress={iconVisible ? goToAvailableListScreen : null}
          >
            <Icon
              name={"format-list-bulleted"}
              type={"material-community"}
              size={25}
              color={colors.white}
            />
          </TouchableOpacity>
        </View>
      )}

      {mapIconVisible && (
        <View style={{ paddingRight: 10 }}>
          <TouchableOpacity onPress={mapIconVisible ? goToDashboard : null}>
            <Icon
              name={"google-maps"}
              type={"material-community"}
              size={25}
              color={colors.white}
            />
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};
