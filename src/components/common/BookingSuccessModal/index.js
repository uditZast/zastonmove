import React, { Component, Fragment } from "react";
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import Modal from "react-native-modal";
import { Icon } from "react-native-elements";
// import LottieView from "lottie-react-native";
import colors from "../Colors";
// import SuccessIcon from "../../../images/success.gif";

class CustomerInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      input: null
    };
  }

  render() {
    const { isVisible, message, closeModal } = this.props;

    return (
      <Fragment>
        <Modal
          isVisible={isVisible}
          onBackButtonPress={closeModal}
          onBackdropPress={closeModal}
          animationIn={"slideInUp"}
          style={{
            margin: 0
          }}
        >
          <TouchableOpacity style={styles.modalView} onPress={closeModal}>
            {/*<LottieView*/}
            {/*  ref={animation => {*/}
            {/*    this.animation = animation;*/}
            {/*  }}*/}
            {/*  source={require("./success.json")}*/}
            {/*/>*/}
            <Icon
              name="check"
              type="evilicon"
              color={colors.greenishBlue}
              size={80}
            />
            <View style={{ paddingTop: 4 }}>
              <Text style={{ color: colors.greenishBlue }}>{message}</Text>
            </View>
          </TouchableOpacity>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalView: {
    // height: 300,
    // height: 300,
    width: "80%",
    aspectRatio: 4 / 2,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  searchBarContainer: {
    backgroundColor: colors.white,
    borderWidth: 0,
    padding: 16
  }
});

export default CustomerInput;
