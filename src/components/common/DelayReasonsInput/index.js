import React, { Component, Fragment } from "react";
import { Platform, Text } from "react-native";
import SelectInput from "react-native-select-input-ios";
import customStyles from "../Styles";
import { View } from "react-native-animatable";
import colors from "../Colors";

class DelayReasonsInput extends Component {
  constructor(props) {
    super(props);
  }

  getDelayReasons = () => {
    const { delayReasons } = this.props;
    const options = [{ label: "Select Delay Reason", value: 0 }];
    const delayReasonsArray = Object.keys(delayReasons);
    for (const delayReasonId of delayReasonsArray) {
      const { code, value } = delayReasons[delayReasonId] || {};
      options.push({
        label: value,
        value: code
      });
    }
    return options;
  };

  selectDelayReason = id => {
    const { setDelayReason } = this.props;
    setDelayReason(id);
  };

  render() {
    const { getDelayReasons, selectDelayReason } = this;
    const { selectedDelayReason = 0 } = this.props;

    return (
      <Fragment>
        <View style={customStyles.border}>
          <Text style={customStyles.labelText}>Select Delay Reason</Text>
          <SelectInput
            style={Platform.OS === "ios" ? customStyles.pickerView : null}
            options={getDelayReasons()}
            mode={"dropdown"}
            labelStyle={{
              color: selectedDelayReason === 0 ? colors.black25 : colors.black,
              ...Platform.select({
                android: {
                  height: 30
                },
                ios: {
                  alignSelf: "flex-start"
                }
              })
            }}
            value={selectedDelayReason}
            onSubmitEditing={selectDelayReason}
          />
        </View>
      </Fragment>
    );
  }
}

export default DelayReasonsInput;
