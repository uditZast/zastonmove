import React, { Component, Fragment } from "react";
import { Icon } from "react-native-elements";
import colors from "../Colors";

class CustomSliderMarkerLeft extends Component {
  setCurrentValue = () => {
    const { setValue, currentValue } = this.props;
    setValue(currentValue);
  };

  render() {
    const { currentValue } = this.props;

    return (
      <Fragment>
        <Icon
          name="truck"
          type="material-community"
          size={25}
          color={colors.blue}
        />
      </Fragment>
    );
  }
}

export default CustomSliderMarkerLeft;
