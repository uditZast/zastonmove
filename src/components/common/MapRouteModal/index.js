import React, { Component, Fragment } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import Modal from "react-native-modal";
import colors from "../Colors";
import MapView, { Marker, Polyline } from "react-native-maps";
import { Icon } from "react-native-elements";
import bluePng from "../../../images/blue-color.png";

const BluePng = <Image source={bluePng} />;

class MapRouteModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      plotList = [],
      location,
      closeModal,
      isVisible = false
    } = this.props;

    return (
      <Fragment>
        <Modal
          isVisible={isVisible}
          onBackButtonPress={closeModal}
          onBackdropPress={closeModal}
          animationIn={"fadeIn"}
          animationOut={"fadeOut"}
          style={{ margin: 0 }}
        >
          <View style={styles.modalView}>
            <View style={{ flex: 1 }}>
              <MapView
                // ref={ref => {
                //   this.map = ref;
                // }}
                initialRegion={{
                  latitude: 20.5937,
                  longitude: 78.9629,
                  latitudeDelta: 20,
                  longitudeDelta: 20
                }}
                provider={this.props.provider}
                style={{ flex: 1 }}
              >
                <Polyline
                  coordinates={plotList}
                  strokeColor={colors.red} // fallback for when `strokeColors` is not supported by the map-provider
                  strokeWidth={2}
                />
                {plotList.length > 0 &&
                  plotList.map((coordinates, index) => {
                    const { latitude, longitude } = coordinates;
                    return (
                      <Fragment key={index}>
                        {index === 0 && (
                          <Marker
                            pinColor={colors.red}
                            coordinate={{
                              latitude,
                              longitude
                            }}
                          />
                        )}
                        {index === plotList.length - 1 && (
                          <Marker
                            pinColor={colors.blue}
                            coordinate={{
                              latitude,
                              longitude
                            }}
                          />
                        )}
                        {index !== 0 && index !== plotList.length - 1 && (
                          <Marker
                            anchor={{ x: 0.5, y: 0.5 }}
                            pinColor={colors.black85}
                            image={bluePng}
                            coordinate={{
                              latitude,
                              longitude
                            }}
                          />
                        )}
                      </Fragment>
                    );
                  })}
              </MapView>
            </View>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalView: {
    width: "90%",
    height: "80%",
    borderRadius: 4,
    backgroundColor: colors.white,
    alignSelf: "center"
  },
  searchBarContainer: {
    backgroundColor: colors.white,
    borderWidth: 0,
    padding: 16
  }
});

export default MapRouteModal;
