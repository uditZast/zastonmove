import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { Button, Icon } from "react-native-elements";
import colors from "../../../colors/Colors";
import Modal from "react-native-modal";
import moment from "moment";
import Snackbar from "react-native-snackbar";

class DownloadReportModal extends Component {
  state = {
    selectedVendor: 0,
    selectedVehicle: 0,
    isLoading: false,
    isVisible: false,
    driverPhoneNumber: null,
    vendorDisabled: false,
    showModal: false,
    showError: false,
    isButtonDisabled: false,
    lastRequestTime: new moment()
  };

  closeModal = () => {
    this.setState({ isVisible: false, input: null });
  };

  submitData = () => {
    console.log("---- submit data called ----");
    this.setState({ isLoading: true });
    const { closeModal } = this.props;

    const { sendDownloadReport, canDownloadReport = true } = this.props;

    if (canDownloadReport) {
      sendDownloadReport()
        .then(response => {
          const { status, data } = response;
          console.log("data ------------------", data);

          if (status === true) {
            this.setState({ isLoading: false });
            setTimeout(() => {
              Snackbar.show({
                title: data,
                duration: Snackbar.LENGTH_LONG
              });
            }, 700);

            closeModal();
          } else {
            console.log("---- else method ----");
            this.setState({ isLoading: false });
          }
        })
        .catch(error => {
          console.log("error========DOWNLOAD REPORT=======" + error);
        });
    } else {
      this.setState({ isLoading: false });
    }
  };

  close = () => {
    const { closeModal } = this.props;
    this.setState({ showError: false });
    closeModal();
  };

  render() {
    const {
      bookingCode,
      closeModal,
      isVisible,
      text,
      canDownloadReport
    } = this.props;

    return (
      <Modal
        isVisible={isVisible}
        onBackButtonPress={this.close}
        onBackdropPress={this.close}
      >
        <View style={styles.modalOuterStyle}>
          <View style={styles.modalInnerStyle}>
            <View>
              <TouchableOpacity
                style={styles.imageStyle}
                hitSlop={{ top: 30, bottom: 30, left: 70, right: 70 }}
                onPress={this.close}
              >
                <Icon
                  name="cancel"
                  type="material"
                  size={25}
                  color={colors.blueColor}
                />
              </TouchableOpacity>

              {canDownloadReport === true ? (
                <View style={{ marginTop: 10 }}>
                  <Text style={styles.textStyle}>
                    This report will be sent to your respective mail id.
                    <Text style={styles.textStyle}> Are you sure ? </Text>
                  </Text>

                  <Button
                    onPress={this.submitData}
                    title={"Confirm"}
                    buttonStyle={{ margin: 10 }}
                    loading={this.state.isLoading}
                  />
                </View>
              ) : (
                <View>
                  <Text
                    style={[
                      styles.textStyle,
                      { color: colors.red, padding: 10 }
                    ]}
                  >
                    {
                      "You have already placed a request. Please try again after sometime."
                    }
                  </Text>
                  {/*<Button*/}
                  {/*  onPress={this.close}*/}
                  {/*  title={"Ok"}*/}
                  {/*  buttonStyle={{margin: 10}}*/}
                  {/*  loading={this.state.isLoading}*/}
                  {/*/>*/}
                </View>
              )}
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = {
  imageStyle: {
    width: 23,
    height: 22,
    marginTop: -18,
    marginLeft: -18
  },
  buttonStyle: {
    color: colors.white,
    backgroundColor: colors.blueColor,
    fontSize: 15,
    justifyContent: "flex-end",
    marginTop: 20,
    marginLeft: 2,
    marginRight: 2,
    marginBottom: 10,
    textAlign: "center",
    padding: 10
  },
  modalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0)"
  },
  modalInnerStyle: {
    backgroundColor: "#FFF",
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    padding: 10,
    width: "90%"
  },
  textStyle: {
    fontSize: 15,
    color: colors.black85,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
    fontFamily: "CircularStd-Book"
  }
};

export default DownloadReportModal;
