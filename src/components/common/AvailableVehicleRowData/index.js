import React, { Component, Fragment } from "react";
import { StyleSheet, Text, View } from "react-native";
import colors from "../Colors";
import { Icon } from "react-native-elements";
import customStyles from "../Styles";
import moment from "moment";

class AvailableVehicleRowData extends Component {
  render() {
    const { rowData } = this.props;

    const {
      number,
      vehicle_type,
      trip_status,
      route,
      trip_code,
      customer,
      booking_type,
      vendor,
      isBooked,
      category,
      location,
      recorded_at,
      nick_name
    } = rowData || {};

    const recordedAt = moment(recorded_at);

    return (
      <Fragment key={trip_code}>
        <View style={{ flex: 1 }}>
          <View
            style={
              category !== "con"
                ? [
                    styles.cardContainer,
                    {
                      backgroundColor: colors.blue2,
                      marginBottom: location !== "" ? 0 : 5
                    }
                  ]
                : [
                    styles.cardContainer,
                    {
                      backgroundColor: colors.white,
                      marginBottom: location !== "" ? 0 : 5
                    }
                  ]
            }
          >
            <View style={styles.leftVehicleContainer}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                {isBooked && (
                  <Icon
                    name={"bookmark"}
                    type="material-community"
                    size={20}
                    color={colors.red}
                  />
                )}
                {/*<Text style={[customStyles.fs16, {color: colors.blue}]}>*/}
                {/*  {`${number}   `}*/}
                {/*</Text>*/}
                <Text style={[customStyles.fs14]}>{route}</Text>
              </View>
              <View>
                <Text>{trip_status}</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text>{nick_name}</Text>

                <Text style={[customStyles.fs14]}>({vehicle_type})</Text>
              </View>
            </View>

            <View style={styles.rightVehicleContainer}>
              <View style={styles.miniBox}>
                <Text style={[customStyles.fs14, { fontWeight: "bold" }]}>
                  {number}
                </Text>
              </View>
              {/*<View style={styles.miniBox}>*/}
              {/*  <Text style={customStyles.fs14}>{trip_code}</Text>*/}
              {/*</View>*/}
              <View style={styles.miniBox}>
                <Text
                  style={customStyles.fs14}
                >{`${customer} ${booking_type}`}</Text>
              </View>
              <View style={styles.miniBox}>
                <Text style={customStyles.fs14}>{trip_code}</Text>
              </View>
              {/*<View style={styles.miniBox}>*/}
              {/*  <Text style={customStyles.fs14}>{vendor}</Text>*/}
              {/*</View>*/}
            </View>
          </View>

          {location !== "" && (
            <View
              style={
                category !== "con"
                  ? [
                      styles.locationContainer,
                      { backgroundColor: colors.blue2 }
                    ]
                  : [
                      styles.locationContainer,
                      { backgroundColor: colors.white }
                    ]
              }
            >
              <Text style={[customStyles.fs14, { paddingLeft: 5 }]}>
                {location}
                {recordedAt.isValid() && (
                  <Text
                    style={{
                      color: new moment(recordedAt)
                        .add(2880, "minutes")
                        .isAfter(new moment())
                        ? colors.darkGreen
                        : colors.red
                    }}
                  >
                    {" "}
                    ({recordedAt.fromNow()})
                  </Text>
                )}
              </Text>
            </View>
          )}
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.listBackground
  },
  filterBorder: {
    borderWidth: 1,
    borderRadius: 3,
    borderColor: colors.blue,
    flex: 1,
    margin: 10,
    height: 40
  },
  leftVehicleContainer: {
    flex: 2,
    justifyContent: "space-between",
    padding: 6
  },
  rightVehicleContainer: {
    flex: 1,
    justifyContent: "space-around",
    borderWidth: 1,
    borderLeftColor: colors.border_grey,
    borderTopColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: "transparent",
    padding: 6
  },
  miniBox: {
    borderWidth: 1,
    borderColor: colors.blue,
    alignItems: "center",
    borderRadius: 2,
    margin: 2
  },
  pickerStyle: {
    fontSize: 14,
    color: colors.grey
  },
  cardContainer: {
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10,
    justifyContent: "space-between"
  },
  locationContainer: {
    marginLeft: 10,
    marginRight: 10,
    borderTopColor: colors.border_grey,
    borderWidth: 0.5,
    borderBottomColor: "transparent",
    borderRightColor: "transparent",
    borderLeftColor: "transparent",
    paddingTop: 6,
    paddingRight: 6,
    paddingBottom: 6,
    flexDirection: "row",
    marginBottom: 5
  },
  legendContainer: {
    backgroundColor: colors.listBackground,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 10
  },
  radioButtonStyle: {
    margin: 0
  },
  cardStyle: {
    borderColor: colors.blue,
    borderWidth: 1,
    borderRadius: 2,
    borderBottomWidth: 1,
    marginLeft: 5,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    borderTopLeftRadius: 3,
    backgroundColor: colors.white,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3
  },
  textStyle: {
    fontFamily: "CircularStd-Book",
    fontSize: 13,
    marginTop: 10,
    color: colors.black65
  },
  sectionTextStyle: {
    color: colors.white,
    fontWeight: "bold",
    padding: 5,
    width: 60,
    textAlign: "center",
    fontFamily: "CircularStd-Book",
    fontSize: 14
  }
});

export default AvailableVehicleRowData;
