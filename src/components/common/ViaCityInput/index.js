import React, { Component, Fragment } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import Modal from "react-native-modal";
import colors from "../Colors";
import { Icon, SearchBar } from "react-native-elements";
import CustomStyles from "../Styles";

class ViaCityInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      input: null
    };
  }

  setCity = id => () => {
    const { addViaCity } = this.props;
    addViaCity(id);
    this.closeModal();
  };

  removeIndexElement = index => () => {
    const { removeViaCity } = this.props;
    removeViaCity(index);
  };

  openModal = () => {
    this.setState({ isVisible: true });
  };

  closeModal = () => {
    this.setState({ isVisible: false, input: null });
  };

  setSearch = text => {
    this.setState({ input: text });
  };

  clearInput = () => {
    this.setState({ input: null });
  };

  renderCity = ({ index, item: city_id }) => {
    const { cities } = this.props;
    const { basicInfo: { name, code } = {} } = cities[city_id] || {};

    return (
      <Fragment key={index}>
        <TouchableOpacity
          style={{
            paddingTop: 10,
            paddingBottom: 10,
            flexDirection: "row",
            borderWidth: 1,
            borderColor: colors.white,
            borderBottomColor: colors.grey
          }}
          onPress={this.setCity(city_id)}
        >
          <View style={{ flex: 1 }}>
            <Text style={{ color: colors.blue }}>{code}</Text>
          </View>
          <Text style={{ color: colors.blue }}>{":"}</Text>
          <View style={{ flex: 4, paddingLeft: 16 }}>
            <Text>{name}</Text>
          </View>
        </TouchableOpacity>
      </Fragment>
    );
  };

  renderCityList = () => {
    const { cities, cityIds } = this.props;
    let list = cityIds;
    const { input } = this.state;

    if (input && input.toUpperCase()) {
      const u_input = input.toUpperCase();
      list = list.filter(city_id => {
        const { basicInfo: { name = "", code } = {} } = cities[city_id] || {};
        return (
          name.toUpperCase().includes(u_input) ||
          code.toUpperCase().includes(u_input)
        );
      });
    }
    return (
      <FlatList
        data={list}
        keyboardShouldPersistTaps={"handled"}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderCity}
        showsVerticalScrollIndicator={false}
      />
    );
  };

  render() {
    const { closeModal, openModal } = this;
    const { cities, selectedViaCities = [] } = this.props;
    const { isVisible } = this.state;

    return (
      <Fragment>
        <View style={styles.border}>
          <Text style={styles.labelText}>Add Via City</Text>
          <View style={styles.pickerView}>
            {selectedViaCities.length === 0 && (
              <TouchableOpacity onPress={openModal} style={{ width: "100%" }}>
                <Text style={{ color: colors.black25 }}>Add Via City</Text>
              </TouchableOpacity>
            )}
            {selectedViaCities.map((viaCityId, index) => {
              const { basicInfo: { name, code } = {} } =
                cities[viaCityId] || {};
              return (
                <View
                  key={index}
                  style={{ flexDirection: "row", alignItems: "center" }}
                >
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "center",
                      paddingTop: 8
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <Text style={{ color: colors.blue }}>{code}</Text>
                    </View>
                    <Text style={{ color: colors.blue }}>{":"}</Text>
                    <View style={{ flex: 4, paddingLeft: 16 }}>
                      <Text>{name}</Text>
                    </View>
                    <Icon
                      containerStyle={{
                        position: "absolute",
                        right: 40,
                        paddingTop: 8
                      }}
                      name="plus"
                      type="antdesign"
                      color={colors.blue}
                      onPress={this.openModal}
                    />
                    <Icon
                      containerStyle={{
                        position: "absolute",
                        right: 10,
                        paddingTop: 8
                      }}
                      name="minus"
                      type="antdesign"
                      color={colors.red}
                      onPress={this.removeIndexElement(index)}
                    />
                  </View>
                </View>
              );
            })}
          </View>
        </View>
        <Modal
          isVisible={isVisible}
          onBackButtonPress={closeModal}
          onBackdropPress={closeModal}
          animationIn={"slideInUp"}
          // coverScreen={true}
          style={{
            position: "absolute",
            margin: 0,
            top: 95,
            bottom: 0,
            left: 0,
            right: 0
          }}
          //onSwipeComplete={closeModal}
          //swipeDirection="down"
        >
          <View style={styles.modalView}>
            <SearchBar
              placeholder="Search Source City..."
              onChangeText={this.setSearch}
              lightTheme
              containerStyle={styles.searchBarContainer}
              value={this.state.input}
              onCancel={this.clearInput}
              onClear={this.clearInput}
            />
            <View style={{ padding: 16, paddingTop: 0, flex: 1 }}>
              {this.renderCityList()}
            </View>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalView: {
    width: "100%",
    flex: 1,
    borderRadius: 4,
    backgroundColor: colors.white,
    alignSelf: "center",
    paddingBottom: 30
  },
  searchBarContainer: {
    backgroundColor: colors.white,
    borderWidth: 0,
    padding: 16
  },
  border: {
    borderWidth: 1,
    borderRadius: 3,
    borderColor: colors.blue,
    margin: 10,
    flex: 0,
    minHeight: 40
  },
  labelText: {
    marginTop: -10,
    color: colors.blue,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  pickerView: {
    flex: 0,
    paddingLeft: 10,
    justifyContent: "center",
    paddingBottom: 8
  }
});

export default ViaCityInput;
