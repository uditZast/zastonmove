import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import colors from '../../colors/Colors';
import CircleHeaderTripDetails from '../shapes/CircleHeaderTripDetail';


const TripDetailHeader = (props) => {

    console.log("vehicle number header" + props.vehicle);
    return (
        <View style={styles.mainContainer}>

            <CircleHeaderTripDetails speed={props.speed}/>

            <View style={styles.subContainer}>

                <Text style={styles.headerText}>{props.vehicle}</Text>
                <Text style={styles.normalText}>{props.t_code}{' (' + (props.type) + ')'}</Text>

            </View>

        </View>
    );

};


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colors.blueColor,
        flexDirection: 'row',
        marginBottom: 10
    },
    subContainer: {
        flexDirection: 'column',
        marginTop: 5,
        marginLeft: 15,
        marginBottom: 5
    },
    headerText: {
        fontSize: 15,
        color: colors.white,
        fontWeight: 'bold',
        fontFamily: 'CircularStd-Book'
    },
    normalText: {
        fontSize: 12,
        color: colors.white,
        fontFamily: 'CircularStd-Book',
        marginTop: 5

    }

});

export default TripDetailHeader;
// speed vehicle no trip code
//customer/route/trip type/status
//map//actions