import React, { Component, Fragment } from "react";
import { FloatingAction } from "react-native-floating-action";
import colors from "../../../colors/Colors";
import { NAVIGATE } from "../../../../constants";

class FloatingActionNew extends Component {
  gotoAdHocOneWayScreen = () => {
    this.props.navigation.navigate(NAVIGATE.BOOKING_ONE_WAY);
  };

  render() {
    return (
      <Fragment>
        <FloatingAction actions={actions} color={colors.blueColor} />
      </Fragment>
    );
  }
}

export default FloatingActionNew;
