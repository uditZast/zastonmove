import React, { Component, Fragment } from "react";
import { Platform, Text } from "react-native";
import SelectInput from "react-native-select-input-ios";
import customStyles from "../Styles";
import { View } from "react-native-animatable";
import colors from "../Colors";

class HaltReasonInput extends Component {
  constructor(props) {
    super(props);
  }

  getHaltReasons = () => {
    const { haltReasons } = this.props;
    const options = [{ label: "Select Halt Reason", value: 0 }];

    const haltReasonsArray = Object.keys(haltReasons);
    for (const haltId of haltReasonsArray) {
      const { code, value } = haltReasons[haltId] || {};
      options.push({
        label: value,
        value: code
      });
    }
    return options;
  };

  selectHaltReason = id => {
    const { setHaltReason } = this.props;
    setHaltReason(id);
  };

  render() {
    const { getHaltReasons, selectHaltReason } = this;
    const { selectedHaltReason = 0 } = this.props;

    return (
      <Fragment>
        <View style={customStyles.border}>
          <Text style={customStyles.labelText}>Select Halt Reason</Text>
          <SelectInput
            style={Platform.OS === "ios" ? customStyles.pickerView : null}
            options={getHaltReasons()}
            mode={"dropdown"}
            labelStyle={{
              color: selectedHaltReason === 0 ? colors.black25 : colors.black,
              ...Platform.select({
                android: {
                  height: 30
                },
                ios: {
                  alignSelf: "flex-start"
                }
              })
            }}
            value={selectedHaltReason}
            onSubmitEditing={selectHaltReason}
          />
        </View>
      </Fragment>
    );
  }
}

export default HaltReasonInput;
