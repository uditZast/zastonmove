import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import route from "../../images/destination.png";
import clock from "../../images/clock.png";
import colors from "../../colors/Colors";
import locIcon from "../../images/placeholder.png";


const IntransitTripSection2 = (props) => {

    const {loc, updated, start} = props;

    function checkUndefinedStarted(started) {
        if (started !== null && started !== '' && started !== undefined && started !== 'Started: ') {
            return (
                <View style={styles.startedStyle}>
                    <Image source={clock}
                           style={styles.iconStyle}/>
                    <Text style={styles.locationTextStyle}>{started}</Text>
                </View>
            );
        } else {
            return null
        }
    }


    function checkUndefinedValues(loc, updated) {
        console.log(loc, updated)
        if (loc !== null && loc !== '') {
            console.log('location::::');
            return (
                <View style={styles.locationViewStyle}>
                    <Image source={locIcon}
                           style={styles.iconStyle}/>
                    <Text style={styles.locationTextStyle1}>{loc}
                        <Text style={styles.blanktextStyle}>1</Text>
                        <Text style={styles.redTextStyle}>{updated}</Text>
                    </Text>

                </View>
            );
        } else {
            return null
        }
    }


    return (
        <View style={{flex: 1, marginBottom: 5}}>

            <View style={styles.routeStyle}>
                <Image source={route}
                       style={styles.routeIcon}/>
                <Text style={styles.textStyle2}>{props.route}</Text>
            </View>


            {checkUndefinedStarted(start)}
            {checkUndefinedValues(loc, updated)}

        </View>

    );

}

const styles = StyleSheet.create({
    startedStyle: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 40,
    },
    iconStyle: {
        height: 14,
        width: 14,
        marginLeft: 10,
        marginTop: 3,
        marginBottom: 3
    },
    locationTextStyle: {
        color: colors.textcolor,
        fontSize: 12,
        flex: 1,
        marginLeft: 10,
        marginTop: 4,
        fontFamily: 'CircularStd-Book'
    },
    routeStyle: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 40
    },
    routeIcon: {
        height: 16,
        width: 16,
        marginLeft: 10,
        marginTop: 5,
        marginBottom: 3
    },
    textStyle2: {
        color: colors.textcolor,
        fontSize: 12,
        marginLeft: 10,
        marginTop: 5,
        fontFamily: 'CircularStd-Book'
    },
    locationViewStyle: {
        flex: 1,
        marginTop: 3,
        flexDirection: 'row',
        marginBottom: 5,
        marginLeft: 40,
        marginRight: 20
    },
    locationTextStyle1: {
        color: colors.textcolor,
        fontSize: 12,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        marginRight: 15,
        fontFamily: 'CircularStd-Book',
        textAlign: 'left',
        lineHeight: 20,
    },
    blanktextStyle: {
        fontSize: 12,
        color: colors.white
    },
    redTextStyle: {
        fontSize: 12,
        fontFamily: 'CircularStd-Book',
        color: colors.loginGradient1,
        marginLeft: 10,
        marginRight: 5
    }

});

export default IntransitTripSection2;