import React, { Component, Fragment } from "react";
import {
  ActivityIndicator,
  Platform,
  Text,
  TextInput,
  View
} from "react-native";
import CustomStyles from "../Styles";
import colors from "../Colors";
import { Icon } from "react-native-elements";
import { doRequest } from "../../../helpers/network";
import { AUTO_FILL_TAT, REQUEST_TYPE } from "../../../../constants";
import { Bookings } from "../../../helpers/Urls";
import Snackbar from "react-native-snackbar";

class DistanceInputNew extends Component {
  constructor(props) {
    super(props);
    const {
      initialValue,
      distanceValue,
      enableDistance,
      enableTat,
      apiHit
    } = props;
    this.state = {
      text: initialValue,
      fetchingTat: false,
      enabled: enableTat,
      distance: distanceValue,
      distanceEnable: enableDistance,
      apiHitFlag: apiHit
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.count !== this.props.count) {
      if (this.props.count === 1) {
        this.setState({
          distanceEnable: this.props.enableDistance,
          enabled: this.props.enableTat
        });
      } else {
        if (
          prevProps.vehicle_type_id !== this.props.vehicle_type_id ||
          prevProps.customer_id !== this.props.customer_id ||
          prevProps.source_city !== this.props.source_city ||
          prevProps.destination_city !== this.props.destination_city ||
          prevProps.via_cities !== this.props.via_cities
        ) {
          if (this.props.dryRunFlag) {
            console.log("---- dryRun Flag true ----");
            this.setState({ enabled: true });
          } else {
            console.log("---- dryRun Flag false ----");
            this.setState({ enabled: false });
          }

          this.setState(
            {
              distanceEnable: false,
              distance: null,
              text: null
            },
            this.onEndEditing
          );
        }
      }
    } else {
    }

    if (prevProps.initialValue !== this.props.initialValue) {
      this.setState({ text: this.props.initialValue });
    }

    if (prevProps.distanceValue !== this.props.distanceValue) {
      this.setState({ distance: this.props.distanceValue });
    }
  }

  onEndEditing = () => {
    const { updateText } = this.props;
    updateText(this.state.text, this.state.distance);
  };

  fetchTat = () => {
    const {
      trip_id,
      vehicle_type_id,
      customer_id,
      source_city,
      destination_city,
      via_cities,
      source_flag
    } = this.props;

    console.log("source_city ----", source_city);
    console.log("destination_city ----", destination_city);

    if (
      (source_flag === AUTO_FILL_TAT.CREATE_BOOKING &&
        vehicle_type_id &&
        customer_id &&
        source_city &&
        destination_city) ||
      (source_flag === AUTO_FILL_TAT.EDIT_ROUTE &&
        source_city &&
        destination_city) ||
      (source_flag === AUTO_FILL_TAT.EDIT_BOOKING &&
        customer_id &&
        vehicle_type_id &&
        source_city &&
        destination_city) ||
      (source_flag === AUTO_FILL_TAT.EDIT_BOOKING &&
        vehicle_type_id &&
        source_city &&
        destination_city)
    ) {
      const data = {
        trip_id,
        vehicle_type_id,
        customer_id,
        source_city,
        destination_city,
        via_cities,
        source_flag
      };
      this.setState({ fetchingTat: true });
      const response = doRequest({
        method: REQUEST_TYPE.POST,
        url: Bookings.fetchExpectedBookingURL(),
        isMAPI: true,
        data
      })
        .then(result => {
          console.log("result ----", result);
          const { data: { expected_tat, distance } = {} } = result;
          if (expected_tat && distance) {
            this.setState({
              text: `${expected_tat}`,
              enabled: false,
              distance: `${distance}`,
              distanceEnable: false
            });
          } else if (distance) {
            this.setState({
              distance: `${distance}`,
              distanceEnable: false,
              enabled: true
            });
          } else if (expected_tat) {
            this.setState({
              text: `${expected_tat}`,
              enabled: false,
              distanceEnable: true
            });
          } else {
            this.setState({
              text: null,
              enabled: true,
              distance: null,
              distanceEnable: true
            });
          }
        })
        .catch(err => {
          const { message = "" } = err;
          Snackbar.show({
            title: `${message}`,
            duration: Snackbar.LENGTH_SHORT
          });
        })
        .finally(() =>
          this.setState({ fetchingTat: false }, this.onEndEditing)
        );
    }
  };

  render() {
    const { message, placeholder, action } = this.props;
    const {
      fetchingTat,
      enabled,
      distance,
      text,
      disableDistance,
      distanceEnable
    } = this.state;

    return (
      <Fragment>
        <View
          style={
            enabled === false
              ? CustomStyles.borderDisabled
              : CustomStyles.border
          }
        >
          <Text
            style={
              enabled === false
                ? [CustomStyles.labelText, { color: colors.black25 }]
                : CustomStyles.labelText
            }
          >
            {message}
          </Text>

          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <TextInput
              placeholder={placeholder}
              style={[
                CustomStyles.pickerView,
                enabled === false
                  ? { color: colors.black25 }
                  : { color: colors.black85 },
                {
                  ...Platform.select({
                    ios: { maxHeight: 30, paddingBottom: 10 },
                    android: { flexGrow: 1, paddingBottom: 0, paddingTop: 0 }
                  })
                }
              ]}
              keyboardType="numeric"
              onChangeText={text =>
                this.setState({ text: text }, this.onEndEditing)
              }
              value={this.state.text ? String(this.state.text) : ""}
              placeholderTextColor={colors.black25}
              onEndEditing={this.onEndEditing}
              editable={enabled}
            />

            {fetchingTat ? (
              <ActivityIndicator
                size="small"
                style={{ marginRight: 8 }}
                color={colors.darkblue}
              />
            ) : (
              <Icon
                size={20}
                name="play-circle"
                onPress={this.fetchTat}
                type="material-community"
                underlayColor={"transparent"}
                containerStyle={{ marginRight: 8 }}
                color={colors.darkblue}
              />
            )}
          </View>
        </View>

        <View
          style={
            distanceEnable === false
              ? CustomStyles.borderDisabled
              : CustomStyles.border
          }
        >
          <Text
            style={
              distanceEnable === false
                ? [CustomStyles.labelText, { color: colors.black25 }]
                : CustomStyles.labelText
            }
          >
            {"Distance"}
          </Text>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <TextInput
              placeholder={"Click to autofill Distance"}
              style={[
                CustomStyles.pickerView,
                distanceEnable === false
                  ? { color: colors.black25 }
                  : { color: colors.black85 },
                {
                  ...Platform.select({
                    ios: { maxHeight: 30, paddingBottom: 10 },
                    android: { flexGrow: 1, paddingBottom: 0, paddingTop: 0 }
                  })
                }
              ]}
              keyboardType="numeric"
              onChangeText={text =>
                this.setState({ distance: text }, this.onEndEditing)
              }
              value={this.state.distance ? String(this.state.distance) : ""}
              placeholderTextColor={colors.black25}
              onEndEditing={this.onEndEditing}
              editable={distanceEnable}
            />
          </View>
        </View>
      </Fragment>
    );
  }
}

export default DistanceInputNew;
