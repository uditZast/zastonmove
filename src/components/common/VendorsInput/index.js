import React, { Component, Fragment } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import Modal from "react-native-modal";
import CustomStyles from "../Styles";
import colors from "../Colors";
import { SearchBar } from "react-native-elements";

const vendorPlaceHolder = "Select Vendor";
const disabledPlaceHolder = "Please select vehicle first";

class VendorsInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      input: null
    };
  }

  setVendor = (id, code) => () => {
    const { selectVendor } = this.props;
    selectVendor(id, code);
    this.closeModal();
  };

  openModal = () => {
    this.setState({ isVisible: true });
  };

  closeModal = () => {
    this.setState({ isVisible: false, input: null });
  };

  setSearch = text => {
    this.setState({ input: text });
  };

  clearInput = () => {
    this.setState({ input: null });
  };

  renderVendor = ({ index, item: vendor_id }) => {
    const { vendors } = this.props;
    const { basicInfo: { name, code, nick_name } = {} } =
      vendors[vendor_id] || {};
    return (
      <Fragment key={index}>
        <TouchableOpacity
          style={{
            paddingTop: 10,
            paddingBottom: 10,
            borderWidth: 1,
            borderColor: colors.white,
            borderBottomColor: colors.grey
          }}
          onPress={this.setVendor(vendor_id, code)}
        >
          <View style={{ flex: 3 }}>
            <Text style={{ color: colors.blue }}>{nick_name}</Text>
          </View>
          {/*<Text style={{ color: colors.blue }}>{":"}</Text>*/}
          <View style={{ flex: 4, paddingTop: 10 }}>
            <Text>{name}</Text>
          </View>
          {/*<View style={{flex: 4, paddingLeft: 16}}>*/}
          {/*  <Text>{name}</Text>*/}
          {/*</View>*/}
        </TouchableOpacity>
      </Fragment>
    );
  };

  renderVendorsList = () => {
    const { vendors } = this.props;

    let list = Object.keys(vendors);
    const { input } = this.state;

    if (input && input.toUpperCase()) {
      const u_input = input.toUpperCase();
      list = list.filter(vendor_id => {
        const { basicInfo: { name = "", code, nick_name = "" } = {} } =
          vendors[vendor_id] || {};
        return (
          name.toUpperCase().includes(u_input) ||
          code.toUpperCase().includes(u_input) ||
          nick_name.toUpperCase().includes(u_input)
        );
      });
    }

    return (
      <FlatList
        data={[...list]}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderVendor}
        keyboardShouldPersistTaps={"handled"}
        showsVerticalScrollIndicator={false}
      />
    );
  };

  render() {
    const { closeModal, openModal } = this;
    const {
      selectedVendor = 0,
      vendors,
      disabled = false,
      vehicleApprovalScreen = false
    } = this.props;
    const {
      basicInfo: {
        name = disabled
          ? vehicleApprovalScreen
            ? "Select Deployer"
            : disabledPlaceHolder
          : vehicleApprovalScreen
          ? "Select Owner"
          : vendorPlaceHolder,
        id
      } = {}
    } = vendors[selectedVendor] || {};
    const { isVisible } = this.state;

    return (
      <Fragment>
        <TouchableOpacity
          style={disabled ? CustomStyles.borderDisabled : CustomStyles.border}
          onPress={disabled ? null : openModal}
          activeOpacity={disabled ? 1 : 0.2}
        >
          <Text
            style={
              disabled ? CustomStyles.labelTextDisabled : CustomStyles.labelText
            }
          >
            {vehicleApprovalScreen ? this.props.title : "Vendor"}
          </Text>
          <View style={CustomStyles.pickerView}>
            <Text
              style={
                disabled || name === vendorPlaceHolder
                  ? { color: colors.black25 }
                  : {}
              }
            >
              {name}
            </Text>
          </View>
        </TouchableOpacity>
        <Modal
          isVisible={isVisible}
          onBackButtonPress={closeModal}
          onBackdropPress={closeModal}
          animationIn={"slideInUp"}
          // coverScreen={true}
          style={{
            position: "absolute",
            margin: 0,
            top: 95,
            bottom: 0,
            left: 0,
            right: 0
          }}
          //onSwipeComplete={closeModal}
          //swipeDirection="down"
        >
          <View style={styles.modalView}>
            <SearchBar
              placeholder={
                vehicleApprovalScreen ? this.props.title : "Search Vendors..."
              }
              onChangeText={this.setSearch}
              lightTheme
              containerStyle={styles.searchBarContainer}
              value={this.state.input}
              onCancel={this.clearInput}
              onClear={this.clearInput}
            />
            <View style={{ padding: 16, paddingTop: 0, flex: 1 }}>
              {this.renderVendorsList()}
            </View>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalView: {
    width: "100%",
    flex: 1,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    alignSelf: "center",
    paddingBottom: 30
  },
  searchBarContainer: {
    backgroundColor: colors.white,
    borderWidth: 0,
    padding: 16
  }
});

export default VendorsInput;
