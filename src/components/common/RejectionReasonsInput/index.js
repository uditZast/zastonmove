import React, { Component, Fragment } from "react";
import customStyles from "../Styles";
import { View } from "react-native-animatable";
import { Platform, StyleSheet, Text } from "react-native";
import NumberInput from "../NumberInput";
import colors from "../Colors";
import Snackbar from "react-native-snackbar";
import { Button } from "react-native-elements";
import SelectInput from "react-native-select-input-ios";

class RejectionReasonInput extends Component {
  constructor(props) {
    super(props);
    this.state = { comment: null, isLoading: false };
  }

  getRejectionReasons = () => {
    const { rejectReasons } = this.props;

    const options = [{ label: "Select Reject Reasons", value: 0 }];
    const rejectReasonsArray = Object.keys(rejectReasons);
    for (const rejectReasonId of rejectReasonsArray) {
      const { code, value } = rejectReasons[rejectReasonId] || {};
      options.push({
        label: value,
        value: code
      });
    }
    return options;
  };

  setRejectReason = code => {
    console.log(code);
    const { selectReason } = this.props;
    selectReason(code);
  };

  updateComment = comment => {
    console.log("phoneNumber ----", comment);
    this.setState({ comment: comment });
  };

  submitRejectReasons = () => {
    this.setState({ isLoading: false });
    const { bookingId, selectedReason = "VNA", visible } = this.props;
    const { comment = null } = this.state;

    if (selectedReason === null) {
      this.setState({ isLoading: false });

      Snackbar.show({
        title: "Select Reject Reason",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (comment === null) {
      this.setState({ isLoading: false });

      Snackbar.show({
        title: "Add Reject Comment",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      const requestData = {
        booking_id: bookingId,
        code: selectedReason,
        comment: comment
      };

      const { rejectBooking } = this.props;

      rejectBooking(requestData)
        .then(response => {
          console.log("response ----", response);
          if (response.status === true) {
            console.log("---- if method ----");
            this.setState({ isLoading: false });
            Snackbar.show({
              title: "" + response.message,
              duration: Snackbar.LENGTH_LONG
            });

            visible();
          } else {
            console.log("---- else method ----");
            this.setState({ isLoading: false });
            Snackbar.show({
              title: "" + response.message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .catch(error => {
          console.log("error===============" + error);
        });
    }
  };

  render() {
    const { getRejectionReasons, setRejectReason } = this;
    const { selectedReason, closeRejectLayout } = this.props;
    let selectedReasonLocal = selectedReason ? selectedReason : 0;

    return (
      <Fragment>
        <View style={[customStyles.border, { marginTop: 25 }]}>
          <View style={styles.labelText}>
            <Text
              style={{
                color: colors.blue,
                backgroundColor: "#00000003"
              }}
            >
              Reject Reasons
            </Text>
          </View>

          <SelectInput
            style={Platform.OS === "ios" ? customStyles.pickerView : null}
            options={getRejectionReasons()}
            mode={"dropdown"}
            labelStyle={{
              color: selectedReasonLocal === 0 ? colors.black25 : colors.black,
              ...Platform.select({
                android: {
                  height: 30
                },
                ios: {
                  alignSelf: "flex-start"
                }
              })
            }}
            value={selectedReasonLocal}
            onSubmitEditing={setRejectReason}
          />
        </View>

        <NumberInput
          numericProp={false}
          message={"Comment"}
          updateText={this.updateComment}
          placeholder={"Enter Comment"}
          styleFlag={true}
        />

        <Button
          title={"Reject"}
          titleStyle={{
            fontSize: 15,
            fontFamily: "CircularStd-Book"
          }}
          onPress={this.submitRejectReasons}
          loading={this.state.isLoading}
          buttonStyle={{
            marginTop: 10,
            marginBottom: 10,
            marginLeft: 10,
            marginRight: 10
          }}
        />
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  labelText: {
    marginTop: -10,
    color: colors.blue,
    marginLeft: 10,
    backgroundColor: colors.white,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  }
});
export default RejectionReasonInput;
