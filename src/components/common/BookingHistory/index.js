import React, { Component, Fragment } from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import Modal from "react-native-modal";
import colors from "../Colors";
import { Icon } from "react-native-elements";

let date, month, year, hrs, mins;

class BookingHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { fetchBookingHistoryById } = this.props;
    const request_data = {
      booking_id: this.props.bookingId,
      trip_code: this.props.tripId,
      entity: "history"
    };
    fetchBookingHistoryById(request_data);
  }

  checkTime(value, index) {
    let text = value,
      textDate,
      textTime;

    textDate = text.split(" ")[0];
    textTime = text.split(" ")[1];

    date = textDate.split("-")[0];
    month = textDate.split("-")[1];
    year = textDate.split("-")[2];

    hrs = textTime.split(":")[0];
    mins = textTime.split(":")[1];

    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];

    const d = new Date(year, month - 1, date, hrs, mins);

    return (
      <View
        style={
          index === 0
            ? [styles.borderStyle, { marginTop: 10 }]
            : styles.borderStyle
        }
      >
        <Text style={styles.textStyle}>
          {monthNames[d.getMonth()] + d.getDate() + ","}
          {d.getFullYear()} {textTime}
        </Text>
      </View>
    );
  }

  changeCaseFirstLetter = params => {
    let value;

    if (typeof params === "string") {
      value = params.charAt(0).toUpperCase() + params.slice(1);
    }

    return (
      <Text
        style={{
          fontSize: 12,
          color: colors.black85,
          fontFamily: "CircularStd-Book",
          marginLeft: 10,
          paddingTop: 5
        }}
      >
        {value}
      </Text>
    );
  };

  render() {
    const { isVisible, closeModal, booking_history_id } = this.props;
    const { bHistory: { data = [] } = {} } = this.props || {};
    let time, date;
    return (
      <Fragment>
        <Modal
          isVisible={isVisible}
          onBackButtonPress={closeModal}
          onBackdropPress={closeModal}
          animationIn={"slideInUp"}
          style={{
            position: "absolute",
            margin: 0,
            top: 95,
            bottom: 0,
            left: 0,
            right: 0
          }}
        >
          <View style={styles.modalView}>
            <View style={{ padding: 16, paddingTop: 0, flex: 1 }}>
              <ScrollView>
                {data.map((item, index) => {
                  return (
                    <Fragment key={index}>
                      {this.checkTime(item.time, index)}
                      <View style={{ flexDirection: "row" }}>
                        <View style={styles.verticalLineStyle} />
                        <View>
                          <Text
                            style={{
                              fontSize: 12,
                              color: colors.black85,
                              fontFamily: "CircularStd-Book",
                              marginLeft: 10,
                              paddingTop: 5
                            }}
                          >
                            {item.action}
                          </Text>

                          {this.changeCaseFirstLetter(item.user)}
                        </View>
                      </View>

                      {index === data.length - 1 ? (
                        <Icon
                          name={"circle"}
                          type={"feather"}
                          size={10}
                          color={colors.blue}
                          containerStyle={{
                            alignSelf: "flex-start",
                            marginLeft: 5
                          }}
                        />
                      ) : null}
                    </Fragment>
                  );
                })}
              </ScrollView>
            </View>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalView: {
    width: "100%",
    flex: 1,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    alignSelf: "center",
    paddingBottom: 30
  },
  verticalLineStyle: {
    borderLeftColor: colors.blue,
    borderLeftWidth: 1,
    justifyContent: "center",
    // alignSelf: "center",
    height: 70,
    alignSelf: "flex-start",
    marginLeft: 10
  },
  timeStyle: {
    alignItems: "center",
    justifyContent: "center",
    marginRight: 5,
    width: 80
  },
  dateText: {
    justifyContent: "center",
    alignSelf: "center",
    fontFamily: "CircularStd-Book",
    fontSize: 12,
    padding: 5
  },
  timeText: {
    paddingTop: 5,
    justifyContent: "center",
    alignSelf: "center",
    fontFamily: "CircularStd-Book",
    fontSize: 12
  },
  iconViewStyle: {
    alignItems: "flex-end",
    alignSelf: "flex-end",
    justifyContent: "flex-end"
  },
  actionViewStyle: {
    justifyContent: "center",
    marginLeft: 5,
    flex: 1
  },
  actionTextStyle: {
    fontFamily: "CircularStd-Book",
    fontSize: 12,
    color: colors.blue,
    fontWeight: "bold",
    marginRight: 10,
    justifyContent: "center"
  },
  userTextStyle: {
    fontFamily: "CircularStd-Book",
    fontSize: 12,
    color: colors.black
  },
  borderStyle: {
    borderWidth: 1,
    borderRadius: 10,
    borderColor: colors.blue,
    backgroundColor: colors.white,
    alignSelf: "flex-start"
  },
  textStyle: {
    padding: 5,
    color: colors.black85,
    fontFamily: "CircularStd-Book",
    fontSize: 12
  }
});

export default BookingHistory;

{
  /*<View*/
}
{
  /*  style={*/
}
{
  /*    index !== 0*/
}
{
  /*      ? {*/
}
{
  /*        flexDirection: "row",*/
}
{
  /*        ...Platform.select({*/
}
{
  /*          ios: {*/
}
{
  /*            marginRight: 10,*/
}
{
  /*            paddingRight: 10*/
}
{
  /*          },*/
}
{
  /*          android: {*/
}
{
  /*            marginRight: 10,*/
}
{
  /*            paddingRight: 10*/
}
{
  /*          }*/
}
{
  /*        })*/
}
{
  /*      }*/
}
{
  /*      : {*/
}
{
  /*        flexDirection: "row",*/
}
{
  /*        marginTop: 10,*/
}
{
  /*        marginRight: 10,*/
}
{
  /*        paddingRight: 10*/
}
{
  /*      }*/
}
{
  /*  }*/
}
{
  /*>*/
}
{
  /*  <View style={styles.timeStyle}>*/
}
{
  /*    <Text style={styles.dateText}>*/
}
{
  /*      {item.time.split(" ")[0]}*/
}
{
  /*    </Text>*/
}
{
  /*    <Text style={styles.timeText}>*/
}
{
  /*      {item.time.split(" ")[1]}*/
}
{
  /*    </Text>*/
}
{
  /*  </View>*/
}
{
  /*  */
}
{
  /*  <View style={styles.iconViewStyle}>*/
}
{
  /*    <View style={styles.verticalLineStyle}/>*/
}
{
  /*    <Icon*/
}
{
  /*      name={"circle"}*/
}
{
  /*      type={"feather"}*/
}
{
  /*      size={16}*/
}
{
  /*      color={colors.blue}*/
}
{
  /*    />*/
}
{
  /*    <View style={styles.verticalLineStyle}/>*/
}
{
  /*  </View>*/
}
{
  /*  */
}
{
  /*  <View style={styles.actionViewStyle}>*/
}
{
  /*    <Text style={styles.actionTextStyle}>*/
}
{
  /*      {item.action !== "" ? item.action : item.status}*/
}
{
  /*      */
}
{
  /*      <Text style={styles.userTextStyle}>*/
}
{
  /*        {" by "}*/
}
{
  /*        {item.user}*/
}
{
  /*      </Text>*/
}
{
  /*    </Text>*/
}
{
  /*  </View>*/
}
{
  /*</View>*/
}
