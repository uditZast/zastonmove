import React, { Component, Fragment } from "react";
import {
  ActivityIndicator,
  ImageBackground,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../Colors";
import { Icon } from "react-native-elements";
import PhotoUpload from "react-native-photo-upload";
import ImageViewer from "../ImageViewer";

class PhotoUploadContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z/C/HgAGgwJ/lK3Q6wAAAABJRU5ErkJggg==",
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkYPhfDwAChwGA60e6kgAAAABJRU5ErkJggg==",
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8X8JQDwAF4QH0rBN43gAAAABJRU5ErkJggg=="
      ],
      loading: false,
      visible: false,
      index: 0
    };
  }

  removeImageFromArray = index => {
    this.setState(prevState => {
      const newImages = [...prevState.images];
      newImages.splice(index, 1);
      return { images: newImages };
    });
  };

  removeImage = index => () => {
    this.removeImageFromArray(index);
  };

  startLoading = () => {
    this.setState({ loading: true });
  };

  endLoading = () => {
    this.setState({ loading: false }, this.updateParent);
  };

  updateParent = () => {
    const { updateImages } = this.props;
    const { images } = this.state;
    if (updateImages) {
      updateImages(images);
    }
  };

  openImageViewer = index => () => {
    this.setState({ visible: true, index });
  };

  closeImageViewer = () => {
    this.setState({ visible: false });
  };

  render() {
    const { loading = false, visible = false, index } = this.state;
    const { url } = this.props;
    let images = [url];

    return (
      <Fragment>
        <ScrollView
          horizontal={true}
          contentContainerStyle={{
            paddingLeft: 10,
            paddingRight: 10,
            marginTop: 10,
            marginBottom: 20
          }}
        >
          <PhotoUpload
            onStart={this.startLoading}
            onCancel={this.endLoading}
            onError={this.endLoading}
            onPhotoSelect={image => {
              if (image) {
                const baseImage = "data:image/png;base64," + image;
                this.setState(prevState => {
                  const modifiedImages = [...prevState.images];
                  modifiedImages.push(baseImage);
                  return { images: modifiedImages };
                });
              }
              this.endLoading();
            }}
          >
            <View style={styles.borderBox}>
              {loading ? (
                <ActivityIndicator size="small" color={colors.darkblue} />
              ) : (
                <Icon
                  size={27}
                  name="plus"
                  type="material-community"
                  underlayColor={"transparent"}
                  color={colors.darkblue}
                />
              )}
            </View>
          </PhotoUpload>
          {images.map((image, index) => {
            return (
              <Fragment key={`${image}_${index}`}>
                <TouchableOpacity onPress={this.openImageViewer(index)}>
                  <ImageBackground
                    style={styles.imageContainer}
                    source={{ uri: image }}
                  >
                    <TouchableOpacity
                      onPress={this.removeImage(index)}
                      style={styles.circleOutline}
                    >
                      <Icon
                        size={16}
                        name="close"
                        type="material-community"
                        underlayColor={"transparent"}
                        color={colors.black85}
                      />
                    </TouchableOpacity>
                  </ImageBackground>
                </TouchableOpacity>
              </Fragment>
            );
          })}
        </ScrollView>
        {visible && (
          <ImageViewer
            url={url}
            images={images} //The images to show
            visible={visible} //Should Image viewer become visible
            closeModal={this.closeImageViewer} //Close Modal
            index={index} //Opening index of image
            showDelete={true} //Should show the delete image from array option
            removeImage={this.removeImageFromArray} //Function to remove image from array
          />
        )}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  borderBox: {
    width: 60,
    height: 60,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fafafa",
    borderWidth: 1,
    borderColor: colors.darkblue,
    borderStyle: "dashed",
    borderRadius: 1
  },
  imageContainer: {
    width: 60,
    height: 60,
    marginLeft: 10,
    position: "relative"
  },
  circleOutline: {
    backgroundColor: colors.white,
    borderRadius: 40,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    top: 4,
    right: 4
  }
});

export default PhotoUploadContainer;
