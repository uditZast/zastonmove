import React, { Component, Fragment } from "react";
import { Platform, Text, View } from "react-native";
import CustomStyles from "../Styles";
import moment from "moment";
import DatePicker from "react-native-datepicker";
import colors from "../Colors";

class DateInput extends Component {
  constructor(props) {
    super(props);
    const { initialValue = null } = props;
    this.state = {
      dateTime: initialValue
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.initialValue !== this.props.initialValue) {
      this.setState({ dateTime: this.props.initialValue });
    }
  }

  setDateTime = (date, dateTime) => {
    const { selectDateTime } = this.props;
    const momentDate = new moment(dateTime);
    this.setState({ dateTime: momentDate }, selectDateTime(momentDate));
  };

  render() {
    const {
      message,
      selectedDate,
      upperbound = false,
      lowerbound
    } = this.props;

    return (
      <Fragment>
        <View style={CustomStyles.border}>
          <Text style={CustomStyles.labelText}>{message}</Text>
          <DatePicker
            date={this.state.dateTime}
            mode="date"
            placeholder={"Select Date and Time"}
            showIcon={false}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            is24Hour={true}
            onDateChange={this.setDateTime}
            style={{
              ...Platform.select({
                ios: {
                  width: "100%",
                  paddingLeft: 10,
                  flex: 1,
                  justifyContent: "center",
                  paddingBottom: 0,
                  paddingTop: 0,
                  height: 30
                },
                android: {
                  flexGrow: 1,
                  width: "100%",
                  paddingLeft: 10,
                  justifyContent: "center",
                  height: 30
                }
              })
            }}
            maxDate={
              upperbound ? new moment().format("YYYY-MM-DD HH:mm") : undefined
            }
            // minDate={undefined}
            minDate={
              lowerbound
                ? new moment(lowerbound).format("YYYY-MM-DD")
                : undefined
            }
            hideText={!moment(selectedDate).isValid()}
            customStyles={{
              dateInput: {
                borderWidth: 0,
                backgroundColor: "transparent"
              },
              disabled: {
                backgroundColor: "transparent"
              },
              dateText: {
                color: "#000",
                alignSelf: "flex-start"
              },
              placeholderText: {
                alignSelf: "flex-start",
                color: colors.black25
              }
              //For Dark Mode
              // datePickerCon: { backgroundColor: "black" }
            }}
            getDateStr={date => {
              return new moment(date).format("DD-MM-YYYY");
            }}
          />
        </View>
      </Fragment>
    );
  }
}

export default DateInput;
