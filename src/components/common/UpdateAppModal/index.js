import React, { Component, Fragment } from "react";
import {
  Image,
  Linking,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import Modal from "react-native-modal";
import colors from "../Colors";
import { OS } from "../../../../constants";
import logoColor from "../../../images/onmove_logo.png";

class UpdateAppModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  openStore = () => {
    const link =
      Platform.OS === OS.IOS
        ? "https://apps.apple.com/us/app/Zast_Onmove/id1485521987"
        : "market://details?id=com.zastlogisolutions.zastlogics.bookingapplication";
    console.log("link ----", link);
    Linking.canOpenURL(link).then(
      supported => {
        supported && Linking.openURL(link);
      },
      err => console.log(err)
    );
  };

  render() {
    const { isVisible, closeModal } = this.props;

    return (
      <Fragment>
        <Modal
          isVisible={isVisible}
          onBackButtonPress={closeModal}
          // onBackdropPress={closeModal}
          animationIn={"slideInUp"}
          style={{
            margin: 0
          }}
        >
          <View style={styles.modalView}>
            <Image
              source={logoColor}
              style={{ height: 90, width: 100 }}
              resizeMode="contain"
            />

            <Text style={{ padding: 10, color: colors.blue }}>
              New App Version Available
            </Text>

            {/*<Text style={{padding: 10, color: colors.black65,alignSelf: 'center',alignItems:'center'}}>Your app is below the recommended version.*/}
            {/*    Please update the*/}
            {/*    app.</Text>*/}

            <View style={{ flexDirection: "row", padding: 8 }}>
              <TouchableOpacity
                style={[
                  styles.button,
                  { borderColor: colors.red, marginRight: 4 }
                ]}
                onPress={closeModal}
              >
                <Text style={{ color: colors.red }}>Close</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.button, { marginLeft: 4 }]}
                onPress={this.openStore}
              >
                <Text style={{ color: colors.blue }}>Update Now</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalView: {
    width: "80%",
    borderRadius: 4,
    backgroundColor: "#ffffff",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  button: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 8,
    borderWidth: 2,
    borderColor: colors.blue,
    borderRadius: 4
  },
  buttonDisabled: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 8,
    borderWidth: 2,
    borderColor: colors.black15,
    borderRadius: 4
  }
});

export default UpdateAppModal;
