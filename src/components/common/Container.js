import React from 'react'
import {SafeAreaView, View} from "react-native";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'

const Container = (props) => {
    return (
        <SafeAreaView style={{flex: 1}}>
            <View style={{flex: 1}}>
                {/*<OfflineNotice/>*/}
                <KeyboardAwareScrollView
                    resetScrollToCoords={{x: 0, y: 0}}
                    scrollEnabled={true}
                    style={styles.containerStyle}
                    contentContainerStyle={{paddingVertical: 15}}
                    keyboardShouldPersistTaps="never"
                >
                    {props.children}
                </KeyboardAwareScrollView>
            </View>
        </SafeAreaView>
    )
};

const styles = {
    containerStyle: {
        flex: 1,
    }
};

export {Container}
