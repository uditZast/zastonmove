import React, { Component, Fragment } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../../../colors/Colors";
import CardOptionsNew from "../../bookings/bookingactions/CardOptionsNew";
import AssignModal from "../../../containers/AssignModal";
import BookingHistory from "../../../containers/BookingHistory";
import RejectionReasonsInput from "../../../containers/RejectionReasonsInput";
import DatePickerInput from "../../../containers/DatePickerInput";
import moment from "moment";
import { NAVIGATE } from "../../../../constants";

class BookingListCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rejectModal: false,
      showRejectModal: false,
      assignModal: false,
      historyModal: false,
      isVisible: false,
      selectedReason: null,
      showDockModal: false,
      selectedDateTime: new moment(),
      reassignModal: false,
      selectedIndex: 0
    };
  }

  checkReturnBookingType = () => {
    const { bookings, return_booking_id, vehicleTypes, vehicles } = this.props;
    const { basicInfo: { vehicle_id, vehicle_type_id, code, type } = {} } =
      bookings[return_booking_id] || {};

    const vehicle = vehicles[vehicle_id] || {};
    const vehicleType = vehicleTypes[vehicle_type_id] || {};

    const { basicInfo: { name: vehicleTypeName } = {} } = vehicleType;
    const { basicInfo: { vehicle_number } = {} } = vehicle;

    return (
      <View style={{ flex: 0.5 }}>
        <View style={styles.forwardStyle}>
          <Text style={styles.boldTextStyle}>{code}</Text>
          <Text style={styles.normalTextStyle}>{type}</Text>
        </View>

        <View style={styles.rowStyle}>
          {vehicleTypeName && (
            <Text style={styles.normalTextStyle}>{vehicleTypeName}</Text>
          )}
          {vehicle_number && (
            <Text style={styles.normalTextStyle}>{vehicle_number}</Text>
          )}
        </View>
      </View>
    );
  };

  checkVehicle1 = (value, value1) => {
    if (value !== null) {
      return (
        <View style={styles.rowStyle}>
          <Text style={styles.textStyle2}>{value}</Text>
          <Text style={styles.normalTextStyle}>{value1}</Text>
        </View>
      );
    } else {
      return (
        <View style={{ flex: 0.5, flexDirection: "row", marginLeft: 0 }}>
          <Text style={styles.textStyle2}>{value1}</Text>
        </View>
      );
    }
  };

  checkRoute = value => {
    if (value !== null) {
      return (
        <View style={styles.rowStyle1}>
          <Text style={styles.textStyle1}>Route</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {value}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  };

  checkVendor = value => {
    if (value !== null && value !== "") {
      return (
        <View style={styles.rowStyle1}>
          <Text style={styles.textStyle1}>Vendor</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {value}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  };

  checkExpectedTat = value => {
    return (
      <View style={styles.rowStyle1}>
        <Text style={styles.textStyle1}>Expected TAT</Text>
        <Text style={[styles.textStyle1, { color: colors.grey }]}>{value}</Text>
      </View>
    );
  };

  checkScheduledTime = value => {
    if (value !== null && value !== "" && value !== undefined) {
      return (
        <View style={[styles.rowStyle1]}>
          <Text style={styles.textStyle1}>Scheduled Time</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {new moment(value).format("LLL")}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  };

  renderListActions = () => {
    this.props.selectIndex(this.props.index);
  };

  visibleRejectModal = () => {
    if (this.state.rejectModal === false) {
      this.setState({
        rejectModal: true
      });
    } else {
      this.setState({
        rejectModal: false
      });
    }
  };

  assignModalVisible = () => {
    this.setState({ assignModal: true, isVisible: true });
  };

  reAssignModalVisible = () => {
    this.setState({ reassignModal: true, isVisible: true });
  };

  visibleBookingHistory = () => {
    this.setState({ historyModal: true, isVisible: true });
  };

  closeModal = () => {
    this.setState({ isVisible: false });
  };

  selectRejectReason = code => {
    this.setState({ selectedReason: code });
  };

  closeRejectLayout = () => {
    this.setState({ rejectModal: false });
  };

  visibleDockModal = () => {
    if (this.state.showDockModal === false) {
      this.setState({ showDockModal: true });
    } else {
      this.setState({ showDockModal: false });
    }
  };

  setDate = dateTime => {
    this.setState({ selectedDateTime: dateTime });
  };

  onEditBooking = () => {
    const { id, bookingType, screen } = this.props;

    console.log(
      "bookingType onEdit Tap----------------------------------------------",
      bookingType
    );
    if (screen === "new_request") {
      if (bookingType === "AH" || bookingType === "SCH") {
        this.props.navigation.navigate(NAVIGATE.EDIT_BOOKING_ONE_WAY, {
          booking_id: id,
          action: "edit"
        });
      } else if (bookingType === "AHF" || bookingType === "SCHF") {
        this.props.navigation.navigate(NAVIGATE.EDIT_BOOKING_TWO_WAY, {
          booking_id: id,
          action: "edit"
        });
      } else if (bookingType === "DR") {
        this.props.navigation.navigate(NAVIGATE.EDIT_DRY_RUN, {
          booking_id: id,
          action: "edit"
        });
      }
    } else if (screen === "accepted") {
      if (
        bookingType === "AH" ||
        bookingType === "AHF" ||
        bookingType === "AHR" ||
        bookingType === "SCHF" ||
        bookingType === "SCHR"
      ) {
        this.props.navigation.navigate(NAVIGATE.EDIT_BOOKING_ONE_WAY, {
          booking_id: id,
          action: "edit"
        });
      } else {
      }
    } else if (screen === "assigned") {
      if (
        bookingType === "AH" ||
        bookingType === "AHF" ||
        bookingType === "AHR" ||
        bookingType === "SCHF" ||
        bookingType === "SCHR"
      ) {
        this.props.navigation.navigate(NAVIGATE.EDIT_BOOKING_ONE_WAY, {
          booking_id: id,
          action: "edit"
        });
      } else {
      }
    }
  };

  onAcceptBooking = () => {
    const { id, bookingType } = this.props;
    if (bookingType === "AH") {
      this.props.navigation.navigate(NAVIGATE.EDIT_BOOKING_ONE_WAY, {
        booking_id: id,
        action: "accept"
      });
    } else if (bookingType === "AHF") {
      this.props.navigation.navigate(NAVIGATE.EDIT_BOOKING_TWO_WAY, {
        booking_id: id,
        action: "accept"
      });
    } else if (bookingType === "DR") {
      this.props.navigation.navigate(NAVIGATE.DRY_RUN, {
        booking_id: id
      });
    } else {
    }
  };

  closeDock = () => {
    this.setState({ showDockModal: false });
  };

  render() {
    const {
      rejectModal = false,
      showRejectModal = false,
      assignModal = false,
      historyModal = false,
      showDockModal = false,
      reassignModal = false
    } = this.state;

    const {
      selectedIndex,
      index,
      vehicleType,
      vehicle,
      customer,
      vendor,
      bookings,
      return_booking_id
    } = this.props;

    const { basicInfo: { name: vehicleTypeName } = {} } = vehicleType;
    const { basicInfo: { vehicle_number } = {} } = vehicle;
    const { basicInfo: { name: customerName } = {} } = customer;
    const { basicInfo: { name: vendorName } = {} } = vendor;

    return (
      <Fragment>
        <TouchableOpacity onPress={this.renderListActions}>
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 0.5 }}>
              <View style={styles.forwardStyle}>
                <Text style={styles.boldTextStyle}>{this.props.code}</Text>
                <Text style={styles.normalTextStyle}>
                  {this.props.bookingType}
                </Text>
              </View>

              <View style={styles.rowStyle}>
                {vehicleTypeName && (
                  <Text style={styles.normalTextStyle}>{vehicleTypeName}</Text>
                )}
                {vehicle_number && (
                  <Text style={styles.normalTextStyle}>{vehicle_number}</Text>
                )}
              </View>
            </View>
            {this.checkReturnBookingType()}
          </View>

          <View style={styles.lineStyle} />

          {customerName && (
            <View style={styles.rowStyle1}>
              <Text style={styles.textStyle1}>Customer</Text>
              <Text style={[styles.textStyle1, { color: colors.grey }]}>
                {customerName}
              </Text>
            </View>
          )}
          {this.checkRoute(this.props.route)}
          {this.checkExpectedTat(this.props.tat)}
          {this.checkScheduledTime(this.props.time)}
          {/*{this.checkVehicle()}*/}

          {selectedIndex === index && (
            <CardOptionsNew
              onAcceptTap={this.onAcceptBooking}
              onEditTap={this.onEditBooking}
              onRejectTap={this.visibleRejectModal}
              onHistoryTap={this.visibleBookingHistory}
              screen={this.props.screen}
              change="1"
              onAssignTap={this.assignModalVisible}
              id={this.props.id}
              onReAssignTap={this.reAssignModalVisible}
              onDockTap={this.visibleDockModal}
              {...this.props}
            />
          )}
        </TouchableOpacity>

        {rejectModal && (
          <RejectionReasonsInput
            visible={this.closeRejectLayout}
            bookingId={this.props.id}
            rejectReasons={this.props.rejectReasons}
            selectedReason={this.state.selectedReason}
            selectReason={this.selectRejectReason}
          />
        )}

        {assignModal && (
          <AssignModal
            bookingId={this.props.id}
            isVisible={this.state.isVisible}
            vehicles={this.props.vehicles}
            vendors={this.props.vendors}
            vehicleTypes={this.props.vehicleTypes}
            closeModal={this.closeModal}
            text={"Assign Vehicle"}
          />
        )}

        {reassignModal && (
          <AssignModal
            bookingId={this.props.id}
            isVisible={this.state.isVisible}
            vehicles={this.props.vehicles}
            vendors={this.props.vendors}
            vehicleTypes={this.props.vehicleTypes}
            closeModal={this.closeModal}
            text={"Re-Assign Vehicle"}
          />
        )}

        {historyModal && (
          <BookingHistory
            bookingId={this.props.id}
            tripId={null}
            isVisible={this.state.isVisible}
            closeModal={this.closeModal}
          />
        )}

        {showDockModal && (
          <DatePickerInput
            close={this.closeDock}
            bookingId={this.props.id}
            message={"Select Dock Time"}
            selectedDateTime={this.state.selectedDateTime}
            selectDateTime={this.setDate}
          />
        )}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  textStyle2: {
    fontSize: 12,
    color: colors.textcolor,
    paddingTop: 3,
    fontFamily: "CircularStd-Book"
  },
  rowStyle2: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 0
  },
  forwardStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    paddingTop: 3
  },
  returnStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginRight: 10,
    marginLeft: 0,
    marginTop: 3
  },
  boldTextStyle: {
    fontSize: 15,
    fontWeight: "bold",
    color: colors.blueColor,
    fontFamily: "CircularStd-Book"
  },
  normalTextStyle: {
    fontSize: 12,
    color: colors.textcolor,
    marginLeft: 5,
    paddingTop: 3,
    fontFamily: "CircularStd-Book"
  },
  textStyle1: {
    flex: 0.5,
    fontSize: 12,
    color: colors.textcolor,
    fontFamily: "CircularStd-Book"
  },
  rowStyle1: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 3
  },
  rowStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 5
  },
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    marginTop: 3,
    paddingBottom: 5
  },

  optionContainer: {
    flex: 1,
    borderColor: "#2196f3"
  },
  optionContainerRight: {
    borderRightWidth: 1
  },
  cardStyle: {
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderWidth: 1,
    borderColor: "#2196f3",
    flexDirection: "row",
    flex: 1,
    justifyContent: "space-between"
  },
  modalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.7)"
  },
  modalInnerStyle: {
    backgroundColor: "#FFF",
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    padding: 10,
    width: "90%"
  }
});

export default BookingListCard;
