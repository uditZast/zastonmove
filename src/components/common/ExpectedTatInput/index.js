import React, { Component, Fragment } from "react";
import {
  ActivityIndicator,
  Platform,
  Text,
  TextInput,
  View
} from "react-native";
import CustomStyles from "../Styles";
import colors from "../Colors";
import { Icon } from "react-native-elements";
import { doRequest } from "../../../helpers/network";
import { AUTO_FILL_TAT, REQUEST_TYPE } from "../../../../constants";
import { Bookings } from "../../../helpers/Urls";
import Snackbar from "react-native-snackbar";

class ExpectedTatInput extends Component {
  constructor(props) {
    super(props);
    const { initialValue } = props;
    this.state = {
      text: initialValue,
      fetchingTat: false,
      disabled: true
    };
  }

  onEndEditing = () => {
    const { updateText } = this.props;
    const { text, distance } = this.state;
    updateText(text, distance);
  };

  fetchTat = () => {
    const {
      trip_id,
      vehicle_type_id,
      customer_id,
      source_city,
      destination_city,
      via_cities,
      source_flag
    } = this.props;
    if (
      (source_flag === AUTO_FILL_TAT.CREATE_BOOKING &&
        vehicle_type_id &&
        customer_id &&
        source_city &&
        destination_city) ||
      (source_flag === AUTO_FILL_TAT.EDIT_ROUTE &&
        source_city &&
        destination_city) ||
      (source_flag === AUTO_FILL_TAT.EDIT_BOOKING &&
        customer_id &&
        vehicle_type_id &&
        source_city &&
        destination_city)
    ) {
      const data = {
        trip_id,
        vehicle_type_id,
        customer_id,
        source_city,
        destination_city,
        via_cities,
        source_flag
      };
      this.setState({ fetchingTat: true });
      const response = doRequest({
        method: REQUEST_TYPE.POST,
        url: Bookings.fetchExpectedBookingURL(),
        isMAPI: true,
        data
      })
        .then(result => {
          console.log("result ----", result);
          const { data: { expected_tat, distance } = {} } = result;
          // if (expected_tat || distance) {
          //   this.setState(() => {
          //     let result = { disabled: true };
          //     if (expected_tat) {
          //       result = { ...result, text: `${expected_tat}` };
          //     }
          //     if (distance) {
          //       result = { ...result, distance };
          //     }
          //     return result;
          //   });
          // }
          if (distance) {
            this.setState({ distance });
          }
          if (expected_tat) {
            this.setState({ text: `${expected_tat}`, disabled: true });
          } else {
            this.setState({ text: null, disabled: false });
          }
        })
        .catch(err => {
          const { message = "" } = err;
          Snackbar.show({
            title: `${message}`,
            duration: Snackbar.LENGTH_SHORT
          });
        })
        .finally(() =>
          this.setState({ fetchingTat: false }, this.onEndEditing)
        );
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.vehicle_type_id !== this.props.vehicle_type_id ||
      prevProps.customer_id !== this.props.customer_id ||
      prevProps.source_city !== this.props.source_city ||
      prevProps.destination_city !== this.props.destination_city ||
      prevProps.via_cities.length !== this.props.via_cities.length
    ) {
      this.setState({ disabled: true, text: null, distance: null });
    }
    if (prevProps.initialValue !== this.props.initialValue) {
      this.setState({ text: this.props.initialValue });
    }
  }

  render() {
    const { message, placeholder } = this.props;
    const { fetchingTat, disabled } = this.state;

    return (
      <Fragment>
        <View
          style={disabled ? CustomStyles.borderDisabled : CustomStyles.border}
        >
          <Text
            style={
              disabled
                ? [CustomStyles.labelText, { color: colors.black25 }]
                : CustomStyles.labelText
            }
          >
            {message}
          </Text>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <TextInput
              placeholder={placeholder}
              style={[
                CustomStyles.pickerView,
                disabled
                  ? { color: colors.black25 }
                  : { color: colors.black85 },
                {
                  ...Platform.select({
                    ios: { maxHeight: 30, paddingBottom: 10 },
                    android: { flexGrow: 1, paddingBottom: 0, paddingTop: 0 }
                  })
                }
              ]}
              keyboardType="numeric"
              onChangeText={text =>
                this.setState({ text: text }, this.onEndEditing)
              }
              value={this.state.text ? String(this.state.text) : ""}
              placeholderTextColor={colors.black25}
              onEndEditing={this.onEndEditing}
              editable={!disabled}
            />
            {fetchingTat ? (
              <ActivityIndicator
                size="small"
                style={{ marginRight: 8 }}
                color={colors.darkblue}
              />
            ) : (
              <Icon
                size={20}
                name="play-circle"
                onPress={this.fetchTat}
                type="material-community"
                underlayColor={"transparent"}
                containerStyle={{ marginRight: 8 }}
                color={colors.darkblue}
              />
            )}
          </View>
        </View>
      </Fragment>
    );
  }
}

export default ExpectedTatInput;
