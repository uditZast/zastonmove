import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import colors from "../../colors/Colors";

const IntransitRows = (props) => {
    return (
        <View style={{flex: 1}}>

            <View style={styles.rowOneStyle}>
                <Text style={styles.boldTextStyle}>{props.vehicle}</Text>
            </View>

            <View style={styles.rowTwoStyle}>
                <View style={styles.borderStyle}>
                    <Text style={styles.textStyle3}>{props.t_code}</Text>
                </View>
                <View style={styles.borderStyle2}>
                    <Text style={styles.textStyle3}>{props.cust}</Text>
                </View>
                <View style={styles.borderStyle3}>
                    <Text style={styles.textStyle3}>{props.vend}</Text>
                </View>

            </View>

            <View style={styles.lineStyle}></View>
        </View>

    );
}


const styles = StyleSheet.create({
    rowOneStyle: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 10,
        marginTop: 5,
    },
    boldTextStyle: {
        fontWeight: 'bold',
        fontSize: 15,
        color: colors.blueColor,
        fontFamily: 'CircularStd-Book'
    },
    rowTwoStyle: {
        flexDirection: 'row',
        marginLeft: 10,
        marginTop: 5,
    },
    borderStyle: {
        borderWidth: 1,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        borderColor: colors.blueColor,
        width: 80
    },
    borderStyle2: {
        borderWidth: 1,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        borderColor: colors.blueColor,
        marginLeft: 5,
        marginRight: 5,
        width: 70
    },
    borderStyle3: {
        borderWidth: 1,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        borderColor: colors.blueColor,
        marginLeft: 5,
        marginRight: 5,
        width: 60
    },
    textStyle3: {
        color: colors.textcolor,
        fontSize: 12,
        paddingLeft: 3,
        paddingRight: 3,
        paddingTop: 1,
        paddingBottom: 2,
        alignSelf: 'center',
        fontWeight: '400',
        fontFamily: 'CircularStd-Book'
    },
    lineStyle: {
        borderBottomColor: colors.border_grey,
        borderBottomWidth: 1,
        marginTop: 8,
        marginLeft: 10,
        marginRight: 5,
        marginBottom: 5
    }

});

export default IntransitRows;