import React, { Component } from "react";
import Gallery from "react-native-photo-gallery";

class PhotoZoom extends Component {
  render() {
    const { url } = this.props;

    const data = [
      {
        id: "first image",
        image: { uri: url },
        thumb: { uri: url }
      }
    ];

    console.log("url --------------PHOTO ZOOM----", url);
    return <Gallery data={data} />;
  }
}

export default PhotoZoom;
