import React, { Component, Fragment } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import CustomStyles from "../Styles";
import colors from "../Colors";
import moment from "moment";
import { NAVIGATE, USER_RIGHTS } from "../../../../constants";
import { Icon } from "react-native-elements";
import { hasRights } from "../checkRights";

class VehicleApprovalCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rejectModal: false,
      showRejectModal: false,
      assignModal: false,
      historyModal: false,
      isVisible: false,
      selectedReason: null,
      showDockModal: false,
      selectedDateTime: new moment(),
      reassignModal: false,
      selectedIndex: 0
    };
  }

  goToVehicleApprovalForm = () => {
    console.log("---- VEHICLE APPROVAL FORM ----");
    const { id } = this.props;
    const { loggedInUser } = this.props;
    {
      hasRights(USER_RIGHTS.EDIT_VEHICLE, loggedInUser) &&
        this.props.navigation.navigate(NAVIGATE.VEHICLE_EDIT, {
          vehicle_id: id
        });
    }
  };

  render() {
    const {
      vNumber,
      vTypeName,
      category,
      vendorName,
      isActive,
      isApproved,
      roundTrip,
      phone,
      cityName
    } = this.props;

    return (
      <Fragment>
        <TouchableOpacity
          style={styles.cardContainer}
          onPress={this.goToVehicleApprovalForm}
        >
          <View style={{ flex: 1, padding: 0 }}>
            <View
              style={{
                padding: 10,
                flex: 1
              }}
            >
              <View style={{ flexDirection: "row", flex: 1, marginTop: 5 }}>
                <View style={{ flexDirection: "row", flex: 0.5 }}>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "600",
                      color: colors.blue
                    }}
                  >
                    {vNumber}
                  </Text>

                  <Text
                    style={{
                      fontSize: 14,
                      color: colors.black85,
                      marginLeft: 10
                    }}
                  >
                    {vTypeName}
                  </Text>
                </View>
                <View style={{ flex: 0.5 }}>
                  <Text
                    style={{
                      fontSize: 14,
                      color: colors.black85
                    }}
                  >
                    {"Category : "}
                    {category === "CON"
                      ? "Contract"
                      : category === "ZAS"
                      ? "ZastNow"
                      : null}
                  </Text>
                </View>
              </View>

              <View style={styles.headingContainer} />
              {/*=================LINE====================*/}
              <View style={CustomStyles.dashedLine} />
              {/*=================LINE====================*/}

              <View>
                {vendorName && (
                  <View
                    style={{
                      alignSelf: "flex-start",
                      flexDirection: "row",
                      alignItems: "center",
                      paddingTop: 4,
                      paddingBottom: 4,
                      marginRight: 10
                    }}
                  >
                    <Icon
                      size={20}
                      name={"account"}
                      type="material-community"
                      containerStyle={{ marginRight: 10 }}
                      color={colors.darkblue}
                    />
                    <Text
                      style={{ color: colors.black65, fontSize: 14 }}
                      numberOfLines={1}
                    >
                      {vendorName}
                    </Text>
                  </View>
                )}
                {cityName && (
                  <View
                    style={{
                      alignSelf: "flex-start",
                      flexDirection: "row",
                      alignItems: "center",
                      paddingTop: 4,
                      paddingBottom: 4,
                      marginRight: 10
                    }}
                  >
                    <Icon
                      size={20}
                      name={"office-building"}
                      type="material-community"
                      containerStyle={{ marginRight: 10 }}
                      color={colors.darkblue}
                    />
                    <Text
                      style={{ color: colors.black65, fontSize: 14 }}
                      numberOfLines={1}
                    >
                      {cityName}
                    </Text>
                  </View>
                )}

                <View style={{ flexDirection: "row" }}>
                  <View
                    style={{
                      flex: 0.5,
                      alignSelf: "flex-start",
                      flexDirection: "row",
                      alignItems: "center",
                      paddingTop: 4,
                      paddingBottom: 4
                    }}
                  >
                    <Icon
                      size={20}
                      name={roundTrip ? "check" : "close"}
                      type="material-community"
                      containerStyle={{ marginRight: 5 }}
                      color={roundTrip ? colors.blue : colors.red}
                    />
                    <Text
                      style={{ color: colors.black65, fontSize: 14 }}
                      numberOfLines={1}
                    >
                      {"Round Trip"}
                    </Text>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      paddingTop: 4,
                      paddingBottom: 4,
                      flex: 0.5,
                      marginLeft: 5
                    }}
                  >
                    <Icon
                      size={20}
                      name={isApproved ? "check" : "close"}
                      type="material-community"
                      containerStyle={{ marginRight: 5 }}
                      color={isApproved ? colors.blue : colors.redColor}
                    />
                    <Text
                      style={{ color: colors.black65, fontSize: 14 }}
                      numberOfLines={1}
                    >
                      {"Approved"}
                    </Text>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      paddingTop: 4,
                      paddingBottom: 4,
                      flex: 0.5,
                      marginLeft: 5
                    }}
                  >
                    <Icon
                      size={20}
                      name={isActive ? "check" : "close"}
                      type="material-community"
                      containerStyle={{ marginRight: 5 }}
                      color={isActive ? colors.blue : colors.redColor}
                    />
                    <Text
                      style={{ color: colors.black65, fontSize: 14 }}
                      numberOfLines={1}
                    >
                      {"Active"}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    margin: 10,
    marginTop: 0,
    flexDirection: "row",
    borderWidth: 1,
    backgroundColor: colors.black02,
    borderColor: colors.black15,
    borderRadius: 6
  },
  headingContainer: {
    flex: 1,
    paddingBottom: 4,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start"
  },
  routeContainer: {
    paddingTop: 4,
    paddingBottom: 4
  },
  addressContainer: {
    flexDirection: "row",
    alignItems: "flex-start",
    paddingTop: 4
  }
});

export default VehicleApprovalCard;
