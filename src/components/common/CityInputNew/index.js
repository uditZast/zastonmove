import React, { Component, Fragment } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import Modal from "react-native-modal";
import CustomStyles from "../Styles";
import colors from "../Colors";
import { SearchBar } from "react-native-elements";

class CityInputNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      input: null
    };
  }

  setCity = id => () => {
    const { selectCity } = this.props;
    selectCity(id);
    this.closeModal();
  };

  openModal = () => {
    this.setState({ isVisible: true });
  };

  closeModal = () => {
    this.setState({ isVisible: false, input: null });
  };

  setSearch = text => {
    this.setState({ input: text });
  };

  clearInput = () => {
    this.setState({ input: null });
  };

  renderCity = ({ index, item: city_id }) => {
    const { cities } = this.props;
    const { basicInfo: { name, code } = {} } = cities[city_id] || {};

    return (
      <Fragment key={index}>
        <TouchableOpacity
          style={{
            paddingTop: 10,
            paddingBottom: 10,
            flexDirection: "row",
            borderWidth: 1,
            borderColor: colors.white,
            borderBottomColor: colors.grey
          }}
          onPress={this.setCity(city_id)}
        >
          <View style={{ flex: 1 }}>
            <Text style={{ color: colors.blue }}>{code}</Text>
          </View>
          <Text style={{ color: colors.blue }}>{":"}</Text>
          <View style={{ flex: 4, paddingLeft: 16 }}>
            <Text>{name}</Text>
          </View>
        </TouchableOpacity>
      </Fragment>
    );
  };

  renderCityList = () => {
    const { cities } = this.props;
    let list = Object.keys(cities);
    const { input } = this.state;

    if (input && input.toUpperCase()) {
      const u_input = input.toUpperCase();
      list = list.filter(city_id => {
        const { basicInfo: { name = "", code } = {} } = cities[city_id] || {};
        return (
          name.toUpperCase().includes(u_input) ||
          code.toUpperCase().includes(u_input)
        );
      });
    }
    return (
      <FlatList
        data={[...list]}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderCity}
        keyboardShouldPersistTaps={"handled"}
        showsVerticalScrollIndicator={false}
      />
    );
  };

  render() {
    const { closeModal, openModal } = this;
    const { selectedCity = 0, cities, message, disabled = false } = this.props;
    const { basicInfo: { name = "Select City", code } = {} } =
      cities[selectedCity] || {};
    const { isVisible } = this.state;

    console.log("disbaled cityinput-----------------", disabled);
    return (
      <Fragment>
        <TouchableOpacity
          style={disabled ? CustomStyles.borderDisabled : CustomStyles.border}
          onPress={disabled ? null : openModal}
        >
          <Text style={CustomStyles.labelText}>{message}</Text>
          <View style={CustomStyles.pickerView}>
            {/*<Text>{name}</Text>*/}
            {code && name && (
              <Fragment>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  <View style={{ flex: 1 }}>
                    <Text style={{ color: colors.blue }}>{code}</Text>
                  </View>
                  <Text style={{ color: colors.blue }}>{":"}</Text>
                  <View style={{ flex: 4, paddingLeft: 16 }}>
                    <Text>{name}</Text>
                  </View>
                </View>
              </Fragment>
            )}
            {!code && <Text style={{ color: colors.black25 }}>{name}</Text>}
          </View>
        </TouchableOpacity>
        <Modal
          isVisible={isVisible}
          onBackButtonPress={closeModal}
          onBackdropPress={closeModal}
          animationIn={"slideInUp"}
          style={{
            position: "absolute",
            margin: 0,
            top: 95,
            bottom: 0,
            left: 0,
            right: 0
          }}
        >
          <View style={styles.modalView}>
            <SearchBar
              placeholder="Search Source City..."
              onChangeText={this.setSearch}
              lightTheme
              containerStyle={styles.searchBarContainer}
              value={this.state.input}
              onCancel={this.clearInput}
              onClear={this.clearInput}
            />
            <View style={{ padding: 16, paddingTop: 0, flex: 1 }}>
              {this.renderCityList()}
            </View>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalView: {
    width: "100%",
    flex: 1,
    borderRadius: 4,
    backgroundColor: colors.white,
    alignSelf: "center",
    paddingBottom: 30
  },
  searchBarContainer: {
    backgroundColor: colors.white,
    borderWidth: 0,
    padding: 16
  }
});

export default CityInputNew;
