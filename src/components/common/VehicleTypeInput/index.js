import React, { Component, Fragment } from "react";
import { Platform, Text } from "react-native";
import SelectInput from "react-native-select-input-ios";
import customStyles from "../Styles";
import { View } from "react-native-animatable";
import colors from "../Colors";

class VehicleTypeInput extends Component {
  constructor(props) {
    super(props);
  }

  getVehicles = () => {
    const { vehiclesTypes, selectOne = false } = this.props;
    const options = selectOne
      ? [{ label: "Select Vehicle Type", value: 0 }]
      : [{ label: "All", value: 0 }];
    const vehicleTypesArray = Object.keys(vehiclesTypes);
    for (const vehicle of vehicleTypesArray) {
      const { basicInfo: { name, id } = {} } = vehiclesTypes[vehicle] || {};
      options.push({
        label: name,
        value: id
      });
    }
    return options;
  };

  setVehicle = id => {
    const { selectVehicle } = this.props;
    selectVehicle(id);
  };

  render() {
    const { getVehicles, setVehicle } = this;
    const {
      selectedVehicle = 0,
      selectOne = false,
      disabled = true,
      textCenter = false
    } = this.props;

    return (
      <Fragment>
        <View style={customStyles.border}>
          <Text style={customStyles.labelText}>Vehicle Type</Text>
          <SelectInput
            style={Platform.OS === "ios" ? customStyles.pickerView : null}
            options={getVehicles()}
            mode={"dropdown"}
            enabled={disabled}
            labelStyle={{
              color:
                selectedVehicle === 0 && selectOne
                  ? colors.black25
                  : colors.black,
              ...Platform.select({
                android: {
                  height: 30
                },
                ios: {
                  alignSelf: selectOne
                    ? "flex-start"
                    : textCenter
                    ? "center"
                    : "flex-start"
                }
              })
            }}
            value={selectedVehicle}
            onSubmitEditing={setVehicle}
          />
        </View>
      </Fragment>
    );
  }
}

export default VehicleTypeInput;
