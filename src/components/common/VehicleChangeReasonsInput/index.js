import React, { Component, Fragment } from "react";
import { Platform, Text } from "react-native";
import SelectInput from "react-native-select-input-ios";
import customStyles from "../Styles";
import { View } from "react-native-animatable";
import colors from "../Colors";

class VehicleChangeReasonsInput extends Component {
  constructor(props) {
    super(props);
  }

  getChangeReasons = () => {
    const { vehicleChangeReasons } = this.props;
    const options = [{ label: "Select Vehicle Change Reason", value: 0 }];
    const vehicleChangeReasonsArray = Object.keys(vehicleChangeReasons);
    for (const reason of vehicleChangeReasonsArray) {
      const { value, code } = vehicleChangeReasons[reason] || {};
      options.push({
        label: value,
        value: code
      });
    }
    return options;
  };

  setReason = id => {
    const { selectReason } = this.props;
    selectReason(id);
  };

  render() {
    const { getChangeReasons, setReason } = this;
    const { selectedReason = 0 } = this.props;

    return (
      <Fragment>
        <View style={customStyles.border}>
          <Text style={customStyles.labelText}>Vehicle Change Reason</Text>
          <SelectInput
            style={Platform.OS === "ios" ? customStyles.pickerView : null}
            labelStyle={{
              color: selectedReason === 0 ? colors.black25 : colors.black,
              ...Platform.select({
                android: {
                  height: 30
                },
                ios: {
                  alignSelf: "flex-start"
                }
              })
            }}
            options={getChangeReasons()}
            mode={"dropdown"}
            value={selectedReason}
            onSubmitEditing={setReason}
          />
        </View>
      </Fragment>
    );
  }
}

export default VehicleChangeReasonsInput;
