import React, { Component, Fragment } from "react";
import { Icon } from "react-native-elements";
import colors from "../Colors";

class CustomSliderMarkerRight extends Component {
  render() {
    // console.log('this.props.currentValue ----', this.props.currentValue);
    return (
      <Fragment>
        <Icon
          name="truck"
          type="material-community"
          size={25}
          color={colors.blue}
        />
      </Fragment>
    );
  }
}

export default CustomSliderMarkerRight;
