//Return True if user has right or false
export const hasRights = (right, loggedInUser) => {
  const { rights = [] } = loggedInUser;
  return rights.indexOf(right) !== -1;
};
