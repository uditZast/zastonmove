import React, { Component, Fragment } from "react";
import { Text, View, TextInput } from "react-native";
import CustomStyles from "../Styles";

class InputText extends Component {
  constructor(props) {
    super(props);
    const { initialValue } = props;
    this.state = {
      text: initialValue
    };
  }

  onEndEditing = () => {
    const { updateText } = this.props;
    updateText(this.state.text);
  };

  render() {
    const { message, placeholder } = this.props;

    return (
      <Fragment>
        <View style={CustomStyles.border}>
          <Text style={CustomStyles.labelText}>{message}</Text>
          <TextInput
            placeholder={placeholder}
            style={CustomStyles.pickerView}
            onChangeText={text => this.setState({ text: text })}
            value={this.state.text}
            // placeholderTextColor={"#000"}
            onEndEditing={this.onEndEditing}
            // editable={!readOnly}
          />
        </View>
      </Fragment>
    );
  }
}

export default InputText;
