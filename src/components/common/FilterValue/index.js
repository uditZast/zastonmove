import React, { Component, Fragment } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../Colors";
import { Icon } from "react-native-elements";

class FilterValue extends Component {
  clear = () => {
    const { clearValue } = this.props;
    clearValue();
  };

  render() {
    const { displayValue, iconName } = this.props;

    return (
      <Fragment>
        <View style={styles.container}>
          <View style={{ marginLeft: 5 }}>
            <Icon
              name={iconName}
              type="material-community"
              size={15}
              color={iconName === "bookmark" ? colors.red : colors.white}
            />
          </View>

          <Text style={styles.textStyle}>{displayValue}</Text>

          <TouchableOpacity
            onPress={this.clear}
            style={styles.imageStyle}
            hitSlop={{ top: 30, bottom: 30, left: 70, right: 70 }}
          >
            <Icon
              name="close"
              type="material-community"
              size={15}
              color={colors.white}
            />
          </TouchableOpacity>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    margin: 5,
    borderColor: colors.blue,
    backgroundColor: colors.blue,
    borderWidth: 1,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    justifyContent: "center",
    alignItems: "center"
  },
  imageStyle: {
    width: 23,
    height: 22,
    marginTop: -15,
    alignSelf: "flex-end"
  },
  textStyle: {
    padding: 5,
    justifyContent: "center",
    alignSelf: "center",
    color: colors.white
  }
});

export default FilterValue;
