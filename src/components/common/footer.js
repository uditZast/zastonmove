import React, { Fragment } from "react";
import { TouchableOpacity, Text, ActivityIndicator } from "react-native";
import colors from "./Colors";

export default ({ name, action, loading = false }) => {
  const performAction = () => {
    action();
  };

  return (
    <Fragment>
      <TouchableOpacity
        style={{
          bottom: 0,
          height: 50,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: colors.blue
        }}
        onPress={loading ? null : performAction}
      >
        {loading && <ActivityIndicator size="small" color={colors.white} />}
        {!loading && (
          <Text
            style={{ fontSize: 16, fontWeight: "500", color: colors.white }}
          >
            {name}
          </Text>
        )}
      </TouchableOpacity>
    </Fragment>
  );
};
