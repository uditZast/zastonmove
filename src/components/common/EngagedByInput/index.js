import React, { Component, Fragment } from "react";
import { Platform, Text } from "react-native";
import SelectInput from "react-native-select-input-ios";
import customStyles from "../Styles";
import { View } from "react-native-animatable";
import colors from "../Colors";

class EngagedByInput extends Component {
  constructor(props) {
    super(props);
  }

  getEngagedBy = () => {
    const { engagedBy } = this.props;
    const options = [{ label: "Regular Vehicle", value: 0 }];
    const engagedByArray = Object.keys(engagedBy);
    for (const engageId of engagedByArray) {
      const { basicInfo: { name, id } = {} } = engagedBy[engageId] || {};
      options.push({
        label: name,
        value: id
      });
    }
    return options;
  };

  setEngagedBy = id => {
    const { setEngageReason } = this.props;
    setEngageReason(id);
  };

  render() {
    const { getEngagedBy, setEngagedBy } = this;
    const { selectedEngageReason = 0 } = this.props;

    return (
      <Fragment>
        <View style={customStyles.border}>
          <Text style={customStyles.labelText}>Select Engaged By</Text>
          <SelectInput
            style={Platform.OS === "ios" ? customStyles.pickerView : null}
            options={getEngagedBy()}
            mode={"dropdown"}
            labelStyle={{
              color: selectedEngageReason === 0 ? colors.black25 : colors.black,
              ...Platform.select({
                android: {
                  height: 30
                },
                ios: {
                  alignSelf: "flex-start"
                }
              })
            }}
            value={selectedEngageReason}
            onSubmitEditing={setEngagedBy}
          />
        </View>
      </Fragment>
    );
  }
}

export default EngagedByInput;
