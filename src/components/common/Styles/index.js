import { StyleSheet } from "react-native";
import colors from "../Colors";

export default StyleSheet.create({
  flex1: {
    flex: 1
  },
  row: {
    flexDirection: "row"
  },
  fs16: {
    fontSize: 16
  },
  fs14: {
    fontSize: 14
  },
  border: {
    borderWidth: 1,
    borderRadius: 3,
    borderColor: colors.blue,
    margin: 10,
    minHeight: 40
  },
  borderDisabled: {
    borderWidth: 1,
    borderRadius: 3,
    borderColor: colors.black25,
    margin: 10,
    minHeight: 40
  },
  labelText: {
    marginTop: -10,
    color: colors.blue,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  labelTextDisabled: {
    marginTop: -10,
    color: colors.black25,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  pickerView: {
    flex: 1,
    alignItems: "flex-start",
    paddingLeft: 10,
    justifyContent: "center",
    paddingBottom: 10,
    maxHeight: 30
  },
  scrollContainer: {
    flexGrow: 1,
    paddingTop: 10,
    paddingBottom: 10
  },
  searchBarContainer: {
    backgroundColor: colors.white,
    borderWidth: 0,
    padding: 16
  },
  dashedLine: {
    height: 0,
    borderWidth: 1,
    borderColor: colors.black15,
    marginTop: 6,
    marginBottom: 6,
    borderStyle: "dashed"
  },
  commonModalView: {
    flex: 1,
    width: "100%",
    borderRadius: 4,
    backgroundColor: "#ffffff",
    alignSelf: "center"
  },
  commonButton: {
    bottom: 0,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.blue
  }
});
