import React from "react";
import { Text, View } from "react-native";
import colors from "../Colors";

class OfflineBanner extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { bottom = 0 } = this.props;

    return (
      <View
        style={{
          flexDirection: "row",
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          height: 24,
          position: "absolute",
          left: 0,
          right: 0,
          backgroundColor: colors.black,
          bottom: bottom
        }}
      >
        <Text style={{ color: colors.white }}>No Internet Connection</Text>
      </View>
    );
  }
}

export default OfflineBanner;
