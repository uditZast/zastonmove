import React, { Component, Fragment } from "react";
import { StyleSheet, Text, View } from "react-native";
import colors from "../common/Colors";
import moment from "moment";

// import {Icon} from "react-native-elements";

class PreviousAdvanceRequest extends Component {
  constructor(props) {
    super(props);
  }

  timeSince = date => {
    let seconds = Math.floor((new moment() - date) / 1000);

    // let interval = Math.floor(seconds / 31536000);
    // if (interval > 1) {
    //   return interval + " years";
    // }

    // interval = Math.floor(seconds / 2592000);
    // if (interval > 1) {
    //   return interval + " months";
    // }

    let interval = Math.floor(seconds / 86400);
    if (interval > 1) {
      return interval + " days ago";
    } else {
      return "today";
    }
    // interval = Math.floor(seconds / 3600);
    // if (interval > 1) {
    //   return interval + " hours";
    // }

    // interval = Math.floor(seconds / 60);
    // if (interval > 1) {
    //   return interval + " minutes";
    // }

    // return Math.floor(seconds);
  };

  render() {
    const { amount, paymentDate, uploadedBy, status, reference } = this.props;

    const payDate = moment(paymentDate);

    return (
      <Fragment>
        <View style={styles.cardContainerStyle}>
          <View
            style={{ flexDirection: "row", marginLeft: 10, marginRight: 10 }}
          >
            <View style={{ flex: 0.6 }}>
              <Text
                style={[
                  styles.textStyle,
                  { flex: 0.7, color: colors.black85, fontSize: 14 }
                ]}
              >
                {reference}
              </Text>

              <Text
                style={[
                  styles.textStyle,
                  {
                    flex: 0.7,
                    color: colors.black40,
                    fontSize: 12,
                    paddingBottom: 3
                  }
                ]}
              >
                {new moment(paymentDate).format("LL")}
                {" ( "}
                {this.timeSince(payDate)}
                {" ) "}
              </Text>
            </View>

            <View style={{ flex: 0.4, alignItems: "flex-end" }}>
              <Text
                style={[
                  styles.textStyle,
                  {
                    flex: 0.7,
                    color: colors.black85,
                    fontWeight: "bold",
                    fontSize: 16
                  }
                ]}
              >
                {"₹"} {amount}
              </Text>

              <Text
                style={[
                  styles.textStyle,
                  {
                    fontSize: 12,
                    flex: 0.7,
                    paddingBottom: 3,
                    color:
                      status === "RQ"
                        ? colors.warningColor
                        : status === "RJ"
                        ? colors.red
                        : status === "C"
                        ? colors.black85
                        : status === "D"
                        ? colors.black85
                        : status === "A"
                        ? colors.darkGreen
                        : status === "DL"
                        ? colors.red
                        : null
                  }
                ]}
              >
                {status === "RQ"
                  ? "Requested"
                  : status === "RJ"
                  ? "Rejected"
                  : status === "C"
                  ? "Under Process"
                  : status === "D"
                  ? "Under Process"
                  : status === "A"
                  ? "Payment Done"
                  : status === "DL"
                  ? "Deleted"
                  : ""}
              </Text>
            </View>
          </View>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  cardContainerStyle: {
    borderColor: "#ddd",
    borderRadius: 2,
    borderBottomWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    marginLeft: 5,
    marginRight: 5,
    borderTopLeftRadius: 3,
    backgroundColor: colors.white,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3
  },
  textStyle: {
    color: colors.black65,
    fontFamily: "CircularStd-Book",
    flex: 0.5,
    paddingRight: 5,
    paddingTop: 5
  }
});

export default PreviousAdvanceRequest;
