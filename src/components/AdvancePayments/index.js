import React, { Component, Fragment } from "react";
import Header from "../common/header";
import { ScrollView } from "react-navigation";
import {
  FlatList,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from "react-native";
import colors from "../common/Colors";
import AdvanceTripDetails from "../../containers/AdvanceTripDetails";
import BeneficiaryDetails from "../BeneficiaryDetails";
import PreviousAdvanceRequest from "../PreviousAdvanceRequest";
import AdvanceTripModal from "../../containers/AdvanceTripModal";
import { Icon } from "react-native-elements";
import { SceneMap, SceneRendererProps } from "react-native-tab-view";
import Animated from "react-native-reanimated";

class AdvancePayments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageViewVisible: false,
      documentUrl: null,
      amount: null,
      showModal: false,
      benefName: null,
      bank: null,
      account: null,
      ifsc: null,
      paymentDone: 0,
      paymentUnderProcess: 0,
      advances: [],
      balRemaining: 0,
      otpVerified: true,

      index: 0,
      routes: [
        { key: "first", title: "Trip Details" },
        { key: "second", title: "Beneficiary Details" }
      ]
    };
  }

  componentDidMount() {
    const { requestTripAdvance } = this.props;

    const { trip_advance } = requestTripAdvance;

    const {
      freight,
      benef_name,
      bank_name,
      account,
      ifsc,
      advances,
      payment_done_amount,
      under_process_amount,
      balance_remaining,
      tds_percentage,
      otp_verified,
      balance_message,
      add_advance_allowed,
      first_advance_request_vendor,
      total_trip_charges,
      phone
    } = trip_advance;

    if (otp_verified) {
      this.setState({ otpVerified: otp_verified });
    }

    if (benef_name) {
      this.setState({ benefName: benef_name });
    }

    if (bank_name) {
      this.setState({ bank: bank_name });
    }

    if (account) {
      this.setState({ account });
    }

    if (ifsc) {
      this.setState({ ifsc });
    }

    if (payment_done_amount) {
      this.setState({ paymentDone: payment_done_amount });
    }

    if (under_process_amount) {
      this.setState({ paymentUnderProcess: under_process_amount });
    }

    if (advances) {
      this.setState({ advances });
    }

    if (balance_remaining) {
      this.setState({ balRemaining: balance_remaining });
    }
  }

  updateCode = code => {
    this.setState({ code: code });
  };

  onEndEditing = amount => {
    this.setState({ amount });
  };

  openModal = () => {
    this.setState({ showModal: true });
  };

  closeModal = async () => {
    this.setState({ showModal: false });
  };

  componentDidUpdate(prevProps, prevState) {
    const { requestTripAdvance } = this.props;
    const { trip_advance } = requestTripAdvance;

    const { payment_done_amount, under_process_amount } = trip_advance;
    const { tripAdvance } = prevProps;

    if (prevProps.tripAdvance !== this.props.trip_advance) {
      console.log("---- component did update ----");

      if (payment_done_amount) {
        this.setState({ paymentDone: payment_done_amount });
      }

      if (under_process_amount) {
        this.setState({ paymentUnderProcess: under_process_amount });
      }
    }
  }

  renderPreviousPayment = () => {
    const { requestTripAdvance } = this.props;

    const { trip_advance } = requestTripAdvance;

    const { advances } = trip_advance || {};

    console.log("advances -------------", advances);

    const {
      amount,
      payment_date,
      uploaded_by,
      status,
      customer_reference_number
    } = advances;

    console.log("amount --------------", amount);
    return (
      <PreviousAdvanceRequest
        amount={amount}
        paymentDate={payment_date}
        uploadedBy={uploaded_by}
        status={status}
        reference={customer_reference_number}
      />
    );
  };

  _handleIndexChange = index => this.setState({ index });

  FirstRoute = () => (
    <View>
      <AdvanceTripDetails showCustomer={true} showVendor={false} />
    </View>
  );

  SecondRoute = () => (
    <View>
      <BeneficiaryDetails beneficiaryDetails={this.props.requestTripAdvance} />
    </View>
  );

  _renderScene = SceneMap({
    first: this.FirstRoute,
    second: this.SecondRoute
  });

  renderTabBar = (props: SceneRendererProps & { navigationState: State }) => (
    <View style={styles.tabbar}>
      {this.state.routes.map((route, index: number) => {
        return (
          <TouchableWithoutFeedback
            key={route.key}
            onPress={() => props.jumpTo(route.key)}
          >
            {this.renderItem(props)({ route, index })}
          </TouchableWithoutFeedback>
        );
      })}
    </View>
  );

  renderItem = ({
    navigationState,
    position
  }: {
    navigationState: State,
    position: Animated.Node<number>
  }) => ({ route, index }: { route: Route, index: number }) => {
    const inputRange = navigationState.routes.map((_, i) => i);

    const activeOpacity = Animated.interpolate(position, {
      inputRange,
      outputRange: inputRange.map((i: number) => (i === index ? 1 : 0))
    });
    const inactiveOpacity = Animated.interpolate(position, {
      inputRange,
      outputRange: inputRange.map((i: number) => (i === index ? 0 : 1))
    });

    return (
      <View style={styles.tab}>
        <Animated.View
          style={[
            styles.item,
            styles.inactiveItem,
            { opacity: inactiveOpacity }
          ]}
        >
          <Text style={[styles.label, styles.inactive]}>{route.title}</Text>
        </Animated.View>
        <Animated.View
          style={[styles.item, styles.activeItem, { opacity: activeOpacity }]}
        >
          <Text style={[styles.label, styles.active]}>{route.title}</Text>
        </Animated.View>
      </View>
    );
  };

  render() {
    const { showModal } = this.state;

    const { requestTripAdvance } = this.props;

    const { trip_advance } = requestTripAdvance;

    const {
      advances = [],
      payment_done_amount = 0,
      under_process_amount = 0,
      total_trip_charges = 0
    } = trip_advance || {};

    return (
      <Fragment>
        <Header
          name={"Advance Payments"}
          navigation={this.props.navigation}
          goBack={true}
        />

        <ScrollView
          contentContainerStyle={{ flexGrow: 1, backgroundColor: "#EBE9E9" }}
        >
          {/*<TabView*/}
          {/*  navigationState={this.state}*/}
          {/*  renderScene={this._renderScene}*/}
          {/*  renderTabBar={this.renderTabBar}*/}
          {/*  onIndexChange={this._handleIndexChange}*/}
          {/*  indicatorContainerStyle={{backgroundColor: colors.red}}*/}
          {/*/>*/}

          {/*<Text*/}
          {/*  style={[*/}
          {/*    {*/}
          {/*      fontSize: 14,*/}
          {/*      color: colors.black85,*/}
          {/*      fontFamily: "CircularStd-Book",*/}
          {/*      padding: 5,*/}
          {/*      fontWeight: "300"*/}
          {/*    }*/}
          {/*  ]}*/}
          {/*>*/}
          {/*  Trip Details*/}
          {/*</Text>*/}

          <View style={{ marginTop: 10 }} />
          {/*<AdvanceTripDetails showCustomer={true} showVendor={false}/>*/}

          <Text
            style={[
              {
                fontSize: 16,
                color: colors.black65,
                fontFamily: "CircularStd-Book",
                padding: 5,
                fontWeight: "300",
                marginBottom: 5
              }
            ]}
          >
            Beneficiary Details
          </Text>

          <BeneficiaryDetails
            beneficiaryDetails={this.props.requestTripAdvance}
          />

          <Text
            style={[
              {
                fontSize: 16,
                color: colors.black65,
                fontFamily: "CircularStd-Book",
                padding: 5,
                fontWeight: "300"
              }
            ]}
          >
            Payment Status
          </Text>

          <View
            style={[
              { marginLeft: 10, marginTop: 10, marginBottom: 10 },
              styles.cardContainerStyle
            ]}
          >
            <View
              style={{
                flexDirection: "row",
                paddingLeft: 5,
                alignItems: "center",
                paddingRight: 5,
                paddingTop: 5
              }}
            >
              <Text
                style={[
                  styles.labelTextStyle,
                  { color: colors.black85, paddingLeft: 5 }
                ]}
              >
                Total Trip Charges:{" ₹"}
              </Text>
              <Text
                style={[
                  styles.labelTextStyle,
                  { fontWeight: "bold", color: colors.black85, fontSize: 16 }
                ]}
              >
                {total_trip_charges}
              </Text>
            </View>

            <View
              style={{
                flexDirection: "row",
                paddingLeft: 5,
                paddingRight: 5,
                paddingTop: 5,
                paddingBottom: 5
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  flex: 0.5,
                  alignItems: "center"
                }}
              >
                <Text
                  style={[
                    styles.labelTextStyle,
                    { color: colors.darkGreen, paddingLeft: 5 }
                  ]}
                >
                  Payment Done:{" ₹"}
                </Text>
                <Text
                  style={[
                    styles.labelTextStyle,
                    {
                      fontWeight: "bold",
                      color: colors.darkGreen,
                      fontSize: 16
                    }
                  ]}
                >
                  {payment_done_amount}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  flex: 0.5,
                  alignItems: "center"
                }}
              >
                <Text
                  style={[
                    styles.labelTextStyle,
                    { color: colors.warningColor }
                  ]}
                >
                  Pending :{" ₹"}
                </Text>
                <Text
                  style={[
                    styles.labelTextStyle,
                    {
                      color: colors.warningColor,
                      fontSize: 16,
                      fontWeight: "bold"
                    }
                  ]}
                >
                  {under_process_amount}
                </Text>
              </View>
            </View>
          </View>

          {advances.length > 0 && (
            <Text
              style={{
                fontSize: 16,
                color: colors.black65,
                padding: 5,
                marginBottom: 5,
                fontFamily: "CircularStd-Book"
              }}
            >
              Advance Request{" "}
            </Text>
          )}

          {/*{advances && (*/}
          {/*  <FlatList*/}
          {/*    data={[...advances]}*/}
          {/*    keyboardShouldPersistTaps={"handled"}*/}
          {/*    renderItem={(rowData) => {*/}
          {/*      const {*/}
          {/*        amount,*/}
          {/*        payment_date,*/}
          {/*        uploaded_by,*/}
          {/*        status,*/}
          {/*        customer_reference_number*/}
          {/*      } = rowData;*/}
          {/*      return (*/}
          {/*        <PreviousAdvanceRequest*/}
          {/*          amount={amount}*/}
          {/*          paymentDate={payment_date}*/}
          {/*          uploadedBy={uploaded_by}*/}
          {/*          status={status}*/}
          {/*          reference={customer_reference_number}*/}
          {/*        />*/}
          {/*      );*/}
          {/*      */}
          {/*    }}*/}
          {/*    showsVerticalScrollIndicator={false}*/}
          {/*  />*/}
          {/*)}*/}

          {advances && (
            <View style={{ marginBottom: 5 }}>
              {advances.map(rowData => {
                const {
                  amount,
                  payment_date,
                  uploaded_by,
                  status,
                  customer_reference_number
                } = rowData;
                return (
                  <PreviousAdvanceRequest
                    amount={amount}
                    paymentDate={payment_date}
                    uploadedBy={uploaded_by}
                    status={status}
                    reference={customer_reference_number}
                  />
                );
              })}
            </View>
          )}
        </ScrollView>

        <View
          style={{
            position: "absolute",
            width: 56,
            height: 56,
            alignItems: "center",
            justifyContent: "center",
            right: 20,
            bottom: 20,
            elevation: 8,
            backgroundColor: colors.blue,
            padding: 5,
            borderRadius: 100 / 2
          }}
        >
          <TouchableOpacity onPress={this.openModal}>
            <Icon
              name="plus"
              type="material-community"
              color={colors.white}
              size={30}
            />
          </TouchableOpacity>
        </View>

        {showModal && (
          <AdvanceTripModal
            otpVerified={this.state.otpVerified}
            tripData={this.props.tripData}
            isVisible={this.state.showModal}
            closeModal={this.closeModal}
            balRemaining={this.state.balRemaining}
          />
        )}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  labelTextStyle: {
    fontSize: 14,
    color: colors.black65,
    fontFamily: "CircularStd-Book"
  },

  textInputStyle: {
    flex: 1,
    alignItems: "flex-start",
    paddingLeft: 10,
    justifyContent: "center",
    ...Platform.select({
      ios: { maxHeight: 30, paddingBottom: 10 },
      android: {
        maxHeight: 30,
        paddingTop: 0,
        paddingBottom: 0,
        textAlignVertical: "center"
      }
    })
  },
  cardContainerStyle: {
    borderColor: "#ddd",
    borderWidth: 1,
    borderRadius: 2,
    borderBottomWidth: 1,
    marginLeft: 5,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    borderTopLeftRadius: 3,
    backgroundColor: colors.white,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3
  },
  tabBar: {
    flexDirection: "row"
  },
  tabItem: {
    flex: 1,
    alignItems: "center",
    // backgroundColor: "#E0E0E0",
    padding: 16
  },
  tabContainer: {
    flex: 1
  },
  tabbar: {
    flexDirection: "row",
    justifyContent: "space-between",
    height: 40
  },
  tab: {
    flex: 1,
    alignItems: "center",
    padding: 16,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: "rgba(0, 0, 0, .2)"
  },
  activeItem: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: "#A9A9A9"
  },
  inactiveItem: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: "#C0C0C0"
  },
  active: {
    color: colors.white
  },
  inactive: {
    color: colors.black85
  },
  item: {
    alignItems: "center",
    justifyContent: "center"
  }
});

export default AdvancePayments;

// _renderTabBar = props => {
//   const inputRange = props.navigationState.routes.map((x, i) => i);
//
//   return (
//     <View style={styles.tabBar}>
//       {props.navigationState.routes.map((route, i) => {
//         const color = Animated.color(
//           Animated.round(
//             Animated.interpolate(props.position, {
//               inputRange,
//               outputRange: inputRange.map(inputIndex =>
//                 inputIndex === i ? 255 : 0
//               ),
//             })
//           ),
//           inputRange.map(inputIndex =>
//             inputIndex !== i ? 255 : 0
//           ),
//           inputRange.map(inputIndex =>
//             inputIndex !== i ? 255 : 0
//           )
//         );
//
//         const backgroundColors = Animated.color(
//           Animated.interpolate(props.position, {
//             inputRange,
//             outputRange: inputRange.map(inputIndex =>
//               inputIndex === i ? 160 : 255
//             ),
//           }),
//           inputRange.map(inputIndex =>
//             inputIndex === i ? 160 : 51
//           ),
//           inputRange.map(inputIndex =>
//             inputIndex === i ? 160 : 51
//           )
//         );
//
//         return (
//           <TouchableOpacity
//             style={[styles.tabItem]}
//             onPress={() => this.setState({index: i})}>
//             <Animated.Text style={[{backgroundColor: backgroundColors,padding:16}, {color}]}>{route.title}</Animated.Text>
//           </TouchableOpacity>
//         );
//       })}
//     </View>
//   );
// };
