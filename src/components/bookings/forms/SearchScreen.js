import React, { Component } from "react";
import { Platform, ScrollView, StyleSheet, View } from "react-native";
// import CommonModal from "../../../modal/CommonModal";
import AsyncStorage from "@react-native-community/async-storage";
import { SEARCH } from "../../../constants/Constants";
import colors from "../../../colors/Colors";
import Card from "../../Card";
import axios from "axios";
import { Container } from "../../common/Container";
import Snackbar from "react-native-snackbar";
import CommonModal from "../../../containers/CommonModal";
import CityInput from "../../common/CityInput";
import Footer from "../../common/footer";

let tempData, jsonData;

class SearchScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      customerName: "",
      customerId: "",
      customerCode: "",
      vendorId: "",
      vendorName: "",
      vendorCode: "",
      vehicleName: "",
      vehicleId: "",
      vehicleTypeName: "",
      vehicleTypeId: "",
      fromDate: "",
      toDate: "",
      bookingId: "",
      tripId: "",
      todateTime: "",
      todateTimeArray: [],
      fromdateTime: "",
      fromdateTimeArray: [],
      toshowdatePicker: false,
      fromshowdatePicker: false,
      reference: "",
      sourceCityId: "",
      sourceCityName: "",
      destiCityName: "",
      destiCityId: "",
      bookingHistorySearchReasult: [],
      vehicleListResult: [],
      history_title: "",
      title: "",
      showModal: false,
      editTripReferenceFlag: "",
      value: "",
      flag: "",
      filterData: [],
      originalData: [],

      //new Variables
      selectedSourceCity: 0,
      selectedCustomer: 0,
      selectedDestinationCity: 0,
      selectedVehicle: 0,
      selectedType: 0
    };
  }

  componentDidMount() {
    console.log("test");

    // this.setState({jsonData: this.props.products.products}, () => console.log(JSON.stringify('JSON DATA :::: ' + this.state.jsonData)))
  }

  renderModal() {
    console.log(
      "Filter Data in SEARCH :::::::::::::::::::::" +
        JSON.stringify(this.state.filterData)
    );

    if (this.state.filterData.length > 0) {
      return (
        <CommonModal
          display={this.state.showModal}
          closeModal={() => {
            this.setState({ showModal: false });
          }}
          SelectedValue={this.state.selectedValue}
          title={this.state.title}
          tickVisible={this.state.tickVisible}
          filterSearch={this.filterSearch.bind(this)}
          filterData={this.state.filterData}
        />
      );
    } else {
      return null;
    }
  }

  filterSearch(text) {
    if (text !== "") {
      const jsonData_filter = this.state.originalData.filter(item => {
        let itemData = "";
        if (this.state.title === "Vehicle") {
          itemData = item.number + item.vehicle_type;
        } else if (this.state.title === "Vehicle Type") {
          itemData = item.name.toUpperCase();
        } else {
          itemData = item.code.toUpperCase() + item.name.toUpperCase();
        }
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });

      this.setState({
        text: text,
        filterData: jsonData_filter,
        vendorStr: text // after filter we are setting users to new array
      });
    } else {
      this.setState({
        filterData: this.state.originalData
      });
    }
  }

  getSearchResult() {
    if (
      this.state.bookingId === "" &&
      this.state.tripId === "" &&
      this.state.vehicleId === "" &&
      this.state.customerId === "" &&
      this.state.sourceCityId === "" &&
      this.state.destiCityId === "" &&
      this.state.fromdateTime === "" &&
      this.state.todateTime === ""
    ) {
      this.setState({ isLoading: false });

      return Snackbar.show({
        title: "Fields cannot be empty",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      AsyncStorage.getItem("token").then(value => {
        this.setState({ value: value });
        axios
          .post(
            SEARCH,
            {
              booking_code: this.state.bookingId,
              trip_code: this.state.tripId,
              reference: this.state.reference,
              vehicle: this.state.vehicleId,
              customer: this.state.customerId,
              source_city: this.state.sourceCityId,
              destination_city: this.state.desticityId,
              from_date: this.state.fromdateTime,
              to_date: this.state.todateTime
            },
            {
              headers: {
                Authorization: "Token " + value,
                "Content-Type": "application/json"
              }
            }
          )
          .then(response => {
            // console.log(response.data);
            // console.log("reject message" + response.data);
            if (response.data.error === false) {
              this.setState(
                {
                  searchData: response.data.data,
                  bookingHistorySearchReasult: response.data.history,
                  commentsSearchResult: response.data.comments,
                  vehicleListResult: response.data.vehicle_list,
                  history_title: response.data.history_title,
                  title: response.data.title,
                  editTripReferenceFlag: response.data.flag,
                  isLoading: false
                },
                () =>
                  this.props.navigation.navigate("SearchResult", {
                    title: response.data.title,
                    history_title: this.state.history_title,
                    searchData: this.state.searchData,
                    bookingHistorySearchReasult: this.state
                      .bookingHistorySearchReasult,
                    commentsSearchResult: this.state.commentsSearchResult,
                    vehicleListResult: this.state.vehicleListResult,
                    editTripReferenceFlag: this.state.editTripReferenceFlag,
                    token: this.state.value
                  })
              );
              console.log(
                "Title :::::::::" +
                  this.state.title +
                  " Search title ::::::" +
                  response.data.title
              );
            } else {
            }
          })
          .catch(error => {
            // console.log("catch" + error);
            if (error.response) {
              if (error.response.status === 401) {
                AsyncStorage.removeItem("token");
                this.props.navigation.navigate("Auth");
              } else if (error.response.status === 400) {
                // console.log('error 400:::::::' + error.response)
              } else {
                // console.log("error" + error);
              }
            }
          });
      });
    }
  }

  clearState() {
    this.setState({
      isLoading: false,
      customerName: "",
      customerId: "",
      customerCode: "",
      vendorId: "",
      vendorName: "",
      vendorCode: "",
      vehicleName: "",
      vehicleId: "",
      vehicleTypeName: "",
      vehicleTypeId: "",
      fromDate: "",
      toDate: "",
      bookingId: "",
      tripId: "",
      todateTime: "",
      fromdateTime: "",
      toshowdatePicker: false,
      fromshowdatePicker: false,
      reference: "",
      sourceCityId: "",
      sourceCityName: "",
      destiCityName: "",
      destiCityId: "",
      history_title: "",
      title: "",
      showModal: false,
      editTripReferenceFlag: ""
    });
  }

  selectSourceCity = city => {
    this.setState({ selectedSourceCity: city });
  };

  selectCustomer = customer => {
    this.setState({ selectedCustomer: customer });
  };
  selectVehicle = vehicle => {
    this.setState({ selectedVehicle: vehicle });
  };
  selectVType = vtype => {
    this.setState({ selectedVType: vtype });
  };

  render() {
    console.log("this.props.haltReason ----", this.props.haltReason);
    return (
      <Container>
        <View>
          <ScrollView
            style={{ marginBottom: 10 }}
            keyboardShouldPersistTaps="always"
            showsVerticalScrollIndicator={false}
          >
            <Card>
              <View style={{ flex: 1 }}>
                <CityInput
                  cities={this.props.cities}
                  selectedCity={this.state.selectedSourceCity}
                  selectCity={this.selectSourceCity}
                  message={"Source City"}
                />
              </View>

              <Footer name={"SAVE"} />
            </Card>
          </ScrollView>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  customerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 20,
    flex: 1,
    paddingLeft: 5,
    paddingRight: 5,
    alignItems: "flex-start"
  },
  customerStyleInput: {
    fontSize: 12,
    color: colors.border_grey,
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
    fontFamily: "CircularStd-Book",
    marginLeft: 5,
    ...Platform.select({
      ios: {
        padding: 5,
        overflow: "hidden"
      },
      android: {
        padding: 5
      }
    })
  },
  phcustomerStyle: {
    marginTop: -10,
    color: colors.dark_grey,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  StyleInput: {
    fontSize: 12,
    color: colors.textcolor,
    paddingLeft: 5,
    marginLeft: 5,
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center",
    ...Platform.select({
      ios: {
        padding: 5,
        overflow: "hidden"
      },
      android: {
        padding: 0
      }
    })
  },
  buttonStyle: {
    color: colors.white,
    backgroundColor: colors.blueColor,
    fontSize: 12,
    justifyContent: "flex-end",
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    padding: 10,
    textAlign: "center",
    fontFamily: "CircularStd-Book",
    marginBottom: 20
  },
  selectedTextColor: {
    fontSize: 12,
    color: colors.textcolor,
    marginLeft: 5,
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
    fontFamily: "CircularStd-Book",
    ...Platform.select({
      ios: {
        padding: 5,
        overflow: "hidden"
      },
      android: {
        padding: 10
      }
    })
  }
});

// const mapStateToProps = state => ({
//     state: state,
//     error: state.error,
//     products: state.productsReducer.products,
//     pending: state.productsReducer.pending,
// });
//
//
// export default connect(
//     mapStateToProps,
//     null
// )(SearchScreen);
//
//

export default SearchScreen;
