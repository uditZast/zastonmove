import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../../../colors/Colors";
import Card from "../../Card";
import { ScrollView } from "react-navigation";
import CardOptions from "../bookingactions/CardOptions";
import ReferenceModal from "../../../modal/ReferenceModal";
import AsyncStorage from "@react-native-community/async-storage";
import { Container } from "../../common/Container";

let searchData = [],
  bookinghistoryResult = [],
  commentsSearchResult = [],
  vehicleListResult = [],
  history_title,
  title,
  editTripReferenceFlag = 0,
  token;

export default class SearchResultScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      actionsArray: [],
      showReferenceModal: false,
      showHistoryCard: false,
      showCommentCard: false,
      rightsArray: []
    };
  }

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;

    searchData = params.searchData;
    commentsSearchResult = params.commentsSearchResult;
    history_title = params.history_title;
    title = params.title;
    token = params.token;

    if (
      params.bookingHistorySearchReasult !== null &&
      params.bookingHistorySearchReasult !== undefined
    ) {
      bookinghistoryResult = params.bookingHistorySearchReasult;
    } else {
    }

    if (
      params.vehicleListResult !== null &&
      params.vehicleListResult !== undefined
    ) {
      vehicleListResult = params.vehicleListResult;
      console.log(
        "vehicleListResult Search Result Length" + vehicleListResult.length
      );
    } else {
      console.log("vehicleList Search : else part");
    }

    if (
      params.editTripReferenceFlag !== null &&
      params.editTripReferenceFlag !== undefined
    ) {
      editTripReferenceFlag = params.editTripReferenceFlag;
      console.log("edit reference flag ::::::::::" + editTripReferenceFlag);
    } else {
    }

    return {
      // title: navigation.getParam('Title', params ? tripId : ''),
      headerTitle: "Search Details",
      headerStyle: {
        backgroundColor: navigation.getParam(
          "BackgroundColor",
          colors.blueColor
        )
      },
      headerTintColor: navigation.getParam("HeaderTintColor", "#fff"),
      //Text color of ActionBar
      headerTitleStyle: {
        fontWeight: "bold",
        fontSize: 13,
        fontFamily: "CircularStd-Book"
      }
    };
  };

  componentDidMount() {
    AsyncStorage.getItem("rights").then(value =>
      this.setState({ rightsArray: value })
    );
    this.setState({ actionsArray: searchData });
  }

  componentWillUnmount() {
    console.log("component will unmount");
    vehicleListResult = [];
    bookinghistoryResult = [];
    commentsSearchResult = [];
    searchData = [];
    editTripReferenceFlag = 0;
  }

  getBookingHistoryDetails() {
    console.log("booking BookingHistory details");
    return (
      <View>
        <Text style={styles.headingTextStyle}>{history_title}</Text>
        {bookinghistoryResult.map(rowData => {
          return (
            <Card key={rowData.id}>
              {rowData.user !== null && rowData.user !== "" ? (
                <View style={styles.rowStyle}>
                  <Text style={styles.commentsTextStyle}>User</Text>
                  <Text style={styles.commentsTextStyle1}>{rowData.user}</Text>
                </View>
              ) : null}

              {rowData.status !== null && rowData.status !== "" ? (
                <View style={styles.rowStyle}>
                  <Text style={styles.commentsTextStyle}>Status</Text>
                  <Text style={styles.commentsTextStyle1}>
                    {rowData.status}
                  </Text>
                </View>
              ) : null}
              {rowData.action !== null && rowData.action !== "" ? (
                <View style={styles.rowStyle}>
                  <Text style={styles.commentsTextStyle}>Status</Text>
                  <Text style={styles.commentsTextStyle1}>
                    {rowData.action}
                  </Text>
                </View>
              ) : null}
              {rowData.time !== null && rowData.time !== "" ? (
                <View style={styles.rowStyle}>
                  <Text style={styles.commentsTextStyle}>Time</Text>
                  <Text style={styles.commentsTextStyle1}>{rowData.time}</Text>
                </View>
              ) : null}
            </Card>
          );
        })}
      </View>
    );
  }

  getCommentsData() {
    return (
      <View>
        <Text style={styles.headingTextStyle}>Comments</Text>

        <View>
          {commentsSearchResult.map(rowData => {
            return (
              <Card key={rowData.id}>
                {rowData.user !== null && rowData.user !== "" ? (
                  <View style={styles.rowStyle}>
                    <Text style={styles.commentsTextStyle}>User</Text>
                    <Text style={styles.commentsTextStyle1}>
                      {rowData.user}
                    </Text>
                  </View>
                ) : null}

                {rowData.comment !== null && rowData.comment !== "" ? (
                  <View style={styles.rowStyle}>
                    <Text style={styles.commentsTextStyle}>Status</Text>
                    <Text style={styles.commentsTextStyle1}>
                      {rowData.comment}
                    </Text>
                  </View>
                ) : null}
              </Card>
            );
          })}
        </View>
      </View>
    );
  }

  getVehicleListData() {
    return (
      <View>
        <Text style={styles.headingTextStyle}>Vehicle Details</Text>
        {vehicleListResult.map(rowData => {
          return (
            <Card key={rowData.number}>
              {rowData.number !== null && rowData.number !== "" ? (
                <View style={styles.rowStyle}>
                  <Text style={styles.commentsTextStyle}>Vehicle</Text>
                  <Text style={styles.commentsTextStyle1}>
                    {rowData.number} {" (" + rowData.type + " )"}
                  </Text>
                </View>
              ) : null}
              {rowData.vendor !== null && rowData.vendor !== "" ? (
                <View style={styles.rowStyle}>
                  <Text style={styles.commentsTextStyle}>Vendor</Text>
                  <Text style={styles.commentsTextStyle1}>
                    {rowData.vendor}
                  </Text>
                </View>
              ) : null}
              {rowData.phone !== null && rowData.phone !== "" ? (
                <View style={styles.rowStyle}>
                  <Text style={styles.commentsTextStyle}>Phone</Text>
                  <Text style={styles.commentsTextStyle1}>{rowData.phone}</Text>
                </View>
              ) : null}
            </Card>
          );
        })}
      </View>
    );
  }

  renderActions(index) {
    let actionsArray = this.state.actionsArray;

    actionsArray.forEach((option, thisindex) => {
      actionsArray[thisindex]["actions"] = thisindex === index;
    });

    this.setState({
      actionsArray,
      selectedIndex: index
    });

    console.log(
      "index renderactions ===================================" + index
    );
    console.log("selected index " + this.state.selectedIndex);
  }

  renderListActions(element, index) {
    if (index === 0) {
      return (
        <CardOptions
          visible={
            element.hasOwnProperty("actions") ? element.actions : index === 0
          }
          onCommentTapNav={() =>
            this.props.navigation.navigate("TripComments", {
              bookingId: null,
              tripId: element.t_code
            })
          }
          screen="searchTripDetails"
          change="1"
          onReferenceTap={() => this.setState({ showReferenceModal: true })}
          editTripReferenceFlag={editTripReferenceFlag}
          onHistoryTap={() =>
            this.setState(
              {
                showHistoryCard: true,
                showCommentCard: false
              },
              () =>
                console.log(
                  "show History card updated" + this.state.showHistoryCard
                )
            )
          }
          onCommentTap={() =>
            this.setState(
              {
                showCommentCard: true,
                showHistoryCard: false
              },
              () =>
                console.log(
                  "show comment card updated" + this.state.showCommentCard
                )
            )
          }
          onEditTap={() =>
            this.props.navigation.navigate("AdHocOne", {
              bookingId: element.b_code,
              action: "edit",
              from: "search"
            })
          }
          locationKey="1"
          onLocationTap={() => this.props.navigation.navigate("LocationScreen")}
          rightsArray={this.state.rightsArray}
        />
      );
    } else {
      return (
        <CardOptions
          visible={
            element.hasOwnProperty("actions") ? element.actions : index === 0
          }
          onCommentTapNav={() =>
            this.props.navigation.navigate("TripComments", {
              bookingId: null,
              tripId: element.t_code
            })
          }
          screen="searchTripDetails"
          change="1"
          onReferenceTap={() => this.setState({ showReferenceModal: true })}
          editTripReferenceFlag={editTripReferenceFlag}
          onHistoryTap={() =>
            this.setState(
              {
                showHistoryCard: true,
                showCommentCard: false
              },
              () =>
                console.log(
                  "show History card updated" + this.state.showHistoryCard
                )
            )
          }
          onCommentTap={() =>
            this.setState(
              {
                showCommentCard: true,
                showHistoryCard: false
              },
              () =>
                console.log(
                  "show comment card updated" + this.state.showHistoryCard
                )
            )
          }
          onEditTap={() =>
            this.props.navigation.navigate("AdHocOne", {
              bookingId: element.b_code,
              action: "edit",
              from: "search"
            })
          }
          rightsArray={this.state.rightsArray}
        />
      );
    }
  }

  // renderListActions(element, index) {
  //     return (
  //         <CardOptions
  //             visible={element.hasOwnProperty('actions') ? element.actions : index === 0}
  //             onHistoryTapNav={() => this.props.navigation.navigate('BookingHistory', {
  //                 bookingId: element.b_code,
  //                 tripId: null
  //             })}
  //             onCommentTapNav={() => this.props.navigation.navigate('TripComments', {
  //                 bookingId: null,
  //                 tripId: element.t_code
  //             })}
  //             screen='searchTripDetails'
  //             change='1'
  //             onReferenceTap={() => this.setState({showReferenceModal: true})}
  //             editTripReferenceFlag={editTripReferenceFlag}
  //             onHistoryTap={() => this.setState({
  //                 showHistoryCard: true,
  //                 showCommentCard: false
  //             }, () => console.log('show History card updated' + this.state.showHistoryCard))}
  //             onCommentTap={() => this.setState({
  //                 showCommentCard: true,
  //                 showHistoryCard: false
  //             }, () => console.log('show comment card updated' + this.state.showHistoryCard))}
  //             onEditTap={() => this.props.navigation.navigate('AdHocOne', {
  //                 bookingId: element.b_code,
  //                 action: 'edit',
  //                 from: 'search'
  //             })}
  //             rightsArray={this.state.rightsArray}
  //         />
  //     );
  // }

  renderReferenceModal(value) {
    return (
      <ReferenceModal
        display={this.state.showReferenceModal}
        closeModal={() => {
          this.setState({ showReferenceModal: false });
        }}
        tripId={value}
        token={token}
      />
    );
  }

  render() {
    return (
      <Container>
        <View style={{ flex: 1, backgroundColor: colors.light_grey }}>
          <ScrollView>
            <Card>
              <Text style={styles.headingTextStyle}>{title}</Text>
              {searchData.map((rowData, index) => {
                return (
                  <Card key={rowData.b_code}>
                    <View style={{ flexDirection: "row" }}>
                      <TouchableOpacity
                        onPress={this.renderActions.bind(this, index)}
                        activeOpacity={0.7}
                      >
                        <View style={styles.rowStyle}>
                          {rowData.t_code !== "-" ? (
                            <Text style={styles.codeTextStyle}>
                              {rowData.t_code} {" (" + rowData.type + ") "}
                            </Text>
                          ) : (
                            <Text style={styles.codeTextStyle}>
                              {rowData.b_code} {" (" + rowData.type + ") "}
                            </Text>
                          )}

                          {rowData.route !== null && rowData.route !== "" ? (
                            <View style={{ alignItems: "flex-end" }}>
                              <Text
                                style={[
                                  styles.codeTextStyle,
                                  { marginLeft: 5 }
                                ]}
                              >
                                {rowData.route}
                              </Text>
                            </View>
                          ) : null}

                          <View style={styles.lineStyle} />
                        </View>

                        {this.renderReferenceModal(rowData.t_code)}

                        {rowData.t_code !== "-" ? (
                          <View style={styles.rowStyle}>
                            <Text style={styles.section2textStyle}>
                              Booking Code
                            </Text>
                            <Text
                              style={[
                                styles.section2textStyle,
                                { paddingLeft: 5 }
                              ]}
                            >
                              {rowData.b_code}
                            </Text>
                          </View>
                        ) : null}

                        {rowData.cust !== null && rowData.cust !== "" ? (
                          <View style={styles.rowStyle}>
                            <Text style={styles.section2textStyle}>
                              Customer
                            </Text>
                            <Text
                              style={[
                                styles.section2textStyle,
                                { paddingLeft: 5 }
                              ]}
                            >
                              {rowData.cust}
                            </Text>
                          </View>
                        ) : null}

                        {rowData.vehicle !== null && rowData.vehicle !== "" ? (
                          <View style={styles.rowStyle}>
                            <Text style={styles.section2textStyle}>
                              Vehicle
                            </Text>
                            <Text
                              style={[
                                styles.section2textStyle,
                                { paddingLeft: 5 }
                              ]}
                            >
                              {rowData.vehicle}
                            </Text>
                          </View>
                        ) : null}

                        {rowData.vendor !== null && rowData.vendor !== "" ? (
                          <View style={styles.rowStyle}>
                            <Text style={styles.section2textStyle}>Vendor</Text>
                            <Text
                              style={[
                                styles.section2textStyle,
                                { paddingLeft: 5 }
                              ]}
                            >
                              {rowData.vendor}
                            </Text>
                          </View>
                        ) : null}

                        {rowData.time !== null && rowData.time !== "" ? (
                          <View style={styles.rowStyle}>
                            <Text style={styles.section2textStyle}>Time</Text>
                            <Text
                              style={[
                                styles.section2textStyle,
                                { paddingLeft: 5 }
                              ]}
                            >
                              {rowData.time}
                            </Text>
                          </View>
                        ) : null}

                        {rowData.docking_hours !== null &&
                        rowData.docking_hours !== "" ? (
                          <View style={styles.rowStyle}>
                            <Text style={styles.section2textStyle}>
                              Docking Hours
                            </Text>
                            <Text
                              style={[
                                styles.section2textStyle,
                                { paddingLeft: 5 }
                              ]}
                            >
                              {rowData.docking_hours}
                            </Text>
                          </View>
                        ) : null}

                        {rowData.tat !== null && rowData.tat !== "" ? (
                          <View style={styles.rowStyle}>
                            <Text style={styles.section2textStyle}>
                              Expected TAT
                            </Text>
                            <Text
                              style={[
                                styles.section2textStyle,
                                { paddingLeft: 5 }
                              ]}
                            >
                              {rowData.tat}
                            </Text>
                          </View>
                        ) : null}

                        {rowData.status !== null && rowData.status !== "" ? (
                          <View style={styles.rowStyle}>
                            <Text style={styles.section2textStyle}>Status</Text>
                            <Text
                              style={[
                                styles.section2textStyle,
                                { paddingLeft: 5 }
                              ]}
                            >
                              {rowData.status}
                            </Text>
                          </View>
                        ) : null}
                      </TouchableOpacity>
                    </View>

                    {this.renderListActions(rowData, index)}
                  </Card>
                );
              })}
            </Card>

            {this.state.showHistoryCard === true &&
            bookinghistoryResult.length > 0
              ? this.getBookingHistoryDetails()
              : null}

            {this.state.showCommentCard === true &&
            commentsSearchResult.length > 0
              ? this.getCommentsData()
              : null}

            {vehicleListResult !== null &&
            vehicleListResult !== undefined &&
            vehicleListResult.length > 0
              ? this.getVehicleListData()
              : null}
          </ScrollView>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    marginTop: 8,
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 5
  },
  rowStyle: {
    flex: 1,
    flexDirection: "row",
    padding: 5,
    marginLeft: 10
  },
  section2textStyle: {
    flex: 0.5,
    fontFamily: "CircularStd-Book",
    fontSize: 12,
    color: colors.textcolor
  },
  commentsTextStyle: {
    flex: 0.2,
    fontFamily: "CircularStd-Book",
    fontSize: 12,
    color: colors.textcolor,
    marginLeft: 10
  },
  commentsTextStyle1: {
    flex: 0.8,
    fontFamily: "CircularStd-Book",
    fontSize: 12,
    color: colors.textcolor,
    marginLeft: 10
  },
  headingTextStyle: {
    fontFamily: "CircularStd-Book",
    fontSize: 14,
    fontWeight: "bold",
    color: colors.blueColor,
    textAlign: "center",
    padding: 10
  },
  codeTextStyle: {
    fontFamily: "CircularStd-Book",
    fontSize: 12,
    fontWeight: "bold",
    color: colors.blueColor
  }
});
