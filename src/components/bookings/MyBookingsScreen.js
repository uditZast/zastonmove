import React, { Component } from "react";
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { MY_BOOKINGS } from "../../constants/Constants";
import CardOptions from "./bookingactions/CardOptions";
import Card from "../Card";
import colors from "../../colors/Colors";
import { BarIndicator } from "react-native-indicators";
import NetInfo from "@react-native-community/netinfo";
import Snackbar from "react-native-snackbar";
import axios from "axios";

export default class MyBookingsScreen extends Component {
  state = { data: [], values: "" };

  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      OriginalData: [],
      actionsArray: [],
      isLoading: true,
      showActions: false,
      selectedIndex: 0
    };
  }

  componentDidMount() {
    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        AsyncStorage.getItem("token").then(value => {
          console.log("token:::::::" + value);
          this.setState({ values: value });
          axios
            .get(MY_BOOKINGS, {
              headers: {
                Authorization: "Token " + value,
                "Content-Type": "application/json"
              }
            })
            .then(responseJson => {
              console.log("data::::" + JSON.stringify(responseJson));
              this.setState({
                dataSource: responseJson.data.data,
                OriginalData: responseJson.data.data,
                actionsArray: responseJson.data.data,
                isLoading: false
              });
            })
            .catch(error => {
              console.log("catch new request" + JSON.stringify(error));
              console.log("catch new request : " + error);
              if (error.response.status === 401) {
                AsyncStorage.removeItem("token");
                this.props.navigation.navigate("Auth");
              } else {
              }
            });
        });
      } else {
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });
  }

  //actions menu

  renderActions(index) {
    let actionsArray = this.state.actionsArray;

    actionsArray.forEach((option, thisindex) => {
      actionsArray[thisindex]["actions"] = thisindex === index;
    });

    this.setState({ actionsArray, selectedIndex: index });
  }

  renderListActions(element, index) {
    return (
      <CardOptions
        visible={
          element.hasOwnProperty("actions") ? element.actions : index === 0
        }
        screen="docked"
        change="1"
      />
    );
  }

  filterSearch(text) {
    console.log("filter " + text);
    const dataSource_filter = this.state.OriginalData.filter(item => {
      const itemData =
        item.booking_code.toUpperCase() +
        item.route.toUpperCase() +
        item.vehicle +
        item.v_type;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      text: text,
      dataSource: dataSource_filter // after filter we are setting users to new array
    });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <BarIndicator color={colors.blueColor} size={20} />
        </View>
      );
    }

    return (
      <ScrollView style={styles.container}>
        <View style={{ backgroundColor: colors.white }}>
          <TextInput
            style={styles.customerBorderStyle}
            onChangeText={text => this.filterSearch(text)}
            value={this.state.text}
            placeholderTextColor={"#9D9B9D"}
            placeholder="Search BookingId/ Vehicle/ Vendor/ Route"
          />
        </View>

        {this.state.dataSource.map((rowData, index) => {
          console.log("dataSource" + this.state.dataSource.length);
          console.log("codeee:::::" + rowData.code);
          console.log("codeee:::::" + index);

          return (
            <Card key={rowData.booking_code}>
              <TouchableOpacity onPress={this.renderActions.bind(this, index)}>
                <View
                  style={
                    this.state.selectedIndex === index
                      ? styles.section1ColorChangeStyle
                      : styles.section1Style
                  }
                >
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ flex: 0.7 }}>
                      <View style={styles.forwardStyle}>
                        <Text style={styles.boldTextStyle}>
                          {rowData.booking_code}
                        </Text>
                        <Text style={styles.normalTextStyle}>
                          {rowData.b_type}
                        </Text>
                      </View>

                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <Text style={[styles.textStyle1, { paddingTop: 5 }]}>
                          {rowData.vehicle}
                        </Text>
                        <Text style={[styles.textStyle1, { paddingTop: 5 }]}>
                          {rowData.v_type}
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        flex: 0.3,
                        justifyContent: "center",
                        alignSelf: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text
                        style={[
                          styles.textStyle1,
                          {
                            textAlignVertical: "center",
                            paddingTop: 5,
                            paddingBottom: 3
                          }
                        ]}
                      >
                        {rowData.booking_status}
                      </Text>
                    </View>
                  </View>
                </View>

                <View
                  style={
                    this.state.selectedIndex === index
                      ? [styles.lineStyle, { backgroundColor: colors.blue }]
                      : styles.lineStyle
                  }
                />
                <View
                  style={
                    this.state.selectedIndex === index
                      ? styles.section2ColorChangeStyle
                      : styles.section2Style
                  }
                >
                  {/*<View><Text*/}
                  {/*    style={[styles.textStyle1, {paddingTop: 5}]}>{rowData.vehicle + ' ' + rowData.v_type + ' ' + rowData.customer}</Text></View>*/}

                  <View style={{ flex: 1, flexDirection: "row", marginTop: 3 }}>
                    <Text style={[styles.textStyle1, { flex: 0.3 }]}>
                      Route
                    </Text>
                    <Text
                      style={[
                        styles.textStyle1,
                        {
                          flex: 0.7,
                          color: colors.grey
                        }
                      ]}
                    >
                      {rowData.route}
                    </Text>
                  </View>

                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <Text style={[styles.textStyle1, { flex: 0.3 }]}>
                      Customer
                    </Text>
                    <Text
                      style={[
                        styles.textStyle1,
                        {
                          flex: 0.7,
                          color: colors.grey
                        }
                      ]}
                    >
                      {rowData.customer}
                    </Text>
                  </View>

                  <View style={[styles.rowStyle1, { marginBottom: 5 }]}>
                    <Text style={[styles.textStyle1, { flex: 0.3 }]}>
                      Expected Tat
                    </Text>

                    <Text
                      style={[
                        styles.textStyle1,
                        {
                          flex: 0.7,
                          color: colors.grey
                        }
                      ]}
                    >
                      {rowData.tat}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>

              {this.renderListActions(rowData, index)}
            </Card>
          );
        })}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.listbackgroundcolor
  },
  forwardStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    paddingTop: 3
  },
  boldTextStyle: {
    fontSize: 15,
    fontWeight: "bold",
    color: colors.blueColor,
    fontFamily: "CircularStd-Book"
  },
  normalTextStyle: {
    fontSize: 12,
    color: colors.textcolor,
    marginLeft: 5,
    paddingTop: 3,
    fontFamily: "CircularStd-Book"
  },
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 5
  },
  rowStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 5
  },
  rowStyle1: {
    flex: 0.5,
    flexDirection: "row",
    marginRight: 10,
    paddingTop: 3,
    paddingBottom: 3
  },
  textStyle1: {
    fontSize: 12,
    color: colors.textcolor,
    fontFamily: "CircularStd-Book",
    marginLeft: 10
  },
  textStyle2: {
    fontSize: 12,
    color: colors.textcolor,
    paddingTop: 3,
    fontFamily: "CircularStd-Book"
  },
  rowStyle2: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 0
  },
  customerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    color: colors.dark_grey,
    ...Platform.select({
      ios: {
        padding: 10,
        borderRadius: 10,
        overflow: "hidden"
      },
      android: {
        paddingTop: 5,
        paddingLeft: 12,
        paddingRight: 12,
        paddingBottom: 5,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3
      }
    })
  },
  section1Style: {
    flex: 1,
    flexDirection: "row"
  },
  section2Style: {
    flex: 1
  },
  section1ColorChangeStyle: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: colors.blue
  },
  section2ColorChangeStyle: {
    flex: 1,
    backgroundColor: colors.blue
  }
});
