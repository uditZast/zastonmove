import React, {Component} from 'react';
import {ActivityIndicator, Alert, ScrollView, Text, TextInput, TouchableOpacity, View} from "react-native";
import colors from "../../../colors/Colors";
import CommonModal from "../../../modal/CommonModal";
import AsyncStorage from "@react-native-community/async-storage";
import {
    CREATE_BOOKING,
    CREATE_BOOKING_DETAILS,
    GET_BOOKING_DETAILS,
    UPDATE_BOOKING
} from "../../../constants/Constants";
import axios from "axios";
import Snackbar from 'react-native-snackbar';
import Card from "../../Card";
import DateTimePicker from "react-native-modal-datetime-picker";
import {Icon} from "react-native-elements";

let jsonData, autopopulateData, booking_type, bookingId, action, URL, returnBookingType, forwardBookingType,
    isLoadingData = true, headingText, from;

export default class CreateBookingForms extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            showModal: false,
            showVendorModal: false,
            showVehicleModal: false,
            showSourceCityModal: false,
            showDestiCityModal: false,
            showVehicleTypeModal: false,
            showViaModal: false,
            jsonData: [],
            customerName: '',
            customerId: '',
            customerCode: '',
            vendorName: '',
            vendorId: '',
            vendorStr: '',
            vendorCode: '',
            vehicleName: '',
            vehicleId: '',
            sourcecityName: '',
            sourcecityId: '',
            sourcecitycode: '',
            desticityName: '',
            desticityId: '',
            desticitycode: '',
            vehicletypeid: '',
            vehicletypename: '',
            viacityid: '',
            viaSelectedCityId: '',
            viacityName: '',
            viacityCode: '',
            viaList: [],
            expectedtat: '',
            driverPhone: '',
            dynamicRow: false,
            route: '',
            fullRoute: '',
            error: '',
            customerRouteArray: [],
            tatEdit: true,
            via_array: [{}],
            viaIndex: -1,
            date: '',
            time: '',
            dateTime: '',
            dateTimeArray: [],
            showdatePicker: false,
            tatWarning: false,
            changeViaText: '',
            viaID: '',
            distance: 0,
            submitMessage: '',
            vendorApFlag: '',

            bookingStatus: '',

            showModalReturn: false,
            showVendorModalReturn: false,
            showVehicleModalReturn: false,
            showSourceCityModalReturn: false,
            showDestiCityModalReturn: false,
            showVehicleTypeModalReturn: false,
            showViaModalReturn: false,
            jsonDataReturn: [],
            customerNameReturn: '',
            customerIdReturn: '',
            customerCodeReturn: '',
            vendorNameReturn: '',
            vendorIdreturn: '',
            vehicleNameReturn: '',
            vehicleIdReturn: '',
            sourcecityNameReturn: '',
            sourcecityIdReturn: '',
            sourcecitycodeReturn: '',
            desticityNameReturn: '',
            desticityIdReturn: '',
            desticitycodeReturn: '',
            vehicletypeidReturn: '',
            vehicletypenameReturn: '',
            viacityidReturn: '',
            viacityNameReturn: '',
            viacityCodeReturn: '',
            viaListReturn: [],
            expectedtatReturn: '',
            driverPhoneReturn: '',
            dynamicRowReturn: false,
            routeReturn: '',
            fullRouteReturn: '',
            customerRouteArrayReturn: [],
            tatEditReturn: true,
            via_arrayReturn: [{}],
            viaIndexReturn: -1,
            dateReturn: '',
            timeReturn: '',
            dateTimeReturn: '',
            dateTimeArrayReturn: [],
            showdatePickerReturn: false,
            tatWarningReturn: false,
            vendorStrReturn: '',
            filterData: [],
            dockHours: ''
        };

    }

    static navigationOptions = ({navigation}) => {
        const {params} = navigation.state;
        headingText;

        booking_type = params.bookingType;
        action = params.action;
        bookingId = params.bookingId;


        // console.log('action::::' + params.action)
        // console.log('bookingId::::' + params.bookingId)
        console.log('BOOKING TYPE:::::::::' + params.bookingType)

        if (action === 'accept') {
            headingText = 'Accept Booking'
            URL = UPDATE_BOOKING
        } else if (action === 'edit') {
            headingText = 'Edit Booking'
            URL = UPDATE_BOOKING;
        } else {
            headingText = params.title
            URL = CREATE_BOOKING
            isLoadingData = false
        }


        if (booking_type === 'AH') {
            forwardBookingType = 'AH'
        } else if (booking_type === 'AH2') {
            forwardBookingType = 'AHF', returnBookingType = 'AHR'
        } else if (booking_type === 'DR') {
            forwardBookingType = 'DR'
        }

        if (params.from !== null) {
            from = params.from;
        } else {
            from = null;
        }

        return {
            // title: navigation.getParam('Title', params ? params.title : ''),
            title: navigation.getParam('Title', params ? headingText : ''),
            //Default Title of ActionBar
            headerStyle: {
                backgroundColor: navigation.getParam('BackgroundColor', colors.blueColor),
            },
            headerTintColor: navigation.getParam('HeaderTintColor', '#fff'),
            //Text color of ActionBar
            headerTitleStyle: {
                fontWeight: 'bold',
                fontSize: 13,
                fontFamily: 'CircularStd-Book'
            }

        };
    };

    componentDidMount() {
        console.log('test')
        AsyncStorage.getItem('token').then(
            (value) => {
                this.setState({value: value})
                fetch(CREATE_BOOKING_DETAILS, {
                    headers: {
                        'Authorization': 'Token ' + value
                    }
                })
                    .then((response) => response.json())
                    .then((responseJson) => {

                        // console.log('response Json' + JSON.stringify(responseJson))
                        jsonData = responseJson.data;
                        autopopulateData = jsonData[5]['tat_dict']
                        this.setState({
                            jsonData,
                            autopopulateData,
                            isLoading: false,
                        }, () => 'json data length compount mount' + this.state.jsonData.length)

                        // console.log('action compount did mount:::::' + action)
                        if (action === 'create') {
                            console.log('create')
                        } else {
                            console.log('accept/edit')
                            this.getBookingDetails()
                        }
                    })
                    .catch((error) => {

                    });

            })
    }

    getBookingDetails() {

        AsyncStorage.getItem('token').then(
            (value) => {
                this.setState({value: value})
                axios.post(GET_BOOKING_DETAILS, {
                    "booking_code": bookingId,
                }, {
                    headers: {
                        'Authorization': 'Token ' + value,
                        'Content-Type': 'application/json'
                    }
                }).then((response) => {
                    // console.log("viaCities length::::::" + response.data.data.via_cities.length)

                    booking_type = response.data.data.type;
                    console.log('BOOOOKINGG TYPE ' + booking_type)

                    if (from === 'accepted' || from === 'assigned' || from === 'search') {


                        this.setState({
                            sourcecityName: response.data.data.source.name,
                            sourcecityId: response.data.data.source.id,
                            sourcecitycode: response.data.data.source.code,
                            desticityName: response.data.data.destination.name,
                            desticitycode: response.data.data.destination.code,
                            desticityId: response.data.data.destination.id,
                            vehicletypeid: response.data.data.vehicle_type.id,
                            vehicletypename: response.data.data.vehicle_type.name,
                            expectedtat: response.data.data.expected_tat,
                            driverPhone: response.data.data.phone,
                            dateTime: response.data.data.trip_time,
                            bookingStatus: response.data.data.status,
                            // isLoadingData: false,
                            viaList: response.data.data.via_cities.map(function (item) {
                                return item['id']
                            })
                        }, () => isLoadingData = false)

                        //via_array: response.data.data.via_cities,   copied from above setState
                        console.log('expected Tat     ' + response.data.data.expected_tat)

                        if (response.data.data.customer !== null) {
                            this.setState({
                                customerName: response.data.data.customer.name,
                                customerId: response.data.data.customer.id,
                                customerCode: response.data.data.customer.code
                            })
                        } else {

                        }

                        if (response.data.data.vehicle !== null) {
                            // console.log('vendorName' + response.data.data.vehicle.name)
                            // console.log('vendorId' + response.data.data.vehicle.id)

                            this.setState({
                                vehicleName: response.data.data.vehicle.number,
                                vehicleId: response.data.data.vehicle.id
                            })
                        } else {
                            // console.log('vehicle object null ')
                        }

                        if (response.data.data.vendor !== null) {
                            // console.log('vendorName' + response.data.data.vendor.name)
                            // console.log('vendorId' + response.data.data.vendor.id)

                            this.setState({
                                vendorId: response.data.data.vendor.id,
                                vendorName: response.data.data.vendor.name,
                                vendorStr: response.data.data.vendor.str,
                                vendorApFlag: response.data.data.vendor.ap
                            })
                        } else {
                            // console.log('vendor object null ')
                        }

                        if (response.data.data.via_cities.length > 0) {
                            this.setState({via_array: response.data.data.via_cities}, () => this.addViaId())
                        } else {
                            this.setState({via_array: [{}]})
                        }

                    } else {

                        this.setState({
                            sourcecityName: response.data.data.source.name,
                            sourcecityId: response.data.data.source.id,
                            sourcecitycode: response.data.data.source.code,
                            desticityName: response.data.data.destination.name,
                            desticitycode: response.data.data.destination.code,
                            desticityId: response.data.data.destination.id,
                            vehicletypeid: response.data.data.vehicle_type.id,
                            vehicletypename: response.data.data.vehicle_type.name,
                            expectedtat: response.data.data.expected_tat,
                            driverPhone: response.data.data.phone,
                            dateTime: response.data.data.trip_time,
                            bookingStatus: response.data.data.status,
                            // isLoadingData: false,
                            viaList: response.data.data.via_cities.map(function (item) {
                                return item['id']
                            })
                        }, () => this.setState({isLoading: false}))

                        //via_array: response.data.data.via_cities,   copied from above setState
                        console.log('expected Tat     ' + response.data.data.expected_tat)

                        if (response.data.data.customer !== null) {
                            this.setState({
                                customerName: response.data.data.customer.name,
                                customerId: response.data.data.customer.id,
                                customerCode: response.data.data.customer.code
                            })
                        } else {

                        }

                        if (response.data.data.vehicle !== null) {
                            // console.log('vendorName' + response.data.data.vehicle.name)
                            // console.log('vendorId' + response.data.data.vehicle.id)

                            this.setState({
                                vehicleName: response.data.data.vehicle.number,
                                vehicleId: response.data.data.vehicle.id
                            })
                        } else {
                            // console.log('vehicle object null ')
                        }

                        if (response.data.data.vendor !== null) {
                            // console.log('vendorName' + response.data.data.vendor.name)
                            // console.log('vendorId' + response.data.data.vendor.id)

                            this.setState({
                                vendorId: response.data.data.vendor.id,
                                vendorName: response.data.data.vendor.name,
                                vendorStr: response.data.data.vendor.str,
                                vendorApFlag: response.data.data.vendor.ap
                            })
                        } else {
                            // console.log('vendor object null ')
                        }

                        if (response.data.data.via_cities.length > 0) {
                            this.setState({via_array: response.data.data.via_cities}, () => this.addViaId())
                        } else {
                            this.setState({via_array: [{}]})
                        }


                        //RETURN BOOKING GET DETAILS


                        if (response.data.data.return_booking !== null) {
                            this.setState({
                                sourcecityNameReturn: response.data.data.return_booking.source.name,
                                sourcecityIdReturn: response.data.data.return_booking.source.id,
                                sourcecitycodeReturn: response.data.data.return_booking.source.code,
                                desticityNameReturn: response.data.data.return_booking.destination.name,
                                desticitycodeReturn: response.data.data.return_booking.destination.code,
                                desticityIdReturn: response.data.data.return_booking.destination.id,
                                expectedtatReturn: response.data.data.return_booking.expected_tat,
                                dateTimeReturn: response.data.data.return_booking.trip_time,

                                isLoading: false,
                                viaListReturn: response.data.data.via_cities.map(function (item) {
                                    return item['id']
                                })
                            })

                            if (response.data.data.via_cities.length > 0) {
                                this.setState({via_arrayReturn: response.data.data.via_cities}, () => this.addViaId())
                            } else {
                                this.setState({via_arrayReturn: [{}]})
                            }


                        } else {

                        }

                    }


                }).catch((error) => {
                    console.log("catch getBoking details catch" + error)
                    if (error.response) {
                        if (error.response.status === 401) {
                            this.props.navigation.navigate('Auth')
                        }

                    }
                });

            })

    }


    customerSelectedValue = (changeText, selectedId, selectedCode) => {
        console.log("selected value activity" + changeText);
        this.setState({
            customerName: changeText,
            customerId: selectedId,
            showModal: false,
            customerCode: selectedCode
        }, () => this.checkAutoPopulatedTat())
        // console.log('Selected Customer code main:::' + this.state.customerCode)
        // console.log('Selected Customer code main:::' + selectedCode)

    }

    VendorSelectedValue = (changeText, selectedId, code) => {
        // console.log("selected value vendorName" + changeText);
        // console.log("selected value vendorId" + selectedId);
        this.setState({
            vendorName: changeText,
            vendorId: selectedId,
            showModal: false,
            vendorCode: code
        })
    }

    VehicleSelectedValue = (changeText, selectedId, code, vehicleTypeId) => {
        // console.log("selected value numer" + changeText + "  vehicletype name " + code + " vehicle type id  " + vehicleTypeId);
        this.setState({
            vehicleName: changeText,
            vehicleId: selectedId,
            showModal: false,
            vehicletypename: code,
            vehicletypeid: vehicleTypeId
        }, () => this.checkAutoPopulatedTat())
    }

    VehicleTypeSelectedValue = (changeText, selectedId) => {
        // console.log("selected value vendorName" + changeText);
        this.setState({
            vehicletypename: changeText,
            vehicletypeid: selectedId,
            showModal: false,
            vehicleId: '',
            vehicleName: '',
            vendorId: '',
            vendorName: ''
        }, () => this.checkAutoPopulatedTat())
        // this.checkAutoPopulatedTat()
    }


    SourcecitySelectedValue = (changeText, selectedId, code) => {
        console.log("selected value ssourcecity" + changeText);
        console.log("viacityId" + this.state.viacityid);


        if (selectedId === this.state.desticityId) {
            return (
                Snackbar.show({
                    title: 'Source and Destination cannot be same',
                    duration: Snackbar.LENGTH_LONG,
                })
            );
        } else if (this.state.viaList.includes(selectedId)) {
            // console.log('source via check')
            return (
                Snackbar.show({
                    title: 'Source City and Via cannot be same',
                    duration: Snackbar.LENGTH_LONG,
                })
            );

        } else {
            this.setState({
                sourcecityName: changeText,
                sourcecityId: selectedId,
                showModal: false,
                sourcecitycode: code,
            }, () =>
                this.createRoute());

        }

    }

    DesticitySelectedValue = (changeText, selectedId, code) => {
        // console.log("selected value vendorName" + changeText);
        // console.log("selected value DestinationCity" + selectedId);

        if (selectedId === this.state.sourcecityId) {
            return (
                Snackbar.show({
                    title: 'Source and Destination cannot be same',
                    duration: Snackbar.LENGTH_LONG,
                })

            );
        } else if (this.state.viaList.includes(selectedId)) {
            console.log("vialist:::::     ", this.state.viaList)
            return (
                Snackbar.show({
                    title: 'Destination cannot be in Via',
                    duration: Snackbar.LENGTH_LONG,
                })

            );

        } else {
            this.setState({
                    desticityName: changeText,
                    desticityId: selectedId,
                    showModal: false,
                    desticitycode: code
                }, () =>
                    this.createRoute()
            );
        }

    }


    ViaSelectedValue = (changeText, selectedId, code) => {
        console.log("selected value viaa :::::::" + selectedId);

        this.setState({
            changeViaText: changeText,
            viaSelectedCityId: selectedId,
            viacityCode: code
        })

        console.log('ViaIndex at via Selection ********************************************************************' + this.state.viaIndex);

        this.addToViaList(selectedId, this.state.viaIndex)

    }

    addViaId() {
        const {via_array} = this.state
        let temp_array = [...via_array]

        temp_array[this.state.viaIndex] = {
            name: this.state.changeViaText,
            id: this.state.viaSelectedCityId,
            code: this.state.viacityCode
        };

        this.setState({
            via_array: temp_array,
            showModal: false
        }, () => this.createRoute());
    }


    filterSearch(text) {
        if (text !== '') {
            const jsonData_filter = this.state.originalData.filter((item) => {
                let itemData = ''
                if (this.state.title === 'Vehicle') {
                    itemData = item.number + item.vehicle_type
                } else if (this.state.title === 'Vehicle Type') {
                    itemData = item.name.toUpperCase()
                } else {
                    itemData = item.code + item.name.toUpperCase()
                }
                const textData = text.toUpperCase()
                return itemData.indexOf(textData) > -1
            });

            this.setState({
                text: text,
                filterData: jsonData_filter,
                vendorStr: text// after filter we are setting users to new array
            });
        } else {
            this.setState({
                filterData: this.state.originalData
            })
        }
    }

    renderModal() {
        // console.log('json length' + this.state.jsonData.length)

        if (this.state.jsonData.length > 0) {
            return (
                <CommonModal
                    display={this.state.showModal}
                    closeModal={() => {
                        this.setState({showModal: false});
                    }}
                    SelectedValue={this.state.selectedValue}
                    title={this.state.title}
                    tickVisible={this.state.tickVisible}
                    filterSearch={this.filterSearch.bind(this)}
                    filterData={this.state.filterData}
                />
            )
        } else {
            return null
        }
    }

    showModal(type) {

        if (this.state.jsonData.length > 0) {
            // console.log('json length >0 ')
            const showModal = true;
            let originalData = [];
            let selectedValue = true;
            let tickVisible = false;
            let title = '';

            if (type === 'customer') {
                originalData = this.state.jsonData[0]['customers'];
                selectedValue = this.customerSelectedValue;
                title = 'Customers'
            } else if (type === 'vendor') {
                originalData = this.state.jsonData[12]['vendors'];
                selectedValue = this.VendorSelectedValue;
                title = 'Vendors';
                tickVisible = true;
            } else if (type === 'vehicle') {
                originalData = this.state.jsonData[3]['vehicles'];
                selectedValue = this.VehicleSelectedValue;
                title = 'Vehicle';
            } else if (type === 'vehicle_type') {
                originalData = this.state.jsonData[1]['vehicle_types'];
                selectedValue = this.VehicleTypeSelectedValue;
                title = 'Vehicle Type';
            } else if (type === 'source_city') {
                originalData = this.state.jsonData[2]['cities'];
                selectedValue = this.SourcecitySelectedValue;
                title = 'Source City';
            } else if (type === 'destination_city') {
                originalData = this.state.jsonData[2]['cities'];
                selectedValue = this.DesticitySelectedValue;
                title = 'Destination City';
            } else if (type === 'via') {
                originalData = this.state.jsonData[2]['cities'];
                selectedValue = this.ViaSelectedValue;
                title = 'Via';
            } else if (type === 'source_city_return') {
                originalData = this.state.jsonData[2]['cities'];
                selectedValue = this.SourcecitySelectedValueReturn;
                title = 'Source City';
            } else if (type === 'destination_city_return') {
                originalData = this.state.jsonData[2]['cities'];
                selectedValue = this.DesticitySelectedValueReturn;
                title = 'Destination City';
            } else if (type === 'via_return') {
                originalData = this.state.jsonData[2]['cities'];
                selectedValue = this.ViaSelectedValueReturn;
                title = 'Via';
            }
            this.setState({
                originalData,
                showModal,
                filterData: originalData,
                selectedValue,
                tickVisible,
                title,
            })
        } else {
            // console.log('json length is 0 ')
        }
    }

    checkCustomerName(value, value1) {
        // console.log('customer Name BT:::: ' + booking_type);
        // console.log('customer Name :::: ' + value);
        if (booking_type === 'DR') {
            return null
        } else {
            return (
                <TouchableOpacity onPress={() => {
                    this.showModal("customer")
                }}
                                  activeOpacity={.9}
                                  hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}>
                    <View style={styles.customerBorderStyle}>
                        <Text style={styles.phcustomerStyle}>Customer</Text>
                        <Text
                            style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value + ' (' + value1 + ' )' : 'Customer'}</Text>
                    </View>
                </TouchableOpacity>
            );

        }
    }

    checkVendor() {

        if (this.state.vehicleId !== null && this.state.vehicleId !== '') {
            this.showModal("vendor")
        } else {
            // console.log('vendorCheck Snackbar')
            return (
                Snackbar.show({
                    title: 'Please Select Vehicle for Vendor Selection',
                    duration: Snackbar.LENGTH_LONG,
                })
            );

        }
    }

    checkVendorName(value, value1) {

        if (from !== 'search') {
            console.log("booking status =---------" + this.state.bookingStatus)

            if (booking_type === 'AH2' || booking_type === 'AHF' || booking_type === 'AHR') {
                // console.log("hello AH2 vendor style")
                if (value1 !== null && value1 !== '' && value1 !== undefined) {
                    // console.log('value1 not empty')

                    if (this.state.bookingStatus === 'Accepted' || this.state.bookingStatus === 'Assigned') {
                        return (
                            <TouchableOpacity>
                                <View style={[styles.BorderStyle, styles.vendorAh2Style]}>
                                    <Text style={styles.phcustomerStyle}>Vendor</Text>
                                    <Text
                                        label="Vendor"
                                        style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value + ' (' + value1 + ' )' : 'Vendor'}</Text>

                                </View>
                            </TouchableOpacity>
                        );

                    } else {
                        return (
                            <TouchableOpacity onPress={() => this.checkVendor()}
                                              activeOpacity={.9}
                                              hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}>
                                <View style={[styles.BorderStyle, styles.vendorAh2Style]}>
                                    <Text style={styles.phcustomerStyle}>Vendor</Text>
                                    <Text
                                        label="Vendor"
                                        style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value + ' (' + value1 + ' )' : 'Vendor'}</Text>

                                </View>
                            </TouchableOpacity>
                        );

                    }

                } else {
                    // console.log('value1 empty')
                    return (
                        <TouchableOpacity onPress={() => this.checkVendor()}
                                          activeOpacity={.9}
                                          hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}>
                            <View style={[styles.BorderStyle, styles.vendorAh2Style]}>

                                <Text style={styles.phcustomerStyle}>Vendor</Text>
                                <Text
                                    label="Vendor"
                                    style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value : 'Vendor'}</Text>

                            </View>
                        </TouchableOpacity>
                    );

                }

            } else {
                if (value1 !== null && value1 !== '' && value1 !== undefined) {
                    // console.log('value1 not empty')

                    if (this.state.bookingStatus === 'Accepted' || this.state.bookingStatus === 'Assigned') {
                        return (
                            <TouchableOpacity>
                                <View style={styles.BorderStyle}>

                                    <Text style={styles.phcustomerStyle}>Vendor</Text>
                                    <Text
                                        label="Vendor"
                                        style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value + ' (' + value1 + ' )' : 'Vendor'}</Text>

                                </View>
                            </TouchableOpacity>
                        );

                    } else {
                        return (
                            <TouchableOpacity onPress={() => this.checkVendor()}
                                              activeOpacity={.9}
                                              hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}>
                                <View style={styles.BorderStyle}>

                                    <Text style={styles.phcustomerStyle}>Vendor</Text>
                                    <Text
                                        label="Vendor"
                                        style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value + ' (' + value1 + ' )' : 'Vendor'}</Text>

                                </View>
                            </TouchableOpacity>
                        );

                    }

                } else {
                    if (this.state.bookingStatus === 'Accepted' || this.state.bookingStatus === 'Assigned') {
                        return (
                            <TouchableOpacity>
                                <View style={styles.BorderStyle}>

                                    <Text style={styles.phcustomerStyle}>Vendor</Text>
                                    <Text
                                        label="Vendor"
                                        style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value : 'Vendor'}</Text>

                                </View>
                            </TouchableOpacity>
                        );

                    } else {
                        return (
                            <TouchableOpacity onPress={() => this.checkVendor()}
                                              activeOpacity={.9}
                                              hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}>
                                <View style={styles.BorderStyle}>

                                    <Text style={styles.phcustomerStyle}>Vendor</Text>
                                    <Text
                                        label="Vendor"
                                        style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value : 'Vendor'}</Text>

                                </View>
                            </TouchableOpacity>
                        );

                    }

                }

            }

        } else {

        }


    }

    checkVehicleName(value, value1) {
        // console.log('booking type vehicle check  ' + booking_type)
        console.log("booking status =---------" + this.state.bookingStatus)

        if (booking_type === 'DR') {

            if (this.state.bookingStatus === 'Accepted' || this.state.bookingStatus === 'Assigned') {
                return (
                    <View style={styles.vehicleFullViewstyle}>
                        <View style={styles.BorderStyle}>

                            <Text style={styles.phcustomerStyle}>Vehicle</Text>
                            <Text
                                label="Vehicle"
                                style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value : 'Vehicle'}</Text>

                        </View>

                    </View>

                );

            } else {
                return (
                    <View style={styles.vehicleFullViewstyle}>
                        <TouchableOpacity style={{flex: 1}}
                                          onPress={() => {
                                              this.showModal('vehicle')
                                          }}
                                          hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                                          activeOpacity={.9}>
                            <View style={styles.BorderStyle}>

                                <Text style={styles.phcustomerStyle}>Vehicle</Text>
                                <Text
                                    label="Vehicle"
                                    style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value : 'Vehicle'}</Text>

                            </View>

                        </TouchableOpacity>

                    </View>

                );

            }

        } else {
            if (this.state.bookingStatus === 'Accepted' || this.state.bookingStatus === 'Assigned' || from === 'search') {
                return (
                    <View style={styles.vehicleFullViewstyle}>
                        <TouchableOpacity style={{flex: 0.5}}>
                            <View style={styles.BorderStyle}>

                                <Text style={styles.phcustomerStyle}>Vehicle</Text>
                                <Text
                                    label="Vehicle"
                                    style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value : 'Vehicle'}</Text>

                            </View>

                        </TouchableOpacity>

                        <TouchableOpacity style={{flex: 0.5}}>
                            <View style={styles.BorderStyle}>

                                <Text style={styles.phcustomerStyle}>Vehicle Type</Text>
                                <Text
                                    label="Vehicle Type"
                                    style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value1 !== null && value1 !== '' ? value1 : 'Vehicle Type'}</Text>

                            </View>
                        </TouchableOpacity>


                    </View>

                );

            } else {
                return (
                    <View style={styles.vehicleFullViewstyle}>
                        <TouchableOpacity style={{flex: 0.5}}
                                          onPress={() => {
                                              this.showModal('vehicle')
                                          }}
                                          hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                                          activeOpacity={.9}>
                            <View style={styles.BorderStyle}>

                                <Text style={styles.phcustomerStyle}>Vehicle</Text>
                                <Text
                                    label="Vehicle"
                                    style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value : 'Vehicle'}</Text>

                            </View>

                        </TouchableOpacity>

                        <TouchableOpacity style={{flex: 0.5}}
                                          activeOpacity={.9}
                                          hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                                          onPress={() => {
                                              this.showModal('vehicle_type')
                                          }}>
                            <View style={styles.BorderStyle}>

                                <Text style={styles.phcustomerStyle}>Vehicle Type</Text>
                                <Text
                                    label="Vehicle Type"
                                    style={value !== '' && value !== undefined && value !== null ? styles.selectedTextColor : styles.customerStyleInput}>{value1 !== null && value1 !== '' ? value1 : 'Vehicle Type'}</Text>

                            </View>
                        </TouchableOpacity>


                    </View>

                );

            }


        }

    }

    checkSourceCityName(value, value1) {
        // console.log('Source city bt::::: ' + booking_type)
        // console.log('Source city name::::: ' + this.state.sourcecityName)
        return (
            <TouchableOpacity onPress={() => {
                this.showModal('source_city')
            }}
                              hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                              activeOpacity={.9}>
                <View style={styles.BorderStyle}>

                    <Text style={styles.phcustomerStyle}>Source</Text>
                    <Text
                        label="Source"
                        style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value + ' (' + value1 + ' )' : 'Source City'}</Text>

                </View>
            </TouchableOpacity>
        );
    }

    checkDestiCityName(value, value1) {
        // console.log('Source city bt::::: ' + booking_type)
        // console.log('desti city name::::: ' + this.state.desticityName)

        return (
            <TouchableOpacity activeOpacity={.9}
                              hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                              onPress={() => {
                                  this.showModal('destination_city')
                              }}>
                <View style={styles.BorderStyle}>

                    <Text style={styles.phcustomerStyle}>Destination</Text>
                    <Text
                        label="Destination"
                        style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value + ' (' + value1 + ' )' : 'Destination city'}</Text>

                </View>
            </TouchableOpacity>
        );
    }

    renderNewViaRowCheck() {
        console.log('via array check ' + this.state.via_array[0])
        const {via_array} = this.state;
        let temp_array = [...via_array];
        temp_array.push({});
        this.setState({via_array: temp_array})
    }

    checkViaCity() {
        console.log('checkVia City called *****************************************' + this.state.via_array.length)
        console.log('checkVia City *************************************************' + JSON.stringify(this.state.via_array))
        console.log('checkViaList **************************************************' + JSON.stringify(this.state.viaList))

        if (booking_type === 'DR') {
            return null
        } else {

            console.log('check Via else ')
            return (
                this.state.via_array.map((element, index) => {
                    return (
                        <View style={{
                            flexDirection: 'row',
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }} key={index}>
                            <TouchableOpacity style={{flex: 0.9}}
                                              activeOpacity={.9}
                                              hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                                              onPress={() => {
                                                  this.setState({viaIndex: index}, () => {
                                                      this.showModal('via')
                                                      console.log('via index in checkViaCity *******************************************' + this.state.viaIndex)
                                                  })
                                              }}>
                                <Text
                                    style={element['name'] !== null && element['name'] !== 'Select Via City' && element['name'] !== '' && element['name'] !== undefined ? styles.selectedTextColor : styles.viaCityStyle}>{'name' in element ? element['name'] + ' (' + element.code + ')' : 'Select Via City'}</Text>
                            </TouchableOpacity>

                            <View
                                style={{flexDirection: 'row', justifyContent: 'flex-end', flex: 0.1, marginRight: 10}}>
                                <TouchableOpacity
                                    hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                                    onPress={() => {
                                        !('code' in this.state.via_array[0]) ?
                                            Snackbar.show({
                                                title: 'Please add via',
                                                duration: Snackbar.LENGTH_LONG,
                                            }) : this.renderNewViaRowCheck()
                                    }}>

                                    <Icon
                                        name='add'
                                        color={colors.blueColor}/>

                                </TouchableOpacity>

                                <TouchableOpacity style={{marginLeft: 10}}
                                                  hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                                                  onPress={() => {
                                                      const {via_array} = this.state;
                                                      let array = [...via_array]; // make a separate copy of the array
                                                      // console.log('viaArray length:::::' + via_array.length)
                                                      if (via_array.length !== 1) {
                                                          array.splice(index, 1);
                                                          this.setState({via_array: array}, () => this.createRoute());
                                                          this.removeVia(index)
                                                      } else {
                                                          this.setState({
                                                              via_array: [{}],
                                                              viaList: [],
                                                          }, () => this.createRoute());

                                                      }
                                                  }}>

                                    <Icon
                                        name='remove'
                                        color={colors.redColor}/>

                                </TouchableOpacity>


                            </View>

                        </View>
                    )
                })
            )
        }
    }

    removeVia(index) {
        console.log("remove via : " + index)
        console.log("length", this.state.viaList.length)
        console.log("viaList:::", JSON.stringify(this.state.viaList))

        let array = [...this.state.viaList];
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({viaList: array});
        } else {
        }
    }


    renderDynamicRow() {
        if (this.state.dynamicRow !== false) {
            return (
                <View style={{flexDirection: 'row', marginTop: 10, marginLeft: 10, flex: 1}}>
                    <TouchableOpacity style={{flex: 0.9}}
                                      activeOpacity={.9}
                                      hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                                      onPress={() => this.setState({showViaModal: true})}>
                        <Text style={styles.viaTextStyle}>{this.state.viacityName}{this.state.viacityCode}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{flex: 0.1}}
                                      hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}>
                        <Text style={styles.plusStyle}>-</Text>
                    </TouchableOpacity>
                </View>

            );
        } else {

        }
    }

    checkSourceInViaList() {
        return (
            Snackbar.show({
                title: 'Source City and Via cannot be same',
                duration: Snackbar.LENGTH_LONG,
            })
        );
    }

    checkDestiInViaList() {
        return (
            Snackbar.show({
                title: 'Destination City and Via cannot be same',
                duration: Snackbar.LENGTH_LONG,
            })
        );
    }


    addToViaList(value, position) {
        const {sourcecityId, desticityId, viaList} = this.state;

        console.log('ViaCityId=====================' + value);
        console.log('position in ViaList=====================' + position);
        console.log('sourceCity=====================' + sourcecityId);
        console.log('desticity=====================' + desticityId);
        console.log('viaList=====================' + JSON.stringify(viaList));

        if (value === sourcecityId) {
            console.log('source includes in via')
            {
                this.checkSourceInViaList()
            }
        } else if (value === desticityId) {
            console.log('destination includes in via')
            {
                this.checkDestiInViaList()
            }
        } else if (viaList.includes(value)) {
            console.log('viaList includes in via')
            {
                this.renderViaListCheck()
            }
        } else {
            let temp_array = [...viaList];

            temp_array[position] = value;
            this.setState({viaList: temp_array}, () => this.addViaId())
            console.log('via list > position')

            console.log('length::::::::::::' + this.state.viaList.length)
        }
    }

    renderViaListCheck() {
        // console.log("via Check Visible")
        return (
            Snackbar.show({
                title: 'City already added in Via List',
                duration: Snackbar.LENGTH_LONG,
            })
        );

    }

    createRoute() {
        if (this.state.sourcecityName === '' && this.state.desticityId === '') {
            // console.log('check source')
        } else {
            const {sourcecitycode, desticitycode} = this.state;
            let concatRoute;

            console.log('viaArrayListcreateRoute' + this.state.via_array.length)
            console.log("via_array")
            console.log('code' in this.state.via_array[0])
            console.log("via_array" + JSON.stringify(this.state.via_array))


            if (this.state.via_array.length === 1 && !('code' in this.state.via_array[0])) {
                // console.log('if condition');
                concatRoute = sourcecitycode + '-' + desticitycode;
            } else {
                console.log('ViaArrayList::::::::::::::' + this.state.via_array.length)
                // console.log('ViaArrayList Data' + JSON.stringify(this.state.via_array))

                concatRoute = sourcecitycode + '-'
                // console.log('route:::' + concatRoute);
                for (let i = 0; i < this.state.via_array.length; i++) {
                    // console.log('console1' + this.state.via_array[i].code)
                    if (this.state.via_array[i].code !== '') {
                        if (!concatRoute.endsWith('-')) {
                            // console.log('console2')
                            concatRoute = concatRoute + '-' + this.state.via_array[i].code
                        } else {
                            // console.log('console3')
                            concatRoute = concatRoute + this.state.via_array[i].code
                        }
                    } else {
                        // console.log('console4')
                        concatRoute = concatRoute
                    }

                }
                // console.log('console5')
                concatRoute = concatRoute + '-' + desticitycode
            }

            this.setState({route: concatRoute}, () => this.checkAutoPopulatedTat())

            // console.log('route:::::::::::::::::::::::::::::::::::::::::::' + this.state.sourcecitycode + "-" + this.state.desticitycode)
            // console.log('route:::::::::::::::::::::::::::::::::::::::::::' + this.state.route)
        }
    }

    checkAutoPopulatedTat() {
        const {autopopulateData, customerCode, route, vehicletypeid, expectedtat, expectedtatReturn, routeReturn} = this.state;

        let customerRouteArray = []
        let customerRouteArrayReturn = []

        /// For return bookings

        if (customerCode !== null && customerCode !== '' && route !== null && route !== '') {

            // console.log('1st loop ::' + customerCode + ' ' + route + ' ' + vehicletypeid)
            for (let i = 0; i < autopopulateData.length; i++) {
                if (autopopulateData[i].customer_code === customerCode && autopopulateData[i].route === route) {
                    // console.log('customerRouteArray forward: ' + customerRouteArray)
                    // console.log("first match forward")

                    customerRouteArray.push(autopopulateData[i])

                    // console.log("match1 forward" + autopopulateData[i].customer_code + "-" + autopopulateData[i].route)

                } else {
                    this.setState({tatEdit: true, expectedtat: '', tatWarning: false})
                    // console.log("inner else forward")
                }

            }
        } else {
            // console.log("outer else forward")
        }


        for (let i = 0; i < customerRouteArray.length; i++) {
            // console.log("Customer route", customerRouteArray[i])
            // console.log(customerCode, route, vehicletypeid)
            if (customerCode !== '' && route !== '' && vehicletypeid !== '') {
                if (customerRouteArray[i].customer_code === customerCode && customerRouteArray[i].route === route && customerRouteArray[i].vehicle_type_id === vehicletypeid) {
                    // console.log("match forward" + customerCode + "-" + route + "-" + vehicletypeid)
                    this.setState({
                        expectedtat: customerRouteArray[i].tat,
                        tatEdit: false
                    });

                    // console.log('expected tat forward' + customerRouteArray[i].tat)
                    break;
                } else {
                    // console.log('expected tat forward:::::::::' + customerRouteArray[0].tat)
                    this.setState({tatEdit: true, expectedtat: customerRouteArray[0].tat, tatWarning: true})
                    // console.log("inner else2 forward")
                }
            } else {
                // console.log('match not found forward')
            }
        }


        // console.log('booking type is return: ' + booking_type)

        if (customerCode !== null && customerCode !== '' && routeReturn !== null && routeReturn !== '') {

            // console.log('1st loop ::' + customerCode + ' ' + routeReturn + ' ' + vehicletypeid)
            for (let i = 0; i < autopopulateData.length; i++) {
                if (autopopulateData[i].customer_code === customerCode && autopopulateData[i].route === routeReturn) {
                    // console.log("first match return ")

                    customerRouteArrayReturn.push(autopopulateData[i])

                    // console.log("match1 return " + autopopulateData[i].customer_code + "-" + autopopulateData[i].route)

                } else {
                    this.setState({tatEditReturn: true, expectedtatReturn: '', tatWarningReturn: false})
                    // console.log("inner else return")
                }

            }
        } else {
            // console.log("outer else return")
        }


        for (let i = 0; i < customerRouteArrayReturn.length; i++) {
            // console.log("Customer route", customerRouteArrayReturn[i])
            // console.log(customerCode, routeReturn, vehicletypeid)
            if (customerCode !== '' && routeReturn !== '' && vehicletypeid !== '') {
                if (customerRouteArrayReturn[i].customer_code === customerCode && customerRouteArrayReturn[i].route === routeReturn && customerRouteArrayReturn[i].vehicle_type_id === vehicletypeid) {
                    // console.log("match return :::::::" + customerCode + "-" + route + "-" + vehicletypeid)
                    this.setState({
                        expectedtatReturn: customerRouteArrayReturn[i].tat,
                        tatEditReturn: false
                    });

                    // console.log('expected tat Return' + customerRouteArrayReturn[i].tat)
                    break;
                } else {
                    // console.log('expected tat Return:::::::::' + customerRouteArrayReturn[0].tat)
                    this.setState({
                        tatEditReturn: true,
                        expectedtatReturn: customerRouteArrayReturn[0].tat,
                        tatWarningReturn: true
                    })
                    // console.log("inner else2 return")
                }
            } else {
                // console.log('match not found return ')
            }
        }

    }

    checkExpectedTat() {
        if (this.state.tatEdit === false) {
            return (
                <View style={[styles.BorderStyle, styles.vendorAh2Style]}>

                    <Text style={styles.phcustomerStyle}>Expected TAT</Text>
                    <TextInput
                        editable={false}
                        multiline={false}
                        keyboardType='numeric'
                        text={this.state.expectedtat}
                        style={[this.state.expectedtat ? styles.selectedTextColor : styles.StyleInput]}>{this.state.expectedtat !== '' ? this.state.expectedtat : 'Expected Tat'}</TextInput>


                </View>

            );
        } else {
            return (
                <View style={[styles.BorderStyle, styles.vendorAh2Style]}>
                    <Text style={styles.phcustomerStyle}>Expected TAT</Text>
                    <TextInput
                        editable={true}
                        placeholder="Expected Tat"
                        keyboardType='numeric'
                        multiline={false}
                        text={this.state.expectedtat}
                        placeholderTextColor={colors.border_grey}
                        style={[this.state.expectedtat !== '' ? styles.selectedTextColor : styles.StyleInput]}
                        onChangeText={(text) => this.setState({expectedtat: text})}>{this.state.expectedtat}</TextInput>

                    {this.checkTatWarning()}
                </View>

            );

        }
    }


    checkTatWarning() {
        if (this.state.tatWarning) {
            return (
                <Text style={{
                    color: colors.redColor,
                    fontSize: 12,
                    marginLeft: 10
                }}>{this.state.tatWarning ? 'Please verify TAT for this vehicle type' : null}</Text>
            );
        } else {
            return null
        }
    }

    checkDriverPhone() {
        return (
            <View style={[styles.BorderStyle, styles.vendorAh2Style]}>

                <Text style={styles.phcustomerStyle}>Driver Phone</Text>
                <TextInput
                    style={[this.state.driverPhone !== '' ? styles.selectedTextColor : styles.StyleInput]}
                    placeholder={'Driver Phone'}
                    keyboardType='numeric'
                    maxLength={10}
                    placeholderTextColor={colors.border_grey}
                    onChangeText={(text) => this.setState({driverPhone: text})}>{this.state.driverPhone}</TextInput>
            </View>
        );
    }

    checkDistance() {
        if (booking_type === 'DR') {
            return (
                <View style={styles.BorderStyle}>
                    <Text style={styles.phcustomerStyle}>Distance</Text>
                    <TextInput
                        editable={true}
                        placeholder="Distance"
                        multiline={false}
                        keyboardType='numeric'
                        placeholderTextColor={colors.border_grey}
                        style={[this.state.expectedtat !== '' ? styles.selectedTextColor : styles.StyleInput]}
                        onChangeText={(text) => this.setState({distance: text})}>{this.state.distance}</TextInput>
                </View>

            );
        } else {
            return null
        }

    }


    // Two way functions


    SourcecitySelectedValueReturn = (changeText, selectedId, code) => {
        // console.log("selected value vendorNameReturn" + changeText);

        if (selectedId === this.state.desticityIdReturn) {
            return (
                Snackbar.show({
                    title: 'Source and Destination cannot be same',
                    duration: Snackbar.LENGTH_LONG,
                })
            );
        } else if (this.state.viaListReturn.includes(selectedId)) {
            return (
                Snackbar.show({
                    title: 'Source cannot be in Via',
                    duration: Snackbar.LENGTH_LONG,
                })
            );
        } else {
            this.setState({
                sourcecityNameReturn: changeText,
                sourcecityIdReturn: selectedId,
                showModal: false,
                sourcecitycodeReturn: code,
            }, () => this.createRouteReturn());
        }

    }

    DesticitySelectedValueReturn = (changeText, selectedId, code) => {
        // console.log("selected value vendorName" + changeText);
        // console.log("selected value DestinationCity" + selectedId);
        // console.log("selected value DestinationCity" + code);

        if (selectedId === this.state.sourcecityIdReturn) {
            return (
                Snackbar.show({
                    title: 'Source and Destination cannot be same',
                    duration: Snackbar.LENGTH_LONG,
                })
            );
        } else if (this.state.viaListReturn.includes(selectedId)) {
            return (
                Snackbar.show({
                    title: 'Destination cannot be in Via',
                    duration: Snackbar.LENGTH_LONG,
                })
            );

        } else {
            this.setState({
                desticityNameReturn: changeText,
                desticityIdReturn: selectedId,
                showModal: false,
                desticitycodeReturn: code
            }, () => this.createRouteReturn());
        }

        // this.createRoute();
        // this.checkAutoPopulatedTat()
    }


    ViaSelectedValueReturn = (changeText, selectedId, code) => {
        // console.log("selected value viaa :::::::" + selectedId);

        this.setState({changeViaTextReturn: changeText, viacityidReturn: selectedId, viacityCodeReturn: code})

        this.addToViaListReturn(selectedId)

    }


    addViaIdReturn() {
        const {via_arrayReturn} = this.state
        let temp_array = [...via_arrayReturn]
        temp_array[this.state.viaIndexReturn] = {
            name: this.state.changeViaTextReturn,
            id: this.state.viacityidReturn,
            code: this.state.viacityCodeReturn
        };

        this.setState({
            via_arrayReturn: temp_array,
            showModal: false,
        }, () => this.createRouteReturn());


    }

    checkSourceCityNameReturn(value, value1) {
        return (
            <TouchableOpacity onPress={() => {
                this.showModal('source_city_return')
            }}
                              hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                              activeOpacity={.9}>
                <View style={styles.BorderStyle}>

                    <Text style={styles.phcustomerStyle}>Source</Text>
                    <Text
                        label="Source"
                        style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value + ' (' + value1 + ' )' : 'Source City'}</Text>

                </View>
            </TouchableOpacity>
        );
    }

    checkDestiCityNameReturn(value, value1) {
        return (
            <TouchableOpacity activeOpacity={.9}
                              onPress={() => {
                                  this.showModal('destination_city_return')
                              }}
                              hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}>
                <View style={styles.BorderStyle}>

                    <Text style={styles.phcustomerStyle}>Destination</Text>
                    <Text
                        label="Destination"
                        style={value !== '' ? styles.selectedTextColor : styles.customerStyleInput}>{value !== null && value !== '' ? value + ' (' + value1 + ' )' : 'Destination city'}</Text>

                </View>
            </TouchableOpacity>
        );
    }


    checkViaCityReturn() {
        return (
            this.state.via_arrayReturn.map((element, index) => {
                // console.log('element' + element)
                // console.log('element' + JSON.stringify(element))

                return (
                    <View style={{
                        flexDirection: 'row',
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }} key={index}>
                        <TouchableOpacity style={{flex: 0.9}}
                                          hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                                          activeOpacity={.9}
                                          onPress={() => {
                                              this.setState({viaIndexReturn: index}, () => {
                                                  this.showModal('via_return')
                                              })
                                          }}>
                            <Text
                                style={element['name'] !== null && element['name'] !== 'Select Via City' && element['name'] !== '' && element['name'] !== undefined ? styles.selectedTextColor : styles.viaCityStyle}>{'name' in element ? element['name'] + ' (' + element.code + ')' : 'Select Via City'}</Text>
                        </TouchableOpacity>

                        <View
                            style={{flexDirection: 'row', justifyContent: 'flex-end', flex: 0.1, marginRight: 10}}>
                            <TouchableOpacity
                                hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                                onPress={() => {
                                    !('code' in this.state.via_arrayReturn[0]) ?
                                        Snackbar.show({
                                            title: 'Please add via',
                                            duration: Snackbar.LENGTH_LONG,
                                        }) : this.renderNewViaRowCheckReturn()
                                }}>

                                <Icon
                                    name='add'
                                    color={colors.blueColor}/>

                                {/*<Text style={styles.plusStyle}>+</Text>*/}
                            </TouchableOpacity>

                            <TouchableOpacity style={{marginLeft: 10}}
                                              hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                                              onPress={() => {
                                                  const {via_arrayReturn} = this.state;
                                                  let array = [...via_arrayReturn]; // make a separate copy of the array
                                                  // console.log('viaArray length:::::' + via_array.length)
                                                  if (via_arrayReturn.length !== 1) {
                                                      array.splice(index, 1);
                                                      this.setState({via_arrayReturn: array}, () => this.createRouteReturn());
                                                      // console.log("minus");
                                                      this.removeViaReturn(index)
                                                  } else {
                                                      this.setState({
                                                          via_arrayReturn: [{}],
                                                          viaListReturn: [],
                                                      }, () => this.createRouteReturn());

                                                  }
                                              }}>

                                <Icon
                                    name='remove'
                                    color={colors.redColor}/>

                            </TouchableOpacity>


                        </View>

                    </View>
                )
            })
        )

    }


    removeViaReturn(index) {
        let array = [...this.state.viaListReturn];
        // console.log("index::::::::::::" + index);
        if (index !== -1) {
            array.splice(index, 1);
            this.setState({viaListReturn: array});
            // console.log('vialist length:::' + this.state.viaList.length)
        } else {
            // console.log("remove ViaList else")
        }
    }


    renderDynamicRowReturn() {
        if (this.state.dynamicRow !== false) {
            return (
                <View style={{flexDirection: 'row', marginTop: 10, marginLeft: 10, flex: 1}}>
                    <TouchableOpacity style={{flex: 0.9}}
                                      hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                                      activeOpacity={.9}
                                      onPress={() => this.setState({showViaModalReturn: true})}>
                        <Text style={styles.viaTextStyle}>{this.state.viacityNameReturn}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{flex: 0.1}}>
                        <Text style={styles.plusStyle}>-</Text>
                    </TouchableOpacity>
                </View>

            );
        } else {

        }
    }


    addToViaListReturn(value) {
        const {sourcecityIdReturn, desticityIdReturn, viaListReturn} = this.state;

        // console.log('ViaCityId=====================' + value);
        // console.log('sourceCity=====================' + sourcecityIdReturn);
        // console.log('desticity=====================' + desticityIdReturn);

        if (value === sourcecityIdReturn) {
            console.log('source includes in via')
            {
                this.checkSourceInViaList()
            }
        } else if (value === desticityIdReturn) {
            console.log('destination includes in via')
            {
                this.checkDestiInViaList()
            }
        } else if (viaListReturn.includes(value)) {
            console.log('viaList includes in via')
            {
                this.renderViaListCheck()
            }
        } else {
            let temp_array = [...viaListReturn];

            temp_array[position] = value;
            this.setState({viaListReturn: temp_array}, () => this.addViaIdReturn())
            console.log('via list > position')

            // let temp_array = [...viaListReturn];
            // temp_array.push(value);
            // this.setState({viaListReturn: temp_array}, () => this.addViaIdReturn())


            console.log('length::::::::::::' + this.state.viaListReturn.length)
        }

    }


    createRouteReturn() {
        if (this.state.sourcecityNameReturn === '' && this.state.desticityIdReturn === '') {
            console.log('check source')
        } else {
            const {sourcecitycodeReturn, desticitycodeReturn} = this.state;
            let concatRouteReturn;


            if (this.state.via_array.length === 1 && !('code' in this.state.via_arrayReturn[0])) {
                console.log('if condition');
                concatRouteReturn = sourcecitycodeReturn + '-' + desticitycodeReturn;
            } else {
                console.log('ViaArrayListReturn::::::::::::::' + this.state.via_arrayReturn.length)
                // console.log('ViaArrayList Data' + JSON.stringify(this.state.via_array))

                concatRouteReturn = sourcecitycodeReturn + '-'
                console.log('routeReturn:::' + concatRouteReturn);
                for (let i = 0; i < this.state.via_arrayReturn.length; i++) {
                    console.log('console1' + this.state.via_arrayReturn[i].code)
                    if (this.state.via_arrayReturn[i].code !== '') {
                        if (!concatRouteReturn.endsWith('-')) {
                            console.log('console2')
                            concatRouteReturn = concatRouteReturn + '-' + this.state.via_arrayReturn[i].code
                        } else {
                            console.log('console3')
                            concatRouteReturn = concatRouteReturn + this.state.via_arrayReturn[i].code
                        }
                    } else {
                        console.log('console4')
                        concatRouteReturn = concatRouteReturn
                    }

                }
                console.log('console5')
                concatRouteReturn = concatRouteReturn + '-' + desticitycodeReturn
            }

            this.setState({routeReturn: concatRouteReturn}, () => this.checkAutoPopulatedTat())

            // console.log('route:::::::::::::::::::::::::::::::::::::::::::' + this.state.sourcecitycode + "-" + this.state.desticitycode)
            console.log('route:::::::::::::::::::::::::::::::::::::::::::' + this.state.route)
        }
    }

    checkExpectedTatReturn() {
        if (this.state.tatEditReturn === false) {
            return (
                <View style={[styles.BorderStyle, styles.vendorAh2Style]}>

                    <Text style={styles.phcustomerStyle}>Expected TAT</Text>
                    <TextInput
                        editable={false}
                        multiline={false}
                        keyboardType='numeric'
                        style={[this.state.expectedtatReturn ? styles.selectedTextColor : styles.StyleInput]}>{this.state.expectedtatReturn !== '' ? this.state.expectedtatReturn : 'Expected Tat'}</TextInput>


                </View>

            );
        } else {
            return (
                <View style={[styles.BorderStyle, styles.vendorAh2Style]}>
                    <Text style={styles.phcustomerStyle}>Expected TAT</Text>
                    <TextInput
                        editable={true}
                        placeholder="Expected Tat"
                        keyboardType='numeric'
                        multiline={false}
                        placeholderTextColor={colors.border_grey}
                        style={[this.state.expectedtatReturn !== '' ? styles.selectedTextColor : styles.StyleInput]}
                        onChangeText={(text) => this.setState({expectedtatReturn: text})}>{this.state.expectedtatReturn}</TextInput>

                    {this.checkTatWarningReturn()}
                </View>

            );

        }
    }

    checkTatWarningReturn() {
        if (this.state.tatWarningReturn) {
            return (
                <Text style={{
                    color: colors.redColor,
                    fontSize: 12,
                    marginLeft: 10
                }}>{this.state.tatWarningReturn ? 'Please verify TAT for this vehicle type' : null}</Text>
            );
        } else {
            return null
        }
    }

    checkDockHours() {
        if (action === 'accept') {
            return (
                <View style={[styles.BorderStyle, styles.vendorAh2Style]}>
                    <TouchableOpacity
                        hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                        onPress={() => this.setState({showModalReturn: true})}>

                        <Text style={styles.phcustomerStyle}>Dock Hours</Text>
                        <TextInput
                            style={[this.state.dockHours !== '' ? styles.StyleInput : styles.selectedTextColor]}
                            placeholder={'Dock Hours'}
                            keyboardType='numeric'
                            placeholderTextColor={colors.border_grey}
                            onChangeText={(text) => this.setState({dockHours: text})}>{this.state.dockHours}</TextInput>
                    </TouchableOpacity>
                </View>
            );
        } else {
            return null
        }
    }


    createBooking() {
        const {customerId, vehicleId, vehicletypeid, vendorId, sourcecityId, desticityId, expectedtat, driverPhone, viaList, dateTime, vendorStr, vendorName, sourcecityIdReturn, desticityIdReturn, expectedtatReturn, viaListReturn, vendorApFlag, timeReturn, dateTimeReturn, dockHours} = this.state;


        console.log('customerId ' + customerId)
        console.log('vehicleId ' + vehicleId)
        console.log('vehicletypeid ' + vehicletypeid)
        console.log('vendorId ' + vendorId)
        console.log('sourcecityId ' + sourcecityId)
        console.log('desticityId ' + desticityId)
        console.log('dockHours ' + dockHours)
        console.log('driverPhone ' + driverPhone)

        this.setState({isLoading: true})

        console.log('from create booking' + from);


        if (from !== 'search' && from !== 'accepted' && from !== 'assigned') {
            console.log('check condition 1');
            if (booking_type === 'AH') {
                console.log('one way booking checks')
                if (customerId === '') {
                    console.log("customer null");
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please select Customer',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (vehicletypeid === '') {
                    console.log("vehicle type null");
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please select Vehicle Type',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (sourcecityId === '') {
                    console.log("source city null");
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please select Source City',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (desticityId === '') {
                    console.log("destination null");
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please select Destination City',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (sourcecityId === desticityId) {
                    console.log("expected tat null");
                    this.setState({isLoading: false})
                    return (Snackbar.show({
                        title: 'Source City and Destination City cannot be same',
                        duration: Snackbar.LENGTH_LONG,
                    }));
                } else if (this.state.viaList.includes(sourcecityId)) {
                    console.log("expected tat null");
                    this.setState({isLoading: false})
                    return (Snackbar.show({
                        title: 'Source City cannot be in Via List',
                        duration: Snackbar.LENGTH_LONG,
                    }));
                } else if (this.state.viaList.includes(desticityId)) {
                    console.log("expected tat null");
                    this.setState({isLoading: false})
                    return (Snackbar.show({
                        title: 'Destination City cannot be in Via List',
                        duration: Snackbar.LENGTH_LONG,
                    }));
                } else if (expectedtat === '') {
                    console.log("expected tat null");
                    this.setState({isLoading: false})
                    return (Snackbar.show({
                        title: 'Please fill in the Expected TAT',
                        duration: Snackbar.LENGTH_LONG,
                    }));
                } else if (vehicleId == null && vendorId !== null) {
                    console.log("vehicle null vendor not null");
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please select Vehicle for Vendor selection',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (vehicleId !== '' && vendorName === '') {
                    console.log("vehicle not null vendor null");
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please select Vendor',
                            duration: Snackbar.LENGTH_SHORT,
                        })
                    );
                } else if (vehicleId !== '' && driverPhone === '') {
                    console.log("vdriverPhone null");
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please enter driver phone',
                            duration: Snackbar.LENGTH_SHORT,
                        })
                    );
                } else if (action === 'accept') {
                    console.log('check for accept ')
                    if (dockHours === '') {
                        this.setState({isLoading: false})
                        return (
                            Snackbar.show({
                                title: 'Please enter Docking Hours',
                                duration: Snackbar.LENGTH_SHORT,
                            })
                        );

                    } else if (vendorApFlag === '0') {
                        this.setState({isLoading: false})
                        return (
                            Snackbar.show({
                                title: 'Vendor is not approved',
                                duration: Snackbar.LENGTH_SHORT,
                            })
                        );

                    } else {
                        if (booking_type === 'AH') {
                            if (dateTime === '') {
                                console.log('dateTime check')
                                this.setState({dateTime: null}, () => this.createBookingSubmitData())
                            } else {
                                this.createBookingSubmitData()
                            }
                        }

                    }
                } else {
                    console.log("booking type create booking::::" + booking_type)
                    if (booking_type === 'AH') {
                        if (dateTime === '') {
                            console.log('dateTime check')
                            this.setState({dateTime: null}, () => this.createBookingSubmitData())
                        } else {
                            this.createBookingSubmitData()
                        }
                    }
                }
            }


            if (booking_type === 'AH2' || booking_type === 'AHF' || booking_type === 'AHR') {
                console.log('two way booking checks ')
                if (customerId === '') {
                    this.setState({isLoading: false})
                    console.log("customer null");
                    return (
                        Snackbar.show({
                            title: 'Please select Customer',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (vehicletypeid === '') {
                    this.setState({isLoading: false})
                    console.log("vehicle type null");
                    return (
                        Snackbar.show({
                            title: 'Please select Vehicle Type',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (sourcecityId === '') {
                    this.setState({isLoading: false})
                    console.log("source city null");
                    return (
                        Snackbar.show({
                            title: 'Please select Source City',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (desticityId === '') {
                    this.setState({isLoading: false})
                    console.log("destination null");
                    return (
                        Snackbar.show({
                            title: 'Please select Destination City',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (expectedtat === '') {
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please enter the Expected Tat',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (driverPhone === '') {
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please enter the Driver Phone',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (sourcecityIdReturn === '') {
                    this.setState({isLoading: false})
                    console.log("source city null");
                    return (
                        Snackbar.show({
                            title: 'Please select Source City',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (desticityIdReturn === '') {
                    this.setState({isLoading: false})
                    console.log("destination null");
                    return (
                        Snackbar.show({
                            title: 'Please select Destination City',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (sourcecityIdReturn === desticityIdReturn) {
                    this.setState({isLoading: false})
                    console.log("expected tat null");
                    return (Snackbar.show({
                        title: 'Source City and Destination City cannot be same',
                        duration: Snackbar.LENGTH_LONG,
                    }));
                } else if (this.state.viaListReturn.includes(sourcecityIdReturn)) {
                    this.setState({isLoading: false})
                    console.log("expected tat null");
                    return (Snackbar.show({
                        title: 'Source City cannot be in Via List',
                        duration: Snackbar.LENGTH_LONG,
                    }));
                } else if (this.state.viaListReturn.includes(desticityIdReturn)) {
                    this.setState({isLoading: false})
                    console.log("expected tat null");
                    return (Snackbar.show({
                        title: 'Destination City cannot be in Via List',
                        duration: Snackbar.LENGTH_LONG,
                    }));
                } else if (expectedtatReturn === '') {
                    this.setState({isLoading: false})
                    console.log("expected tat null");
                    return (Snackbar.show({
                        title: 'Please fill in the Expected TAT',
                        duration: Snackbar.LENGTH_LONG,
                    }));
                } else if (dateTime === '') {
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please enter Planned Date and Time',
                            duration: Snackbar.LENGTH_SHORT,
                        })
                    );
                } else if (dateTimeReturn === '') {
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please enter Planned Date and Time',
                            duration: Snackbar.LENGTH_SHORT,
                        })
                    );

                } else if (vehicleId == null && vendorId !== null) {
                    this.setState({isLoading: false})
                    console.log("vehicle null vendor not null");
                    return (
                        Snackbar.show({
                            title: 'Please select Vehicle for Vendor selection',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (vehicleId !== '' && vendorName === '') {
                    this.setState({isLoading: false})
                    console.log("vehicle not null vendor null");
                    return (
                        Snackbar.show({
                            title: 'Please select Vendor',
                            duration: Snackbar.LENGTH_SHORT,
                        })
                    );
                } else if (vehicleId !== '' && driverPhone === '') {
                    this.setState({isLoading: false})
                    console.log("vdriverPhone null");
                    return (
                        Snackbar.show({
                            title: 'Please enter driver phone',
                            duration: Snackbar.LENGTH_SHORT,
                        })
                    );
                } else if (action === 'accept') {

                    console.log('check for accept ')
                    if (dockHours === '') {
                        this.setState({isLoading: false})
                        return (
                            Snackbar.show({
                                title: 'Please enter Docking Hours',
                                duration: Snackbar.LENGTH_SHORT,
                            })
                        );

                    } else if (vendorApFlag === '0') {
                        this.setState({isLoading: false})
                        return (
                            Snackbar.show({
                                title: 'Vendor is not approved',
                                duration: Snackbar.LENGTH_SHORT,
                            })
                        );

                    } else {
                        this.createBookingSubmitData()

                    }
                } else {
                    console.log('ELSE CALLED')
                    this.createBookingSubmitData()
                }
            }


            if (booking_type === 'DR') {
                console.log("Booking type received" + booking_type)
                if (vehicletypeid === '') {
                    console.log(" DR vehicle type null");
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please select Vehicle',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (sourcecityId === '') {
                    console.log("DR source city null");
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please select Source City',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (desticityId === '') {
                    console.log("DR destination null");
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please select Destination City',
                            duration: Snackbar.LENGTH_LONG,
                        })
                    );
                } else if (sourcecityId === desticityId) {
                    console.log(" DR expected tat null");
                    this.setState({isLoading: false})
                    return (Snackbar.show({
                        title: 'Source City and Destination City cannot be same',
                        duration: Snackbar.LENGTH_LONG,
                    }));
                } else if (dateTime === '') {
                    this.setState({isLoading: false})
                    return (Snackbar.show({
                        title: 'Please select Planned Date and Time',
                        duration: Snackbar.LENGTH_LONG,
                    }));
                }
                // else if (vehicleId !== '' && driverPhone === '') {
                //         return (Snackbar.show({
                //             title: 'Please add Driver phone',
                //             duration: Snackbar.LENGTH_LONG,
                //         }));
                //     }
                else if (vehicleId !== '' && vendorName === '') {
                    this.setState({isLoading: false})
                    return (Snackbar.show({
                        title: 'Please Select Vendor',
                        duration: Snackbar.LENGTH_LONG,
                    }));
                } else if (this.state.expectedtat === '') {
                    this.setState({isLoading: false})
                    return (Snackbar.show({
                        title: 'Please Add Expected Tat',
                        duration: Snackbar.LENGTH_LONG,
                    }));

                } else if (action === 'accept') {
                    console.log('check for accept ')
                    if (dockHours === '') {
                        this.setState({isLoading: false})
                        return (
                            Snackbar.show({
                                title: 'Please enter Docking Hours',
                                duration: Snackbar.LENGTH_SHORT,
                            })
                        );

                    } else {
                        this.createBookingSubmitData()
                    }
                } else {
                    {
                        this.createBookingSubmitData()
                    }
                }
            }


        } else {

            console.log('else part create booking details')

            if (customerId === '') {
                console.log("customer null");
                this.setState({isLoading: false})
                return (
                    Snackbar.show({
                        title: 'Please select Customer',
                        duration: Snackbar.LENGTH_LONG,
                    })
                );
            } else if (vehicletypeid === '') {
                console.log("vehicle type null");
                this.setState({isLoading: false})
                return (
                    Snackbar.show({
                        title: 'Please select Vehicle Type',
                        duration: Snackbar.LENGTH_LONG,
                    })
                );
            } else if (sourcecityId === '') {
                console.log("source city null");
                this.setState({isLoading: false})
                return (
                    Snackbar.show({
                        title: 'Please select Source City',
                        duration: Snackbar.LENGTH_LONG,
                    })
                );
            } else if (desticityId === '') {
                console.log("destination null");
                this.setState({isLoading: false})
                return (
                    Snackbar.show({
                        title: 'Please select Destination City',
                        duration: Snackbar.LENGTH_LONG,
                    })
                );
            } else if (sourcecityId === desticityId) {
                console.log("expected tat null");
                this.setState({isLoading: false})
                return (Snackbar.show({
                    title: 'Source City and Destination City cannot be same',
                    duration: Snackbar.LENGTH_LONG,
                }));
            } else if (this.state.viaList.includes(sourcecityId)) {
                console.log("expected tat null");
                this.setState({isLoading: false})
                return (Snackbar.show({
                    title: 'Source City cannot be in Via List',
                    duration: Snackbar.LENGTH_LONG,
                }));
            } else if (this.state.viaList.includes(desticityId)) {
                console.log("expected tat null");
                this.setState({isLoading: false})
                return (Snackbar.show({
                    title: 'Destination City cannot be in Via List',
                    duration: Snackbar.LENGTH_LONG,
                }));
            } else if (expectedtat === '') {
                console.log("expected tat null");
                this.setState({isLoading: false})
                return (Snackbar.show({
                    title: 'Please fill in the Expected TAT',
                    duration: Snackbar.LENGTH_LONG,
                }));
            } else if (vehicleId == null && vendorId !== null) {
                console.log("vehicle null vendor not null");
                this.setState({isLoading: false})
                return (
                    Snackbar.show({
                        title: 'Please select Vehicle for Vendor selection',
                        duration: Snackbar.LENGTH_LONG,
                    })
                );
            } else if (vehicleId !== '' && vendorName === '') {
                console.log("vehicle not null vendor null");
                this.setState({isLoading: false})
                return (
                    Snackbar.show({
                        title: 'Please select Vendor',
                        duration: Snackbar.LENGTH_SHORT,
                    })
                );
            } else if (vehicleId !== '' && driverPhone === '') {
                console.log("vdriverPhone null");
                this.setState({isLoading: false})
                return (
                    Snackbar.show({
                        title: 'Please enter driver phone',
                        duration: Snackbar.LENGTH_SHORT,
                    })
                );
            } else if (action === 'accept') {
                console.log('check for accept ')
                if (dockHours === '') {
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Please enter Docking Hours',
                            duration: Snackbar.LENGTH_SHORT,
                        })
                    );

                } else if (vendorApFlag === '0') {
                    this.setState({isLoading: false})
                    return (
                        Snackbar.show({
                            title: 'Vendor is not approved',
                            duration: Snackbar.LENGTH_SHORT,
                        })
                    );

                } else {
                    if (booking_type === 'AH') {
                        if (dateTime === '') {
                            console.log('dateTime check')
                            this.setState({dateTime: null}, () => this.createBookingSubmitData())
                        } else {
                            this.createBookingSubmitData()
                        }
                    }

                }
            } else {
                console.log("booking type create booking::::" + booking_type)
                if (booking_type === 'AH') {
                    if (dateTime === '') {
                        console.log('dateTime check')
                        this.setState({dateTime: null}, () => this.createBookingSubmitData())
                    } else {
                        console.log('condition check 2')
                        this.createBookingSubmitData()
                    }
                } else {
                    this.createBookingSubmitData();
                }
            }

        }


    }

    createBookingSubmitData() {
        const {customerId, vehicleId, vehicletypeid, vendorId, sourcecityId, desticityId, expectedtat, driverPhone, viaList, dateTime, vendorName, sourcecityIdReturn, desticityIdReturn, expectedtatReturn, driverPhoneReturn, viaListReturn, dateReturn, timeReturn, dateTimeReturn, vendorStrReturn, dockHours, distance} = this.state;

        let bookingData;

        let booking_1 = {
            "customer": customerId,
            "vehicle": vehicleId,
            "vehicle_type": vehicletypeid,
            "vendor_id": vendorId,
            "source_city": sourcecityId,
            "destination_city": desticityId,
            "via_cities": viaList,
            "expected_tat": expectedtat,
            "phone": driverPhone,
            "type": forwardBookingType,
            "frequency": '0',
            "trip_time": dateTime,
            "vendor_str": vendorName,
        };

        let booking_2 = {
            "customer": customerId,
            "vehicle": vehicleId,
            "vehicle_type": vehicletypeid,
            "vendor_id": vendorId,
            "source_city": sourcecityIdReturn,
            "destination_city": desticityIdReturn,
            "via_cities": viaListReturn,
            "expected_tat": expectedtatReturn,
            "phone": driverPhoneReturn,
            "type": returnBookingType,
            "frequency": '0',
            "trip_time": dateTimeReturn,
            "vendor_str": vendorName
        };


        if (from !== 'accepted' && from !== 'assigned' && from !== 'search') {
            let booking_3 = {
                booking_1, booking_2
            };

            if (action === 'accept') {
                booking_1["booking_code"] = bookingId;
                booking_1["action"] = action;
                booking_1["docking_hours"] = dockHours
            }


            if (action === 'edit') {
                booking_1["booking_code"] = bookingId;
                booking_1["action"] = action;
            }


            if (booking_type === 'DR') {
                customerId === null;
                booking_1["distance"] = distance;
            }


            if (booking_type === 'AH' || booking_type === 'DR') {
                // console.log('booking data is booking 1')
                bookingData = {booking_1};
                // console.log(JSON.stringify(bookingData))
            }

            if (booking_type === 'AH2' || booking_type === 'AHF' || booking_type === 'AHR') {
                // console.log('BOOKING DATA IS 3')
                bookingData = booking_3;

                // console.log(JSON.stringify(bookingData))
            }

        } else {
            console.log('check condition 3');
            if (action === 'edit') {
                booking_1["booking_code"] = bookingId;
                booking_1["action"] = action;
            }

            if (booking_type === 'DR') {
                customerId === null;
                booking_1["distance"] = distance;
            }

            bookingData = {booking_1};


        }

        AsyncStorage.getItem('token')
            .then((value) => {
                this.setState({value: value})

                axios.post(URL, bookingData, {
                    headers: {
                        'Authorization': 'Token ' + value,
                        'Content-Type': 'application/json'
                    }
                })
                    .then((response) => {
                        console.log(response);

                        if (response.data.error === false) {
                            // this.props.navigation.navigate('AllBooking')
                            this.setState({
                                submitMessage: '' + response.data.message,
                                isLoading: false
                            }, () => this.showSuccessfullMessage())
                            console.log('error false')
                        } else {
                            this.setState({
                                submitMessage: '' + response.data.message,
                                isLoading: false
                            }, () => this.showErrorMessage())

                            console.log('error true')
                        }
                    })
                    .catch((error) => {
                        if (error.response) {
                            if (error.response.status === 401) {
                                this.props.navigation.navigate('Auth')
                            } else if (error.response.status === 400 && typeof error.response.data == 'object' && 'error_message' in error.response.data) {
                                this.setState({error: error.response.data['error_message'], isLoading: false})
                                console.log("400")
                            } else if (error.response.status === 404) {
                                console.log('404 error' + JSON.stringify(error.response.message))
                            } else {
                                this.setState({error: 'Something Went Wrong', isLoading: false})
                            }
                        }
                    })
            })
            .catch((error) => console.log(error))

    }


    checkReturnBookingReturnHeading() {
        if (booking_type === 'AH2' || booking_type === 'AHF' || booking_type === 'AHR' || booking_type === 'SCHF' || booking_type === 'SCHR') {
            return (
                <View
                    style={{
                        flexDirection: 'row',
                        alignSelf: 'center',
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 5
                    }}>
                    <Icon
                        name='arrow-back'
                        color={colors.blueColor}/>
                    <Text
                        style={styles.bookingTypeStyle}>{booking_type === 'AH2' || booking_type === 'AHF' || booking_type === 'AHR' || booking_type === 'SCHF' || booking_type === 'SCHR' ? 'Return' : null}</Text>
                </View>
            );
        } else {
            return null
        }
    }

    checkReturnBookingForwardHeading() {
        if (booking_type === 'AH2' || booking_type === 'AHF' || booking_type === 'AHR' || booking_type === 'SCHF' || booking_type === 'SCHR') {
            return (
                <View style={{flexDirection: 'row', justifyContent: 'center', marginTop: 5}}>
                    <Icon
                        name='arrow-forward'
                        color={colors.blueColor}/>
                    <Text
                        style={styles.bookingTypeStyle}>{booking_type === 'AH2' || booking_type === 'AHF' || booking_type === 'AHR' ? 'Forward' : null}</Text>

                </View>
            );
        } else {
            return null
        }
    }


    showErrorMessage() {
        Alert.alert(
            'Message',
            this.state.submitMessage,
            [

                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ]
        );
    }

    showSuccessfullMessage() {
        Alert.alert(
            'Message',
            this.state.submitMessage,
            [
                {text: 'OK', onPress: () => this.props.navigation.navigate('AllBooking')},
            ]
        );
    }


    renderReturnBooking() {

        console.log('From return ' + from + 'booking type ' + booking_type);
        if (this.state.isLoading !== true) {
            return (
                <ScrollView style={{marginBottom: 10}}
                            showsVerticalScrollIndicator={false}>
                    <Card>
                        <View style={styles.container}>
                            {this.checkReturnBookingReturnHeading()}

                            <TouchableOpacity onPress={() => this.setState({showdatePickerReturn: true})}>
                                <View style={styles.customerBorderStyle}>

                                    <Text style={styles.phcustomerStyle}>Planned Trip Date and Time</Text>
                                    <Text
                                        label="Planned Date and Time"
                                        style={[this.state.dateTimeReturn !== null && this.state.dateTimeReturn !== '' && this.state.dateTimeReturn !== 'Select Trip Date and Time' ? styles.selectedTextColor : styles.customerStyleInput]}>{this.state.dateTimeReturn !== null && this.state.dateTimeReturn !== '' ? this.state.dateTimeReturn : 'Select Trip Date and Time'}</Text>

                                </View>
                            </TouchableOpacity>

                            <DateTimePicker
                                isVisible={this.state.showdatePickerReturn}
                                mode={'datetime'}
                                onConfirm={(date) => {
                                    console.log("A date has been picked: ", date)

                                    this.state.dateTimeArrayReturn.push(date)

                                    let d = this.state.dateTimeArrayReturn[0];
                                    console.log('0th index::::' + this.state.dateTimeArrayReturn[0]);
                                    console.log('d :::::' + d)
                                    let fromDate = new Date(d);

                                    console.log('getDate' + fromDate.getDate() + '-' + fromDate.getMonth() + '-' + fromDate.getFullYear() + " " + fromDate.getHours() + ":" + fromDate.getMinutes())
                                    this.setState({dateTimeReturn: fromDate.getDate() + '-' + fromDate.getMonth() + '-' + fromDate.getFullYear() + " " + fromDate.getHours() + ":" + fromDate.getMinutes()})
                                }}
                                onCancel={() => this.setState({showdatePickerReturn: false})}/>

                            {
                                this.checkSourceCityNameReturn(this.state.sourcecityNameReturn, this.state.sourcecitycodeReturn)
                            }


                            {
                                this.checkDestiCityNameReturn(this.state.desticityNameReturn, this.state.desticitycodeReturn)
                            }


                            <View style={styles.BorderStyle}>
                                <Text style={styles.phcustomerStyle}>Add Via City</Text>
                                {
                                    this.checkViaCityReturn()
                                }

                            </View>


                            {this.renderDynamicRowReturn()}

                            {this.checkExpectedTatReturn()}


                        </View>
                    </Card>

                </ScrollView>


            );

        } else {
            return (
                <View style={styles.loadingData}>
                    <ActivityIndicator/>
                </View>
            )

        }
    }


    renderViaBox() {
        if (booking_type === 'DR') {
            return null
        } else {
            return (
                <View style={styles.BorderStyle}>
                    <Text style={styles.phcustomerStyle}>Add Via City</Text>

                    {
                        this.checkViaCity(this.state.viacityName)
                        // this.checkViaCity(this.state.viacityName)
                    }

                </View>

            );
        }
    }

    commonFieldTwoWayDesign() {
        if (booking_type === 'AH2' || booking_type === 'AHF' || booking_type === 'AHR' || booking_type === 'SCHF' || booking_type === 'SCHR' && from === 'new') {
            return (
                <Card>
                    {/*{this.renderModal()}*/}

                    {this.checkCustomerName(this.state.customerName, this.state.customerCode)}

                    {this.checkVehicleName(this.state.vehicleName, this.state.vehicletypename)}

                    {this.checkVendorName(this.state.vendorName, this.state.vendorCode)}

                    {/*{this.checkDriverPhoneReturn()}*/}
                    {this.checkDriverPhone()}

                </Card>
            );
        } else {
            return null
        }
    }

    commonFieldOneWayDesign() {
        if (booking_type === 'AH' || booking_type === 'SCH') {
            return (
                <View>
                    {/*{this.renderModal()}*/}

                    {this.checkCustomerName(this.state.customerName, this.state.customerCode)}

                    {this.checkVehicleName(this.state.vehicleName, this.state.vehicletypename)}

                    {this.checkVendorName(this.state.vendorName, this.state.vendorCode)}

                    {this.checkDriverPhone()}

                </View>

            );
        } else if (booking_type === 'DR') {
            return (
                <View>
                    {this.checkVehicleName(this.state.vehicleName, this.state.vehicletypename)}

                    {this.checkVendorName(this.state.vendorName, this.state.vendorCode)}

                </View>
            );
        } else {
            return null
        }
    }

    render() {
        // console.log('booking type render method' + booking_type)

        if (this.state.isLoading === true) {
            return (
                <View style={styles.loadingData}>
                    <ActivityIndicator/>
                </View>
            )
        } else {
            return (
                <View style={{flex: 1, backgroundColor: colors.light_grey}}>

                    <ScrollView style={{marginBottom: 10}}
                                keyboardShouldPersistTaps='always'
                                showsVerticalScrollIndicator={false}>

                        {this.renderModal()}

                        {/*{booking_type === 'AH2' && from === 'new' ? this.commonFieldTwoWayDesign() : null}*/}
                        {this.commonFieldTwoWayDesign()}

                        <Card>
                            <View style={styles.container}>

                                {booking_type === 'AH2' || from === 'new' ? this.checkReturnBookingForwardHeading() : null}

                                {/*{this.checkReturnBookingForwardHeading()}*/}

                                {this.commonFieldOneWayDesign()}

                                {this.checkDockHours()}

                                <TouchableOpacity
                                    hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                                    onPress={() => this.setState({showdatePicker: true})}>
                                    <View style={styles.customerBorderStyle}>

                                        <Text style={[styles.phcustomerStyle]}>Planned Trip Date and Time</Text>
                                        <Text
                                            label="Planned Date and Time"
                                            style={[this.state.dateTime !== null && this.state.dateTime !== '' && this.state.dateTime !== 'Select Trip Date and Time' ? styles.selectedTextColor : styles.customerStyleInput]}>{this.state.dateTime !== null && this.state.dateTime !== '' ? this.state.dateTime : 'Select Trip Date and Time'}</Text>

                                    </View>
                                </TouchableOpacity>

                                <DateTimePicker
                                    isVisible={this.state.showdatePicker}
                                    mode={'datetime'}
                                    onConfirm={(date) => {
                                        console.log("A date has been picked: ", date)

                                        this.state.dateTimeArray.push(date)

                                        let d;

                                        if (this.state.dateTimeArray.length > 1) {
                                            console.log('dateTimeArray > 1')
                                            d = this.state.dateTimeArray[this.state.dateTimeArray.length - 1];
                                        } else {
                                            console.log('dateTimeArray < 1')
                                            d = this.state.dateTimeArray[0];
                                        }

                                        console.log('0th index::::' + this.state.dateTimeArray[0]);
                                        console.log('d :::::' + d)
                                        let fromDate = new Date(d);

                                        console.log('getDate' + fromDate.getDate() + '-' + fromDate.getMonth() + '-' + fromDate.getFullYear() + " " + fromDate.getHours() + ":" + fromDate.getMinutes())
                                        this.setState({
                                            dateTime: fromDate.getDate() + '-' + fromDate.getMonth() + '-' + fromDate.getFullYear() + " " + fromDate.getHours() + ":" + fromDate.getMinutes(),
                                            showdatePicker: false
                                        })
                                    }}
                                    onCancel={() => this.setState({showdatePicker: false})}/>


                                {this.checkSourceCityName(this.state.sourcecityName, this.state.sourcecitycode)}

                                {this.checkDestiCityName(this.state.desticityName, this.state.desticitycode)}

                                {this.renderViaBox()}

                                {this.renderDynamicRow()}

                                {this.checkDistance()}

                                {this.checkExpectedTat()}


                            </View>

                        </Card>

                        {booking_type === 'AHF' || booking_type === 'AHR' || booking_type === 'SCHF' || booking_type === 'SCHR' && from === 'new' || from === 'AH2create' ? this.renderReturnBooking() : null}
                        {/*{from === 'new' || from === 'AH2create' && booking_type === 'AHF' || booking_type === 'AHR' || booking_type === 'SCHF' || booking_type === 'SCHR' ? this.renderReturnBooking() : null}*/}

                        {this.state.isLoading === true ?
                            <View style={styles.loadingData}><ActivityIndicator size='large'/></View> : null}


                        <View style={{justifyContent: 'center', padding: 5, marginTop: 5}}>
                            <TouchableOpacity
                                hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                                style={{justifyContent: 'flex-end'}}
                                onPress={() =>
                                    this.setState({isLoading: true}, () => this.createBooking())
                                }>
                                <Text style={styles.addBookingStyle}>{headingText}</Text>
                            </TouchableOpacity>

                        </View>

                    </ScrollView>


                </View>
            );

        }


    }
}


const styles = {
    container: {
        flex: 1,
    },
    customerStyleInput: {
        fontSize: 12,
        color: colors.border_grey,
        marginLeft: 5,
        justifyContent: 'center',
        alignItems: 'center',
        textAlignVertical: 'center',
        height: 35,
        paddingLeft: 5,
        fontFamily: 'CircularStd-Book',
    },
    customerBorderStyle: {
        borderWidth: 1,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        borderColor: colors.blueColor,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 20,
        flex: 1,
        paddingLeft: 5,
        paddingRight: 5,
        alignItems: 'flex-start'
    },
    BorderStyle: {
        borderWidth: 1,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        borderColor: colors.blueColor,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 20,
        flex: 1,
        paddingLeft: 5,
        paddingRight: 5,
    },
    phcustomerStyle: {
        marginTop: -10,
        color: colors.dark_grey,
        backgroundColor: colors.white,
        marginLeft: 5,
        paddingLeft: 5,
        paddingRight: 5,
        fontFamily: 'CircularStd-Book',
        alignSelf: 'flex-start'
    },
    vehicleFullViewstyle: {
        flex: 1,
        flexDirection: 'row',
    },
    addBookingStyle: {
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        borderColor: colors.blueColor,
        color: colors.white,
        backgroundColor: colors.blueColor,
        padding: 10,
        textAlign: 'center',
        fontFamily: 'CircularStd-Book',
        fontSize: 14,
    },
    plusStyle: {
        backgroundColor: colors.blueColor,
        width: 20,
        height: 20,
        padding: 10,
        justifyContent: 'center',
        fontFamily: 'CircularStd-Book',
    },
    viaTextStyle: {
        width: '70%'
    },
    bookingTypeStyle: {
        fontSize: 13,
        fontWeight: 'bold',
        color: colors.blueColor,
        padding: 5
    },
    StyleInput: {
        fontSize: 12,
        color: colors.border_grey,
        paddingLeft: 5,
        marginLeft: 5,
        fontFamily: 'CircularStd-Book',
        textAlignVertical: 'center',
        padding: 0,
        marginTop: 5
    },
    vendorAh2Style: {
        marginBottom: 10,
    },
    selectedTextColor: {
        fontSize: 12,
        color: colors.dark_grey,
        marginLeft: 5,
        padding: 5,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        textAlignVertical: 'center',
        fontFamily: 'CircularStd-Book',
    },
    viaCityStyle: {
        paddingRight: 10,
        paddingLeft: 10,
        marginBottom: 5,
        fontSize: 12,
        height: 35,
        color: colors.border_grey,
        fontFamily: 'CircularStd-Book',
        textAlignVertical: 'center',
    },
    loadingData: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    }
}
