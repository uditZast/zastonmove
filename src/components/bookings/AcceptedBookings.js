import React, { Component } from "react";
import {
  Platform,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import colors from "../../colors/Colors";
import Card from "../Card";
import CardOptions from "./bookingactions/CardOptions";
import { ACCEPTED_BOOKING } from "../../constants/Constants";
// import RejectionReasonsInput from "../../modal/RejectionReasonsInput";
// import AssignModal from "../../modal/AssignModal";
import { BarIndicator } from "react-native-indicators";
import Snackbar from "react-native-snackbar";
import NetInfo from "@react-native-community/netinfo";
import { Container } from "../common/Container";
import axios from "axios";
import AssignModal from "../../containers/AssignModal";
import RejectModal from "../../containers/RejectionReasonsInput";

let selectedIndex = 0;

class AcceptedBookings extends Component {
  state = { data: [], value: "" };

  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      OriginalData: [],
      actionsArray: [],
      isLoading: true,
      showActions: false,
      showModal: false,
      historyArray: [],
      bookingId: "",
      tripId: "",
      action: "",
      drop_down_data: [],
      showRejectModal: false,
      comment: "",
      reason: "",
      token: "",
      showAssignModal: false,
      refreshing: false,
      rightsArray: [],
      text: "",
      role: ""
    };
  }

  componentDidMount() {
    AsyncStorage.getItem("rights").then(value =>
      this.setState({ rightsArray: value })
    );
    AsyncStorage.getItem("role").then(value => this.setState({ role: value }));

    // console.log('test' + JSON.stringify(this.props.products.products));

    this.setState({
      // drop_down_data: this.props.products.products,
      isLoading: false
    });

    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        // this.getCreateBookingDetailsData();
        this.getNewBookingList();

        this._onFocusListener = this.props.navigation.addListener(
          "didFocus",
          payload => {
            selectedIndex = 0;
            this.setState({ text: "" });
            // this.getCreateBookingDetailsData();
            this.getNewBookingList();
            console.log("component did mount ");
          }
        );
      } else {
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });
  }

  componentWillUnmount() {
    this._onFocusListener.remove();
  }

  getNewBookingList() {
    console.log("NEW REQUEST LIST ");

    AsyncStorage.getItem("token").then(value => {
      console.log("token:::::::" + value);
      axios
        .get(ACCEPTED_BOOKING, {
          headers: {
            Authorization: "Token " + value,
            "Content-Type": "application/json"
          }
        })
        .then(response => {
          console.log("then new accepted");
          this.setState({
            dataSource: response.data.data,
            OriginalData: response.data.data,
            actionsArray: response.data.data,
            refreshing: false,
            isLoading: false
          });
          return response.data;
        })
        .catch(error => {
          if (error.response.status === 401) {
            AsyncStorage.removeItem("token");
            this.props.navigation.navigate("Auth");
          } else {
          }
        });
    });

    // AsyncStorage.getItem('token').then(
    //     (value) => {
    //         this.setState({value: value});
    //         fetch(ACCEPTED_BOOKING, {
    //             headers: {
    //                 'Authorization': 'Token ' + value
    //             }
    //         })
    //             .then((response) => response.json())
    //             .then((responseJson) => {
    //                 this.setState({
    //                     dataSource: responseJson.data,
    //                     OriginalData: responseJson.data,
    //                     actionsArray: responseJson.data,
    //                     refreshing: false,
    //                     isLoading: false,
    //                 })
    //
    //             })
    //             .catch((error) => {
    //                 console.log("testing");
    //                 console.error(error);
    //             });
    //
    //     })
  }

  checkVehicle(value, value1) {
    if (value !== null) {
      return (
        <View style={styles.rowStyle}>
          <Text style={styles.normalTextStyle}>{value}</Text>
          <Text style={styles.normalTextStyle}>{value1}</Text>
        </View>
      );
    } else {
      return (
        <View style={styles.rowStyle2}>
          <Text style={styles.textStyle2}>{value1}</Text>
        </View>
      );
    }
  }

  checkVehicle1(value, value1) {
    if (value !== null) {
      return (
        <View style={styles.rowStyle}>
          <Text style={styles.textStyle2}>{value}</Text>
          <Text style={styles.normalTextStyle}>{value1}</Text>
        </View>
      );
    } else {
      return (
        <View style={{ flex: 0.5, flexDirection: "row", marginLeft: 0 }}>
          <Text style={styles.textStyle2}>{value1}</Text>
        </View>
      );
    }
  }

  checkCustomer(value) {
    if (value !== null) {
      return (
        <View style={styles.rowStyle1}>
          <Text style={styles.textStyle1}>Customer</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {value}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkRoute(value) {
    if (value !== null) {
      return (
        <View style={styles.rowStyle1}>
          <Text style={styles.textStyle1}>Route</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {value}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkScheduledTime(value) {
    if (value !== null && value !== "") {
      return (
        <View style={styles.rowStyle1}>
          <Text style={styles.textStyle1}>Schedule Time</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {value}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkVendor(value) {
    if (value !== null) {
      return (
        <View style={styles.rowStyle1}>
          <Text style={styles.textStyle1}>Vendor</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {value}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }

  //action Modals

  renderRejectModal() {
    return (
      <RejectModal
        display={this.state.showRejectModal}
        closeModal={() => {
          this.setState({ showRejectModal: false });
        }}
        token={this.state.value}
        bookingId={this.state.bookingId}
        refreshing={() => this._onRefresh()}
      />
    );
  }

  renderAssignModal() {
    return (
      <AssignModal
        display={this.state.showAssignModal}
        closeModal={() => {
          this.setState({ showAssignModal: false });
        }}
        booking_code={this.state.bookingId}
        token={this.state.value}
        refreshing={() => this._onRefresh()}
      />
    );
  }

  //actions menu

  renderActions(index) {
    let actionsArray = this.state.actionsArray;

    actionsArray.forEach((option, thisindex) => {
      actionsArray[thisindex]["actions"] = thisindex === index;
    });

    selectedIndex = index;

    this.setState({ actionsArray });
  }

  renderListActions(element, index) {
    return (
      <CardOptions
        visible={
          element.hasOwnProperty("actions") ? element.actions : index === 0
        }
        onAssignTap={() =>
          this.setState({ showAssignModal: true, bookingId: element.code })
        }
        onEditTap={() =>
          this.props.navigation.navigate("AdHocOne", {
            bookingId: element.code,
            action: "edit",
            from: "accepted"
          })
        }
        onRejectTap={() =>
          this.setState({ showRejectModal: true, bookingId: element.code })
        }
        screen="accepted"
        change="1"
        rightsArray={this.state.rightsArray}
        role={this.state.role}
      />
    );
  }

  filterSearch(text) {
    console.log("filter " + text);
    const dataSource_filter = this.state.OriginalData.filter(item => {
      const itemData =
        item.code.toUpperCase() +
        item.type.toUpperCase() +
        item.route.toUpperCase() +
        item.vehicle +
        item.vehicle_type +
        item.customer;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      text: text,
      dataSource: dataSource_filter // after filter we are setting users to new array
    });
  }

  _onRefresh = () => {
    selectedIndex = 0;
    this.setState({ refreshing: true });
    this.getNewBookingList();
  };

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <BarIndicator color={colors.blueColor} size={20} />
        </View>
      );
    }

    return (
      <Container>
        <View style={{ flex: 1 }}>
          <TextInput
            style={styles.customerBorderStyle}
            onChangeText={text => this.filterSearch(text)}
            value={this.state.text}
            placeholderTextColor={"#9D9B9D"}
            placeholder="Search BookingId/ Vehicle/ Vendor/ Route"
          />

          {this.renderRejectModal()}
          {this.renderAssignModal()}

          {this.state.dataSource.length > 0 ? (
            <ScrollView
              style={styles.container}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            >
              {this.state.dataSource.map((rowData, index) => {
                return (
                  <Card key={rowData.code}>
                    <TouchableOpacity
                      onPress={this.renderActions.bind(this, index)}
                    >
                      <View
                        style={
                          selectedIndex === index
                            ? styles.row1StyleColorChange
                            : styles.row1Style
                        }
                      >
                        <View style={{ flex: 1 }}>
                          <View style={styles.forwardStyle}>
                            <Text style={styles.boldTextStyle}>
                              {rowData.code}
                            </Text>
                            <Text style={styles.normalTextStyle}>
                              {rowData.type}
                            </Text>
                          </View>
                          {this.checkVehicle(
                            rowData.vehicle,
                            rowData.vehicle_type
                          )}
                        </View>
                      </View>

                      <View
                        style={
                          selectedIndex === index
                            ? styles.colorChangelineStyle
                            : styles.lineStyle
                        }
                      />

                      <View
                        style={
                          selectedIndex === index
                            ? styles.row2StyleColorChange
                            : styles.row2Style
                        }
                      >
                        {this.checkCustomer(rowData.customer)}
                        {this.checkRoute(rowData.route)}

                        {this.checkScheduledTime(rowData.trip_time)}

                        {rowData.vendor.code !== null &&
                        rowData.vendor.code !== ""
                          ? this.checkVendor(rowData.vendor.code)
                          : null}

                        <View style={styles.rowStyle1}>
                          <Text style={styles.textStyle1}>Expected TAT</Text>

                          <Text
                            style={[styles.textStyle1, { color: colors.grey }]}
                          >
                            {rowData.expected_tat}
                          </Text>
                        </View>
                      </View>
                    </TouchableOpacity>

                    {this.renderListActions(rowData, index)}
                  </Card>
                );
              })}
            </ScrollView>
          ) : null}
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.listbackgroundcolor
  },
  forwardStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    marginTop: 3
  },
  returnStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginRight: 10,
    marginLeft: 0
  },
  boldTextStyle: {
    fontSize: 15,
    fontWeight: "bold",
    color: colors.blueColor,
    fontFamily: "CircularStd-Book"
  },
  normalTextStyle: {
    fontSize: 12,
    color: colors.textcolor,
    marginLeft: 5,
    marginTop: 3,
    fontFamily: "CircularStd-Book"
  },
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    marginTop: 5,
    marginBottom: 5
  },
  colorChangelineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    paddingTop: 5,
    backgroundColor: colors.blue,
    paddingRight: 10,
    paddingLeft: 10
  },
  rowStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 5
  },
  rowStyle1: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 3
  },
  textStyle1: {
    flex: 0.5,
    fontSize: 12,
    color: colors.textcolor,
    fontFamily: "CircularStd-Book"
  },
  textStyle2: {
    fontSize: 12,
    color: colors.textcolor,
    marginTop: 3,
    fontFamily: "CircularStd-Book"
  },
  rowStyle2: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 0
  },
  customerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    ...Platform.select({
      ios: {
        padding: 10,
        borderRadius: 10,
        marginTop: 10,
        overflow: "hidden"
      },
      android: {
        paddingTop: 5,
        paddingBottom: 5,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        marginTop: 8,
        paddingLeft: 12,
        paddingRight: 12
      }
    })
  },
  row1Style: {
    flex: 1,
    flexDirection: "row"
  },
  row2Style: {
    flex: 1,
    marginBottom: 10
  },
  row1StyleColorChange: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: colors.blue
  },
  row2StyleColorChange: {
    flex: 1,
    paddingBottom: 10,
    backgroundColor: colors.blue
  }
});

//
// const mapStateToProps = state => ({
//     state: state,
//     error: state.error,
//     products: state.productsReducer.products,
//     pending: state.productsReducer.pending,
// });
//
//
// export default connect(
//     mapStateToProps,
//     null
// )(AcceptedBookings);
//

export default AcceptedBookings;
