import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../../../../colors/Colors";
import { hasRights } from "../../../common/checkRights";
import { USER_RIGHTS } from "../../../../../constants";

class CardOptionsNew extends Component {
  renderOptions() {
    const { type } = this.props;
    const {
      screen,
      onAcceptTap,
      onEditTap,
      onRejectTap,
      onHistoryTap,
      change,
      onAssignTap,
      onDockTap,
      onReAssignTap,
      onCommentTap,
      loggedInUser
    } = this.props;

    const { role } = loggedInUser;

    if (screen === "new_request") {
      return (
        <View
          style={change === "1" ? styles.selectedcardStyle : styles.cardStyle}
        >
          {hasRights(USER_RIGHTS.ACCEPT_BOOKING, loggedInUser) ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onAcceptTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Accept</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          {hasRights(USER_RIGHTS.EDIT_BOOKING, loggedInUser) ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onEditTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Edit</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          {hasRights(USER_RIGHTS.REJECT_BOOKING, loggedInUser) ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onRejectTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Reject</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          <View style={styles.optionContainer}>
            <TouchableOpacity onPress={onHistoryTap}>
              <Text style={styles.optionStyle}>History</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    } else if (screen === "accepted") {
      return (
        <View
          style={change === "1" ? styles.selectedcardStyle : styles.cardStyle}
        >
          {hasRights(USER_RIGHTS.ASSIGN_BOOKING, loggedInUser) ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onAssignTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Assign</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          {hasRights(USER_RIGHTS.REJECT_BOOKING, loggedInUser) ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onRejectTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Reject</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          {/*<View style={[styles.optionContainer, styles.optionContainerRight]}>*/}
          {/*  <TouchableOpacity onPress={onRejectTap} activeOpacity={0.9}>*/}
          {/*    <Text style={styles.optionStyle}>Reject</Text>*/}
          {/*  </TouchableOpacity>*/}
          {/*</View>*/}

          {hasRights(USER_RIGHTS.EDIT_BOOKING, loggedInUser) &&
          role !== "GRN-OPS" ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onEditTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Edit</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          {/*<View style={[styles.optionContainer, styles.optionContainerRight]}>*/}
          {/*  <TouchableOpacity onPress={onEditTap} activeOpacity={0.9}>*/}
          {/*    <Text style={styles.optionStyle}>Edit</Text>*/}
          {/*  </TouchableOpacity>*/}
          {/*</View>*/}

          <View style={styles.optionContainer}>
            <TouchableOpacity onPress={onHistoryTap} activeOpacity={0.9}>
              <Text style={styles.optionStyle}>History</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    } else if (screen === "assigned") {
      return (
        <View
          style={change === "1" ? styles.selectedcardStyle : styles.cardStyle}
        >
          {hasRights(USER_RIGHTS.DOCK_BOOKING, loggedInUser) ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onDockTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Dock</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          {hasRights(USER_RIGHTS.ASSIGN_BOOKING, loggedInUser) ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onReAssignTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Re-Assign</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          {hasRights(USER_RIGHTS.REJECT_BOOKING, loggedInUser) ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onRejectTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Reject</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          {/*<View style={[styles.optionContainer, styles.optionContainerRight]}>*/}
          {/*  <TouchableOpacity onPress={onRejectTap} activeOpacity={0.9}>*/}
          {/*    <Text style={styles.optionStyle}>Reject</Text>*/}
          {/*  </TouchableOpacity>*/}
          {/*</View>*/}

          {hasRights(USER_RIGHTS.EDIT_BOOKING, loggedInUser) &&
          role !== "GRN-OPS" ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onEditTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Edit</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          {/*<View style={[styles.optionContainer, styles.optionContainerRight]}>*/}
          {/*  <TouchableOpacity onPress={onEditTap} activeOpacity={0.9}>*/}
          {/*    <Text style={styles.optionStyle}>Edit</Text>*/}
          {/*  </TouchableOpacity>*/}
          {/*</View>*/}

          <View style={styles.optionContainer}>
            <TouchableOpacity onPress={onHistoryTap}>
              <Text style={styles.optionStyle}>History</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    } else if (screen === "docked") {
      return (
        <View
          style={change === "1" ? styles.selectedcardStyle : styles.cardStyle}
        >
          <View style={styles.optionContainer}>
            <TouchableOpacity onPress={onHistoryTap} activeOpacity={0.9}>
              <Text style={styles.optionStyle}>History</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    } else if (screen === "my_bookings") {
      return (
        <View
          style={change === "1" ? styles.selectedcardStyle : styles.cardStyle}
        >
          <View style={styles.optionContainer}>
            <TouchableOpacity onPress={onHistoryTap} activeOpacity={0.9}>
              <Text style={styles.optionStyle}>History</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    } else if (screen === "search") {
      return (
        <View
          style={change === "1" ? styles.selectedcardStyle : styles.cardStyle}
        >
          {/*<View style={[styles.optionContainer, styles.optionContainerRight]}>*/}
          {/*  <TouchableOpacity onPress={onCommentTap} activeOpacity={0.9}>*/}
          {/*    <Text style={styles.optionStyle}>Comment</Text>*/}
          {/*  </TouchableOpacity>*/}
          {/*</View>*/}

          <View style={styles.optionContainer}>
            <TouchableOpacity onPress={onHistoryTap} activeOpacity={0.9}>
              <Text style={styles.optionStyle}>History</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    } else if (screen === "trip_search") {
      return (
        <View
          style={change === "1" ? styles.selectedcardStyle : styles.cardStyle}
        >
          <View style={[styles.optionContainer, styles.optionContainerRight]}>
            <TouchableOpacity onPress={onCommentTap} activeOpacity={0.9}>
              <Text style={styles.optionStyle}>Comment</Text>
            </TouchableOpacity>
          </View>
          <View style={[styles.optionContainer, { borderLeftWidth: 1 }]}>
            <TouchableOpacity onPress={onHistoryTap} activeOpacity={0.9}>
              <Text style={styles.optionStyle}>History</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    } else {
    }
  }

  render() {
    return <View>{this.renderOptions()}</View>;
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderWidth: 1,
    borderColor: "#2196f3",
    flexDirection: "row",
    flex: 1,
    marginTop: 3,
    justifyContent: "space-between"
  },
  selectedcardStyle: {
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderWidth: 1,
    borderColor: "#2196f3",
    flexDirection: "row",
    flex: 1,
    marginTop: 3,
    justifyContent: "space-between",
    backgroundColor: colors.blue
  },
  optionStyle: {
    alignSelf: "center",
    fontSize: 13,
    color: "#1e88e5",
    paddingTop: 8,
    paddingBottom: 8,
    fontFamily: "CircularStd-Book"
  },
  optionContainer: {
    flex: 1,
    borderColor: "#2196f3"
  },
  optionContainerRight: {
    borderRightWidth: 1
  }
});

export default CardOptionsNew;
