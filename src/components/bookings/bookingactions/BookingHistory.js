import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import axios from "axios";
import { BOOKING_TRIP_History } from "../../../constants/Constants";
import colors from "../../../colors/Colors";
import { ScrollView } from "react-navigation";
import Card from "../../Card";
import { BarIndicator } from "react-native-indicators";
import NetInfo from "@react-native-community/netinfo";
import Snackbar from "react-native-snackbar";

let bookingId, tripId, date, month, year, hrs, mins;

export default class BookingHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      dataSource: [],
      isLoading: true,
      date: "",
      time: "",
      dateTime: []
    };
  }

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;

    bookingId = params.bookingId;
    tripId = params.tripId;

    console.log("bookingId::::" + params.bookingId);
    console.log("tripId::::" + params.tripId);

    return {
      // title: navigation.getParam('Title', params ? params.title : ''),
      title: navigation.getParam(
        "Title",
        bookingId === null ? tripId : bookingId
      ),
      //Default Title of ActionBar
      headerStyle: {
        backgroundColor: navigation.getParam(
          "BackgroundColor",
          colors.blueColor
        )
      },
      headerTintColor: navigation.getParam("HeaderTintColor", "#fff"),
      //Text color of ActionBar
      headerTitleStyle: {
        fontWeight: "bold",
        fontSize: 13,
        fontFamily: "CircularStd-Book"
      }
    };
  };

  componentDidMount() {
    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        this.getHistoryData();
      } else {
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });

    // this.getHistoryData()
  }

  getHistoryData() {
    const { params } = this.props.navigation.state;
    const { bookingId } = params;
    console.log("boookingId received::::::::::" + bookingId);
    console.log("tripId received::::::::::" + tripId);

    AsyncStorage.getItem("token").then(value => {
      this.setState({ value: value });
      console.log("token:::::::" + value);
      axios
        .post(
          BOOKING_TRIP_History,
          {
            booking_code: bookingId,
            trip_code: tripId,
            entity: "history"
          },
          {
            headers: {
              Authorization: "Token " + value,
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          this.setState({
            dataSource: response.data.data,
            isLoading: false
          });
          // console.log('rowData::::: ' + response.data.data.length)
        })
        .catch(error => {
          console.log("catch" + error);
          if (error.response) {
            if (error.response.status === 401) {
              AsyncStorage.removeItem("token");
              this.props.navigation.navigate("Auth");
            }
          }
        });
    });
  }

  render() {
    if (this.state.isLoading) {
      return <BarIndicator color={colors.blueColor} size={20} />;
    } else {
      return (
        <View style={{ flex: 1 }}>
          <ScrollView
            style={{ flex: 1, backgroundColor: "#ECF0F1" }}
            showsVerticalScrollIndicator={false}
          >
            {this.state.dataSource.map(element => {
              return (
                <View
                  key={element.id}
                  style={{ flexDirection: "row", flex: 1 }}
                >
                  {this.checkTime(element.time)}

                  <View style={{ flex: 0.8 }}>
                    <Card>
                      {/*<View style={styles.VerticallineStyle}></View>*/}
                      {element.status !== null && element.status !== "" ? (
                        <Text style={styles.textStyle}>{element.status}</Text>
                      ) : null}

                      {element.action !== null && element.action !== "" ? (
                        <Text style={styles.textStyle}>{element.action}</Text>
                      ) : null}

                      <View
                        style={{
                          alignSelf: "flex-end",
                          marginRight: 5,
                          padding: 3
                        }}
                      >
                        <Text
                          style={[
                            styles.textStyle,
                            styles.BorderStyle,
                            {
                              marginBottom: 5,
                              textAlign: "center"
                            }
                          ]}
                        >
                          {element.user}
                        </Text>
                      </View>
                    </Card>
                  </View>
                </View>
              );
            })}
          </ScrollView>
        </View>
      );
    }
  }
}
const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    marginLeft: 5,
    marginTop: 5,
    marginRight: 5,
    borderTopLeftRadius: 3,
    backgroundColor: colors.white,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    flex: 1
  },
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5
  },
  placeholderStyle: {
    color: colors.textcolor,
    fontFamily: "CircularStd-Book",
    fontSize: 12,
    padding: 5,
    flex: 0.2,
    marginLeft: 10
  },
  textStyle: {
    fontSize: 13,
    color: colors.textcolor,
    padding: 5,
    fontFamily: "CircularStd-Book"
  },
  headerStyle: {
    backgroundColor: colors.blueColor,
    height: 50,
    flexDirection: "row",
    alignItems: "center"
  },
  headerTextStyle: {
    color: colors.white,
    fontFamily: "CircularStd-Book",
    fontSize: 15,
    padding: 10,
    fontWeight: "bold",
    marginLeft: 10,
    justifyContent: "center"
  },
  iconStyle: {
    width: 15,
    height: 15,
    padding: 5,
    marginLeft: 10
  },
  circle: {
    width: 5,
    height: 5,
    borderRadius: 5 / 2,
    backgroundColor: colors.black,
    marginTop: 10,
    marginLeft: 10
  },
  VerticallineStyle: {
    borderLeftColor: colors.border_grey,
    borderLeftWidth: 1,
    marginTop: 10,
    marginBottom: 10
  },
  timeStyle: {
    fontSize: 13,
    color: colors.textcolor,
    fontFamily: "CircularStd-Book",
    paddingLeft: 5
  },
  BorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    paddingLeft: 5,
    paddingRight: 5,
    flexDirection: "column"
  }
});
