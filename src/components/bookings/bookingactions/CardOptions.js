import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../../../colors/Colors";

class CardOptions extends Component {
  renderOptions() {
    const { type } = this.props;
    const {
      onAcceptTap,
      onEditTap,
      onRejectTap,
      onHistoryTap,
      screen,
      onAssignTap,
      onDock,
      onReAssign,
      change,
      onCommentTap,
      editTripReferenceFlag,
      onReferenceTap,
      onHistoryTapNav,
      onCommentTapNav,
      rightsArray,
      onLocationTap,
      locationKey,
      role
    } = this.props;

    console.log("screen ----", screen);

    if (screen === "new_request") {
      console.log("---- card options ----");
      return (
        <View
          style={change === "1" ? styles.selectedcardStyle : styles.cardStyle}
        >
          <View style={[styles.optionContainer, styles.optionContainerRight]}>
            <TouchableOpacity onPress={onAcceptTap} activeOpacity={0.9}>
              <Text style={styles.optionStyle}>Accept</Text>
            </TouchableOpacity>
          </View>

          <View style={[styles.optionContainer, styles.optionContainerRight]}>
            <TouchableOpacity onPress={onEditTap} activeOpacity={0.9}>
              <Text style={styles.optionStyle}>Edit</Text>
            </TouchableOpacity>
          </View>

          <View style={[styles.optionContainer, styles.optionContainerRight]}>
            <TouchableOpacity onPress={onRejectTap} activeOpacity={0.9}>
              <Text style={styles.optionStyle}>Reject</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.optionContainer}>
            <TouchableOpacity onPress={onHistoryTap} activeOpacity={0.9}>
              <Text style={styles.optionStyle}>History</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    } else if (screen === "accepted") {
      return (
        <View
          style={change === "1" ? styles.selectedcardStyle : styles.cardStyle}
        >
          {rightsArray.includes("assign-booking") ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onAssignTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Assign</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          {rightsArray.includes("reject-booking") ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onRejectTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Reject</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          {rightsArray.includes("edit-booking") && role !== "GRN-OPS" ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onEditTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Edit</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          <View style={styles.optionContainer}>
            <TouchableOpacity onPress={onHistoryTap} activeOpacity={0.9}>
              <Text style={styles.optionStyle}>History</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    } else if (screen === "assigned") {
      return (
        <View
          style={change === "1" ? styles.selectedcardStyle : styles.cardStyle}
        >
          {rightsArray.includes("dock-booking") ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onDock} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Dock</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          {rightsArray.includes("assign-booking") ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onReAssign} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Re-Assign</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          {rightsArray.includes("reject-booking") ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onRejectTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Reject</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          {rightsArray.includes("edit-booking") && role !== "GRN-OPS" ? (
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onEditTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Edit</Text>
              </TouchableOpacity>
            </View>
          ) : null}

          <View style={styles.optionContainer}>
            <TouchableOpacity onPress={onHistoryTap} activeOpacity={0.9}>
              <Text style={styles.optionStyle}>History</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    } else if (screen === "docked") {
      return (
        <View
          style={change === "1" ? styles.selectedcardStyle : styles.cardStyle}
        >
          <View style={styles.optionContainer}>
            <TouchableOpacity onPress={onHistoryTap} activeOpacity={0.9}>
              <Text style={styles.optionStyle}>History</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    } else if (screen === "searchTripDetails") {
      if (editTripReferenceFlag === 1) {
        return (
          <View
            style={change === "1" ? styles.selectedcardStyle : styles.cardStyle}
          >
            {rightsArray.includes("edit-from-search") ? (
              <View
                style={[styles.optionContainer, styles.optionContainerRight]}
              >
                <TouchableOpacity onPress={onEditTap} activeOpacity={0.9}>
                  <Text style={styles.optionStyle}>Trip Edit</Text>
                </TouchableOpacity>
              </View>
            ) : null}
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onReferenceTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Reference</Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onCommentTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Comments</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.optionContainer}>
              <TouchableOpacity onPress={onHistoryTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>History</Text>
              </TouchableOpacity>
            </View>
          </View>
        );
      } else if (locationKey === "1") {
        return (
          <View
            style={change === "1" ? styles.selectedcardStyle : styles.cardStyle}
          >
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onCommentTapNav} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Comments</Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onHistoryTapNav} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>History</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.optionContainer}>
              <TouchableOpacity onPress={onLocationTap} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Location</Text>
              </TouchableOpacity>
            </View>
          </View>
        );
      } else {
        return (
          <View
            style={change === "1" ? styles.selectedcardStyle : styles.cardStyle}
          >
            <View style={[styles.optionContainer, styles.optionContainerRight]}>
              <TouchableOpacity onPress={onCommentTapNav} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>Comments</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.optionContainer}>
              <TouchableOpacity onPress={onHistoryTapNav} activeOpacity={0.9}>
                <Text style={styles.optionStyle}>History</Text>
              </TouchableOpacity>
            </View>
          </View>
        );

        console.log("card options else ");
      }
    }
  }

  render() {
    const { visible } = this.props;
    if (visible) {
      return <View>{this.renderOptions()}</View>;
    } else {
      return null;
    }
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderWidth: 1,
    borderColor: "#2196f3",
    flexDirection: "row",
    flex: 1,
    justifyContent: "space-between"
  },
  selectedcardStyle: {
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderWidth: 1,
    borderColor: "#2196f3",
    flexDirection: "row",
    flex: 1,
    justifyContent: "space-between",
    backgroundColor: colors.blue
  },
  optionStyle: {
    alignSelf: "center",
    fontSize: 13,
    color: "#1e88e5",
    paddingTop: 8,
    paddingBottom: 8,
    fontFamily: "CircularStd-Book"
  },
  optionContainer: {
    flex: 1,
    borderColor: "#2196f3"
  },
  optionContainerRight: {
    borderRightWidth: 1
  }
});

export default CardOptions;
