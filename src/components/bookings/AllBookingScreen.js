import React from "react";
import { Image } from "react-native";
import {
  createBottomTabNavigator,
  createStackNavigator
} from "react-navigation";
import NewRequestBookings from "./NewRequestBookings";
import AcceptedBookings from "./AcceptedBookings";
import AssignedBookings from "./AssignedBookings";
import DockedBookings from "./DockedBookings";
import colors from "../../colors/Colors";
import newrequest from "../../images/newrequest.png";
import accepted from "../../images/accepted.png";
import assigned from "../../images/shipped.png";
import dock from "../../images/dock.png";
import { Icon } from "react-native-vector-icons";

const NewRequest = createStackNavigator({
  NewRequest: {
    screen: NewRequestBookings,
    navigationOptions: { header: null }
  }
});

const Accepted = createStackNavigator({
  Accepted: {
    screen: AcceptedBookings,
    navigationOptions: {
      header: null

      // title: 'Accepted',
      // }
    }
  }
});

const Assigned = createStackNavigator({
  Assigned: {
    screen: AssignedBookings,
    navigationOptions: {
      header: null
      // title: 'Assigned',
    }
  }
});

const Docked = createStackNavigator({
  Docked: {
    screen: DockedBookings,
    navigationOptions: {
      header: null
      // title: 'Docked',
    }
  }
});

const AllBookingScreen = createBottomTabNavigator(
  {
    "New Request": {
      screen: NewRequestBookings,
      navigationOptions: { header: null }
    },
    Accepted: {
      screen: AcceptedBookings,
      navigationOptions: {
        header: null
      }
    },
    Assigned: {
      screen: AssignedBookings,
      navigationOptions: {
        header: null
      }
    },
    Docked: {
      screen: DockedBookings,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    tabBarOptions: {
      activeTintColor: colors.blueColor,
      inactiveTintColor: colors.black,
      style: {
        backgroundColor: colors.white,
        borderBottomWidth: 0,
        borderBottomColor: colors.blueColor
      },
      indicatorStyle: {
        backgroundColor: colors.redColor
      }
    },
    headerMode: null,
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName = "";
        if (routeName === "New Request") {
          return (
            <Image style={{ height: 14, width: 14 }} source={newrequest} />
          );
          // iconName = 'assignment';
        } else if (routeName === "Accepted") {
          // iconName = 'checklist';
          return <Image style={{ height: 14, width: 14 }} source={accepted} />;
        } else if (routeName === "Assigned") {
          // iconName = 'truck-check';
          return <Image style={{ height: 14, width: 14 }} source={assigned} />;
        } else if (routeName === "Docked") {
          // iconName = 'clock';
          return <Image style={{ height: 14, width: 14 }} source={dock} />;
        }
        return <Icon name={iconName} size={25} color={colors.blueColor} />;
      }
    })
  }
);

export default AllBookingScreen;
// export default createAppContainer(AllBookingScreen);

// export default class AllBookingScreen extends Component {
//
//
//     render() {
//         const bottomTabNavigator = createBottomTabNavigator({
//             NewRequest: {
//                 screen: NewRequestBookings,
//                 navigationOptions: {
//                     title: 'New Request',
//                 }
//             },
//             Accepted: {
//                 screen: AcceptedBookings,
//                 navigationOptions: {
//                     title: 'Accepted',
//                 }
//             },
//             Assigned: {
//                 screen: AssignedBookings,
//                 navigationOptions: {
//                     title: 'Assigned',
//                 }
//             },
//             Docked: {
//                 screen: DockedBookings,
//                 navigationOptions: {
//                     title: 'Docked',
//                 }
//             },
//
//         });
//
//         const Navigator = createSwitchNavigator(
//             {
//                 Booking: bottomTabNavigator,
//
//             }, {
//                 BookingHistory: {
//                     screen: BookingHistory
//                 },
//             }
//         );
//
//         const MainNavigator = createAppContainer(Navigator);
//
//
//         return (
//             <MainNavigator/>
//         );
//     }
//
// }

const styles = {
  tabStyle: {
    backgroundColor: colors.blue
  },
  labelStyle: {
    fontSize: 14
  }
};
