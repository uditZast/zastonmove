import React, { Component } from "react";
import {
  Platform,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import colors from "../../colors/Colors";
import Card from "../Card";
import CardOptions from "./bookingactions/CardOptions";
import RejectModal from "../../containers/RejectionReasonsInput";
import { REQUESTED_BOOKING } from "../../constants/Constants";
import { BarIndicator } from "react-native-indicators";
import Snackbar from "react-native-snackbar";
import NetInfo from "@react-native-community/netinfo";
import { Container } from "../common/Container";
import axios from "axios";

// let selectedIndex = 0;
export default class NewRequestBookings extends Component {
  selectedIndex = 0;

  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      OriginalData: [],
      dataSource_filter: [],
      actionsArray: [],
      isLoading: true,
      showActions: false,
      showModal: false,
      historyArray: [],
      bookingId: "",
      tripId: "",
      action: "",
      drop_down_data: [],
      showRejectModal: false,
      text: "",
      refreshing: false,
      data: [],
      values: "",
      rightsArray: []
    };
  }

  componentDidMount() {
    AsyncStorage.getItem("rights").then(value =>
      this.setState({ rightsArray: value })
    );

    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        this.getNewBookingList();

        this._onFocusListener = this.props.navigation.addListener(
          "didFocus",
          payload => {
            this.selectedIndex = 0;
            this.setState({ text: "" });
            this.getNewBookingList();
            console.log("component did mount ");
          }
        );
      } else {
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });
  }

  componentWillUnmount() {
    this._onFocusListener.remove();
  }

  clearState() {
    this.state = {
      dataSource: [],
      OriginalData: [],
      dataSource_filter: [],
      actionsArray: [],
      isLoading: true,
      showActions: false,
      showModal: false,
      historyArray: [],
      bookingId: "",
      tripId: "",
      action: "",
      drop_down_data: [],
      showRejectModal: false,
      text: "",
      refreshing: false
    };
  }

  getNewBookingList() {
    AsyncStorage.getItem("token").then(value => {
      this.setState({ values: value });

      AsyncStorage.getItem("token").then(value => {
        console.log("token:::::::" + value);
        axios
          .get(REQUESTED_BOOKING, {
            headers: {
              Authorization: "Token " + value,
              "Content-Type": "application/json"
            }
          })
          .then(response => {
            console.log("then new request");
            this.setState(
              {
                dataSource: response.data.data,
                OriginalData: response.data.data,
                actionsArray: response.data.data,
                refreshing: false,
                isLoading: false
              },
              () => console.log(this.state)
            );
            return response.data;
          })
          .catch(error => {
            console.log("catch new request" + JSON.stringify(error));
            if (error.response.status === 401) {
              AsyncStorage.removeItem("token");
              this.props.navigation.navigate("Auth");
            } else {
            }
          });
      });

      // fetch(REQUESTED_BOOKING, {
      //     headers: {
      //         'Authorization': 'Token ' + value
      //     }
      // })
      //     .then((response) => response.json())
      //     .then((responseJson) => {
      //         console.log(responseJson);
      //         this.setState({
      //             dataSource: responseJson.data,
      //             OriginalData: responseJson.data,
      //             actionsArray: responseJson.data,
      //             refreshing: false,
      //             isLoading: false
      //         }, () => console.log(this.state))
      //
      //     })
      //     .catch((error) => {
      //         console.error(error);
      //
      //     });
    });
  }

  filterSearch(text) {
    const dataSource_filter = this.state.OriginalData.filter(item => {
      const itemData =
        item.code.toUpperCase() +
        item.type.toUpperCase() +
        item.route.toUpperCase() +
        item.vehicle +
        item.vehicle_type +
        item.customer;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      text: text,
      dataSource: dataSource_filter // after filter we are setting users to new array
    });
  }

  _onRefresh = () => {
    this.selectedIndex = 0;
    this.setState({ refreshing: true });
    this.getNewBookingList();
  };

  checkReturnBookingType(value) {
    if (value !== null) {
      return (
        <View style={{ flex: 1 }}>
          <View style={styles.returnStyle}>
            <Text style={styles.boldTextStyle}>{value.code}</Text>
            <Text style={styles.normalTextStyle}>{value.type}</Text>
          </View>

          {this.checkVehicle1(value.vehicle, value.vehicle_type)}
        </View>
      );
    } else {
      return null;
    }
  }

  checkVehicle(value, value1) {
    if (value !== null) {
      return (
        <View style={styles.rowStyle}>
          <Text style={styles.normalTextStyle}>{value}</Text>
          <Text style={styles.normalTextStyle}>{value1}</Text>
        </View>
      );
    } else {
      return (
        <View style={styles.rowStyle2}>
          <Text style={styles.textStyle2}>{value1}</Text>
        </View>
      );
    }
  }

  checkVehicle1(value, value1) {
    if (value !== null) {
      return (
        <View style={styles.rowStyle}>
          <Text style={styles.textStyle2}>{value}</Text>
          <Text style={styles.normalTextStyle}>{value1}</Text>
        </View>
      );
    } else {
      return (
        <View style={{ flex: 0.5, flexDirection: "row", marginLeft: 0 }}>
          <Text style={styles.textStyle2}>{value1}</Text>
        </View>
      );
    }
  }

  checkCustomer(value) {
    if (value !== null && value !== "") {
      return (
        <View style={styles.rowStyle1}>
          <Text style={styles.textStyle1}>Customer</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {value}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkVendor(value) {
    if (value !== null && value !== "") {
      return (
        <View style={styles.rowStyle1}>
          <Text style={styles.textStyle1}>Vendor</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {value}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkRoute(value) {
    if (value !== null) {
      return (
        <View style={styles.rowStyle1}>
          <Text style={styles.textStyle1}>Route</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {value}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkScheduledTime(value) {
    if (value !== null && value !== "") {
      return (
        <View style={styles.rowStyle1}>
          <Text style={styles.textStyle1}>Schedule Time</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {value}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }

  //action Modals

  renderRejectModal() {
    return (
      <RejectModal
        display={this.state.showRejectModal}
        closeModal={() => {
          this.setState({ showRejectModal: false });
        }}
        jsonData={this.state.drop_down_data}
        token={this.state.values}
        bookingId={this.state.bookingId}
        refreshing={() => this._onRefresh()}
      />
    );
  }

  //actions menu

  renderActions(index) {
    let actionsArray = this.state.actionsArray;

    actionsArray.forEach((option, thisindex) => {
      actionsArray[thisindex]["actions"] = thisindex === index;
    });

    this.setState({
      actionsArray
    });
    this.selectedIndex = index;
  }

  renderListActions(element, index) {
    return (
      <CardOptions
        visible={
          element.hasOwnProperty("actions") ? element.actions : index === 0
        }
        onAcceptTap={() =>
          this.props.navigation.push("AdHocOne", {
            bookingId: element.code,
            action: "accept",
            from: "new"
          })
        }
        onEditTap={() =>
          this.props.navigation.push("AdHocOne", {
            bookingId: element.code,
            action: "edit",
            from: "new"
          })
        }
        onRejectTap={() =>
          this.setState({ showRejectModal: true, bookingId: element.code })
        }
        screen="new_request"
        change="1"
        rightsArray={this.state.rightsArray}
      />
    );
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <BarIndicator color={colors.blueColor} size={20} />
        </View>
      );
    }

    return (
      <Container>
        <View style={{ flex: 1 }}>
          <TextInput
            style={styles.customerBorderStyle}
            onChangeText={text => this.filterSearch(text)}
            value={this.state.text}
            placeholderTextColor={"#9D9B9D"}
            placeholder="Search BookingId/ Vehicle/ Vendor/ Route"
          />

          {this.state.dataSource.length > 0 ? (
            <ScrollView
              style={styles.container}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            >
              {this.renderRejectModal()}

              {this.state.dataSource.map((rowData, index) => {
                return (
                  <Card key={rowData.code}>
                    <TouchableOpacity
                      onPress={this.renderActions.bind(this, index)}
                      activeOpacity={0.7}
                    >
                      {console.log(
                        "initial index" +
                          index +
                          " selectedindex" +
                          this.selectedIndex
                      )}

                      {this.selectedIndex === index
                        ? console.log("matched")
                        : console.log("not matched")}
                      <View
                        style={
                          this.selectedIndex === index
                            ? styles.rowBackgroundColor
                            : styles.row1Background
                        }
                      >
                        <View style={{ flex: 1 }}>
                          <View style={styles.forwardStyle}>
                            <Text style={styles.boldTextStyle}>
                              {rowData.code}
                            </Text>
                            <Text style={styles.normalTextStyle}>
                              {rowData.type}
                            </Text>
                          </View>
                          {this.checkVehicle(
                            rowData.vehicle,
                            rowData.vehicle_type
                          )}
                        </View>

                        {this.checkReturnBookingType(rowData.return_booking)}
                      </View>

                      <View
                        style={
                          this.selectedIndex === index
                            ? styles.changeColorlineStyle
                            : styles.lineStyle
                        }
                      />

                      <View
                        style={
                          this.selectedIndex === index
                            ? styles.columnBackgroundColor
                            : styles.row2Background
                        }
                      >
                        {this.checkCustomer(rowData.customer)}

                        {this.checkRoute(rowData.route)}

                        {this.checkScheduledTime(rowData.trip_time)}

                        {rowData.vendor !== null
                          ? this.checkVendor(rowData.vendor.code)
                          : null}

                        <View style={[styles.rowStyle1, { paddingBottom: 5 }]}>
                          <Text style={styles.textStyle1}>Expected TAT</Text>
                          <Text
                            style={[styles.textStyle1, { color: colors.grey }]}
                          >
                            {rowData.expected_tat}
                          </Text>
                        </View>
                      </View>
                    </TouchableOpacity>

                    {this.renderListActions(rowData, index)}
                  </Card>
                );
              })}
            </ScrollView>
          ) : null}
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.listbackgroundcolor
  },
  forwardStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    paddingTop: 3
  },
  returnStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginRight: 10,
    marginLeft: 0,
    marginTop: 3
  },
  boldTextStyle: {
    fontSize: 15,
    fontWeight: "bold",
    color: colors.blueColor,
    fontFamily: "CircularStd-Book"
  },
  normalTextStyle: {
    fontSize: 12,
    color: colors.textcolor,
    marginLeft: 5,
    paddingTop: 3,
    fontFamily: "CircularStd-Book"
  },
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    marginTop: 5,
    paddingBottom: 5
  },
  changeColorlineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 5,
    backgroundColor: colors.blue
  },
  rowStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 5
  },
  rowStyle1: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 3
  },
  textStyle1: {
    flex: 0.5,
    fontSize: 12,
    color: colors.textcolor,
    fontFamily: "CircularStd-Book"
  },
  textStyle2: {
    fontSize: 12,
    color: colors.textcolor,
    paddingTop: 3,
    fontFamily: "CircularStd-Book"
  },
  rowStyle2: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 0
  },
  customerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    ...Platform.select({
      ios: {
        padding: 10,
        borderRadius: 10,
        marginTop: 10,
        overflow: "hidden"
      },
      android: {
        paddingTop: 5,
        paddingLeft: 12,
        paddingRight: 12,
        paddingBottom: 5,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        marginTop: 8
      }
    })
  },
  row1Background: {
    flex: 1,
    flexDirection: "row"
  },
  row2Background: {
    flex: 1,
    paddingBottom: 10
  },
  rowBackgroundColor: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: colors.blue
  },
  columnBackgroundColor: {
    flex: 1,
    backgroundColor: colors.blue
  }
});
