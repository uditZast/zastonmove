import React, { Component } from "react";
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { DOCKED_BOOKING } from "../../constants/Constants";
import CardOptions from "./bookingactions/CardOptions";
import colors from "../../colors/Colors";
import Card from "../Card";
import { BarIndicator } from "react-native-indicators";
import NetInfo from "@react-native-community/netinfo";
import Snackbar from "react-native-snackbar";
import { Container } from "../common/Container";
import axios from "axios";

let selectedIndex = 0;
export default class DockedBookings extends Component {
  state = { data: [], values: "" };

  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      OriginalData: [],
      actionsArray: [],
      isLoading: true,
      showActions: false,
      text: ""
    };
  }

  componentDidMount() {
    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        this.getDockedBooking();

        this._onFocusListener = this.props.navigation.addListener(
          "didFocus",
          payload => {
            selectedIndex = 0;
            this.setState({ text: "" });
            this.getDockedBooking();
            console.log("component did mount ");
          }
        );
      } else {
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });
  }

  getDockedBooking() {
    AsyncStorage.getItem("token").then(value => {
      console.log("token:::::::" + value);
      axios
        .get(DOCKED_BOOKING, {
          headers: {
            Authorization: "Token " + value,
            "Content-Type": "application/json"
          }
        })
        .then(response => {
          console.log("then new accepted");
          this.setState({
            dataSource: response.data.data,
            OriginalData: response.data.data,
            actionsArray: response.data.data,
            isLoading: false
          });
          return response.data;
        })
        .catch(error => {
          console.log("catch accepted" + JSON.stringify(error.response));
          console.log("catch accepted : " + error.response.status);
          if (error.response.status === 401) {
            AsyncStorage.removeItem("token");
            this.props.navigation.navigate("Auth");
          } else {
          }
        });
    });

    // AsyncStorage.getItem('token').then(
    //     (value) => {
    //         this.setState({value: value});
    //         fetch(DOCKED_BOOKING, {
    //             headers: {
    //                 'Authorization': 'Token ' + value
    //             }
    //         })
    //             .then((response) => response.json())
    //             .then((responseJson) => {
    //                 this.setState({
    //                     dataSource: responseJson.data,
    //                     OriginalData: responseJson.data,
    //                     actionsArray: responseJson.data,
    //                     isLoading: false,
    //                 })
    //
    //             })
    //             .catch((error) => {
    //                 console.log("testing");
    //                 console.error(error);
    //             });
    //
    //     })
  }

  checkVehicle(value, value1) {
    if (value !== null) {
      return (
        <View style={styles.rowStyle}>
          <Text style={styles.normalTextStyle}>{value}</Text>
          <Text style={styles.normalTextStyle}>{value1}</Text>
        </View>
      );
    } else {
      return (
        <View style={styles.rowStyle2}>
          <Text style={styles.textStyle2}>{value1}</Text>
        </View>
      );
    }
  }

  checkVehicle1(value, value1) {
    if (value !== null) {
      return (
        <View style={styles.rowStyle}>
          <Text style={styles.textStyle2}>{value}</Text>
          <Text style={styles.normalTextStyle}>{value1}</Text>
        </View>
      );
    } else {
      return (
        <View style={{ flex: 0.5, flexDirection: "row", marginLeft: 0 }}>
          <Text style={styles.textStyle2}>{value1}</Text>
        </View>
      );
    }
  }

  checkCustomer(value) {
    if (value !== null) {
      return (
        <View style={styles.rowStyle1}>
          <Text style={styles.textStyle1}>Customer</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {value}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkRoute(value) {
    if (value !== null) {
      return (
        <View style={styles.rowStyle1}>
          <Text style={styles.textStyle1}>Route</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {value}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkScheduledTime(value) {
    if (value !== null && value !== "") {
      return (
        <View style={styles.rowStyle1}>
          <Text style={styles.textStyle1}>Schedule Time</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {value}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkVendor(value) {
    if (value !== null) {
      return (
        <View style={styles.rowStyle1}>
          <Text style={styles.textStyle1}>Vendor</Text>
          <Text style={[styles.textStyle1, { color: colors.grey }]}>
            {value}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }

  //actions menu

  renderActions(index) {
    let actionsArray = this.state.actionsArray;

    actionsArray.forEach((option, thisindex) => {
      actionsArray[thisindex]["actions"] = thisindex === index;
    });

    this.setState({ actionsArray });
    selectedIndex = index;
  }

  renderListActions(element, index) {
    return (
      <CardOptions
        visible={
          element.hasOwnProperty("actions") ? element.actions : index === 0
        }
        screen="docked"
        change="1"
      />
    );
  }

  filterSearch(text) {
    console.log("filter " + text);
    const dataSource_filter = this.state.OriginalData.filter(item => {
      const itemData =
        item.code.toUpperCase() +
        item.type.toUpperCase() +
        item.route.toUpperCase() +
        item.vehicle +
        item.vehicle_type +
        item.customer;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      text: text,
      dataSource: dataSource_filter // after filter we are setting users to new array
    });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <BarIndicator color={colors.blueColor} size={20} />
        </View>
      );
    }

    return (
      <Container>
        <ScrollView style={styles.container}>
          <View style={{ backgroundColor: colors.white }}>
            <TextInput
              style={styles.customerBorderStyle}
              onChangeText={text => this.filterSearch(text)}
              value={this.state.text}
              placeholderTextColor={"#9D9B9D"}
              placeholder="Search BookingId/ Vehicle/ Vendor/ Route"
            />
          </View>

          {this.state.dataSource.map((rowData, index) => {
            console.log("dataSource" + this.state.dataSource.length);
            console.log("codeee:::::" + rowData.code);
            console.log("codeee:::::" + index);

            return (
              <Card key={rowData.code}>
                <TouchableOpacity
                  onPress={this.renderActions.bind(this, index)}
                >
                  <View
                    style={
                      selectedIndex === index
                        ? styles.section1ColorChangeStyle
                        : styles.section1Style
                    }
                  >
                    <View style={{ flex: 1 }}>
                      <View style={styles.forwardStyle}>
                        <Text style={styles.boldTextStyle}>{rowData.code}</Text>
                        <Text style={styles.normalTextStyle}>
                          {rowData.type}
                        </Text>
                      </View>
                      {this.checkVehicle(rowData.vehicle, rowData.vehicle_type)}
                    </View>
                  </View>

                  <View
                    style={
                      selectedIndex === index
                        ? [styles.lineStyle, { backgroundColor: colors.blue }]
                        : styles.lineStyle
                    }
                  />

                  <View
                    style={
                      selectedIndex === index
                        ? styles.section2ColorChangeStyle
                        : styles.section2Style
                    }
                  >
                    {this.checkCustomer(rowData.customer)}
                    {this.checkRoute(rowData.route)}

                    {this.checkScheduledTime(rowData.trip_time)}

                    {rowData.vendor !== null
                      ? this.checkVendor(rowData.vendor.code)
                      : null}

                    <View style={styles.rowStyle1}>
                      <Text style={styles.textStyle1}>Expected TAT</Text>

                      <Text style={[styles.textStyle1, { color: colors.grey }]}>
                        {rowData.expected_tat}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>

                {this.renderListActions(rowData, index)}
              </Card>
            );
          })}
        </ScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.listbackgroundcolor
  },
  forwardStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    paddingTop: 3
  },
  boldTextStyle: {
    fontSize: 15,
    fontWeight: "bold",
    color: colors.blueColor,
    fontFamily: "CircularStd-Book"
  },
  normalTextStyle: {
    fontSize: 12,
    color: colors.textcolor,
    marginLeft: 5,
    paddingTop: 3,
    fontFamily: "CircularStd-Book"
  },
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 5
  },
  rowStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 5
  },
  rowStyle1: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10,
    paddingTop: 3,
    paddingBottom: 3
  },
  textStyle1: {
    flex: 0.5,
    fontSize: 12,
    color: colors.textcolor,
    fontFamily: "CircularStd-Book"
  },
  textStyle2: {
    fontSize: 12,
    color: colors.textcolor,
    paddingTop: 3,
    fontFamily: "CircularStd-Book"
  },
  rowStyle2: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 0
  },
  customerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    ...Platform.select({
      ios: {
        padding: 10,
        borderRadius: 10,
        marginTop: 10,
        overflow: "hidden"
      },
      android: {
        paddingTop: 5,
        paddingLeft: 12,
        paddingRight: 12,
        paddingBottom: 5,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        marginTop: 8
      }
    })
  },
  section1Style: {
    flex: 1,
    flexDirection: "row"
  },
  section2Style: {
    flex: 1,
    paddingBottom: 10
  },
  section1ColorChangeStyle: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: colors.blue
  },
  section2ColorChangeStyle: {
    flex: 1,
    backgroundColor: colors.blue
  }
});
