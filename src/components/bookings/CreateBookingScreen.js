import React, {Component} from 'react';
import {Platform, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import colors from "../../colors/Colors";

export default class CreateBookingScreen extends Component {

    gotoAdHocOneWayScreen() {
        this.props.navigation.navigate('AdHocOne', {
            bookingType: 'AH',
            title: 'AdHoc One Way',
            action: 'create',
            from: ''
        });
    }

    gotoAdHocTwoWayScreen() {
        this.props.navigation.navigate('AdHocOne', {
            bookingType: 'AH2',
            title: 'AdHoc Two Way',
            action: 'create',
            from: 'AH2create'
        });
    }

    gotoDryRunScreen() {
        this.props.navigation.navigate('AdHocOne', {bookingType: 'DR', title: 'Dry Run', action: 'create', from: ''});
    }

    render() {
        return (
            <View style={styles.container}>

                <TouchableOpacity style={styles.opacityStyle}
                                  onPress={this.gotoAdHocOneWayScreen.bind(this)}>
                    <Text style={styles.textStyle}>AdHoc One Way </Text>
                </TouchableOpacity>


                <TouchableOpacity style={styles.opacityStyle}
                                  onPress={this.gotoAdHocTwoWayScreen.bind(this)}>
                    <Text style={styles.textStyle}>AdHoc Two Way </Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.opacityStyle}
                                  onPress={this.gotoDryRunScreen.bind(this)}>
                    <Text style={styles.textStyle}>Dry Run</Text>
                </TouchableOpacity>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    textStyle: {
        borderColor: colors.blueColor,
        color: colors.white,
        backgroundColor: colors.blueColor,
        padding: 10,
        textAlign: 'center',
        fontFamily: 'CircularStd-Book',
        fontSize: 14,
        ...Platform.select({
            ios: {
                borderRadius: 5,
                overflow: 'hidden',
            },
            android: {
                borderRadius: 10,
            }
        })
    },
    opacityStyle: {
        marginTop: 15,
        marginLeft: 10,
        marginRight: 10
    }
});