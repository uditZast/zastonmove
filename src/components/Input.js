import React from 'react';
import {TextInput,View,Text,Image,StyleSheet} from 'react-native';
import colors from "../colors/Colors";

const Input=({label,value,onChangeText,placeholder,source})=>{

    const{inputStyle,labelStyle,containerStyle,secureTextEntry,inlineImg}=styles;

    return(
        <View style={styles.containerStyle}>
            {/*<Text style={styles.labelStyle}>{label}</Text>*/}
            <Image source={source}
                   style={styles.icon}/>
            <TextInput
                placeholder={placeholder}
                autocorrect={false}
                style={styles.inputStyle}
                value={ value}
                autoCapitalize={'none'}
                onChangeText={onChangeText}
            />
        </View>
    );
};


const styles=StyleSheet.create({
    inputStyle:{
        color:colors.textcolor,
        paddingRight:5,
        paddingLeft:5,
        fontSize:16,
        lineHeight:23,
        flex:2,
        alignItems:'center',
        marginLeft:20,
    },
    labelStyle:{
        fontSize:18,
        paddingLeft:20,
        flex:1,
    },
    containerStyle:{
        flex:1,
        height:40,
        flexDirection:'row',
        alignItems:'center'
    },
    icon: {
        position: 'absolute',
        width: 15,
        height: 15,
        alignItems:'center',
        marginLeft: 10
    },
    lineStyle: {
        borderBottomWidth: 0.5,
        borderBottomColor: colors.textcolor,
        borderTopWidth: 0,
        borderStartWidth: 0,
        borderRightWidth: 0,
        borderWidth: 1,
        borderColor: '#ddd',
        width: "90%",
        marginLeft: 45,
        marginRight: 20
    }

});

export default Input;