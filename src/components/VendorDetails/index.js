import React, { Component, Fragment } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import ImageViewModal from "../../modal/ImageViewModal";
import colors from "../../components/common/Colors";
import Header from "../common/header";

class VendorDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      panImageView: false,
      chequeImageView: false,
      tdsImageView: false,
      name: null,
      bName: null,
      account: null,
      bank: null,
      ifscCode: null,
      apprStatus: null
    };
  }

  closePanViewModal = () => {
    this.setState({ panImageView: false });
  };
  closeChequeViewModal = () => {
    this.setState({ chequeImageView: false });
  };
  closeTdsViewModal = () => {
    this.setState({ tdsImageView: false });
  };

  showPanViewModal = () => {
    this.setState({ panImageView: true });
  };

  showChequeViewModal = () => {
    this.setState({ chequeImageView: true });
  };

  showTdsViewModal = () => {
    this.setState({ tdsImageView: true });
  };

  render() {
    const { vendorDetailsData, vendorData } = this.props;

    const { requestTripAdvance } = this.props;

    const { vendor_details } = requestTripAdvance;

    const {
      basicInfo: { code, name }
    } = Object.values(vendorData)[0] || {};

    const {
      account,
      bank_name,
      benef_name,
      cheque_url,
      ifsc,
      pan_url,
      status,
      tds_url,
      tds_category,
      tds_declaration_url
    } = vendor_details || {};

    const {
      panImageView = false,
      chequeImageView = false,
      tdsImageView = false
    } = this.state;

    return (
      <Fragment>
        <Header
          name={"VENDOR DETAILS"}
          navigation={this.props.navigation}
          goBack={true}
        />

        <View style={{ backgroundColor: "#EBE9E9", flex: 1 }}>
          <View style={{ marginTop: 10 }} />

          <Text
            style={[
              {
                fontSize: 16,
                color: colors.black65,
                fontFamily: "CircularStd-Book",
                padding: 5,
                fontWeight: "300",
                marginBottom: 5,
                marginLeft: 5
              }
            ]}
          >
            Vendor Details
          </Text>

          <View
            style={[
              styles.cardContainerStyle,
              {
                borderBottomWidth: 0
              },
              styles.cardWithTopRadius,
              styles.cardWithBottomRadius
            ]}
          >
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Text
                style={[
                  styles.labelTextStyle,
                  { fontSize: 16, color: colors.black65, fontWeight: "bold" }
                ]}
              >
                {name}
              </Text>

              <Text
                style={[
                  styles.labelTextStyle,
                  { fontSize: 14, color: colors.black40 }
                ]}
              >
                {" ("}
                {code}
                {")"}
              </Text>
            </View>
            <Text
              style={[
                styles.labelTextStyle,
                { fontSize: 14, color: colors.darkGreen }
              ]}
            >
              {status}
            </Text>
          </View>

          <View style={{ marginTop: 10 }} />

          <Text
            style={[
              {
                fontSize: 16,
                color: colors.black65,
                fontFamily: "CircularStd-Book",
                padding: 5,
                fontWeight: "300",
                marginBottom: 5,
                marginLeft: 5
              }
            ]}
          >
            Beneficiary Details
          </Text>

          <View
            style={[
              styles.cardContainerStyle,
              {
                flexDirection: "row"
              }
            ]}
          >
            <Text
              style={[
                styles.labelTextStyle,
                { fontSize: 16, color: colors.black65, fontWeight: "bold" }
              ]}
            >
              {benef_name}
            </Text>
          </View>

          <View
            style={[
              styles.cardContainerStyle,
              {
                borderBottomWidth: 0
              }
            ]}
          >
            <Text
              style={[
                styles.labelTextStyle,
                { fontSize: 14, color: colors.black40 }
              ]}
            >
              {bank_name}
            </Text>
          </View>

          <View
            style={[
              styles.cardContainerStyle,
              {
                borderBottomWidth: 1,

                flexDirection: "row"
              },
              styles.cardWithBottomRadius
            ]}
          >
            <Text
              style={[
                styles.labelTextStyle,
                { fontSize: 14, color: colors.black40 }
              ]}
            >
              {account}
            </Text>

            <Text
              style={[
                styles.labelTextStyle,
                { fontSize: 14, color: colors.black40 }
              ]}
            >
              {" ("} {ifsc}
              {")"}
            </Text>
          </View>

          <View style={{ marginTop: 10 }} />

          <Text
            style={[
              {
                fontSize: 16,
                color: colors.black65,
                fontFamily: "CircularStd-Book",
                padding: 5,
                fontWeight: "300",
                marginBottom: 5,
                marginLeft: 5
              }
            ]}
          >
            View Documents
          </Text>

          <View
            style={[
              styles.cardContainerStyle,
              {
                borderBottomWidth: 1,
                borderBottomRightRadius: 0,
                borderBottomLeftRadius: 0,
                borderTopLeftRadius: 3,
                borderTopRightRadius: 3
              }
            ]}
          >
            <Text
              style={[
                styles.labelTextStyle,
                { color: colors.black65, fontSize: 16 }
              ]}
            >
              PAN Card
            </Text>
            <View>
              <TouchableOpacity
                onPress={pan_url ? this.showPanViewModal : null}
              >
                <Text
                  style={[
                    styles.labelTextStyle,
                    { color: colors.blue, fontSize: 14 }
                  ]}
                >
                  [View Uploaded File]
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={[
              styles.cardContainerStyle,
              {
                borderBottomWidth: 1,
                borderBottomRightRadius: 0,
                borderBottomLeftRadius: 0,
                borderTopLeftRadius: 0,
                borderTopRightRadius: 0
              }
            ]}
          >
            <Text
              style={[
                styles.labelTextStyle,
                { color: colors.black65, fontSize: 16 }
              ]}
            >
              Cancelled Cheque
            </Text>
            <View>
              <TouchableOpacity
                onPress={cheque_url ? this.showChequeViewModal : null}
              >
                <Text
                  style={[
                    styles.labelTextStyle,
                    { color: colors.blue, fontSize: 14 }
                  ]}
                >
                  [View Uploaded File]
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={[
              styles.cardContainerStyle,
              {
                borderBottomWidth: 1,
                marginBottom: 10
              },
              styles.cardWithBottomRadius
            ]}
          >
            <Text
              style={[
                styles.labelTextStyle,
                { color: colors.black65, fontSize: 16 }
              ]}
            >
              TDS Category
            </Text>
            <View>
              <Text
                style={[
                  { fontSize: 14, color: colors.black40 },
                  styles.labelTextStyle
                ]}
              >
                {tds_category}
              </Text>

              {tds_declaration_url && (
                <TouchableOpacity
                  onPress={tds_declaration_url ? this.showTdsViewModal : null}
                >
                  <Text
                    style={[
                      styles.labelTextStyle,
                      { color: colors.blue, fontSize: 14 }
                    ]}
                  >
                    [View Uploaded File]
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>

          {panImageView && (
            <ImageViewModal
              imageViewVisible={panImageView}
              closeModal={this.closePanViewModal}
              imageVisible={true}
              url={pan_url}
            />
          )}

          {chequeImageView && (
            <ImageViewModal
              imageViewVisible={chequeImageView}
              closeModal={this.closeChequeViewModal}
              imageVisible={true}
              url={cheque_url}
            />
          )}

          {tdsImageView && (
            <ImageViewModal
              imageViewVisible={tdsImageView}
              closeModal={this.closeTdsViewModal}
              imageVisible={true}
              url={tds_declaration_url}
            />
          )}
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  labelTextStyle: {
    fontSize: 14,
    color: colors.black65,
    fontFamily: "CircularStd-Book",
    padding: 4
  },
  cardContainerStyle: {
    borderColor: "#ddd",
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderRadius: 2,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: colors.white
  },
  cardWithTopRadius: {
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0
  },
  cardWithBottomRadius: {
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3
  },
  cardWithNoRadius: {
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0
  }
});

export default VendorDetails;
