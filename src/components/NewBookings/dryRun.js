import React, { Component, Fragment } from "react";
import { View } from "react-native";
import Header from "../common/header";
import moment from "moment";
import VehicleInput from "../common/VehicleInput";
import VendorInput from "../common/VendorsInput";
import DateTimeInput from "../common/DateTimeInput";
import CityInput from "../common/CityInput";
import Footer from "../common/footer";
import { BOOKING_TYPE, NAVIGATE, VEHICLE_CATEGORY } from "../../../constants";
import customStyles from "../common/Styles";
import BookingSuccessModal from "../common/BookingSuccessModal";
import Snackbar from "react-native-snackbar";

class DryRun extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedVehicle: 0,
      selectedVendor: 0,
      selectedDateTime: new moment(),
      selectedSourceCity: 0,
      selectedDestinationCity: 0,
      selectedViaCities: [],
      vendorDisabled: true,
      creatingBooking: false,
      showModal: false,
      dateFlag: false,
      reference: null
    };
  }

  componentDidMount() {}

  selectVehicle = vehicle => {
    const { vehicles } = this.props;
    const { basicInfo: { vehicle_category, vendor_id } = {} } =
      vehicles[vehicle] || {};
    if (VEHICLE_CATEGORY.CON === vehicle_category) {
      this.setState({
        selectedVehicle: vehicle,
        selectedVendor: vendor_id,
        vendorDisabled: true
      });
    } else {
      this.setState({
        selectedVehicle: vehicle,
        selectedVendor: 0,
        vendorDisabled: false
      });
    }
  };

  selectVendor = vendor => {
    this.setState({ selectedVendor: vendor });
  };

  setDate = dateTime => {
    this.setState({ selectedDateTime: dateTime, dateFlag: true });
  };

  selectSourceCity = city => {
    const { selectedDestinationCity, selectedViaCities } = this.state;
    const addedInVia = selectedViaCities.indexOf(city) === -1;
    if (`${city}` !== `${selectedDestinationCity}` && addedInVia) {
      this.setState({ selectedSourceCity: city });
    } else {
      // this.setState({ selectedSourceCity: 0 });
      Snackbar.show({
        title: "Cannot choose same city again.",
        duration: Snackbar.LENGTH_LONG
      });
    }
  };

  selectDestinationCity = city => {
    const { selectedSourceCity, selectedViaCities } = this.state;
    const addedInVia = selectedViaCities.indexOf(city) === -1;
    if (`${selectedSourceCity}` !== `${city}` && addedInVia) {
      this.setState({ selectedDestinationCity: city });
    } else {
      // this.setState({ selectedDestinationCity: 0 });
      Snackbar.show({
        title: "Cannot choose same city again.",
        duration: Snackbar.LENGTH_LONG
      });
    }
  };

  getErrors = () => {
    let message = null;
    let status = false;
    const {
      selectedVehicle,
      selectedVendor,
      selectedDateTime,
      selectedSourceCity,
      selectedDestinationCity,
      dateFlag
    } = this.state;
    if (selectedVehicle && (!selectedVendor || selectedVendor === 0)) {
      message = "Please Select Vendor";
      status = true;
    } else if (dateFlag === false) {
      message = "Please Select Date and Time of trip";
      status = true;
    } else if (!selectedSourceCity) {
      message = "Please Select Source City";
      status = true;
    } else if (!selectedDestinationCity) {
      message = "Please Select Destination City";
      status = true;
    }
    console.log("message ----", message);
    if (message) {
      Snackbar.show({
        title: message,
        duration: Snackbar.LENGTH_LONG
      });
    }
    return status;
  };

  createDryRun = () => {
    const errors = this.getErrors();
    if (errors === false) {
      const { vehicles } = this.props;
      this.setState({ creatingBooking: true });
      const type = BOOKING_TYPE.DRY_RUN;
      const {
        selectedVehicle: vehicle_id,
        selectedVendor: vendor_id,
        selectedDateTime,
        selectedSourceCity: source_city,
        selectedDestinationCity: destination_city,
        reference
      } = this.state;
      const { basicInfo: { vehicle_type_id } = {} } =
        vehicles[vehicle_id] || {};
      const request_data = {
        vehicle_id,
        vehicle_type_id,
        vendor_id,
        forward: {
          date_time: selectedDateTime.format("DD-MM-YYYY HH:mm"),
          source_city,
          destination_city,
          reference
        }
      };
      const { createBooking } = this.props;

      createBooking({ type, request_data })
        .then(result => {
          console.log("result ----", result);
          const { status, message } = result;
          if (status) {
            this.setState({ showModal: message });
          } else {
            Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .finally(() => this.setState({ creatingBooking: false }));
    }
  };

  goToBookings = () => {
    this.setState({ showModal: false }, () => {
      this.props.navigation.navigate(NAVIGATE.ALL_BOOKINGS);
    });
  };

  updateReference = reference => {
    this.setState({ reference });
  };

  render() {
    return (
      <Fragment>
        <Header
          name={"CREATE DRY RUN"}
          navigation={this.props.navigation}
          goBack={true}
        />
        <View style={customStyles.scrollContainer}>
          <VehicleInput
            vehicles={this.props.vehicles}
            vehicleTypes={this.props.vehicleTypes}
            selectedVehicle={this.state.selectedVehicle}
            selectVehicle={this.selectVehicle}
          />
          <VendorInput
            vendors={this.props.vendors}
            selectedVendor={this.state.selectedVendor}
            selectVendor={this.selectVendor}
            disabled={!this.state.selectedVehicle || this.state.vendorDisabled}
          />
          <DateTimeInput
            message={"Planned Trip Date and Time"}
            selectedDateTime={this.state.selectedDateTime}
            selectDateTime={this.setDate}
          />
          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedCity={this.state.selectedSourceCity}
            selectCity={this.selectSourceCity}
            message={"Source City"}
          />
          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedCity={this.state.selectedDestinationCity}
            selectCity={this.selectDestinationCity}
            message={"Destination City"}
          />
          {/*TODO: to add reference uncomment this for dry run*/}

          {/*<NumberInput*/}
          {/*  numericProp={false}*/}
          {/*  message={"Reference"}*/}
          {/*  updateText={this.updateReference}*/}
          {/*  placeholder={"Enter Reference"}*/}
          {/*  initialValue={this.state.reference}*/}
          {/*/>*/}
        </View>
        <Footer
          name={"CREATE BOOKING"}
          action={this.createDryRun}
          loading={this.state.creatingBooking}
        />
        <BookingSuccessModal
          isVisible={!!this.state.showModal}
          message={this.state.showModal}
          closeModal={this.goToBookings}
        />
      </Fragment>
    );
  }
}

export default DryRun;
