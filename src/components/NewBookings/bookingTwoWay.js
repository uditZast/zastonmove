import React, { Component, Fragment } from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import moment from "moment";
import { Icon } from "react-native-elements";
import Header from "../common/header";
import Snackbar from "react-native-snackbar";
import CustomerInput from "../common/CustomerInput";
import VehicleInput from "../common/VehicleInput";
import VehicleTypeInput from "../common/VehicleTypeInput";
import VendorInput from "../common/VendorsInput";
import DateTimeInput from "../common/DateTimeInput";
import NumberInput from "../common/NumberInput";
import CityInput from "../common/CityInput";
import ViaCityInput from "../common/ViaCityInput";
import Footer from "../common/footer";
import {
  AUTO_FILL_TAT,
  BOOKING_TYPE,
  NAVIGATE,
  VEHICLE_CATEGORY
} from "../../../constants";
import colors from "../common/Colors";
import customStyles from "../common/Styles";
import BookingSuccessModal from "../common/BookingSuccessModal";
import ExpectedTatInput from "../common/ExpectedTatInput";
import EngagedByInput from "../../containers/EngagedByInput";

class BookingTwoWay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCustomer: 0,
      selectedVehicle: 0,
      selectedVehicleType: 0,
      selectedVendor: 0,
      forwardDateTime: new moment(),
      returnDateTime: new moment(),
      driverPhoneNumber: null,
      forwardSourceCity: 0,
      forwardDestinationCity: 0,
      forwardViaCities: [],
      forwardExpectedTat: 0,
      returnSourceCity: 0,
      returnDestinationCity: 0,
      returnViaCities: [],
      returnExpectedTat: 0,
      vendorDisabled: true,
      creatingBooking: false,
      showModal: false,
      return_booking_id: 0,
      selectedEngageReason: 0,
      forwardDateFlag: false,
      returnDateFlag: false,
      forwardReference: null,
      returnReference: null
    };
  }

  componentDidMount() {}

  selectCustomer = selectedCustomer => {
    this.setState({ selectedCustomer });
  };

  selectVehicle = vehicle => {
    const { vehicles } = this.props;
    const { basicInfo: { vehicle_type_id, vehicle_category, vendor_id } = {} } =
      vehicles[vehicle] || {};
    if (VEHICLE_CATEGORY.CON === vehicle_category) {
      this.setState({ selectedVendor: vendor_id, vendorDisabled: true });
    } else {
      this.setState({ selectedVendor: 0, vendorDisabled: false });
    }
    this.setState({
      selectedVehicle: vehicle,
      selectedVehicleType: vehicle_type_id
    });
  };

  selectVehicleType = vehicleType => {
    this.setState({
      selectedVehicleType: vehicleType,
      selectedVehicle: 0,
      selectedVendor: 0
    });
  };

  selectVendor = vendor => {
    this.setState({ selectedVendor: vendor });
  };

  setForwardDate = dateTime => {
    this.setState({ forwardDateTime: dateTime, forwardDateFlag: true });
  };

  setReturnDate = dateTime => {
    this.setState({ returnDateTime: dateTime, returnDateFlag: true });
  };

  updateDriverNumber = phoneNumber => {
    this.setState({ driverPhoneNumber: phoneNumber });
  };

  updateForwardTAT = forwardExpectedTat => {
    this.setState({ forwardExpectedTat });
  };

  updateReturnTAT = returnExpectedTat => {
    this.setState({ returnExpectedTat });
  };

  selectForwardSourceCity = city => {
    const { forwardDestinationCity, forwardViaCities } = this.state;
    const addedInVia = forwardViaCities.indexOf(city) === -1;
    if (`${city}` !== `${forwardDestinationCity}` && addedInVia) {
      this.setState({ forwardSourceCity: city });
    } else {
      // this.setState({ forwardSourceCity: 0 });
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 400);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_LONG
      // });
    }
  };

  selectReturnSourceCity = city => {
    const {
      returnDestinationCity,
      returnViaCities,
      forwardSourceCity,
      forwardViaCities
    } = this.state;
    const addedInVia =
      returnViaCities.indexOf(city) === -1 &&
      forwardViaCities.indexOf(city) === -1;
    const isNotSourceOfForward = `${city}` !== `${forwardSourceCity}`;
    if (
      `${city}` !== `${returnDestinationCity}` &&
      addedInVia &&
      isNotSourceOfForward
    ) {
      this.setState({ returnSourceCity: city, forwardDestinationCity: city });
    } else {
      // this.setState({ returnSourceCity: 0 });
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_LONG
      // });
    }
  };

  selectForwardDestinationCity = city => {
    const {
      forwardSourceCity,
      forwardViaCities,
      returnViaCities,
      returnDestinationCity
    } = this.state;
    const addedInVia =
      forwardViaCities.indexOf(city) === -1 &&
      returnViaCities.indexOf(city) === -1;
    const isNotDestinationOfReturn = `${city}` !== `${returnDestinationCity}`;
    if (
      `${forwardSourceCity}` !== `${city}` &&
      isNotDestinationOfReturn &&
      addedInVia
    ) {
      this.setState({ forwardDestinationCity: city, returnSourceCity: city });
    } else {
      // this.setState({ forwardDestinationCity: 0 });
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_LONG
      // });
    }
  };

  selectReturnDestinationCity = city => {
    const { returnSourceCity, returnViaCities } = this.state;
    const addedInVia = returnViaCities.indexOf(city) === -1;
    if (`${returnSourceCity}` !== `${city}` && addedInVia) {
      this.setState({ returnDestinationCity: city });
    } else {
      // this.setState({ returnDestinationCity: 0 });
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_LONG
      // });
    }
  };

  addForwardSelectedViaCity = cityId => {
    const {
      forwardViaCities = [],
      forwardSourceCity,
      forwardDestinationCity
    } = this.state;
    if (
      forwardViaCities.indexOf(cityId) === -1 &&
      forwardSourceCity !== cityId &&
      forwardDestinationCity !== cityId
    ) {
      this.setState(prevState => {
        const { forwardViaCities } = prevState;
        const newForwardVia = [...forwardViaCities];
        newForwardVia.push(cityId);
        return { forwardViaCities: newForwardVia };
      });
    } else {
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_LONG
      // });
    }
  };

  removeForwardSelectedViaCity = index => {
    this.setState(prevState => {
      const { forwardViaCities } = prevState;
      const newForwardVia = [...forwardViaCities];
      newForwardVia.splice(index, 1);
      return { forwardViaCities: newForwardVia };
    });
  };

  addReturnSelectedViaCity = cityId => {
    const {
      returnViaCities = [],
      returnSourceCity,
      returnDestinationCity
    } = this.state;
    if (
      returnViaCities.indexOf(cityId) === -1 &&
      returnSourceCity !== cityId &&
      returnDestinationCity !== cityId
    ) {
      this.setState(prevState => {
        const { returnViaCities } = prevState;
        const newReturnVia = [...returnViaCities];
        newReturnVia.push(cityId);
        return { returnViaCities: newReturnVia };
      });
    } else {
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_LONG
      // });
    }
  };

  removeReturnSelectedViaCity = index => {
    this.setState(prevState => {
      const { returnViaCities } = prevState;
      const newReturnVia = [...returnViaCities];
      newReturnVia.splice(index, 1);
      return { returnViaCities: newReturnVia };
    });
  };

  getErrors = () => {
    let message = null;
    let status = false;
    const {
      selectedCustomer,
      selectedVehicle,
      selectedVehicleType,
      selectedVendor,
      driverPhoneNumber,
      forwardDateTime,
      returnDateTime,
      forwardSourceCity,
      forwardDestinationCity,
      forwardExpectedTat,
      returnSourceCity,
      returnDestinationCity,
      returnExpectedTat,
      forwardDateFlag,
      returnDateFlag
    } = this.state;
    if (!selectedCustomer) {
      message = "Please Select Customer";
      status = true;
    } else if (!selectedVehicleType || selectedVehicleType === 0) {
      message = "Please Select Vehicle Or Vehicle Type";
      status = true;
    } else if (selectedVehicle && (!selectedVendor || selectedVendor === 0)) {
      message = "Please Select Vendor";
      status = true;
    } else if (selectedVehicle && !driverPhoneNumber) {
      message = "Please Enter Driver Number";
      status = true;
    } else if (forwardDateFlag === false || returnDateFlag === false) {
      message = "Please Select Date and Time of both trips";
      status = true;
    } else if (!forwardSourceCity || !returnSourceCity) {
      message = "Please Select Source City";
      status = true;
    } else if (!forwardDestinationCity || !returnDestinationCity) {
      message = "Please Select Destination City";
      status = true;
    } else if (!forwardExpectedTat || !returnExpectedTat) {
      message = "Please enter expectedTat";
      status = true;
    }
    console.log("message ----", message);
    if (message) {
      Snackbar.show({
        title: message,
        duration: Snackbar.LENGTH_LONG
      });
    }
    return status;
  };

  createTwoWayBooking = () => {
    const errors = this.getErrors();
    if (errors === false) {
      this.setState({ creatingBooking: true });
      const type = BOOKING_TYPE.AD_HOC_TWO_WAY;
      const {
        selectedCustomer: customer_id,
        selectedVehicle: vehicle_id,
        selectedVehicleType: vehicle_type_id,
        selectedVendor: vendor_id,
        driverPhoneNumber: driver_phone,
        forwardDateTime,
        forwardSourceCity,
        forwardDestinationCity,
        forwardViaCities,
        forwardExpectedTat,
        returnDateTime,
        returnSourceCity,
        returnDestinationCity,
        returnViaCities,
        returnExpectedTat,
        selectedEngageReason: engaged_by_id,
        forwardReference,
        returnReference
      } = this.state;
      const request_data = {
        customer_id,
        vehicle_id,
        vehicle_type_id,
        vendor_id,
        driver_phone,
        engaged_by_id,
        forward: {
          date_time: forwardDateTime.format("DD-MM-YYYY HH:mm"),
          source_city: forwardSourceCity,
          destination_city: forwardDestinationCity,
          via_cities: forwardViaCities,
          expected_tat: forwardExpectedTat,
          reference: forwardReference
        },
        return: {
          date_time: returnDateTime.format("DD-MM-YYYY HH:mm"),
          source_city: returnSourceCity,
          destination_city: returnDestinationCity,
          via_cities: returnViaCities,
          expected_tat: returnExpectedTat,
          reference: returnReference
        }
      };
      const { createBooking } = this.props;
      createBooking({ type, request_data })
        .then(result => {
          console.log("result ----", result);
          const { status, message } = result;
          if (status) {
            this.setState({ showModal: message });
          } else {
            Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .finally(() => this.setState({ creatingBooking: false }));
    }
  };

  goToBookings = () => {
    this.setState({ showModal: false }, () => {
      this.props.navigation.navigate(NAVIGATE.ALL_BOOKINGS);
    });
  };

  setEngageReason = selectedEngageReason => {
    this.setState({ selectedEngageReason });
  };

  updateForwardReference = forwardReference => {
    this.setState({ forwardReference });
  };

  updateReturnReference = returnReference => {
    this.setState({ returnReference });
  };

  render() {
    const { customers } = this.props;
    const { selectedCustomer } = this.state;
    return (
      <Fragment>
        <Header
          name={"CREATE AD HOC TWO WAY"}
          navigation={this.props.navigation}
          goBack={true}
        />
        <ScrollView contentContainerStyle={customStyles.scrollContainer}>
          <CustomerInput
            customers={customers}
            disabled={false}
            customerIds={this.props.customerIds}
            selectedCustomer={selectedCustomer}
            selectCustomer={this.selectCustomer}
          />
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <VehicleInput
                vehicles={this.props.vehicles}
                vehicleTypes={this.props.vehicleTypes}
                selectedVehicle={this.state.selectedVehicle}
                selectVehicle={this.selectVehicle}
              />
            </View>
            <View style={{ flex: 1 }}>
              <VehicleTypeInput
                vehiclesTypes={this.props.vehicleTypes}
                selectedVehicle={this.state.selectedVehicleType}
                selectVehicle={this.selectVehicleType}
                selectOne={true}
                textCenter={false}
              />
            </View>
          </View>
          <VendorInput
            vendors={this.props.vendors}
            selectedVendor={this.state.selectedVendor}
            selectVendor={this.selectVendor}
            disabled={!this.state.selectedVehicle || this.state.vendorDisabled}
          />
          <NumberInput
            numericProp={true}
            message={"Driver Phone"}
            updateText={this.updateDriverNumber}
            placeholder={"Enter Driver Phone Number"}
            initialValue={this.state.driverPhoneNumber}
          />

          <EngagedByInput
            engagedBy={this.props.engagedBy}
            selectedEngageReason={this.state.selectedEngageReason}
            setEngageReason={this.setEngageReason}
          />

          {/*==============================FORWARD===============================*/}
          <View style={styles.subheadingContainer}>
            <Text style={styles.subheading}>FORWARD</Text>
            <Icon
              name="truck-delivery"
              type="material-community"
              color={colors.darkblue}
              containerStyle={{ marginLeft: 8 }}
            />
          </View>

          <DateTimeInput
            message={"Forward Trip Date and Time"}
            selectedDateTime={this.state.forwardDateTime}
            selectDateTime={this.setForwardDate}
          />

          <NumberInput
            numericProp={false}
            message={"Reference"}
            updateText={this.updateForwardReference}
            placeholder={"Enter Forward Reference"}
            initialValue={this.state.forwardReference}
          />

          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedCity={this.state.forwardSourceCity}
            selectCity={this.selectForwardSourceCity}
            message={"Source City"}
          />
          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedCity={this.state.forwardDestinationCity}
            selectCity={this.selectForwardDestinationCity}
            message={"Destination City"}
          />
          <ViaCityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedViaCities={this.state.forwardViaCities}
            addViaCity={this.addForwardSelectedViaCity}
            removeViaCity={this.removeForwardSelectedViaCity}
          />
          <ExpectedTatInput
            message={"Expected TAT"}
            updateText={this.updateForwardTAT}
            vehicle_type_id={this.state.selectedVehicleType}
            customer_id={this.state.selectedCustomer}
            source_city={this.state.forwardSourceCity}
            destination_city={this.state.forwardDestinationCity}
            via_cities={this.state.forwardViaCities}
            source_flag={AUTO_FILL_TAT.CREATE_BOOKING}
            placeholder={"Click Icon to autofill tat"}
          />
          {/*==============================RETURN===============================*/}
          <View style={styles.subheadingContainer}>
            <Text style={styles.subheading}>RETURN</Text>
            <Icon
              name="truck-delivery"
              type="material-community"
              color={colors.darkblue}
              iconStyle={{ transform: [{ rotateY: "180deg" }] }}
              containerStyle={{ marginLeft: 8 }}
            />
          </View>
          <DateTimeInput
            message={"Return Trip Date and Time"}
            selectedDateTime={this.state.returnDateTime}
            selectDateTime={this.setReturnDate}
          />

          <NumberInput
            numericProp={false}
            message={"Reference"}
            updateText={this.updateReturnReference}
            placeholder={"Enter Return Reference"}
            initialValue={this.state.returnReference}
          />

          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedCity={this.state.returnSourceCity}
            selectCity={this.selectReturnSourceCity}
            message={"Source City"}
          />
          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedCity={this.state.returnDestinationCity}
            selectCity={this.selectReturnDestinationCity}
            message={"Destination City"}
          />
          <ViaCityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedViaCities={this.state.returnViaCities}
            addViaCity={this.addReturnSelectedViaCity}
            removeViaCity={this.removeReturnSelectedViaCity}
          />
          <ExpectedTatInput
            message={"Expected TAT"}
            updateText={this.updateReturnTAT}
            vehicle_type_id={this.state.selectedVehicleType}
            customer_id={this.state.selectedCustomer}
            source_city={this.state.returnSourceCity}
            destination_city={this.state.returnDestinationCity}
            via_cities={this.state.returnViaCities}
            source_flag={AUTO_FILL_TAT.CREATE_BOOKING}
            placeholder={"Click Icon to autofill tat"}
          />
        </ScrollView>
        <Footer
          name={"CREATE BOOKING"}
          action={this.createTwoWayBooking}
          loading={this.state.creatingBooking}
        />
        <BookingSuccessModal
          isVisible={!!this.state.showModal}
          message={this.state.showModal}
          closeModal={this.goToBookings}
        />
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  subheadingContainer: {
    paddingTop: 14,
    paddingLeft: 12,
    paddingRight: 12,
    paddingBottom: 0,
    flexDirection: "row"
  },
  subheading: {
    color: colors.darkblue,
    fontWeight: "600",
    fontSize: 16
  }
});

export default BookingTwoWay;
