import React, { Component, Fragment } from "react";
import { View } from "react-native";
import Header from "../common/header";
import moment from "moment";
import Snackbar from "react-native-snackbar";
import CustomerInput from "../common/CustomerInput";
import VehicleInput from "../common/VehicleInput";
import VehicleTypeInput from "../common/VehicleTypeInput";
import VendorInput from "../common/VendorsInput";
import DateTimeInput from "../common/DateTimeInput";
import NumberInput from "../common/NumberInput";
import CityInput from "../common/CityInput";
import ViaCityInput from "../common/ViaCityInput";
import Footer from "../common/footer";
import {
  AUTO_FILL_TAT,
  BOOKING_TYPE,
  NAVIGATE,
  VEHICLE_CATEGORY
} from "../../../constants";
import BookingSuccessModal from "../common/BookingSuccessModal";
import ExpectedTatInput from "../common/ExpectedTatInput";
import EngagedByInput from "../../containers/EngagedByInput";
import { ScrollView } from "react-navigation";
import PhotoUpload from "../common/PhotoUpload";

class BookingOneWay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCustomer: 0,
      selectedVehicle: 0,
      selectedVehicleType: 0,
      selectedVendor: 0,
      selectedDateTime: new moment(),
      driverPhoneNumber: null,
      selectedSourceCity: 0,
      selectedDestinationCity: 0,
      selectedViaCities: [],
      expectedTat: 0,
      reference: null,
      vendorDisabled: true,
      creatingBooking: false,
      showModal: false,
      selectedEngageReason: 0,
      dateFlag: false
    };
  }

  selectCustomer = selectedCustomer => {
    this.setState({ selectedCustomer });
  };

  selectVehicle = vehicle => {
    const { vehicles } = this.props;
    const { basicInfo: { vehicle_type_id, vehicle_category, vendor_id } = {} } =
      vehicles[vehicle] || {};
    if (VEHICLE_CATEGORY.CON === vehicle_category) {
      this.setState({ selectedVendor: vendor_id, vendorDisabled: true });
    } else {
      this.setState({ selectedVendor: 0, vendorDisabled: false });
    }
    this.setState({
      selectedVehicle: vehicle,
      selectedVehicleType: vehicle_type_id
    });
  };

  selectVehicleType = vehicleType => {
    this.setState({
      selectedVehicleType: vehicleType,
      selectedVehicle: 0,
      selectedVendor: 0
    });
  };

  selectVendor = vendor => {
    this.setState({ selectedVendor: vendor });
  };

  setDate = dateTime => {
    this.setState({ selectedDateTime: dateTime, dateFlag: true });
  };

  updateDriverNumber = phoneNumber => {
    this.setState({ driverPhoneNumber: phoneNumber });
  };

  updateTAT = expectedTat => {
    this.setState({ expectedTat });
  };

  selectSourceCity = city => {
    const { selectedDestinationCity, selectedViaCities } = this.state;
    const addedInVia = selectedViaCities.indexOf(city) === -1;
    if (`${city}` !== `${selectedDestinationCity}` && addedInVia) {
      this.setState({ selectedSourceCity: city });
    } else {
      // this.setState({ selectedSourceCity: 0 });

      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_LONG
      // });
    }
  };

  selectDestinationCity = city => {
    const { selectedSourceCity, selectedViaCities } = this.state;
    const addedInVia = selectedViaCities.indexOf(city) === -1;
    if (`${selectedSourceCity}` !== `${city}` && addedInVia) {
      this.setState({ selectedDestinationCity: city });
    } else {
      // this.setState({ selectedDestinationCity: 0 });
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);
    }
  };

  addSelectedViaCity = cityId => {
    const {
      selectedViaCities = [],
      selectedSourceCity,
      selectedDestinationCity
    } = this.state;
    if (
      selectedViaCities.indexOf(cityId) === -1 &&
      selectedSourceCity !== cityId &&
      selectedDestinationCity !== cityId
    ) {
      this.setState(prevState => {
        const { selectedViaCities } = prevState;
        const newViaCities = [...selectedViaCities];
        newViaCities.push(cityId);
        return { selectedViaCities: newViaCities };
      });
    } else {
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);
    }
  };

  removeSelectedViaCity = index => {
    this.setState(prevState => {
      const { selectedViaCities } = prevState;
      const newViaCities = [...selectedViaCities];
      newViaCities.splice(index, 1);
      return { selectedViaCities: newViaCities };
    });
  };

  getErrors = () => {
    let message = null;
    let status = false;
    const {
      selectedCustomer,
      selectedVehicle,
      selectedVehicleType,
      selectedVendor,
      selectedDateTime,
      driverPhoneNumber,
      selectedSourceCity,
      selectedDestinationCity,
      expectedTat,
      dateFlag
    } = this.state;
    if (!selectedCustomer) {
      message = "Please Select Customer";
      status = true;
    } else if (!selectedVehicleType || selectedVehicleType === 0) {
      message = "Please Select Vehicle Type";
      status = true;
    } else if (selectedVehicle && (!selectedVendor || selectedVendor === 0)) {
      message = "Please Select Vendor";
      status = true;
    } else if (dateFlag === false) {
      message = "Please Select Date and Time of trip";
      status = true;
    } else if (selectedVehicle && !driverPhoneNumber) {
      message = "Please Enter Driver Number";
      status = true;
    } else if (!selectedSourceCity) {
      message = "Please Select Source City";
      status = true;
    } else if (!selectedDestinationCity) {
      message = "Please Select Destination City";
      status = true;
    } else if (!expectedTat) {
      message = "Please enter expectedTat";
      status = true;
    }
    console.log("message ----", message);
    if (message) {
      Snackbar.show({
        title: message,
        duration: Snackbar.LENGTH_LONG
      });
    }
    return status;
  };

  createOneWayBooking = () => {
    const errors = this.getErrors();
    console.log("errors ----", errors);
    if (errors === false) {
      this.setState({ creatingBooking: true });
      const type = BOOKING_TYPE.AD_HOC_ONE_WAY;
      const {
        selectedCustomer: customer_id,
        selectedVehicle: vehicle_id,
        selectedVehicleType,
        selectedVendor: vendor_id,
        selectedDateTime,
        driverPhoneNumber: driver_phone,
        selectedSourceCity: source_city,
        selectedDestinationCity: destination_city,
        selectedViaCities: via_cities,
        expectedTat: expected_tat,
        selectedEngageReason: engaged_by_id,
        reference
      } = this.state;
      const request_data = {
        customer_id,
        vehicle_id,
        vehicle_type_id: `${selectedVehicleType}`,
        vendor_id,
        driver_phone,
        engaged_by_id,
        forward: {
          date_time: selectedDateTime.format("DD-MM-YYYY HH:mm"),
          source_city,
          destination_city,
          expected_tat,
          via_cities,
          reference
        }
      };
      const { createBooking } = this.props;
      createBooking({ type, request_data })
        .then(result => {
          console.log("result ----", result);
          const { status, message } = result;
          if (status) {
            this.setState({ showModal: message });
          } else {
            Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .finally(() => this.setState({ creatingBooking: false }));
    }
  };

  goToBookings = () => {
    this.setState({ showModal: false }, () => {
      this.props.navigation.navigate(NAVIGATE.ALL_BOOKINGS);
    });
  };

  setEngageReason = selectedEngageReason => {
    this.setState({ selectedEngageReason });
  };

  updateReference = reference => {
    this.setState({ reference });
  };

  render() {
    const { customers } = this.props;
    const { selectedCustomer, creatingBooking, driverPhoneNumber } = this.state;

    return (
      <Fragment>
        <Header
          name={"CREATE AD HOC ONE WAY"}
          navigation={this.props.navigation}
          goBack={true}
        />
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <CustomerInput
            customers={customers}
            customerIds={this.props.customerIds}
            selectedCustomer={selectedCustomer}
            selectCustomer={this.selectCustomer}
            disabled={false}
          />
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <VehicleInput
                vehicles={this.props.vehicles}
                vehicleTypes={this.props.vehicleTypes}
                selectedVehicle={this.state.selectedVehicle}
                selectVehicle={this.selectVehicle}
              />
            </View>
            <View style={{ flex: 1 }}>
              <VehicleTypeInput
                vehiclesTypes={this.props.vehicleTypes}
                selectedVehicle={this.state.selectedVehicleType}
                selectVehicle={this.selectVehicleType}
                selectOne={true}
                textCenter={true}
              />
            </View>
          </View>
          <VendorInput
            vendors={this.props.vendors}
            selectedVendor={this.state.selectedVendor}
            selectVendor={this.selectVendor}
            disabled={!this.state.selectedVehicle || this.state.vendorDisabled}
          />
          <NumberInput
            numericProp={true}
            message={"Driver Phone"}
            updateText={this.updateDriverNumber}
            placeholder={"Enter Driver Phone Number"}
            initialValue={driverPhoneNumber}
          />

          <DateTimeInput
            message={"Planned Trip Date and Time"}
            selectedDateTime={this.state.selectedDateTime}
            selectDateTime={this.setDate}
          />
          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedCity={this.state.selectedSourceCity}
            selectCity={this.selectSourceCity}
            message={"Source City"}
          />
          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedCity={this.state.selectedDestinationCity}
            selectCity={this.selectDestinationCity}
            message={"Destination City"}
          />
          <ViaCityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedViaCities={this.state.selectedViaCities}
            addViaCity={this.addSelectedViaCity}
            removeViaCity={this.removeSelectedViaCity}
          />
          <ExpectedTatInput
            message={"Expected TAT"}
            updateText={this.updateTAT}
            vehicle_type_id={this.state.selectedVehicleType}
            customer_id={this.state.selectedCustomer}
            source_city={this.state.selectedSourceCity}
            destination_city={this.state.selectedDestinationCity}
            via_cities={this.state.selectedViaCities}
            source_flag={AUTO_FILL_TAT.CREATE_BOOKING}
            placeholder={"Click Icon to autofill tat"}
            disabledInitialValue={true}
          />

          <NumberInput
            numericProp={false}
            message={"Reference"}
            updateText={this.updateReference}
            placeholder={"Enter Reference"}
            initialValue={this.state.reference}
          />

          <EngagedByInput
            engagedBy={this.props.engagedBy}
            selectedEngageReason={this.state.selectedEngageReason}
            setEngageReason={this.setEngageReason}
          />
        </ScrollView>
        <Footer
          name={"CREATE BOOKING"}
          action={this.createOneWayBooking}
          loading={creatingBooking}
        />
        <BookingSuccessModal
          isVisible={!!this.state.showModal}
          message={this.state.showModal}
          closeModal={this.goToBookings}
        />
      </Fragment>
    );
  }
}

export default BookingOneWay;
