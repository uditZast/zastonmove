import React, { Component, Fragment } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import Header from "../common/header";
import moment from "moment";
import VehicleInput from "../common/VehicleInput";
import VendorInput from "../common/VendorsInput";
import DateTimeInput from "../common/DateTimeInput";
import CityInput from "../common/CityInput";
import Footer from "../common/footer";
import {
  AUTO_FILL_TAT,
  BOOKING_TYPE,
  NAVIGATE,
  VEHICLE_CATEGORY
} from "../../../constants";
import customStyles from "../common/Styles";
import BookingSuccessModal from "../common/BookingSuccessModal";
import { CheckBox } from "react-native-elements";
import colors from "../../components/common/Colors";
import Snackbar from "react-native-snackbar";
import DistanceInputNew from "../common/DistanceInputNew";

let count = 0;

class EditDryRun extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedVehicle: 0,
      selectedVendor: 0,
      selectedDateTime: new moment(),
      selectedSourceCity: 0,
      selectedDestinationCity: 0,
      selectedViaCities: [],
      vendorDisabled: true,
      creatingBooking: false,
      showModal: false,
      expectedTat: 0,
      distance: 0,
      non_billable: false,
      checkBoxVisibility: false,
      distanceEnable: true,
      reference: null,
      expectedTatEnable: true
    };
  }

  componentDidMount() {
    const { action } = this.props;
    count = 0;
    if (action === "accept") {
      this.setState({ checkBoxVisibility: true });
    } else {
      this.setState({ checkBoxVisibility: false });
    }

    this.getBookingData();
  }

  getBookingData = () => {
    const { fetchBookingData, booking_id } = this.props;

    fetchBookingData(booking_id)
      .then(response => {
        const { status = false } = response;

        if (status) {
          const { bookings, booking_id, vehicles } = this.props;
          const {
            basicInfo: {
              vehicle_id,
              vehicle_type_id,
              expected_tat,
              source_city_id,
              destination_city_id,
              trip_time,
              vendor_id,
              phone,
              type,
              engaged_by_id,
              enable_tat,
              distance,
              disable_distance,
              via_cities,
              reference,
              enable_distance
            } = {}
          } = bookings[booking_id];
          const { basicInfo: { vehicle_category } = {} } =
            vehicles[vehicle_id] || {};
          if (VEHICLE_CATEGORY.CON !== vehicle_category) {
            this.setState({ vendorDisabled: false });
          }

          const { status = null } = bookings[booking_id];

          if (status === "DK" || status === "CL") {
            this.setState({
              sourceCityDisable: true,
              destinationCityDisable: true
            });
          }

          if (status !== "RQ") {
            this.setState({
              vehicleDisabled: true,
              vehicleTypeDisabled: true
            });
          }

          if (reference) {
            this.setState({ reference: reference });
          }

          count++;

          this.setState({
            selectedVehicle: vehicle_id,
            selectedVendor: vendor_id,
            selectedSourceCity: source_city_id,
            selectedDestinationCity: destination_city_id,
            driverPhoneNumber: `${phone}`,
            selectedDateTime: new moment(trip_time),
            expectedTat: expected_tat,
            selectedVehicleType: vehicle_type_id,
            bookingType: type,
            selectedEngagedBy: engaged_by_id,
            expectedTatEnable: enable_tat,
            distance: distance,
            bookingStatus: status,
            selectedViaCities: via_cities,
            distanceEnable: enable_distance
          });
        } else {
          Snackbar.show({
            title: "" + response.message,
            duration: Snackbar.LENGTH_SHORT
          });
        }
      })
      .catch(error => {
        console.log("error ----", error);
        Snackbar.show({
          title: "Could not fetch details",
          duration: Snackbar.LENGTH_SHORT
        });
      });
  };

  selectVehicle = vehicle => {
    const { vehicles } = this.props;
    const { basicInfo: { vehicle_category, vendor_id } = {} } =
      vehicles[vehicle] || {};
    if (VEHICLE_CATEGORY.CON === vehicle_category) {
      this.setState({
        selectedVehicle: vehicle,
        selectedVendor: vendor_id,
        vendorDisabled: true
      });
    } else {
      this.setState({
        selectedVehicle: vehicle,
        selectedVendor: 0,
        vendorDisabled: false
      });
    }
  };

  selectVendor = vendor => {
    this.setState({ selectedVendor: vendor });
  };

  setDate = dateTime => {
    this.setState({ selectedDateTime: dateTime });
  };

  selectSourceCity = city => {
    count++;

    const { selectedDestinationCity, selectedViaCities } = this.state;
    // const addedInVia = selectedViaCities.indexOf(city) === -1;
    if (`${city}` !== `${selectedDestinationCity}`) {
      this.setState({ selectedSourceCity: city });
    } else {
      Snackbar.show({
        title: "Cannot choose same city again.",
        duration: Snackbar.LENGTH_SHORT
      });
    }
  };

  selectDestinationCity = city => {
    count++;
    const { selectedSourceCity, selectedViaCities } = this.state;
    // const addedInVia = selectedViaCities.indexOf(city) === -1;
    if (`${selectedSourceCity}` !== `${city}`) {
      this.setState({ selectedDestinationCity: city });
    } else {
      // this.setState({ selectedDestinationCity: 0 });
      Snackbar.show({
        title: "Cannot choose same city again.",
        duration: Snackbar.LENGTH_SHORT
      });
    }
  };

  getErrors = () => {
    let message = null;
    let status = false;
    const {
      selectedVehicle,
      selectedVendor,
      selectedDateTime,
      selectedSourceCity,
      selectedDestinationCity,
      expectedTat,
      distance
    } = this.state;
    console.log("distance -------------------", distance);
    if (selectedVehicle && (!selectedVendor || selectedVendor === 0)) {
      message = "Please Select Vendor";
      status = true;
    } else if (!selectedDateTime) {
      message = "Please Select Date and Time of trip";
      status = true;
    } else if (!selectedSourceCity) {
      message = "Please Select Source City";
      status = true;
    } else if (!selectedDestinationCity) {
      message = "Please Select Destination City";
      status = true;
    } else if (!expectedTat) {
      message = "Please Enter Expected Tat";
      status = true;
    } else if (this.props.action === "accept" && !distance) {
      message = "Please enter Distance";
      status = true;
    }
    if (message) {
      Snackbar.show({
        title: message,
        duration: Snackbar.LENGTH_LONG
      });
    }
    return status;
  };

  editDryRun = () => {
    const errors = this.getErrors();
    if (errors === false) {
      const { vehicles } = this.props;
      this.setState({ creatingBooking: true });
      const type = BOOKING_TYPE.DRY_RUN;
      const {
        selectedVehicle: vehicle_id,
        selectedVendor: vendor_id,
        selectedDateTime,
        selectedSourceCity: source_city,
        selectedDestinationCity: destination_city,
        distance,
        reference
      } = this.state;
      const { basicInfo: { vehicle_type_id } = {} } =
        vehicles[vehicle_id] || {};
      const request_data = {
        vehicle_id,
        vehicle_type_id,
        vendor_id,
        forward: {
          booking_id: this.props.booking_id,
          date_time: new moment(selectedDateTime).format("DD-MM-YYYY HH:mm"),
          source_city,
          destination_city,
          distance,
          expected_tat: this.state.expectedTat,
          reference
        }
      };
      const { editBooking } = this.props;
      editBooking({ type, request_data })
        .then(result => {
          const { status, message } = result;
          if (status) {
            Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });

            this.props.navigation.goBack();
          } else {
            Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .finally(() => this.setState({ creatingBooking: false }));
    }
  };

  goToBookings = () => {
    this.setState({ showModal: false }, () => {
      this.props.navigation.navigate(NAVIGATE.ALL_BOOKINGS);
    });
  };

  acceptDryRun = () => {
    const errors = this.getErrors();
    if (errors === false) {
      const { vehicles } = this.props;
      this.setState({ creatingBooking: true });

      const type = BOOKING_TYPE.DRY_RUN;
      const {
        selectedVehicle: vehicle_id,
        selectedVendor: vendor_id,
        selectedDateTime,
        selectedSourceCity: source_city,
        selectedDestinationCity: destination_city,
        distance,
        reference
      } = this.state;
      const { basicInfo: { vehicle_type_id } = {} } =
        vehicles[vehicle_id] || {};
      const request_data = {
        vehicle_id,
        vehicle_type_id,
        vendor_id,
        forward: {
          booking_id: this.props.booking_id,
          date_time: new moment(selectedDateTime).format("DD-MM-YYYY HH:mm"),
          source_city,
          destination_city,
          distance,
          reference,
          non_billable: this.state.non_billable,
          expected_tat: this.state.expectedTat
        }
      };
      const { acceptBooking } = this.props;
      acceptBooking({ type, request_data })
        .then(result => {
          const { status, message } = result;
          if (status) {
            Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });

            this.goToBookings();
          } else {
            Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .finally(() => this.setState({ creatingBooking: false }));
    }
  };

  updateTAT = (expectedTat, distance) => {
    this.setState({ expectedTat: expectedTat, distance: distance });
  };

  checkBillable = () => {
    if (this.state.non_billable) {
      this.setState({ non_billable: false });
    } else {
      this.setState({ non_billable: true });
    }
  };

  // updateReference = reference => {
  //   this.setState({reference });
  // };

  render() {
    return (
      <Fragment>
        <Header
          name={
            this.props.action === "edit" ? "EDIT BOOKING" : "ACCEPT BOOKING"
          }
          navigation={this.props.navigation}
          goBack={true}
        />
        <View style={customStyles.scrollContainer}>
          <VehicleInput
            vehicles={this.props.vehicles}
            vehicleTypes={this.props.vehicleTypes}
            selectedVehicle={this.state.selectedVehicle}
            selectVehicle={this.selectVehicle}
          />
          <VendorInput
            vendors={this.props.vendors}
            selectedVendor={this.state.selectedVendor}
            selectVendor={this.selectVendor}
            disabled={!this.state.selectedVehicle || this.state.vendorDisabled}
          />
          <DateTimeInput
            message={"Planned Trip Date and Time"}
            selectedDateTime={this.state.selectedDateTime}
            selectDateTime={this.setDate}
            initialValue={this.state.selectedDateTime}
          />
          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedCity={this.state.selectedSourceCity}
            selectCity={this.selectSourceCity}
            message={"Source City"}
          />
          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedCity={this.state.selectedDestinationCity}
            selectCity={this.selectDestinationCity}
            message={"Destination City"}
          />

          <DistanceInputNew
            bookingID={this.props.booking_id}
            message={"Expected TAT"}
            initialValue={this.state.expectedTat}
            updateText={this.updateTAT}
            vehicle_type_id={this.state.selectedVehicleType}
            customer_id={this.state.selectedCustomer}
            source_city={this.state.selectedSourceCity}
            destination_city={this.state.selectedDestinationCity}
            via_cities={this.state.selectedViaCities}
            source_flag={AUTO_FILL_TAT.EDIT_BOOKING}
            placeholder={"Click Icon to autofill tat"}
            distanceValue={this.state.distance}
            action={this.props.action}
            enableDistance={this.state.distanceEnable}
            enableTat={this.state.expectedTatEnable}
            apiHit={this.state.apiHit}
            count={count}
            dryRunFlag={true}
          />

          {/*{this.props.action === "accept" && (*/}
          {/*  <NumberInput*/}
          {/*    message={"Distance"}*/}
          {/*    updateText={this.updateDistance}*/}
          {/*    placeholder={"Distance"}*/}
          {/*    initialValue={this.state.distance}*/}
          {/*    numericProp={true}*/}
          {/*    disableDistance={!this.state.distanceEnable}*/}
          {/*  />*/}
          {/*)}*/}
          {/*<NumberInput*/}
          {/*  message={"Expected TAT"}*/}
          {/*  updateText={this.updateTAT}*/}
          {/*  placeholder={"Expected TAT"}*/}
          {/*  initialValue={this.state.expectedTat}*/}
          {/*  numericProp={true}*/}
          {/*/>*/}

          {/*TODO: to add reference uncomment this for dry run*/}

          {/*<NumberInput*/}
          {/*  numericProp={false}*/}
          {/*  message={"Reference"}*/}
          {/*  updateText={this.updateReference}*/}
          {/*  placeholder={"Enter Reference"}*/}
          {/*  initialValue={this.state.reference}*/}
          {/*/>*/}

          {this.state.checkBoxVisibility && (
            <Fragment>
              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  marginTop: 10,
                  marginBottom: 10,
                  alignItems: "center"
                }}
                onPress={this.checkBillable}
              >
                <CheckBox
                  containerStyle={{ padding: 0, margin: 0 }}
                  right
                  checked={this.state.non_billable}
                  iconType="material-community"
                  checkedIcon="checkbox-marked-outline"
                  uncheckedIcon="checkbox-blank-outline"
                  checkedColor={colors.darkblue}
                  uncheckColor={colors.blue}
                  onPress={this.checkBillable}
                />
                <Text>Please check if this is a non billable dry run.</Text>
              </TouchableOpacity>
            </Fragment>
          )}
        </View>
        <Footer
          name={"SAVE"}
          action={
            this.props.action === "edit" ? this.editDryRun : this.acceptDryRun
          }
          loading={this.state.creatingBooking}
        />
        <BookingSuccessModal
          isVisible={!!this.state.showModal}
          message={this.state.showModal}
          closeModal={this.goToBookings}
        />
      </Fragment>
    );
  }
}

export default EditDryRun;
