import React, { Component, Fragment } from "react";
import {
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  Text,
  View
} from "react-native";
import colors from "../common/Colors";
import { TRIP_ENTITIES } from "../../../constants";
import Modal from "react-native-modal";

class TripCommentsSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: null,
      savingComment: false,
      loading: true
    };
  }

  componentDidMount() {
    const { trip_id, fetchTripComments } = this.props;

    fetchTripComments({ trip_id, entity: TRIP_ENTITIES.COMMENT }).finally(() =>
      this.setState({ loading: false })
    );
  }

  renderComment = comment => {
    const { time, user, comments } = comment;
    return (
      <Fragment>
        <View
          style={{
            backgroundColor: colors.black02,
            marginBottom: 10,
            borderRadius: 6,
            borderWidth: 1,
            borderColor: colors.black15,
            padding: 8
          }}
        >
          <View>
            <Text>{comments}</Text>
          </View>
          <View style={{ alignSelf: "flex-end", paddingTop: 8 }}>
            <Text>
              -by <Text style={{ color: colors.blue }}>{user}</Text>
            </Text>
          </View>
          <View style={{ alignSelf: "flex-end", paddingTop: 4 }}>
            <Text>
              at <Text style={{ color: colors.blue }}>{time}</Text>
            </Text>
          </View>
        </View>
      </Fragment>
    );
  };

  render() {
    const {
      trip,
      tripComments,
      isVisible,
      trip_comment_id,
      closeModal
    } = this.props;
    const { basicInfo: { code } = {} } = trip || {};
    const { data = [] } = tripComments[trip_comment_id] || {};
    const { input, savingComment, loading } = this.state;

    return (
      <Fragment>
        <Modal
          isVisible={isVisible}
          onBackButtonPress={closeModal}
          onBackdropPress={closeModal}
          animationIn={"slideInUp"}
          style={{
            position: "absolute",
            margin: 0,
            top: 95,
            bottom: 0,
            left: 0,
            right: 0
          }}
        >
          <View style={styles.modalView}>
            <View style={{ flex: 1 }}>
              {loading && (
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <ActivityIndicator size="large" color={colors.darkblue} />
                </View>
              )}
              {!loading && (
                <ScrollView
                  contentContainerStyle={{
                    flexGrow: 1,
                    padding: 10,
                    paddingTop: 0
                  }}
                >
                  {data &&
                    data.length > 0 &&
                    data.map((item, index) => {
                      return (
                        <View
                          key={index}
                          style={index === 0 ? { marginTop: 10 } : {}}
                        >
                          {this.renderComment(item)}
                        </View>
                      );
                    })}
                  {!data || (data.length === 0 && <Text>NO COMMENTS</Text>)}
                </ScrollView>
              )}
            </View>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    bottom: 0,
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  modalView: {
    width: "100%",
    flex: 1,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    alignSelf: "center",
    paddingBottom: 30
  }
});

export default TripCommentsSearch;
