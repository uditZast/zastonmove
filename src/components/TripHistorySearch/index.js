import React, { Component, Fragment } from "react";
import {
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  Text,
  View
} from "react-native";
import colors from "../common/Colors";
import { TRIP_ENTITIES } from "../../../constants";
import Modal from "react-native-modal";

class TripHistorySearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  componentDidMount() {
    const { fetchTripHistory, trip_id, trip_history_id } = this.props;
    fetchTripHistory({ trip_id, entity: TRIP_ENTITIES.HISTORY }).finally(() =>
      this.setState({ loading: false })
    );
  }

  renderHistory = historyData => {
    const { time, user, action, status } = historyData;
    return (
      <Fragment>
        <View
          style={{
            backgroundColor: colors.black02,
            marginBottom: 10,
            borderRadius: 6,
            borderWidth: 1,
            borderColor: colors.black15,
            padding: 8
          }}
        >
          <View>
            <Text>{action}</Text>
          </View>
          <View style={{ paddingTop: 8 }}>
            <Text style={{ fontWeight: "500" }}>
              Status: <Text style={{ color: colors.blue }}>{status}</Text>
            </Text>
          </View>
          <View style={{ alignSelf: "flex-end", paddingTop: 8 }}>
            <Text>
              -by <Text style={{ color: colors.blue }}>{user}</Text>
            </Text>
          </View>
          <View style={{ alignSelf: "flex-end", paddingTop: 4 }}>
            <Text>
              at <Text style={{ color: colors.blue }}>{time}</Text>
            </Text>
          </View>
        </View>
      </Fragment>
    );
  };

  render() {
    const {
      trip,
      tripHistory,
      trip_history_id,
      isVisible,
      closeModal
    } = this.props;
    const { basicInfo: { code } = {} } = trip || {};
    const { data = [] } = tripHistory[trip_history_id] || {};
    const { loading } = this.state;

    return (
      <Fragment>
        <Modal
          isVisible={isVisible}
          onBackButtonPress={closeModal}
          onBackdropPress={closeModal}
          animationIn={"slideInUp"}
          style={{
            position: "absolute",
            margin: 0,
            top: 95,
            bottom: 0,
            left: 0,
            right: 0
          }}
        >
          {/*<Header name={code} navigation={this.props.navigation} goBack={true}/>*/}
          <View style={styles.modalView}>
            <View style={{ flex: 1 }}>
              {loading && (
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <ActivityIndicator size="large" color={colors.darkblue} />
                </View>
              )}
              {!loading && (
                <ScrollView
                  contentContainerStyle={{
                    flexGrow: 1,
                    padding: 10
                  }}
                >
                  {data &&
                    data.length > 0 &&
                    data.map((item, index) => {
                      return (
                        <Fragment key={index}>
                          {this.renderHistory(item)}
                        </Fragment>
                      );
                    })}
                  {!data || (data.length === 0 && <Text>NO HISTORY</Text>)}
                </ScrollView>
              )}
            </View>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    bottom: 0,
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  modalView: {
    width: "100%",
    flex: 1,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    alignSelf: "center",
    paddingBottom: 30
  }
});

export default TripHistorySearch;
