import React, { Component, Fragment } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import colors from "../colors/Colors";
import MapView from "react-native-map-clustering";
import { BarIndicator } from "react-native-indicators";
import FloatingActionButton from "./common/FloatingAction";
import Header from "./common/header";
import OfflineBanners from "./common/OfflineBanner";
import { Callout, Marker } from "react-native-maps";
import { Icon } from "react-native-elements";
import DeviceInfo from "react-native-device-info";
import { NAVIGATE, OS } from "../../constants";
import UpdateAppModal from "./common/UpdateAppModal";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      coordinate: {
        latitude: 0,
        longitude: 0
      },
      visible: false
    };
  }

  componentDidMount() {
    console.log("this.props Dashboard---------------------------", this.props);
    const { fetchInitialData, fetchAvailableVehicles } = this.props;

    fetchAvailableVehicles()
      .then(response => {
        console.log("response ----", response);
      })
      .catch(error => {
        console.log("error ----------", error);
      });

    this.initialFunction();
  }

  initialFunction = () => {
    const { fetchInitialData } = this.props;

    fetchInitialData()
      .then(result => {
        const { status, data: { versions = {} } = {} } = result;
        const { auth: { role } = {} } = this.props;
        if (status) {
          const {
            ios_force_update_version,
            ios_recommended_update_version,
            android_force_update_version,
            android_recommended_update_version
          } = versions[role] || {};

          let version = DeviceInfo.getBuildNumber();
          version = version ? Number(version) : 0;
          if (
            (Platform.OS === OS.IOS && version < ios_force_update_version) ||
            (Platform.OS === OS.ANDROID &&
              version < android_force_update_version)
          ) {
            const { signOut } = this.props;
            signOut().then(() =>
              this.props.navigation.navigate(NAVIGATE.FORCED_UPDATE)
            );
          } else if (
            (Platform.OS === OS.IOS &&
              version < ios_recommended_update_version) ||
            (Platform.OS === OS.ANDROID &&
              version < android_recommended_update_version)
          ) {
            this.setState({ visible: true });
          }
        }
      })
      .finally(() => {
        this.setState({ loading: false });
      });
  };

  closeModal = () => {
    this.setState({ visible: false });
  };

  componentDidUpdate(prevProps, prevState) {
    const { isFocused } = this.props;
    const isCurrentlyFocused = isFocused;
    const { isFocused: wasPreviouslyFocused } = prevProps;
    const { fetchInitialData } = this.props;

    if (wasPreviouslyFocused === false && isCurrentlyFocused) {
      console.log("---- component did update called----");
      fetchInitialData();
    }
  }

  render() {
    const { loading = false, visible } = this.state;
    const {
      isConnected,
      pageData: { available_vehicles = [] } = {}
    } = this.props;
    return (
      <Fragment>
        <Header
          name={"Available Vehicles"}
          navigation={this.props.navigation}
          menu={true}
          iconVisible={true}
        />

        <View style={styles.container}>
          <MapView
            style={styles.map}
            clusterColor="#2160FB"
            clusterTextColor="#fff"
            clusterBorderColor="#2160FB"
            clusterBorderWidth={1}
            maxZoomLevel={15}
            minZoomLevel={1}
            region={{
              latitude: 20.5937,
              longitude: 78.9629,
              latitudeDelta: 14.5,
              longitudeDelta: 14.5
            }}
          >
            {available_vehicles.map(item => {
              const {
                coordinate = {},
                free_since,
                isBooked,
                number: itemNumber,
                vehicle_type
              } = item || {};

              const { latitude, longitude } = coordinate || {};

              return (
                <Marker
                  key={item.trip_code}
                  coordinate={{ latitude, longitude }}
                >
                  <Callout>
                    <View style={{ flexDirection: "row", padding: 5 }}>
                      <Text style={styles.textStyle}>{itemNumber}</Text>
                      <Text style={styles.textStyle}>{" | "}</Text>
                      <Text style={styles.textStyle}>{vehicle_type}</Text>
                      <Text style={styles.textStyle}>{" | "}</Text>
                      <Text style={styles.textStyle}>
                        {"free Since : " + free_since}
                      </Text>
                      {isBooked && (
                        <Icon
                          name={"bookmark"}
                          type="material-community"
                          size={15}
                          color={colors.red}
                        />
                      )}
                    </View>
                  </Callout>
                </Marker>
              );
            })}
          </MapView>

          {loading && (
            <View style={styles.backCover}>
              <BarIndicator color={colors.blueColor} size={20} />
            </View>
          )}

          {!isConnected && <OfflineBanners bottom={0} />}

          <UpdateAppModal isVisible={visible} closeModal={this.closeModal} />

          <FloatingActionButton />
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  map: {
    flex: 1,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  backCover: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "rgba(0, 0, 0, 0.15)",
    justifyContent: "center",
    alignItems: "center"
  },
  plainView: {
    width: 60
  },
  textStyle: {
    fontSize: 12,
    fontFamily: "CircularStd-Book"
  }
});

export default Dashboard;
