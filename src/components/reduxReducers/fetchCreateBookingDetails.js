export const MARK_ALL_NOTIFICATIONS_AS_SEEN = "MARK_ALL_NOTIFICATIONS_AS_SEEN";
export const MARK_ALL_NOTIFICATIONS_AS_SEEN_COMPLETED =
  "MARK_ALL_NOTIFICATIONS_AS_SEEN_COMPLETED";
export const MARK_ALL_NOTIFICATIONS_AS_SEEN_COMPLETED_WITH_ERROR =
  "MARK_ALL_NOTIFICATIONS_AS_SEEN_COMPLETED_WITH_ERROR";

export const markAllAsSeen = () => {
  return async dispatch => {
    try {
      dispatch({ type: MARK_ALL_NOTIFICATIONS_AS_SEEN });
      const response = await doRequest({
        method: REQUEST_TYPE.POST,
        url: Notifications.markAllAsSeen()
      });
      const { payload, status } = response;
      if (status === true) {
        dispatch({
          type: MARK_ALL_NOTIFICATIONS_AS_SEEN_COMPLETED,
          payload: { notifications: payload.data }
        });
      } else if (status === false) {
        const { message = {} } = payload;
        dispatch({
          type: MARK_ALL_NOTIFICATIONS_AS_SEEN_COMPLETED_WITH_ERROR,
          payload: { error: message }
        });
      }
    } catch (err) {
      throw err;
    }
  };
};

export default (state = intial_state, action = {}) => {
  const { type, payload = {} } = action;
  switch (type) {
    case MARK_ALL_NOTIFICATIONS_AS_SEEN_COMPLETED:
      return changeUnseenCount(state);
    case MARK_ALL_NOTIFICATIONS_AS_READ_COMPLETED:
      return markAllNotificationAsRead(state);
    case MARK_NOTIFICATION_AS_READ_COMPLETED:
      return markSingleNotificationAsRead(state, payload);
    default: {
      if (payload.notifications) {
        return { ...state, ...payload.notifications };
      }
      return state;
    }
  }
};
