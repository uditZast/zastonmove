import React, { Component, Fragment } from "react";
import {
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import Header from "../common/header";
import CustomStyles from "../common/Styles";
import Snackbar from "react-native-snackbar";
import Footer from "../common/footer";
import { Icon } from "react-native-elements";
import Modal from "react-native-modal";
import colors from "../common/Colors";
import CityInput from "../common/CityInput";
import ViaCityInput from "../common/ViaCityInput";
import { AUTO_FILL_TAT } from "../../../constants";
import ExpectedTatInput from "../common/ExpectedTatInput";

class EditRoute extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedSourceCity: 0,
      selectedDestinationCity: 0,
      selectedViaCities: [],
      expectedTat: null,
      distance: null,
      editingTrip: false,
      distanceDisabled: true,
      isVisible: false
    };
  }

  selectSourceCity = city => {
    const { selectedDestinationCity, selectedViaCities } = this.state;
    const addedInVia = selectedViaCities.indexOf(city) === -1;
    if (`${city}` !== `${selectedDestinationCity}` && addedInVia) {
      this.setState({ selectedSourceCity: city });
    } else {
      // this.setState({ selectedSourceCity: 0 });

      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_LONG
      // });
    }
  };

  selectDestinationCity = city => {
    const { selectedSourceCity, selectedViaCities } = this.state;
    const addedInVia = selectedViaCities.indexOf(city) === -1;
    if (`${selectedSourceCity}` !== `${city}` && addedInVia) {
      this.setState({ selectedDestinationCity: city });
    } else {
      // this.setState({ selectedDestinationCity: 0 });
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_LONG
      // });
    }
  };

  addSelectedViaCity = cityId => {
    const {
      selectedViaCities = [],
      selectedSourceCity,
      selectedDestinationCity
    } = this.state;
    if (
      selectedViaCities.indexOf(cityId) === -1 &&
      selectedSourceCity !== cityId &&
      selectedDestinationCity !== cityId
    ) {
      this.setState(prevState => {
        const { selectedViaCities } = prevState;
        const newVia = [...selectedViaCities];
        newVia.push(cityId);
        return { selectedViaCities: newVia };
      });
    } else {
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_LONG
      // });
    }
  };

  removeSelectedViaCity = index => {
    this.setState(prevState => {
      const { selectedViaCities } = prevState;
      const newVia = [...selectedViaCities];
      newVia.splice(index, 1);
      return { selectedViaCities: newVia };
    });
  };

  closeModal = () => {
    this.setState({ isVisible: false }, this.goBack);
  };

  updateTAT = (expectedTat, distance) => {
    this.setState(() => {
      let result = { expectedTat };
      if (distance) {
        result = { ...result, distanceDisabled: true, distance };
      } else {
        result = { ...result, distanceDisabled: false, distance: null };
      }
      return result;
    });
  };

  updateDistance = distance => {
    this.setState({ distance });
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  updateTrip = () => {
    const { editTrip, trip_id } = this.props;
    const {
      selectedSourceCity: source_city,
      selectedDestinationCity: destination_city,
      selectedViaCities: via_cities,
      expectedTat: expected_tat,
      distance
    } = this.state;
    if (source_city && destination_city && expected_tat && distance) {
      this.setState({ editingTrip: true });
      const data = {
        source_city,
        destination_city,
        via_cities,
        trip_id,
        expected_tat,
        distance
      };
      editTrip(data)
        .then(result => {
          const { status = false, message = "Failed" } = result;
          if (status) {
            this.setState({ isVisible: message });
          } else if (status === false) {
            Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .finally(() => this.setState({ editingTrip: false }));
    } else {
      Snackbar.show({
        title:
          "Please select Source, destination city, distance and expected tat",
        duration: Snackbar.LENGTH_LONG
      });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.selectedSourceCity !== this.state.selectedSourceCity ||
      prevState.selectedDestinationCity !==
        this.state.selectedDestinationCity ||
      prevState.selectedViaCities !== this.state.selectedViaCities
    ) {
      this.setState({ distanceDisabled: true, distance: null });
    }
  }

  render() {
    const { trip, trip_id } = this.props;
    const {
      basicInfo: {
        code,
        expected_tat,
        route,
        distance: distanceValue = "NA"
      } = {}
    } = trip || {};
    const { isVisible, distanceDisabled, distance } = this.state;

    return (
      <Fragment>
        <Header
          name={`EDIT ROUTE (${code})`}
          navigation={this.props.navigation}
          goBack={true}
        />
        <KeyboardAvoidingView
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "center"
          }}
          behavior="padding"
          enabled
          // keyboardVerticalOffset={100}
        >
          <ScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            keyboardShouldPersistTaps="handled"
          >
            <View style={CustomStyles.flex1}>
              <View style={{ flex: 1 }}>
                <View
                  style={{
                    paddingBottom: 8,
                    paddingTop: 10,
                    paddingLeft: 10,
                    paddingRight: 10
                  }}
                >
                  <View style={{ paddingBottom: 10 }}>
                    <Text style={styles.heading}>Current Details</Text>
                  </View>
                  <View style={{ paddingBottom: 6 }}>
                    <Text style={styles.subheading}>Current Route</Text>
                    <Text style={styles.value}>{route}</Text>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      paddingTop: 6,
                      paddingBottom: 6
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <Text style={styles.subheading}>Expected Tat</Text>
                      <Text style={styles.value}>{expected_tat}</Text>
                    </View>

                    <View style={{ flex: 1 }}>
                      <Text style={styles.subheading}>Distance</Text>
                      <Text style={styles.value}>{distanceValue}</Text>
                    </View>
                  </View>
                </View>
                <View style={{ padding: 10 }}>
                  <Text style={styles.heading}>New Details</Text>
                </View>

                <CityInput
                  cities={this.props.cities}
                  cityIds={this.props.cityIds}
                  selectedCity={this.state.selectedSourceCity}
                  selectCity={this.selectSourceCity}
                  message={"Source City"}
                />
                <CityInput
                  cities={this.props.cities}
                  cityIds={this.props.cityIds}
                  selectedCity={this.state.selectedDestinationCity}
                  selectCity={this.selectDestinationCity}
                  message={"Destination City"}
                />
                <ViaCityInput
                  cities={this.props.cities}
                  cityIds={this.props.cityIds}
                  selectedViaCities={this.state.selectedViaCities}
                  addViaCity={this.addSelectedViaCity}
                  removeViaCity={this.removeSelectedViaCity}
                />
                <ExpectedTatInput
                  message={"Expected TAT"}
                  updateText={this.updateTAT}
                  trip_id={this.props.trip_id}
                  source_city={this.state.selectedSourceCity}
                  destination_city={this.state.selectedDestinationCity}
                  via_cities={this.state.selectedViaCities}
                  source_flag={AUTO_FILL_TAT.EDIT_ROUTE}
                  placeholder={"Click Icon to autofill tat"}
                />
                <View
                  style={
                    distanceDisabled
                      ? CustomStyles.borderDisabled
                      : CustomStyles.border
                  }
                >
                  <Text
                    style={
                      distanceDisabled
                        ? CustomStyles.labelTextDisabled
                        : CustomStyles.labelText
                    }
                  >{`Distance`}</Text>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <TextInput
                      placeholder={
                        !distanceDisabled
                          ? "Enter distance in kilometers"
                          : "Please fill TAT first"
                      }
                      style={[
                        CustomStyles.pickerView,
                        distanceDisabled
                          ? { color: colors.black25 }
                          : { color: colors.black85 },
                        {
                          ...Platform.select({
                            ios: { maxHeight: 30, paddingBottom: 10 },
                            android: {
                              flexGrow: 1,
                              paddingBottom: 0,
                              paddingTop: 0
                            }
                          })
                        }
                      ]}
                      onChangeText={text => this.setState({ distance: text })}
                      value={distance ? String(distance) : ""}
                      keyboardType="numeric"
                      placeholderTextColor={colors.black25}
                      editable={!distanceDisabled}
                    />
                  </View>
                </View>
              </View>
              <Footer
                name={"SAVE"}
                action={this.updateTrip}
                loading={this.state.editingTrip}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
        <Modal
          isVisible={!!isVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          animationIn={"slideInUp"}
          style={{ margin: 0 }}
        >
          <TouchableOpacity style={styles.modalView} onPress={this.closeModal}>
            <Icon
              name="check"
              type="evilicon"
              color={colors.greenishBlue}
              size={80}
            />
            <View style={{ paddingTop: 4 }}>
              <Text style={{ color: colors.greenishBlue }}>{isVisible}</Text>
            </View>
          </TouchableOpacity>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalView: {
    width: "80%",
    aspectRatio: 4 / 2,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  heading: { fontSize: 16, fontWeight: "500", color: colors.darkblue },
  subheading: { fontSize: 12, color: colors.black65, paddingBottom: 4 },
  value: { fontSize: 14, color: colors.black85 }
});

export default EditRoute;
