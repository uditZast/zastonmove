import React, { Component, Fragment } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  View,
  KeyboardAvoidingView
} from "react-native";
import Header from "../common/header";
import CustomStyles from "../common/Styles";
import Snackbar from "react-native-snackbar";
import Footer from "../common/footer";
import { Icon } from "react-native-elements";
import Modal from "react-native-modal";
import colors from "../common/Colors";
import { AUTO_FILL_TAT } from "../../../constants";
import ExpectedTatInput from "../common/ExpectedTatInput";
import CustomerInput from "../common/CustomerInput";

class EditCustomer extends Component {
  constructor(props) {
    super(props);
    const { trip } = props;
    const { basicInfo: { customer_id } = {} } = trip || {};
    this.state = {
      selectedCustomer: customer_id,
      expectedTat: null,
      updatingCustomer: false,
      isVisible: false
    };
  }

  closeModal = () => {
    this.setState({ isVisible: false }, this.goBack);
  };

  updateTAT = (expectedTat, distance) => {
    console.log("expectedTat ----", expectedTat);
    console.log("distance ----", distance);
    this.setState(() => {
      let result = { expectedTat };
      if (distance) {
        result = { ...result, distanceDisabled: true, distance };
      } else {
        result = { ...result, distanceDisabled: false, distance: null };
      }
      return result;
    });
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  updateCustomer = () => {
    const { editTripCustomer, trip_id } = this.props;
    const {
      selectedCustomer: customer_id,
      expectedTat: expected_tat
    } = this.state;
    if (expected_tat) {
      this.setState({ updatingCustomer: true });
      editTripCustomer({ trip_id, customer_id, expected_tat })
        .then(result => {
          const { status = false, message = "Failed" } = result;
          if (status) {
            this.setState({ isVisible: message });
          } else if (status === false) {
            Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .catch(err => {
          Snackbar.show({
            title: "Unable to Update Customer. Try again later.",
            duration: Snackbar.LENGTH_SHORT
          });
        })
        .finally(() => {
          this.setState({ updatingCustomer: false });
        });
    } else {
      Snackbar.show({
        title: "Please Fill Tat First",
        duration: Snackbar.LENGTH_LONG
      });
    }
  };

  selectCustomer = selectedCustomer => {
    this.setState({ selectedCustomer });
  };

  render() {
    const { trip, vehicleTypes, customers } = this.props;
    const {
      basicInfo: {
        customer_id,
        source_city_id,
        destination_city_id,
        via_cities = [],
        vehicle_type_id,
        expected_tat,
        route,
        distanceValue = "NA"
      } = {}
    } = trip || {};
    const { basicInfo: { name: customerName = "-" } = {} } =
      customers[customer_id] || {};
    const { basicInfo: { name: vehicleTypeName } = {} } =
      vehicleTypes[vehicle_type_id] || {};
    const { isVisible } = this.state;

    return (
      <Fragment>
        <Header
          name={"EDIT CUSTOMER"}
          navigation={this.props.navigation}
          goBack={true}
        />
        <KeyboardAvoidingView
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "center"
          }}
          behavior="padding"
          enabled
          // keyboardVerticalOffset={100}
        >
          <ScrollView
            contentContainerStyle={{ flexGrow: 1 }}
            keyboardShouldPersistTaps="handled"
          >
            <View style={CustomStyles.flex1}>
              <View style={{ flex: 1 }}>
                <View
                  style={{
                    paddingBottom: 8,
                    paddingTop: 10,
                    paddingLeft: 10,
                    paddingRight: 10
                  }}
                >
                  <View style={{ paddingBottom: 10 }}>
                    <Text style={styles.heading}>Current Details</Text>
                  </View>
                  <View style={{ paddingBottom: 6 }}>
                    <Text style={styles.subheading}>Current Customer</Text>
                    <Text style={styles.value}>{customerName}</Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      paddingTop: 6,
                      paddingBottom: 6
                    }}
                  >
                    <Text style={styles.subheading}>Current Route</Text>
                    <Text style={styles.value}>{route}</Text>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      paddingTop: 6,
                      paddingBottom: 6
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <Text style={styles.subheading}>Expected Tat</Text>
                      <Text style={styles.value}>{expected_tat}</Text>
                    </View>

                    <View style={{ flex: 1 }}>
                      <Text style={styles.subheading}>Vehicle Type</Text>
                      <Text style={styles.value}>{vehicleTypeName}</Text>
                    </View>
                  </View>
                </View>
                <View style={{ padding: 10 }}>
                  <Text style={styles.heading}>New Details</Text>
                </View>

                <CustomerInput
                  customers={this.props.customers}
                  customerIds={this.props.customerIds}
                  selectedCustomer={this.state.selectedCustomer}
                  selectCustomer={this.selectCustomer}
                  disabled={false}
                />

                <ExpectedTatInput
                  message={"Expected TAT"}
                  customer_id={this.state.selectedCustomer}
                  vehicle_type_id={vehicle_type_id}
                  updateText={this.updateTAT}
                  trip_id={this.props.trip_id}
                  source_city={source_city_id}
                  destination_city={destination_city_id}
                  via_cities={via_cities}
                  source_flag={AUTO_FILL_TAT.CREATE_BOOKING}
                  placeholder={"Click Icon to autofill tat"}
                />
              </View>
              <Footer
                name={"SAVE"}
                action={this.updateCustomer}
                loading={this.state.updatingCustomer}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
        <Modal
          isVisible={!!isVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          animationIn={"slideInUp"}
          style={{ margin: 0 }}
        >
          <TouchableOpacity style={styles.modalView} onPress={this.closeModal}>
            <Icon
              name="check"
              type="evilicon"
              color={colors.greenishBlue}
              size={80}
            />
            <View style={{ paddingTop: 4 }}>
              <Text style={{ color: colors.greenishBlue }}>{isVisible}</Text>
            </View>
          </TouchableOpacity>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalView: {
    width: "80%",
    aspectRatio: 4 / 2,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  heading: { fontSize: 16, fontWeight: "500", color: colors.darkblue },
  subheading: { fontSize: 12, color: colors.black65, paddingBottom: 4 },
  value: { fontSize: 14, color: colors.black85 }
});

export default EditCustomer;
