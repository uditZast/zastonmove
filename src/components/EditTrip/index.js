import React, { Component, Fragment } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView
} from "react-native";
import Header from "../common/header";
import CustomStyles from "../common/Styles";
import { VEHICLE_CATEGORY } from "../../../constants";
import Snackbar from "react-native-snackbar";
import VehicleInput from "../common/VehicleInput";
import VendorInput from "../common/VendorsInput";
import EngagedByInput from "../common/EngagedByInput";
import NumberInput from "../common/NumberInput";
import Footer from "../common/footer";
import { Icon } from "react-native-elements";
import Modal from "react-native-modal";
import colors from "../common/Colors";

class EditTrip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedVehicle: 0,
      selectedVendor: 0,
      driverPhoneNumber: null,
      selectedEngageReason: 0,
      editingTrip: false,
      isVisible: false
    };
  }

  updateTrip = () => {
    const { editTrip, trip_id } = this.props;
    const {
      selectedVehicle: vehicle_id,
      selectedVendor: vendor_id,
      driverPhoneNumber: phone,
      selectedEngageReason: engaged_by_id
    } = this.state;
    if (vehicle_id && vendor_id && phone) {
      this.setState({ editingTrip: true });
      const data = { vehicle_id, vendor_id, phone, trip_id };
      const requestData =
        engaged_by_id === 0 ? { ...data } : { ...data, engaged_by_id };

      console.log("requestData ----", requestData);
      editTrip(requestData)
        .then(result => {
          const { status = false, message = "Failed" } = result;
          if (status) {
            this.setState({ isVisible: message });
          } else if (status === false) {
            Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .finally(() => this.setState({ editingTrip: false }));
    } else {
      Snackbar.show({
        title: "Please select vehicle, vendor and phone",
        duration: Snackbar.LENGTH_SHORT
      });
    }
  };

  selectVehicle = vehicle => {
    const { vehicles } = this.props;
    const { basicInfo: { vehicle_category, vendor_id } = {} } =
      vehicles[vehicle] || {};
    if (VEHICLE_CATEGORY.CON === vehicle_category) {
      this.setState({ selectedVendor: vendor_id, vendorDisabled: true });
    } else {
      this.setState({ selectedVendor: 0, vendorDisabled: false });
    }
    this.setState({
      selectedVehicle: vehicle
    });
  };

  selectVendor = vendor => {
    this.setState({ selectedVendor: vendor });
  };

  updateDriverNumber = phoneNumber => {
    this.setState({ driverPhoneNumber: phoneNumber });
  };

  setEngageReason = selectedEngageReason => {
    this.setState({ selectedEngageReason });
  };

  closeModal = () => {
    this.setState({ isVisible: false }, this.goBack);
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    const { trip } = this.props;
    const { basicInfo: { code } = {} } = trip || {};
    const { isVisible } = this.state;

    return (
      <Fragment>
        <Header
          name={`EDIT VEHICLE (${code})`}
          navigation={this.props.navigation}
          goBack={true}
        />
        <ScrollView
          contentContainerStyle={{ flexGrow: 1 }}
          keyboardShouldPersistTaps="handled"
        >
          <View style={CustomStyles.flex1}>
            <View style={[CustomStyles.flex1, { paddingTop: 10 }]}>
              <VehicleInput
                vehicles={this.props.vehicles}
                vehicleTypes={this.props.vehicleTypes}
                selectedVehicle={this.state.selectedVehicle}
                selectVehicle={this.selectVehicle}
              />
              <VendorInput
                vendors={this.props.vendors}
                selectedVendor={this.state.selectedVendor}
                selectVendor={this.selectVendor}
                disabled={
                  !this.state.selectedVehicle || this.state.vendorDisabled
                }
              />
              <NumberInput
                numericProp={true}
                message={"Driver Phone"}
                updateText={this.updateDriverNumber}
                placeholder={"Enter Driver Phone Number"}
              />
              <EngagedByInput
                engagedBy={this.props.engagedBy}
                selectedEngageReason={this.state.selectedEngageReason}
                setEngageReason={this.setEngageReason}
              />
            </View>
          </View>
        </ScrollView>
        <Footer
          name={"SAVE"}
          action={this.updateTrip}
          loading={this.state.editingTrip}
        />
        <Modal
          isVisible={!!isVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          animationIn={"slideInUp"}
          style={{ margin: 0 }}
        >
          <TouchableOpacity style={styles.modalView} onPress={this.closeModal}>
            <Icon
              name="check"
              type="evilicon"
              color={colors.greenishBlue}
              size={80}
            />
            <View style={{ paddingTop: 4 }}>
              <Text style={{ color: colors.greenishBlue }}>{isVisible}</Text>
            </View>
          </TouchableOpacity>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalView: {
    width: "80%",
    aspectRatio: 4 / 2,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center"
  }
});

export default EditTrip;
