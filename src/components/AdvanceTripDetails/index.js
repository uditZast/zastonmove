import React, { Component, Fragment } from "react";
import { StyleSheet, Text, View } from "react-native";
import colors from "../../components/common/Colors";
import moment from "moment";

class AdvanceTripDetails extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      vendors,
      vehicles,
      showCustomer = false,
      customers,
      showVendor = false
    } = this.props;

    const { requestTripAdvance } = this.props;

    const { trips } = requestTripAdvance;

    const {
      basicInfo: { vendor_id, vehicle_id, route, trip_time, code, customer_id },
      status
    } = Object.values(trips)[0] || {};

    const {
      basicInfo: { name, code: vendorCode }
    } = vendors[vendor_id] || {};

    const {
      basicInfo: { vehicle_number }
    } = vehicles[vehicle_id] || {};

    const {
      basicInfo: { name: customerName, code: customerCode }
    } = customers[customer_id] || {};

    return (
      <Fragment>
        <View style={styles.cardContainerStyle}>
          <Text
            style={[
              {
                fontSize: 16,
                color: colors.black65,
                fontFamily: "CircularStd-Book",
                padding: 5,
                fontWeight: "300",
                marginBottom: 5,
                marginLeft: 5
              }
            ]}
          >
            Trip Details
          </Text>

          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              paddingLeft: 5,
              paddingRight: 5
            }}
          >
            <Text
              style={[
                styles.labelTextStyle,
                { fontWeight: "bold", fontSize: 16, color: colors.black85 }
              ]}
            >
              {code}
            </Text>
            <Text
              style={[
                styles.labelTextStyle,
                {
                  fontWeight: "bold",
                  fontSize: 16,
                  color: colors.black85
                }
              ]}
            >
              {route}
            </Text>
          </View>
          <View
            style={{ flexDirection: "row", paddingLeft: 5, paddingRight: 5 }}
          >
            <Text
              style={[
                styles.labelTextStyle,
                { color: colors.black40, fontSize: 14, paddingTop: 5 }
              ]}
            >
              {vehicle_number}
            </Text>
            {showCustomer && (
              <Text
                style={[
                  styles.labelTextStyle,
                  { color: colors.black40, fontSize: 14, paddingTop: 5 }
                ]}
              >
                {customerCode}
              </Text>
            )}
            {showVendor && (
              <Text
                style={[
                  styles.labelTextStyle,
                  { color: colors.black40, fontSize: 14, paddingTop: 5 }
                ]}
              >
                {name}
              </Text>
            )}
          </View>

          <View
            style={{
              flexDirection: "row",
              paddingBottom: 5,
              paddingLeft: 5,
              paddingRight: 5
            }}
          >
            <Text
              style={[
                styles.labelTextStyle,
                { fontSize: 14, color: colors.black40, paddingTop: 5 }
              ]}
            >
              {new moment(trip_time).format("LLL")}

              <Text
                style={[
                  styles.labelTextStyle,
                  {
                    alignSelf: "flex-start",
                    fontSize: 14,
                    color: colors.black40,
                    paddingTop: 5
                  }
                ]}
              >
                {" ("}
                {status === "CR"
                  ? "Created"
                  : status === "HL"
                  ? "Halted"
                  : status === "IN"
                  ? "In-Transit"
                  : status === "CL"
                  ? "Closed"
                  : status === "AI"
                  ? "Touching"
                  : status === "AR"
                  ? "Arrived at destination"
                  : status === "TM"
                  ? "Terminated"
                  : "Status"}
                {")"}
              </Text>
            </Text>
          </View>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  labelTextStyle: {
    fontFamily: "CircularStd-Book",
    paddingRight: 5,
    paddingLeft: 5
  },
  cardContainerStyle: {
    borderColor: "#ddd",
    borderWidth: 1,
    borderRadius: 2,
    borderBottomWidth: 1,
    marginBottom: 5,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: colors.white,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3
  }
});

export default AdvanceTripDetails;
