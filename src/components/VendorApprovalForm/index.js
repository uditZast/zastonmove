import React, { Component, Fragment } from "react";
import Header from "../common/header";
import Snackbar from "react-native-snackbar";
import NumberInput from "../common/NumberInput";
import Footer from "../common/footer";
import { USER_RIGHTS } from "../../../constants";
import { BOOKING_TYPE, NAVIGATE } from "../../../constants";
import BookingSuccessModal from "../common/BookingSuccessModal";
import { ScrollView } from "react-navigation";
import ImageViewModal from "../../modal/ImageViewModal";
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../common/Colors";
import CustomStyles from "../common/Styles";
import CityInput from "../common/CityInput";
import { hasRights } from "../common/checkRights";
import * as Orientation from "react-native-orientation";

class VendorApprovalForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nickName: null,
      selectedBaseLocation: 0,
      approvalLoader: false,
      showModal: false,
      name: null,
      code: null,
      pocName: null,
      email: null,
      phone1: null,
      phone2: null,
      address: null,
      imageViewVisible: false,
      documentUrl: null,
      nicknameVisible: false,
      panNumber: null
    };
  }

  componentDidMount() {
    // Orientation.lockToPortrait();
    this.getVendorData();
  }

  getVendorData = () => {
    const { vendors, vendor_id } = this.props;

    const {
      basicInfo: {
        nick_name,
        name,
        code,
        poc,
        phone_1,
        phone_2,
        email,
        address,
        base_location,
        document_url,
        pan
      } = {}
    } = vendors[vendor_id] || {};

    const { loggedInUser } = this.props;

    if (nick_name) {
      this.setState({ nickName: nick_name, nicknameVisible: true });
    }

    if (hasRights(USER_RIGHTS.APPROVE_VENDOR_OPS, loggedInUser)) {
      this.setState({ nicknameVisible: true });
    }

    if (name) {
      this.setState({ name });
    }

    if (code) {
      this.setState({ code });
    }

    if (poc) {
      this.setState({ pocName: poc });
    }

    if (address) {
      this.setState({ address });
    }

    if (phone_1) {
      this.setState({ phone1: phone_1 });
    }

    if (phone_2) {
      this.setState({ phone2: phone_2 });
    }

    if (base_location) {
      this.setState({ selectedBaseLocation: base_location });
    }

    if (document_url) {
      this.setState({ documentUrl: document_url });
    }

    if (email) {
      this.setState({ email });
    }

    if (pan) {
      this.setState({ panNumber: pan });
    }
  };

  updateNickName = nickName => {
    this.setState({ nickName: nickName });
  };

  updateName = name => {
    this.setState({ name: name });
  };

  updateCode = code => {
    this.setState({ code: code });
  };

  updatePocName = pocName => {
    this.setState({ pocName: pocName });
  };

  updateEmail = email => {
    this.setState({ email: email });
  };

  updatePhone1 = phone1 => {
    this.setState({ phone1: phone1 });
  };

  updatePhone2 = phone2 => {
    this.setState({ phone2: phone2 });
  };

  updateAddress = address => {
    console.log("address --------------------------", address);
    this.setState({ address });
  };

  selectBaseLocation = city => {
    this.setState({ selectedBaseLocation: city });
  };

  updatePanCard = panNumber => {
    this.setState({ panNumber });
  };

  getErrors = () => {
    let message = null;
    let status = false;
    const {
      nickName,
      name,
      pocName,
      phone1,
      address,
      selectedBaseLocation,
      nicknameVisible,
      email,
      panNumber
    } = this.state;

    if (nicknameVisible && !nickName) {
      message = "Please Enter Nick Name";
      status = true;
    } else if (!name) {
      message = "Please Enter Name";
      status = true;
    } else if (!pocName) {
      message = "Please Enter POC Name";
      status = true;
    } else if (!phone1) {
      message = "Please Enter Phone Number";
      status = true;
    } else if (!email) {
      message = "Please Enter Email";
      status = true;
    } else if (!selectedBaseLocation) {
      message = "Please Select Base Location";
      status = true;
    } else if (!address) {
      message = "Please Add Address";
      status = true;
    } else if (!panNumber) {
      message = "Please PAN Number";
      status = true;
    }
    if (message) {
      Snackbar.show({
        title: message,
        duration: Snackbar.LENGTH_LONG
      });
    }
    return status;
  };

  vendorApproval = () => {
    console.log("---- APPROVE VENDOR ----");
    const errors = this.getErrors();
    const { vendor_id } = this.props;

    if (errors === false) {
      this.setState({ approvalLoader: true });
      const {
        nickName: nick_name,
        name,
        pocName: poc,
        phone1: phone_1,
        phone2: phone_2,
        address,
        email,
        selectedBaseLocation: base_location,
        panNumber: pan
      } = this.state;

      const request_data = {
        vendor_id,
        nick_name,
        name,
        poc,
        phone_1,
        phone_2,
        address,
        email,
        base_location,
        pan
      };
      const { editVendorDetails } = this.props;
      editVendorDetails(request_data)
        .then(result => {
          const { status, message } = result;

          if (status) {
            this.setState({ showModal: message });
            Snackbar.show({
              title: "" + message,
              duration: Snackbar.LENGTH_LONG
            });
            this.goBack();
          } else {
            Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .finally(() => this.setState({ approvalLoader: false }));
    }
  };

  setEngageReason = selectedEngageReason => {
    this.setState({ selectedEngageReason });
  };

  openImageViewModal = () => {
    return (
      <ImageViewModal
        isVisible={!this.state.imageViewVisible}
        closeModal={this.closeImageViewModal}
      />
    );
  };

  closeImageViewModal = () => {
    console.log("---- closeModal Called ----");
    this.setState({ imageViewVisible: false, imageViewModal: false });
  };

  viewImageViewModal = () => {
    if (this.state.documentUrl) {
      this.setState({ imageViewModal: true, imageViewVisible: true });
    }
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  editVendorDetails = () => {
    const errors = this.getErrors();
    const { vendor_id } = this.props;

    if (errors === false) {
      this.setState({ approvalLoader: true });

      const {
        nickName: nick_name,
        selectedBaseLocation: base_location,
        name,
        pocName: poc_name,
        email,
        phone1: phone_1,
        phone2: phone_2,
        address,
        panNumber: pan
      } = this.state;

      const request_data = {
        vendor_id,
        nick_name,
        base_location,
        name,
        poc_name,
        email,
        phone_1,
        phone_2,
        address,
        pan
      };

      const { editVendorDetails } = this.props;
      editVendorDetails(request_data)
        .then(response => {
          console.log("response ------EDIT--------------------", response);
          const { status, message } = response;
          if (status) {
            Snackbar.show({
              title: "" + message,
              duration: Snackbar.LENGTH_LONG
            });
            this.goBack();
          } else {
            Snackbar.show({
              title: "" + message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .catch(error => {
          console.log("error ----", error);
        })
        .finally(() => this.setState({ approvalLoader: false }));
    }
  };

  render() {
    const {
      approvalLoader,
      nickName,
      name,
      code,
      pocName,
      email,
      phone1,
      phone2,
      address,
      panNumber
    } = this.state;

    const { loggedInUser } = this.props;

    return (
      <Fragment>
        <Header
          name={
            hasRights(USER_RIGHTS.APPROVE_VENDOR_OPS, loggedInUser)
              ? "APPROVE"
              : "EDIT"
          }
          navigation={this.props.navigation}
          goBack={true}
        />

        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          {(this.state.nickName ||
            hasRights(USER_RIGHTS.APPROVE_VENDOR_OPS, loggedInUser)) && (
            <NumberInput
              numericProp={false}
              message={"Nick Name"}
              updateText={this.updateNickName}
              placeholder={"Nick Name"}
              initialValue={nickName}
              maxLengthProp={true}
            />
          )}

          <NumberInput
            numericProp={false}
            message={"Name"}
            updateText={this.updateName}
            placeholder={"Name"}
            initialValue={name}
          />

          <NumberInput
            numericProp={false}
            message={"Code"}
            updateText={this.updateCode}
            placeholder={"Code"}
            initialValue={code}
            disableDistance={true}
          />

          <NumberInput
            numericProp={false}
            message={"POC Name"}
            updateText={this.updatePocName}
            placeholder={"POC Name"}
            initialValue={pocName}
          />

          <NumberInput
            numericProp={false}
            message={"Email"}
            updateText={this.updateEmail}
            placeholder={"Email"}
            initialValue={email}
          />

          <NumberInput
            numericProp={true}
            message={"Phone 1"}
            updateText={this.updatePhone1}
            placeholder={"Phone 1"}
            initialValue={phone1}
          />

          <NumberInput
            numericProp={true}
            message={"Phone 2"}
            updateText={this.updatePhone2}
            placeholder={"Phone 2"}
            initialValue={phone2}
          />

          <View style={styles.border}>
            <View style={styles.labelText}>
              <Text
                style={{
                  color: colors.blue,
                  backgroundColor: "#00000003"
                }}
              >
                Address
              </Text>
            </View>
            <TextInput
              placeholder={"Enter Address"}
              multiline={true}
              style={{
                textAlignVertical: "top",
                paddingLeft: 10,
                paddingTop: 5,
                paddingBottom: 10
              }}
              onChangeText={this.updateAddress}
              value={this.state.address}
              placeholderTextColor={colors.black25}
            />
          </View>

          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedCity={this.state.selectedBaseLocation}
            selectCity={this.selectBaseLocation}
            message={"Base Location City"}
          />

          <NumberInput
            numericProp={false}
            message={"PanCard"}
            updateText={this.updatePanCard}
            placeholder={"Enter PanCard number"}
            initialValue={this.state.panNumber}
          />

          <View style={CustomStyles.border}>
            <View style={styles.labelText}>
              <Text style={{ color: colors.blue }}>UPLOAD ID</Text>
            </View>
            <TouchableOpacity onPress={this.viewImageViewModal}>
              <Text style={styles.textInputStyle}>View Uploaded File</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>

        {this.state.imageViewModal && (
          <ImageViewModal
            imageViewVisible={this.state.imageViewVisible}
            closeModal={this.closeImageViewModal}
            imageVisible={true}
            url={this.state.documentUrl}
          />
        )}

        <Footer
          name={
            hasRights(USER_RIGHTS.APPROVE_VENDOR_OPS, loggedInUser)
              ? "APPROVE"
              : "EDIT"
          }
          action={this.editVendorDetails}
          loading={approvalLoader}
        />

        <BookingSuccessModal
          isVisible={!!this.state.showModal}
          message={this.state.showModal}
          closeModal={this.goToBookings}
        />
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  labelText: {
    marginTop: -10,
    color: colors.blue,
    marginLeft: 10,
    backgroundColor: colors.white,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  textInputStyle: {
    flex: 1,
    alignItems: "flex-start",
    paddingLeft: 10,
    justifyContent: "center",
    ...Platform.select({
      ios: { maxHeight: 30, paddingBottom: 10 },
      android: {
        maxHeight: 30,
        paddingTop: 0,
        paddingBottom: 0,
        textAlignVertical: "center"
      }
    })
  },
  border: {
    borderWidth: 1,
    borderRadius: 3,
    borderColor: colors.blue,
    margin: 10
  }
});

export default VendorApprovalForm;
