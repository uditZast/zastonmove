import React, { Component } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import { BarIndicator } from "react-native-indicators";
import colors from "../colors/Colors";
import { NAVIGATE } from "../../constants";

class AuthLoading extends Component {
  constructor(props) {
    super(props);
    this.bootstrapAsync();
  }

  bootstrapAsync = async () => {
    const data = await AsyncStorage.getItem("token");
    if (data !== null) {
      this.props.navigation.navigate(NAVIGATE.HOME);
    } else {
      this.props.navigation.navigate("Auth");
    }
  };

  render() {
    return <BarIndicator color={colors.blueColor} size={20} />;
  }
}

export default AuthLoading;
