import React, { Component, Fragment } from "react";
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity
} from "react-native";
import moment from "moment";
import CommonStyles from "../common/Styles";
import colors from "../common/Colors";
import Header from "../common/header";
import { Icon } from "react-native-elements";
import MapModal from "../common/MapModal";
import { VEHICLE_RUNNING_STATUS } from "../../../constants";

class TrackingSearchResultsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: null,
      lat: null,
      long: null,
      visible: false,
      loading: false
    };
    this.page = 1;
  }

  componentDidMount() {
    this.onRefresh();
  }

  fetchTrackingDetails = () => {
    const {
      fetchVehicleTrackingDetails,
      vehicle_id,
      start_date,
      end_date
    } = this.props;
    const startDate = new moment(start_date).format("x");
    const endDate = new moment(end_date).format("x");
    const data = {
      vehicle_id,
      start_date: startDate,
      end_date: endDate,
      page: this.page
    };
    fetchVehicleTrackingDetails(data).finally(() => {
      this.setState({ loading: false });
    });
  };

  renderSeparator = () => {
    return <View style={styles.separator} />;
  };

  renderFooter = () => {
    const { pageData: { isFetching } = {} } = this.props;
    if (!isFetching) return null;
    return <ActivityIndicator style={{ color: colors.blue }} size={"large"} />;
  };

  handleLoadMore = () => {
    const { pageData: { load_more, isFetching } = {} } = this.props;
    if (!isFetching && load_more) {
      this.page = this.page + 1;
      this.fetchTrackingDetails();
    }
  };

  onRefresh = () => {
    this.page = 1;
    this.setState({ loading: true });
    this.fetchTrackingDetails();
  };

  setModalDetails = (address, lat, long) => () => {
    this.setState({ address, lat, long, visible: true });
  };

  getVehicleRunningStatus = status => {
    switch (status) {
      case VEHICLE_RUNNING_STATUS.RUNNING:
        return <Text style={{ color: colors.green }}>On</Text>;
      case VEHICLE_RUNNING_STATUS.STOPPED:
        return <Text style={{ color: colors.red }}>Off</Text>;
      case VEHICLE_RUNNING_STATUS.NOT_IN_CONTACT:
        return <Text style={{ color: colors.yellow }}>Not in contact</Text>;
      default:
        return <Text style={{ color: colors.yellow }}>Status not found</Text>;
    }
  };

  renderTrackingDetail = ({ index, item: trackingTime }) => {
    const { tracking } = this.props;
    const {
      basicInfo: {
        lat,
        long,
        address,
        today_run,
        recorded_at,
        speed,
        status,
        gps_provider
      } = {}
    } = tracking[trackingTime] || {};

    return (
      <Fragment key={trackingTime}>
        <View
          style={{
            padding: 10,
            margin: 8,
            marginBottom: 0,
            borderWidth: 1,
            borderRadius: 6,
            borderColor: colors.black25
          }}
        >
          <TouchableOpacity
            style={{
              flexDirection: "row",
              paddingBottom: 4,
              alignItems: "center"
            }}
            onPress={this.setModalDetails(address, lat, long)}
          >
            <Icon
              size={25}
              name="crosshairs-gps"
              type="material-community"
              containerStyle={{ paddingRight: 8 }}
              color={colors.blue}
            />
            <View style={{ flexWrap: "wrap", flex: 1 }}>
              <Text style={{ color: colors.blue, fontSize: 14 }}>
                {address}
              </Text>
            </View>
          </TouchableOpacity>
          <View style={{ flexDirection: "row", paddingBottom: 4 }}>
            <View style={{ flex: 1 }}>
              <Text style={{ color: colors.black65, fontSize: 14 }}>
                Provider:{" "}
                <Text style={{ color: colors.black85 }}>{gps_provider}</Text>
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text style={{ color: colors.black65, fontSize: 14 }}>
                Speed:{" "}
                <Text style={{ color: colors.black85 }}>{speed} Kmph</Text>
              </Text>
            </View>
          </View>

          <View style={{ flexDirection: "row", paddingBottom: 4 }}>
            <View style={{ flex: 1 }}>
              <Text style={{ color: colors.black65, fontSize: 14 }}>
                GPS Status: {this.getVehicleRunningStatus(status)}
              </Text>
            </View>
          </View>

          <Text style={{ color: colors.black65, fontSize: 14 }}>
            Recorded at:{" "}
            <Text style={{ color: colors.black85 }}>{recorded_at}</Text>
          </Text>
        </View>
      </Fragment>
    );
  };

  renderTrackingDetails = () => {
    const { pageData: { tracking_ids = [] } = {} } = this.props;
    const { loading } = this.state;

    return (
      <FlatList
        data={[...tracking_ids]}
        refreshing={loading}
        onRefresh={this.onRefresh}
        renderItem={this.renderTrackingDetail}
        keyExtractor={(item, index) => index.toString()}
        // ItemSeparatorComponent={this.renderSeparator}
        ListFooterComponent={tracking_ids.length > 0 ? this.renderFooter : null}
        onEndReachedThreshold={0.4}
        onEndReached={this.handleLoadMore}
      />
    );
  };

  renderSearchedFields = () => {
    const { vehicles, vehicle_id, start_date = 0, end_date = 0 } = this.props;
    const startDate = start_date ? new moment(start_date) : null;
    const endDate = end_date ? new moment(end_date) : null;

    const { basicInfo: { vehicle_number } = {} } = vehicles[vehicle_id] || {};
    return (
      <View style={styles.searchContainer}>
        <Fragment>
          {!!vehicle_number && (
            <Text style={styles.subHeadingStyle}>{vehicle_number} </Text>
          )}
          {startDate && (
            <Text style={styles.searchHeadingStyle}>
              Start Date:{" "}
              <Text style={styles.subHeadingStyle}>
                {startDate.format("DD-MM-YYYY")}{" "}
              </Text>
            </Text>
          )}
          {endDate && (
            <Text style={styles.searchHeadingStyle}>
              End Date:{" "}
              <Text style={styles.subHeadingStyle}>
                {endDate.format("DD-MM-YYYY")}
              </Text>
            </Text>
          )}
        </Fragment>
      </View>
    );
  };

  closeModal = () => {
    this.setState({ visible: false });
  };

  render() {
    const { address, lat, long, visible } = this.state;
    return (
      <Fragment>
        <Header
          name={"Vehicle Tracking Details"}
          navigation={this.props.navigation}
          goBack={true}
        />
        <Fragment>
          {this.renderSearchedFields()}
          {this.renderSeparator()}
          <View style={CommonStyles.flex1}>{this.renderTrackingDetails()}</View>
          {lat && long && (
            <MapModal
              location={address}
              lat={lat}
              long={long}
              isVisible={visible}
              closeModal={this.closeModal}
            />
          )}
        </Fragment>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  searchContainer: {
    margin: 10,
    padding: 10,
    borderWidth: 1,
    backgroundColor: colors.black02,
    borderColor: colors.black15,
    borderRadius: 6,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center"
  },
  subHeadingStyle: {
    fontSize: 16,
    fontWeight: "bold",
    color: colors.black65
  },
  searchHeadingStyle: {
    fontSize: 16,
    fontWeight: "bold",
    color: colors.blue
  },
  separator: {
    height: 2,
    width: "100%",
    backgroundColor: "#CED0CE"
  },
  flex1: {
    flex: 1
  }
});

export default TrackingSearchResultsPage;
