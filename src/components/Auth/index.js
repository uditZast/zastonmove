import React, { Fragment } from "react";
import { STACK } from "../../../constants";
import { BarIndicator } from "react-native-indicators";
import colors from "../../colors/Colors";

class AuthScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  _bootstrapAsync = async () => {
    const {
      auth: { accessToken }
    } = this.props;
    if (accessToken) {
      this.props.navigation.navigate(STACK.APP);
    } else {
      this.props.navigation.navigate(STACK.AUTH);
    }
  };

  render() {
    return (
      <Fragment>
        <BarIndicator color={colors.blueColor} size={20} />
      </Fragment>
    );
  }
}

export default AuthScreen;
