import React, { Component, Fragment } from "react";
import { FlatList, View } from "react-native";
import Header from "../common/header";
import customStyles from "../common/Styles";
import { SearchBar } from "react-native-elements";
import BookingCard from "../common/BookingCard";

class MyBookings extends Component {
  constructor(props) {
    super(props);
    this.state = { input: null };
  }

  componentDidMount() {
    this.fetchBookings();
  }

  fetchBookings = () => {
    const { fetchMyBookings } = this.props;
    fetchMyBookings();
  };

  setSearch = text => {
    this.setState({ input: text });
  };

  clearInput = () => {
    this.setState({ input: null });
  };

  selectIndex = selectedIndex => {
    this.setState({ selectedIndex });
  };

  renderBooking = ({ index, item: booking_id }) => {
    const {
      bookings,
      customers,
      vendors,
      vehicles,
      navigation,
      vehicleTypes,
      engagedBy,
      trips
    } = this.props;
    const booking = bookings[booking_id] || {};
    return (
      <View key={index} style={`${index}` === "0" ? { paddingTop: 10 } : {}}>
        <BookingCard
          booking={booking}
          customers={customers}
          vendors={vendors}
          vehicles={vehicles}
          vehicleTypes={vehicleTypes}
          engagedBy={engagedBy}
          trips={trips}
          navigation={navigation}
          statusVisibility={true}
          bookings={bookings}
          index={index}
          selectIndex={this.selectIndex}
          selectedIndex={this.state.selectedIndex}
          screen={"my_bookings"}
          {...this.props}
        />
      </View>
    );
  };

  renderMyBookingsList = () => {
    const {
      bookings,
      customers,
      vehicles,
      vendors,
      pageData: { booking_ids = [], isFetching = false } = {}
    } = this.props;
    const { input } = this.state;
    let list = booking_ids;
    if (input && input.toUpperCase()) {
      const u_input = input.toUpperCase();
      list = list.filter(booking_id => {
        const {
          basicInfo: {
            code,
            type,
            route,
            vehicle_id,
            customer_id,
            vendor_id
          } = {}
        } = bookings[booking_id] || {};

        let return_val =
          code.toUpperCase().includes(u_input) ||
          type.toUpperCase().includes(u_input) ||
          route.toUpperCase().includes(u_input);

        if (vendor_id) {
          const { basicInfo: { code: vendorCode } = {} } =
            vendors[vendor_id] || {};
          return_val = return_val || vendorCode.toUpperCase().includes(u_input);
        }

        if (vehicle_id) {
          const { basicInfo: { vehicle_number } = {} } = vehicles[vehicle_id];
          return_val =
            return_val || vehicle_number.toUpperCase().includes(u_input);
        }

        if (customer_id) {
          const { basicInfo: { name, code } = {} } = customers[customer_id];
          return_val =
            return_val ||
            name.toUpperCase().includes(u_input) ||
            code.toUpperCase().includes(u_input);
        }

        return return_val;
      });
    }
    return (
      <FlatList
        data={list}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderBooking}
        onRefresh={this.fetchBookings}
        refreshing={isFetching}
        keyboardShouldPersistTaps={"handled"}
        showsVerticalScrollIndicator={false}
        extraData={this.state.selectedIndex}
      />
    );
  };

  render() {
    return (
      <Fragment>
        <Header name={"MY BOOKINGS"} navigation={this.props.navigation} />
        <View style={customStyles.flex1}>
          <SearchBar
            placeholder="Search Booking Id/Customer/Vehicle Number/Route/Vehicle Type"
            onChangeText={this.setSearch}
            lightTheme
            containerStyle={customStyles.searchBarContainer}
            value={this.state.input}
            onCancel={this.clearInput}
            onClear={this.clearInput}
          />
          <View style={{ flex: 1 }}>{this.renderMyBookingsList()}</View>
        </View>
      </Fragment>
    );
  }
}

export default MyBookings;
