import { applyMiddleware, compose, createStore } from "redux";
import thunk from "redux-thunk";
import rootReducer from "../../modules/rootReducer";
import { composeWithDevTools } from "redux-devtools-extension";

const enhancer = compose(composeWithDevTools(applyMiddleware(thunk)));

export default createStore(rootReducer, enhancer);
