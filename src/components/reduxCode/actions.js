export const FETCH_DATA_PENDING = 'FETCH_PRODUCTS_PENDING';
export const FETCH_DATA_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_DATA_ERROR = 'FETCH_PRODUCTS_ERROR';
export const HANDLE_401_ERROR = '401_UNAUTHORIZATION';


export function fetchDataPending() {
    console.log('fetch Products Pending');
    return {
        type: FETCH_DATA_PENDING
    }
}

export function fetchDataSuccess(data) {
    console.log('fetch Products Success' + JSON.stringify(data));
    return {
        type: FETCH_DATA_SUCCESS,
        data: data
    }
}

export function fetchDataError(error) {
    console.log('fetch Products Pending');

    return {
        type: FETCH_DATA_ERROR,
        error: error
    }
}

export function handle401error(error) {
    console.log('handle 401 status code in action ');
    return {
        type: HANDLE_401_ERROR,
        error: error
    }
}