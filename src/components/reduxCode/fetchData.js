import React from 'react';
import {fetchDataPending, fetchDataSuccess} from './actions';
import {CREATE_BOOKING_DETAILS} from "../../constants/Constants";
import AsyncStorage from "@react-native-community/async-storage";


function fetchData() {
    return dispatch => {
        dispatch(fetchDataPending());

        console.log('came here fetch data :::::::::');

        AsyncStorage.getItem('token').then(
            (value) => {
                fetch(CREATE_BOOKING_DETAILS, {
                    headers: {
                        'Authorization': 'Token ' + value
                    }
                })
                    .then(res => res.json())
                    .then(res => {
                        console.log('Status Code is : ' + JSON.stringify(res));
                        if (res.error) {
                            // console.log('Error fetch' + res.error + 'status' + res.status);
                            throw(res.error);
                        }
                        dispatch(fetchDataSuccess(res.data));

                        return res.data;
                    })
                    .catch(error => {
                        console.log('came here 4 fetchProducts' + error);

                        console.log('Error1 fetch' + error);
                        // dispatch(fetchProductsError(error));

                    });
            })


        // AsyncStorage.getItem('token').then(
        //     (value) => {
        //         console.log("token:::::::" + value);
        //         axios.get(CREATE_BOOKING_DETAILS, {
        //             headers: {
        //                 'Authorization': 'Token ' + value,
        //                 'Content-Type': 'application/json'
        //             }
        //         }).then((response) => {
        //             console.log('Status Code is : ' + JSON.stringify(response));
        //             if (response.error) {
        //                 console.log('Error fetch' + response.error + 'status' + response.status);
        //                 throw(response.error);
        //             }
        //             dispatch(fetchProductsSuccess(response.data));
        //
        //             // console.log('REDUX :) :) :) :) :) :) :) :) :) :) :) :) :) :) :) :) ' + JSON.stringify(response));
        //             return response.data;
        //         }).catch((error) => {
        //             console.log("catch" + JSON.stringify(error.response));
        //             dispatch(fetchProductsError(error));
        //         });
        //     })


    }
}

export default fetchData;


