import {FETCH_DATA_ERROR, FETCH_DATA_PENDING, FETCH_DATA_SUCCESS} from './actions';

const initialState = {
    pending: false,
    data: [],
    error: null
};

export function dataReducer(state = initialState, action) {
    console.log('this props reducer : ' + JSON.stringify(state));
    switch (action.type) {
        case FETCH_DATA_PENDING:
            return {
                ...state,
                pending: true
            };
        case FETCH_DATA_SUCCESS:
            return {
                ...state,
                pending: false,
                data: action
            };
        case FETCH_DATA_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            };
        default:
            return state;
    }
}

export const getProducts = state => state.products;
export const getProductsPending = state => state.pending;
export const getProductsError = state => state.error;
