const intial_state = {};

export default (state = intial_state, action = {}) => {
    const {type, payload = {}} = action;
    switch (type) {
        default: {
            if (payload.customers) {
                return {...state, ...payload.customers};
            }
            return state;
        }
    }
};