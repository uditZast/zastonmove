import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import fetchProductsAction from './fetchData';
import {BarIndicator} from "react-native-indicators";
import colors from "../../colors/Colors";

class ProductView extends Component {
    constructor(props) {
        super(props);

        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        const {fetchProducts} = this.props;
        fetchProducts();

    }

    shouldComponentRender() {
        const {pending} = this.props;
        console.log('pending should component' + pending)
        if (pending === false) return false;
        // more tests
        return true;
    }

    render() {
        const {products, error, pending} = this.props;

        console.log("statttetee" + this.props.state)
        if (!this.shouldComponentRender()) return <BarIndicator color={colors.blueColor}/>

        console.log('products ::::' + JSON.stringify(this.props.products.products))

        return (
            <View>
                {console.log('redux data before' + products)}
                <Text>Products</Text>

            </View>

        )
    }
}


const mapStateToProps = state => ({
    state: state,
    error: state.error,
    products: state.productsReducer.products,
    pending: state.productsReducer.pending,
});


const mapDispatchToProps = dispatch => bindActionCreators({
    fetchProducts: fetchProductsAction
}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductView);
