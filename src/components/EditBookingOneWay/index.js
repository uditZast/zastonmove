import React, { Component, Fragment } from "react";
import { ScrollView, View } from "react-native";
import Header from "../common/header";
import moment from "moment";
import Snackbar from "react-native-snackbar";
import CustomerInput from "../common/CustomerInput";
import VehicleInput from "../common/VehicleInput";
import VehicleTypeInput from "../common/VehicleTypeInput";
import VendorInput from "../common/VendorsInput";
import DateTimeInput from "../common/DateTimeInput";
import NumberInput from "../common/NumberInput";
import CityInput from "../common/CityInput";
import ViaCityInput from "../common/ViaCityInput";
import Footer from "../common/footer";
import {
  AUTO_FILL_TAT,
  BOOKING_TYPE,
  NAVIGATE,
  VEHICLE_CATEGORY
} from "../../../constants";
import customStyles from "../common/Styles";
import EngagedByInput from "../../containers/EngagedByInput";
import DistanceInputNew from "../common/DistanceInputNew";

let count;

class EditBookingOneWay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCustomer: 0,
      selectedVehicle: 0,
      selectedVehicleType: 0,
      selectedVendor: 0,
      selectedDateTime: new moment(),
      driverPhoneNumber: null,
      selectedSourceCity: 0,
      selectedDestinationCity: 0,
      selectedViaCities: [],
      expectedTat: 0,
      vendorDisabled: true,
      vehicleDisabled: false,
      vehicleTypeDisabled: true,
      sourceCityDisable: false,
      destinationCityDisabled: false,
      bookingType: null,
      comment: null,
      selectedEngagedBy: null,
      expectedTatEnable: true,
      distance: 0,
      enableDistance: true,
      bookingStatus: null,
      selectedEngageReason: 0,
      dockHours: 0,
      apiHit: false,
      dateFlag: false,
      reference: null,
      creatingBooking: false,
      customerDisabled: false
    };
  }

  componentDidMount() {
    const { booking_id, action } = this.props;

    count = 0;

    if (booking_id) {
      this.getBookingData();
    } else {
    }
  }

  getBookingData = () => {
    const { fetchBookingData, booking_id } = this.props;

    fetchBookingData(booking_id)
      .then(response => {
        const { status = false } = response;

        if (status) {
          const { bookings, booking_id } = this.props;
          const {
            basicInfo: {
              vehicle_id,
              vehicle_type_id,
              expected_tat,
              source_city_id,
              destination_city_id,
              customer_id,
              trip_time,
              vendor_id,
              phone,
              type,
              engaged_by_id,
              enable_tat,
              distance,
              enable_distance,
              via_cities,
              reference
            } = {}
          } = bookings[booking_id];

          const { status = null } = bookings[booking_id];

          if (this.props.action === "accept") {
            this.setState({ customerDisabled: true });
          }

          if (
            (type === "SCH" || type === "SCHF" || type === "SCHR") &&
            status === "DK"
          ) {
            this.setState({ customerDisable: true });
          }

          if (status === "RQ" || status === "AC" || status === "AS") {
            this.setState({ customerDisable: true });
          }

          if (
            (type === "AHF" ||
              type === "AHR" ||
              type === "SCHF" ||
              type === "SCHR") &&
            (status === "DK" || status === "CL")
          ) {
            this.setState({
              sourceCityDisable: true,
              destinationCityDisable: true
            });
          }

          if (status !== "RQ") {
            this.setState({
              vendorDisabled: true,
              vehicleDisabled: true,
              vehicleTypeDisabled: true
            });
          }

          if (
            status !== "RQ" &&
            (type === "AHF" ||
              type === "AHR" ||
              type === "SCHF" ||
              type === "SCHR")
          ) {
            this.setState({
              sourceCityDisable: true,
              destinationCityDisable: true
            });
          }

          if (
            type === "SCHF" ||
            (type === "SCHR" && this.props.action === "accept")
          ) {
            this.setState({
              vehicleDisabled: true,
              vendorDisabled: true,
              vehicleTypeDisabled: false
            });
          }

          if (via_cities !== undefined) {
            this.setState({
              selectedViaCities: via_cities
            });
          } else {
            this.setState({
              selectedViaCities: []
            });
          }

          if (reference !== null) {
            this.setState({ reference: reference });
          }

          if (type === "SCH" || type === "SCHF" || type === "SCHR") {
            this.setState({ customerDisabled: true });
          }

          if (type === "SCH" && type === "RQ") {
            this.setState({
              sourceCityDisable: true,
              destinationCityDisable: false
            });
          }

          count++;

          this.setState({
            apiHit: true,
            selectedVehicle: vehicle_id,
            selectedCustomer: customer_id,
            selectedVendor: vendor_id,
            selectedSourceCity: source_city_id,
            selectedDestinationCity: destination_city_id,
            driverPhoneNumber: `${phone}`,
            selectedDateTime: new moment(trip_time),
            expectedTat: expected_tat,
            selectedVehicleType: vehicle_type_id,
            bookingType: type,
            selectedEngageReason: engaged_by_id,
            distance: distance,
            bookingStatus: status,
            enableDistance: enable_distance,
            dateFlag: true,
            expectedTatEnable: enable_tat
          });
        } else {
          Snackbar.show({
            title: "" + response.message,
            duration: Snackbar.LENGTH_SHORT
          });
        }
      })
      .catch(error => {
        console.log("error ----", error);
        Snackbar.show({
          title: "Could not fetch details",
          duration: Snackbar.LENGTH_SHORT
        });
      });
  };

  selectCustomer = selectedCustomer => {
    this.setState({ selectedCustomer });
  };

  selectVehicle = vehicle => {
    count++;
    const { vehicles } = this.props;
    const { basicInfo: { vehicle_type_id, vehicle_category, vendor_id } = {} } =
      vehicles[vehicle] || {};

    if (VEHICLE_CATEGORY.CON === vehicle_category) {
      this.setState({ selectedVendor: vendor_id, vendorDisabled: true });
    } else {
      this.setState({ selectedVendor: 0, vendorDisabled: false });
    }
    this.setState({
      selectedVehicle: vehicle,
      selectedVehicleType: vehicle_type_id
    });
  };

  selectVehicleType = vehicleType => {
    count++;
    this.setState({
      selectedVehicleType: vehicleType,
      selectedVehicle: 0,
      selectedVendor: 0
    });
  };

  selectVendor = vendor => {
    this.setState({ selectedVendor: vendor });
  };

  setDate = dateTime => {
    this.setState({ selectedDateTime: dateTime, dateFlag: true });
  };

  updateDriverNumber = phoneNumber => {
    this.setState({ driverPhoneNumber: phoneNumber });
  };

  updateTAT = (expectedTat, distance) => {
    this.setState({ expectedTat: expectedTat, distance: distance });
  };

  updateDistance = distance => {
    this.setState({ distance });
  };

  updateComment = comment => {
    this.setState({ comment });
  };

  updateDockHours = dockHours => {
    this.setState({ dockHours });
  };

  selectSourceCity = city => {
    count++;
    const { selectedDestinationCity, selectedViaCities = [] } = this.state;
    const addedInVia = selectedViaCities.indexOf(city) === -1;
    if (`${city}` !== `${selectedDestinationCity}` && addedInVia) {
      this.setState({ selectedSourceCity: city, apiHit: false });
    } else {
      // this.setState({ selectedSourceCity: 0 });
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_SHORT
      // });
    }
  };

  selectDestinationCity = city => {
    count++;
    const { selectedSourceCity, selectedViaCities = [] } = this.state;
    const addedInVia = selectedViaCities.indexOf(city) === -1;
    if (`${selectedSourceCity}` !== `${city}` && addedInVia) {
      this.setState({ selectedDestinationCity: city });
    } else {
      // this.setState({ selectedDestinationCity: 0 });
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_SHORT
      // });
    }
  };

  addSelectedViaCity = cityId => {
    count++;
    const {
      selectedViaCities = [],
      selectedSourceCity,
      selectedDestinationCity
    } = this.state;
    if (
      selectedViaCities.indexOf(cityId) === -1 &&
      selectedSourceCity !== cityId &&
      selectedDestinationCity !== cityId
    ) {
      this.setState(prevState => {
        const { selectedViaCities } = prevState;
        const newViaCities = [...selectedViaCities];
        newViaCities.push(cityId);
        return { selectedViaCities: newViaCities };
      });

      // this.setState(prevState => {
      //   const { selectedViaCities } = prevState;
      //   return selectedViaCities.push(cityId);
      // });
    } else {
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_LONG
      // });
    }
  };

  // removeSelectedViaCity = index => {
  //   count++;
  //   this.setState(prevState => {
  //     const {selectedViaCities = []} = prevState;
  //     selectedViaCities.splice(index, 1);
  //     return selectedViaCities;
  //   });
  // };

  removeSelectedViaCity = index => {
    count++;
    this.setState(prevState => {
      const { selectedViaCities } = prevState;
      const newViaCities = [...selectedViaCities];
      newViaCities.splice(index, 1);
      return { selectedViaCities: newViaCities };
    });
  };

  getErrors = () => {
    let message = null;
    let status = false;
    const {
      selectedCustomer,
      selectedVehicle,
      selectedVehicleType,
      selectedVendor,
      selectedDateTime,
      selectedSourceCity,
      selectedDestinationCity,
      expectedTat,
      dockHours,
      distance,
      dateFlag
    } = this.state;
    if (!selectedCustomer) {
      message = "Please Select Customer";
      status = true;
    } else if (!selectedVehicleType || selectedVehicleType === 0) {
      message = "Please Select Vehicle Type";
      status = true;
    } else if (selectedVehicle && (!selectedVendor || selectedVendor === 0)) {
      message = "Please Select Vendor";
      status = true;
    } else if (dateFlag === false) {
      message = "Please Select Date and Time of trip";
      status = true;
    } else if (!selectedSourceCity) {
      message = "Please Select Source City";
      status = true;
    } else if (!selectedDestinationCity) {
      message = "Please Select Destination City";
      status = true;
    } else if (!expectedTat) {
      message = "Please enter expectedTat";
      status = true;
    } else if (this.props.action === "accept" && !dockHours) {
      message = "Please enter Dock Hours";
      status = true;
    } else if (this.props.action === "accept" && !distance) {
      message = "Please enter Distance";
      status = true;
    }
    if (message) {
      Snackbar.show({
        title: message,
        duration: Snackbar.LENGTH_SHORT
      });
    }
    return status;
  };

  editOneWayBooking = () => {
    const errors = this.getErrors();
    const { action } = this.props;

    if (errors === false) {
      this.setState({ creatingBooking: true });

      const type = BOOKING_TYPE.AD_HOC_ONE_WAY;
      const {
        selectedCustomer: customer_id,
        selectedVehicle: vehicle_id,
        selectedVehicleType,
        selectedVendor: vendor_id,
        selectedDateTime,
        selectedSourceCity: source_city,
        selectedDestinationCity: destination_city,
        selectedViaCities: via_cities,
        selectedEngageReason: engaged_by_id,
        expectedTat: expected_tat,
        distance,
        comment,
        reference
      } = this.state;
      const request_data = {
        customer_id,
        vehicle_id,
        vehicle_type_id: `${selectedVehicleType}`,
        vendor_id,
        action,
        engaged_by_id,
        forward: {
          booking_id: this.props.booking_id,
          date_time: new moment(selectedDateTime).format("DD-MM-YYYY HH:mm"),
          source_city,
          destination_city,
          via_cities,
          expected_tat,
          distance,
          comment,
          reference
        }
      };

      const { editBooking } = this.props;
      editBooking({ type, request_data })
        .then(response => {
          const { status } = response;
          if (status) {
            Snackbar.show({
              title: "" + response.message,
              duration: Snackbar.LENGTH_LONG
            });

            this.props.navigation.goBack();
          } else {
            Snackbar.show({
              title: "" + response.message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .catch(error => {
          console.log("error ----", error);
        })
        .finally(() => this.setState({ creatingBooking: false }));
    }
  };

  goToBookings = () => {
    this.setState({ showModal: false }, () => {
      this.props.navigation.navigate(NAVIGATE.ALL_BOOKINGS);
    });
  };

  updateReference = reference => {
    this.setState({ reference });
  };

  acceptOneWayBooking = () => {
    const errors = this.getErrors();
    const { action } = this.props;

    if (errors === false) {
      this.setState({ creatingBooking: true });

      const type = BOOKING_TYPE.AD_HOC_ONE_WAY;
      const {
        selectedCustomer: customer_id,
        selectedVehicle: vehicle_id,
        selectedVehicleType,
        selectedVendor: vendor_id,
        selectedDateTime,
        selectedSourceCity: source_city,
        selectedDestinationCity: destination_city,
        selectedViaCities: via_cities,
        selectedEngageReason: engaged_by_id,
        expectedTat: expected_tat,
        distance,
        dockHours: docking_hours,
        reference
      } = this.state;
      const request_data = {
        booking_id: this.props.booking_id,
        customer_id,
        vehicle_id,
        vehicle_type_id: `${selectedVehicleType}`,
        vendor_id,
        action,
        engaged_by_id,
        forward: {
          booking_id: this.props.booking_id,
          date_time: new moment(selectedDateTime).format("DD-MM-YYYY HH:mm"),
          source_city,
          destination_city,
          via_cities,
          expected_tat,
          distance,
          docking_hours,
          reference
        }
      };

      const { acceptBooking } = this.props;
      acceptBooking({ type, request_data })
        .then(response => {
          const { status } = response;
          if (status) {
            Snackbar.show({
              title: "" + response.message,
              duration: Snackbar.LENGTH_LONG
            });

            this.goToBookings();
          } else {
            Snackbar.show({
              title: "" + response.message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .catch(error => {
          console.log("error ----", error);
        })
        .finally(this.setState({ creatingBooking: false }));
    }
  };

  setEngageReason = selectedEngageReason => {
    this.setState({ selectedEngageReason });
  };

  render() {
    const { customers, booking_id = null, action } = this.props;
    const {
      selectedCustomer,
      driverPhoneNumber,
      bookingType,
      selectedDateTime,
      bookingStatus,
      enableDistance
    } = this.state;

    console.log("this.state ----", this.state);

    return (
      <Fragment>
        <Header
          name={
            this.props.action === "edit" ? "EDIT BOOKING" : "ACCEPT BOOKING"
          }
          navigation={this.props.navigation}
          goBack={true}
        />

        <ScrollView>
          <View style={customStyles.scrollContainer}>
            <CustomerInput
              customers={customers}
              customerIds={this.props.customerIds}
              selectedCustomer={selectedCustomer}
              selectCustomer={this.selectCustomer}
              disabled={this.state.customerDisabled}
              // disabled={this.props.action === "accept" ? true : false}
            />

            {bookingStatus === "RQ" ? (
              <View>
                <View style={{ flexDirection: "row" }}>
                  <View style={{ flex: 1 }}>
                    <VehicleInput
                      vehicles={this.props.vehicles}
                      vehicleTypes={this.props.vehicleTypes}
                      selectedVehicle={this.state.selectedVehicle}
                      selectVehicle={this.selectVehicle}
                      disabled={this.state.vehicleDisabled}
                    />
                  </View>

                  <View style={{ flex: 1 }}>
                    <VehicleTypeInput
                      vehiclesTypes={this.props.vehicleTypes}
                      selectedVehicle={this.state.selectedVehicleType}
                      selectVehicle={this.selectVehicleType}
                      selectOne={true}
                      disabled={this.state.vehicleTypeDisabled}
                    />
                  </View>
                </View>
                <VendorInput
                  vendors={this.props.vendors}
                  selectedVendor={this.state.selectedVendor}
                  selectVendor={this.selectVendor}
                  disabled={
                    !this.state.selectedVehicle || this.state.vendorDisabled
                  }
                />
              </View>
            ) : null}
            <DateTimeInput
              message={"Planned Trip Date and Time"}
              selectedDateTime={this.state.selectedDateTime}
              selectDateTime={this.setDate}
              initialValue={this.state.selectedDateTime}
            />
            <CityInput
              cities={this.props.cities}
              cityIds={this.props.cityIds}
              selectedCity={this.state.selectedSourceCity}
              selectCity={this.selectSourceCity}
              message={"Source City"}
              initialValue={this.state.selectedSourceCity}
              disabled={this.state.sourceCityDisable}
            />
            <CityInput
              cities={this.props.cities}
              cityIds={this.props.cityIds}
              selectedCity={this.state.selectedDestinationCity}
              selectCity={this.selectDestinationCity}
              message={"Destination City"}
              initialValue={this.state.selectedDestinationCity}
              disabled={this.state.destinationCityDisable}
            />

            <ViaCityInput
              cities={this.props.cities}
              cityIds={this.props.cityIds}
              selectedViaCities={this.state.selectedViaCities}
              addViaCity={this.addSelectedViaCity}
              removeViaCity={this.removeSelectedViaCity}
            />

            {this.props.action === "accept" && (
              <Fragment>
                <NumberInput
                  numericProp={true}
                  disable={true}
                  message={"Dock Hours"}
                  updateText={this.updateDockHours}
                  placeholder={"Dock Hours"}
                  initialValue={this.state.dockHours}
                />
              </Fragment>
            )}

            {/*<ExpectedTatInput*/}
            {/*  message={"Expected TAT"}*/}
            {/*  initialValue={this.state.expectedTat}*/}
            {/*  updateText={this.updateTAT}*/}
            {/*  vehicle_type_id={this.state.selectedVehicleType}*/}
            {/*  customer_id={this.state.selectedCustomer}*/}
            {/*  source_city={this.state.selectedSourceCity}*/}
            {/*  destination_city={this.state.selectedDestinationCity}*/}
            {/*  via_cities={this.state.selectedViaCities}*/}
            {/*  source_flag={AUTO_FILL_TAT.EDIT_BOOKING}*/}
            {/*  placeholder={"Click Icon to autofill tat"}*/}
            {/*  disabledInitialValue={true}*/}
            {/*  distanceInitialValue={this.state.distance}*/}
            {/*/>*/}

            <DistanceInputNew
              bookingID={this.props.booking_id}
              message={"Expected TAT"}
              initialValue={this.state.expectedTat}
              updateText={this.updateTAT}
              vehicle_type_id={this.state.selectedVehicleType}
              customer_id={this.state.selectedCustomer}
              source_city={this.state.selectedSourceCity}
              destination_city={this.state.selectedDestinationCity}
              via_cities={this.state.selectedViaCities}
              source_flag={AUTO_FILL_TAT.EDIT_BOOKING}
              placeholder={"Click Icon to autofill tat"}
              distanceValue={this.state.distance}
              action={this.props.action}
              enableDistance={this.state.enableDistance}
              enableTat={this.state.expectedTatEnable}
              apiHit={this.state.apiHit}
              count={count}
            />

            {/*<DistanceInput*/}
            {/*  message={"Distance"}*/}
            {/*  updateText={this.updateDistance}*/}
            {/*  placeholder={"Distance"}*/}
            {/*  initialValue={this.state.distance}*/}
            {/*  vehicle_type_id={this.state.selectedVehicleType}*/}
            {/*  customer_id={this.state.selectedCustomer}*/}
            {/*  enableDistance={enableDistance}*/}
            {/*  source_city={this.state.selectedSourceCity}*/}
            {/*  destination_city={this.state.selectedDestinationCity}*/}
            {/*  via_cities={this.state.selectedViaCities}*/}
            {/*  source_flag={AUTO_FILL_TAT.EDIT_BOOKING}*/}
            {/*/>*/}

            <NumberInput
              numericProp={false}
              message={"Reference"}
              updateText={this.updateReference}
              placeholder={"Enter Reference"}
              initialValue={this.state.reference}
            />

            <EngagedByInput
              engagedBy={this.props.engagedBy}
              selectedEngageReason={this.state.selectedEngageReason}
              setEngageReason={this.setEngageReason}
            />
          </View>
        </ScrollView>
        <Footer
          name={action === "edit" ? "EDIT" : "ACCEPT"}
          action={
            action === "edit"
              ? this.editOneWayBooking
              : this.acceptOneWayBooking
          }
          loading={this.state.creatingBooking}
        />
      </Fragment>
    );
  }
}

export default EditBookingOneWay;
