import React, { Component, Fragment } from "react";
import {
  ActivityIndicator,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  KeyboardAvoidingView,
  View
} from "react-native";
import Header from "../common/header";
import VendorInput from "../common/VendorsInput";
import VehicleInput from "../common/VehicleInput";
import { NAVIGATE, REQUEST_TYPE, VEHICLE_CATEGORY } from "../../../constants";
import CityInput from "../common/CityInput";
import Snackbar from "react-native-snackbar";
import VehicleChangeReasonsInput from "../common/VehicleChangeReasonsInput";
import EngagedByInput from "../../containers/EngagedByInput";
import NumberInput from "../common/NumberInput";
import CustomStyles from "../common/Styles";
import colors from "../common/Colors";
import Footer from "../common/footer";
import MapView, { Marker } from "react-native-maps";
import { Icon } from "react-native-elements";
import OfflineBanners from "../common/OfflineBanner";
import { doRequest } from "../../helpers/network";
import { Bookings, Trips } from "../../helpers/Urls";
import DateTimeInput from "../common/DateTimeInput";
import moment from "moment";
import BookingSuccessModal from "../common/BookingSuccessModal";

class CrossDockTrip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedVendor: null,
      selectedVehicle: null,
      selectedCity: null,
      selectedReason: 0,
      selectedEngageReason: 0,
      driverPhoneNumber: null,
      distance: null,
      distanceDisabled: true,
      fetchingDistance: false,
      crossDocking: false,
      loading: false,
      location: null,
      latitude: null,
      longitude: null,
      showModal: false,
      selectedDateTime: new moment()
    };
    this.timeout = 0;
  }

  componentDidMount() {}

  crossDockVehicle = () => {
    const { crossDock, trip_id, vehicleChangeReasons } = this.props;
    const {
      selectedVendor: vendor_id,
      selectedVehicle: vehicle_id,
      selectedCity: crossdock_city_id,
      selectedReason,
      selectedEngageReason: engaged_by_id,
      driverPhoneNumber: phone,
      distance,
      location,
      latitude: lat,
      longitude: long,
      selectedDateTime: crossdock_time
    } = this.state;
    const { value: reason } = vehicleChangeReasons[selectedReason] || {};
    if (
      vendor_id &&
      vehicle_id &&
      crossdock_city_id &&
      reason &&
      phone &&
      distance &&
      location &&
      lat &&
      long &&
      crossdock_time
    ) {
      this.setState({ crossDocking: true });
      const data = {
        trip_id,
        vehicle_id,
        vendor_id,
        engaged_by_id,
        lat,
        long,
        location,
        crossdock_city_id,
        reason,
        crossdock_time: crossdock_time.format("DD-MM-YYYY HH:mm"),
        distance,
        phone
      };
      console.log("data ----", data);
      crossDock(data)
        .then(result => {
          const { status = false, message = "Failed" } = result;
          if (status) {
            this.setState({ showModal: "CrossDocked Successfully" });
          }
          if (status === false) {
            Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .finally(() => this.setState({ crossDocking: false }));
    } else {
      Snackbar.show({
        title: "Please select all fields",
        duration: Snackbar.LENGTH_SHORT
      });
    }
  };

  selectVehicle = vehicle => {
    const { vehicles } = this.props;
    const { basicInfo: { vehicle_type_id, vehicle_category, vendor_id } = {} } =
      vehicles[vehicle] || {};
    if (VEHICLE_CATEGORY.CON === vehicle_category) {
      this.setState({ selectedVendor: vendor_id, vendorDisabled: true });
    } else {
      this.setState({ selectedVendor: 0, vendorDisabled: false });
    }
    this.setState({
      selectedVehicle: vehicle,
      selectedVehicleType: vehicle_type_id
    });
  };

  selectVendor = vendor => {
    this.setState({ selectedVendor: vendor });
  };

  selectCity = city => {
    this.setState(
      { selectedCity: city, fetchingDistance: true },
      this.fetchDistance
    );
  };

  selectReason = selectedReason => {
    this.setState({ selectedReason });
  };

  setEngageReason = selectedEngageReason => {
    this.setState({ selectedEngageReason });
  };

  updateDriverNumber = phoneNumber => {
    this.setState({ driverPhoneNumber: phoneNumber });
  };

  setDate = dateTime => {
    this.setState({ selectedDateTime: dateTime });
  };

  fetchDistance = () => {
    const { selectedCity } = this.state;
    const { trip } = this.props;
    const { basicInfo: { source_city_id } = {} } = trip || {};
    const data = {
      source_flag: "CROSSDOCK",
      source_city: source_city_id,
      destination_city: selectedCity
    };
    const response = doRequest({
      method: REQUEST_TYPE.POST,
      url: Bookings.fetchExpectedBookingURL(),
      isMAPI: true,
      data
    })
      .then(result => {
        console.log("result ----", result);
        const { status, data: { distance } = {} } = result;
        if (distance) {
          this.setState({ distance, distanceDisabled: true });
        } else {
          this.setState({ distanceDisabled: false, distance: null });
        }
      })
      .finally(() => {
        this.setState({ fetchingDistance: false });
      });
  };

  fetchLocation = async ({ lat, long }) => {
    let response = await doRequest({
      method: REQUEST_TYPE.GET,
      url: Trips.reverseGeocodeURL(lat, long),
      isMAPI: true
    });
    console.log("response ----", response);
    return response;
  };

  regionChangedLocation = coordinate => {
    const { latitude, longitude } = coordinate || {};
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.setState({ loading: true, latitude, longitude });
      this.fetchLocation({ lat: latitude, long: longitude })
        .then(result => {
          console.log("result ----", result);
          const { status, data: { location } = {} } = result;
          if (status) {
            this.setState({ location, loading: false });
          } else {
            this.setState({ loading: false });
          }
        })
        .catch(error => {
          this.setState({ loading: false });
        });
    }, 600);
  };

  animateMap = () => {
    const { latitude, longitude } = this.state;
    const region = {
      latitude: latitude,
      longitude: longitude,
      latitudeDelta: 0.002,
      longitudeDelta: 0.002
    };
    this.map.animateToRegion(region, 700);
  };

  updateLocation = async region => {
    const { latitude, longitude, location } = region;
    this.setState({ latitude, longitude, location }, this.animateMap);
  };

  goToLocationSearch = () => {
    this.props.navigation.navigate(NAVIGATE.LOCATION_SEARCH, {
      updateLocation: this.updateLocation.bind(this)
    });
  };

  goBack = () => {
    this.setState({ showModal: false }, () => {
      this.props.navigation.goBack();
    });
  };

  render() {
    const { trip, isConnected, cities } = this.props;
    const { basicInfo: { code, lowerBound, source_city_id } = {} } = trip || {};
    const { basicInfo: { name: cityName = "Crossdock city" } = {} } =
      cities[source_city_id] || {};
    const { loading, distance, distanceDisabled } = this.state;

    return (
      <Fragment>
        <Header
          name={`CROSS DOCK ${code}`}
          navigation={this.props.navigation}
          goBack={true}
        />
        <View style={{ flex: 1 }}>
          <View style={this.state.expand ? { flex: 2 } : { flex: 1 }}>
            <MapView
              ref={ref => {
                this.map = ref;
              }}
              provider={this.props.provider}
              style={styles.map}
              initialRegion={{
                latitude: 20.5937,
                longitude: 78.9629,
                latitudeDelta: 12,
                longitudeDelta: 12
              }}
              //onMarkerDragStart={() => this.setState({ expand: true })}
              //onMarkerDragEnd={() => this.setState({ expand: false })}
              // onRegionChange={e => console.log(e)}
              onRegionChangeComplete={this.regionChangedLocation}
            />
            <Icon
              size={24}
              raised
              containerStyle={{ position: "absolute", top: 10, right: 10 }}
              name={"magnify"}
              type="material-community"
              color={colors.darkblue}
              onPress={this.goToLocationSearch}
            />
            <View
              pointerEvents="none"
              style={{
                position: "absolute",
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "transparent"
              }}
            >
              <Icon
                size={35}
                name={"map-marker"}
                type="material-community"
                color={colors.darkblue}
              />
            </View>
          </View>

          <KeyboardAvoidingView
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "center"
            }}
            behavior="padding"
            enabled
            keyboardVerticalOffset={100}
          >
            <View style={{ flex: 1 }} elevation={100}>
              <ScrollView contentContainerStyle={CustomStyles.scrollContainer}>
                <View style={CustomStyles.borderDisabled}>
                  <Text style={CustomStyles.labelTextDisabled}>Location</Text>
                  <View style={styles.locationText}>
                    <View style={{ flex: loading ? 0.8 : 1, flexWrap: "wrap" }}>
                      <Text style={{ color: colors.black25 }}>
                        {this.state.location
                          ? String(this.state.location)
                          : "Searching..."}
                      </Text>
                    </View>
                    {loading && (
                      <View style={{ flex: 0.2 }}>
                        <ActivityIndicator
                          size="small"
                          color={colors.darkblue}
                        />
                      </View>
                    )}
                  </View>
                </View>

                <VehicleInput
                  vehicles={this.props.vehicles}
                  vehicleTypes={this.props.vehicleTypes}
                  selectedVehicle={this.state.selectedVehicle}
                  selectVehicle={this.selectVehicle}
                />
                <VendorInput
                  vendors={this.props.vendors}
                  selectedVendor={this.state.selectedVendor}
                  selectVendor={this.selectVendor}
                  disabled={
                    !this.state.selectedVehicle || this.state.vendorDisabled
                  }
                />
                <CityInput
                  cities={this.props.cities}
                  cityIds={this.props.cityIds}
                  selectedCity={this.state.selectedCity}
                  selectCity={this.selectCity}
                  message={"Select Crossdock City"}
                />
                <View
                  style={
                    distanceDisabled
                      ? CustomStyles.borderDisabled
                      : CustomStyles.border
                  }
                >
                  <Text
                    style={
                      distanceDisabled
                        ? CustomStyles.labelTextDisabled
                        : CustomStyles.labelText
                    }
                  >{`Distance from ${cityName}`}</Text>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <TextInput
                      placeholder={
                        !distanceDisabled
                          ? "Enter distance in kilometers"
                          : "Please select Crossdock City first"
                      }
                      style={[
                        CustomStyles.pickerView,
                        distanceDisabled
                          ? { color: colors.black25 }
                          : { color: colors.black85 },
                        {
                          ...Platform.select({
                            ios: { maxHeight: 30, paddingBottom: 10 },
                            android: {
                              flexGrow: 1,
                              paddingBottom: 0,
                              paddingTop: 0
                            }
                          })
                        }
                      ]}
                      onChangeText={text => this.setState({ distance: text })}
                      value={distance ? String(distance) : ""}
                      keyboardType="numeric"
                      placeholderTextColor={colors.black25}
                      editable={!distanceDisabled}
                    />
                    {this.state.fetchingDistance && (
                      <ActivityIndicator
                        size="small"
                        style={{ marginRight: 8 }}
                        color={colors.darkblue}
                      />
                    )}
                  </View>
                </View>
                <VehicleChangeReasonsInput
                  selectedReason={this.state.selectedReason}
                  selectReason={this.selectReason}
                  vehicleChangeReasons={this.props.vehicleChangeReasons}
                />
                <NumberInput
                  message={"Driver Phone"}
                  updateText={this.updateDriverNumber}
                  numericProp={true}
                  placeholder={"Enter Driver Phone Number"}
                />
                <DateTimeInput
                  message={"Crossdock Date and Time"}
                  selectedDateTime={this.state.selectedDateTime}
                  selectDateTime={this.setDate}
                  upperbound={true}
                  lowerBound={lowerBound ? lowerBound : null}
                />
                <EngagedByInput
                  engagedBy={this.props.engagedBy}
                  selectedEngageReason={this.state.selectedEngageReason}
                  setEngageReason={this.setEngageReason}
                />
              </ScrollView>
            </View>
          </KeyboardAvoidingView>
        </View>
        {!isConnected && <OfflineBanners bottom={0} />}
        <Footer
          name={"CROSS DOCK"}
          action={this.crossDockVehicle}
          loading={this.state.crossDocking}
        />
        <BookingSuccessModal
          isVisible={!!this.state.showModal}
          message={this.state.showModal}
          closeModal={this.goBack}
        />
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  // distanceText: {
  //   flex: 1,
  //   alignItems: "flex-start",
  //   paddingLeft: 10,
  //   justifyContent: "center",
  //   ...Platform.select({
  //     ios: { maxHeight: 30, paddingBottom: 10 },
  //     android: { flexGrow: 1, paddingBottom: 0, paddingTop: 0 }
  //   })
  // },
  locationText: {
    flex: 1,
    flexDirection: "row",
    alignItems: "flex-start",
    paddingLeft: 10,
    justifyContent: "center",
    ...Platform.select({
      ios: { paddingBottom: 10 },
      android: { flexGrow: 1, paddingBottom: 6, paddingTop: 2 }
    })
  },
  map: { flex: 1 },
  marker: { height: 20, width: 20 }
});

export default CrossDockTrip;
