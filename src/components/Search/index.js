import React, { Component, Fragment } from "react";
import VehicleInput from "../common/VehicleInput";
import Header from "../common/header";
import moment from "moment";
import { View } from "react-native";
import { withNavigationFocus } from "react-navigation";
import NumberInput from "../common/NumberInput";
import { NAVIGATE, SEARCH_REQUEST_DATA } from "../../../constants";
import { Button } from "react-native-elements";
import CityInput from "../common/CityInput";
import CustomerInput from "../common/CustomerInput";
import Snackbar from "react-native-snackbar";

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedSourceCity: 0,
      selectedDestinationCity: 0,
      selectedCustomer: 0,
      selectedVehicle: 0,
      selectedVehicleType: 0,
      selectedDateTime: new moment(),
      bookingId: null,
      tripId: null,
      isLoading: false,
      reference: null
    };
  }

  componentDidMount() {}

  componentDidUpdate(prevProps) {
    if (prevProps.isFocused !== this.props.isFocused) {
      this.setState({
        selectedSourceCity: 0,
        selectedDestinationCity: 0,
        selectedCustomer: 0,
        selectedVehicle: 0,
        selectedVehicleType: 0,
        selectedDateTime: new moment(),
        bookingId: null,
        tripId: null,
        isLoading: false,
        reference: null
      });
    }
  }

  selectCustomer = selectedCustomer => {
    this.setState({ selectedCustomer });
  };

  selectVehicle = vehicle => {
    this.setState({ selectedVehicle: vehicle });
  };

  selectVehicleType = vehicleType => {
    this.setState({ selectedVehicleType: vehicleType });
  };
  setDate = dateTime => {
    this.setState({ selectedDateTime: dateTime });
  };
  selectSourceCity = city => {
    this.setState({ selectedSourceCity: city });
  };
  selectDestinationCity = city => {
    this.setState({ selectedDestinationCity: city });
  };
  setReference = reference => {
    this.setState({ reference });
  };

  setBookingId = id => {
    console.log("id ----", id);
    this.setState({ bookingId: id });
  };

  setTripId = id => {
    console.log("id ----", id);
    this.setState({ tripId: id });
  };

  getSearchData = () => {
    const {
      bookingId,
      tripId: trip_code,
      selectedSourceCity,
      selectedDestinationCity,
      selectedCustomer,
      selectedVehicle,
      selectedDateTime,
      reference
    } = this.state;

    if (
      selectedSourceCity ||
      selectedDestinationCity ||
      selectedCustomer ||
      selectedVehicle ||
      bookingId ||
      trip_code
    ) {
      const requestData = {
        [SEARCH_REQUEST_DATA.BOOKING_ID]: bookingId,
        trip_code,
        reference,
        vehicle_id: selectedVehicle,
        customer_id: selectedCustomer,
        source_city_id: selectedSourceCity,
        destination_city_id: selectedDestinationCity
      };

      // NAVIGATE.SEARCH_REQUEST_DATA = requestData;

      this.props.navigation.navigate(NAVIGATE.SEARCH_RESULT, { requestData });

      this.setState({ isLoading: false });
    } else {
      Snackbar.show({
        title: "Please enter one field for Search",
        duration: Snackbar.LENGTH_LONG
      });
    }
  };

  clearData = () => {
    this.setState({
      selectedSourceCity: 0,
      selectedDestinationCity: 0,
      selectedCustomer: 0,
      selectedVehicle: 0,
      selectedVehicleType: 0,
      bookingId: "",
      tripId: ""
    });
  };

  render() {
    return (
      <Fragment>
        <Header
          name={"SEARCH"}
          navigation={this.props.navigation}
          menu={true}
        />

        <View style={{ flex: 1, marginTop: 10 }}>
          <NumberInput
            numericProp={false}
            message={"BookingId"}
            updateText={this.setBookingId}
            placeholder={"Enter BookingId"}
            initialValue={this.state.bookingId}
          />
          <NumberInput
            numericProp={false}
            message={"TripId"}
            updateText={this.setTripId}
            placeholder={"Enter TripId"}
            initialValue={this.state.tripId}
          />
          {/*<NumberInput*/}
          {/*  numericProp={false}*/}
          {/*  message={"Reference"}*/}
          {/*  updateText={this.setReference}*/}
          {/*  placeholder={"Enter Reference"}*/}
          {/*  initialValue={this.state.tripId}*/}
          {/*/>*/}

          <CustomerInput
            customers={this.props.customers}
            customerIds={this.props.customerIds}
            selectedCustomer={this.state.selectedCustomer}
            selectCustomer={this.selectCustomer}
            disabled={false}
          />

          <VehicleInput
            vehicles={this.props.vehicles}
            vehicleTypes={this.props.vehicleTypes}
            selectedVehicle={this.state.selectedVehicle}
            selectVehicle={this.selectVehicle}
          />

          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedCity={this.state.selectedSourceCity}
            selectCity={this.selectSourceCity}
            message={"Source City"}
          />

          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedCity={this.state.selectedDestinationCity}
            selectCity={this.selectDestinationCity}
            message={"Destination City"}
          />
        </View>

        <View
          style={{
            flex: 1,
            flexDirection: "row",
            alignItems: "flex-end",
            justifyContent: "center"
          }}
        >
          <Button
            title={"SEARCH"}
            titleStyle={{
              fontSize: 15,
              fontFamily: "CircularStd-Book"
            }}
            onPress={this.getSearchData}
            loading={this.state.isLoading}
            buttonStyle={{ marginTop: 15, marginBottom: 10 }}
          />
          <Button
            title={"CLEAR"}
            titleStyle={{
              fontSize: 15,
              fontFamily: "CircularStd-Book"
            }}
            onPress={this.clearData}
            buttonStyle={{ marginTop: 15, marginBottom: 10, marginLeft: 10 }}
          />
        </View>
      </Fragment>
    );
  }
}

export default withNavigationFocus(Search);
