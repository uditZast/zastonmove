import React, { Component, Fragment } from "react";
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity
} from "react-native";
import VehicleInput from "../common/VehicleInput";
import Header from "../common/header";
import DateInput from "../common/DateInput";
import moment from "moment";
import CommonStyles from "../common/Styles";
import colors from "../common/Colors";
import Snackbar from "react-native-snackbar";
import { NAVIGATE } from "../../../constants";

class TrackingSearchPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedVehicle: null,
      startDate: null,
      endDate: null
    };
  }

  componentDidMount() {
    // this.fetchTrackingDetails(1);
  }

  fetchTrackingDetails = () => {
    const { selectedVehicle, startDate, endDate } = this.state;
    let data = { vehicle_id: selectedVehicle };
    if (selectedVehicle) {
      if (startDate) {
        data = { ...data, start_date: new moment(startDate) };
      }
      if (endDate) {
        data = { ...data, end_date: new moment(endDate) };
      }
      this.props.navigation.navigate(NAVIGATE.TRACKING_SEARCH_RESULT, {
        ...data
      });
    } else {
      Snackbar.show({
        title: "Please select a vehicle",
        duration: Snackbar.LENGTH_LONG
      });
    }
  };

  selectVehicle = vehicle => {
    this.setState({ selectedVehicle: vehicle });
  };

  setStartDate = date => {
    this.setState({ startDate: date });
  };

  setEndDate = date => {
    this.setState({ endDate: date });
  };

  render() {
    return (
      <Fragment>
        <Header
          name={"VEHICLE TRACKING"}
          navigation={this.props.navigation}
          goBack={false}
        />
        <View style={styles.searchContainer}>
          <View>
            <View style={styles.padding10}>
              <VehicleInput
                vehicles={this.props.vehicles}
                vehicleTypes={this.props.vehicleTypes}
                selectedVehicle={this.state.selectedVehicle}
                selectVehicle={this.selectVehicle}
              />
            </View>
            <View style={CommonStyles.row}>
              <View style={CommonStyles.flex1}>
                <DateInput
                  message={"Start Date"}
                  selectedDateTime={this.state.startDate}
                  selectDateTime={this.setStartDate}
                  upperbound={true}
                />
              </View>
              <View style={CommonStyles.flex1}>
                <DateInput
                  message={"End Date"}
                  selectedDateTime={this.state.endDate}
                  selectDateTime={this.setEndDate}
                  upperbound={true}
                  lowerbound={this.state.startDate}
                />
              </View>
            </View>
          </View>
          <TouchableOpacity
            style={CommonStyles.commonButton}
            onPress={this.fetchTrackingDetails}
          >
            {this.props.pageData.isFetching && (
              <ActivityIndicator size="small" color={colors.white} />
            )}
            {!this.props.pageData.isFetching && (
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: "500",
                  color: colors.white
                }}
              >
                Search Vehicle
              </Text>
            )}
          </TouchableOpacity>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  padding10: {
    paddingTop: 6
  },
  searchContainer: {
    flex: 1,
    justifyContent: "space-between"
  }
});

export default TrackingSearchPage;
