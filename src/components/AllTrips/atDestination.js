import React, { Component, Fragment } from "react";
import { FlatList, View } from "react-native";
import Header from "../common/header";
import { TRIP_STATUS } from "../../../constants";
import customStyles from "../common/Styles";
import { SearchBar } from "react-native-elements";
import TripCard from "../common/TripCard";

class AtDestinationTrips extends Component {
  constructor(props) {
    super(props);
    this.state = { input: null };
  }

  componentDidMount() {
    this.fetchTrips();
  }

  fetchTrips = () => {
    const { fetchTripsByStatus } = this.props;
    fetchTripsByStatus(TRIP_STATUS.AT_DESTINATION);
  };

  setSearch = text => {
    this.setState({ input: text });
  };

  clearInput = () => {
    this.setState({ input: null });
  };

  renderTrip = ({ index, item: trip_id }) => {
    const { trips, customers, vendors, vehicles, navigation } = this.props;
    const trip = trips[trip_id] || {};
    return (
      <View key={index} style={`${index}` === "0" ? { paddingTop: 10 } : {}}>
        <TripCard
          trip={trip}
          customers={customers}
          vendors={vendors}
          vehicles={vehicles}
          navigation={navigation}
        />
      </View>
    );
  };

  renderAtDestinationTripsList = () => {
    const {
      trips,
      vendors,
      vehicles,
      pageData: { at_dest_trip_ids: trip_ids = [], isFetching = false } = {}
    } = this.props;
    const { input } = this.state;
    let list = trip_ids;
    if (input && input.toUpperCase()) {
      const u_input = input.toUpperCase();
      list = list.filter(trip_id => {
        const { basicInfo: { code, type, route, vendor_id, vehicle_id } = {} } =
          trips[trip_id] || {};
        const { basicInfo: { code: vendorCode, nick_name } = {} } =
          vendors[vendor_id] || {};
        const { basicInfo: { vehicle_number } = {} } = vehicles[vehicle_id];

        let return_val =
          code.toUpperCase().includes(u_input) ||
          type.toUpperCase().includes(u_input) ||
          route.toUpperCase().includes(u_input) ||
          vendorCode.toUpperCase().includes(u_input) ||
          vehicle_number.toUpperCase().includes(u_input);

        if (nick_name) {
          return_val = return_val || nick_name.toUpperCase().includes(u_input);
        }

        return return_val;
      });
    }
    return (
      <FlatList
        data={list}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderTrip}
        onRefresh={this.fetchTrips}
        keyboardShouldPersistTaps={"handled"}
        refreshing={isFetching}
        showsVerticalScrollIndicator={false}
        extraData={this.props.trips}
      />
    );
  };

  render() {
    return (
      <Fragment>
        <Header
          name={"AT DESTINATION TRIPS"}
          navigation={this.props.navigation}
        />
        <View style={{ flex: 1 }}>
          <SearchBar
            placeholder="Search Trip id/Vehicle/Vendor/Route..."
            onChangeText={this.setSearch}
            lightTheme
            containerStyle={customStyles.searchBarContainer}
            value={this.state.input}
            onCancel={this.clearInput}
            onClear={this.clearInput}
          />
          <View style={{ flex: 1 }}>{this.renderAtDestinationTripsList()}</View>
        </View>
      </Fragment>
    );
  }
}

export default AtDestinationTrips;
