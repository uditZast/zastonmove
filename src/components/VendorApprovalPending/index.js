import React, { Component, Fragment } from "react";
import { FlatList, Platform, StyleSheet, View } from "react-native";
import VendorApprovalCard from "../common/VendorApprovalCard";
import colors from "../../colors/Colors";
import Header from "../common/header";
import { SearchBar } from "react-native-elements";
import customStyles from "../common/Styles";

class VendorApprovalPending extends Component {
  constructor(props) {
    super(props);
    this.state = { selectedIndex: 0, input: null };
  }

  componentDidMount() {
    const { loggedInUser } = this.props;
    const { role } = loggedInUser;
    this.fetchVendorApprovalPending();
  }

  fetchVendorApprovalPending = () => {
    const { fetchVendorApprovalPending } = this.props;
    fetchVendorApprovalPending();
  };

  clearInput = () => {
    this.setState({ input: null });
  };

  setSearch = text => {
    this.setState({ input: text });
  };

  renderVendorItem = ({ index, item: vendor_id }) => {
    const { vendors, navigation } = this.props;

    const {
      basicInfo: {
        id,
        nick_name,
        name,
        code,
        poc,
        phone_1,
        phone_2,
        email,
        address,
        is_active,
        is_approved,
        is_otp_verified,
        status
      } = {}
    } = vendors[vendor_id] || {};

    const { loggedInUser, users } = this.props;

    return (
      <View key={index} style={index === 0 ? { paddingTop: 10 } : {}}>
        <VendorApprovalCard
          id={id}
          nickname={nick_name}
          name={name}
          code={code}
          poc_name={poc}
          phone1={phone_1}
          phone2={phone_2}
          email={email}
          address={address}
          is_active={is_active}
          is_approved={is_approved}
          is_otpVerified={is_otp_verified}
          status={status}
          navigation={navigation}
          loggedInUser={loggedInUser}
          users={users}
        />
      </View>
    );
  };

  selectIndex = selectedIndex => {
    this.setState({ selectedIndex });
  };

  renderVendorsList = () => {
    const { vendors, vendorIds } = this.props;

    let list = vendorIds;

    // let list = Object.keys(vendors);
    const { input } = this.state;
    if (input && input.toUpperCase()) {
      const u_input = input.toUpperCase();
      list = list.filter(vendor_id => {
        const { basicInfo: { name = "", code } = {} } =
          vendors[vendor_id] || {};
        return (
          name.toUpperCase().includes(u_input) ||
          code.toUpperCase().includes(u_input)
        );
      });
    }
    return (
      <FlatList
        keyboardShouldPersistTaps={"handled"}
        data={[...list]}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderVendorItem}
        showsVerticalScrollIndicator={false}
      />
    );
  };

  render() {
    return (
      <Fragment>
        <Header name={"VENDOR APPROVAL"} navigation={this.props.navigation} />

        <SearchBar
          placeholder="Search"
          onChangeText={this.setSearch}
          lightTheme
          containerStyle={customStyles.searchBarContainer}
          value={this.state.input}
          onCancel={this.clearInput}
          onClear={this.clearInput}
        />

        {this.renderVendorsList()}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.listbackgroundcolor
  },
  forwardStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    paddingTop: 3
  },
  returnStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginRight: 10,
    marginLeft: 0,
    marginTop: 3
  },
  boldTextStyle: {
    fontSize: 15,
    fontWeight: "bold",
    color: colors.blueColor,
    fontFamily: "CircularStd-Book"
  },
  normalTextStyle: {
    fontSize: 12,
    color: colors.textcolor,
    marginLeft: 5,
    paddingTop: 3,
    fontFamily: "CircularStd-Book"
  },
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    marginTop: 5,
    paddingBottom: 5
  },
  changeColorlineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 5,
    backgroundColor: colors.blue
  },
  rowStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 5
  },
  rowStyle1: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 3
  },
  textStyle1: {
    flex: 0.5,
    fontSize: 12,
    color: colors.textcolor,
    fontFamily: "CircularStd-Book"
  },
  textStyle2: {
    fontSize: 12,
    color: colors.textcolor,
    paddingTop: 3,
    fontFamily: "CircularStd-Book"
  },
  rowStyle2: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 0
  },
  customerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    ...Platform.select({
      ios: {
        padding: 10,
        borderRadius: 10,
        marginTop: 10,
        overflow: "hidden"
      },
      android: {
        paddingTop: 5,
        paddingLeft: 5,
        paddingBottom: 5,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        marginTop: 8
      }
    })
  },
  row1Background: {
    flex: 1,
    flexDirection: "row"
  },
  row2Background: {
    flex: 1,
    paddingBottom: 10
  },
  rowBackgroundColor: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: colors.blue
  },
  columnBackgroundColor: {
    flex: 1,
    backgroundColor: colors.blue
  }
});

export default VendorApprovalPending;
