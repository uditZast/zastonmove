import React, { Component, Fragment } from "react";
import {
  Keyboard,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View
} from "react-native";
import Header from "../common/header";
import { Button } from "react-native-elements";
import colors from "../common/Colors";
import { NAVIGATE } from "../../../constants";
import VendorDetails from "../../containers/VendorDetails";
import Snackbar from "react-native-snackbar";
import RequestTripAdvanceStatus from "../../containers/RequestTripAdvanceStatus";
import AdvanceTripDetails from "../../containers/AdvanceTripDetails";
// icon settings initialIcon: 1 ='ok' , initialIcon: -1='error' , initialIcon: 0='warning';

class RequestTripAdvance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tripId: null,
      isLoading: false,
      vendorDetails: false,
      vendorIcon: -1,
      tripChargesIcon: -1,
      podIcon: -1,
      advancePaymentIcon: -1,
      vendorDetailsData: null,
      tripCharges: null,
      trip_id: null,
      tripData: null,
      vendorData: null,
      tripAdvance: null,
      vehiclesData: null,
      vendorDetailsMsg: null,
      tripChargesMsg: null,
      podMsg: null,
      advancePayMsg: null,
      vendorDetailsShow: false,
      showError: false,
      errorMsg: null,
      showTripDetails: false
    };
  }

  setTripId = id => {
    this.setState({ tripId: id });
  };

  getTripData = () => {
    this.setState({ isLoading: true });
    const { tripId, vehiclesData } = this.state;

    Keyboard.dismiss();

    const { requestTripAdvance, vendors } = this.props;

    const requestData = { trip_code: tripId };

    if (tripId === null) {
      this.setState({ isLoading: false });
      Snackbar.show({
        title: "Please enter Trip Code",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      requestTripAdvance(requestData)
        .then(response => {
          const { status, data, message } = response;
          if (status) {
            const {
              vendor_details,
              trip_advance,
              trip_charges,
              pod_history,
              trip_id,
              vendors,
              trips,
              vehicles
            } = data;

            const { icon: vDetailsIcon, msg: vMsg } = vendor_details;
            const { icon: tAdvanceIcon, msg: advanceMsg } = trip_advance;
            const { icon: tChargesIcon, msg: chargesMsg } = trip_charges;
            const { icon: podIcon, msg: podMsg } = pod_history;

            if (vMsg) {
              this.setState({ vendorDetailsMsg: vMsg });
            }

            if (advanceMsg) {
              this.setState({ advancePayMsg: advanceMsg });
            }

            if (podMsg) {
              this.setState({ podMsg });
            }

            if (chargesMsg) {
              this.setState({ tripChargesMsg: chargesMsg });
            }

            if (vDetailsIcon === "ok") {
              this.setState({ vendorIcon: 1 });
            } else if (vDetailsIcon === "warning") {
              this.setState({ vendorIcon: 0 });
            } else {
              this.setState({ vendorIcon: 2 });
            }

            if (tAdvanceIcon === "ok") {
              this.setState({ advancePaymentIcon: 1 });
            } else if (tAdvanceIcon === "warning") {
              this.setState({ advancePaymentIcon: 0 });
            } else {
              this.setState({ advancePaymentIcon: 2 });
            }

            if (tChargesIcon === "ok") {
              this.setState({ tripChargesIcon: 1 });
            } else if (tChargesIcon === "warning") {
              this.setState({ tripChargesIcon: 0 });
            } else {
              this.setState({ tripChargesIcon: 2 });
            }

            if (podIcon === "ok") {
              this.setState({ podIcon: 1 });
            } else if (podIcon === "warning") {
              this.setState({ podIcon: 0 });
            } else {
              this.setState({ podIcon: 2 });
            }

            this.setState({
              vendorDetailsData: vendor_details,
              tripCharges: trip_charges,
              trip_id,
              vendorData: vendors,
              tripData: trips,
              tripAdvance: trip_advance,
              vehiclesData: vehicles,
              vendorDetailsShow: true,
              showTripDetails: true
            });

            // this.setState({tripCharges: trip_charges});
          } else {
            this.setState({
              tripId: null,
              isLoading: false,
              vendorDetails: false,
              vendorIcon: -1,
              tripChargesIcon: -1,
              podIcon: -1,
              advancePaymentIcon: -1,
              vendorDetailsData: null,
              tripCharges: null,
              trip_id: null,
              tripData: null,
              vendorData: null,
              tripAdvance: null,
              vehiclesData: null,
              vendorDetailsMsg: null,
              tripChargesMsg: null,
              podMsg: null,
              advancePayMsg: null,
              vendorDetailsShow: false
            });

            return Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .catch(error => {
          console.log("error ----", error);
        })
        .finally(() => {
          this.setState({ isLoading: false });
        });
    }
  };

  showVendorDetails = value => {
    const { vendorIcon } = this.state;
    console.log("---- open details ----");

    this.setState({ vendorDetails: value });

    // if (vendorIcon === 1) {
    //   this.setState({vendorDetails: true});
    // } else {
    //   this.setState({vendorDetails: false});
    // }
  };

  closeVendorDetails = () => {
    this.setState({ vendorDetails: false });
  };

  goToTripCharges = () => {
    this.setState({ vendorDetails: false });
    this.props.navigation.navigate(NAVIGATE.ADD_TRIP_CHARGES, {
      tripCharges: this.state.tripCharges,
      tripData: this.state.tripData
    });
  };

  goToVendorDetails = () => {
    console.log("---- go to Vendor Details Tap ----");
    this.props.navigation.navigate(NAVIGATE.VENDOR_DETAILS, {
      vendorDetailsData: this.state.vendorDetailsData,
      vendorData: this.state.vendorData
    });
  };

  goToAdvancePayments = () => {
    this.setState({ vendorDetails: false });
    this.props.navigation.navigate(NAVIGATE.ADVANCE_PAYMENTS);
  };

  showErrorMsg = error => {
    Snackbar.show({
      title: error,
      duration: Snackbar.LENGTH_LONG
    });

    // this.setState({showError: true, errorMsg: error});
  };

  clearInput = () => {
    this.setState({ tripId: null });
  };

  onEndEditing = text => {
    this.setState({ tripId: text });
  };

  render() {
    const { vendorDetails } = this.state;

    return (
      <Fragment>
        <Header
          name={"REQUEST TRIP ADVANCE"}
          navigation={this.props.navigation}
          menu={true}
        />

        <View style={[styles.cardContainerStyle]}>
          <View style={styles.textInputView}>
            <TextInput
              placeholder={"Enter TripId"}
              style={styles.textInput}
              onChangeText={text => this.setState({ tripId: text })}
              value={this.state.tripId ? String(this.state.tripId) : ""}
              placeholderTextColor={colors.black25}
              autoCapitalize={"none"}
            />
          </View>

          <Button
            title={"SEARCH"}
            titleStyle={{
              fontSize: 15,
              fontFamily: "CircularStd-Book"
            }}
            onPress={this.getTripData}
            loading={this.state.isLoading}
            buttonStyle={{ margin: 10 }}
          />
        </View>

        {this.state.showTripDetails && (
          <View>
            {/*<Text*/}
            {/*  style={[*/}
            {/*    {*/}
            {/*      fontSize: 16,*/}
            {/*      color: colors.black65,*/}
            {/*      fontFamily: "CircularStd-Book",*/}
            {/*      padding: 5,*/}
            {/*      fontWeight: "300",*/}
            {/*      marginBottom: 5*/}
            {/*    }*/}
            {/*  ]}*/}
            {/*>*/}
            {/*  Trip Details*/}
            {/*</Text>*/}

            <AdvanceTripDetails showCustomer={true} showVendor={false} />
          </View>
        )}

        <RequestTripAdvanceStatus
          vendorDetailsShow={this.state.vendorDetailsShow}
          closeVendorDetails={this.closeVendorDetails}
          vendorDetailsTap={this.goToVendorDetails}
          tripChargesOnTap={this.goToTripCharges}
          tripAdvanceOnTap={this.goToAdvancePayments}
          showErrorMsg={this.showErrorMsg}
        />

        {vendorDetails && (
          <VendorDetails
            vendorDetailsData={this.state.vendorDetailsData}
            vendorData={this.state.vendorData}
          />
        )}

        {/*{this.state.showError && (*/}
        {/*  <Text>{this.state.errorMsg}</Text>*/}
        {/*)}*/}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  verticalLineStyle: {
    borderLeftColor: colors.black65,
    borderLeftWidth: 1,
    height: 30,
    alignSelf: "center",
    justifyContent: "center"
  },
  cardContainerStyle: {
    borderColor: "#ddd",
    borderWidth: 1,
    borderRadius: 2,
    borderBottomWidth: 1,
    marginLeft: 5,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    borderTopLeftRadius: 3,
    backgroundColor: colors.white,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3
  },
  textStyle: {
    fontSize: 14,
    color: colors.textcolor,
    marginTop: 3,
    fontFamily: "CircularStd-Book"
  },

  textInputView: {
    flexDirection: "row",
    marginTop: 16,
    marginBottom: 16,
    marginLeft: 10,
    marginRight: 10,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderBottomColor: colors.black15,
    borderTopColor: colors.black15,
    borderLeftColor: colors.black15,
    borderRightColor: colors.black15
  },
  textInput: {
    flex: 1,
    padding: 10,
    ...Platform.select({
      android: { paddingBottom: 2 }
    })
  }
});

export default RequestTripAdvance;
