import React, { Component, Fragment } from "react";
import Header from "../common/header";
import Snackbar from "react-native-snackbar";
import NumberInput from "../common/NumberInput";
import Footer from "../common/footer";
import { USER_RIGHTS, VEHICLE_CATEGORY_APPROVE } from "../../../constants";
import BookingSuccessModal from "../common/BookingSuccessModal";
import { ScrollView } from "react-navigation";
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../common/Colors";
import CityInput from "../common/CityInput";
import { hasRights } from "../common/checkRights";
import VehicleTypeInput from "../common/VehicleTypeInput";
import VendorInput from "../common/VendorsInput";
import { CheckBox } from "react-native-elements";
import customStyles from "../common/Styles";
import SelectInput from "react-native-select-input-ios";

class VehicleApprovalForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      vNumber: null,
      selectedVehicleType: 0,
      selectedDeployer: 0,
      maxPayload: null,
      maxVolume: null,
      phone: null,
      isLoading: false,
      isActive: false,
      roundTrip: false,
      vehicleCat: null,
      vehicleCatId: 0,
      deployerSame: false,
      selectedOwner: 0,
      selectedBaseLocation: 0,
      enableEditVehicleCat: false,
      ownerDetails: false,
      initialOwner: 0,
      initialDeployer: 0,
      initialCat: null,
      initalCatId: 0,
      enableEditDeployer: false,
      enableEditOwner: false,
      showDeployerOwnerCheck: false,
      showKnowOwnerCheck: false
    };
  }

  componentDidMount() {
    // Orientation.lockToPortrait();
    this.getVehicleData();

    const { loggedInUser } = this.props;
    console.log(
      "hasRights(USER_RIGHTS.EDIT_VEHICLE_CATEGORY, loggedInUser) ----",
      hasRights(USER_RIGHTS.EDIT_VEHICLE_CATEGORY, loggedInUser)
    );

    if (hasRights(USER_RIGHTS.EDIT_VEHICLE_CATEGORY, loggedInUser)) {
      this.setState({ enableEditVehicleCat: true });
    }
  }

  getVehicleData = () => {
    const { vehicles, vehicle_id, vehicleTypes, vendors } = this.props;

    const {
      basicInfo: {
        vehicle_number,
        vendor_id,
        vehicle_type_id,
        phone,
        vehicle_category,
        base_location_id,
        max_volume,
        max_payload,
        is_active,
        round_trip,
        owner_id,
        know_owner
      } = {}
    } = vehicles[vehicle_id] || {};

    console.log("owner_id -------------------------------", owner_id);
    const { loggedInUser } = this.props;

    if (vehicle_number) {
      this.setState({ vNumber: vehicle_number });
    }

    if (phone) {
      this.setState({ phone });
    }

    if (vehicle_type_id) {
      this.setState({ selectedVehicleType: vehicle_type_id });
    }

    if (vendor_id) {
      this.setState({
        selectedDeployer: vendor_id,
        initialDeployer: vendor_id
      });
    }

    if (base_location_id) {
      this.setState({ selectedBaseLocation: base_location_id });
    }

    if (vehicle_category) {
      this.setState({
        vehicleCat: vehicle_category,
        initialCat: vehicle_category
      });

      if (vehicle_category === "CON") {
        this.setState({ vehicleCatId: 1, initialCatId: 1 });
      } else if (vehicle_category === "ZAS") {
        this.setState({ vehicleCatId: 2, initialCatId: 2 });
      } else {
        this.setState({ vehicleCatId: 0, initialCatId: 0 });
      }
    }

    if (max_payload) {
      this.setState({ maxPayload: max_payload });
    }

    if (max_volume) {
      this.setState({ maxVolume: max_volume });
    }

    if (is_active) {
      this.setState({ isActive: is_active });
    }

    if (round_trip) {
      this.setState({ roundTrip: round_trip });
    }

    if (know_owner) {
      this.setState({ ownerDetails: know_owner });
    }

    if (owner_id) {
      if (vehicle_category === "CON") {
        console.log("---- contract if ----");
        if (vendor_id === owner_id) {
          console.log("---- same ownerid and vendor id ----");
          this.setState({ deployerSame: true });
        } else {
          console.log("---- different owner and vendor id  ----");
          this.setState({ deployerSame: false, selectedOwner: owner_id });
        }
      } else if (vehicle_category === "ZAS") {
        console.log("---- vehicle catgeory zast ----");
        this.setState({
          ownerDetails: true,
          selectedOwner: owner_id,
          deployerSame: true
        });
      }
    } else {
      console.log("---- ownerid null ----");
      if (vehicle_category === "CON") {
        this.setState({ deployerSame: false });
      } else if (vehicle_category === "ZAS") {
        console.log("---- vehicle cat zast  ----");
        this.setState({ ownerDetails: false, deployerSame: true });
      }
    }

    // if ((owner_id !== null || owner_id !== undefined) && vehicle_category === "CON") {
    //   console.log('---- owner details and category con ----');
    //   if (vendor_id === owner_id) {
    //     console.log("---- vendor and owner are same ----");
    //   } else {
    //     console.log("---- vendor and owner are different ----");
    //   }
    // } else if ((owner_id !== null || owner_id !== undefined) && vehicle_category === "ZAS") {
    //   console.log('---- owner id and category zast ----');
    // } else {
    // }

    if (hasRights(USER_RIGHTS.EDIT_VEHICLE_CATEGORY, loggedInUser)) {
      if (vehicle_category === "CON") {
        console.log("---- vehicle catgerogy con ----");
        this.setState(
          {
            enableEditDeployer: true,
            enableEditOwner: true,
            showSameDeployerOwnerCheck: true
          },
          () =>
            console.log(
              "deployer enable----------------------" +
                this.state.enableEditDeployer
            )
        );
      } else if (vehicle_category === "ZAS") {
        this.setState({ enableEditOwner: true, showKnowOwnerCheck: true });
      }
    }

    // if (owner_id) {
    //   if (vehicle_category === "CON" && vendor_id === owner_id) {
    //     console.log("---- vendor and owner are same ----");
    //     this.setState({deployerSame: true});
    //   } else {
    //     console.log("---- vendor and owner are different ----");
    //     this.setState({deployerSame: false, selectedOwner: owner_id});
    //   }
    //
    //   if (vehicle_category === "ZAS") {
    //     console.log("---- if zast category ----");
    //     this.setState({
    //       ownerDetails: true,
    //       selectedOwner: owner_id,
    //       deployerSame: true
    //     });
    //   } else {
    //     console.log("---- else zast catgeory ----");
    //     this.setState({deployerSame: true, ownerDetails: false});
    //   }
    // }
  };

  selectVehicleType = vehicleType => {
    this.setState({
      selectedVehicleType: vehicleType
    });
  };

  updateVNumber = vNumber => {
    this.setState({ vNumber });
  };

  updateMaxPayload = payload => {
    this.setState({ maxPayload: payload });
  };

  updateMaxVolume = volume => {
    this.setState({ maxVolume: volume });
  };

  selectDeployer = vendor => {
    this.setState({ selectedDeployer: vendor, selectedOwner: 0 });
  };

  selectOwner = vendor => {
    this.setState({ selectedOwner: vendor });
  };

  checkRoundTrip = () => {
    if (this.state.roundTrip) {
      this.setState({ roundTrip: false });
    } else {
      this.setState({ roundTrip: true });
    }
  };

  checkActive = () => {
    if (this.state.isActive) {
      this.setState({ isActive: false });
    } else {
      this.setState({ isActive: true });
    }
  };

  selectBaseLocation = city => {
    this.setState({ selectedBaseLocation: city });
  };

  updatePhone = phone => {
    this.setState({ phone });
  };

  getVehicleCategory = () => {
    const options = [];
    VEHICLE_CATEGORY_APPROVE.forEach((type, index) => {
      options.push({
        label: type,
        value: index
      });
    });

    return options;
  };

  setVehicleCategory = id => {
    console.log("id ---------------", id);
    const { loggedInUser } = this.props;

    let name = VEHICLE_CATEGORY_APPROVE[id];
    if (id === 1) {
      if (hasRights(USER_RIGHTS.EDIT_VEHICLE_CATEGORY, loggedInUser)) {
        this.setState({
          enableEditDeployer: true,
          enableEditOwner: true,
          showSameDeployerOwnerCheck: true
        });
      }
      this.setState({
        vehicleCatId: id,
        vehicleCat: "CON",
        showDeployerOwnerCheck: true,
        selectedOwner: 0
      });
    } else if (id === 2) {
      console.log("---- category change ----");
      if (hasRights(USER_RIGHTS.EDIT_VEHICLE_CATEGORY, loggedInUser)) {
        this.setState({ enableEditOwner: true, showKnowOwnerCheck: true });
      }

      this.setState(
        {
          vehicleCatId: id,
          vehicleCat: "ZAS",
          showKnowOwnerCheck: true,
          selectedDeployed: 0,
          deployerSame: true,
          selectedOwner: 0,
          selectedDeployer: 0
        },
        () => {
          console.log(
            "vehicleCat --------------------------------------------------" +
              this.state.vehicleCat
          );
        }
      );
    } else {
      this.setState({ vehicleCatId: id, vehicleCat: name });
    }
  };

  deployerValue = () => {
    if (this.state.deployerSame) {
      this.setState({ deployerSame: false });
    } else {
      this.setState({ deployerSame: true, selectedOwner: 0 });
    }
  };

  knowOwnerDetails = () => {
    if (this.state.ownerDetails) {
      this.setState({ ownerDetails: false, selectedOwner: 0 });
    } else {
      this.setState({ ownerDetails: true });
    }
  };

  getErrors = () => {
    let message = null;
    let status = false;
    const {
      vNumber,
      selectedDeployer,
      selectedVehicleType,
      vehicleCat,
      ownerDetails,
      selectedOwner,
      deployerSame,
      selectedBaseLocation,
      vehicleCatId
    } = this.state;

    if (!vNumber) {
      message = "Please Enter Vehicle Number";
      status = true;
    } else if (vehicleCat === "CON" && !selectedDeployer) {
      message = "Please select Deployer";
      status = true;
    } else if (!selectedVehicleType) {
      message = "Please select Vehicle Type";
      status = true;
    } else if (!vehicleCat) {
      message = "Please select Vehicle Category";
      status = true;
    } else if (vehicleCatId === 1 && !deployerSame && !selectedOwner) {
      message = "Please select Owner";
      status = true;
    } else if (vehicleCatId === 2 && ownerDetails && !selectedOwner) {
      message = "Please select Owner";
      status = true;
    } else if (vehicleCat === "CON" && !selectedBaseLocation) {
      message = "Please select Base location";
      status = true;
    } else if (vehicleCatId === 0) {
      message = "Please select Vehicle Category";
      status = true;
    }
    if (message) {
      Snackbar.show({
        title: message,
        duration: Snackbar.LENGTH_LONG
      });
    }
    return status;
  };

  vehicleApproval = () => {
    console.log("---- APPROVE VEHICLE ----");
    const errors = this.getErrors();

    const { vehicle_id } = this.props;

    if (errors === false) {
      this.setState({ isLoading: true });
      const {
        vNumber: vehicle_number,
        selectedDeployer: vendor_id,
        selectedVehicleType: vehicle_type_id,
        vehicleCat: vehicle_category,
        phone: phone,
        isActive: is_active,
        selectedBaseLocation: base_location_id,
        roundTrip: round_trip,
        deployerSame: same_owner,
        selectedOwner: owner_id,
        maxPayload: max_payload,
        maxVolume: max_volume,
        ownerDetails: know_owner
      } = this.state;

      let request_data;

      if (same_owner && vehicle_category === "CON") {
        console.log("---- same owner ----");
        request_data = {
          vehicle_id,
          vehicle_type_id,
          vehicle_number,
          vendor_id,
          vehicle_category,
          phone,
          is_active,
          base_location_id,
          round_trip,
          same_owner,
          max_payload,
          max_volume,
          know_owner
        };
      } else if (know_owner && vehicle_category === "ZAS") {
        console.log("---- knowDetails ----");
        request_data = {
          vehicle_id,
          vehicle_type_id,
          vehicle_number,
          vendor_id,
          vehicle_category,
          phone,
          is_active,
          base_location_id,
          round_trip,
          max_payload,
          max_volume,
          know_owner,
          owner_id
        };
      } else {
        request_data = {
          vehicle_id,
          vehicle_type_id,
          vehicle_number,
          vendor_id,
          vehicle_category,
          phone,
          is_active,
          base_location_id,
          round_trip,
          owner_id,
          same_owner,
          max_payload,
          max_volume
        };
      }

      console.log("request_data ----------------------", request_data);

      const { editVehicleDetails } = this.props;
      editVehicleDetails(request_data)
        .then(result => {
          const { status, message } = result;

          if (status) {
            this.setState({ showModal: message });
            Snackbar.show({
              title: "Vehicle Successfully Edited",
              duration: Snackbar.LENGTH_LONG
            });
            this.goBack();
          } else {
            Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .finally(() => this.setState({ isLoading: false }));
    }
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    const { vNumber, maxPayload, maxVolume, phone } = this.state;
    const { loggedInUser } = this.props;

    return (
      <Fragment>
        <Header
          name={
            hasRights(USER_RIGHTS.EDIT_VEHICLE_CATEGORY, loggedInUser)
              ? "APPROVE"
              : "SAVE"
          }
          navigation={this.props.navigation}
          goBack={true}
        />

        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <NumberInput
            numericProp={false}
            message={"Vehicle Number"}
            updateText={this.updateVNumber}
            placeholder={"Vehicle Number"}
            initialValue={vNumber}
          />

          <VehicleTypeInput
            vehiclesTypes={this.props.vehicleTypes}
            selectedVehicle={this.state.selectedVehicleType}
            selectVehicle={this.selectVehicleType}
            selectOne={true}
            textCenter={true}
          />

          <View
            style={
              this.state.enableEditVehicleCat
                ? [customStyles.border]
                : [customStyles.borderDisabled]
            }
          >
            <Text
              style={
                this.state.enableEditVehicleCat
                  ? customStyles.labelText
                  : customStyles.labelTextDisabled
              }
            >
              Vehicle Category
            </Text>

            <SelectInput
              enabled={this.state.enableEditVehicleCat}
              style={
                Platform.OS === "ios" ? customStyles.pickerView : { height: 30 }
              }
              labelStyle={{
                color:
                  this.state.vehicleCatId === 0 ||
                  this.state.enableEditVehicleCat === false
                    ? colors.black25
                    : colors.black85,
                ...Platform.select({
                  android: {
                    height: 30
                  },
                  ios: {
                    alignSelf: "flex-start"
                  }
                })
              }}
              options={this.getVehicleCategory()}
              mode={"dropdown"}
              value={this.state.vehicleCatId}
              onSubmitEditing={
                this.state.enableEditVehicleCat ? this.setVehicleCategory : null
              }
            />
          </View>

          {this.state.vehicleCat === "CON" ? (
            <View>
              <VendorInput
                vendors={this.props.vendors}
                selectedVendor={this.state.selectedDeployer}
                selectVendor={this.selectDeployer}
                vehicleApprovalScreen={true}
                title={"Deployer"}
                disabled={!this.state.enableEditDeployer}
              />
              {this.state.showSameDeployerOwnerCheck ? (
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 10
                  }}
                >
                  <CheckBox
                    containerStyle={{ padding: 0, margin: 0 }}
                    right
                    checked={this.state.deployerSame}
                    iconType="material-community"
                    checkedIcon="checkbox-marked-outline"
                    uncheckedIcon="checkbox-blank-outline"
                    checkedColor={colors.darkblue}
                    uncheckColor={colors.blue}
                    onPress={this.deployerValue}
                  />

                  <Text
                    style={{
                      fontSize: 14,
                      color: colors.black85,
                      justifyContent: "center",
                      alignSelf: "center"
                    }}
                  >
                    IS THE DEPLOYER AND OWNER SAME
                  </Text>
                </View>
              ) : null}
            </View>
          ) : null}

          {this.state.vehicleCat === "ZAS" ? (
            <View>
              {this.state.showKnowOwnerCheck ? (
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 10
                  }}
                >
                  <CheckBox
                    containerStyle={{ padding: 0, margin: 0 }}
                    right
                    checked={this.state.ownerDetails}
                    iconType="material-community"
                    checkedIcon="checkbox-marked-outline"
                    uncheckedIcon="checkbox-blank-outline"
                    checkedColor={colors.darkblue}
                    uncheckColor={colors.blue}
                    onPress={this.knowOwnerDetails}
                  />

                  <Text
                    style={{
                      fontSize: 14,
                      color: colors.black85,
                      justifyContent: "center",
                      alignSelf: "center"
                    }}
                  >
                    Do you know Owner Details ?
                  </Text>
                </View>
              ) : null}

              {this.state.ownerDetails && (
                <VendorInput
                  vendors={this.props.vendors}
                  selectedVendor={this.state.selectedOwner}
                  selectVendor={this.selectOwner}
                  vehicleApprovalScreen={true}
                  title={"Owner"}
                />
              )}
            </View>
          ) : null}

          {!this.state.deployerSame ? (
            <VendorInput
              vendors={this.props.vendors}
              selectedVendor={this.state.selectedOwner}
              selectVendor={this.selectOwner}
              vehicleApprovalScreen={true}
              title={"Owner"}
              disabled={!this.state.enableEditOwner}
            />
          ) : null}

          <NumberInput
            numericProp={true}
            message={"Max Payload"}
            updateText={this.updateMaxPayload}
            placeholder={"Max Payload"}
            initialValue={maxPayload}
          />

          <NumberInput
            numericProp={true}
            message={"Max Volume"}
            updateText={this.updateMaxVolume}
            placeholder={"Max Volume"}
            initialValue={maxVolume}
          />

          <NumberInput
            numericProp={true}
            message={"Phone"}
            updateText={this.updatePhone}
            placeholder={"Phone"}
            initialValue={phone}
          />

          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedCity={this.state.selectedBaseLocation}
            selectCity={this.selectBaseLocation}
            message={"Base Location City"}
          />

          {this.state.vehicleCat !== "ZAS" && (
            <TouchableOpacity
              style={{
                flexDirection: "row",
                marginTop: 5,
                marginBottom: 5,
                alignItems: "center"
              }}
              onPress={this.checkRoundTrip}
            >
              <CheckBox
                containerStyle={{ padding: 0, margin: 0 }}
                right
                checked={this.state.roundTrip}
                iconType="material-community"
                checkedIcon="checkbox-marked-outline"
                uncheckedIcon="checkbox-blank-outline"
                checkedColor={colors.darkblue}
                uncheckColor={colors.blue}
                onPress={this.checkRoundTrip}
              />
              <Text
                style={{
                  fontSize: 14,
                  color: colors.black85,
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              >
                Round Trip
              </Text>
            </TouchableOpacity>
          )}

          <TouchableOpacity
            style={{
              flexDirection: "row",
              marginTop: 10,
              marginBottom: 10,
              alignItems: "center"
            }}
            onPress={this.checkActive}
          >
            <CheckBox
              containerStyle={{ padding: 0, margin: 0 }}
              right
              checked={this.state.isActive}
              iconType="material-community"
              checkedIcon="checkbox-marked-outline"
              uncheckedIcon="checkbox-blank-outline"
              checkedColor={colors.darkblue}
              uncheckColor={colors.blue}
              onPress={this.checkActive}
            />
            <Text
              style={{
                fontSize: 14,
                color: colors.black85,
                justifyContent: "center",
                alignSelf: "center"
              }}
            >
              Is Active
            </Text>
          </TouchableOpacity>
        </ScrollView>

        <Footer
          name={
            hasRights(USER_RIGHTS.EDIT_VEHICLE, loggedInUser)
              ? "SAVE / APPROVE"
              : "EDIT"
          }
          action={this.vehicleApproval}
          loading={this.state.isLoading}
        />

        <BookingSuccessModal
          isVisible={!!this.state.showModal}
          message={this.state.showModal}
          closeModal={this.goToBookings}
        />
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  labelText: {
    marginTop: -10,
    color: colors.blue,
    marginLeft: 10,
    backgroundColor: colors.white,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  textInputStyle: {
    flex: 1,
    alignItems: "flex-start",
    paddingLeft: 10,
    justifyContent: "center",
    ...Platform.select({
      ios: { maxHeight: 30, paddingBottom: 10 },
      android: {
        maxHeight: 30,
        paddingTop: 0,
        paddingBottom: 0,
        textAlignVertical: "center"
      }
    })
  },
  border: {
    borderWidth: 1,
    borderRadius: 3,
    borderColor: colors.blue,
    margin: 10
  }
});

export default VehicleApprovalForm;
