import React, { Component, Fragment } from "react";
import {
  Linking,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import Header from "../common/header";
import colors from "../common/Colors";
import { OS } from "../../../constants";

class ForcedUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  openPlayStore = () => {
    const link =
      Platform.OS === OS.IOS
        ? "https://apps.apple.com/in/app/zast-onmove/id1485521987"
        : "market://details?id=com.zastlogisolutions.zastlogics.bookingapplication";

    console.log("link ----", link);
    Linking.canOpenURL(link).then(
      supported => {
        console.log("supported ----", supported);
        supported && Linking.openURL(link);
      },
      err => console.log(err)
    );
  };

  render() {
    return (
      <Fragment>
        <Header
          name={"UPDATE"}
          navigation={this.props.navigation}
          goBack={true}
        />

        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <View style={{ padding: 16 }}>
            <Text style={{ color: colors.red }}>
              Please update the app to continue.
            </Text>
          </View>

          <TouchableOpacity style={styles.button} onPress={this.openPlayStore}>
            <Text style={{ color: colors.red }}>Update Now</Text>
          </TouchableOpacity>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    padding: 8,
    borderWidth: 2,
    borderColor: colors.red,
    borderRadius: 4
  }
});

export default ForcedUpdate;
