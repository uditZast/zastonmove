import React, { Component, Fragment } from "react";
import { View, Text, TouchableOpacity, ScrollView } from "react-native";
import Header from "../common/header";
import OfflineBanners from "../common/OfflineBanner";
import customStyles from "../common/Styles";
import { SearchBar } from "react-native-elements";
import { doRequest } from "../../helpers/network";
import { REQUEST_TYPE } from "../../../constants";
import { Trips } from "../../helpers/Urls";
import colors from "../common/Colors";

class LocationSearch extends Component {
  constructor(props) {
    super(props);
    this.state = { input: null, loading: false, search_results: [] };
    this.timeout = 0;
  }

  fetchLocations = async () => {
    const { input } = this.state;
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      const request = doRequest({
        method: REQUEST_TYPE.GET,
        url: Trips.fetchLocationsURL(input),
        isMAPI: true
      })
        .then(response => {
          console.log("response --1111111--", response);
          const { status, data: { search_results } = {} } = response;
          if (status) {
            this.setState({ search_results, loading: false });
          }
        })
        .catch(error => {
          this.setState({ loading: false });
          console.log("err during fetching facilities-----------", error);
        });
    }, 600);
  };

  setSearch = text => {
    this.setState({ input: text, loading: true }, this.fetchLocations);
  };

  clearInput = () => {
    this.setState({ input: null });
  };

  updateLocationInMap = (lat, long, name) => () => {
    const {
      navigation: { state: { params: { updateLocation } = {} } = {} } = {}
    } = this.props;
    if (updateLocation) {
      updateLocation({ latitude: lat, longitude: long, location: name }).then(
        () => this.props.navigation.goBack()
      );
    }
  };

  render() {
    const { isConnected } = this.props;
    const { search_results = [] } = this.state;
    return (
      <Fragment>
        <Header
          name="SEARCH LOCATION"
          navigation={this.props.navigation}
          goBack={true}
        />
        <View style={{ flex: 1 }}>
          <SearchBar
            placeholder="Search Place"
            onChangeText={this.setSearch}
            lightTheme
            containerStyle={customStyles.searchBarContainer}
            value={this.state.input}
            onCancel={this.clearInput}
            onClear={this.clearInput}
            showLoading={this.state.loading}
          />
          <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            {search_results.map((location, index) => {
              const { name, lat, long } = location;
              return (
                <Fragment key={index}>
                  <TouchableOpacity
                    style={{
                      padding: 16,
                      // paddingTop: index === 0 ? 16: 0,
                      borderBottomColor: colors.black25,
                      borderBottomWidth: 1
                    }}
                    onPress={this.updateLocationInMap(lat, long, name)}
                  >
                    <Text>{name}</Text>
                  </TouchableOpacity>
                </Fragment>
              );
            })}
          </ScrollView>
        </View>
        {!isConnected && <OfflineBanners bottom={0} />}
      </Fragment>
    );
  }
}

export default LocationSearch;
