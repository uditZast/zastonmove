import React, { Component, Fragment } from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import moment from "moment";
import { Icon } from "react-native-elements";
import Header from "../common/header";
import Snackbar from "react-native-snackbar";
import CustomerInput from "../common/CustomerInput";
import VehicleInput from "../common/VehicleInput";
import VehicleTypeInput from "../common/VehicleTypeInput";
import VendorInput from "../common/VendorsInput";
import DateTimeInput from "../common/DateTimeInput";
import NumberInput from "../common/NumberInput";
import CityInput from "../common/CityInput";
import ViaCityInput from "../common/ViaCityInput";
import Footer from "../common/footer";
import { AUTO_FILL_TAT, BOOKING_TYPE, NAVIGATE } from "../../../constants";
import colors from "../common/Colors";
import customStyles from "../common/Styles";
import EngagedByInput from "../../containers/EngagedByInput";
import DistanceInputNew from "../common/DistanceInputNew";
import DistanceInputNewReturn from "../common/DistanceInputNewReturn";

let count, returnCount;

class EditBookingTwoWay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCustomer: 0,
      selectedVehicle: 0,
      selectedVehicleType: 0,
      selectedVendor: 0,
      forwardDateTime: new moment(),
      returnDateTime: new moment(),
      driverPhoneNumber: null,
      forwardSourceCity: 0,
      forwardDestinationCity: 0,
      forwardViaCities: [],
      forwardExpectedTat: 0,
      returnSourceCity: 0,
      returnDestinationCity: 0,
      returnViaCities: [],
      returnExpectedTat: 0,
      return_booking_id: 0,

      selectedEngagedBy: null,
      vendorDisabled: true,
      vehicleDisabled: false,
      vehicleTypeDisabled: true,
      customerDisable: false,
      forwardSourceCityDisabled: false,
      forwardDestinationCityDisabled: false,
      returnSourceCityDisabled: false,
      returnDestinationCityDisabled: false,
      bookingType: null,
      comment: null,
      forwardExpectedTatDisable: false,
      distance: 0,
      disableDistance: false,
      bookingStatus: null,
      forwardDistance: 0,
      returnDistance: 0,
      forwardDistanceEnable: true,
      returnDistanceEnable: true,
      dockHours: 0,
      forwardSourceCityDisable: true,
      forwardDestinationCityDisable: true,
      returnSourceCityDisable: true,
      returnDestinationCityDisable: true,
      selectedEngageReason: 0,
      returnDockHours: 0,
      forwardDockHours: 0,
      forwardDateFlag: false,
      returnDateFlag: false,
      forwardEnableTat: false,
      forwardEnableDistance: false,
      returnEnableTat: false,
      returnEnableDistance: false,
      apiHit: false,
      apiHitReturn: false,
      forwardReference: null,
      returnReference: null,
      creatingBooking: false
    };
  }

  componentDidMount() {
    count = 0;
    returnCount = 0;

    this.getBookingData();
  }

  getBookingData = () => {
    const { fetchBookingData, booking_id } = this.props;

    fetchBookingData(booking_id)
      .then(response => {
        const { status = false } = response;

        if (status) {
          const { bookings, booking_id } = this.props;
          const {
            basicInfo: {
              vehicle_id,
              vehicle_type_id,
              expected_tat,
              source_city_id,
              destination_city_id,
              customer_id,
              trip_time,
              vendor_id,
              phone,
              type,
              return_booking_id,
              engaged_by_id,
              disable_tat,
              distance,
              disable_distance,
              via_cities,
              enable_tat,
              enable_distance,
              reference
            } = {}
          } = bookings[booking_id];

          const { status } = bookings[booking_id];

          if (type === "SCHF" || (type === "SCHR" && status === "DK")) {
            this.setState({ customerDisable: true });
          }

          if (
            type === "AHF" ||
            type === "AHR" ||
            type === "SCHF" ||
            (type === "SCHR" && status === "DK") ||
            status === "CL"
          ) {
            this.setState({
              sourceCityDisable: true,
              destinationCityDisable: true
            });
          }

          if (status !== "RQ") {
            this.setState({
              vendorDisabled: true,
              vehicleDisabled: true,
              vehicleTypeDisabled: true
            });
          }

          if (this.props.action === "accept") {
            this.setState({ customerDisable: true });
          }

          if (
            type === "AHF" ||
            type === "AHR" ||
            type === "SCHF" ||
            (type === "SCHR" && this.props.action === "accept")
          ) {
            this.setState({
              forwardSourceCityDisable: false,
              forwardDestinationCityDisable: false,
              returnSourceCityDisable: false,
              returnDestinationCityDisable: false
            });
          }

          if (type === "SCHF" || (type === "SCHR" && type === "RQ")) {
            this.setState({
              forwardSourceCityDisable: true,
              forwardDestinationCityDisable: true,
              returnSourceCityDisable: true,
              returnDestinationCityDisable: true
            });
          }

          if (
            type === "SCHF" ||
            (type === "SCHR" && this.props.action === "accept")
          ) {
            this.setState({
              vehicleDisabled: false,
              vendorDisabled: false,
              vehicleTypeDisabled: false
            });
          } else {
            this.setState({ vehicleDisable: false });
          }

          if (via_cities !== undefined) {
            this.setState({ forwardViaCities: via_cities });
          } else {
            this.setState({ forwardViaCities: [] });
          }

          if (reference) {
            this.setState({ forwardReference: reference });
          }

          count++;
          this.setState({
            selectedVehicle: vehicle_id,
            selectedCustomer: customer_id,
            selectedVendor: vendor_id,
            forwardSourceCity: source_city_id,
            forwardDestinationCity: destination_city_id,
            driverPhoneNumber: phone,
            forwardDateTime: new moment(trip_time),
            forwardExpectedTat: expected_tat,
            selectedVehicleType: vehicle_type_id,
            selectedEngagedBy: engaged_by_id,

            forwardExpectedTatDisable: disable_tat,
            forwardDistance: distance,
            forwardDisableDistance: disable_distance,
            return_booking_id: return_booking_id,
            bookingType: type,
            forwardDateFlag: true,
            forwardEnableTat: enable_tat,
            forwardEnableDistance: enable_distance,
            apiHit: true
          });

          this.getReturnBooking(return_booking_id);
        } else {
          Snackbar.show({
            title: "" + response.message,
            duration: Snackbar.LENGTH_SHORT
          });
        }
      })
      .catch(error => {
        console.log(error);
        Snackbar.show({
          title: "Could not fetch details",
          duration: Snackbar.LENGTH_SHORT
        });
      });
  };

  getReturnBooking = return_booking_id => {
    const { fetchBookingData } = this.props;

    fetchBookingData(return_booking_id)
      .then(response => {
        const { status = false } = response;

        if (status) {
          const { bookings } = this.props;
          const {
            basicInfo: {
              expected_tat,
              source_city_id,
              destination_city_id,
              trip_time,
              phone,
              enable_tat,
              enable_distance,
              distance,
              via_cities,
              reference
            } = {}
          } = bookings[return_booking_id];

          if (via_cities !== undefined) {
            this.setState({ returnViaCities: via_cities });
          } else {
            this.setState({ returnViaCities: [] });
          }
          if (reference) {
            this.setState({ returnReference: reference });
          }

          returnCount++;
          this.setState({
            returnSourceCity: source_city_id,
            returnDestinationCity: destination_city_id,
            returnDateTime: new moment(trip_time),
            returnExpectedTat: expected_tat,
            returnDistanceEnable: enable_distance,
            returnDistance: distance,
            returnDateFlag: true,
            returnEnableTat: enable_tat,
            returnEnableDistance: enable_distance,
            apiHitReturn: true
          });
        } else {
          Snackbar.show({
            title: "" + response.message,
            duration: Snackbar.LENGTH_SHORT
          });
        }
      })
      .catch(error => {
        Snackbar.show({
          title: "Could not fetch details",
          duration: Snackbar.LENGTH_SHORT
        });
      });
  };

  selectCustomer = selectedCustomer => {
    this.setState({ selectedCustomer });
  };

  selectVehicle = vehicle => {
    count++;
    returnCount++;

    const { vehicles } = this.props;
    const { basicInfo: { vehicle_type_id } = {} } = vehicles[vehicle] || {};
    this.setState({
      selectedVehicle: vehicle,
      selectedVehicleType: vehicle_type_id
    });
  };

  selectVehicleType = vehicleType => {
    count++;
    returnCount++;
    this.setState({
      selectedVehicleType: vehicleType,
      selectedVehicle: 0,
      selectedVendor: 0
    });
  };

  selectVendor = vendor => {
    this.setState({ selectedVendor: vendor });
  };

  setForwardDate = dateTime => {
    this.setState({ forwardDateTime: dateTime, forwardDateFlag: true });
  };

  setReturnDate = dateTime => {
    this.setState({ returnDateTime: dateTime, returnDateFlag: true });
  };

  updateDriverNumber = phoneNumber => {
    this.setState({ driverPhoneNumber: phoneNumber });
  };

  updateForwardTAT = (forwardExpectedTat, forwardDistance) => {
    this.setState({
      forwardExpectedTat: forwardExpectedTat,
      forwardDistance: forwardDistance
    });
  };

  updateReturnTAT = (returnExpectedTat, returnDistance) => {
    this.setState({
      returnExpectedTat: returnExpectedTat,
      returnDistance: returnDistance
    });
  };

  updateForwardDistance = forwardDistance => {
    this.setState({ forwardDistance });
  };

  updateReturnDistance = returnDistance => {
    this.setState({ returnDistance });
  };

  selectForwardSourceCity = city => {
    count++;
    const { forwardDestinationCity, forwardViaCities } = this.state;
    const addedInVia = forwardViaCities.indexOf(city) === -1;
    if (`${city}` !== `${forwardDestinationCity}` && addedInVia) {
      this.setState({ forwardSourceCity: city });
    } else {
      // this.setState({ forwardSourceCity: 0 });
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_SHORT
      // });
    }
  };

  selectReturnSourceCity = city => {
    count++;
    returnCount++;
    const {
      returnDestinationCity,
      returnViaCities,
      forwardSourceCity,
      forwardViaCities
    } = this.state;
    const addedInVia =
      returnViaCities.indexOf(city) === -1 &&
      forwardViaCities.indexOf(city) === -1;
    const isNotSourceOfForward = `${city}` !== `${forwardSourceCity}`;
    if (
      `${city}` !== `${returnDestinationCity}` &&
      addedInVia &&
      isNotSourceOfForward
    ) {
      this.setState({ returnSourceCity: city, forwardDestinationCity: city });
    } else {
      // this.setState({ returnSourceCity: 0 });
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_SHORT
      // });
    }
  };

  selectForwardDestinationCity = city => {
    count++;
    returnCount++;
    const {
      forwardSourceCity,
      forwardViaCities,
      returnViaCities,
      returnDestinationCity
    } = this.state;
    const addedInVia =
      forwardViaCities.indexOf(city) === -1 &&
      returnViaCities.indexOf(city) === -1;
    const isNotDestinationOfReturn = `${city}` !== `${returnDestinationCity}`;
    if (
      `${forwardSourceCity}` !== `${city}` &&
      isNotDestinationOfReturn &&
      addedInVia
    ) {
      this.setState({ forwardDestinationCity: city, returnSourceCity: city });
    } else {
      // this.setState({ forwardDestinationCity: 0 });
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_SHORT
      // });
    }
  };

  selectReturnDestinationCity = city => {
    returnCount++;
    const { returnSourceCity, returnViaCities } = this.state;
    const addedInVia = returnViaCities.indexOf(city) === -1;
    if (`${returnSourceCity}` !== `${city}` && addedInVia) {
      this.setState({ returnDestinationCity: city });
    } else {
      // this.setState({ returnDestinationCity: 0 });
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_SHORT
      // });
    }
  };

  addForwardSelectedViaCity = cityId => {
    count++;
    const {
      forwardViaCities = [],
      forwardSourceCity,
      forwardDestinationCity
    } = this.state;
    if (
      forwardViaCities.indexOf(cityId) === -1 &&
      forwardSourceCity !== cityId &&
      forwardDestinationCity !== cityId
    ) {
      this.setState(prevState => {
        const { forwardViaCities } = prevState;
        const newViaCities = [...forwardViaCities];
        newViaCities.push(cityId);
        return { forwardViaCities: newViaCities };
      });

      // this.setState(prevState => {
      //   const {forwardViaCities} = prevState;
      //   return forwardViaCities.push(cityId);
      // });
    } else {
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_SHORT
      // });
    }
  };

  // removeForwardSelectedViaCity = index => {
  //   count++;
  //   this.setState(prevState => {
  //     const {forwardViaCities} = prevState;
  //     forwardViaCities.splice(index, 1);
  //     return forwardViaCities;
  //   });
  // };

  removeForwardSelectedViaCity = index => {
    count++;
    this.setState(prevState => {
      const { forwardViaCities } = prevState;
      const newViaCities = [...forwardViaCities];
      newViaCities.splice(index, 1);
      return { forwardViaCities: newViaCities };
    });
  };

  addReturnSelectedViaCity = cityId => {
    returnCount++;
    const {
      returnViaCities = [],
      returnSourceCity,
      returnDestinationCity
    } = this.state;
    if (
      returnViaCities.indexOf(cityId) === -1 &&
      returnSourceCity !== cityId &&
      returnDestinationCity !== cityId
    ) {
      this.setState(prevState => {
        const { returnViaCities } = prevState;
        const newViaCities = [...returnViaCities];
        newViaCities.push(cityId);
        return { returnViaCities: newViaCities };
      });

      // this.setState(prevState => {
      //   const {returnViaCities} = prevState;
      //   return returnViaCities.push(cityId);
      // });
    } else {
      setTimeout(() => {
        Snackbar.show({
          title: "Cannot choose same city again.",
          duration: Snackbar.LENGTH_LONG
        });
      }, 500);

      // Snackbar.show({
      //   title: "Cannot choose same city again.",
      //   duration: Snackbar.LENGTH_SHORT
      // });
    }
  };

  // removeReturnSelectedViaCity = index => {
  //   returnCount++;
  //   this.setState(prevState => {
  //     const {returnViaCities} = prevState;
  //     returnViaCities.splice(index, 1);
  //     return returnViaCities;
  //   });
  // };

  removeReturnSelectedViaCity = index => {
    returnCount++;
    this.setState(prevState => {
      const { returnViaCities } = prevState;
      const newViaCities = [...returnViaCities];
      newViaCities.splice(index, 1);
      return { returnViaCities: newViaCities };
    });
  };

  setEngageReason = selectedEngageReason => {
    this.setState({ selectedEngageReason });
  };

  getErrors = () => {
    let message = null;
    let status = false;
    const {
      selectedCustomer,
      selectedVehicle,
      selectedVehicleType,
      selectedVendor,
      forwardDateTime,
      returnDateTime,
      forwardSourceCity,
      forwardDestinationCity,
      forwardExpectedTat,
      forwardDistance,
      returnSourceCity,
      returnDestinationCity,
      returnExpectedTat,
      returnDistance,
      forwardDockHours,
      returnDockHours,
      forwardDateFlag,
      returnDateFlag
    } = this.state;

    console.log("this.props.action -----------------", this.props.action);
    if (!selectedCustomer) {
      message = "Please Select Customer";
      status = true;
    } else if (!selectedVehicleType || selectedVehicleType === 0) {
      message = "Please Select Vehicle Type";
      status = true;
    } else if (selectedVehicle && (!selectedVendor || selectedVendor === 0)) {
      message = "Please Select Vendor";
      status = true;
    } else if (forwardDateFlag === false || returnDateFlag === false) {
      message = "Please Select Date and Time of both trips";
      status = true;
    } else if (!forwardSourceCity || !returnSourceCity) {
      message = "Please Select Source City";
      status = true;
    } else if (!forwardDestinationCity || !returnDestinationCity) {
      message = "Please Select Destination City";
      status = true;
    } else if (!forwardExpectedTat || !returnExpectedTat) {
      message = "Please enter expectedTat";
      status = true;
    } else if (
      this.props.action === "accept" &&
      (!forwardDistance || !returnDistance)
    ) {
      message = "Please enter Distance";
      status = true;
    } else if (
      this.props.action === "accept" &&
      (!forwardDockHours || !returnDockHours)
    ) {
      message = "Please enter Dock Hours";
      status = true;
    }
    console.log("message ----", message);
    if (message) {
      Snackbar.show({
        title: message,
        duration: Snackbar.LENGTH_SHORT
      });
    }
    return status;
  };

  updateForwardReference = forwardReference => {
    this.setState({ forwardReference });
  };

  updateReturnReference = returnReference => {
    this.setState({ returnReference });
  };

  editTwoWayBooking = () => {
    const errors = this.getErrors();
    const { action } = this.props;

    if (errors === false) {
      this.setState({ creatingBooking: true });

      const type = BOOKING_TYPE.AD_HOC_TWO_WAY;
      const {
        selectedCustomer: customer_id,
        selectedVehicle: vehicle_id,
        selectedVehicleType: vehicle_type_id,
        selectedVendor: vendor_id,
        driverPhoneNumber: driver_phone,
        forwardDateTime,
        forwardSourceCity,
        forwardDestinationCity,
        forwardViaCities,
        forwardExpectedTat,
        returnDateTime,
        returnSourceCity,
        returnDestinationCity,
        returnViaCities,
        returnExpectedTat,
        selectedEngageReason: engaged_by_id,
        forwardDistance,
        returnDistance,
        forwardReference,
        returnReference
      } = this.state;

      const request_data = {
        customer_id,
        vehicle_id,
        vehicle_type_id,
        vendor_id,
        driver_phone,
        action,
        engaged_by_id,
        forward: {
          booking_id: this.props.booking_id,
          date_time: new moment(forwardDateTime).format("DD-MM-YYYY HH:MM"),
          source_city: forwardSourceCity,
          destination_city: forwardDestinationCity,
          via_cities: forwardViaCities,
          expected_tat: forwardExpectedTat,
          distance: forwardDistance,
          reference: forwardReference
        },
        return: {
          booking_id: this.state.return_booking_id,
          date_time: new moment(returnDateTime).format("DD-MM-YYYY HH:MM"),
          source_city: returnSourceCity,
          destination_city: returnDestinationCity,
          via_cities: returnViaCities,
          expected_tat: returnExpectedTat,
          distance: returnDistance,
          reference: returnReference
        }
      };

      const { editBooking } = this.props;
      editBooking({ type, request_data })
        .then(response => {
          console.log("response ----", response);
          const { status } = response;
          if (status) {
            Snackbar.show({
              title: "" + response.message,
              duration: Snackbar.LENGTH_LONG
            });

            this.props.navigation.goBack();
          } else {
            Snackbar.show({
              title: "" + response.message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .catch(error => {
          console.log("error ----", error);
        })
        .finally(this.setState({ creatingBooking: false }));
    }
  };

  goToBookings = () => {
    this.setState({ showModal: false }, () => {
      this.props.navigation.navigate(NAVIGATE.ALL_BOOKINGS);
    });
  };

  acceptTwoWayBooking = () => {
    const errors = this.getErrors();
    const { action } = this.props;

    if (errors === false) {
      this.setState({ creatingBooking: true });

      const type = BOOKING_TYPE.AD_HOC_TWO_WAY;
      const {
        selectedCustomer: customer_id,
        selectedVehicle: vehicle_id,
        selectedVehicleType: vehicle_type_id,
        selectedVendor: vendor_id,
        driverPhoneNumber: driver_phone,
        forwardDateTime,
        forwardSourceCity,
        forwardDestinationCity,
        forwardViaCities,
        forwardExpectedTat,
        returnDateTime,
        returnSourceCity,
        returnDestinationCity,
        returnViaCities,
        returnExpectedTat,
        selectedEngageReason: engaged_by_id,
        forwardDistance,
        returnDistance,
        forwardReference,
        returnReference
      } = this.state;

      const request_data = {
        customer_id,
        vehicle_id,
        vehicle_type_id,
        vendor_id,
        driver_phone,
        action,
        engaged_by_id,
        forward: {
          booking_id: this.props.booking_id,
          date_time: new moment(forwardDateTime).format("DD-MM-YYYY HH:MM"),
          source_city: forwardSourceCity,
          destination_city: forwardDestinationCity,
          via_cities: forwardViaCities,
          expected_tat: forwardExpectedTat,
          distance: forwardDistance,
          reference: forwardReference
        },
        return: {
          booking_id: this.state.return_booking_id,
          date_time: new moment(returnDateTime).format("DD-MM-YYYY HH:MM"),
          source_city: returnSourceCity,
          destination_city: returnDestinationCity,
          via_cities: returnViaCities,
          expected_tat: returnExpectedTat,
          distance: returnDistance,
          reference: returnReference
        }
      };

      const { acceptBooking } = this.props;
      acceptBooking({ type, request_data })
        .then(response => {
          const { status } = response;
          if (status) {
            Snackbar.show({
              title: "" + response.message,
              duration: Snackbar.LENGTH_LONG
            });

            this.goToBookings();
          } else {
            Snackbar.show({
              title: "" + response.message,
              duration: Snackbar.LENGTH_LONG
            });
          }
        })
        .catch(error => {
          console.log("error ----", error);
        })
        .finally(this.setState({ creatingBooking: false }));
    }
  };

  updateFDockHours = forwardDockHours => {
    this.setState({ forwardDockHours });
  };

  updateRDockHours = returnDockHours => {
    this.setState({ returnDockHours });
  };

  render() {
    const { customers, booking_id, action } = this.props;
    const { selectedCustomer, bookingType, bookingStatus } = this.state;

    console.log(
      "this.state.vehicleTypeDisabled ----",
      this.state.vehicleTypeDisabled
    );
    return (
      <Fragment>
        <Header
          name={
            this.props.action === "edit" ? "EDIT BOOKING" : "ACCEPT BOOKING"
          }
          navigation={this.props.navigation}
          goBack={true}
        />
        <ScrollView contentContainerStyle={customStyles.scrollContainer}>
          <CustomerInput
            customers={customers}
            customerIds={this.props.customerIds}
            disabled={this.state.customerDisable}
            selectedCustomer={selectedCustomer}
            selectCustomer={this.selectCustomer}
          />
          {bookingStatus !== "RQ" ? (
            <View>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <VehicleInput
                    disabled={this.state.vehicleDisabled}
                    vehicles={this.props.vehicles}
                    vehicleTypes={this.props.vehicleTypes}
                    selectedVehicle={this.state.selectedVehicle}
                    selectVehicle={this.selectVehicle}
                  />
                </View>
                <View style={{ flex: 1 }}>
                  <VehicleTypeInput
                    disabled={this.state.vehicleTypeDisabled}
                    vehiclesTypes={this.props.vehicleTypes}
                    selectedVehicle={this.state.selectedVehicleType}
                    selectVehicle={this.selectVehicleType}
                    selectOne={true}
                  />
                </View>
              </View>
              <VendorInput
                vendors={this.props.vendors}
                selectedVendor={this.state.selectedVendor}
                selectVendor={this.selectVendor}
                disabled={
                  !this.state.selectedVehicle || this.state.vendorDisabled
                }
              />
            </View>
          ) : null}

          {booking_id === null && (
            <NumberInput
              message={"Driver Phone"}
              updateText={this.updateDriverNumber}
              placeholder={"Enter Driver Phone Number"}
              initialValue={this.state.driverPhoneNumber}
            />
          )}

          <EngagedByInput
            engagedBy={this.props.engagedBy}
            selectedEngageReason={this.state.selectedEngageReason}
            setEngageReason={this.setEngageReason}
          />

          {/*{(booking_id !== null && bookingType === "AHF") ||*/}
          {/*bookingType === "AHR" ||*/}
          {/*bookingType === "SCHF" ||*/}
          {/*bookingType === "SCHR" ? (*/}
          {/*  <NumberInput*/}
          {/*    numericProp={false}*/}
          {/*    message={"Comment"}*/}
          {/*    updateText={this.updateComment}*/}
          {/*    placeholder={"Comment"}*/}
          {/*    initialValue={this.state.expectedTat}*/}
          {/*  />*/}
          {/*) : null}*/}

          {/*==============================FORWARD===============================*/}
          <View style={styles.subheadingContainer}>
            <Text style={styles.subheading}>FORWARD</Text>
            <Icon
              name="truck-delivery"
              type="material-community"
              color={colors.darkblue}
              containerStyle={{ marginLeft: 8 }}
            />
          </View>

          <DateTimeInput
            message={"Forward Trip Date and Time"}
            selectedDateTime={this.state.forwardDateTime}
            selectDateTime={this.setForwardDate}
            initialValue={this.state.forwardDateTime}
          />

          <NumberInput
            numericProp={false}
            message={"Reference"}
            updateText={this.updateForwardReference}
            placeholder={"Enter Forward Reference"}
            initialValue={this.state.forwardReference}
          />

          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            disabled={this.state.forwardSourceCityDisable}
            selectedCity={this.state.forwardSourceCity}
            selectCity={this.selectForwardSourceCity}
            message={"Source City"}
          />
          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            disabled={this.state.forwardDestinationCityDisable}
            selectedCity={this.state.forwardDestinationCity}
            selectCity={this.selectForwardDestinationCity}
            message={"Destination City"}
          />
          <ViaCityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            selectedViaCities={this.state.forwardViaCities}
            addViaCity={this.addForwardSelectedViaCity}
            removeViaCity={this.removeForwardSelectedViaCity}
          />

          {this.props.action === "accept" && (
            <Fragment>
              <NumberInput
                disable={true}
                message={"Dock Hours"}
                updateText={this.updateFDockHours}
                placeholder={"Dock Hours"}
                initialValue={this.state.forwardDockHours}
              />
            </Fragment>
          )}

          {/*<ExpectedTatInput*/}
          {/*  message={"Expected TAT"}*/}
          {/*  updateText={this.updateForwardTAT}*/}
          {/*  disabledInitialValue={this.state.expectedTatEnable}*/}
          {/*  vehicle_type_id={this.state.selectedVehicleType}*/}
          {/*  customer_id={this.state.selectedCustomer}*/}
          {/*  source_city={this.state.selectedSourceCity}*/}
          {/*  destination_city={this.state.selectedDestinationCity}*/}
          {/*  via_cities={this.state.selectedViaCities}*/}
          {/*  source_flag={AUTO_FILL_TAT.CREATE_BOOKING}*/}
          {/*  placeholder={"Click Icon to autofill tat"}*/}
          {/*  initialValue={this.state.forwardExpectedTat}*/}
          {/*/>*/}

          <DistanceInputNew
            message={"Expected TAT"}
            initialValue={this.state.forwardExpectedTat}
            updateText={this.updateForwardTAT}
            vehicle_type_id={this.state.selectedVehicleType}
            customer_id={this.state.selectedCustomer}
            source_city={this.state.forwardSourceCity}
            destination_city={this.state.forwardDestinationCity}
            via_cities={this.state.forwardViaCities}
            source_flag={AUTO_FILL_TAT.EDIT_BOOKING}
            placeholder={"Click Icon to autofill tat"}
            distanceValue={this.state.forwardDistance}
            action={this.props.action}
            enableDistance={this.state.forwardEnableDistance}
            enableTat={this.state.forwardEnableTat}
            apiHit={this.state.apiHit}
            count={count}
          />

          {/*==============================RETURN===============================*/}
          <View style={styles.subheadingContainer}>
            <Text style={styles.subheading}>RETURN</Text>
            <Icon
              name="truck-delivery"
              type="material-community"
              color={colors.darkblue}
              iconStyle={{ transform: [{ rotateY: "180deg" }] }}
              containerStyle={{ marginLeft: 8 }}
            />
          </View>
          <DateTimeInput
            message={"Return Trip Date and Time"}
            selectedDateTime={this.state.returnDateTime}
            selectDateTime={this.setReturnDate}
            initialValue={this.state.forwardDateTime}
          />

          <NumberInput
            numericProp={false}
            message={"Reference"}
            updateText={this.updateReturnReference}
            placeholder={"Enter Return Reference"}
            initialValue={this.state.returnReference}
          />

          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            disabled={this.state.returnSourceCityDisable}
            selectedCity={this.state.returnSourceCity}
            selectCity={this.selectReturnSourceCity}
            message={"Source City"}
          />

          <CityInput
            cities={this.props.cities}
            cityIds={this.props.cityIds}
            disabled={this.state.returnDestinationCityDisable}
            selectedCity={this.state.returnDestinationCity}
            selectCity={this.selectReturnDestinationCity}
            message={"Destination City"}
          />
          <ViaCityInput
            cityIds={this.props.cityIds}
            cities={this.props.cities}
            selectedViaCities={this.state.returnViaCities}
            addViaCity={this.addReturnSelectedViaCity}
            removeViaCity={this.removeReturnSelectedViaCity}
          />

          {this.props.action === "accept" && (
            <Fragment>
              <NumberInput
                disable={true}
                message={"Dock Hours"}
                updateText={this.updateRDockHours}
                placeholder={"Dock Hours"}
                initialValue={this.state.returnDockHours}
              />
            </Fragment>
          )}

          {/*<ExpectedTatInput*/}
          {/*  message={"Expected TAT"}*/}
          {/*  updateText={this.updateReturnTAT}*/}
          {/*  vehicle_type_id={this.state.selectedVehicleType}*/}
          {/*  customer_id={this.state.selectedCustomer}*/}
          {/*  source_city={this.state.selectedSourceCity}*/}
          {/*  destination_city={this.state.selectedDestinationCity}*/}
          {/*  via_cities={this.state.selectedViaCities}*/}
          {/*  source_flag={AUTO_FILL_TAT.CREATE_BOOKING}*/}
          {/*  placeholder={"Click Icon to autofill tat"}*/}
          {/*  initialValue={this.state.returnExpectedTat}*/}
          {/*/>*/}

          <DistanceInputNewReturn
            message={"Expected TAT"}
            initialValue={this.state.returnExpectedTat}
            updateText={this.updateReturnTAT}
            vehicle_type_id={this.state.selectedVehicleType}
            customer_id={this.state.selectedCustomer}
            source_city={this.state.returnSourceCity}
            destination_city={this.state.returnDestinationCity}
            via_cities={this.state.returnViaCities}
            source_flag={AUTO_FILL_TAT.EDIT_BOOKING}
            placeholder={"Click Icon to autofill tat"}
            distanceValue={this.state.returnDistance}
            action={this.props.action}
            enableDistance={this.state.returnEnableDistance}
            enableTat={this.state.returnEnableTat}
            apiHitReturn={this.state.apiHitReturn}
            count={returnCount}
          />
        </ScrollView>
        <Footer
          name={action === "edit" ? "EDIT" : "ACCEPT"}
          action={
            action === "edit"
              ? this.editTwoWayBooking
              : this.acceptTwoWayBooking
          }
          loading={this.state.creatingBooking}
        />
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  subheadingContainer: {
    paddingTop: 14,
    paddingLeft: 12,
    paddingRight: 12,
    paddingBottom: 0,
    flexDirection: "row"
  },
  subheading: {
    color: colors.darkblue,
    fontWeight: "600",
    fontSize: 16
  }
});

export default EditBookingTwoWay;
