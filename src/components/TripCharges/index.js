import React, { Component, Fragment } from "react";
import Header from "../common/header";
import Snackbar from "react-native-snackbar";
import NumberInput from "../common/NumberInput";
import Footer from "../common/footer";
import { ScrollView } from "react-navigation";
import ImageViewModal from "../../modal/ImageViewModal";
import {
  ActivityIndicator,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../common/Colors";
import AdvanceTripDetails from "../../containers/AdvanceTripDetails";
import PhotoUpload from "react-native-photo-upload";
import { Icon } from "react-native-elements";

class TripCharges extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageViewVisible: false,
      documentUrl: null,
      freight: null,
      loadingCharges: null,
      unloadingCharges: null,
      detentionCharges: null,
      additionalCost: null,
      penalty: null,
      consignNoteCharge: null,
      remarks: null,
      loadingSlipUrl: null,
      uploadedImage: null,
      imageViewModal: false,
      tripCode: null,
      tripTime: null,
      route: null,
      tripStatus: null,
      contentType: "image/png",
      vendorName: null,
      vendorCode: null,
      vehicleNumber: null,
      vehicleType: null,
      loader: false,
      imageLoader: false,
      images: [],
      selectedImage: null,
      testImageView: false
    };
  }

  componentDidMount() {
    const { requestTripAdvance } = this.props;
    const { trips, trip_charges, vendor_details } = requestTripAdvance;

    const {
      basicInfo: { code, route, trip_time },
      status = {}
    } = Object.values(trips)[0] || {};

    const {
      freight,
      loading_slip_url,
      loading,
      unloading,
      detention,
      additional_cost,
      penalty,
      consignment_note_charge,
      remarks
    } = trip_charges;

    if (freight) {
      this.setState({ freight });
    }

    if (loading_slip_url) {
      this.setState({
        loadingSlipUrl: loading_slip_url
      });
    }

    if (loading) {
      this.setState({ loadingCharges: loading });
    }

    if (unloading) {
      this.setState({ unloadingCharges: unloading });
    }

    if (detention) {
      this.setState({ detentionCharges: detention });
    }

    if (additional_cost) {
      this.setState({ additionalCost: additional_cost });
    }

    if (penalty) {
      this.setState({ penalty: penalty });
    }

    if (consignment_note_charge) {
      this.setState({ consignNoteCharge: consignment_note_charge });
    }

    if (remarks) {
      this.setState({ remarks });
    }

    this.setState({
      tripCode: code,
      tripTime: trip_time,
      tripStatus: status,
      route
    });
  }

  updateNickName = nickName => {
    this.setState({ nickName: nickName });
  };

  updateFreight = freight => {
    this.setState({ freight });
  };

  updateDetention = detention => {
    this.setState({ detentionCharges: detention });
  };

  updateLoading = loading => {
    this.setState({ loadingCharges: loading });
  };

  updateUnLoading = unloading => {
    this.setState({ unloadingCharges: unloading });
  };

  updateAdditionalCode = additionalCost => {
    this.setState({ additionalCost: additionalCost });
  };

  updatePenalty = penalty => {
    this.setState({ penalty });
  };

  updateConsignNoteCharge = consignNoteCharge => {
    this.setState({ consignNoteCharge });
  };

  uploadImageModal = () => {
    console.log("uploadImageModal called");
    return (
      <PhotoUpload
        containerStyle={{
          width: 50,
          height: 50,
          borderStyle: "dashed",
          borderWidth: 1,
          borderColor: colors.black25,
          borderRadius: 1,
          alignItems: "center",
          margin: 10,
          justifyContent: "center"
        }}
        onStart={this.startLoading}
        onCancel={this.endLoading}
        onError={this.endLoading}
        onPhotoSelect={image => {
          console.log("image -------------------------------", image);
          if (image) {
            const baseImage = "data:image/png;base64," + image;

            this.setState({
              uploadedImage: image,
              loadingSlipUrl: null,
              selectedImage: baseImage
            });
          }
          this.endLoading();
        }}
      >
        <Icon
          name="plus"
          type="material-community"
          size={18}
          color={colors.blue}
        />
      </PhotoUpload>
    );
  };

  viewUploadImage = () => {
    if (this.state.loadingSlipUrl) {
      console.log("---- document url not null ----");
      this.setState({ imageViewModal: true });
    } else if (this.state.selectedImage) {
      console.log("---- selectedImage not null ----");
      this.setState({ imageViewModal: true });
    } else {
      console.log("---- null  ----");
      this.setState({ imageViewModal: false });
    }
  };
  uploadRemarks = remarks => {
    this.setState({ remarks });
  };

  getErrors = () => {
    let message = null;
    let status = false;
    const { freight } = this.state;

    const { loggedInUser } = this.props;

    if (!freight) {
      message = "Please Enter Nick Name";
      status = true;
    } else if (freight !== "") {
      message = "Please Enter Nick Name";
      status = true;
    }
    if (message) {
      Snackbar.show({
        title: message,
        duration: Snackbar.LENGTH_LONG
      });
    }
    return status;
  };

  setEngageReason = selectedEngageReason => {
    this.setState({ selectedEngageReason });
  };

  openImageViewModal = () => {
    return (
      <ImageViewModal
        isVisible={!this.state.imageViewVisible}
        closeModal={this.closeImageViewModal}
      />
    );
  };

  closeImageViewModal = () => {
    this.setState({ imageViewVisible: false, imageViewModal: false });
  };

  viewImageViewModal = () => {
    this.setState({ imageViewModal: true, imageViewVisible: true });
  };

  saveChanges = () => {
    const { addTripCharges } = this.props;

    this.setState({ loader: true });
    const {
      freight,
      loadingCharges,
      unloadingCharges,
      detentionCharges,
      loadingSlipUrl,
      tripCode,
      uploadedImage,
      contentType,
      additionalCost,
      consignNoteCharge,
      penalty,
      remarks,
      imageLoader
    } = this.state;

    const requestData = {
      trip_code: tripCode,
      freight,
      loading: loadingCharges,
      unloading: unloadingCharges,
      detention: detentionCharges,
      loading_slip: uploadedImage,
      content_type: contentType,
      additional_cost: additionalCost,
      penalty,
      consignment_note_charge: consignNoteCharge,
      remarks
    };

    if (imageLoader) {
      this.setState({ loader: false });
      Snackbar.show({
        title: "Please wait Loading Slip is being uploaded",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (freight === null || freight === "") {
      this.setState({ loader: false });
      Snackbar.show({
        title: "Please enter Freight Charges",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      addTripCharges(requestData)
        .then(response => {
          console.log("response ----", response);

          this.goBack();
        })
        .catch(error => {})
        .finally(() => {
          this.setState({ loader: false });
        });
    }
  };

  goBack = () => {
    // this.props.navigation.navigate(NAVIGATE.REQUEST_TRIP_ADVANCE, {clearData: true});
    this.props.navigation.goBack();
  };

  startLoading = () => {
    this.setState({ imageLoader: true });
  };

  endLoading = () => {
    this.setState({ imageLoader: false });
  };

  render() {
    const { loggedInUser, tripData } = this.props;

    return (
      <Fragment>
        <Header
          name={"Add Trip Charges"}
          navigation={this.props.navigation}
          goBack={true}
        />

        <View style={{ marginTop: 10 }} />
        <AdvanceTripDetails
          showVendor={true}
          route={this.state.route}
          tripCode={this.state.tripCode}
          tripTime={this.state.tripTime}
          status={this.state.tripStatus}
          tripData={this.props.tripData}
        />

        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <Text
            style={{
              color: colors.black85,
              fontSize: 18,
              alignSelf: "center",
              fontWeight: "300",
              margin: 10
            }}
          >
            ADD TRIP CHARGES
          </Text>

          <Text
            style={{
              fontSize: 14,
              alignSelf: "center",
              color: "#CD5C5C",
              padding: 3,
              marginBottom: 10,
              marginTop: 10
            }}
          >
            NOTE: Buying Freight and Loading Slip is mandatory for Advance
            Requests
          </Text>

          <NumberInput
            numericProp={true}
            message={"Buying Freight"}
            updateText={this.updateFreight}
            placeholder={"Buying Freight"}
            initialValue={this.state.freight}
          />

          <NumberInput
            numericProp={true}
            message={"Loading Charges"}
            updateText={this.updateLoading}
            placeholder={"Loading Charges"}
            initialValue={this.state.loadingCharges}
            disableDistance={false}
          />

          <NumberInput
            numericProp={true}
            message={"Unloading Charges"}
            updateText={this.updateUnLoading}
            placeholder={"Unloading Charges"}
            initialValue={this.state.unloadingCharges}
          />

          <NumberInput
            numericProp={true}
            message={"Detention Charges"}
            updateText={this.updateDetention}
            placeholder={"Detention Charges"}
            initialValue={this.state.detentionCharges}
          />

          <NumberInput
            numericProp={true}
            message={"Additional Cost"}
            updateText={this.updateAdditionalCode}
            placeholder={"Additional Cost"}
            initialValue={this.state.additionalCost}
          />

          <NumberInput
            numericProp={true}
            message={"Penalty"}
            updateText={this.updatePenalty}
            placeholder={"Penalty"}
            initialValue={this.state.penalty}
          />

          <NumberInput
            numericProp={true}
            message={"Consign. Note Charge"}
            updateText={this.updateConsignNoteCharge}
            placeholder={"Consign. Note Charge"}
            initialValue={this.state.consignNoteCharge}
          />

          <NumberInput
            numericProp={false}
            message={"Remarks"}
            updateText={this.uploadRemarks}
            placeholder={"Remarks (If Any)"}
            initialValue={this.state.remarks}
          />

          {this.state.testImageView && (
            <Image
              source={{ uri: this.state.selectedImage }}
              style={{
                height: 100,
                width: 100
              }}
            />
          )}

          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text
              style={[
                styles.labelText,
                { textAlign: "center", alignSelf: "center" }
              ]}
            >
              Loading Slip
            </Text>
            <View
              style={{
                width: 50,
                height: 50,
                borderStyle: "dashed",
                borderWidth: 1,
                borderColor: colors.black25,
                borderRadius: 1,
                alignItems: "center",
                margin: 10,
                justifyContent: "center"
              }}
            >
              <PhotoUpload
                onStart={this.startLoading}
                onCancel={this.endLoading}
                onError={this.endLoading}
                onPhotoSelect={image => {
                  console.log("image -------------------------------", image);
                  if (image) {
                    const baseImage = "data:image/png;base64," + image;

                    this.setState({
                      uploadedImage: image,
                      loadingSlipUrl: null,
                      selectedImage: baseImage
                    });
                  }
                  this.endLoading();
                }}
              >
                <View
                  style={{
                    width: 50,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Icon
                    name="plus"
                    type="material-community"
                    size={18}
                    color={colors.blue}
                  />
                </View>
              </PhotoUpload>
            </View>
            {/*<TouchableOpacity onPress={this.uploadImageModal}>*/}
            {/*  <PhotoUpload*/}
            {/*    containerStyle={{*/}
            {/*      width: 50,*/}
            {/*      height: 50,*/}
            {/*      borderStyle: "dashed",*/}
            {/*      borderWidth: 1,*/}
            {/*      borderColor: colors.black25,*/}
            {/*      borderRadius: 1,*/}
            {/*      alignItems: "center",*/}
            {/*      margin: 10,*/}
            {/*      justifyContent: "center"*/}
            {/*    }}*/}
            {/*    onStart={this.startLoading}*/}
            {/*    onCancel={this.endLoading}*/}
            {/*    onError={this.endLoading}*/}
            {/*    onPhotoSelect={image => {*/}
            {/*      console.log("image -------------------------------", image);*/}
            {/*      if (image) {*/}
            {/*        const baseImage = "data:image/png;base64," + image;*/}
            {/*        */}
            {/*        this.setState({*/}
            {/*          uploadedImage: image,*/}
            {/*          loadingSlipUrl: null,*/}
            {/*          selectedImage: baseImage*/}
            {/*        });*/}
            {/*      }*/}
            {/*      this.endLoading();*/}
            {/*    }}*/}
            {/*  >*/}
            {/*    <Icon*/}
            {/*      name="plus"*/}
            {/*      type="material-community"*/}
            {/*      size={18}*/}
            {/*      color={colors.blue}*/}
            {/*    />*/}
            {/*  </PhotoUpload>*/}
            {/*</TouchableOpacity>*/}

            {this.state.imageLoader && (
              <ActivityIndicator size="small" color={colors.blue} />
            )}

            {this.state.imageLoader === false &&
            (this.state.loadingSlipUrl || this.state.selectedImage) ? (
              <TouchableOpacity onPress={this.viewUploadImage}>
                <View
                  style={{
                    width: 50,
                    height: 50,
                    borderStyle: "dashed",
                    borderWidth: 1,
                    borderColor: colors.black25,
                    borderRadius: 1,
                    alignItems: "center",
                    margin: 10,
                    justifyContent: "center"
                  }}
                >
                  <Image
                    source={
                      this.state.selectedImage
                        ? { uri: this.state.selectedImage }
                        : { uri: this.state.loadingSlipUrl }
                    }
                    style={{ flex: 1, height: 50, width: 50 }}
                    resizeMode={"contain"}
                  />
                </View>
              </TouchableOpacity>
            ) : null}
          </View>

          {/*<TouchableOpacity onPress={this.viewImageViewModal}>*/}
          {/*  <Text>Open Uploaded Image</Text>*/}
          {/*</TouchableOpacity>*/}
        </ScrollView>

        {this.state.imageViewModal && (
          <ImageViewModal
            imageViewVisible={this.state.imageViewModal}
            closeModal={this.closeImageViewModal}
            imageVisible={true}
            image={this.state.selectedImage}
            url={this.state.loadingSlipUrl}
            screen={"trip-charges"}
          />
        )}

        <Footer
          name="SAVE"
          action={this.saveChanges}
          loading={this.state.loader}
        />
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  labelText: {
    marginTop: -10,
    color: colors.blue,
    marginLeft: 10,
    backgroundColor: colors.white,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  textInputStyle: {
    flex: 1,
    alignItems: "flex-start",
    paddingLeft: 10,
    justifyContent: "center",
    ...Platform.select({
      ios: { maxHeight: 30, paddingBottom: 10 },
      android: {
        maxHeight: 30,
        paddingTop: 0,
        paddingBottom: 0,
        textAlignVertical: "center"
      }
    })
  },
  cardContainerStyle: {
    borderColor: "#ddd",
    borderWidth: 1,
    borderRadius: 2,
    borderBottomWidth: 1,
    marginLeft: 5,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    borderTopLeftRadius: 3,
    backgroundColor: colors.white,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3
  }
});

export default TripCharges;

// onPhotoSelect Previous working code
// this.setState(
//   {
//     uploadedImage: avatar,
//     loadingSlipUrl: null
//   });

// Feb 28
// this.setState({
//   uploadedImage: image,
//   loadingSlipUrl: null,
//   selectedImage: baseImage
// });
