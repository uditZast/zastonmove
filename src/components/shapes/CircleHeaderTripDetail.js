import React from 'react';
import {Text, View,StyleSheet} from 'react-native';
import colors from "../../colors/Colors";

const CircleHeaderTripDetails = (props) => {
    return (
        <View style={styles.containerStyle}>
            <View style={styles.circle}>
                <Text style={styles.textStyle}>{props.speed.split(' ')[0]}</Text>
            </View>

            <Text style={styles.belowCircleText}>{props.speed.split(' ')[1]}</Text>
        </View>

    );
}

const styles = StyleSheet.create({
    circle: {
        width: 22,
        height: 22,
        borderRadius: 100 / 2,
        backgroundColor: colors.border_grey,
        marginLeft: 5
    },
    textStyle: {
        color: colors.white,
        fontSize: 13,
        textAlign: 'center'
    },
    containerStyle: {
        flexDirection: 'column',
        marginLeft: 10,
        marginTop: 10,
    },
    belowCircleText: {
        fontSize: 11,
        textAlign: 'center',
        color: colors.white
    }

});

export default CircleHeaderTripDetails;