import React from 'react';
import {View,StyleSheet} from 'react-native';
import colors from "../colors/Colors";

const Card = (props) => {
    return (
        <View style={styles.containerStyle}>
            {props.children}
        </View>
    );
};

const styles = StyleSheet.create({
    containerStyle: {
        borderColor: '#ddd',
        borderWidth: 1,
        borderRadius: 2,
        borderBottomWidth: 1,
        marginLeft: 5,
        marginTop: 5,
        marginRight: 5,
        marginBottom: 5,
        borderTopLeftRadius: 3,
        backgroundColor: colors.white,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        flex: 1,
    }

});

// shadowOpacity: 0.1,
//     shadowRadius: 2,

//        shadowOffset: {width: 0, height: 2},
//shadowColor: '#000',
//borderColor: '#ddd',
export default Card;