import React, { Component, Fragment } from "react";
import { View, Text, ScrollView, ActivityIndicator } from "react-native";
import Header from "../common/header";
import CustomStyles from "../common/Styles";
import colors from "../common/Colors";
import { TRIP_ENTITIES } from "../../../constants";

class TripHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  componentDidMount() {
    const { fetchTripHistory, trip_id } = this.props;
    fetchTripHistory({ trip_id, entity: TRIP_ENTITIES.HISTORY }).finally(() =>
      this.setState({ loading: false })
    );
  }

  renderHistory = historyData => {
    const { time, user, action, status } = historyData;
    return (
      <Fragment>
        <View
          style={{
            backgroundColor: colors.black02,
            marginBottom: 10,
            borderRadius: 6,
            borderWidth: 1,
            borderColor: colors.black15,
            padding: 8
          }}
        >
          <View>
            <Text>{action}</Text>
          </View>
          <View style={{ paddingTop: 8 }}>
            <Text style={{ fontWeight: "500" }}>
              Status: <Text style={{ color: colors.blue }}>{status}</Text>
            </Text>
          </View>
          <View style={{ alignSelf: "flex-end", paddingTop: 8 }}>
            <Text>
              -by <Text style={{ color: colors.blue }}>{user}</Text>
            </Text>
          </View>
          <View style={{ alignSelf: "flex-end", paddingTop: 4 }}>
            <Text>
              at <Text style={{ color: colors.blue }}>{time}</Text>
            </Text>
          </View>
        </View>
      </Fragment>
    );
  };

  render() {
    const { trip, tripHistory } = this.props;
    const { basicInfo: { code, trip_history_id } = {} } = trip || {};
    const { data = [] } = tripHistory[trip_history_id] || {};
    const { loading } = this.state;

    return (
      <Fragment>
        <Header name={code} navigation={this.props.navigation} goBack={true} />
        <View style={CustomStyles.flex1}>
          <View style={{ flex: 1 }}>
            {loading && (
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <ActivityIndicator size="large" color={colors.darkblue} />
              </View>
            )}
            {!loading && (
              <ScrollView
                contentContainerStyle={{
                  flexGrow: 1,
                  padding: 10
                }}
              >
                {data &&
                  data.length > 0 &&
                  data.map((item, index) => {
                    return (
                      <Fragment key={index}>
                        {this.renderHistory(item)}
                      </Fragment>
                    );
                  })}
                {!data || (data.length === 0 && <Text>NO HISTORY</Text>)}
              </ScrollView>
            )}
          </View>
        </View>
      </Fragment>
    );
  }
}

export default TripHistory;
