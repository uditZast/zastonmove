import React, { Component, Fragment } from "react";
import {
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from "react-native";
import colors from "../common/Colors";
import { Icon } from "react-native-elements";
import DatePicker from "react-native-datepicker";
import moment from "moment";
import {
  NAVIGATE,
  TRIP_FLAG,
  TRIP_STATUS_TABLE,
  USER_RIGHTS
} from "../../../constants";
import TerminateModal from "./terminateModal";
import { hasRights } from "../common/checkRights";

class BottomTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dateTime: new moment(),
      tripInTime: new moment(),
      tripOutTime: new moment(),
      reachedDestinationTime: new moment(),
      tripCloseTime: new moment(),
      markingBegin: false,
      markingIn: false,
      markingTripOut: false,
      markingReachedAtDestination: false,
      markingClose: false,
      isVisible: false
    };
  }

  insertIcon = name => {
    return (
      <Icon
        name={name}
        type="material-community"
        size={20}
        color={colors.darkblue}
      />
    );
  };

  openDatePicker = () => {
    console.log("this.datePicker ----", this.datePicker);
    this.datePicker.onPressDate();
  };

  openTripClosePicker = () => {
    this.tripClosePicker.onPressDate();
  };

  openTripInDatePicker = () => {
    this.tripInDatePicker.onPressDate();
  };

  openTripOutDatePicker = () => {
    this.tripOutDatePicker.onPressDate();
  };

  openReachedAtDestinationPicker = () => {
    this.reachedAtDestinationPicker.onPressDate();
  };

  setDateTime = (date, dateTime) => {
    const momentDate = new moment(dateTime);
    this.setState(
      { dateTime: momentDate, markingBegin: true },
      this.markTripBegin
    );
  };

  setTripCloseTime = (date, dateTime) => {
    const momentDate = new moment(dateTime);
    this.setState(
      { tripCloseTime: momentDate, markingClose: true },
      this.markTripAsClose
    );
  };

  setTripInTime = (date, dateTime) => {
    const momentDate = new moment(dateTime);
    this.setState(
      { tripInTime: momentDate, markingIn: true },
      this.markTripAsIn
    );
  };

  setTripOutTime = (date, dateTime) => {
    const momentDate = new moment(dateTime);
    this.setState(
      { tripOutTime: momentDate, markingTripOut: true },
      this.markTripAsOut
    );
  };

  setReachedAtDestinationTime = (date, dateTime) => {
    const momentDate = new moment(dateTime);
    this.setState(
      {
        reachedDestinationTime: momentDate,
        markingReachedAtDestination: true
      },
      this.markReachedAtDestination
    );
  };

  markTripBegin = () => {
    const { markTripAsBegin, trip_id } = this.props;
    const { dateTime } = this.state;
    const date_time = new moment(dateTime).format("DD-MM-YYYY HH:mm");
    markTripAsBegin({ date_time, trip_id }).finally(() =>
      this.setState({ markingBegin: false })
    );
  };

  markTripAsClose = () => {
    const { markTripAsClosed, trip_id } = this.props;
    const { tripCloseTime } = this.state;
    const date_time = new moment(tripCloseTime).format("DD-MM-YYYY HH:mm");
    markTripAsClosed({ date_time, trip_id }).finally(() =>
      this.setState({ markingClose: false })
    );
  };

  markTripAsIn = () => {
    const { markTripAsTouchingIn, trip_id, trip } = this.props;
    const { tripInTime } = this.state;
    const { basicInfo: { city_id } = {} } = trip || {};
    const date_time = new moment(tripInTime).format("DD-MM-YYYY HH:mm");
    markTripAsTouchingIn({ date_time, trip_id, city_id }).finally(() =>
      this.setState({ markingIn: false })
    );
  };

  markTripAsOut = () => {
    const { markTripAsBegin, trip_id } = this.props;
    const { tripOutTime } = this.state;
    const date_time = new moment(tripOutTime).format("DD-MM-YYYY HH:mm");
    markTripAsBegin({ date_time, trip_id }).finally(() =>
      this.setState({ markingTripOut: false })
    );
  };

  markReachedAtDestination = () => {
    const { markTripAsArrived, trip_id } = this.props;
    const { reachedDestinationTime } = this.state;
    const date_time = new moment(reachedDestinationTime).format(
      "DD-MM-YYYY HH:mm"
    );
    markTripAsArrived({ date_time, trip_id }).finally(() =>
      this.setState({ markingReachedAtDestination: false })
    );
  };

  openTripComments = () => {
    const { trip_id } = this.props;
    this.props.navigation.navigate(NAVIGATE.TRIP_COMMENTS, { trip_id });
  };

  openHaltTrip = () => {
    const { trip_id } = this.props;
    this.props.navigation.navigate(NAVIGATE.HALT_TRIP, { trip_id });
  };

  openHistory = () => {
    const { trip_id } = this.props;
    this.props.navigation.navigate(NAVIGATE.TRIP_HISTORY, { trip_id });
  };

  openCrossDock = () => {
    const { trip_id } = this.props;
    this.props.navigation.navigate(NAVIGATE.CROSS_DOCK_TRIP, { trip_id });
  };

  openRunningStatus = () => {
    const { trip_id } = this.props;
    this.props.navigation.navigate(NAVIGATE.TRIP_RUNNING_STATUS, { trip_id });
  };

  goToEditTrip = () => {
    const { trip_id } = this.props;
    this.props.navigation.navigate(NAVIGATE.EDIT_TRIP, { trip_id });
  };

  goToEditRoute = () => {
    const { trip_id } = this.props;
    this.props.navigation.navigate(NAVIGATE.EDIT_TRIP_ROUTE, { trip_id });
  };

  openModal = () => {
    this.setState({ isVisible: true });
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  closeModal = (response = false) => {
    this.setState({ isVisible: false }, () => {
      if (response === true) {
        this.goBack();
      }
    });
  };

  renderTouchingActions = () => {
    const { trip, loggedInUser, cities } = this.props;
    const { basicInfo: { display_flag, city_id } = {}, status } = trip || {};
    const { basicInfo: { code } = {} } = cities[city_id] || {};
    const { markingTripOut, markingReachedAtDestination } = this.state;

    if (
      hasRights(USER_RIGHTS.TOUCHING_IN, loggedInUser) &&
      display_flag === TRIP_FLAG.IN
    )
      return (
        <TouchableOpacity
          onPress={this.openTripInDatePicker}
          style={styles.center}
        >
          {this.insertIcon("arrow-collapse-right")}
          <Text style={styles.bottomFont}>In {code}</Text>
        </TouchableOpacity>
      );
    else if (
      hasRights(USER_RIGHTS.TOUCHING_OUT, loggedInUser) &&
      display_flag === TRIP_FLAG.OUT
    ) {
      return (
        <TouchableOpacity
          onPress={this.openTripOutDatePicker}
          style={styles.center}
        >
          {markingTripOut && (
            <ActivityIndicator size="small" color={colors.darkblue} />
          )}
          {!markingTripOut && this.insertIcon("arrow-expand-right")}
          <Text style={styles.bottomFont}>Out {code}</Text>
        </TouchableOpacity>
      );
    } else if (
      hasRights(USER_RIGHTS.REACHED_DESTINATION, loggedInUser) &&
      display_flag === TRIP_FLAG.ARRIVED
    ) {
      return (
        <TouchableOpacity
          onPress={this.openReachedAtDestinationPicker}
          style={styles.center}
        >
          {markingReachedAtDestination && (
            <ActivityIndicator size="small" color={colors.darkblue} />
          )}
          {!markingReachedAtDestination &&
            this.insertIcon("arrow-down-circle-outline")}
          <Text style={styles.bottomFont}>Reached Dest</Text>
        </TouchableOpacity>
      );
    }
  };

  render() {
    const { trip, loggedInUser } = this.props;
    const { basicInfo: { lower_bound } = {}, status } = trip || {};
    const { markingBegin } = this.state;
    return (
      <Fragment>
        <View style={{ height: 60, backgroundColor: colors.black02 }}>
          <ScrollView
            horizontal={true}
            contentContainerStyle={styles.bottomContainer}
            showsHorizontalScrollIndicator={false}
          >
            {hasRights(USER_RIGHTS.BEGIN_TRIP, loggedInUser) &&
              (status === TRIP_STATUS_TABLE.CREATED ||
                status === TRIP_STATUS_TABLE.HALTED) && (
                <TouchableOpacity
                  onPress={this.openDatePicker}
                  style={styles.center}
                >
                  {markingBegin && (
                    <ActivityIndicator size="small" color={colors.darkblue} />
                  )}
                  {!markingBegin && this.insertIcon("truck-delivery")}
                  <Text style={styles.bottomFont}>Begin</Text>
                </TouchableOpacity>
              )}

            <TouchableOpacity onPress={this.openHistory} style={styles.center}>
              {this.insertIcon("history")}
              <Text style={styles.bottomFont}>History</Text>
            </TouchableOpacity>

            {hasRights(USER_RIGHTS.EDIT_TRIP_VEHICLE, loggedInUser) &&
              (status === TRIP_STATUS_TABLE.IN_TRANSIT ||
                status === TRIP_STATUS_TABLE.TOUCHING ||
                status === TRIP_STATUS_TABLE.HALTED) && (
                <TouchableOpacity
                  onPress={this.openCrossDock}
                  style={styles.center}
                >
                  {this.insertIcon("call-merge")}
                  <Text style={styles.bottomFont}>Cross Dock</Text>
                </TouchableOpacity>
              )}

            {(status === TRIP_STATUS_TABLE.IN_TRANSIT ||
              status === TRIP_STATUS_TABLE.HALTED ||
              status === TRIP_STATUS_TABLE.TOUCHING ||
              status === TRIP_STATUS_TABLE.ARRIVED) && (
              <TouchableOpacity
                onPress={this.openRunningStatus}
                style={styles.center}
              >
                {this.insertIcon("truck-fast")}
                <Text style={styles.bottomFont}>Running Status</Text>
              </TouchableOpacity>
            )}

            {this.renderTouchingActions()}

            {hasRights(USER_RIGHTS.HALT_TRIP, loggedInUser) &&
              status === TRIP_STATUS_TABLE.IN_TRANSIT && (
                <TouchableOpacity
                  onPress={this.openHaltTrip}
                  style={styles.center}
                >
                  {this.insertIcon("close-outline")}
                  <Text style={styles.bottomFont}>Halt Trip</Text>
                </TouchableOpacity>
              )}

            <TouchableOpacity
              onPress={this.openTripComments}
              style={styles.center}
            >
              {this.insertIcon("comment-outline")}
              <Text style={styles.bottomFont}>Comment</Text>
            </TouchableOpacity>

            {hasRights(USER_RIGHTS.EDIT_TRIP_VEHICLE, loggedInUser) &&
              status === TRIP_STATUS_TABLE.CREATED && (
                <TouchableOpacity
                  onPress={this.goToEditTrip}
                  style={styles.center}
                >
                  {this.insertIcon("file-document-edit")}
                  <Text style={styles.bottomFont}>Edit Vehicle</Text>
                </TouchableOpacity>
              )}

            {hasRights(USER_RIGHTS.EDIT_TRIP_ROUTE, loggedInUser) &&
              (status === TRIP_STATUS_TABLE.CREATED ||
                status === TRIP_STATUS_TABLE.IN_TRANSIT ||
                status === TRIP_STATUS_TABLE.HALTED ||
                status === TRIP_STATUS_TABLE.TOUCHING) && (
                <TouchableOpacity
                  onPress={this.goToEditRoute}
                  style={styles.center}
                >
                  {this.insertIcon("map-marker-plus")}
                  <Text style={styles.bottomFont}>Edit Route</Text>
                </TouchableOpacity>
              )}

            {hasRights(USER_RIGHTS.TERMINATE_TRIP, loggedInUser) &&
              (status !== TRIP_STATUS_TABLE.ARRIVED &&
                status !== TRIP_STATUS_TABLE.CLOSED &&
                status !== TRIP_STATUS_TABLE.TERMINATED) && (
                <TouchableOpacity
                  onPress={this.openModal}
                  style={styles.center}
                >
                  {this.insertIcon("truck-trailer")}
                  <Text style={styles.bottomFont}>Terminate</Text>
                </TouchableOpacity>
              )}

            {hasRights(USER_RIGHTS.CLOSE_TRIP, loggedInUser) &&
              status === TRIP_STATUS_TABLE.ARRIVED && (
                <TouchableOpacity
                  onPress={this.openTripClosePicker}
                  style={styles.center}
                >
                  {markingBegin && (
                    <ActivityIndicator size="small" color={colors.darkblue} />
                  )}
                  {!markingBegin && this.insertIcon("truck-check")}
                  <Text style={styles.bottomFont}>Close</Text>
                </TouchableOpacity>
              )}
          </ScrollView>
        </View>

        {/*Set Date Time*/}
        <DatePicker
          ref={picker => {
            this.datePicker = picker;
          }}
          date={this.state.dateTime}
          mode="datetime"
          maxDate={new moment().format("YYYY-MM-DD HH:mm")}
          minDate={
            lower_bound && moment(lower_bound).isValid()
              ? new moment(lower_bound).format("YYYY-MM-DD HH:mm")
              : undefined
          }
          placeholder={"Select Date and Time"}
          showIcon={false}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          onDateChange={this.setDateTime}
          style={{ height: 0, width: 0 }}
          hideText={true}
          getDateStr={date => {
            return new moment(date).format("LLL");
          }}
        />

        {/*Set Trip Close Time*/}
        <DatePicker
          ref={picker => {
            this.tripClosePicker = picker;
          }}
          date={this.state.tripCloseTime}
          mode="datetime"
          maxDate={new moment().format("YYYY-MM-DD HH:mm")}
          minDate={
            lower_bound && moment(lower_bound).isValid()
              ? new moment(lower_bound).format("YYYY-MM-DD HH:mm")
              : undefined
          }
          placeholder={"Select Date and Time"}
          showIcon={false}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          onDateChange={this.setTripCloseTime}
          style={{ height: 0, width: 0 }}
          hideText={true}
          getDateStr={date => {
            return new moment(date).format("LLL");
          }}
        />

        {/*TRIP IN*/}
        <DatePicker
          ref={picker => {
            this.tripInDatePicker = picker;
          }}
          date={this.state.tripInTime}
          mode="datetime"
          maxDate={new moment().format("YYYY-MM-DD HH:mm")}
          minDate={
            lower_bound && moment(lower_bound).isValid()
              ? new moment(lower_bound).format("YYYY-MM-DD HH:mm")
              : undefined
          }
          placeholder={"Select Date and Time"}
          showIcon={false}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          onDateChange={this.setTripInTime}
          style={{ height: 0, width: 0 }}
          hideText={true}
          getDateStr={date => {
            return new moment(date).format("LLL");
          }}
        />

        {/*TRIP OUT*/}
        <DatePicker
          ref={picker => {
            this.tripOutDatePicker = picker;
          }}
          date={this.state.tripOutTime}
          mode="datetime"
          maxDate={new moment().format("YYYY-MM-DD HH:mm")}
          minDate={
            lower_bound && moment(lower_bound).isValid()
              ? new moment(lower_bound).format("YYYY-MM-DD HH:mm")
              : undefined
          }
          placeholder={"Select Date and Time"}
          showIcon={false}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          onDateChange={this.setTripOutTime}
          style={{ height: 0, width: 0 }}
          hideText={true}
          getDateStr={date => {
            return new moment(date).format("LLL");
          }}
        />

        {/*REACHED AT DESTINATION*/}
        <DatePicker
          ref={picker => {
            this.reachedAtDestinationPicker = picker;
          }}
          date={this.state.reachedDestinationTime}
          mode="datetime"
          maxDate={new moment().format("YYYY-MM-DD HH:mm")}
          minDate={
            lower_bound && moment(lower_bound).isValid()
              ? new moment(lower_bound).format("YYYY-MM-DD HH:mm")
              : undefined
          }
          placeholder={"Select Date and Time"}
          showIcon={false}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          onDateChange={this.setReachedAtDestinationTime}
          style={{ height: 0, width: 0 }}
          hideText={true}
          getDateStr={date => {
            return new moment(date).format("LLL");
          }}
        />

        {this.state.isVisible && (
          <TerminateModal
            isVisible={this.state.isVisible}
            closeModal={this.closeModal}
            trip_id={this.props.trip_id}
            markTripAsTerminate={this.props.markTripAsTerminate}
          />
        )}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  bottomContainer: {
    height: 60,
    alignSelf: "flex-end",
    // justifyContent: "space-around",
    // alignItems: "center",
    //width: "100%",
    // flexGrow: 1,
    // flex: 0,
    // flexShrink: 0,
    flexDirection: "row"
  },
  center: {
    // flex: 1,
    width: 80,
    justifyContent: "center",
    alignItems: "center"
  },
  bottomFont: {
    fontSize: 10,
    color: colors.darkblue,
    paddingTop: 4,
    fontWeight: "500"
  }
});

export default BottomTab;
