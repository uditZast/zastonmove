import React, { Component, Fragment } from "react";
import { ActivityIndicator, Text, TouchableOpacity, View } from "react-native";
import customStyles from "../../common/Styles";
import colors from "../../common/Colors";
import Modal from "react-native-modal";
import Snackbar from "react-native-snackbar";
import { Icon } from "react-native-elements";
import CustomerInput from "../../common/CustomerInput";

class ChangeCustomer extends Component {
  constructor(props) {
    super(props);
    const { trip } = props;
    const { basicInfo: { customer_id } = {} } = trip || {};
    this.state = {
      isVisible: false,
      selectedCustomer: customer_id ? customer_id : 0,
      updatingCustomer: false
    };
  }

  openModal = () => {
    this.setState({ isVisible: true });
  };

  closeModal = () => {
    const { trip } = this.props;
    const { basicInfo: { customer_id } = {} } = trip || {};
    this.setState({
      isVisible: false,
      updatingCustomer: false,
      selectedCustomer: customer_id
    });
  };

  selectCustomer = selectedCustomer => {
    this.setState({ selectedCustomer });
  };

  updateCustomer = () => {
    const { updateDriverPhone, trip_id } = this.props;
    const { selectedCustomer: customer } = this.state;
    this.setState({ updatingCustomer: true });
    updateDriverPhone({ trip_id, customer })
      .then(result => {
        const { status = false, message = "" } = result;
        if (!status) {
          Snackbar.show({
            title: message,
            duration: Snackbar.LENGTH_LONG
          });
        } else if (status === true) {
          Snackbar.show({
            title: "Customer Updated",
            duration: Snackbar.LENGTH_LONG
          });
          this.setState({ isVisible: false });
        }
      })
      .catch(err => {
        Snackbar.show({
          title: "Unable to Update Customer. Try again later.",
          duration: Snackbar.LENGTH_SHORT
        });
      })
      .finally(() => {
        this.setState({ updatingCustomer: false });
      });
  };

  render() {
    const { updatingCustomer } = this.state;
    const { trip, customers } = this.props;
    const { basicInfo: { customer_id } = {} } = trip || {};
    const { basicInfo: { name: customerName = "-" } = {} } =
      customers[customer_id] || {};

    return (
      <Fragment>
        <Icon
          size={20}
          name="pencil"
          onPress={this.openModal}
          type="material-community"
          underlayColor={"transparent"}
          containerStyle={{ marginLeft: 8 }}
          color={colors.darkblue}
        />
        <Modal
          isVisible={this.state.isVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          animationIn={"slideInUp"}
          style={{
            position: "absolute",
            margin: 0,
            top: 95,
            bottom: 0,
            left: 0,
            right: 0
          }}
        >
          <View style={customStyles.commonModalView}>
            <View style={{ flex: 1 }}>
              <View style={{ padding: 16, paddingTop: 26 }}>
                <Text>Current Customer - {customerName}</Text>
              </View>
              <CustomerInput
                customers={this.props.customers}
                customerIds={this.props.customerIds}
                selectedCustomer={this.state.selectedCustomer}
                selectCustomer={this.selectCustomer}
                disabled={false}
              />
            </View>
            <TouchableOpacity
              style={customStyles.commonButton}
              onPress={this.state.selectedCustomer ? this.updateCustomer : null}
            >
              {updatingCustomer && (
                <ActivityIndicator size="small" color={colors.white} />
              )}
              {!updatingCustomer && (
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: "500",
                    color: colors.white
                  }}
                >
                  CHANGE CUSTOMER
                </Text>
              )}
            </TouchableOpacity>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

export default ChangeCustomer;
