import React, { Component, Fragment } from "react";
import { ActivityIndicator, Text, TouchableOpacity, View } from "react-native";
import NumberInput from "../../common/NumberInput";
import customStyles from "../../common/Styles";
import colors from "../../common/Colors";
import Modal from "react-native-modal";
import Snackbar from "react-native-snackbar";
import { Icon } from "react-native-elements";

class ChangeNumber extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      newTat: null,
      updatingTripTat: false
    };
  }

  openModal = () => {
    this.setState({ isVisible: true });
  };

  closeModal = () => {
    this.setState({
      isVisible: false,
      newTat: null,
      updatingTripTat: false
    });
  };

  setNewTat = newTat => {
    this.setState({ newTat });
  };

  updateTripTat = () => {
    const { updateTripTat, trip_id } = this.props;
    const { newTat: tat } = this.state;
    this.setState({ updatingTripTat: true });
    updateTripTat({ trip_id, tat })
      .then(result => {
        const { status = false, message = "" } = result;
        if (!status) {
          Snackbar.show({
            title: message,
            duration: Snackbar.LENGTH_LONG
          });
        } else if (status === true) {
          Snackbar.show({
            title: "TAT Updated",
            duration: Snackbar.LENGTH_LONG
          });
          this.setState({ isVisible: false });
        }
      })
      .catch(err => {
        Snackbar.show({
          title: "Unable to Update Tat. Try again later.",
          duration: Snackbar.LENGTH_SHORT
        });
      })
      .finally(() => {
        this.setState({ updatingTripTat: false, newTat: null });
      });
  };

  render() {
    const { updatingTripTat } = this.state;
    const { trip } = this.props;
    const { basicInfo: { expected_tat } = {} } = trip || {};

    return (
      <Fragment>
        <Icon
          size={20}
          name="pencil"
          onPress={this.openModal}
          type="material-community"
          underlayColor={"transparent"}
          containerStyle={{ marginLeft: 8 }}
          color={colors.darkblue}
        />
        <Modal
          isVisible={this.state.isVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          animationIn={"slideInUp"}
          style={{
            position: "absolute",
            margin: 0,
            top: 95,
            bottom: 0,
            left: 0,
            right: 0
          }}
        >
          <View style={customStyles.commonModalView}>
            <View style={{ flex: 1 }}>
              <View style={{ padding: 16, paddingTop: 26 }}>
                <Text>Current Expected Tat - {expected_tat}</Text>
              </View>
              <NumberInput
                numericProp={true}
                message={"Update Trip Tat"}
                updateText={this.setNewTat}
                placeholder={"Enter new tat"}
                // onChange={this.onChange}
              />
            </View>
            <TouchableOpacity
              style={customStyles.commonButton}
              onPress={this.state.newTat ? this.updateTripTat : null}
            >
              {updatingTripTat && (
                <ActivityIndicator size="small" color={colors.white} />
              )}
              {!updatingTripTat && (
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: "500",
                    color: colors.white
                  }}
                >
                  CHANGE TAT
                </Text>
              )}
            </TouchableOpacity>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

export default ChangeNumber;
