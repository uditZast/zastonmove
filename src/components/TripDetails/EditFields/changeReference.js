import React, { Component, Fragment } from "react";
import { ActivityIndicator, Text, TouchableOpacity, View } from "react-native";
import NumberInput from "../../common/NumberInput";
import customStyles from "../../common/Styles";
import colors from "../../common/Colors";
import Modal from "react-native-modal";
import Snackbar from "react-native-snackbar";
import { Icon } from "react-native-elements";

class ChangeReference extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      newReference: null,
      updatingReference: false
    };
  }

  openModal = () => {
    this.setState({ isVisible: true });
  };

  closeModal = () => {
    this.setState({
      isVisible: false,
      newReference: null,
      updatingReference: false
    });
  };

  setDriverNumber = newReference => {
    this.setState({ newReference });
  };

  updateReference = () => {
    const { updateReference, trip_id } = this.props;
    const { newReference: reference } = this.state;
    if (reference) {
      this.setState({ updatingReference: true });
      updateReference({ trip_id, reference })
        .then(result => {
          const { status = false, message = "" } = result;
          if (!status) {
            Snackbar.show({
              title: message,
              duration: Snackbar.LENGTH_LONG
            });
          } else if (status === true) {
            Snackbar.show({
              title: "Reference Updated",
              duration: Snackbar.LENGTH_LONG
            });
            this.setState({ isVisible: false });
          }
        })
        .catch(err => {
          Snackbar.show({
            title: "Unable to Update Reference. Try again later.",
            duration: Snackbar.LENGTH_SHORT
          });
        })
        .finally(() => {
          this.setState({ updatingReference: false, newReference: null });
        });
    } else {
      Snackbar.show({
        title: "Please Enter reference",
        duration: Snackbar.LENGTH_LONG
      });
    }
  };

  render() {
    const { updatingReference } = this.state;
    const { trip } = this.props;
    const { basicInfo: { reference = "-" } = {} } = trip || {};

    return (
      <Fragment>
        <Icon
          size={20}
          name="pencil"
          onPress={this.openModal}
          type="material-community"
          underlayColor={"transparent"}
          // containerStyle={{ }}
          color={colors.darkblue}
        />
        <Modal
          isVisible={this.state.isVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          animationIn={"slideInUp"}
          style={{
            position: "absolute",
            margin: 0,
            top: 95,
            bottom: 0,
            left: 0,
            right: 0
          }}
        >
          <View style={customStyles.commonModalView}>
            <View style={{ flex: 1 }}>
              <View style={{ padding: 16, paddingTop: 26 }}>
                <Text>Current Reference - {`${reference}`}</Text>
              </View>
              <NumberInput
                numericProp={true}
                message={"Update Reference"}
                updateText={this.setDriverNumber}
                placeholder={"Enter New Reference Number"}
              />
            </View>
            <TouchableOpacity
              style={customStyles.commonButton}
              onPress={this.state.newReference ? this.updateReference : null}
            >
              {updatingReference && (
                <ActivityIndicator size="small" color={colors.white} />
              )}
              {!updatingReference && (
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: "500",
                    color: colors.white
                  }}
                >
                  CHANGE REFERENCE
                </Text>
              )}
            </TouchableOpacity>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

export default ChangeReference;
