import React, { Component, Fragment } from "react";
import { ActivityIndicator, Text, TouchableOpacity, View } from "react-native";
import NumberInput from "../../common/NumberInput";
import customStyles from "../../common/Styles";
import colors from "../../common/Colors";
import Modal from "react-native-modal";
import Snackbar from "react-native-snackbar";
import { Icon } from "react-native-elements";

class ChangeNumber extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      newPhoneNumber: null,
      updatingDriverPhone: false
    };
  }

  openModal = () => {
    this.setState({ isVisible: true });
  };

  closeModal = () => {
    this.setState({
      isVisible: false,
      newPhoneNumber: null,
      updatingDriverPhone: false
    });
  };

  setDriverNumber = newPhoneNumber => {
    this.setState({ newPhoneNumber });
  };

  updateDriverNumber = () => {
    const { updateDriverPhone, trip_id } = this.props;
    const { newPhoneNumber: phone } = this.state;
    this.setState({ updatingDriverPhone: true });
    updateDriverPhone({ trip_id, phone })
      .then(result => {
        const { status = false, message = "" } = result;
        if (!status) {
          Snackbar.show({
            title: message,
            duration: Snackbar.LENGTH_LONG
          });
        } else if (status === true) {
          Snackbar.show({
            title: "Number Updated",
            duration: Snackbar.LENGTH_LONG
          });
          this.setState({ isVisible: false });
        }
      })
      .catch(err => {
        Snackbar.show({
          title: "Unable to Update Phone Number. Try again later.",
          duration: Snackbar.LENGTH_SHORT
        });
      })
      .finally(() => {
        this.setState({ updatingDriverPhone: false, newPhoneNumber: null });
      });
  };

  render() {
    const { updatingDriverPhone } = this.state;
    const { trip } = this.props;
    const { basicInfo: { phone } = {} } = trip || {};

    return (
      <Fragment>
        <Icon
          size={20}
          name="pencil"
          onPress={this.openModal}
          type="material-community"
          underlayColor={"transparent"}
          containerStyle={{ marginLeft: 8 }}
          color={colors.darkblue}
        />
        <Modal
          isVisible={this.state.isVisible}
          onBackButtonPress={this.closeModal}
          onBackdropPress={this.closeModal}
          animationIn={"slideInUp"}
          style={{
            position: "absolute",
            margin: 0,
            top: 95,
            bottom: 0,
            left: 0,
            right: 0
          }}
        >
          <View style={customStyles.commonModalView}>
            <View style={{ flex: 1 }}>
              <View style={{ padding: 16, paddingTop: 26 }}>
                <Text>Current Phone Number - {phone}</Text>
              </View>
              <NumberInput
                numericProp={true}
                message={"Update Driver Phone"}
                updateText={this.setDriverNumber}
                placeholder={"Enter Driver Phone Number"}
                // onChange={this.onChange}
              />
            </View>
            <TouchableOpacity
              style={customStyles.commonButton}
              onPress={
                this.state.newPhoneNumber ? this.updateDriverNumber : null
              }
            >
              {updatingDriverPhone && (
                <ActivityIndicator size="small" color={colors.white} />
              )}
              {!updatingDriverPhone && (
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: "500",
                    color: colors.white
                  }}
                >
                  CHANGE NUMBER
                </Text>
              )}
            </TouchableOpacity>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

export default ChangeNumber;
