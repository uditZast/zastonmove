import React, { Component, Fragment } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import Header from "../common/header";
import customStyles from "../common/Styles";
import colors from "../common/Colors";
import { Icon } from "react-native-elements";
import { callNumber } from "../common/callNumber";
import moment from "moment";
import BottomTab from "./bottomTab";
import { hasRights } from "../common/checkRights";
import {
  NAVIGATE,
  TRIP_STATUS,
  TRIP_STATUS_TABLE,
  USER_RIGHTS
} from "../../../constants";
import MapView, { Marker } from "react-native-maps";
import ChangeNumber from "./EditFields/changeNumber";
import ChangeReference from "./EditFields/changeReference";
import ChangeTat from "./EditFields/changeTat";
import MapModal from "../common/MapModal";
import MapRouteModal from "../common/MapRouteModal";

class TripDetails extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: false, plotRouteVisible: false, plotList: [] };
  }

  componentDidMount() {
    const { plotTripRoute, trip_id } = this.props;
    plotTripRoute({ trip_id }).then(response => {
      const { data: { plot_coordinates = [] } = {} } = response;
      console.log("response ----", response);
      this.setState({ plotList: plot_coordinates });
    });
  }

  closeModal = () => {
    this.setState({ visible: false });
  };

  openModal = () => {
    this.setState({ visible: true });
  };

  closePlotRouteModal = () => {
    this.setState({ plotRouteVisible: false });
  };

  openPlotRouteModal = () => {
    this.setState({ plotRouteVisible: true });
  };

  makeCall = phone => () => {
    if (phone) {
      callNumber(phone);
    }
  };

  goToEditCustomer = () => {
    const { trip_id } = this.props;
    this.props.navigation.navigate(NAVIGATE.EDIT_TRIP_CUSTOMER, { trip_id });
  };

  render() {
    const {
      trip,
      customers,
      vehicles,
      vendors,
      vehicleTypes,
      loggedInUser,
      engagedBy
    } = this.props;
    const {
      basicInfo: {
        code,
        type,
        route,
        vehicle_id,
        vehicle_type_id,
        customer_id,
        vendor_id,
        phone,
        recorded_at,
        reference = "-",
        location,
        lat,
        long,
        engaged_by_id,
        expected_tat
      } = {},
      status
    } = trip || {};
    const recordedLast = moment(recorded_at);
    const {
      basicInfo: {
        code: customerCode = "Dry Run",
        name: customerName = "-"
      } = {}
    } = customers[customer_id] || {};
    const { basicInfo: { vehicle_number = "-" } = {} } =
      vehicles[vehicle_id] || {};
    const {
      basicInfo: {
        code: vendorCode = "-",
        name: vendorName = "-",
        nick_name
      } = {}
    } = vendors[vendor_id] || {};
    const { basicInfo: { name: vehicleType = "-" } = {} } =
      vehicleTypes[vehicle_type_id] || {};
    const { basicInfo: { name: engaged_by } = {} } =
      engagedBy[engaged_by_id] || {};
    const { visible, plotList } = this.state;

    return (
      <Fragment>
        <Header name={code} navigation={this.props.navigation} goBack={true} />
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
              <ScrollView contentContainerStyle={styles.scrollView}>
                <View style={styles.cardContainer}>
                  <View style={[styles.row, { paddingTop: 0 }]}>
                    <View style={customStyles.flex1}>
                      <Text style={styles.headingText}>Trip Code</Text>
                      <Text style={styles.valueText}>
                        {code} ({type})
                      </Text>
                    </View>
                    <View style={customStyles.flex1}>
                      <Text style={styles.headingText}>Customer</Text>
                      <View style={{ flexDirection: "row", width: "100%" }}>
                        <Text
                          style={styles.valueText}
                          numberOfLines={1}
                        >{`${customerCode}`}</Text>

                        {hasRights(USER_RIGHTS.EDIT_BOOKING, loggedInUser) &&
                          type !== "DR" &&
                          (status !== TRIP_STATUS_TABLE.TERMINATED &&
                            status !== TRIP_STATUS_TABLE.CLOSED) && (
                            <Icon
                              size={20}
                              name="pencil"
                              onPress={this.goToEditCustomer}
                              type="material-community"
                              underlayColor={"transparent"}
                              containerStyle={{ marginLeft: 8 }}
                              color={colors.darkblue}
                            />
                          )}
                        {/*<ChangeCustomer {...this.props} />*/}
                      </View>
                    </View>
                  </View>

                  <View style={styles.row}>
                    <View style={customStyles.flex1}>
                      <Text style={styles.headingText}>Vehicle Number</Text>
                      <Text
                        style={styles.valueText}
                      >{`${vehicle_number} (${vehicleType})`}</Text>
                    </View>
                    <View style={customStyles.flex1}>
                      <Text style={styles.headingText}>Vendor</Text>
                      <Text
                        style={styles.valueText}
                        numberOfLines={1}
                      >{`${nick_name}`}</Text>
                      {/*<Text*/}
                      {/*  style={styles.valueText}*/}
                      {/*  numberOfLines={1}*/}
                      {/*>{`${vendorCode} (${vendorName})`}</Text>*/}
                    </View>
                  </View>

                  <View style={styles.column}>
                    <Text style={styles.headingText}>Route</Text>
                    <Text style={styles.valueText}>{route}</Text>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <View style={[styles.paddingTB, { flex: 0.5 }]}>
                      <Text style={styles.headingText}>Driver Number</Text>
                      <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 1, paddingRight: 4 }}>
                          <TouchableOpacity
                            onPress={this.makeCall(phone)}
                            style={{ flexDirection: "row" }}
                          >
                            <Icon
                              size={20}
                              name={"phone-in-talk"}
                              type="material-community"
                              containerStyle={{ marginRight: 8 }}
                              color={colors.darkblue}
                            />
                            <View style={{ flex: 1 }}>
                              <Text style={styles.valueText} numberOfLines={1}>
                                {phone}
                              </Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                        <View style={{ paddingRight: 8 }}>
                          {hasRights(
                            USER_RIGHTS.EDIT_VEHICLE_PHONE,
                            loggedInUser
                          ) && <ChangeNumber {...this.props} />}
                        </View>
                      </View>
                    </View>

                    {status && (
                      <View style={[styles.paddingTB, { flex: 0.5 }]}>
                        <Text style={styles.headingText}>Status</Text>
                        <Text style={styles.valueText} numberOfLines={1}>
                          {status === "CR"
                            ? "Created"
                            : status === "HL"
                            ? "Halted"
                            : status === "IN"
                            ? "In-Transit"
                            : status === "CL"
                            ? "Closed"
                            : status === "AI"
                            ? "Touching"
                            : status === "AR"
                            ? "Arrived at destination"
                            : status === "TM"
                            ? "Terminated"
                            : "Status"}
                        </Text>
                      </View>
                    )}
                  </View>
                  <View style={[styles.paddingTB, { flexDirection: "row" }]}>
                    <View style={{ flex: 0.5 }}>
                      <Text style={styles.headingText}>Reference</Text>
                      <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 1, paddingRight: 4 }}>
                          <Text style={styles.valueText} numberOfLines={1}>
                            {`${reference}`}
                          </Text>
                        </View>
                        <View style={{ paddingRight: 8 }}>
                          {hasRights(USER_RIGHTS.EDIT_BOOKING, loggedInUser) &&
                            (status !== TRIP_STATUS_TABLE.TERMINATED &&
                              status !== TRIP_STATUS_TABLE.CLOSED) && (
                              <ChangeReference {...this.props} />
                            )}
                        </View>
                      </View>
                    </View>

                    {engaged_by && (
                      <View style={{ flex: 0.5 }}>
                        <Text style={styles.headingText}>Engaged By</Text>
                        <Text style={styles.valueText} numberOfLines={1}>
                          {`${engaged_by}`}
                        </Text>
                      </View>
                    )}
                  </View>

                  {expected_tat && (
                    <View style={[styles.paddingTB, { flexDirection: "row" }]}>
                      <View style={{ flex: 0.5 }}>
                        <Text style={styles.headingText}>Expected TAT</Text>
                        <View style={{ flexDirection: "row" }}>
                          <View style={{ flex: 1, paddingRight: 4 }}>
                            <Text style={styles.valueText} numberOfLines={1}>
                              {`${expected_tat}`}
                            </Text>
                          </View>
                          <View style={{ paddingRight: 8 }}>
                            {hasRights(USER_RIGHTS.EDIT_TAT, loggedInUser) &&
                              (status !== TRIP_STATUS_TABLE.TERMINATED &&
                                status !== TRIP_STATUS_TABLE.CLOSED) && (
                                <ChangeTat {...this.props} />
                              )}
                          </View>
                        </View>
                      </View>
                    </View>
                  )}

                  {location && (
                    <View style={styles.paddingTB}>
                      <Text style={styles.headingText}>Last Location</Text>
                      <TouchableOpacity
                        style={{
                          flexDirection: "row",
                          paddingBottom: 4,
                          alignItems: "center",
                          flex: 1
                        }}
                        onPress={this.openModal}
                      >
                        <Icon
                          size={25}
                          name="crosshairs-gps"
                          type="material-community"
                          containerStyle={{ paddingRight: 8 }}
                          color={colors.blue}
                        />
                        <View style={{ flex: 1 }}>
                          <Text
                            style={{
                              fontSize: 14,
                              fontWeight: "400",
                              color: colors.blue
                            }}
                            numberOfLines={2}
                          >
                            {`${location} `}
                            {recordedLast.isValid() && (
                              <Text
                                style={{
                                  color: new moment(recordedLast)
                                    .add(10, "minutes")
                                    .isAfter(new moment())
                                    ? colors.green
                                    : colors.red
                                }}
                              >
                                ({recordedLast.fromNow(true)})
                              </Text>
                            )}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  )}

                  {plotList.length > 0 && (
                    <View style={styles.paddingTB}>
                      <TouchableOpacity
                        style={{
                          flexDirection: "row",
                          paddingBottom: 4,
                          alignItems: "center",
                          flex: 1
                        }}
                        onPress={this.openPlotRouteModal}
                      >
                        <Icon
                          size={25}
                          name="location-arrow"
                          type="font-awesome"
                          // type="material-community"
                          containerStyle={{ paddingRight: 8 }}
                          color={colors.blue}
                        />
                        <Text
                          style={{
                            fontSize: 14,
                            fontWeight: "400",
                            color: colors.blue
                          }}
                          numberOfLines={1}
                        >
                          Plot Route
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )}
                  {/*{location && (*/}
                  {/*  <TouchableOpacity*/}
                  {/*    style={styles.paddingTB}*/}
                  {/*    onPress={this.openModal}*/}
                  {/*  >*/}
                  {/*    <Text style={styles.headingText}>Last Location</Text>*/}
                  {/*    <Text*/}
                  {/*      style={[styles.valueText, { color: colors.blue }]}*/}
                  {/*      numberOfLines={2}*/}
                  {/*    >*/}
                  {/*      {`${location} `}*/}
                  {/*      {recordedLast.isValid() && (*/}
                  {/*        <Text*/}
                  {/*          style={{*/}
                  {/*            color: new moment(recordedLast)*/}
                  {/*              .add(10, "minutes")*/}
                  {/*              .isAfter(new moment())*/}
                  {/*              ? colors.green*/}
                  {/*              : colors.red*/}
                  {/*          }}*/}
                  {/*        >*/}
                  {/*          ({recordedLast.fromNow(true)})*/}
                  {/*        </Text>*/}
                  {/*      )}*/}
                  {/*    </Text>*/}
                  {/*  </TouchableOpacity>*/}
                  {/*)}*/}
                </View>
              </ScrollView>
            </View>
            {lat && long && (
              <MapModal
                location={location}
                lat={lat}
                long={long}
                isVisible={visible}
                closeModal={this.closeModal}
              />
            )}

            {plotList.length > 0 && (
              <MapRouteModal
                plotList={plotList}
                isVisible={this.state.plotRouteVisible}
                closeModal={this.closePlotRouteModal}
              />
            )}
          </View>
          <BottomTab {...this.props} />
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: { flexGrow: 1, backgroundColor: colors.black02 },
  map: { flex: 1 },
  cardContainer: {
    padding: 16
    // paddingBottom: 60
  },
  paddingTB: { paddingTop: 6, paddingBottom: 6 },
  column: {
    paddingTop: 6,
    paddingBottom: 6
  },
  row: { flexDirection: "row", paddingTop: 6, paddingBottom: 6 },
  iconContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  headingText: {
    fontSize: 12,
    paddingBottom: 2,
    color: colors.black65
  },
  valueText: {
    fontSize: 14,
    paddingTop: 2,
    color: colors.black85
  }
});

export default TripDetails;
