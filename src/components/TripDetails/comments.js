import React, { Component, Fragment } from "react";
import {
  ActivityIndicator,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import Header from "../common/header";
import CustomStyles from "../common/Styles";
import colors from "../common/Colors";
import { TRIP_ENTITIES, USER_RIGHTS } from "../../../constants";
import Snackbar from "react-native-snackbar";
import { hasRights } from "../common/checkRights";

class TripComments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: null,
      savingComment: false,
      loading: true
    };
  }

  componentDidMount() {
    console.log("this.props.trip_id comments----", this.props.trip_id);
    const { fetchTripComments, trip_id } = this.props;
    fetchTripComments({ trip_id, entity: TRIP_ENTITIES.COMMENT }).finally(() =>
      this.setState({ loading: false })
    );
  }

  addComment = () => {
    const { addTripComment, trip_id } = this.props;
    const { input: comment } = this.state;
    this.setState({ savingComment: true });
    addTripComment({ trip_id, comment })
      .then(result => {
        const { status = false } = result;
        if (!status) {
          Snackbar.show({
            title: "Unable to add comment. Try again later.",
            duration: Snackbar.LENGTH_SHORT
          });
        } else if (status === true) {
          Snackbar.show({
            title: "Comment Added",
            duration: Snackbar.LENGTH_LONG
          });
        }
      })
      .catch(err => {
        Snackbar.show({
          title: "Unable to add comment. Try again later.",
          duration: Snackbar.LENGTH_SHORT
        });
      })
      .finally(() => {
        this.setState({ savingComment: false, input: null });
      });
  };

  renderComment = comment => {
    const { time, user, comments } = comment;
    return (
      <Fragment>
        <View
          style={{
            backgroundColor: colors.black02,
            marginBottom: 10,
            borderRadius: 6,
            borderWidth: 1,
            borderColor: colors.black15,
            padding: 8
          }}
        >
          <View>
            <Text>{comments}</Text>
          </View>
          <View style={{ alignSelf: "flex-end", paddingTop: 8 }}>
            <Text>
              -by <Text style={{ color: colors.blue }}>{user}</Text>
            </Text>
          </View>
          <View style={{ alignSelf: "flex-end", paddingTop: 4 }}>
            <Text>
              at <Text style={{ color: colors.blue }}>{time}</Text>
            </Text>
          </View>
        </View>
      </Fragment>
    );
  };

  render() {
    const { trip, tripComments, loggedInUser } = this.props;
    const { basicInfo: { code, trip_comment_id } = {} } = trip || {};
    const { data = [] } = tripComments[trip_comment_id] || {};
    const { input, savingComment, loading } = this.state;

    return (
      <Fragment>
        <Header name={code} navigation={this.props.navigation} goBack={true} />
        <View style={CustomStyles.flex1}>
          <View style={{ flex: 1 }}>
            {hasRights(USER_RIGHTS.ADD_TRIP_COMMENT, loggedInUser) && (
              <View
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: colors.blue,
                  margin: 10,
                  marginTop: 20,
                  marginBottom: 0,
                  height: 70
                }}
              >
                <Text style={CustomStyles.labelText}>Add Comment</Text>
                <TextInput
                  placeholder={"Add Comment"}
                  style={{
                    flex: 1,
                    alignItems: "flex-start",
                    paddingLeft: 10,
                    paddingRight: 10,
                    justifyContent: "center",
                    height: 70
                  }}
                  multiline={true}
                  onChangeText={text => this.setState({ input: text })}
                  value={this.state.input}
                  placeholderTextColor={colors.black25}
                />
              </View>
            )}

            {loading && (
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <ActivityIndicator size="large" color={colors.darkblue} />
              </View>
            )}
            {!loading && (
              <ScrollView
                contentContainerStyle={{
                  flexGrow: 1,
                  padding: 10,
                  paddingTop: 0
                }}
              >
                {data &&
                  data.length > 0 &&
                  data.map((item, index) => {
                    return (
                      <View
                        key={index}
                        style={index === 0 ? { marginTop: 10 } : {}}
                      >
                        {this.renderComment(item)}
                      </View>
                    );
                  })}
                {!data ||
                  (data.length === 0 && (
                    <View
                      style={{
                        flex: 1,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text>NO COMMENTS</Text>
                    </View>
                  ))}
              </ScrollView>
            )}
          </View>

          {hasRights(USER_RIGHTS.ADD_TRIP_COMMENT, loggedInUser) && (
            <TouchableOpacity
              style={[
                styles.button,
                {
                  backgroundColor:
                    input && input.length > 0 ? colors.blue : colors.black25
                }
              ]}
              onPress={input && input.length > 0 ? this.addComment : null}
            >
              {savingComment && (
                <ActivityIndicator size="small" color={colors.white} />
              )}
              {!savingComment && (
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: "500",
                    color: colors.white
                  }}
                >
                  SAVE COMMENT
                </Text>
              )}
            </TouchableOpacity>
          )}
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    bottom: 0,
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  }
});

export default TripComments;
