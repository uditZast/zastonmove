import React, { Component, Fragment } from "react";
import {
  ActivityIndicator,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { TERMINATE_REASONS_ARRAY } from "../../../constants";
import { CheckBox } from "react-native-elements";
import colors from "../common/Colors";
import customStyles from "../common/Styles";
import Modal from "react-native-modal";
import Snackbar from "react-native-snackbar";

class TerminateModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reasonComment: null,
      reason: null,
      loading: false
    };
  }

  selectReason = reason => () => {
    this.setState({ reason });
  };

  goBack = () => {
    const { closeModal } = this.props;
    closeModal(true);
  };

  terminateTrip = () => {
    const { markTripAsTerminate, trip_id } = this.props;
    const { reasonComment: comment, reason } = this.state;
    if (reason) {
      this.setState({ loading: true });
      markTripAsTerminate({ comment, reason, trip_id })
        .then(result => {
          const { status } = result;
          if (status) {
            this.setState({ isVisible: false }, this.goBack);
          }
        })
        .finally(() => this.setState({ loading: false }));
    } else {
      Snackbar.show({
        title: "Please select a reason",
        duration: Snackbar.LENGTH_SHORT
      });
    }
  };

  render() {
    const { isVisible, closeModal } = this.props;
    const { loading } = this.state;

    return (
      <Fragment>
        <Modal
          isVisible={isVisible}
          onBackButtonPress={closeModal}
          onBackdropPress={closeModal}
          animationIn={"slideInUp"}
          style={{
            position: "absolute",
            margin: 0,
            top: 95,
            bottom: 0,
            left: 0,
            right: 0
          }}
        >
          <View style={styles.modalView}>
            <View style={{ padding: 10, flex: 1 }}>
              {TERMINATE_REASONS_ARRAY.map((reason, index) => {
                return (
                  <Fragment key={index}>
                    <TouchableOpacity
                      style={styles.checkboxContainer}
                      onPress={this.selectReason(reason)}
                    >
                      <CheckBox
                        containerStyle={{ padding: 0, margin: 0 }}
                        right
                        checked={`${this.state.reason}` === `${reason}`}
                        checkedIcon="dot-circle-o"
                        uncheckedIcon="circle-o"
                        checkedColor={colors.darkblue}
                        uncheckColor={colors.blue}
                        onPress={this.selectReason(reason)}
                      />
                      <Text>{reason}</Text>
                    </TouchableOpacity>
                  </Fragment>
                );
              })}
              <View style={customStyles.border}>
                <Text style={customStyles.labelText}>Other Reason</Text>
                <TextInput
                  placeholder={"Enter Reason"}
                  style={{
                    flex: 1,
                    alignItems: "flex-start",
                    paddingLeft: 10,
                    justifyContent: "center",
                    ...Platform.select({
                      ios: { maxHeight: 30, paddingBottom: 10 },
                      android: { flexGrow: 1, paddingBottom: 0 }
                    })
                  }}
                  onChangeText={text => this.setState({ reasonComment: text })}
                  value={this.state.reasonComment}
                  placeholderTextColor={colors.black25}
                />
              </View>
            </View>
            <TouchableOpacity
              style={styles.button}
              onPress={this.terminateTrip}
            >
              {loading && (
                <ActivityIndicator size="small" color={colors.white} />
              )}
              {!loading && (
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: "500",
                    color: colors.white
                  }}
                >
                  TERMINATE
                </Text>
              )}
            </TouchableOpacity>
          </View>
        </Modal>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  modalView: {
    width: "100%",
    flex: 1,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    alignSelf: "center",
    paddingTop: 10
    // paddingBottom: 30
  },
  button: {
    bottom: 0,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.blue
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 10,
    alignItems: "center"
  }
});

export default TerminateModal;
