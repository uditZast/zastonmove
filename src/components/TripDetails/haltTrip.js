import React, { Component, Fragment } from "react";
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import Header from "../common/header";
import CustomStyles from "../common/Styles";
import colors from "../common/Colors";
import { USER_RIGHTS } from "../../../constants";
import Snackbar from "react-native-snackbar";
import { hasRights } from "../common/checkRights";
import HaltReasonInput from "../common/HaltReasonsInput";
import DateTimeInput from "../common/DateTimeInput";
import moment from "moment";

class HaltTrip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: null,
      markingHalt: false,
      haltDateTime: new moment(),
      enteredDateTime: false,
      selectedHaltReason: 0,
      isDisabled: true
    };
  }

  haltTrip = () => {
    const { markTripAsHalted, trip_id } = this.props;
    const {
      input: comment,
      selectedHaltReason: reason,
      haltDateTime
    } = this.state;
    this.setState({ markingHalt: true });
    markTripAsHalted({
      trip_id,
      comment,
      reason,
      date_time: haltDateTime.format("DD-MM-YYYY HH:mm")
    })
      .then(result => {
        const { status = false } = result;
        if (!status) {
          Snackbar.show({
            title: "Unable to add comment. Try again later.",
            duration: Snackbar.LENGTH_SHORT
          });
        } else {
          Snackbar.show({
            title: "Halt Successful",
            duration: Snackbar.LENGTH_SHORT
          });

          this.goBack();
        }
      })
      .catch(err => {
        Snackbar.show({
          title: "Unable to add comment. Try again later.",
          duration: Snackbar.LENGTH_SHORT
        });
      })
      .finally(() => {
        this.setState({ markingHalt: false, input: null });
      });
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  setHaltReason = selectedHaltReason => {
    this.setState(prevState => {
      if (prevState.enteredDateTime === true && selectedHaltReason !== 0) {
        return { isDisabled: false, selectedHaltReason };
      } else {
        return { isDisabled: true, selectedHaltReason };
      }
    });
  };

  setHaltTime = dateTime => {
    this.setState(prevState => {
      const newState = { haltDateTime: dateTime, enteredDateTime: true };
      if (prevState.selectedHaltReason !== 0) {
        return { ...newState, isDisabled: false };
      } else {
        return { ...newState, isDisabled: true };
      }
    });
  };

  render() {
    const { trip, loggedInUser } = this.props;
    const { basicInfo: { code, lowerBound } = {} } = trip || {};
    const { markingHalt, isDisabled } = this.state;
    console.log("this.state.isDisabled ----", this.state.isDisabled);

    return (
      <Fragment>
        <Header name={code} navigation={this.props.navigation} goBack={true} />
        <View style={CustomStyles.flex1}>
          <View style={{ flex: 1, paddingTop: 10 }}>
            <HaltReasonInput
              haltReasons={this.props.haltReasons}
              selectedHaltReason={this.state.selectedHaltReason}
              setHaltReason={this.setHaltReason}
            />
            <DateTimeInput
              message={"Halt Trip Time"}
              selectedDateTime={this.state.haltDateTime}
              selectDateTime={this.setHaltTime}
              upperbound={true}
              lowerBound={lowerBound ? lowerBound : null}
            />

            <View
              style={{
                borderWidth: 1,
                borderRadius: 3,
                borderColor: colors.blue,
                margin: 10,
                marginTop: 10,
                height: 70
              }}
            >
              <Text style={CustomStyles.labelText}>Add Comment</Text>
              <TextInput
                placeholder={"Add Comment"}
                style={{
                  flex: 1,
                  alignItems: "flex-start",
                  padding: 10,
                  justifyContent: "center",
                  height: 70
                }}
                multiline={true}
                onChangeText={text => this.setState({ input: text })}
                value={this.state.input}
                placeholderTextColor={colors.black25}
              />
            </View>
          </View>

          {hasRights(USER_RIGHTS.HALT_TRIP, loggedInUser) && (
            <TouchableOpacity
              style={[
                styles.button,
                {
                  backgroundColor: !isDisabled ? colors.blue : colors.black25
                }
              ]}
              onPress={!isDisabled ? this.haltTrip : null}
            >
              {markingHalt && (
                <ActivityIndicator size="small" color={colors.white} />
              )}
              {!markingHalt && (
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: "500",
                    color: colors.white
                  }}
                >
                  HALT TRIP
                </Text>
              )}
            </TouchableOpacity>
          )}
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    bottom: 0,
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  }
});

export default HaltTrip;
