import React, { Component, Fragment } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  TextInput
} from "react-native";
import Header from "../common/header";
import CustomStyles from "../common/Styles";
import colors from "../common/Colors";
import {
  RUNNING_STATUS,
  TRIP_ENTITIES,
  TRIP_STATUS_TABLE,
  USER_RIGHTS
} from "../../../constants";
import { hasRights } from "../common/checkRights";
import Snackbar from "react-native-snackbar";
import { CheckBox } from "react-native-elements";
import NumberInput from "../common/NumberInput";
import DelayReasonsInput from "../common/DelayReasonsInput";

class TripRunningStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      hours: 0,
      minutes: 0,
      selectedDelayReason: 0,
      otherReason: null,
      internalReason: null,
      runningStatus: RUNNING_STATUS.ON_TIME,
      savingRunningStatus: false
    };
  }

  componentDidMount() {
    const { fetchTripRunningStatus, trip_id } = this.props;
    fetchTripRunningStatus({
      trip_id,
      entity: TRIP_ENTITIES.RUNNING_STATUS
    }).finally(() => this.setState({ loading: false }));
  }

  addRunningStatus = () => {
    const { addRunningStatus, trip_id } = this.props;
    const {
      runningStatus: status,
      hours,
      minutes,
      selectedDelayReason: reason,
      otherReason: other_reason,
      internalReason: internal_reason
    } = this.state;
    this.setState({ savingRunningStatus: true });
    let data = { trip_id, status };
    if (status !== RUNNING_STATUS.ON_TIME) {
      data = { ...data, hours, minutes, reason, internal_reason, other_reason };
    }
    addRunningStatus(data)
      .then(result => {
        const { status = false, message = "" } = result;
        if (!status) {
          Snackbar.show({
            title: message,
            duration: Snackbar.LENGTH_LONG
          });
        } else if (status === true) {
          Snackbar.show({
            title: "Running Status Added",
            duration: Snackbar.LENGTH_LONG
          });
        }
      })
      .catch(err => {
        Snackbar.show({
          title: "Unable to add running status. Try again later.",
          duration: Snackbar.LENGTH_LONG
        });
      })
      .finally(() => {
        this.setState({
          savingRunningStatus: false,
          hours: 0,
          minutes: 0,
          selectedDelayReason: 0,
          otherReason: null,
          internalReason: null
        });
      });
  };

  selectReason = runningStatus => () => {
    this.setState({ runningStatus });
  };

  updateHours = hours => {
    this.setState({ hours });
  };

  updateMinutes = minutes => {
    this.setState({ minutes });
  };

  setDelayReason = selectedDelayReason => {
    this.setState({ selectedDelayReason });
  };

  renderRunningStatus = runningStatus => {
    const { time, user, reason, status, duration } = runningStatus;
    console.log("runningStatus ----", runningStatus);
    return (
      <Fragment>
        <View
          style={{
            backgroundColor: colors.black02,
            borderRadius: 6,
            borderWidth: 1,
            borderColor: colors.black15,
            padding: 8
          }}
        >
          {String(reason).length > 0 && <Text>{reason}</Text>}
          {String(duration).length > 0 && <Text>{duration}</Text>}
          <View style={{ paddingTop: 8 }}>
            <Text style={{ fontWeight: "500" }}>
              Status: <Text style={{ color: colors.blue }}>{status}</Text>
            </Text>
          </View>
          <View style={{ alignSelf: "flex-end", paddingTop: 8 }}>
            <Text>
              -by <Text style={{ color: colors.blue }}>{user}</Text>
            </Text>
          </View>
          <View style={{ alignSelf: "flex-end", paddingTop: 4 }}>
            <Text>
              at <Text style={{ color: colors.blue }}>{time}</Text>
            </Text>
          </View>
        </View>
      </Fragment>
    );
  };

  render() {
    const { trip, tripRunningStatus, loggedInUser } = this.props;
    const { basicInfo: { code, trip_running_status_id } = {}, status } =
      trip || {};
    const { data = [] } = tripRunningStatus[trip_running_status_id] || {};
    const { loading, savingRunningStatus, runningStatus } = this.state;

    return (
      <Fragment>
        <Header name={code} navigation={this.props.navigation} goBack={true} />
        <View style={CustomStyles.flex1}>
          <View style={{ flex: 1 }}>
            {loading && (
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <ActivityIndicator size="large" color={colors.darkblue} />
              </View>
            )}
            {!loading && (
              <ScrollView
                contentContainerStyle={{ flexGrow: 1, paddingTop: 0 }}
              >
                {hasRights(USER_RIGHTS.ADD_RUNNING_STATUS, loggedInUser) &&
                  (status === TRIP_STATUS_TABLE.IN_TRANSIT ||
                    status === TRIP_STATUS_TABLE.HALTED ||
                    status === TRIP_STATUS_TABLE.TOUCHING) && (
                    <View style={{ flexDirection: "row", height: 50 }}>
                      <TouchableOpacity
                        style={styles.checkboxContainer}
                        onPress={this.selectReason(RUNNING_STATUS.ON_TIME)}
                      >
                        <CheckBox
                          containerStyle={{ padding: 0, margin: 0 }}
                          right
                          checked={runningStatus === RUNNING_STATUS.ON_TIME}
                          checkedIcon="dot-circle-o"
                          uncheckedIcon="circle-o"
                          checkedColor={colors.darkblue}
                          uncheckColor={colors.blue}
                          onPress={this.selectReason(RUNNING_STATUS.ON_TIME)}
                        />
                        <Text>On Time</Text>
                      </TouchableOpacity>

                      <TouchableOpacity
                        style={styles.checkboxContainer}
                        onPress={this.selectReason(RUNNING_STATUS.DELAYED)}
                      >
                        <CheckBox
                          containerStyle={{ padding: 0, margin: 0 }}
                          right
                          checked={runningStatus === RUNNING_STATUS.DELAYED}
                          checkedIcon="dot-circle-o"
                          uncheckedIcon="circle-o"
                          checkedColor={colors.darkblue}
                          uncheckColor={colors.blue}
                          onPress={this.selectReason(RUNNING_STATUS.DELAYED)}
                        />
                        <Text>Delayed</Text>
                      </TouchableOpacity>
                    </View>
                  )}

                {runningStatus === RUNNING_STATUS.DELAYED && (
                  <Fragment>
                    <View style={{ flexDirection: "row" }}>
                      <View style={{ flex: 1 }}>
                        <NumberInput
                          numericProp={true}
                          initialValue={`${this.state.hours}`}
                          message={"Hours"}
                          updateText={this.updateHours}
                          placeholder={"Enter Hours"}
                        />
                      </View>
                      <View style={{ flex: 1 }}>
                        <NumberInput
                          numericProp={true}
                          initialValue={`${this.state.minutes}`}
                          message={"Minutes"}
                          updateText={this.updateMinutes}
                          placeholder={"Enter Minutes"}
                        />
                      </View>
                    </View>
                    <DelayReasonsInput
                      delayReasons={this.props.delayReasons}
                      selectedDelayReason={this.state.selectedDelayReason}
                      setDelayReason={this.setDelayReason}
                    />
                    <View style={styles.inputContainer}>
                      <Text style={CustomStyles.labelText}>Other Reason</Text>
                      <TextInput
                        placeholder={"Other Reason (For Client)"}
                        style={styles.textInputStyle}
                        multiline={true}
                        onChangeText={text =>
                          this.setState({ otherReason: text })
                        }
                        value={this.state.otherReason}
                        placeholderTextColor={colors.black25}
                      />
                    </View>
                    <View style={styles.inputContainer}>
                      <Text style={CustomStyles.labelText}>
                        Internal Reason (for Zast team)
                      </Text>
                      <TextInput
                        placeholder={"Internal Reason for Zast team"}
                        style={styles.textInputStyle}
                        multiline={true}
                        onChangeText={text =>
                          this.setState({ internalReason: text })
                        }
                        value={this.state.internalReason}
                        placeholderTextColor={colors.black25}
                      />
                    </View>
                  </Fragment>
                )}

                {data &&
                  data.length > 0 &&
                  data.map((item, index) => {
                    return (
                      <View key={index} style={{ margin: 10, marginTop: 0 }}>
                        {this.renderRunningStatus(item)}
                      </View>
                    );
                  })}
                {!data || (data.length === 0 && <Text>NO RUNNING STATUS</Text>)}
              </ScrollView>
            )}
          </View>
          {hasRights(USER_RIGHTS.ADD_RUNNING_STATUS, loggedInUser) &&
            (status === TRIP_STATUS_TABLE.IN_TRANSIT ||
              status === TRIP_STATUS_TABLE.HALTED ||
              status === TRIP_STATUS_TABLE.TOUCHING) && (
              <TouchableOpacity
                style={styles.button}
                onPress={this.addRunningStatus}
              >
                {savingRunningStatus && (
                  <ActivityIndicator size="small" color={colors.white} />
                )}
                {!savingRunningStatus && (
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: "500",
                      color: colors.white
                    }}
                  >
                    Add Running Status
                  </Text>
                )}
              </TouchableOpacity>
            )}
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    bottom: 0,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.blue
  },
  checkboxContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center"
  },
  inputContainer: {
    borderWidth: 1,
    borderRadius: 3,
    borderColor: colors.blue,
    margin: 10,
    height: 70
  },
  textInputStyle: {
    flex: 1,
    alignItems: "flex-start",
    padding: 10,
    justifyContent: "center",
    height: 70
  }
});

export default TripRunningStatus;
