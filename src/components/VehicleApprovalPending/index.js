import React, { Component, Fragment } from "react";
import { FlatList, Platform, StyleSheet, View } from "react-native";
import colors from "../../colors/Colors";
import Header from "../common/header";
import { SearchBar } from "react-native-elements";
import customStyles from "../common/Styles";
import VehicleApprovalCard from "../common/VehicleApprovalCard";

class VehicleApprovalPending extends Component {
  constructor(props) {
    super(props);
    this.state = { selectedIndex: 0, input: null };
  }

  componentDidMount() {
    const { loggedInUser } = this.props;
    const { role } = loggedInUser;
    // this.fetchVendorApprovalPending();
  }

  fetchVehicleApprovalPending = () => {
    const { fetchVehicleApprovalPending } = this.props;
    fetchVehicleApprovalPending();
  };

  clearInput = () => {
    this.setState({ input: null });
  };

  setSearch = text => {
    this.setState({ input: text });
  };

  renderVehicleItem = ({ item: vehicle_id, index }) => {
    const { loggedInUser, users } = this.props;

    const { vehicles, navigation } = this.props;
    const { vehicleTypes, vendors, cities } = this.props;

    const {
      basicInfo: {
        id,
        vehicle_number,
        vehicle_type_id,
        vehicle_category,
        vendor_id,
        is_active = false,
        is_approved = false,
        round_trip = false,
        phone,
        base_location_id
      } = {}
    } = vehicles[vehicle_id] || {};

    const { basicInfo: { name: vTypeName } = {} } =
      vehicleTypes[vehicle_type_id] || {};

    const { basicInfo: { name: vendorName } = {} } = vendors[vendor_id] || {};

    if (base_location_id) {
      const { basicInfo: { name: cityName } = {} } =
        cities[base_location_id] || {};

      return (
        <View key={index} style={index === 0 ? { paddingTop: 10 } : {}}>
          <VehicleApprovalCard
            id={id}
            navigation={navigation}
            loggedInUser={loggedInUser}
            users={users}
            vNumber={vehicle_number}
            vTypeName={vTypeName}
            category={vehicle_category}
            vendorName={vendorName}
            isActive={is_active}
            isApproved={is_approved}
            roundTrip={round_trip}
            phone={phone}
            cityName={cityName}
          />
        </View>
      );
    } else {
      return (
        <View key={index} style={index === 0 ? { paddingTop: 10 } : {}}>
          <VehicleApprovalCard
            id={id}
            navigation={navigation}
            loggedInUser={loggedInUser}
            users={users}
            vNumber={vehicle_number}
            vTypeName={vTypeName}
            category={vehicle_category}
            vendorName={vendorName}
            isActive={is_active}
            isApproved={is_approved}
            roundTrip={round_trip}
            phone={phone}
          />
        </View>
      );
    }
  };

  selectIndex = selectedIndex => {
    this.setState({ selectedIndex });
  };

  renderVehicleList = () => {
    const { vehicles, vehicleIds } = this.props;

    let list = vehicleIds;

    // let list = Object.keys(vendors);
    const { input } = this.state;
    if (input && input.toUpperCase()) {
      const u_input = input.toUpperCase();
      list = list.filter(vehicle_id => {
        const { basicInfo: { vehicle_number = "" } = {} } =
          vehicles[vehicle_id] || {};
        if (vehicle_number) {
          return vehicle_number.includes(u_input);
        }
      });
    }

    return (
      <FlatList
        data={[...list]}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderVehicleItem}
        keyboardShouldPersistTaps={"handled"}
        showsVerticalScrollIndicator={false}
      />
    );
  };

  componentDidUpdate(prevProps, prevState) {
    const { navigation: { isFocused } = {} } = this.props;
    const didFocus = isFocused();
    const { input } = this.state;

    if (!didFocus && input) {
      this.setState({ input: null });
    }
  }

  render() {
    return (
      <Fragment>
        <Header name={"VEHICLE APPROVAL"} navigation={this.props.navigation} />

        <SearchBar
          placeholder="Search"
          onChangeText={this.setSearch}
          lightTheme
          containerStyle={customStyles.searchBarContainer}
          value={this.state.input}
          onCancel={this.clearInput}
          onClear={this.clearInput}
        />

        {this.renderVehicleList()}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.listbackgroundcolor
  },
  forwardStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    paddingTop: 3
  },
  returnStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginRight: 10,
    marginLeft: 0,
    marginTop: 3
  },
  boldTextStyle: {
    fontSize: 15,
    fontWeight: "bold",
    color: colors.blueColor,
    fontFamily: "CircularStd-Book"
  },
  normalTextStyle: {
    fontSize: 12,
    color: colors.textcolor,
    marginLeft: 5,
    paddingTop: 3,
    fontFamily: "CircularStd-Book"
  },
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    marginTop: 5,
    paddingBottom: 5
  },
  changeColorlineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 5,
    backgroundColor: colors.blue
  },
  rowStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 5
  },
  rowStyle1: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 3
  },
  textStyle1: {
    flex: 0.5,
    fontSize: 12,
    color: colors.textcolor,
    fontFamily: "CircularStd-Book"
  },
  textStyle2: {
    fontSize: 12,
    color: colors.textcolor,
    paddingTop: 3,
    fontFamily: "CircularStd-Book"
  },
  rowStyle2: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 0
  },
  customerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    ...Platform.select({
      ios: {
        padding: 10,
        borderRadius: 10,
        marginTop: 10,
        overflow: "hidden"
      },
      android: {
        paddingTop: 5,
        paddingLeft: 5,
        paddingBottom: 5,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        marginTop: 8
      }
    })
  },
  row1Background: {
    flex: 1,
    flexDirection: "row"
  },
  row2Background: {
    flex: 1,
    paddingBottom: 10
  },
  rowBackgroundColor: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: colors.blue
  },
  columnBackgroundColor: {
    flex: 1,
    backgroundColor: colors.blue
  }
});

export default VehicleApprovalPending;
