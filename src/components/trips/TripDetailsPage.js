import React, { Component } from "react";
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../../colors/Colors";
import TripDetailHeader from "../common/TripDetailHeader";
import routeImage from "../../images/route.png";
import DockModal from "../../modal/DockModal";
import TerminateModal from "../../modal/TerminateModal";
import TripComments from "./tripActions/TripComments";
import AsyncStorage from "@react-native-community/async-storage";
import {
  BOOKING_TRIP_History,
  GET_COORDINATES
} from "../../constants/Constants";
import EditRouteModal from "../../modal/EditRouteModal";
import axios from "axios";
import TouchingModal from "../../modal/TouchingModal";
// import NewTripEditTripModal from "../../modal/NewTripEditTripModal";
import { Icon } from "react-native-elements";
import MapView from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import HaltModal from "../../containers/HaltModal";
import NewTripEditTripModal from "../../containers/NewTripEditTripModal";

let tripId,
  token,
  vehicle,
  speed,
  type,
  tripType,
  customer,
  vendor,
  route,
  status,
  lowerBound,
  touchingFlag,
  touchingDisp,
  touchingCity,
  tat,
  rightsArray = [],
  id,
  distance,
  phone,
  reference,
  startTime,
  transitTime,
  role;

export default class TripDetailsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      jsonData: [],
      delayReasons: [],
      showDockModal: false,
      showTerminateModal: false,
      showCloseModal: false,
      showCommentsModal: false,
      showHaltModal: false,
      heading: "",
      showEditRouteModal: false,
      dataSource: [],
      isLoading: true,
      refreshing: true,
      vehcileEditModal: false,
      crossDocData: [],
      showTouchingModal: false,
      shownewTripEditTripModal: false,
      showDetails: false,
      showMoreButton: true,
      showLessButton: false,
      header2Heading: "",
      mapCoordinates: [],
      defaultLat: 0,
      defaultLng: 0
    };
    this.params = this.props.navigation.state.params;
  }

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;

    tripId = params.tripId;
    token = params.token;
    vehicle = params.vehicle;
    speed = params.speed;
    type = params.type;
    tripType = params.tripType;
    customer = params.customer;
    vendor = params.vendor;
    route = params.route;
    tat = params.tat;
    rightsArray = params.rightsArray;
    id = params.id;
    distance = params.distance;
    role = params.role;

    console.log("tripId::::" + params.tripId);
    console.log("token::::" + params.token);
    console.log("rightsssArrraayyy ::::" + params.rightsArray.length);

    {
      params.lowerBound !== null && params.lowerBound !== undefined
        ? (lowerBound = params.lowerBound)
        : (lowerBound = null);
    }

    {
      params.touchingFlag !== null && params.touchingFlag !== undefined
        ? (touchingFlag = params.touchingFlag.flag)
        : (touchingFlag = null);
    }
    {
      params.touchingFlag !== null && params.touchingFlag !== undefined
        ? (touchingDisp = params.touchingFlag.disp)
        : (touchingDisp = null);
    }
    {
      params.touchingFlag !== null && params.touchingFlag !== undefined
        ? (touchingCity = params.touchingFlag.city)
        : (touchingCity = null);
    }

    {
      params.phone !== null &&
      params.phone !== undefined &&
      params.phone !== " "
        ? (phone = params.phone)
        : null;
    }
    {
      params.reference !== null &&
      params.reference !== undefined &&
      params.reference !== ""
        ? (reference = params.reference)
        : null;
    }
    {
      params.startTime !== null &&
      params.startTime !== undefined &&
      params.startTime !== ""
        ? (startTime = params.startTime)
        : null;
    }
    {
      params.transitTime !== null &&
      params.transitTime !== undefined &&
      params.transitTime !== "" &&
      params.transitTime !== "-"
        ? (transitTime = params.transitTime)
        : null;
    }
    {
      params.status !== null &&
      params.status !== undefined &&
      params.status !== " "
        ? (status = params.status)
        : null;
    }

    return {
      // title: navigation.getParam('Title', params ? tripId : ''),
      headerTitle: (
        <TripDetailHeader
          vehicle={vehicle}
          t_code={tripId}
          speed={speed}
          type={type}
        />
      ),
      headerStyle: {
        backgroundColor: navigation.getParam(
          "BackgroundColor",
          colors.blueColor
        )
      },
      headerTintColor: navigation.getParam("HeaderTintColor", "#fff"),
      //Text color of ActionBar
      headerTitleStyle: {
        fontWeight: "bold",
        fontSize: 13,
        fontFamily: "CircularStd-Book"
      }
    };
  };

  componentDidMount() {
    AsyncStorage.getItem("token").then(value => {
      this.setState({ value: value });
      console.log("token:::::::" + value);

      axios
        .post(
          GET_COORDINATES,
          {
            trip_code: tripId
          },
          {
            headers: {
              Authorization: "Token " + value,
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          this.setState(
            {
              mapCoordinates: response.data.data,
              defaultLat: response.data.data[0].coordinate.latitude,
              defaultLng: response.data.data[0].coordinate.longitude
            },
            () =>
              console.log(
                "mapCoordinates setState:: " +
                  this.state.defaultLat +
                  " " +
                  this.state.defaultLng
              )
          );
        })
        .catch(error => {
          console.log("catch" + error);
          if (error.response) {
            if (error.response.status === 401) {
              this.props.navigation.navigate("Auth");
            }
          }
        });
    });
  }

  handleBackPress = () => {
    console.log("backPress");
    this.props.navigation.goBack(null);
    return true;
  };

  getRunningStatusData() {
    AsyncStorage.getItem("token").then(value => {
      this.setState({ value: value });
      console.log("token:::::::" + value);

      axios
        .post(
          BOOKING_TRIP_History,
          {
            booking_code: null,
            trip_code: tripId,
            entity: "running_status"
          },
          {
            headers: {
              Authorization: "Token " + value,
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          this.setState(
            {
              dataSource: response.data.data,
              isLoading: false,
              refreshing: false
            },
            () =>
              console.log(
                "dataSource setState:: " + this.state.dataSource.length
              )
          );
        })
        .catch(error => {
          console.log("catch" + error);
          if (error.response) {
            if (error.response.status === 401) {
              this.props.navigation.navigate("Auth");
            }
          }
        });
    });
  }

  renderDockModal() {
    return (
      <DockModal
        display={this.state.showDockModal}
        closeModal={() => {
          this.setState({ showDockModal: false });
        }}
        bookingId={null}
        tripId={tripId}
        heading={this.state.heading}
        touchingFlag={touchingFlag}
        touchingCity={touchingCity}
        lowerBound={lowerBound}
        refreshing={() => this.handleBackPress()}
      />
    );
  }

  renderTerminateModal() {
    return (
      <TerminateModal
        display={this.state.showTerminateModal}
        closeModal={() => {
          this.setState({ showTerminateModal: false });
        }}
        tripId={tripId}
        token={token}
        back={() => this.handleBackPress()}
      />
    );
  }

  renderHaltModal() {
    return (
      <HaltModal
        display={this.state.showHaltModal}
        closeModal={() => {
          this.setState({ showHaltModal: false });
        }}
        tripId={tripId}
        token={token}
        lowerBound={lowerBound}
        back={() => this.handleBackPress()}
      />
    );
  }

  renderEditRouteModal() {
    return (
      <EditRouteModal
        display={this.state.showEditRouteModal}
        closeModal={() => {
          this.setState({ showEditRouteModal: false });
        }}
        tripId={tripId}
        token={token}
        route={route}
        tat={tat}
        distance={distance}
        id={id}
        refreshing={() => this.handleBackPress()}
        back={() => this.handleBackPress()}
      />
    );
  }

  renderNewTripEditRouteModal() {
    return (
      <NewTripEditTripModal
        display={this.state.shownewTripEditTripModal}
        closeModal={() => {
          this.setState({ shownewTripEditTripModal: false });
        }}
        tripId={tripId}
        token={token}
        tickVisible={false}
        refreshing={() => this.handleBackPress()}
      />
    );
  }

  renderTouchingModal() {
    return (
      <TouchingModal
        display={this.state.showTouchingModal}
        closeModal={() => {
          this.setState({ showTouchingModal: false });
        }}
        tripId={tripId}
        heading={"Touching Time"}
        touchingCity={touchingCity}
        lowerBound={lowerBound}
        touchingFlag={touchingFlag}
        displayData={touchingDisp}
        refreshing={() => this.handleBackPress()}
      />
    );
  }

  newTripActions() {
    return (
      <View style={{ flexDirection: "row" }}>
        {rightsArray.includes("begin-trip") ? (
          <TouchableOpacity
            onPress={() =>
              this.setState({
                showDockModal: true,
                tripId: tripId,
                heading: "Begin Trip"
              })
            }
          >
            <View
              style={[
                styles.rowstyle,
                { alignItems: "center", justifyContent: "center" }
              ]}
            >
              <View
                style={[
                  styles.circle,
                  {
                    backgroundColor: colors.maroon,
                    alignItems: "center",
                    justifyContent: "center"
                  }
                ]}
              >
                <Icon
                  name="clock"
                  type="feather"
                  size={18}
                  color={colors.white}
                />
                {/*<CircleActions image={truck}*/}
                {/*               style={{justifyContent: 'center', marginLeft: 10}}/>*/}
              </View>
              <Text style={styles.listText}>Begin</Text>
            </View>
          </TouchableOpacity>
        ) : null}

        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate("TripComments", {
              bookingId: null,
              tripId: tripId
            })
          }
        >
          <View
            style={[
              styles.rowstyle,
              { alignItems: "center", justifyContent: "center" }
            ]}
          >
            {/*<CircleActionsYellow image={comment}/>*/}
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: "#AF7AC5",
                  alignItems: "center",
                  justifyContent: "center"
                }
              ]}
            >
              <Icon name="message" type="link" size={18} color={colors.white} />
            </View>
            <Text style={styles.listText}>Comment</Text>
          </View>
        </TouchableOpacity>

        {rightsArray.includes("edit-route") ? (
          <TouchableOpacity
            onPress={() =>
              this.setState({ showEditRouteModal: true, tripId: tripId })
            }
          >
            <View style={styles.rowstyle}>
              <View
                style={[
                  styles.circle,
                  {
                    backgroundColor: "#48C9B0",
                    alignItems: "center",
                    justifyContent: "center",
                    alignSelf: "center"
                  }
                ]}
              >
                <Image
                  style={{ height: 18, width: 18, alignSelf: "center" }}
                  source={routeImage}
                />
              </View>

              {/*<CircleActionsPink image={routeImage}/>*/}
              <Text style={styles.listText}>Edit Route</Text>
            </View>
          </TouchableOpacity>
        ) : null}

        {rightsArray.includes("edit-trip-route") ? (
          <TouchableOpacity
            onPress={() =>
              this.setState({ shownewTripEditTripModal: true, tripId: tripId })
            }
          >
            <View
              style={[
                styles.rowstyle,
                { alignItems: "center", justifyContent: "center" }
              ]}
            >
              {/*<CircleActionsPink image={back}/>*/}
              <View
                style={[
                  styles.circle,
                  {
                    backgroundColor: "#5499C7",
                    alignItems: "center",
                    justifyContent: "center"
                  }
                ]}
              >
                <Icon
                  name="edit"
                  type="feather"
                  size={18}
                  color={colors.white}
                />
              </View>
              <Text style={styles.listText}>Edit Trip</Text>
            </View>
          </TouchableOpacity>
        ) : null}

        {rightsArray.includes("terminate-trip") ? (
          <TouchableOpacity
            onPress={() =>
              this.setState({ showTerminateModal: true, tripId: tripId })
            }
          >
            <View
              style={[
                styles.rowstyle,
                { alignItems: "center", justifyContent: "center" }
              ]}
            >
              <View
                style={[
                  styles.circle,
                  {
                    backgroundColor: "#5DADE2",
                    alignItems: "center",
                    justifyContent: "center"
                  }
                ]}
              >
                <Icon name="x" type="feather" size={18} color={colors.white} />
              </View>
              {/*<CircleActionsPink image={terminate}/>*/}
              <Text style={styles.listText}>Terminate</Text>
            </View>
          </TouchableOpacity>
        ) : null}
      </View>
    );
  }

  renderTouchingData() {
    if (touchingFlag === "in" && rightsArray.includes("touching-in")) {
      return (
        <TouchableOpacity
          onPress={() =>
            this.setState({
              showTouchingModal: true,
              tripId: tripId,
              heading: "Touching Time",
              touchingCity: touchingCity,
              lowerBound: lowerBound
            })
          }
        >
          <View
            style={[
              styles.rowstyle,
              { alignItems: "center", justifyContent: "center" }
            ]}
          >
            {/*<CircleActions image={back}*/}
            {/*               style={{justifyContent: 'center', marginLeft: 10}}/>*/}
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: "#48C9B0",
                  alignItems: "center",
                  justifyContent: "center"
                }
              ]}
            >
              <Icon
                name="map-pin"
                type="feather"
                size={18}
                color={colors.white}
              />
            </View>

            <Text style={styles.listText}>{touchingDisp}</Text>
          </View>
        </TouchableOpacity>
      );
    } else if (touchingFlag === "out") {
      return (
        <TouchableOpacity
          onPress={() =>
            this.setState({
              showDockModal: true,
              tripId: tripId,
              heading: "Touching Time",
              touchingCity: touchingCity,
              lowerBound: lowerBound
            })
          }
        >
          <View
            style={[
              styles.rowstyle,
              { alignItems: "center", justifyContent: "center" }
            ]}
          >
            {/*<CircleActions image={back}*/}
            {/*               style={{justifyContent: 'center', marginLeft: 10}}/>*/}
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: "#58D68D",
                  alignItems: "center",
                  justifyContent: "center"
                }
              ]}
            >
              <Icon
                name="map-pin"
                type="feather"
                size={18}
                color={colors.white}
              />
            </View>

            <Text style={styles.listText}>{touchingDisp}</Text>
          </View>
        </TouchableOpacity>
      );
    } else if (
      touchingFlag === "reached" &&
      rightsArray.includes("reached-destination")
    ) {
      return (
        <TouchableOpacity
          onPress={() =>
            this.setState({
              showTouchingModal: true,
              tripId: tripId,
              heading: "Touching Time",
              touchingCity: touchingCity,
              lowerBound: lowerBound
            })
          }
        >
          <View
            style={[
              styles.rowstyle,
              { alignItems: "center", justifyContent: "center" }
            ]}
          >
            {/*<CircleActions image={back}*/}
            {/*               style={{justifyContent: 'center', marginLeft: 10}}/>*/}
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: "#F7DC6F",
                  alignItems: "center",
                  justifyContent: "center"
                }
              ]}
            >
              <Icon
                name="map-pin"
                type="feather"
                size={18}
                color={colors.white}
              />
            </View>

            <Text style={styles.listText}>{touchingDisp}</Text>
          </View>
        </TouchableOpacity>
      );
    } else {
    }
  }

  intransitActions() {
    return (
      <View style={{ flexDirection: "row" }}>
        {rightsArray.includes("begin-trip") && status === "HL" ? (
          <TouchableOpacity
            onPress={() =>
              this.setState({
                showDockModal: true,
                tripId: tripId,
                heading: "Begin Trip"
              })
            }
          >
            <View
              style={[
                styles.rowstyle,
                { alignItems: "center", justifyContent: "center" }
              ]}
            >
              {/*<CircleActions image={truck}*/}
              {/*               style={{justifyContent: 'center', marginLeft: 10}}/>*/}
              <View
                style={[
                  styles.circle,
                  {
                    backgroundColor: "#F1FC17",
                    alignItems: "center",
                    justifyContent: "center"
                  }
                ]}
              >
                <Icon
                  name="clock"
                  type="feather"
                  size={18}
                  color={colors.white}
                />
              </View>

              <Text style={styles.listText}>Begin</Text>
            </View>
          </TouchableOpacity>
        ) : null}

        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate("TripComments", {
              bookingId: null,
              tripId: tripId
            })
          }
        >
          <View
            style={[
              styles.rowstyle,
              { alignItems: "center", justifyContent: "center" }
            ]}
          >
            {/*<CircleActionsYellow image={comment}/>*/}
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: "#B7FC17",
                  alignItems: "center",
                  justifyContent: "center"
                }
              ]}
            >
              <Icon name="message" type="link" size={18} color={colors.white} />
            </View>

            <Text style={styles.listText}>Comment</Text>
          </View>
        </TouchableOpacity>

        {rightsArray.includes("terminate-trip") ? (
          <TouchableOpacity
            onPress={() =>
              this.setState({ showTerminateModal: true, tripId: tripId })
            }
          >
            <View
              style={[
                styles.rowstyle,
                { alignItems: "center", justifyContent: "center" }
              ]}
            >
              {/*<CircleActionsPink image={terminate}/>*/}
              <View
                style={[
                  styles.circle,
                  {
                    backgroundColor: "#17FCD9",
                    alignItems: "center",
                    justifyContent: "center"
                  }
                ]}
              >
                <Icon name="x" type="feather" size={18} color={colors.white} />
              </View>

              <Text style={styles.listText}>Terminate</Text>
            </View>
          </TouchableOpacity>
        ) : null}

        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate("RunningStatus", {
              tripId: tripId,
              token: token,
              dataSource: this.state.dataSource,
              rightsArray: rightsArray
            })
          }
        >
          <View
            style={[
              styles.rowstyle,
              { alignItems: "center", justifyContent: "center" }
            ]}
          >
            {/*<CircleActionsPink image={back}/>*/}
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: "#48C9B0",
                  alignItems: "center",
                  justifyContent: "center",
                  alignSelf: "center"
                }
              ]}
            >
              {/*<Image style={{height: 18, width: 18, alignSelf: 'center'}}*/}
              {/*       source={routeImage}/>*/}

              <Icon
                name="truck"
                type="feather"
                size={18}
                color={colors.white}
              />
            </View>

            <Text style={styles.listText}>Running Status</Text>
          </View>
        </TouchableOpacity>

        {rightsArray.includes("edit-route") ? (
          <TouchableOpacity
            onPress={() =>
              this.setState({ showEditRouteModal: true, tripId: tripId })
            }
          >
            <View
              style={[
                styles.rowstyle,
                { alignItems: "center", justifyContent: "center" }
              ]}
            >
              <View
                style={[
                  styles.circle,
                  {
                    backgroundColor: "#17E4FC",
                    alignItems: "center",
                    justifyContent: "center"
                  }
                ]}
              >
                <Icon
                  name="edit"
                  type="feather"
                  size={18}
                  color={colors.white}
                />
              </View>

              <Text style={styles.listText}>Edit Route</Text>
            </View>
          </TouchableOpacity>
        ) : null}

        {rightsArray.includes("edit-trip-vehicle") ? (
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("EditVehiclePhone", {
                tripId: tripId,
                tickVisible: false,
                delayReasons: this.state.delayReasons,
                token: token,
                route: route,
                id: id
              })
            }
          >
            <View
              style={[
                styles.rowstyle,
                { alignItems: "center", justifyContent: "center" }
              ]}
            >
              <View
                style={[
                  styles.circle,
                  {
                    backgroundColor: "#48C9B0",
                    alignItems: "center",
                    justifyContent: "center",
                    alignSelf: "center"
                  }
                ]}
              >
                <Icon
                  name="truck"
                  type="feather"
                  size={18}
                  color={colors.white}
                />

                {/*<Image style={{height: 18, width: 18, alignSelf: 'center'}}*/}
                {/*       source={routeImage}/>*/}
              </View>

              {/*<CircleActionsPink image={back}/>*/}
              <Text style={styles.listText}>Cross Dock</Text>
            </View>
          </TouchableOpacity>
        ) : null}

        {rightsArray.includes("halt-trip") && status === "IN" ? (
          <TouchableOpacity
            onPress={() =>
              this.setState({ showHaltModal: true, tripId: tripId })
            }
          >
            <View
              style={[
                styles.rowstyle,
                { alignItems: "center", justifyContent: "center" }
              ]}
            >
              {/*<CircleActionsYellow image={back}/>*/}
              <View
                style={[
                  styles.circle,
                  {
                    backgroundColor: "#F217FC",
                    alignItems: "center",
                    justifyContent: "center"
                  }
                ]}
              >
                <Icon
                  name="clock"
                  type="feather"
                  size={18}
                  color={colors.white}
                />
              </View>

              <Text style={styles.listText}>Halt</Text>
            </View>
          </TouchableOpacity>
        ) : null}

        {this.renderTouchingData()}
      </View>
    );
  }

  atDestinationActions() {
    return (
      <View style={{ flexDirection: "row" }}>
        {rightsArray.includes("close-trip") ? (
          <TouchableOpacity
            onPress={() =>
              this.setState({
                showDockModal: true,
                tripId: tripId,
                heading: "Close Time"
              })
            }
          >
            <View
              style={[
                styles.rowstyle,
                { alignItems: "center", justifyContent: "center" }
              ]}
            >
              {/*<CircleActionsPink image={closetrip}/>*/}
              <View
                style={[
                  styles.circle,
                  {
                    backgroundColor: "#FC178D",
                    alignItems: "center",
                    justifyContent: "center"
                  }
                ]}
              >
                <Icon name="x" type="feather" size={18} color={colors.white} />
              </View>

              <Text style={styles.listText}>Close</Text>
            </View>
          </TouchableOpacity>
        ) : null}

        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate("TripComments", {
              bookingId: null,
              tripId: tripId
            })
          }
        >
          <View
            style={[
              styles.rowstyle,
              { alignItems: "center", justifyContent: "center" }
            ]}
          >
            {/*<CircleActionsYellow image={comment}/>*/}
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: "#A2D9CE",
                  alignItems: "center",
                  justifyContent: "center"
                }
              ]}
            >
              <Icon name="message" type="link" size={18} color={colors.white} />
            </View>

            <Text style={styles.listText}>Comment</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.container}>
          <View style={styles.header2Style}>
            <View style={{ flexDirection: "row" }}>
              {this.state.showDetails === true ? null : (
                <View style={{ flexDirection: "row" }}>
                  <Text style={[styles.textStyle, { color: colors.blueColor }]}>
                    {route}
                  </Text>
                  <Text style={[styles.textStyle, { color: colors.blueColor }]}>
                    {customer}
                  </Text>
                  <Text style={[styles.textStyle, { color: colors.blueColor }]}>
                    {vendor}
                  </Text>
                </View>
              )}

              <View style={{ flex: 1 }}>
                {this.state.showMoreButton === true ? (
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        showDetails: true,
                        showMoreButton: false,
                        showLessButton: true
                      })
                    }
                  >
                    <Text
                      style={[
                        styles.textStyle,
                        {
                          textAlign: "right",
                          textDecorationLine: "underline"
                        }
                      ]}
                    >
                      Show more...
                    </Text>
                  </TouchableOpacity>
                ) : null}
              </View>
            </View>

            {this.state.showDetails === true ? (
              <View style={{ marginTop: 5, flexDirection: "row" }}>
                <View style={{ flex: 0.6 }}>
                  <View style={{ flexDirection: "row" }}>
                    <Text
                      style={[
                        styles.detailsTextStyle,
                        {
                          flex: 0.5,
                          padding: 5,
                          color: colors.textcolor
                        }
                      ]}
                    >
                      Customer
                    </Text>
                    <Text
                      style={[
                        styles.detailsTextStyle,
                        { flex: 0.5, padding: 5 }
                      ]}
                    >
                      {customer}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text
                      style={[
                        styles.detailsTextStyle,
                        {
                          flex: 0.5,
                          padding: 5,
                          color: colors.textcolor
                        }
                      ]}
                    >
                      Route
                    </Text>
                    <Text
                      style={[
                        styles.detailsTextStyle,
                        { flex: 0.5, padding: 5 }
                      ]}
                    >
                      {route}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text
                      style={[
                        styles.detailsTextStyle,
                        {
                          flex: 0.5,
                          padding: 5,
                          color: colors.textcolor
                        }
                      ]}
                    >
                      Vendor
                    </Text>
                    <Text
                      style={[
                        styles.detailsTextStyle,
                        { flex: 0.5, padding: 5 }
                      ]}
                    >
                      {vendor}
                    </Text>
                  </View>

                  {reference !== null &&
                  reference !== undefined &&
                  reference !== " " ? (
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={[
                          styles.detailsTextStyle,
                          {
                            flex: 0.5,
                            padding: 5,
                            color: colors.textcolor
                          }
                        ]}
                      >
                        Reference
                      </Text>
                      <Text
                        style={[
                          styles.detailsTextStyle,
                          {
                            flex: 0.5,
                            padding: 5
                          }
                        ]}
                      >
                        {reference}
                      </Text>
                    </View>
                  ) : null}

                  {phone !== null && phone !== undefined && phone !== "" ? (
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={[
                          styles.detailsTextStyle,
                          {
                            flex: 0.5,
                            padding: 5,
                            color: colors.textcolor
                          }
                        ]}
                      >
                        Driver Phone
                      </Text>
                      <Text
                        style={[
                          styles.detailsTextStyle,
                          {
                            flex: 0.5,
                            padding: 5
                          }
                        ]}
                      >
                        {phone}
                      </Text>
                    </View>
                  ) : null}

                  <View style={{ flexDirection: "row" }}>
                    <Text
                      style={[
                        styles.detailsTextStyle,
                        {
                          flex: 0.5,
                          padding: 5,
                          color: colors.textcolor
                        }
                      ]}
                    >
                      Status
                    </Text>
                    <Text
                      style={[
                        styles.detailsTextStyle,
                        {
                          flex: 0.5,
                          padding: 5
                        }
                      ]}
                    >
                      {status === "CR"
                        ? "Created"
                        : status === "HL"
                        ? "Halted"
                        : status === "IN"
                        ? "Intransit"
                        : status === "AI"
                        ? "Arrived Intermediate"
                        : status === "AR"
                        ? "At Destination"
                        : status === "CL"
                        ? "Closed"
                        : status === "TM"
                        ? "Terminated"
                        : null}
                    </Text>
                  </View>

                  {startTime !== "Started: " ? (
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={[
                          styles.detailsTextStyle,
                          {
                            flex: 0.5,
                            padding: 5,
                            color: colors.textcolor
                          }
                        ]}
                      >
                        Started Time
                      </Text>
                      <Text
                        style={[
                          styles.detailsTextStyle,
                          {
                            flex: 0.5,
                            padding: 5
                          }
                        ]}
                      >
                        {startTime.split(" ")[2]}
                      </Text>
                    </View>
                  ) : null}
                </View>

                <View style={{ flex: 0.4 }}>
                  {this.state.showLessButton === true ? (
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          showDetails: false,
                          showMoreButton: true,
                          showLessButton: false
                        })
                      }
                    >
                      <Text
                        style={[
                          styles.textStyle,
                          {
                            textAlign: "right",
                            textDecorationLine: "underline"
                          }
                        ]}
                      >
                        Show less...
                      </Text>
                    </TouchableOpacity>
                  ) : null}
                </View>
              </View>
            ) : null}
          </View>

          <View style={styles.mapViewStyle}>
            {this.state.mapCoordinates.length > 0 ? (
              <MapView
                style={styles.map}
                initialRegion={{
                  latitude: this.state.defaultLat,
                  longitude: this.state.defaultLng,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421
                }}
              >
                {this.state.mapCoordinates.map(marker => {
                  return (
                    <MapViewDirections
                      key={marker.time}
                      origin={this.state.mapCoordinates[0].coordinate}
                      destination={
                        this.state.mapCoordinates[
                          this.state.mapCoordinates.length - 1
                        ].coordinate
                      }
                      strokeWidth={3}
                      strokeColor="hotpink"
                      apikey={"AIzaSyAj-miEdT-tzK792VlaXkxkvEXd3e_1swA"}
                    />
                  );
                })}
              </MapView>
            ) : null}
          </View>

          {this.renderDockModal()}
          {this.renderTerminateModal()}
          {this.renderEditRouteModal()}

          {this.renderTouchingModal()}
          {this.renderNewTripEditRouteModal()}

          {tripType === "in" ? this.renderHaltModal() : null}
          {/*{tripType === 'in' ? this.renderEditVehicleModal() : null}*/}

          <View style={styles.actionStyle}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              {tripType === "new" ? this.newTripActions() : null}

              {tripType === "in" ? this.intransitActions() : null}

              {tripType === "atdesti" ? this.atDestinationActions() : null}
            </ScrollView>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.listbackgroundcolor
  },
  map: {
    flex: 1,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  mapViewStyle: {
    height: "100%"
  },
  header2Style: {
    backgroundColor: colors.white,
    padding: 10
  },
  textStyle: {
    fontSize: 12,
    color: colors.textcolor,
    fontFamily: "CircularStd-Book",
    padding: 5
  },
  listText: {
    fontSize: 10,
    color: colors.textcolor,
    marginTop: 5,
    textAlign: "center"
  },
  rowstyle: {
    marginLeft: 10,
    width: 55,
    justifyContent: "center"
  },
  cardContainer: {
    flex: 1
  },
  card: {
    backgroundColor: "#FE474C",
    borderRadius: 5,
    shadowColor: "rgba(0,0,0,0.5)",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.5
  },
  card1: {
    padding: 10,
    backgroundColor: "#FE474C"
  },
  card2: {
    flex: 1,
    padding: 10,
    backgroundColor: "#FEB12C"
  },
  actionStyle: {
    flexDirection: "row",
    backgroundColor: colors.white,
    paddingTop: 5,
    paddingBottom: 5,
    flex: 1,
    position: "absolute",
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: "center"
  },
  detailsTextStyle: {
    fontSize: 12,
    color: colors.dark_grey
  },
  circle: {
    width: 33,
    height: 33,
    borderRadius: 100 / 2
  },
  iconStyle: {
    height: 15,
    width: 15
  }
});

{
  /*<MapView.Marker*/
}
{
  /*    key={marker.time}*/
}
{
  /*    coordinate={marker.coordinate}*/
}
{
  /*/>*/
}

{
  /*<MapView.Polyline*/
}
{
  /*    coordinate={marker.coordinate}*/
}
{
  /*    strokeWidth={2}*/
}
{
  /*    strokeColor="red"/>*/
}

{
  /*<Marker*/
}
{
  /*    pinColor={'green'}*/
}
{
  /*    coordinate={this.state.mapCoordinates[0].coordinate}*/
}
{
  /*/>*/
}

{
  /*<MapViewDirections*/
}
{
  /*    origin={origin}*/
}
{
  /*    destination={destination}*/
}
{
  /*    strokeWidth={3}*/
}
{
  /*    strokeColor="hotpink"*/
}
{
  /*    apikey={'AIzaSyDjfNsHFVUcBrNM3YkMSULF4y72eRs0o00'}*/
}
{
  /*/>*/
}
