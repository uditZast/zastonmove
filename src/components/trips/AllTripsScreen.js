import React from "react";
import { Image } from "react-native";
import {
  createBottomTabNavigator,
  createStackNavigator
} from "react-navigation";
import colors from "../../colors/Colors";
import newrequest from "../../images/newrequest.png";
import accepted from "../../images/accepted.png";
import assigned from "../../images/shipped.png";
import { Icon } from "react-native-vector-icons";
import NewTrips from "./NewTrips";
import IntransitTrips from "./IntransitTrips";
import AtDestinationScreen from "./AtDestinationScreen";

const NewTrip = createStackNavigator({
  NewTrips: {
    screen: NewTrips,
    navigationOptions: { header: null }
  }
});

const InTransit = createStackNavigator({
  IntransitTrips: {
    screen: IntransitTrips,
    navigationOptions: {
      header: null
    }
  }
});

const AtDestination = createStackNavigator({
  AtDestination: {
    screen: AtDestinationScreen,
    title: "At Destination",
    navigationOptions: {
      header: null
    }
  }
});

const AllTripsScreen = createBottomTabNavigator(
  {
    "New Trip": {
      screen: NewTrips,
      navigationOptions: { header: null }
    },
    "In Transit": {
      screen: IntransitTrips,
      navigationOptions: {
        header: null
      }
    },
    "At Destination": {
      screen: AtDestinationScreen,
      title: "At Destination",
      navigationOptions: {
        header: null
      }
    }
  },
  {
    tabBarOptions: {
      activeTintColor: colors.blueColor,
      inactiveTintColor: colors.black,
      style: {
        backgroundColor: colors.white,
        borderBottomWidth: 0,
        borderBottomColor: colors.blueColor
      },
      indicatorStyle: {
        backgroundColor: colors.redColor
      }
    },
    headerMode: null,
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName = "";
        if (routeName === "New Trip") {
          return (
            <Image style={{ height: 14, width: 14 }} source={newrequest} />
          );
          // iconName = 'assignment';
        } else if (routeName === "In Transit") {
          // iconName = 'checklist';
          return <Image style={{ height: 14, width: 14 }} source={accepted} />;
        } else if (routeName === "At Destination") {
          // iconName = 'truck-check';
          return <Image style={{ height: 14, width: 14 }} source={assigned} />;
        }
        return <Icon name={iconName} size={25} color={colors.blueColor} />;
      }
    })
  }
);

export default AllTripsScreen;

const styles = {
  tabStyle: {
    backgroundColor: colors.blue
  },
  labelStyle: {
    fontSize: 14
  }
};
