import React, { Component } from "react";
import {
  Image,
  Platform,
  RefreshControl,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../../colors/Colors";
import locIcon from "../../images/placeholder.png";
import clock from "../../images/time.png";
import AsyncStorage from "@react-native-community/async-storage";
import Card from "../Card";
import IntransitRows from "../common/IntransitRows";
import IntransitTripSection2 from "../common/IntransitTripsSection2";
import { INTRANSIT_TRIPS } from "../../constants/Constants";
import { ScrollView } from "react-navigation";
import { BarIndicator } from "react-native-indicators";
import NetInfo from "@react-native-community/netinfo";
import Snackbar from "react-native-snackbar";
import { Container } from "../common/Container";
import axios from "axios";

export default class InstransitTrips extends Component {
  state = { data: [], values: "" };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      refreshing: true,
      dataSource_filter: [],
      OriginalData: [],
      dataSource: [],
      rightsArray: [],
      text: ""
    };
  }

  componentDidMount() {
    AsyncStorage.getItem("rights").then(value =>
      this.setState({ rightsArray: value })
    );

    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        this.getIntransitTrips();

        this._onFocusListener = this.props.navigation.addListener(
          "didFocus",
          payload => {
            this.setState({ text: "" });
            this.getIntransitTrips();
            console.log("component did mount ");
          }
        );
      } else {
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });
  }

  componentWillUnmount() {
    this._onFocusListener.remove();
  }

  getIntransitTrips() {
    AsyncStorage.getItem("token").then(value => {
      console.log("token:::::::" + value);
      this.setState({ values: value });

      axios
        .get(INTRANSIT_TRIPS, {
          headers: {
            Authorization: "Token " + value,
            "Content-Type": "application/json"
          }
        })
        .then(responseJson => {
          console.log("data::::" + JSON.stringify(responseJson));
          this.setState({
            isLoading: false,
            dataSource: responseJson.data.data,
            refreshing: false,
            OriginalData: responseJson.data.data
          });
          // this.setState({isLoading: false, dataSource: ds.cloneWithRows(responseJson.data),})
        })
        .catch(error => {
          console.log("catch new request" + JSON.stringify(error));
          console.log("catch new request : " + error);
          if (error.response.status === 401) {
            AsyncStorage.removeItem("token");
            this.props.navigation.navigate("Auth");
          } else {
          }
        });
    });

    // AsyncStorage.getItem('token').then(
    //     (value) => {
    //         this.setState({values: value});
    //         fetch(INTRANSIT_TRIPS, {
    //             headers: {
    //                 'Authorization': 'Token ' + value
    //             }
    //         })
    //             .then((response) => response.json())
    //             .then((responseJson) => {
    //                 console.log("data::::" + JSON.stringify(responseJson));
    //                 this.setState({
    //                     isLoading: false,
    //                     dataSource: responseJson.data,
    //                     refreshing: false,
    //                     OriginalData: responseJson.data,
    //                 })
    //                 // this.setState({isLoading: false, dataSource: ds.cloneWithRows(responseJson.data),})
    //
    //             })
    //             .catch((error) => {
    //                 console.log("testing");
    //                 console.error(error);
    //                 if (error.response.status === 401) {
    //                     this.props.navigation.navigate('Auth')
    //                 } else {
    //
    //                 }
    //             });
    //
    //     })
  }

  checkUndefinedValues(value, value1) {
    if (value !== "") {
      console.log("location::::");
      return (
        <View style={styles.locationViewStyle}>
          <Image source={locIcon} style={styles.iconStyle} />
          <Text style={styles.locationTextStyle1}>
            {value}
            <Text style={styles.blanktextStyle}>1</Text>
            <Text style={styles.redTextStyle}>{value1}</Text>
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkUndefined(value) {
    if (value !== "") {
      return (
        <View style={styles.rowTwoStyle}>
          <Text style={styles.normalTextStyle}>{value}</Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkUndefinedUpdated(value) {
    if (value !== "") {
      return (
        <View style={styles.rowTwoStyle}>
          <Text style={styles.locationTextStyle}>{value}</Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkUndefinedSpeed(value) {
    if (value === "0 Km/hr") {
      return (
        <View style={styles.containerStyle}>
          <View style={styles.circle}>
            <Text style={styles.textStyle}>{0}</Text>
          </View>

          <Text style={styles.belowCircleText}>km/hr</Text>
        </View>
      );
    } else if (value !== "0 Km/hr") {
      return (
        <View style={styles.containerStyle}>
          <View style={styles.greenCircle}>
            <Text style={styles.greentextStyle}>{value.split(" ")[0]}</Text>
          </View>

          <Text style={styles.belowCircleText}>km/hr</Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkUndefinedRef(value) {
    if (value !== "") {
      return (
        <View style={styles.equalHalfView}>
          {/*<Text style={styles.locationTextStyle}>Reference</Text>*/}
          <Text style={styles.textStyle1}>Reference {value}</Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkUndefinedStarted(value) {
    if (value !== "") {
      return (
        <View style={styles.startedStyle}>
          <Image source={clock} style={styles.iconStyle} />
          <Text style={styles.locationTextStyle}>{value}</Text>
        </View>
      );
    } else {
      return null;
    }
  }

  checkLocationIconVisibility(value) {
    if (value !== "") {
      return (
        <View>
          <Image source={locIcon} style={styles.iconStyle} />
        </View>
      );
    } else {
      return null;
    }
  }

  checkDelayicon(value) {
    if (value !== true) {
      return <View style={styles.triangleCorner} />;
    } else {
      return null;
    }
  }

  rendorError() {
    if (this.state.error) {
      return <Text style={styles.errorStyle}>{this.state.error}</Text>;
    } else {
      console.log("location::::else");
      return null;
    }
  }

  openSecondActivity(rowData) {
    console.log("working");
    this.props.navigation.navigate("TripDetails", {
      ListViewClickHolder: rowData,
      tripId: rowData.t_code,
      token: this.state.values,
      vehicle: rowData.vehicle,
      speed: rowData.speed.speed,
      type: rowData.type,
      tripType: "in",
      customer: rowData.cust,
      vendor: rowData.vend,
      route: rowData.route,
      status: rowData.status,
      lowerBound: rowData.lower_bound,
      touchingFlag: rowData.touching,
      tat: rowData.tat,
      id: rowData.id,
      phone: rowData.phone,
      reference: rowData.reference,
      startTime: rowData.start,
      transitTime: rowData.transit_time,
      rightsArray: this.state.rightsArray
    });
  }

  dataToSend(props) {
    return (
      <View>
        <Text>{props.vehicle}</Text>
      </View>
    );
  }

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.getIntransitTrips();
  };

  filterSearch(text) {
    console.log("filter " + text);
    const dataSource_filter = this.state.OriginalData.filter(item => {
      const itemData =
        item.t_code + item.vehicle + item.route + item.vend + item.cust;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      text: text,
      dataSource: dataSource_filter // after filter we are setting users to new array
    });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <BarIndicator color={colors.blueColor} size={20} />
        </View>
      );
    }
    // const Navigator=createAppContainer(screenNavigation);

    return (
      <Container>
        <View style={styles.container}>
          <TextInput
            style={[styles.customerBorderStyle, { marginTop: 0 }]}
            onChangeText={text => this.filterSearch(text)}
            value={this.state.text}
            placeholderTextColor={"#9D9B9D"}
            placeholder="Search TripId/ Vehicle/ Vendor/ Route"
          />

          {this.state.dataSource.length > 0 ? (
            <ScrollView
              style={{ backgroundColor: colors.listbackgroundcolor }}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            >
              {this.state.dataSource.map(rowData => {
                return (
                  <Card key={rowData.t_code}>
                    <View style={{ flex: 1 }}>
                      <View style={styles.rowStyle}>
                        <View style={styles.columnStyle}>
                          <View style={styles.circleContainer}>
                            {this.checkUndefinedSpeed(rowData.speed.speed)}
                          </View>
                        </View>

                        <TouchableOpacity
                          onPress={this.openSecondActivity.bind(this, rowData)}
                          style={{ flex: 1 }}
                        >
                          <IntransitRows
                            vehicle={rowData.vehicle}
                            t_code={rowData.t_code}
                            cust={rowData.cust}
                            vend={rowData.vend}
                          />
                        </TouchableOpacity>

                        <View>
                          {this.checkDelayicon(
                            rowData.running_status.up_to_date
                          )}
                        </View>
                      </View>

                      <IntransitTripSection2
                        route={rowData.route}
                        start={rowData.start}
                        loc={rowData.loc}
                        updated={rowData.updated}
                      />
                    </View>
                  </Card>
                );
              })}
              {/*<ListView*/}
              {/*    dataSource={this.state.dataSource}*/}
              {/*    renderRow={(rowData) =>*/}
              {/*    }/>*/}
            </ScrollView>
          ) : null}
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  rowTwoStyle: {
    flexDirection: "row",
    marginLeft: 10,
    marginTop: 5
  },
  iconStyle: {
    height: 14,
    width: 14,
    marginLeft: 10,
    marginTop: 3,
    marginBottom: 3
  },
  normalTextStyle: {
    color: colors.textcolor,
    fontSize: 12,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    fontFamily: "CircularStd-Book"
  },
  errorStyle: {
    fontSize: 20,
    alignSelf: "center",
    color: "red",
    marginBottom: 10,
    fontFamily: "CircularStd-Book"
  },
  columnStyle: {
    flexDirection: "column"
  },
  rowStyle: {
    flexDirection: "row",
    flex: 1
  },
  viewStyle: {
    flex: 1,
    flexDirection: "row",
    marginLeft: 10,
    width: "100%"
  },
  equalHalfView: {
    flex: 1,
    marginTop: 5
  },
  locationTextStyle: {
    color: colors.textcolor,
    fontSize: 12,
    flex: 1,
    marginLeft: 10,
    marginTop: 4,
    fontFamily: "CircularStd-Book"
  },
  circle: {
    width: 18,
    height: 18,
    borderRadius: 100 / 2,
    backgroundColor: colors.loginGradient1
  },
  textStyle: {
    color: colors.white,
    fontSize: 13,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    fontFamily: "CircularStd-Book"
  },
  greentextStyle: {
    color: colors.white,
    fontSize: 13,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    textAlign: "center",
    fontFamily: "CircularStd-Book"
  },
  containerStyle: {
    flexDirection: "column"
  },
  belowCircleText: {
    fontSize: 11,
    color: colors.textcolor,
    fontFamily: "CircularStd-Book"
  },
  greenCircle: {
    width: 18,
    height: 18,
    borderRadius: 100 / 2,
    backgroundColor: colors.greenColor
  },
  textStyle1: {
    color: colors.textcolor,
    fontSize: 12,
    marginLeft: 5,
    fontFamily: "CircularStd-Book"
  },
  circleContainer: {
    marginLeft: 5,
    marginTop: 5
  },
  locationViewStyle: {
    flex: 1,
    marginTop: 3,
    flexDirection: "row",
    marginBottom: 5,
    marginLeft: 40,
    marginRight: 20
  },
  locationTextStyle1: {
    color: colors.textcolor,
    fontSize: 12,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 10,
    marginRight: 15,
    fontFamily: "CircularStd-Book",
    textAlign: "left",
    lineHeight: 20
  },
  triangleCorner: {
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid",
    borderRightWidth: 10,
    borderTopWidth: 10,
    borderRightColor: "transparent",
    borderTopColor: colors.bookedColor,
    transform: [{ rotate: "90deg" }]
  },
  startedStyle: {
    flex: 1,
    flexDirection: "row",
    marginLeft: 40
  },
  redTextStyle: {
    fontSize: 12,
    fontFamily: "CircularStd-Book",
    color: colors.loginGradient1,
    marginLeft: 10,
    marginRight: 5
  },
  blanktextStyle: {
    fontSize: 12,
    color: colors.white
  },
  customerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginLeft: 10,
    marginRight: 10,
    color: colors.dark_grey,
    marginBottom: 10,
    ...Platform.select({
      ios: {
        padding: 10,
        borderRadius: 10,
        marginTop: 10,
        overflow: "hidden"
      },
      android: {
        paddingTop: 5,
        paddingLeft: 12,
        paddingRight: 12,
        paddingBottom: 5,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        marginTop: 8
      }
    })
  }
});
