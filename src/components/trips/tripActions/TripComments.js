import React, { Component } from "react";
import {
  Alert,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../../../colors/Colors";
import AsyncStorage from "@react-native-community/async-storage";
import axios from "axios";
import {
  ADD_COMMENT,
  BOOKING_TRIP_History
} from "../../../constants/Constants";
import { ScrollView } from "react-navigation";
import Card from "../../Card";
import Snackbar from "react-native-snackbar";
import { BarIndicator } from "react-native-indicators";
import NetInfo from "@react-native-community/netinfo";

let tripId, bookingId, date, month, year, hrs, mins;

export default class TripComments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      dataSource: [],
      comment: "",
      value: "",
      successMessage: "",
      rightsArray: []
    };
  }

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;

    tripId = params.tripId;

    if (params.bookingId !== null && params.bookingId !== undefined) {
      bookingId = params.bookingId;
    } else {
    }

    console.log("tripId::::" + params.tripId);

    return {
      // title: navigation.getParam('Title', params ? params.title : ''),
      title: navigation.getParam(
        "Title",
        tripId !== null && tripId !== "-" && tripId !== undefined
          ? tripId
          : bookingId
      ),
      //Default Title of ActionBar
      headerStyle: {
        backgroundColor: navigation.getParam(
          "BackgroundColor",
          colors.blueColor
        )
      },
      headerTintColor: navigation.getParam("HeaderTintColor", "#fff"),
      //Text color of ActionBar
      headerTitleStyle: {
        fontWeight: "bold",
        fontSize: 13,
        fontFamily: "CircularStd-Book"
      }
    };
  };

  componentDidMount() {
    AsyncStorage.getItem("rights").then(value =>
      this.setState({ rightsArray: value })
    );

    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        this.getCommentsData();
      } else {
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });

    // this.getCommentsData();
  }

  getCommentsData() {
    console.log("tripId received::::::::::" + tripId);

    AsyncStorage.getItem("token").then(value => {
      this.setState({ value: value });
      console.log("token:::::::" + value);
      axios
        .post(
          BOOKING_TRIP_History,
          {
            booking_code: null,
            trip_code: tripId,
            entity: "comments"
          },
          {
            headers: {
              Authorization: "Token " + value,
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          this.setState(
            {
              dataSource: response.data.data,
              isLoading: false
            },
            () =>
              console.log(
                "dataSource setState:: " + this.state.dataSource.length
              )
          );
        })
        .catch(error => {
          console.log("catch" + error);
          if (error.response) {
            if (error.response.status === 401) {
              this.props.navigation.navigate("Auth");
            }
          }
        });
    });
  }

  checkUserName(value) {
    return (
      <View style={{ flexDirection: "row" }}>
        <Text style={styles.placeholderStyle}>User</Text>
        <Text style={styles.textStyle}>{value}</Text>
      </View>
    );
  }

  checkTime(value) {
    let text = value,
      textDate,
      textTime;

    console.log(text);
    console.log(text.split(" "));
    textDate = text.split(" ")[0];
    textTime = text.split(" ")[1];

    console.log("0th element" + text[0]);
    console.log(textDate + " split time" + textTime);

    date = textDate.split("-")[0];
    month = textDate.split("-")[1];
    year = textDate.split("-")[2];
    console.log("date ::::::" + date);

    hrs = textTime.split(":")[0];
    mins = textTime.split(":")[1];

    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];

    const d = new Date(year, month - 1, date, hrs, mins);
    console.log("get monthhhh " + monthNames[d.getMonth()]);

    return (
      <View
        style={{
          marginLeft: 10,
          marginRight: 5,
          marginBottom: 5,
          marginTop: 20,
          flex: 0.2
        }}
      >
        <Text style={styles.textStyle}>
          {d.getDate() + " "}
          {monthNames[d.getMonth()]}
        </Text>
        <Text style={[styles.timeStyle, { textAlign: "center" }]}>
          {textTime}
        </Text>
      </View>
    );
  }

  checkComment(value) {
    return (
      <View style={{ flexDirection: "row" }}>
        <Text style={styles.placeholderStyle}>Status</Text>
        <Text style={styles.textStyle}>{value}</Text>
      </View>
    );
  }

  checkNetworkSettings() {
    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        this.submitComment();
      } else {
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });
  }

  submitComment() {
    if (this.state.comment === "") {
      this.setState({ isLoading: false });
      return Snackbar.show({
        title: "Please add Comment",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      axios
        .post(
          ADD_COMMENT,
          {
            trip_code: tripId,
            comment: this.state.comment
          },
          {
            headers: {
              Authorization: "Token " + this.state.value,
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          console.log(response.data);
          console.log("reject message" + response.data.message);

          if (response.data.error === false) {
            console.log("error === false" + response.data.message[0]);
            this.setState(
              {
                successMessage: response.data.message[0],
                isLoading: false
              },
              () => this.showSuccessfullMessage()
            );
          } else {
            console.log("error === true");
            this.setState(
              {
                successMessage: response.data.message[0],
                isLoading: false
              },
              () => this.showSuccessfullMessage()
            );
          }
        })
        .catch(error => {
          console.log("catch " + error);
          if (error.response) {
            if (error.response.status === 401) {
              AsyncStorage.removeItem("token");
              this.props.navigation.navigate("Auth");
            }
          }
        });
    }
  }

  showSuccessfullMessage() {
    const { closeModal } = this.props;
    console.log(this.state);
    console.log("close Modal Reject::: " + closeModal);
    console.log("success Message" + this.state.successMessage);
    Alert.alert("Message", "" + this.state.successMessage, [
      {
        text: "OK",
        onPress: () => {
          this.setState({ comment: "" });
          this.getCommentsData();
        }
      }
    ]);
  }

  clearState() {
    const { closeModal } = this.props;

    this.setState({ comment: "" });
    closeModal();
  }

  render() {
    const { params } = this.props.navigation.state;
    const { closeModal } = params;

    if (this.state.isLoading) {
      return <BarIndicator color={colors.blueColor} size={20} />;
    } else {
      return (
        <View style={{ flex: 1, backgroundColor: "#ECF0F1" }}>
          <ScrollView
            style={{ flex: 1, backgroundColor: "#ECF0F1" }}
            keyboardShouldPersistTaps="always"
          >
            {this.state.rightsArray.includes("add-trip-comment") ? (
              <Card>
                <View
                  style={{
                    flexDirection: "row",
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center",
                    padding: 10,
                    backgroundColor: colors.white
                  }}
                >
                  <View style={[styles.BorderStyle, { flex: 1 }]}>
                    <Text style={styles.phcustomerStyle}>Comment</Text>
                    <TextInput
                      multiline={true}
                      placeholder={"Enter Comment"}
                      placeholderTextColor={colors.border_grey}
                      style={[
                        this.state.comment
                          ? styles.selectedTextColor
                          : styles.StyleInput
                      ]}
                      onChangeText={text => this.setState({ comment: text })}
                    >
                      {this.state.comment}
                    </TextInput>
                  </View>

                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ isLoading: true });
                      this.checkNetworkSettings();
                    }}
                  >
                    <Text style={styles.addCommentStyle}>Add Comment</Text>
                  </TouchableOpacity>
                </View>
              </Card>
            ) : null}

            <View style={{ backgroundColor: "#ECF0F1", flex: 1 }}>
              {this.state.dataSource.map(element => {
                return (
                  <View
                    key={element.id}
                    style={[styles.container, { flexDirection: "row" }]}
                  >
                    {this.checkTime(element.time)}

                    <Card>
                      {/*<View style={styles.VerticallineStyle}/>*/}

                      <View style={{ marginRight: 5, flex: 0.8 }}>
                        {element.comments !== null &&
                        element.comments !== "" ? (
                          <Text style={styles.textStyle}>
                            {element.comments}
                          </Text>
                        ) : null}
                      </View>

                      <View
                        style={{
                          alignSelf: "flex-end",
                          marginRight: 5,
                          padding: 2
                        }}
                      >
                        <Text
                          style={[
                            styles.textStyle,
                            styles.BorderStyle,
                            {
                              marginBottom: 5
                            }
                          ]}
                        >
                          {element.user}
                        </Text>
                      </View>
                    </Card>
                  </View>
                );
              })}
            </View>
          </ScrollView>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  VerticallineStyle: {
    borderLeftColor: colors.border_grey,
    borderLeftWidth: 1,
    marginTop: 10,
    marginBottom: 10
  },
  textStyle: {
    fontSize: 12,
    color: colors.dark_grey,
    marginRight: 5,
    padding: 5,
    fontFamily: "CircularStd-Book"
  },
  BorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginLeft: 10,
    marginRight: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  phcustomerStyle: {
    marginTop: -10,
    color: colors.dark_grey,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  selectedTextColor: {
    fontSize: 12,
    color: colors.dark_grey,
    marginLeft: 5,
    padding: 5,
    height: 35,
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
    fontFamily: "CircularStd-Book",
    width: "100%"
  },
  StyleInput: {
    fontSize: 12,
    color: colors.border_grey,
    paddingLeft: 5,
    marginLeft: 5,
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center",
    padding: 0,
    marginTop: 5,
    width: "100%"
  },
  addCommentStyle: {
    marginLeft: 5,
    fontSize: 15,
    padding: 10,
    marginRight: 5,
    backgroundColor: colors.blueColor,
    justifyContent: "center",
    alignItems: "center",
    color: colors.white
  }
});
