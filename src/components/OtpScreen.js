import React, { Component } from "react";
import { Alert, Text, TextInput, TouchableOpacity, View } from "react-native";
import colors from "../colors/Colors";
import Snackbar from "react-native-snackbar";
import { RESEND_OTP, VERIFY_OTP } from "../constants/Constants";
import AsyncStorage from "@react-native-community/async-storage";
import CountDown from "react-native-countdown-component";
import { Button } from "react-native-elements";
import { NAVIGATE } from "../../constants";
import { getUniqueId } from "react-native-device-info"; // Gets Device Id

let username, userId;
export default class OtpScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: "",
      totalDuration: 0,
      isLoading: false,
      resendLoading: false,
      counterVisible: false,
      resendButton: true
    };
  }

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;

    {
      params.name !== null && params.name !== undefined && params.name !== ""
        ? (username = params.name)
        : null;
    }

    {
      params.user_id !== null &&
      params.user_id !== undefined &&
      params.user_id !== ""
        ? (userId = params.user_id)
        : null;
    }

    return {
      headerTitle: "OTP Verification",
      headerStyle: {
        backgroundColor: navigation.getParam(
          "BackgroundColor",
          colors.blueColor
        )
      },
      headerTintColor: navigation.getParam("HeaderTintColor", "#fff"),
      headerTitleStyle: {
        fontWeight: "bold",
        fontSize: 13,
        fontFamily: "CircularStd-Book"
      }
    };
  };

  componentWillMount() {
    console.log("Came here 3 otp screen");
  }

  getOtp() {
    if (this.state.otp === "") {
      this.setState({ buttonLoading: false });
      return Snackbar.show({
        title: "Please enter Username",
        duration: Snackbar.LENGTH_SHORT
      });
    } else {
      fetch(VERIFY_OTP, {
        method: "post",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          username: username,
          otp: this.state.otp,
          device_id: getUniqueId()
        })
      })
        .then(response => response.json())
        .then(res => {
          console.log("res::::" + JSON.stringify(res));
          if (res.error === true) {
            Alert.alert("Error", "Error: " + res.message);
            console.log("if");
          } else {
            this.setState({
              auth_token: res.auth_token,
              isLoading: false,
              resendLoading: false
            });
            // Alert.alert("Welcome", " You have succesfully logged in");
            console.log("else");
            console.log("Token:: " + res.token);
            AsyncStorage.setItem("token", "" + res.token);
            AsyncStorage.setItem("role", "" + res.data.role);
            AsyncStorage.setItem("rights", "" + res.data.rights);
            AsyncStorage.setItem("username", "" + res.data.username);
            this.props.navigation.navigate(NAVIGATE.HOME);
            // saveUserToken(res.token);
          }
        })
        .catch(error => {
          console.error(error);
          console.log("error");
        });
    }
  }

  resendOtp() {
    fetch(RESEND_OTP, {
      method: "post",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        username: username,
        user_id: userId
      })
    })
      .then(response => response.json())
      .then(res => {
        console.log("res::::" + JSON.stringify(res));

        {
          this.renderCounter();
        }
      })
      .catch(error => {
        console.error(error);
        console.log("error");
      });
  }

  renderCounter() {
    console.log("render counter ");
    if (this.state.counterVisible) {
      return (
        <View style={{ paddingRight: 10 }}>
          <CountDown
            until={60 * 1 + 30}
            onFinish={() =>
              this.setState({ counterVisible: false, resendButton: true })
            }
            size={10}
            timeToShow={["M", "S"]}
            digitStyle={{ backgroundColor: colors.blueColor }}
            digitTxtStyle={{ color: colors.white }}
            timeLabels={{ m: "M", s: "S" }}
          />
        </View>
      );
    } else {
      return null;
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ height: 100, marginTop: 20 }} />
        <View style={{ flex: 1 }}>
          <Text
            style={[
              styles.headingTextStyle,
              {
                fontSize: 15,
                fontWeight: "400",
                marginTop: 10,
                color: colors.blueColor
              }
            ]}
          >
            OTP Verification
          </Text>
          <Text style={[styles.headingTextStyle, { fontSize: 11 }]}>
            OTP has been sent to your mobile number. Please enter it below
          </Text>

          <TextInput
            placeholder="Enter Your Otp"
            placeholderTextColor={colors.border_grey}
            underlineColor={"#3096f3"}
            keyboardType="numeric"
            multiline={false}
            maxLength={6}
            style={
              this.state.otp !== ""
                ? [styles.StyleInput, { color: colors.dark_grey }]
                : styles.StyleInput
            }
            onChangeText={text => this.setState({ otp: text })}
          />

          <View style={styles.lineStyle} />

          <View style={{ flexDirection: "row", alignSelf: "flex-end" }}>
            <TouchableOpacity
              onPress={() =>
                this.setState(
                  {
                    counterVisible: true,
                    resendButton: false
                  },
                  () => this.resendOtp()
                )
              }
            >
              <Text style={[styles.headingTextStyle, { fontSize: 11 }]}>
                Re-Send
              </Text>
            </TouchableOpacity>
          </View>

          <Button
            title={"Submit"}
            textStyle={{
              fontSize: 12,
              color: colors.white,
              textAlign: "center",
              padding: 5
            }}
            buttonStyle={{
              backgroundColor: colors.blueColor,
              marginLeft: 10,
              marginRight: 10,
              marginTop: 15
            }}
            onPress={() => this.getOtp()}
          />
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#F5FCFF"
  },
  redBackStyle: {
    alignItems: "center",
    flex: 1,
    width: "100%",
    backgroundColor: colors.blueColor,
    justifyContent: "center"
  },
  rowStyle: {
    flexDirection: "row",
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    padding: 10
  },
  textStyle: {
    color: colors.textcolor,
    fontSize: 15,
    marginLeft: 5,
    alignItems: "center"
  },
  buttonStyle: {
    backgroundColor: colors.loginGradient2,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    height: 45,
    width: "70%",
    alignItems: "center",
    marginTop: 20,
    marginBottom: 20
  },
  blankContainer: {
    height: 50,
    width: "100%"
  },
  containerStyle: {
    width: "80%",
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 1,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    marginLeft: 25,
    marginTop: 5,
    marginRight: 25,
    marginBottom: 5,
    borderTopLeftRadius: 3,
    backgroundColor: colors.white,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3
  },
  StyleInput: {
    color: colors.textcolor,
    marginLeft: 10,
    justifyContent: "center",
    fontFamily: "CircularStd-Book",
    padding: 0,
    textAlign: "center",
    width: "100%",
    marginRight: 10,
    marginTop: 20
  },
  headingTextStyle: {
    fontFamily: "CircularStd-Book",
    color: colors.textcolor,
    textAlign: "center",
    padding: 5
  },
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5
  }
};
