import React from "react";
import { createDrawerNavigator, createStackNavigator } from "react-navigation";
import drawerContentComponents from "./drawerContentComponents";
import CreateBookingScreen from "../bookings/CreateBookingScreen";
import { Icon } from "react-native-elements";
import colors from "../../colors/Colors";
import Dashboard from "../../containers/Dashboard";
import SearchScreen from "../../containers/SearchScreen";
import AvailableVehiclesScreen from "../../containers/AvailableVehiclesScreen";
import AllTripsScreen from "../trips/AllTripsScreen";
import AllBookingScreen from "../bookings/AllBookingScreen";
import MyBookingsScreen from "../bookings/MyBookingsScreen";

const dashboard = createStackNavigator({
  Second: {
    screen: Dashboard,
    navigationOptions: ({ navigation }) => ({
      title: "Available Vehicles",
      headerLeft: (
        <Icon
          onPress={() => navigation.toggleDrawer()}
          containerStyle={{ marginLeft: 15 }}
          name="menu"
          color={colors.white}
        />
      ),
      headerStyle: {
        backgroundColor: colors.blueColor
      },
      headerTintColor: "#fff"
    })
  }
});

const allTrips = createStackNavigator({
  Third: {
    screen: AllTripsScreen,
    navigationOptions: ({ navigation }) => ({
      title: "Trips",
      headerLeft: (
        <Icon
          onPress={() => navigation.toggleDrawer()}
          containerStyle={{ marginLeft: 15 }}
          name="menu"
          color={colors.white}
        />
      ),
      headerStyle: {
        backgroundColor: colors.blueColor
      },
      headerTintColor: "#fff"
    })
  }
});

const allBooking = createStackNavigator({
  Fourth: {
    screen: AllBookingScreen,
    navigationOptions: ({ navigation }) => ({
      title: "Bookings",
      headerLeft: (
        <Icon
          onPress={() => navigation.toggleDrawer()}
          containerStyle={{ marginLeft: 15 }}
          name="menu"
          color={colors.white}
        />
      ),
      // headerLeft: (
      //     <TouchableOpacity
      //         onPress={() => navigation.toggleDrawer()}>
      //         <Icon
      //             containerStyle={{width: 18, height: 18, marginLeft: 20}}
      //             name='menu'
      //             color={colors.white}/>
      //         {/*<Image*/}
      //         {/*    containerStyle={{width: 18, height: 18, marginLeft: 10}}*/}
      //         {/*    source={menu}*/}
      //         {/*/>*/}
      //     </TouchableOpacity>
      // ),
      headerStyle: {
        backgroundColor: colors.blueColor
      },
      headerTintColor: "#fff"
    })
  }
});

const createBooking = createStackNavigator({
  First: {
    screen: CreateBookingScreen,
    navigationOptions: ({ navigation }) => ({
      title: "Create Booking ",
      headerLeft: (
        <Icon
          onPress={() => navigation.toggleDrawer()}
          containerStyle={{ marginLeft: 15 }}
          name="menu"
          color={colors.white}
        />
      ),
      headerStyle: {
        backgroundColor: colors.blueColor
      },
      headerTintColor: "#fff"
    })
  }
});

const myBookings = createStackNavigator({
  Fourth: {
    screen: MyBookingsScreen,
    navigationOptions: ({ navigation }) => ({
      title: "My Bookings",
      headerLeft: (
        <Icon
          onPress={() => navigation.toggleDrawer()}
          containerStyle={{ marginLeft: 15 }}
          name="menu"
          color={colors.white}
        />
      ),
      headerStyle: {
        backgroundColor: colors.blueColor
      },
      headerTintColor: "#fff"
    })
  }
});

const search = createStackNavigator({
  Fourth: {
    screen: SearchScreen,
    navigationOptions: ({ navigation }) => ({
      title: "Search",
      headerLeft: (
        <Icon
          onPress={() => navigation.toggleDrawer()}
          containerStyle={{ marginLeft: 15 }}
          name="menu"
          color={colors.white}
        />
      ),
      headerStyle: {
        backgroundColor: colors.blueColor
      },
      headerTintColor: "#fff"
    })
  }
});

const AvailableVehicles = createStackNavigator({
  Fourth: {
    screen: AvailableVehiclesScreen,
    navigationOptions: ({ navigation }) => ({
      title: "Available Vehicles",
      headerLeft: (
        <Icon
          onPress={() => navigation.toggleDrawer()}
          containerStyle={{ marginLeft: 15 }}
          name="menu"
          color={colors.white}
        />
      ),
      headerStyle: {
        backgroundColor: colors.blueColor
      },
      headerTintColor: "#fff"
    })
  }
});

export const DrawerNav = createDrawerNavigator(
  {
    Dashboard: dashboard,
    AllTrips: allTrips,
    AllBooking: allBooking,
    CreateBooking: createBooking,
    MyBookings: myBookings,
    Search: search,
    AvailableVehicles: AvailableVehicles
  },
  {
    contentComponent: drawerContentComponents
  }
);
