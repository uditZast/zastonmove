import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../../colors/Colors";
import AsyncStorage from "@react-native-community/async-storage";
import man from "../../images/man.png";
import { Icon } from "react-native-elements";

export default class drawerContentComponents extends Component {
  constructor(props) {
    super(props);
    this.state = { username: "" };
  }

  componentDidMount() {
    AsyncStorage.getItem("username").then(value =>
      this.setState({ username: value })
    );
  }

  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <View style={styles.headerStyle}>
            <Image source={man} style={styles.logoStyle}></Image>

            <Text style={styles.headingTextStyle}>
              {" "}
              Hi, {this.state.username} !
            </Text>
          </View>
        </View>

        <View style={styles.screenContainer}>
          <View
            style={[
              styles.screenStyle,
              this.props.activeItemKey === "Dashboard"
                ? styles.activeBackgroundColor
                : null
            ]}
          >
            <Icon
              name="home"
              type="feather"
              size={18}
              color={
                this.props.activeItemKey === "Dashboard"
                  ? colors.blueColor
                  : colors.grey
              }
            />

            <Text
              style={[
                styles.screenTextStyle,
                this.props.activeItemKey === "Dashboard"
                  ? styles.selectedTextStyle
                  : null
              ]}
              onPress={this.navigateToScreen("Dashboard")}
            >
              Dashboard
            </Text>
          </View>

          <View style={styles.lineStyle} />

          <View
            style={[
              styles.screenStyle,
              this.props.activeItemKey === "AllTrips"
                ? styles.activeBackgroundColor
                : null
            ]}
          >
            <Icon
              name="truck"
              type="feather"
              size={18}
              color={
                this.props.activeItemKey === "AllTrips"
                  ? colors.blueColor
                  : colors.grey
              }
            />

            <Text
              style={[
                styles.screenTextStyle,
                this.props.activeItemKey === "AllTrips"
                  ? styles.selectedTextStyle
                  : null
              ]}
              onPress={this.navigateToScreen("AllTrips")}
            >
              All Trips
            </Text>
          </View>

          <View style={styles.lineStyle} />

          <View
            style={[
              styles.screenStyle,
              this.props.activeItemKey === "AllBooking"
                ? styles.activeBackgroundColor
                : null
            ]}
          >
            <Icon
              name="book"
              type="feather"
              size={18}
              color={
                this.props.activeItemKey === "AllBooking"
                  ? colors.blueColor
                  : colors.grey
              }
            />

            <Text
              style={[
                styles.screenTextStyle,
                this.props.activeItemKey === "AllBooking"
                  ? styles.selectedTextStyle
                  : null
              ]}
              onPress={this.navigateToScreen("AllBooking")}
            >
              All Booking
            </Text>
          </View>

          <View style={styles.lineStyle} />

          <View
            style={[
              styles.screenStyle,
              this.props.activeItemKey === "CreateBooking"
                ? styles.activeBackgroundColor
                : null
            ]}
          >
            <Icon
              name="book"
              type="feather"
              size={18}
              color={
                this.props.activeItemKey === "CreateBooking"
                  ? colors.blueColor
                  : colors.grey
              }
            />

            <Text
              style={[
                styles.screenTextStyle,
                this.props.activeItemKey === "CreateBooking"
                  ? styles.selectedTextStyle
                  : null
              ]}
              onPress={this.navigateToScreen("CreateBooking")}
            >
              Create Booking
            </Text>
          </View>

          <View style={styles.lineStyle} />

          <View
            style={[
              styles.screenStyle,
              this.props.activeItemKey === "MyBookings"
                ? styles.activeBackgroundColor
                : null
            ]}
          >
            <Icon
              name="book-open"
              type="feather"
              size={18}
              color={
                this.props.activeItemKey === "MyBookings"
                  ? colors.blueColor
                  : colors.grey
              }
            />

            <Text
              style={[
                styles.screenTextStyle,
                this.props.activeItemKey === "MyBookings"
                  ? styles.selectedTextStyle
                  : null
              ]}
              onPress={this.navigateToScreen("MyBookings")}
            >
              MyBookings
            </Text>
          </View>

          <View style={styles.lineStyle} />

          <View
            style={[
              styles.screenStyle,
              this.props.activeItemKey === "Search"
                ? styles.activeBackgroundColor
                : null
            ]}
          >
            <Icon
              name="search"
              type="feather"
              size={18}
              color={
                this.props.activeItemKey === "Search"
                  ? colors.blueColor
                  : colors.grey
              }
            />

            <Text
              style={[
                styles.screenTextStyle,
                this.props.activeItemKey === "Search"
                  ? styles.selectedTextStyle
                  : null
              ]}
              onPress={this.navigateToScreen("Search")}
            >
              Search
            </Text>
          </View>

          <View style={styles.lineStyle} />

          <View
            style={[
              styles.screenStyle,
              this.props.activeItemKey === "AvailableVehicles"
                ? styles.activeBackgroundColor
                : null
            ]}
          >
            <Icon
              name="truck"
              type="feather"
              size={18}
              color={
                this.props.activeItemKey === "AvailableVehicles"
                  ? colors.blueColor
                  : colors.grey
              }
            />

            <Text
              style={[
                styles.screenTextStyle,
                this.props.activeItemKey === "AvailableVehicles"
                  ? styles.selectedTextStyle
                  : null
              ]}
              onPress={this.navigateToScreen("AvailableVehicles")}
            >
              Available Vehicles
            </Text>
          </View>

          <View
            style={[
              styles.screenStyle,
              this.props.activeItemKey === "VendorEdit"
                ? styles.activeBackgroundColor
                : null
            ]}
          >
            <Icon
              name="truck"
              type="feather"
              size={18}
              color={
                this.props.activeItemKey === "VendorEdit"
                  ? colors.blueColor
                  : colors.grey
              }
            />

            <Text
              style={[
                styles.screenTextStyle,
                this.props.activeItemKey === "VendorEdit"
                  ? styles.selectedTextStyle
                  : null
              ]}
              onPress={this.navigateToScreen("Vendor")}
            >
              Available Vehicles
            </Text>
          </View>

          <View style={styles.lineStyle} />

          <View
            style={[
              styles.screenStyle,
              this.props.activeItemKey === "logout"
                ? styles.activeBackgroundColor
                : null
            ]}
          >
            <Icon
              name="log-out"
              type="feather"
              size={18}
              color={
                this.props.activeItemKey === "logout"
                  ? colors.blueColor
                  : colors.grey
              }
            />

            <TouchableOpacity>
              <Text
                style={[
                  styles.screenTextStyle,
                  this.props.activeItemKey === "logout"
                    ? styles.selectedTextStyle
                    : null
                ]}
                onPress={() => {
                  AsyncStorage.removeItem("token");
                  this.props.navigation.navigate("Auth");
                }}
              >
                Logout
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center"
  },
  headerContainer: {
    height: 120,
    flexDirection: "row"
  },
  screenContainer: {
    paddingTop: 10,
    width: "100%"
  },
  screenStyle: {
    height: 40,
    marginTop: 2,
    paddingLeft: 25,
    flexDirection: "row",
    alignItems: "center",
    width: "100%"
  },
  screenTextStyle: {
    fontSize: 16,
    marginLeft: 15,
    padding: 5,
    textAlign: "left",
    flex: 1,
    fontFamily: "CircularStd-Book"
  },
  selectedTextStyle: {
    color: colors.blueColor,
    paddingTop: 5,
    fontFamily: "CircularStd-Book"
  },
  activeBackgroundColor: {
    fontWeight: "400",
    backgroundColor: colors.white
  },
  headerStyle: {
    flex: 1,
    width: 280,
    flexDirection: "row",
    backgroundColor: colors.blueColor,
    alignItems: "flex-end",
    paddingBottom: 10
  },
  headingTextStyle: {
    color: colors.white,
    fontFamily: "CircularStd-Book",
    fontSize: 15,
    padding: 5,
    marginLeft: 10,
    justifyContent: "center"
  },
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 5
  },
  logoStyle: {
    height: 35,
    width: 35,
    marginLeft: 20,
    resizeMode: "contain"
  }
});
