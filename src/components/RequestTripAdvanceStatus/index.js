import React, { Component, Fragment } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import colors from "../../components/common/Colors";
import { Icon } from "react-native-elements";

class RequestTripAdvanceStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      details: this.props.vendorDetails
    };
  }

  closeDetails = () => {
    const { closeVendorDetails } = this.props;
    this.setState({});
    closeVendorDetails(this.state.details);
  };

  showDetails = () => {
    const { vendorDetailsTap } = this.props;
    vendorDetailsTap();
    // const {showVendorDetails} = this.props;
    // if (this.state.details) {
    //   this.setState({details: false}, () =>
    //     showVendorDetails(this.state.details)
    //   );
    // } else {
    //   this.setState({details: true}, () => {
    //     showVendorDetails(this.state.details);
    //   });
    // }
  };

  tripChargesTap = () => {
    const { tripChargesOnTap } = this.props;
    tripChargesOnTap();
  };

  tripAdvanceTap = () => {
    const { tripAdvanceOnTap } = this.props;
    tripAdvanceOnTap();
  };

  showAdvancePayError = () => {
    const { requestTripAdvance } = this.props;
    const { trip_advance = {} } = requestTripAdvance || {};

    const { icon: advancePaymentIcon, msg: advancePayMsg } = trip_advance;

    const { showErrorMsg } = this.props;
    showErrorMsg(advancePayMsg);
  };

  showVendorDetailsError = () => {
    const { requestTripAdvance } = this.props;
    const { vendor_details = {} } = requestTripAdvance || {};

    const { icon: vendorIcon, msg: vendorMsg } = vendor_details;

    const { showErrorMsg } = this.props;
    showErrorMsg(vendorMsg);
  };

  render() {
    const { requestTripAdvance } = this.props;

    const {
      vendor_details = {},
      trip_charges = {},
      trip_advance = {},
      pod_history = {},
      update_trip_advance = false
    } = requestTripAdvance || {};

    const { icon: vendorIcon, msg: vendorMsg } = vendor_details;
    const { icon: tripChargesIcon, msg: tripChargesMsg } = trip_charges;
    const { icon: podIcon, msg: podMsg } = pod_history;
    const { icon: advancePaymentIcon, msg: advancePayMsg } = trip_advance;

    return (
      <Fragment>
        {this.props.vendorDetailsShow && (
          <View style={[{ marginTop: 5, marginBottom: 10 }]}>
            <View>
              <View
                style={[
                  {
                    flexDirection: "row",
                    padding: 15,
                    borderBottomLeftRadius: 0,
                    borderBottomRightRadius: 0,
                    borderTopWidth: 1
                  },
                  styles.cardContainerStyle
                ]}
              >
                <View style={{ flex: 0.1, alignItems: "flex-start" }}>
                  <Icon
                    size={22}
                    name={
                      vendorIcon === "warning"
                        ? "alert-circle"
                        : vendorIcon === "ok"
                        ? "check-circle"
                        : "close-circle"
                    }
                    type="material-community"
                    color={
                      vendorIcon === "warning"
                        ? "#f89406"
                        : vendorIcon === "ok"
                        ? colors.blue
                        : colors.red
                    }
                  />
                </View>
                <View style={{ flex: 0.9 }}>
                  <TouchableOpacity
                    onPress={
                      vendorIcon === "error"
                        ? this.showVendorDetailsError
                        : this.showDetails
                    }
                  >
                    <Text style={styles.textStyle}>Vendor Details</Text>
                    <Text
                      style={[
                        styles.textStyle,
                        { color: colors.black40, fontSize: 14, marginTop: 5 }
                      ]}
                    >
                      {vendorMsg}
                    </Text>
                  </TouchableOpacity>
                </View>

                <View style={{ flex: 0.1, justifyContent: "center" }}>
                  <TouchableOpacity
                    onPress={
                      vendorIcon === "error"
                        ? this.showVendorDetailsError
                        : this.showDetails
                    }
                  >
                    <Icon
                      size={22}
                      name={"arrow-right"}
                      type="material-community"
                      color={colors.blue}
                    />
                  </TouchableOpacity>
                </View>
              </View>

              <View
                style={[
                  {
                    flexDirection: "row",
                    padding: 15,
                    borderBottomLeftRadius: 0,
                    borderBottomRightRadius: 0,
                    borderTopLeftRadius: 0,
                    borderTopRightRadius: 0
                  },
                  styles.cardContainerStyle
                ]}
              >
                <View style={{ flex: 0.1, alignItems: "flex-start" }}>
                  <Icon
                    size={22}
                    name={
                      tripChargesIcon === "warning"
                        ? "alert-circle"
                        : tripChargesIcon === "ok"
                        ? "check-circle"
                        : "close-circle"
                    }
                    type="material-community"
                    color={
                      tripChargesIcon === "warning"
                        ? "#f89406"
                        : tripChargesIcon === "ok"
                        ? colors.blue
                        : colors.red
                    }
                  />
                </View>
                <View style={{ flex: 0.9 }}>
                  <TouchableOpacity onPress={this.tripChargesTap}>
                    <Text style={styles.textStyle}>Trip Charges</Text>
                    <Text
                      style={[
                        styles.textStyle,
                        { fontSize: 14, color: colors.black40, marginTop: 5 }
                      ]}
                    >
                      {tripChargesMsg}
                    </Text>
                  </TouchableOpacity>
                </View>

                <View style={{ flex: 0.1, justifyContent: "center" }}>
                  <TouchableOpacity onPress={this.tripChargesTap}>
                    <Icon
                      size={22}
                      name={"arrow-right"}
                      type="material-community"
                      color={colors.blue}
                    />
                  </TouchableOpacity>
                </View>
              </View>

              <View
                style={[
                  {
                    flexDirection: "row",
                    padding: 15,
                    borderTopLeftRadius: 0,
                    borderTopRightRadius: 0,
                    borderBottomRightRadius: 0,
                    borderBottomLeftRadius: 0
                  },
                  styles.cardContainerStyle
                ]}
              >
                <View style={{ flex: 0.1, alignItems: "flex-start" }}>
                  <Icon
                    size={22}
                    name={
                      podIcon === "warning"
                        ? "alert-circle"
                        : podIcon === "ok"
                        ? "check-circle"
                        : "close-circle"
                    }
                    type="material-community"
                    color={
                      podIcon === "warning"
                        ? "#f89406"
                        : podIcon === "ok"
                        ? colors.blue
                        : colors.red
                    }
                  />
                </View>
                <View style={{ flex: 0.9 }}>
                  <Text style={styles.textStyle}>POD</Text>
                  <Text
                    style={[
                      styles.textStyle,
                      { fontSize: 14, color: colors.black40, marginTop: 5 }
                    ]}
                  >
                    {podMsg}
                  </Text>
                </View>
              </View>

              <View
                style={[
                  {
                    flexDirection: "row",
                    padding: 15,
                    borderTopRightRadius: 0,
                    borderTopLeftRadius: 0
                  },
                  styles.cardContainerStyle
                ]}
              >
                <View style={{ flex: 0.1, alignItems: "flex-start" }}>
                  <Icon
                    size={22}
                    name={
                      advancePaymentIcon === "warning"
                        ? "alert-circle"
                        : advancePaymentIcon === "ok"
                        ? "check-circle"
                        : "close-circle"
                    }
                    type="material-community"
                    color={
                      advancePaymentIcon === "warning"
                        ? "#f89406"
                        : advancePaymentIcon === "ok"
                        ? colors.blue
                        : colors.red
                    }
                  />
                </View>
                <View style={{ flex: 0.9 }}>
                  <TouchableOpacity
                    onPress={
                      update_trip_advance
                        ? this.tripAdvanceTap
                        : this.showAdvancePayError
                    }
                  >
                    <Text style={styles.textStyle}>Advance Payments</Text>
                    <Text
                      style={[
                        styles.textStyle,
                        { fontSize: 14, color: colors.black40, marginTop: 5 }
                      ]}
                    >
                      {advancePayMsg}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 0.1, justifyContent: "center" }}>
                  <TouchableOpacity
                    onPress={
                      update_trip_advance
                        ? this.tripAdvanceTap
                        : this.showAdvancePayError
                    }
                  >
                    <Icon
                      size={22}
                      name={"arrow-right"}
                      type="material-community"
                      color={colors.blue}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        )}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  labelTextStyle: {
    fontSize: 12,
    color: colors.black65,
    fontFamily: "CircularStd-Book",
    padding: 5
  },
  cardContainerStyle: {
    borderColor: "#ddd",
    borderRadius: 2,
    borderLeftWidth: 1,
    borderBottomWidth: 1,
    borderRightWidth: 1,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: colors.white
  },
  textStyle: {
    fontSize: 16,
    color: colors.textcolor,
    fontFamily: "CircularStd-Book"
  },
  verticalLineStyle: {
    borderLeftColor: colors.black65,
    borderLeftWidth: 1,
    height: 40,
    alignSelf: "center",
    justifyContent: "center"
  }
});

export default RequestTripAdvanceStatus;
