import React, { Component } from "react";
import { Image, StyleSheet, TextInput, View } from "react-native";
import colors from "../colors/Colors";
import Icon from "react-native-vector-icons/MaterialIcons";
import logo from "../images/onmove_logo.png";
import AsyncStorage from "@react-native-community/async-storage";
import { LOGIN } from "../constants/Constants";
import Snackbar from "react-native-snackbar";
import { Button } from "react-native-elements";
import { NAVIGATE } from "../../constants";

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      error: null,
      token: "",
      loading: false,
      buttonLoading: false
    };
  }

  componentDidMount() {
    console.log("came here 2 login screen");
  }

  render() {
    // const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={{ flex: 0.3, alignItems: "center" }}>
          <Image source={logo} style={styles.logoStyle} />
        </View>

        <View style={styles.redBackStyle}>
          <View style={styles.containerStyle}>
            <View style={styles.blankContainer} />

            <View style={styles.rowStyle}>
              <Icon name="email" color={colors.blueColor} size={20} />
              <TextInput
                maxLength={50}
                label="Email"
                text={this.state.expectedtat}
                style={styles.StyleInput}
                onChangeText={text => this.setState({ username: text })}
                autoCapitalize={"none"}
                underlineColor={"#FFF"}
                placeholder={"Username"}
                placeholderTextColor={colors.border_grey}
              />
            </View>

            <View style={styles.lineStyle} />

            <View style={styles.rowStyle}>
              <Icon name="lock-outline" size={20} color={colors.blueColor} />
              <TextInput
                maxLength={50}
                label="Password"
                style={styles.StyleInput}
                onChangeText={text => this.setState({ password: text })}
                autoCapitalize={"none"}
                underlineColor={"#FFF"}
                placeholder={"Password"}
                secureTextEntry={true}
                placeholderTextColor={colors.border_grey}
              />
            </View>

            <View style={styles.lineStyle} />

            <Button
              onPress={() => {
                this.setState({ buttonLoading: true });
                this.getLogin();
              }}
              title={"Login"}
              textStyle={{
                fontSize: 15,
                fontFamily: "CircularStd-Book"
              }}
              buttonStyle={{
                marginLeft: 15,
                marginRight: 15,
                marginTop: 20,
                marginBottom: 20
              }}
              loading={this.state.buttonLoading}
            />
          </View>
        </View>
      </View>
    );
  }

  getLogin() {
    console.log("username " + this.state.username);
    console.log("password " + this.state.password);

    if (this.state.username === "") {
      this.setState({ buttonLoading: false });
      return Snackbar.show({
        title: "Please enter Username",
        duration: Snackbar.LENGTH_SHORT
      });
    } else if (this.state.password === "") {
      this.setState({ buttonLoading: false });
      return Snackbar.show({
        title: "Please enter Password",
        duration: Snackbar.LENGTH_SHORT
      });
    } else {
      fetch(LOGIN, {
        method: "post",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          username: this.state.username,
          password: this.state.password
        })
      })
        .then(response => response.json())
        .then(res => {
          console.log("res::::" + JSON.stringify(res));
          if (res.otp_reqd === true) {
            console.log("Otp required " + res.otp_reqd);
            this.props.navigation.navigate("OtpScreen1", {
              name: this.state.username,
              user_id: res.user_id
            });
          } else {
            this.setState({ auth_token: res.auth_token });
            console.log("else");
            console.log("Token:: " + res.token);
            AsyncStorage.setItem("token", "" + res.token);
            AsyncStorage.setItem("role", "" + res.data.role);
            AsyncStorage.setItem("rights", "" + res.data.rights);
            AsyncStorage.setItem("username", "" + res.data.username);
            this.props.navigation.navigate(NAVIGATE.HOME);
          }
        })
        .catch(error => {
          console.error(error);
          console.log("error in login " + error);
        });
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  internalViewStyle: {
    alignItems: "center",
    width: "100%",
    alignSelf: "center",
    justifyContent: "center"
  },
  redBackStyle: {
    alignItems: "center",
    flex: 1,
    width: "100%",
    backgroundColor: colors.blueColor,
    justifyContent: "center"
  },
  rowStyle: {
    flexDirection: "row",
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    padding: 10
  },
  imageStyle: {
    width: 20,
    height: 20
  },
  textStyle: {
    color: colors.textcolor,
    fontSize: 15,
    marginLeft: 5,
    alignItems: "center"
  },
  lineStyle: {
    borderBottomWidth: 1,
    borderBottomColor: colors.border_grey,
    marginLeft: 45,
    marginRight: 20
  },
  buttonStyle: {
    backgroundColor: colors.loginGradient2,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    height: 45,
    width: "70%",
    alignItems: "center",
    marginTop: 20,
    marginBottom: 20
  },
  buttonTextStyle: {
    color: colors.white,
    textAlign: "center",
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 20,
    marginRight: 20,
    padding: 10,
    fontSize: 15,
    fontFamily: "CircularStd-Book",
    backgroundColor: colors.blueColor
  },
  headingStyle: {
    fontSize: 20,
    color: colors.white,
    fontWeight: "bold",
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
    padding: 5,
    alignItems: "center",
    alignSelf: "center"
  },
  logoStyle: {
    height: 80,
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 20,
    marginRight: 20,
    resizeMode: "contain",
    alignSelf: "center"
  },
  blankContainer: {
    height: 50,
    width: "100%"
  },
  containerStyle: {
    width: "80%",
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 1,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    marginLeft: 25,
    marginTop: 5,
    marginRight: 25,
    marginBottom: 5,
    borderTopLeftRadius: 3,
    backgroundColor: colors.white,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3
  },
  StyleInput: {
    fontSize: 12,
    color: colors.textcolor,
    marginLeft: 10,
    justifyContent: "center",
    fontFamily: "CircularStd-Book",
    padding: 0,
    width: "100%"
  }
});

export default LoginScreen;

//
// <View style={styles.container}>
//     <View style={styles.internalViewStyle}>
//         <Image source={logo}
//                style={styles.logoStyle}/>
//
//     </View>
//
//     <View style={styles.redBackStyle}>
//
//         <View style={styles.containerStyle}>
//             {/*<View style={styles.blankContainer}/>*/}
//
//             <View style={styles.rowStyle}>
//
//                 <TextInput
//                     autoCapitalize={'none'}
//                     style={styles.StyleInput}
//                     onChangeText={(text) => this.setState({username: text})}>Username</TextInput>
//
//             </View>
//
//             <View style={styles.lineStyle}/>
//
//             <View style={styles.rowStyle}>
//                 <TextInput
//                     autoCapitalize={'none'}
//                     secureTextEntry={true}
//                     style={styles.StyleInput}
//                     onChangeText={(text) => this.setState({password: text})}>Password</TextInput>
//             </View>
//             <View style={styles.lineStyle}/>
//
//
//             <Button onPress={() => {
//                 this.setState({buttonLoading: true})
//                 this.getLogin()
//             }}
//                     title={'Login'}
//                     style={styles.buttonTextStyle}
//                     loading={this.state.buttonLoading}>
//
//             </Button>
//             {/*<View style={{alignSelf: 'center'}}>*/}
//             {/*    <TouchableOpacity*/}
//             {/*        onPress={this.getLogin.bind(this)}>*/}
//             {/*        <Text style={styles.buttonTextStyle}>Login</Text>*/}
//             {/*    </TouchableOpacity>*/}
//
//             {/*</View>*/}
//
//
//         </View>
//
//     </View>
// </View>
