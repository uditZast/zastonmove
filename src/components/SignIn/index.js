import React, { Component, Fragment } from "react";
import { withNavigationFocus } from "react-navigation";
import {
  Image,
  Text,
  ScrollView,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  ActivityIndicator,
  Keyboard,
  Platform
} from "react-native";
import { Icon } from "react-native-elements";
import Snackbar from "react-native-snackbar";
import colors from "../../components/common/Colors";
import logoColor from "../../images/onmove_logo.png";
import { NAVIGATE, OS } from "../../../constants";
import DeviceInfo from "react-native-device-info";

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      showPassword: false
    };
  }

  componentDidMount() {
    const { signOut } = this.props;
    signOut();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isFocused !== this.props.isFocused) {
      this.setState({ username: null, password: null });
    }
  }

  submitRequest = () => {
    Keyboard.dismiss();
    const { username, password } = this.state;
    if (username === "" || password === "") {
      Snackbar.show({
        title:
          username === "" ? "Please enter Username" : "Please enter Password",
        duration: Snackbar.LENGTH_SHORT
      });
    } else {
      const { signIn } = this.props;
      signIn({ username, password }).then(result => {
        const {
          status,
          message,
          data: { otpReqd = false, user_id, versions = {}, user_role } = {}
        } = result;
        const { ios_force_update_version, android_force_update_version } =
          versions[user_role] || {};
        let version = DeviceInfo.getBuildNumber();
        version = version ? Number(version) : 0;
        if (
          (Platform.OS === OS.IOS && version < ios_force_update_version) ||
          (Platform.OS === OS.ANDROID && version < android_force_update_version)
        ) {
          console.log("---- 11111111111111111 ----");
          this.props.navigation.navigate(NAVIGATE.FORCED_UPDATE);
        } else {
          if (status === false) {
            if (message) {
              Snackbar.show({
                title: message,
                duration: Snackbar.LENGTH_SHORT
              });
            }
            if (otpReqd === true) {
              this.props.navigation.navigate(NAVIGATE.OTP_SCREEN, {
                username: this.state.username,
                user_id
              });
            }
          } else if (status === true) {
            this.props.navigation.navigate(NAVIGATE.HOME);
          }
        }
      });
    }
  };

  toggleShowPassword = () => {
    this.setState(prevState => {
      return { showPassword: !prevState.showPassword };
    });
  };

  render() {
    const { auth: { loading } = {} } = this.props;
    const { showPassword } = this.state;
    return (
      <Fragment>
        <View
          style={{
            top: 0,
            height: 50,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: colors.blue
          }}
        >
          <Text
            style={{ fontSize: 16, fontWeight: "500", color: colors.white }}
          >
            LOGIN
          </Text>
        </View>

        <ScrollView
          contentContainerStyle={{ flexGrow: 1, padding: 16 }}
          keyboardShouldPersistTaps="handled"
          scrollEnabled={false}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginBottom: 16
            }}
          >
            <Image
              source={logoColor}
              style={{ height: 100, width: 200 }}
              resizeMode="contain"
            />
          </View>
          <View>
            <View style={{ marginLeft: 5, marginTop: 16 }}>
              <Text style={{ color: colors.black40 }}>Username</Text>
            </View>
            <View style={styles.textInputView}>
              <TextInput
                // placeholder={"Enter username"}
                style={styles.textInput}
                onChangeText={text => this.setState({ username: text })}
                value={this.state.username ? String(this.state.username) : ""}
                // placeholderTextColor={colors.black25}
                autoCapitalize={"none"}
              />
            </View>
            <View style={{ marginLeft: 5, marginTop: 16 }}>
              <Text style={{ color: colors.black40 }}>Password</Text>
            </View>
            <View style={styles.textInputView}>
              <TextInput
                // placeholder={"Enter password"}
                style={styles.textInput}
                secureTextEntry={!showPassword}
                onChangeText={text => this.setState({ password: text })}
                value={this.state.password ? String(this.state.password) : ""}
                // placeholderTextColor={colors.black25}
                autoCapitalize={"none"}
              />
              <Icon
                size={24}
                containerStyle={{
                  // position: "absolute",
                  alignSelf: "center",
                  right: 10,
                  marginLeft: 4
                }}
                name={showPassword ? "eye-off" : "eye"}
                type="material-community"
                color={colors.darkblue}
                onPress={this.toggleShowPassword}
              />
            </View>
          </View>
          <TouchableOpacity
            style={{
              height: 50,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: colors.blue,
              borderRadius: 6,
              marginTop: 32
            }}
            onPress={loading ? null : this.submitRequest}
          >
            {loading && <ActivityIndicator size="small" color={colors.white} />}
            {!loading && (
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: "500",
                  color: colors.white
                }}
              >
                Continue
              </Text>
            )}
          </TouchableOpacity>
        </ScrollView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  textInputView: {
    flexDirection: "row",
    // marginTop: 16,
    marginBottom: 16,
    marginLeft: 5,
    marginRight: 5,
    borderBottomWidth: 2,
    borderBottomColor: colors.black15
  },
  textInput: {
    flex: 1,
    padding: 10,
    ...Platform.select({
      android: { paddingBottom: 2 }
    })
  }
});

export default withNavigationFocus(SignIn);
