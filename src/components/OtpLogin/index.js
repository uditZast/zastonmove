import React, { Component, Fragment } from "react";
import {
  Keyboard,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import Snackbar from "react-native-snackbar";
import { NAVIGATE } from "../../../constants";
import { getUniqueId } from "react-native-device-info";
import Header from "../common/header";
import colors from "../common/Colors";

export default class OtpScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { otp: null, timer: 60 };
    this.SMSReadSubscription = {};
  }

  componentDidMount() {
    // this.requestReadSmsPermission().then(result => {
    //   console.log("result ----", result);
    //   if (result) {
    //     this.SMSReadSubscription = SmsListener.addListener(message => {
    //       console.log("Message:", message);
    //       const { body = "" } = message;
    //       let newMessage = String(body);
    //       newMessage = newMessage.split("\n");
    //       if (newMessage.length > 0) {
    //         const otpString = newMessage[0];
    //         if (otpString.length > 0) {
    //           const otp = otpString.substring(otpString.length - 6);
    //           if (otp.length === 6) {
    //             this.setState({ otp });
    //           }
    //         }
    //       }
    //     });
    //   }
    // });
    this.interval = setInterval(
      () => this.setState(prevState => ({ timer: prevState.timer - 1 })),
      1000
    );
  }

  // requestReadSmsPermission = async () => {
  //   try {
  //     let granted = await PermissionsAndroid.request(
  //       PermissionsAndroid.PERMISSIONS.READ_SMS,
  //       {
  //         title: "Auto Verification OTP",
  //         message: "need access to read sms, to verify OTP"
  //       }
  //     );
  //     if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //       console.log("READ_SMS permissions granted", granted);
  //       granted = await PermissionsAndroid.request(
  //         PermissionsAndroid.PERMISSIONS.RECEIVE_SMS,
  //         {
  //           title: "Receive SMS",
  //           message: "Need access to receive sms, to verify OTP"
  //         }
  //       );
  //       return granted === PermissionsAndroid.RESULTS.GRANTED;
  //     } else {
  //       return false;
  //     }
  //   } catch (err) {
  //     return false;
  //   }
  // };

  componentDidUpdate() {
    if (this.state.timer === 0) {
      clearInterval(this.interval);
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
    // if (this.SMSReadSubscription.remove) {
    //   this.SMSReadSubscription.remove();
    // }
  }

  verifyOtp = () => {
    Keyboard.dismiss();
    const { verifyOtp, username } = this.props;
    const { otp } = this.state;
    verifyOtp({
      username,
      otp,
      device_id: getUniqueId()
    }).then(result => {
      const { status, message } = result;
      if (status === true) {
        this.props.navigation.navigate(NAVIGATE.HOME);
      } else if (status === false) {
        Snackbar.show({
          title: message,
          duration: Snackbar.LENGTH_LONG
        });
      }
    });
  };

  resendOtp = () => {
    Keyboard.dismiss();
    const { sendOtp, user_id, username } = this.props;
    sendOtp({ user_id, username });
    this.setState({ timer: 60 }, () => {
      this.interval = setInterval(() => {
        this.setState(prevState => ({ timer: prevState.timer - 1 }));
      }, 1000);
    });
  };

  render() {
    const { timer } = this.state;

    return (
      <Fragment>
        <Header
          name={"OTP Verification"}
          navigation={this.props.navigation}
          goBack={true}
        />
        <View style={{ flex: 1 }}>
          <View style={{ padding: 8, paddingTop: 16, paddingBottom: 16 }}>
            <View style={{ paddingBottom: 16 }}>
              <Text style={{ fontWeight: "500", fontSize: 16 }}>
                Enter verification code
              </Text>
            </View>

            <Text>
              Zast Onmove has sent a verification code on your registered mobile
              number. Please enter it below to proceed.
            </Text>
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              paddingTop: 0,
              paddingBottom: 16
              // borderBottomWidth: 1,
              // borderBottomColor: colors.black25
            }}
          >
            <TextInput
              placeholder={"Enter OTP"}
              style={{
                borderBottomWidth: 1,
                borderBottomColor: colors.black25,
                justifyContent: "center",
                alignItems: "center"
              }}
              maxLength={6}
              textContentType="oneTimeCode"
              underlineColorAndroid={"transparent"}
              keyboardType="numeric"
              onChangeText={text => this.setState({ otp: text })}
              value={this.state.otp ? String(this.state.otp) : ""}
              placeholderTextColor={colors.black25}
            />
          </View>
          <View style={{ flexDirection: "row", padding: 8 }}>
            <TouchableOpacity
              style={[
                timer !== 0 ? styles.buttonDisabled : styles.button,
                { marginRight: 4 }
              ]}
              onPress={timer === 0 ? this.resendOtp : null}
            >
              <Text
                style={{ color: timer !== 0 ? colors.black15 : colors.blue }}
              >
                {timer === 0 ? `Resend` : `Resend in ${this.state.timer} sec`}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                timer < 0 ? styles.buttonDisabled : styles.button,
                { marginLeft: 4 }
              ]}
              onPress={this.verifyOtp}
            >
              <Text style={{ color: colors.blue }}>Proceed</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 8,
    borderWidth: 2,
    borderColor: colors.blue,
    borderRadius: 4
  },
  buttonDisabled: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 8,
    borderWidth: 2,
    borderColor: colors.black15,
    borderRadius: 4
  }
});
