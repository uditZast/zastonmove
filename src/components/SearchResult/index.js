import React, { Component, Fragment } from "react";
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { NAVIGATE } from "../../../constants";
import Card from "../Card";
import BookingListCard from "../../containers/BookingListCard";
import Header from "../common/header";
import moment from "moment";
import TripCard from "../common/TripCard";
import colors from "../common/Colors";
import { BarIndicator } from "react-native-indicators";
import BookingCard from "../common/BookingCard";
import NoResult from "../common/NoResultsPage";
import ReadMore from "react-native-read-more-text";
import MapModal from "../common/MapModal";
import { Icon } from "react-native-elements";

class SearchResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customer: null,
      routeName: null,
      vendor: null,
      vehicle: null,
      vehicleType: null,
      tripTime: new moment(),
      code: null,
      type: null,
      tripSelectedIndex: 0,
      bookingSelectedIndex: 0,
      isLoading: true,
      address: null, //For Map Modal
      lat: null, //For Map Modal
      long: null, //For Map Modal
      visible: false //For Map Modal
    };
  }

  componentDidMount() {
    const { fetchSearchResult, requestData } = this.props;

    fetchSearchResult(requestData)
      .then(response => {
        console.log("response ----", response);
      })
      .catch(error => {
        console.log("error ----", error);
      })
      .finally(() => this.setState({ isLoading: false }));
  }

  getBookingSearchData = () => {
    const {
      pageData: { isFetching = false, booking_ids = [] } = {}
    } = this.props;
    if (booking_ids.length > 0) {
      return (
        <Fragment>
          <Text
            style={{
              fontSize: 16,
              fontFamily: "CircularStd-Book",
              color: colors.black,
              marginLeft: 10,
              marginTop: 10
            }}
          >
            Booking Details
          </Text>
          <FlatList
            data={[...booking_ids]}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderBooking}
            refreshing={isFetching}
            onRefresh={this.props.fetchBookingsByStatus}
            showsVerticalScrollIndicator={false}
            extraData={this.state.bookingSelectedIndex}
            keyboardShouldPersistTaps={"handled"}
          />
        </Fragment>
      );
    } else {
      return null;
    }
  };

  getTripsSearchData = () => {
    const { pageData: { trip_ids = [], isFetching = false } = {} } = this.props;

    if (trip_ids.length > 0) {
      return (
        <Fragment>
          <Text
            style={{
              fontSize: 16,
              fontFamily: "CircularStd-Book",
              color: colors.black,
              marginLeft: 10,
              marginTop: 10
            }}
          >
            Trip Details
          </Text>
          <FlatList
            data={[...trip_ids]}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderTrip}
            refreshing={isFetching}
            onRefresh={this.props.fetchBookingsByStatus}
            showsVerticalScrollIndicator={false}
            extraData={this.state.tripSelectedIndex}
            keyboardShouldPersistTaps={"handled"}
          />
        </Fragment>
      );
    } else {
      return null;
    }
  };

  selectBookingIndex = bookingSelectedIndex => {
    this.setState({ bookingSelectedIndex });
  };

  selectTripIndex = tripSelectedIndex => {
    this.setState({ tripSelectedIndex });
  };

  selectIndex = selectedIndex => {
    this.setState({ selectedIndex });
  };

  renderBooking = ({ index, item: booking_id }) => {
    const {
      bookings,
      customers,
      vendors,
      vehicles,
      navigation,
      vehicleTypes,
      engagedBy
    } = this.props;
    const booking = bookings[booking_id] || {};
    return (
      <View key={index} style={`${index}` === "0" ? { paddingTop: 10 } : {}}>
        <BookingCard
          bookings={bookings}
          booking={booking}
          customers={customers}
          vendors={vendors}
          vehicles={vehicles}
          vehicleTypes={vehicleTypes}
          engagedBy={engagedBy}
          navigation={navigation}
          index={index}
          selectIndex={this.selectBookingIndex}
          selectedIndex={this.state.selectedIndex}
          screen={"search"}
          statusVisibility={false}
          {...this.props}
        />
      </View>
    );
  };

  renderBookingItem = ({ index, item: booking_id }) => {
    const { bookings, customers, vehicles, vendors, vehicleTypes } = this.props;

    const {
      basicInfo: {
        code,
        customer_id,
        expected_tat,
        id,
        return_booking_id,
        route,
        trip_time,
        type,
        vehicle_id,
        vehicle_type_id,
        vendor_id,
        booking_history_id
      } = {}
    } = bookings[booking_id] || {};

    const customer = customers[customer_id] || {};
    const vehicle = vehicles[vehicle_id] || {};
    const vendor = vendors[vendor_id] || {};
    const vehicleType = vehicleTypes[vehicle_type_id] || {};

    return (
      <Fragment key={index}>
        <Card>
          <TouchableOpacity activeOpacity={0.7}>
            <BookingListCard
              id={id}
              code={code}
              bookingType={type}
              vehicle={vehicle}
              vehicleType={vehicleType}
              customer={customer}
              vendor={vendor}
              time={trip_time}
              route={route}
              tat={expected_tat}
              index={index}
              selectedIndex={this.state.bookingSelectedIndex}
              selectIndex={this.selectBookingIndex}
              screen={"search"}
              return_booking_id={return_booking_id}
              booking_history_id={booking_history_id}
            />
          </TouchableOpacity>
        </Card>
      </Fragment>
    );
  };

  renderTrip = ({ index, item: trip_id }) => {
    const { trips, customers, vendors, vehicles, navigation } = this.props;

    const trip = trips[trip_id];

    return (
      <View key={index} style={`${index}` === "0" ? { paddingTop: 10 } : {}}>
        <TripCard
          trip={trip}
          customers={customers}
          vendors={vendors}
          vehicles={vehicles}
          index={index}
          showStatus={true}
          navigation={navigation}
          selectedIndex={this.state.tripSelectedIndex}
          selectIndex={this.selectTripIndex}
          actionsVisiblity={false}
          commentsView={this.commentsView}
          {...this.props}
        />
      </View>
    );
  };

  commentsView = () => {
    const { trip_id } = this.props;
    this.props.navigation.navigate(NAVIGATE.TRIP_COMMENTS, { trip_id });
  };

  closeModal = () => {
    this.setState({ visible: false });
  };

  setModalDetails = (address, lat, long) => () => {
    this.setState({ address, lat, long, visible: true });
  };

  renderVehicleDetails = () => {
    const {
      requestData: { vehicle_id } = {},
      vehicles,
      vendors,
      vehicleTypes
    } = this.props;
    const {
      basicInfo: {
        vehicle_number = null,
        vehicle_category = "",
        vendor_id,
        vehicle_type_id
      } = {},
      location: { lat, long, address } = {}
    } = vehicles[vehicle_id] || {};
    const { basicInfo: { code: vendor_code } = {} } = vendors[vendor_id] || {};
    const { basicInfo: { name: vehicleType } = {} } =
      vehicleTypes[vehicle_type_id] || {};
    return (
      <Fragment>
        {vehicle_number && (
          <View style={styles.cardContainer}>
            <View style={styles.cardRow}>
              <View style={styles.flex1}>
                <Text style={styles.headingStyle}>Vehicle Number</Text>
                <Text style={styles.subHeadingStyle}>{vehicle_number}</Text>
              </View>
              <View style={styles.flex1}>
                <Text style={styles.headingStyle}>Vehicle Type</Text>
                <Text style={styles.subHeadingStyle}>{vehicleType}</Text>
              </View>
            </View>
            <View style={styles.cardRow}>
              <View style={styles.flex1}>
                <Text style={styles.headingStyle}>Vendor</Text>
                <Text style={styles.subHeadingStyle}>{vendor_code}</Text>
              </View>
              <View style={styles.flex1}>
                <Text style={styles.headingStyle}>Vehicle Category</Text>
                <Text style={styles.subHeadingStyle}>{vehicle_category}</Text>
              </View>
            </View>
            <View style={styles.cardRow}>
              {address && (
                <Fragment>
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      paddingBottom: 4,
                      alignItems: "center",
                      flex: 1
                    }}
                    onPress={this.setModalDetails(address, lat, long)}
                  >
                    <Icon
                      size={25}
                      name="crosshairs-gps"
                      type="material-community"
                      containerStyle={{ paddingRight: 8 }}
                      color={colors.blue}
                    />
                    <View style={{ flex: 1 }}>
                      <Text
                        style={{
                          fontSize: 14,
                          fontWeight: "400",
                          color: colors.blue
                        }}
                        numberOfLines={2}
                      >
                        {address}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </Fragment>
              )}
            </View>
          </View>
        )}
      </Fragment>
    );
  };

  renderSearchedFields = () => {
    const { requestData, cities, customers, vehicles } = this.props;
    const {
      booking_code,
      trip_code,
      reference,
      vehicle_id,
      customer_id,
      source_city_id,
      destination_city_id
    } = requestData || {};
    const { basicInfo: { name: customerName } = {} } =
      customers[customer_id] || {};
    const { basicInfo: { vehicle_number } = {} } = vehicles[vehicle_id] || {};
    const { basicInfo: { name: sourceCityName } = {} } =
      cities[source_city_id] || {};
    const { basicInfo: { name: destinationCityName } = {} } =
      cities[destination_city_id] || {};

    return (
      <View style={styles.searchContainer}>
        <ReadMore
          numberOfLines={1}
          renderTruncatedFooter={handlePress => {
            return (
              <Text
                style={{ color: colors.blue, marginTop: 5 }}
                onPress={handlePress}
              >
                Show more
              </Text>
            );
          }}
          renderRevealedFooter={handlePress => {
            return (
              <Text
                style={{ color: colors.blue, marginTop: 5 }}
                onPress={handlePress}
              >
                Show less
              </Text>
            );
          }}
        >
          <Fragment>
            {!!booking_code && (
              <Text style={styles.searchHeadingStyle}>
                Booking Id:
                <Text style={styles.subHeadingStyle}> {booking_code} </Text>
              </Text>
            )}
            {!!trip_code && (
              <Text style={styles.searchHeadingStyle}>
                Trip Id:
                <Text style={styles.subHeadingStyle}> {trip_code} </Text>
              </Text>
            )}
            {!!reference && (
              <Text style={styles.searchHeadingStyle}>
                Reference:
                <Text style={styles.subHeadingStyle}> {reference} </Text>
              </Text>
            )}
            {!!customer_id && (
              <Text style={styles.searchHeadingStyle}>
                Customer:
                <Text style={styles.subHeadingStyle}> {customerName} </Text>
              </Text>
            )}
            {!!vehicle_number && (
              <Text style={styles.searchHeadingStyle}>
                Vehicle:
                <Text style={styles.subHeadingStyle}> {vehicle_number} </Text>
              </Text>
            )}
            {!!source_city_id && (
              <Text style={styles.searchHeadingStyle}>
                Source City:
                <Text style={styles.subHeadingStyle}> {sourceCityName} </Text>
              </Text>
            )}
            {!!destination_city_id && (
              <Text style={styles.searchHeadingStyle}>
                Destination City:
                <Text style={styles.subHeadingStyle}>
                  {destinationCityName}
                </Text>
              </Text>
            )}
          </Fragment>
        </ReadMore>
      </View>
    );
  };

  render() {
    const { isLoading } = this.state;
    const { pageData: { booking_ids = [], trip_ids = [] } = {} } = this.props;
    const { address, lat, long, visible } = this.state;

    return (
      <Fragment>
        <Header
          name={"SEARCH"}
          navigation={this.props.navigation}
          goBack={true}
        />
        {isLoading && (
          <View style={{ flex: 1, padding: 20 }}>
            <BarIndicator color={colors.blueColor} size={20} />
          </View>
        )}
        {!isLoading && (
          <Fragment>
            {this.renderSearchedFields()}
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
              {this.renderVehicleDetails()}
              {booking_ids.length > 0 && this.getBookingSearchData()}
              {trip_ids.length > 0 && this.getTripsSearchData()}
              {booking_ids.length === 0 && trip_ids.length === 0 && (
                <NoResult />
              )}
            </ScrollView>
          </Fragment>
        )}
        {lat && long && (
          <MapModal
            location={address}
            lat={lat}
            long={long}
            isVisible={visible}
            closeModal={this.closeModal}
          />
        )}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  searchContainer: {
    margin: 10,
    marginBottom: 0,
    padding: 10,
    borderWidth: 1,
    backgroundColor: colors.black02,
    borderColor: colors.black15,
    borderRadius: 6,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center"
  },
  cardContainer: {
    margin: 10,
    // marginBottom: 0,
    padding: 10,
    paddingBottom: 0,
    borderWidth: 1,
    backgroundColor: colors.black02,
    borderColor: colors.black15,
    borderRadius: 6
  },
  cardRow: {
    flexDirection: "row",
    paddingBottom: 10
  },
  flex1: {
    flex: 1
  },
  subHeadingStyle: {
    fontSize: 16,
    fontWeight: "bold",
    color: colors.black65
  },
  searchHeadingStyle: {
    fontSize: 16,
    fontWeight: "bold",
    color: colors.blue
  },
  headingStyle: {
    fontSize: 14,
    fontWeight: "bold",
    color: colors.blue
  }
});

export default SearchResult;
