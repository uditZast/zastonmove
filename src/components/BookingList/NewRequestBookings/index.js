import React, { Component, Fragment } from "react";
import {
  FlatList,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../../../colors/Colors";
import { SearchBar } from "react-native-elements";
import Header from "../../common/header";
import Card from "../../Card";
import BookingListCard from "../../../containers/BookingListCard";
import customStyles from "../../common/Styles";
import BookingCard from "../../common/BookingCard";

class NewRequestBookings extends Component {
  constructor(props) {
    super(props);
    this.state = { customer: "", selectedIndex: 0, input: null };
  }

  componentDidMount() {
    const { loggedInUser } = this.props;
    const { role } = loggedInUser;
    this.fetchBookings();
  }

  componentDidUpdate(prevProps, prevState) {
    const { navigation: { isFocused } = {} } = this.props;
    const didFocus = isFocused();
    const { input } = this.state;

    if (!didFocus && input) {
      this.setState({ input: null });
    }
  }

  fetchBookings = () => {
    const { fetchBookingsByStatus } = this.props;
    fetchBookingsByStatus();
  };

  clearInput = () => {
    this.setState({ input: null });
  };

  setSearch = text => {
    this.setState({ input: text });
  };

  renderBookingItem = ({ index, item: booking_id }) => {
    const {
      bookings,
      customers,
      vehicles,
      vendors,
      vehicleTypes,
      navigation,
      trips
    } = this.props;

    const {
      basicInfo: {
        code,
        customer_id,
        expected_tat,
        id,
        return_booking_id,
        route,
        trip_time,
        type,
        vehicle_id,
        vehicle_type_id,
        vendor_id,
        booking_history_id
      } = {}
    } = bookings[booking_id] || {};

    const { status = null } = bookings[booking_id];

    const customer = customers[customer_id] || {};
    const vehicle = vehicles[vehicle_id] || {};
    const vendor = vendors[vendor_id] || {};
    const vehicleType = vehicleTypes[vehicle_type_id] || {};

    return (
      <Fragment key={index}>
        <Card>
          <TouchableOpacity activeOpacity={0.7}>
            <BookingListCard
              loggedInUser={this.props.loggedInUser}
              bookingStatus={status}
              id={id}
              code={code}
              bookingType={type}
              vehicle={vehicle}
              vehicleType={vehicleType}
              customer={customer}
              vendor={vendor}
              time={trip_time}
              route={route}
              tat={expected_tat}
              index={index}
              selectIndex={this.selectIndex}
              selectedIndex={this.state.selectedIndex}
              screen={"new_request"}
              return_booking_id={return_booking_id}
              booking_history_id={booking_history_id}
              navigation={navigation}
            />
          </TouchableOpacity>
        </Card>
      </Fragment>
    );
  };

  selectIndex = selectedIndex => {
    this.setState({ selectedIndex });
  };

  renderBookingList = () => {
    const {
      bookings,
      vendors,
      vehicles,
      pageData: { new_requests: booking_ids = [], isFetching = false } = {}
    } = this.props;

    const { input } = this.state;

    let list = booking_ids;

    if (input && input.toUpperCase()) {
      const u_input = input.toUpperCase();
      list = list.filter(booking_id => {
        const { basicInfo: { code, type, route, vendor_id, vehicle_id } = {} } =
          bookings[booking_id] || {};

        let return_val =
          code.toUpperCase().includes(u_input) ||
          type.toUpperCase().includes(u_input) ||
          route.toUpperCase().includes(u_input);

        if (vendor_id) {
          const { basicInfo: { code: vendorCode, nick_name } = {} } =
            vendors[vendor_id] || {};
          if (nick_name) {
            return_val =
              return_val ||
              vendorCode.toUpperCase().includes(u_input) ||
              nick_name.toUpperCase().includes(u_input);
          } else {
            return_val =
              return_val || vendorCode.toUpperCase().includes(u_input);
          }
        }

        if (vehicle_id) {
          const { basicInfo: { vehicle_number } = {} } = vehicles[vehicle_id];
          return_val =
            return_val || vehicle_number.toUpperCase().includes(u_input);
        }

        return return_val;
      });
    }

    return (
      <FlatList
        data={list}
        keyboardShouldPersistTaps={"handled"}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderBooking}
        refreshing={isFetching}
        onRefresh={this.props.fetchBookingsByStatus}
        showsVerticalScrollIndicator={false}
        extraData={this.state.selectedIndex}
      />
    );
  };

  renderBooking = ({ index, item: booking_id }) => {
    const {
      bookings,
      customers,
      vendors,
      vehicles,
      navigation,
      vehicleTypes,
      engagedBy,
      trips
    } = this.props;

    const booking = bookings[booking_id] || {};

    return (
      <View key={index} style={`${index}` === "0" ? { paddingTop: 10 } : {}}>
        <BookingCard
          bookings={bookings}
          booking={booking}
          customers={customers}
          vendors={vendors}
          vehicles={vehicles}
          vehicleTypes={vehicleTypes}
          trips={trips}
          engagedBy={engagedBy}
          navigation={navigation}
          index={index}
          selectIndex={this.selectIndex}
          selectedIndex={this.state.selectedIndex}
          screen={"new_request"}
          statusVisibility={false}
          {...this.props}
        />
      </View>
    );
  };

  render() {
    console.log("this.props.navigation ----", this.props.navigation);

    return (
      <Fragment>
        <Header name={"NEW REQUESTS"} navigation={this.props.navigation} />

        <SearchBar
          placeholder="Search by BookingId/ Route/ Vehicle/ Vehicle Type"
          onChangeText={this.setSearch}
          lightTheme
          containerStyle={customStyles.searchBarContainer}
          value={this.state.input}
          onCancel={this.clearInput}
          onClear={this.clearInput}
        />

        {this.renderBookingList()}
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.listbackgroundcolor
  },
  forwardStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    paddingTop: 3
  },
  returnStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginRight: 10,
    marginLeft: 0,
    marginTop: 3
  },
  boldTextStyle: {
    fontSize: 15,
    fontWeight: "bold",
    color: colors.blueColor,
    fontFamily: "CircularStd-Book"
  },
  normalTextStyle: {
    fontSize: 12,
    color: colors.textcolor,
    marginLeft: 5,
    paddingTop: 3,
    fontFamily: "CircularStd-Book"
  },
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    marginTop: 5,
    paddingBottom: 5
  },
  changeColorlineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 5,
    backgroundColor: colors.blue
  },
  rowStyle: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 5
  },
  rowStyle1: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10,
    marginTop: 3
  },
  textStyle1: {
    flex: 0.5,
    fontSize: 12,
    color: colors.textcolor,
    fontFamily: "CircularStd-Book"
  },
  textStyle2: {
    fontSize: 12,
    color: colors.textcolor,
    paddingTop: 3,
    fontFamily: "CircularStd-Book"
  },
  rowStyle2: {
    flex: 0.5,
    flexDirection: "row",
    marginLeft: 0
  },
  customerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    ...Platform.select({
      ios: {
        padding: 10,
        borderRadius: 10,
        marginTop: 10,
        overflow: "hidden"
      },
      android: {
        paddingTop: 5,
        paddingLeft: 5,
        paddingBottom: 5,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        marginTop: 8
      }
    })
  },
  row1Background: {
    flex: 1,
    flexDirection: "row"
  },
  row2Background: {
    flex: 1,
    paddingBottom: 10
  },
  rowBackgroundColor: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: colors.blue
  },
  columnBackgroundColor: {
    flex: 1,
    backgroundColor: colors.blue
  }
});

export default NewRequestBookings;
