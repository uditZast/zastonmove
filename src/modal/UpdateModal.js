import React from "react";
import {
  Image,
  Linking,
  Modal,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import logo from "../images/onmove_logo.png";

const UpdateModal = props => {
  const { forceUpdate, renderModal, url, closeModal } = props;
  return (
    <Modal animationType="slide" transparent={true} visible={renderModal}>
      <View style={styles.modalOuterStyle}>
        <View style={styles.modalInnerStyle}>
          <Image
            source={logo}
            style={{ flex: 1, height: 50, width: 100 }}
            resizeMode="contain"
          />
          <Text style={styles.versionHeaderStyle}>New Version Available</Text>
          <Text style={styles.updateTextStyle}>
            Looks like you have an old version of the App. Please update to
            enjoy the latest features
          </Text>

          <View style={styles.buttonLineStyle}>
            <TouchableOpacity
              onPress={() => {
                // Linking.openURL(url).catch((err) => console.error('An error occurred', err));
              }}
              activeOpacity={0.9}
            >
              <View style={styles.buttonContainer}>
                <Text style={styles.buttonText}>Update</Text>
              </View>
            </TouchableOpacity>
            {!forceUpdate && (
              <TouchableOpacity onPress={closeModal} activeOpacity={0.9}>
                <View style={styles.buttonContainer}>
                  <Text style={styles.buttonText}>Cancel</Text>
                </View>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = {
  modalInnerStyle: {
    backgroundColor: "#FFF",
    padding: 20,
    height: 250,
    alignItems: "center",
    alignSelf: "center",
    borderRadius: 10
  },
  modalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    padding: 50,
    backgroundColor: "rgba(0,0,0,0.7)"
  },
  versionHeaderStyle: {
    fontSize: 20,
    fontWeight: "500",
    color: "#2196f3"
  },
  updateTextStyle: {
    paddingTop: 10,
    textAlign: "center",
    color: "#000"
  },
  buttonContainer: {
    alignItems: "center",
    alignSelf: "center",
    borderRadius: 10,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "#1e88e5",
    margin: 10
  },
  buttonText: {
    fontSize: 14,
    color: "#FFF"
  },
  buttonLineStyle: {
    flexDirection: "row",
    paddingTop: 10
  }
};

export { UpdateModal };
