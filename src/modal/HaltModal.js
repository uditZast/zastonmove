import React, { Component } from "react";
import {
  Alert,
  Modal,
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../colors/Colors";
import Snackbar from "react-native-snackbar";
import axios from "axios";
import { HALT_TRIP } from "../constants/Constants";
import { ScrollView } from "react-navigation";
import DateTimePicker from "react-native-modal-datetime-picker";
import { Button, Icon } from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";
import SelectInput from "react-native-select-input-ios";

let comment,
  reason_code,
  testDate,
  testMonth,
  testYear,
  lbDate,
  lbMonth,
  lbYear,
  lbHours,
  lbMins,
  testHours,
  testMins,
  jsonData;

class HaltModal extends Component {
  state = {
    successMessage: "",
    reasonCode: "0",
    showdatePicker: false,
    dateTime: "",
    dateTimeArray: [],
    currentDate: "",
    beginTime: "",
    comment: "",
    lbDate: "",
    isLoading: false
  };

  componentDidMount() {
    console.log(
      new Date().getDate() +
        "-" +
        new Date().getMonth() +
        "-" +
        new Date().getFullYear()
    );
    testDate = new Date().getDate();
    testMonth = new Date().getMonth();
    testYear = new Date().getFullYear();
    testHours = new Date().getHours();
    testMins = new Date().getMinutes();
  }

  submitRejectionReason() {
    console.log("rejectionCode" + this.state.reasonCode);
    console.log("rejection commnet" + this.state.comment);
    console.log("datetime" + this.state.dateTime);

    if (
      this.state.reasonCode === null ||
      this.state.reasonCode === "" ||
      this.state.reasonCode === undefined
    ) {
      // ToastAndroid.show('Select a Valid Reason', ToastAndroid.SHORT)
      this.setState({ isLoading: false });
      return Snackbar.show({
        title: "Please Select Reason ",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (this.state.dateTime === "") {
      this.setState({ isLoading: false });
      return Snackbar.show({
        title: "Please Select Halt Date and Time ",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      axios
        .post(
          HALT_TRIP,
          {
            trip_code: this.props.tripId,
            time: this.state.dateTime,
            reason: this.state.reasonCode,
            comment: this.state.comment
          },
          {
            headers: {
              Authorization: "Token " + this.props.token,
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          console.log(response.data);
          console.log("reject message" + response.data.message);

          if (response.data.error === false) {
            console.log("error === false" + response.data.message[0]);
            this.setState(
              {
                successMessage: response.data.message[0],
                isLoading: false
              },
              () => this.showSuccessfullMessage()
            );
          } else {
            console.log("error === true");
            this.setState(
              {
                successMessage: response.data.message[0],
                isLoading: false
              },
              () => this.showSuccessfullMessage()
            );
          }
        })
        .catch(error => {
          console.log("catch " + error);
          if (error.response) {
            if (error.response.status === 401) {
              AsyncStorage.removeItem("token");
              this.props.navigation.navigate("Auth");
            }
          }
        });
    }
  }

  showSuccessfullMessage() {
    const { closeModal, refreshing } = this.props;
    console.log(this.state);
    console.log("close Modal Reject::: " + closeModal);
    console.log("success Message" + this.state.successMessage);
    Alert.alert("Message", "" + this.state.successMessage, [
      {
        text: "OK",
        onPress: () => {
          closeModal();
          this.clearState();
        }
      }
    ]);
  }

  clearState() {
    const { closeModal } = this.props;

    this.setState({ reasonCode: "" });
    reason_code = "";
    comment = "";
    closeModal();
  }

  getHaltReasons = () => {
    const { haltReason = {} } = this.props;
    jsonData = Object.keys(haltReason);

    let options = [
      {
        value: "0",
        label: "Select Halt Reason"
      }
    ];

    console.log("halt reasons ::::::::::::" + JSON.stringify(jsonData));
    let mappedArr = Object.keys(haltReason);
    if (mappedArr.length > 0) {
      for (const index of mappedArr) {
        const reason = haltReason[index] || {};
        const { code, value } = reason || {};

        if (code && value) {
          options.push({
            value: code,
            label: value
          });
        }
      }
    }

    return options;
  };

  render() {
    const { closeModal, display, lowerBound, token } = this.props;

    return (
      <Modal
        visible={display}
        animationType="slide"
        transparent={true}
        onRequestClose={() => this.clearState()}
        avoidKeyboard={true}
      >
        <View style={styles.modalOuterStyle}>
          <View style={styles.modalInnerStyle}>
            <TouchableOpacity
              style={styles.imageStyle}
              hitSlop={{ top: 30, bottom: 30, left: 70, right: 70 }}
              onPress={() => this.clearState()}
            >
              <Icon
                name="cancel"
                type="material"
                size={25}
                color={colors.blueColor}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.setState({ showdatePicker: true })}
            >
              <View style={styles.BorderStyle}>
                <Text style={styles.phcustomerStyle}>{"Halt Time"}</Text>
                <Text
                  style={
                    this.state.dateTime !== ""
                      ? [styles.customerStyleInput, { color: colors.dark_grey }]
                      : styles.customerStyleInput
                  }
                >
                  {this.state.dateTime !== null && this.state.dateTime !== ""
                    ? this.state.dateTime
                    : "Select Date and Time"}
                </Text>
              </View>
            </TouchableOpacity>

            <DateTimePicker
              isVisible={this.state.showdatePicker}
              mode={"datetime"}
              maximumDate={
                new Date(testYear, testMonth, testDate, testHours, testMins)
              }
              onConfirm={date => {
                console.log("A date has been picked: ", date);

                this.state.dateTimeArray.push(date);

                let d;

                if (this.state.dateTimeArray.length > 1) {
                  console.log("dateTimeArray > 1");
                  d = this.state.dateTimeArray[
                    this.state.dateTimeArray.length - 1
                  ];
                } else {
                  console.log("dateTimeArray < 1");
                  d = this.state.dateTimeArray[0];
                }

                console.log("0th index::::" + this.state.dateTimeArray[0]);
                console.log("d :::::" + d);
                let fromDate = new Date(d);

                console.log("split lowerbound " + lowerBound.split(" ")[0]);
                let lbDateSplit = lowerBound.split(" ")[0];
                let lbTimeSplit = lowerBound.split(" ")[1];
                console.log(lbDateSplit.split("-"));
                lbDate = lbDateSplit.split("-")[0];
                lbMonth = lbDateSplit.split("-")[1];
                lbYear = lbDateSplit.split("-")[2];

                lbHours = lbTimeSplit.split(":")[0];
                lbMins = lbTimeSplit.split(":")[1];
                console.log(lbDate);
                console.log(lbMonth);
                console.log(lbYear);
                console.log(lbHours);
                console.log(lbMins);

                let lowerBoundDate = new Date(
                  lbYear,
                  lbMonth - 1,
                  lbDate,
                  lbHours,
                  lbMins
                );
                console.log("lowerbound date object " + lowerBoundDate);

                const monthNames = [
                  "1",
                  "2",
                  "3",
                  "4",
                  "5",
                  "6",
                  "7",
                  "8",
                  "9",
                  "10",
                  "11",
                  "12"
                ];

                console.log(
                  "picked date fromDate : " +
                    fromDate.getDate() +
                    " " +
                    monthNames[fromDate.getMonth()]
                );
                console.log("fromDate : " + fromDate);
                console.log("lowerBoundDate : " + lowerBound);
                console.log("lowerBoundDate : " + lowerBoundDate);

                //test code
                if (fromDate > new Date()) {
                  Snackbar.show({
                    title: "Future time cannot be selected",
                    duration: Snackbar.LENGTH_LONG
                  });
                  this.setState({ showdatePicker: false });
                  console.log("selected time is > than current time");
                } else if (fromDate < lowerBoundDate) {
                  console.log("from date is less than lowerbound");
                  Snackbar.show({
                    title: "Selected Date cannot be before Last Update",
                    duration: Snackbar.LENGTH_LONG
                  });
                  this.setState({ showdatePicker: false });
                } else {
                  console.log("selected time is < than current time");
                  this.setState(
                    {
                      dateTime:
                        fromDate.getDate() +
                        "-" +
                        monthNames[fromDate.getMonth()] +
                        "-" +
                        fromDate.getFullYear() +
                        " " +
                        fromDate.getHours() +
                        ":" +
                        fromDate.getMinutes(),
                      showdatePicker: false
                    },
                    () =>
                      console.log(
                        "set state from Date : " + this.state.dateTime
                      )
                  );
                }
              }}
              onCancel={() => this.setState({ showdatePicker: false })}
            />

            <ScrollView
              scrollEnabled={false}
              showsVerticalScrollIndicator={false}
              keyboardShouldPersistTaps="always"
            >
              <View style={styles.reasonBorderStyle}>
                <Text style={styles.phcustomerStyle}>Halt Reason</Text>
                <SelectInput
                  labelStyle={[
                    {
                      padding: 0,
                      paddingTop: 4,
                      textAlign: "center",
                      margin: 0,
                      color: colors.dark_grey,
                      height: 30,
                      ...Platform.select({
                        ios: {
                          overflow: "hidden",
                          fontSize: 15
                        },
                        android: {
                          paddingBottom: 2,
                          fontSize: 11
                        }
                      })
                    },
                    this.state.reasonCode === "0"
                      ? colors.border_grey
                      : colors.dark_grey
                  ]}
                  options={this.getHaltReasons()}
                  mode={"dropdown"}
                  value={this.state.reasonCode}
                  onValueChange={text => this.setState({ reasonCode: text })}
                />

                {/*<Picker mode="dropdown"*/}
                {/*        style={{padding: 0, height: 33}}*/}
                {/*        itemStyle={{height: 33}}*/}
                {/*        selectedValue={this.state.reasonCode}*/}
                {/*        onValueChange={(itemValue, itemIndex) =>*/}
                {/*            this.setState({reasonCode: itemValue})}>*/}
                {/*    {jsonData.map((item) => {*/}
                {/*        const label = haltReason[item] || {};*/}
                {/*        const {code = "", value = ""} = label;*/}

                {/*        return (*/}
                {/*            <Picker.Item label={value} value={code} key={code}*/}
                {/*                         color={colors.textcolor}/>*/}
                {/*        );*/}
                {/*    })}*/}

                {/*</Picker>*/}
              </View>

              <View style={styles.BorderStyle}>
                <Text style={styles.phcustomerStyle}>Comment</Text>
                <TextInput
                  editable={true}
                  placeholder="Comment"
                  placeholderTextColor={colors.border_grey}
                  style={
                    comment !== null && comment !== undefined && comment !== ""
                      ? [styles.StyleInput, { color: colors.dark_grey }]
                      : styles.StyleInput
                  }
                  onChangeText={text => (comment = text)}
                />
              </View>

              <Button
                title={"Submit"}
                textStyle={{
                  textAlign: "center",
                  fontSize: 11,
                  color: colors.white
                }}
                buttonStyle={{ marginTop: 15 }}
                onPress={() =>
                  this.setState({ isLoading: true }, () =>
                    this.submitRejectionReason()
                  )
                }
                loading={this.state.isLoading}
              />
              {/*<View style={styles.buttonStyle}>*/}
              {/*    <TouchableOpacity onPress={() => this.submitRejectionReason()}>*/}
              {/*        <Text style={styles.submitTextStyle}>Halt</Text>*/}
              {/*    </TouchableOpacity>*/}

              {/*</View>*/}
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = {
  imageStyle: {
    width: 23,
    height: 22,
    marginTop: -15,
    marginLeft: -15
  },
  textStyle: {
    color: colors.white,
    padding: 5,
    fontFamily: "CircularStd-Book",
    justifyContent: "center"
  },
  reasonBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 15,
    paddingLeft: 5,
    paddingRight: 5
  },
  BorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    paddingLeft: 5,
    paddingRight: 5,
    height: 40
  },
  phcustomerStyle: {
    marginTop: -10,
    color: colors.dark_grey,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  buttonStyle: {
    color: colors.white,
    backgroundColor: colors.blueColor,
    fontSize: 12,
    marginTop: 20,
    marginBottom: 20,
    justifyContent: "flex-end",
    height: 35
  },

  customerStyleInput: {
    fontSize: 12,
    color: colors.border_grey,
    marginLeft: 5,
    padding: 5,
    height: 35,
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
    fontFamily: "CircularStd-Book"
  },
  modalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.7)"
  },
  modalInnerStyle: {
    backgroundColor: "#FFF",
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    padding: 10,
    width: "90%"
  },

  StyleInput: {
    fontSize: 13,
    color: colors.border_grey,
    paddingLeft: 5,
    marginLeft: 5,
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center",
    padding: 0,
    marginTop: 5
  },
  pickerItemStyle: {
    fontSize: 12,
    color: colors.blueColor
  },
  submitTextStyle: {
    textAlign: "center",
    padding: 10,
    color: colors.white
  }
};

// const mapStateToProps = state => ({
//     state: state,
//     error: state.error,
//     products: state.productsReducer.products,
//     pending: state.productsReducer.pending,
// });
//
//
// export default connect(
//     mapStateToProps,
//     null
// )(HaltModal);
//

export default HaltModal;
