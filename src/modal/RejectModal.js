import React, { Component } from "react";
import {
  Alert,
  Modal,
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../colors/Colors";
import Snackbar from "react-native-snackbar";
import axios from "axios";
import { REJECT_BOOKING } from "../constants/Constants";
import { ScrollView } from "react-navigation";
import { Button, Icon } from "react-native-elements";
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from "@react-native-community/async-storage";
import SelectInput from "react-native-select-input-ios";

let comment, reason, reason_code, jsonData;

class RejectModal extends Component {
  state = { successMessage: "", reasonCode: "0", isLoading: false };

  checkNetworkSettings() {
    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        this.submitRejectionReason();
      } else {
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });
  }

  submitRejectionReason() {
    console.log("clicked" + this.props.bookingId);
    console.log("clicked" + this.props.token);
    console.log("rejectionCode" + this.state.reasonCode);
    console.log("rejection commnet" + comment);

    if (
      this.state.reasonCode === null ||
      this.state.reasonCode === "0" ||
      this.state.reasonCode === undefined ||
      this.state.reasonCode === "NA "
    ) {
      this.setState({ isLoading: false });
      return Snackbar.show({
        title: "Please Select Reason ",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (comment === undefined || comment === "") {
      this.setState({ isLoading: false });
      return Snackbar.show({
        title: "Please add comment for Rejection",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      this.setState({ isLoading: false });

      axios
        .post(
          REJECT_BOOKING,
          {
            booking_code: this.props.bookingId,
            code: this.state.reasonCode,
            comment: comment
          },
          {
            headers: {
              Authorization: "Token " + this.props.token,
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          console.log(response.data);
          console.log("reject message" + response.data.message);

          if (response.data.error === false) {
            console.log("error === false" + response.data.message[0]);
            this.setState(
              {
                successMessage: response.data.message[0],
                isLoading: false
              },
              () => this.showSuccessfullMessage()
            );
          } else {
            console.log("error === true");
            this.setState(
              {
                successMessage: response.data.message[0],
                isLoading: false
              },
              () => this.showSuccessfullMessage()
            );
          }
        })
        .catch(error => {
          console.log("catch " + error);
          if (error.response) {
            if (error.response.status === 401) {
              AsyncStorage.removeItem("token");
              this.props.navigation.navigate("Auth");
            }
          }
        });
    }
  }

  showSuccessfullMessage() {
    const { closeModal, refreshing } = this.props;
    console.log(this.state);
    console.log("close Modal Reject::: " + closeModal);
    console.log("success Message" + this.state.successMessage);
    Alert.alert("Message", "" + this.state.successMessage, [
      {
        text: "OK",
        onPress: () => {
          closeModal();
          this.clearState();
          refreshing();
        }
      }
    ]);
  }

  clearState() {
    const { closeModal } = this.props;

    this.setState({ reasonCode: "" });
    reason_code = "";
    comment = "";
    closeModal();
  }

  getRejectionReasons = () => {
    const { rejectReasons = {} } = this.props;
    jsonData = Object.keys(rejectReasons);

    let options = [
      {
        value: "0",
        label: "Select Rejection Reason"
      }
    ];

    console.log("rejection reasons ::::::::::::" + JSON.stringify(jsonData));
    let mappedArr = Object.keys(rejectReasons);
    if (mappedArr.length > 0) {
      for (const index of mappedArr) {
        const reason = rejectReasons[index] || {};
        const { code, value } = reason || {};
        if (code && value) {
          options.push({
            value: code,
            label: value
          });
        }
      }
    }

    return options;
  };

  render() {
    const { closeModal, display, bookingId } = this.props;

    let jsonData;
    console.log("1111111111111111----------", this.props);
    const { rejectReasons = {} } = this.props;
    console.log("rejectReasons-----------------------------", rejectReasons);
    jsonData = Object.keys(rejectReasons);

    console.log("-------------6666666666666------------", jsonData);

    return (
      <Modal
        visible={display}
        animationType="slide"
        transparent={true}
        onRequestClose={() => this.clearState()}
        avoidKeyboard={true}
      >
        <View style={styles.modalOuterStyle}>
          <View style={styles.modalInnerStyle}>
            <TouchableOpacity
              style={styles.imageStyle}
              hitSlop={{ top: 30, bottom: 30, left: 70, right: 70 }}
              onPress={() => this.clearState()}
            >
              <Icon
                name="cancel"
                type="material"
                size={25}
                color={colors.blueColor}
              />
            </TouchableOpacity>

            <ScrollView
              scrollEnabled={false}
              showsVerticalScrollIndicator={false}
              keyboardShouldPersistTaps="always"
            >
              <View style={styles.reasonBorderStyle}>
                <Text style={styles.phcustomerStyle}>Reason</Text>

                <SelectInput
                  labelStyle={[
                    {
                      padding: 0,
                      paddingTop: 4,
                      textAlign: "center",
                      margin: 0,
                      color: colors.dark_grey,
                      height: 30,
                      ...Platform.select({
                        ios: {
                          overflow: "hidden",
                          fontSize: 15
                        },
                        android: {
                          paddingBottom: 2,
                          fontSize: 11
                        }
                      })
                    },
                    this.state.reasonCode === "0"
                      ? colors.border_grey
                      : colors.dark_grey
                  ]}
                  pickerItemStyle={{ fontSize: 15 }}
                  options={this.getRejectionReasons()}
                  mode={"dropdown"}
                  value={this.state.reasonCode}
                  onValueChange={text => this.setState({ reasonCode: text })}
                />
              </View>

              <View style={styles.BorderStyle}>
                <Text style={styles.phcustomerStyle}>Comment</Text>
                <TextInput
                  editable={true}
                  placeholder="Comment"
                  placeholderTextColor={colors.border_grey}
                  style={
                    comment !== null && comment !== ""
                      ? [styles.StyleInput, { color: colors.dark_grey }]
                      : styles.StyleInput
                  }
                  onChangeText={text => (comment = text)}
                />
              </View>

              <Button
                onPress={() =>
                  this.setState({ isLoading: true }, () =>
                    this.checkNetworkSettings()
                  )
                }
                title={"Reject"}
                titleStyle={{
                  fontSize: 15,
                  fontFamily: "CircularStd-Book"
                }}
                loading={this.state.isLoading}
                buttonStyle={{ marginTop: 15, marginBottom: 10 }}
              />
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = {
  imageStyle: {
    width: 23,
    height: 22,
    marginTop: -17,
    marginLeft: -17
  },
  textStyle: {
    color: colors.white,
    padding: 5,
    fontFamily: "CircularStd-Book",
    justifyContent: "center"
  },
  reasonBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    paddingLeft: 5,
    paddingRight: 5
  },
  BorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    paddingLeft: 5,
    paddingRight: 5,
    height: 40
  },
  phcustomerStyle: {
    marginTop: -10,
    color: colors.dark_grey,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  buttonStyle: {
    color: colors.white,
    backgroundColor: colors.blueColor,
    fontSize: 15,
    marginTop: 25,
    marginBottom: 15,
    padding: 10,
    fontFamily: "CircularStd-Book",
    textAlign: "center",
    justifyContent: "flex-end"
  },

  customerStyleInput: {
    fontSize: 12,
    color: colors.border_grey,
    marginLeft: 5,
    padding: 5,
    height: 35,
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
    fontFamily: "CircularStd-Book"
  },
  modalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.7)"
  },
  modalInnerStyle: {
    backgroundColor: "#FFF",
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    padding: 10,
    width: "90%"
  },

  StyleInput: {
    fontSize: 13,
    color: colors.border_grey,
    paddingLeft: 5,
    marginLeft: 5,
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center",
    padding: 0,
    marginTop: 5
  },
  pickerItemStyle: {
    fontSize: 12,
    color: colors.blueColor
  }
};

export default RejectModal;
