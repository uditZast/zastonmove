import React, {Component} from 'react';
import {Alert, Modal, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import DateTimePicker from "react-native-modal-datetime-picker";
import Snackbar from "react-native-snackbar";
import {ARRIVED_DESTINATION, ARRIVED_INTERMEDIATE, BEGIN_TRIP, CLOSE_TRIP} from "../constants/Constants";
import AsyncStorage from "@react-native-community/async-storage";
import axios from "axios";
import colors from "../colors/Colors";
import {Button, Icon} from "react-native-elements";


let lowerBoundDate, testDate, testMonth, testYear, lbDate, lbMonth, lbYear, lbHours, lbMins, testHours, testMins;
export default class TouchingModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showdatePicker: false,
            dateTime: '',
            dateTimeArray: [],
            value: '',
            submitMessage: '',
            beginTime: '',
            isLoading: false
        };
    }

    componentDidMount() {
        console.log(new Date().getDate() + '-' + new Date().getMonth() + '-' + new Date().getFullYear());
        testDate = new Date().getDate();
        testMonth = new Date().getMonth();
        testYear = new Date().getFullYear();
        testHours = new Date().getHours();
        testMins = new Date().getMinutes();

    }


    submitTouchingTime() {

        const {closeModal, bookingId, touchingFlag} = this.props;

        console.log("Booking Id" + this.props.bookingId);
        console.log("tripId DockModal" + this.props.tripId);
        console.log("Date and Time" + this.state.dateTime);


        if (this.state.dateTime === '') {
            // ToastAndroid.show('Please add Dock Date and Time', ToastAndroid.SHORT)
            this.setState({isLoading: false});
            return (
                Snackbar.show({
                    title: 'Please add Dock Date and Time',
                    duration: Snackbar.LENGTH_LONG,
                })
            );

        } else {
            let URL, parameters;
            if (this.props.heading === 'Touching Time') {
                if (touchingFlag === 'in') {
                    URL = ARRIVED_INTERMEDIATE;
                } else if (touchingFlag === 'out') {
                    URL = BEGIN_TRIP;
                } else if (touchingFlag === 'reached') {
                    URL = ARRIVED_DESTINATION;
                } else {

                }
                console.log('touching city ' + this.props.touchingCity);
                parameters = {
                    "trip_code": this.props.tripId,
                    "time": this.state.dateTime,
                    "city": this.props.touchingCity
                }
            } else if (this.props.heading === 'Close Time') {
                URL = CLOSE_TRIP;
                parameters = {
                    "trip_code": this.props.tripId,
                    "time": this.state.dateTime
                }
            } else {

            }

            AsyncStorage.getItem('token').then(
                (value) => {
                    this.setState({value: value});
                    console.log("URL -------------------" + URL);
                    axios.post(URL, parameters, {
                        headers: {
                            'Authorization': 'Token ' + value,
                            'Content-Type': 'application/json'
                        }
                    }).then((response) => {
                        console.log("dock message" + JSON.stringify(response.data));

                        if (response.data.error === false) {
                            this.setState({
                                submitMessage: response.data.message[0],
                                isLoading: false
                            }, () => this.showSuccessfullMessage())
                            // closeModal()

                        } else {
                            console.log('error === true');
                            this.setState({submitMessage: response.data.message[0], isLoading: false}, () => {
                                this.showSuccessfullMessage()
                            })
                        }

                    }).catch((error) => {
                        console.log("catch" + error);
                        if (error.response) {
                            if (error.response.status === 401) {
                                AsyncStorage.removeItem('token');
                                this.props.navigation.navigate('Auth')
                            } else if (error.response.status === 400) {
                                console.log('' + JSON.stringify(error.response))
                            } else {

                            }
                        }
                    });

                })

        }

    }

    showSuccessfullMessage() {

        Alert.alert(
            'Message',
            this.state.submitMessage,
            [
                {text: 'OK', onPress: () => this.clearState()},
            ]
        )

    }

    clearState() {
        const {closeModal, refreshing} = this.props;

        console.log('closeModal Dock modal' + closeModal);

        this.setState({
            showdatePicker: false,
            dateTime: '',
            dateTimeArray: [],
            currentDate: '',
            beginTime: ''
        }, () => {
            closeModal()
            refreshing()
        })

    }


    render() {

        const {displayData, lowerBound, closeModal} = this.props;

        let radio_props = [
            {label: displayData, value: displayData},
        ];

        return (
            <Modal
                animationType="slide"
                visible={this.props.display}
                transparent={true}
                onRequestClose={() => this.clearState()}>

                <View style={styles.modalOuterStyle}>
                    <View style={styles.modalInnerStyle}>
                        <TouchableOpacity
                            style={styles.imageStyle}
                            hitSlop={{top: 30, bottom: 30, left: 70, right: 70}}
                            onPress={() => this.clearState()}>
                            <Icon
                                name='cancel'
                                type='material'
                                size={25}
                                color={colors.blueColor}/>
                        </TouchableOpacity>

                        <View style={{flex: 1}}>

                            {/*<RadioForm*/}
                            {/*    style={styles.radioButtonStyle}*/}
                            {/*    radio_props={radio_props}*/}
                            {/*    initial={0}*/}
                            {/*    buttonSize={13}*/}
                            {/*    borderWidth={1}*/}
                            {/*    buttonOuterSize={15}*/}
                            {/*    onPress={(value) => this.setState({value: value}, () => console.log('value selected ' + this.state.value))}/>*/}

                            <Text style={styles.headingStyle}>{displayData}</Text>

                            <TouchableOpacity onPress={() => this.setState({showdatePicker: true})}>
                                <View style={styles.customerBorderStyle}>
                                    <Text
                                        style={styles.phcustomerStyle}>{this.props.heading}</Text>
                                    <Text
                                        style={styles.customerStyleInput}>{this.state.dateTime !== null && this.state.dateTime !== '' ? this.state.dateTime : 'Select Date and Time'}</Text>
                                </View>

                            </TouchableOpacity>

                            <DateTimePicker
                                isVisible={this.state.showdatePicker}
                                mode={'datetime'}
                                maximumDate={new Date(testYear, testMonth, testDate, testHours, testMins)}
                                onConfirm={(date) => {
                                    console.log("A date has been picked: ", date);

                                    this.state.dateTimeArray.push(date);

                                    let d;

                                    if (this.state.dateTimeArray.length > 1) {
                                        console.log('dateTimeArray > 1');
                                        d = this.state.dateTimeArray[this.state.dateTimeArray.length - 1];
                                    } else {
                                        console.log('dateTimeArray < 1');
                                        d = this.state.dateTimeArray[0];
                                    }

                                    console.log('0th index::::' + this.state.dateTimeArray[0]);
                                    console.log('d :::::' + d);
                                    let fromDate = new Date(d);


                                    console.log("split lowerbound " + lowerBound.split(" ")[0]);
                                    let lbDateSplit = lowerBound.split(" ")[0];
                                    let lbTimeSplit = lowerBound.split(" ")[1];
                                    console.log(lbDateSplit.split("-"));
                                    lbDate = lbDateSplit.split("-")[0];
                                    lbMonth = lbDateSplit.split("-")[1];
                                    lbYear = lbDateSplit.split("-")[2];

                                    lbHours = lbTimeSplit.split(":")[0];
                                    lbMins = lbTimeSplit.split(":")[1];
                                    console.log(lbDate);
                                    console.log(lbMonth);
                                    console.log(lbYear);
                                    console.log(lbHours);
                                    console.log(lbMins);


                                    let lowerBoundDate = new Date(lbYear, (lbMonth - 1), lbDate, lbHours, lbMins);
                                    console.log('lowerbound date object ' + lowerBoundDate);

                                    const monthNames = ["1", "2", "3", "4", "5", "6",
                                        "7", "8", "9", "10", "11", "12"
                                    ];

                                    console.log('picked date fromDate : ' + fromDate.getDate() + " " + monthNames[fromDate.getMonth()]);
                                    console.log('fromDate : ' + fromDate);
                                    console.log('lowerBoundDate : ' + lowerBound);
                                    console.log('lowerBoundDate : ' + lowerBoundDate);

                                    //test code
                                    if (fromDate > new Date()) {
                                        Snackbar.show({
                                            title: 'Future time cannot be selected',
                                            duration: Snackbar.LENGTH_LONG,
                                        });
                                        this.setState({showdatePicker: false});
                                        console.log('selected time is > than current time')
                                    } else if (fromDate < lowerBoundDate) {
                                        console.log('from date is less than lowerbound');
                                        Snackbar.show({
                                            title: 'Selected Date cannot be before Last Update',
                                            duration: Snackbar.LENGTH_LONG,
                                        });
                                        this.setState({showdatePicker: false})

                                    } else {
                                        console.log('selected time is < than current time');
                                        this.setState({
                                            dateTime: fromDate.getDate() + '-' + monthNames[fromDate.getMonth()] + '-' + fromDate.getFullYear() + " " + fromDate.getHours() + ":" + fromDate.getMinutes(),
                                            showdatePicker: false
                                        }, () => console.log("set state from Date : " + this.state.dateTime))

                                    }

                                }}
                                onCancel={() => this.setState({showdatePicker: false})}/>

                        </View>

                        {/*<TouchableOpacity onPress={this.submitTouchingTime.bind(this)}>*/}
                        {/*    <Text*/}
                        {/*        style={styles.buttonStyle}>{this.props.heading}</Text>*/}
                        {/*</TouchableOpacity>*/}


                        <Button title={'Submit'}
                                textStyle={{fontSize: 12, color: colors.white, textAlign: 'center'}}
                                buttonStyle={{marginTop: 15, marginLeft: 10, marginRight: 10}}/>
                    </View>

                </View>

            </Modal>

        );
    }
}


const styles = StyleSheet.create({
    container: {flex: 1},
    modalOuterStyle: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.7)'
    },
    modalInnerStyle: {
        backgroundColor: '#FFF',
        marginLeft: 15,
        marginRight: 15,
        borderRadius: 5,
        padding: 10,
        width: '90%',
        height: 180
    },
    imageStyle: {
        width: 23,
        height: 22,
        marginTop: -15,
        marginLeft: -15
    },
    customerBorderStyle: {
        borderWidth: 1,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        borderColor: colors.blueColor,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 20,
        paddingLeft: 5,
        paddingRight: 5,
        alignItems: 'flex-start'
    },
    phcustomerStyle: {
        marginTop: -10,
        color: colors.dark_grey,
        backgroundColor: colors.white,
        marginLeft: 5,
        paddingLeft: 5,
        paddingRight: 5,
        fontFamily: 'CircularStd-Book',
        alignSelf: 'flex-start'
    },
    customerStyleInput: {
        fontSize: 12,
        color: colors.border_grey,
        marginLeft: 5,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        height: 35,
        textAlignVertical: 'center',
        fontFamily: 'CircularStd-Book',
    },
    buttonStyle: {
        padding: 10,
        color: colors.white,
        backgroundColor: colors.blueColor,
        fontSize: 12,
        marginBottom: 10,
        textAlign: 'center',
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
    },
    radioButtonStyle: {
        marginTop: 20,
        marginLeft: 10
    },
    headingStyle: {
        fontSize: 15,
        fontWeight: 'bold',
        fontFamily: 'CircularStd-Book',
        color: colors.blueColor,
        marginTop: 10,
        marginLeft: 10
    }


});




