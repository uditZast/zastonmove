import React, { Component } from "react";
import {
  Image,
  Modal,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import success from "../images/success.png";
import colors from "../colors/Colors";
import { Icon } from "react-native-elements";

export default class VehicleVendorAssignModal extends Component {
  state = {
    vendorStr: ""
  };

  showTick() {
    if (this.props.tickVisible) {
      return (
        <TouchableOpacity
          style={{
            marginTop: -17,
            marginLeft: -17,
            justifyContent: "center",
            alignSelf: "center",
            marginRight: 5
          }}
          onPress={() => {
            this.props.SelectedValue(this.state.vendorStr, null);
          }}
        >
          <Image source={success} style={styles.imageStyle} />
        </TouchableOpacity>
      );
    } else {
      return null;
    }
  }

  render() {
    const {
      closeModal,
      display,
      SelectedValue,
      title,
      filterData
    } = this.props;
    let name, id, vehicleTypeId, apFlag, code, number, vehicle_type;

    console.log(
      "json data vehicle vendor modal " + JSON.stringify(this.props.filterData)
    );

    console.log(
      "filterData vehicle vendor modal " + JSON.stringify(filterData)
    );

    return (
      <Modal
        visible={display}
        animationType="slide"
        style={styles.modalContainer}
        transparent={true}
        onRequestClose={closeModal}
      >
        <View style={styles.containerStyle}>
          <View style={{ backgroundColor: colors.white, flex: 1 }}>
            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity
                style={styles.imageStyle}
                hitSlop={{ top: 30, bottom: 30, left: 70, right: 70 }}
                onPress={closeModal}
              >
                <Icon
                  name="cancel"
                  type="material"
                  size={25}
                  color={colors.blueColor}
                />
              </TouchableOpacity>

              <View style={styles.customerBorderStyle}>
                <Icon name="search" color={colors.border_grey} />
                <TextInput
                  style={styles.textInputstyle}
                  placeholder={"Search " + title}
                  onChangeText={text => {
                    this.setState({ vendorStr: text });
                    this.props.filterSearch(text);
                  }}
                />
              </View>

              {this.showTick()}
            </View>

            <ScrollView>
              {filterData.map(item => {
                console.log(
                  "ITEM:::::::::::::::::::::::::::::::::::::::::: " +
                    JSON.stringify(item)
                );

                if (title === "Vehicle") {
                  name = item.number;
                  id = item.id;
                  code = item.vehicle_type;
                  vehicleTypeId = item.vehicle_type_id;
                } else if (title === "Vehicle Type") {
                  name = item.name;
                  id = item.id;
                } else {
                  name = item.name;
                  id = item.id;
                  code = item.code;
                }

                return (
                  <View
                    style={{
                      padding: 2,
                      marginLeft: 5,
                      justifyContent: "center",
                      marginRight: 15
                    }}
                    key={item.id}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        console.log("selected code::" + item.code);
                        if (title === "Vehicle") {
                          name = item.number;
                          id = item.id;
                          code = item.vehicle_type;
                          SelectedValue(name, id, code, vehicleTypeId);
                          console.log("vehicle Id selected " + id);
                        } else {
                          if (name !== null && name !== "") {
                            name = item.name;
                            id = item.id;
                            code = item.code;
                            apFlag = item.ap;
                            SelectedValue(name, id, code, apFlag);
                          } else {
                            return Snackbar.show({
                              title: "Please Enter Vendor Name",
                              duration: Snackbar.LENGTH_LONG
                            });
                          }
                        }
                      }}
                    >
                      <View style={{ flexDirection: "row" }}>
                        {code && <Text style={styles.textStyle}>{code}</Text>}
                        {code && <Text>:</Text>}
                        <Text style={styles.textStyle1}>{name}</Text>
                      </View>
                    </TouchableOpacity>
                    <View style={styles.lineStyle} />
                  </View>
                );
              })}

              {/*<FlatList*/}
              {/*    data={this.props.filterData}*/}
              {/*    showsVerticalScrollIndicator={false}*/}
              {/*    renderItem={({item}) => {*/}
              {/*        // console.log('item ::::' + item)*/}
              {/*        if (title === 'Vehicle') {*/}
              {/*            name = item.number;*/}
              {/*            id = item.id;*/}
              {/*            code = item.vehicle_type;*/}
              {/*            vehicleTypeId = item.vehicle_type_id*/}
              {/*        } else if (title === 'Vehicle Type') {*/}
              {/*            name = item.name;*/}
              {/*            id = item.id*/}
              {/*        } else {*/}
              {/*            name = item.name;*/}
              {/*            id = item.id;*/}
              {/*            code = item.code*/}
              {/*        }*/}
              {/*        return (*/}
              {/*            <View*/}
              {/*                style={{*/}
              {/*                    padding: 2,*/}
              {/*                    marginLeft: 5,*/}
              {/*                    justifyContent: 'center',*/}
              {/*                    marginRight: 15*/}
              {/*                }}>*/}
              {/*                <TouchableOpacity onPress={() => {*/}
              {/*                    console.log('selected code::' + item.code);*/}
              {/*                    // customerSelectedValue(item.name, item.id, item.code)*/}
              {/*                    if (title === 'Vehicle') {*/}
              {/*                        name = item.number;*/}
              {/*                        id = item.id;*/}
              {/*                        code = item.vehicle_type;*/}
              {/*                        SelectedValue(name, id, code, vehicleTypeId);*/}
              {/*                        console.log('vehicle Id selected ' + id)*/}
              {/*                    } else {*/}
              {/*                        if (name !== null && name !== '') {*/}
              {/*                            name = item.name;*/}
              {/*                            id = item.id;*/}
              {/*                            code = item.code;*/}
              {/*                            apFlag = item.ap;*/}
              {/*                            SelectedValue(name, id, code, apFlag)*/}
              {/*                        } else {*/}
              {/*                            return (*/}
              {/*                                Snackbar.show({*/}
              {/*                                    title: 'Please Enter Vendor Name',*/}
              {/*                                    duration: Snackbar.LENGTH_LONG,*/}
              {/*                                })*/}
              {/*                            );*/}
              {/*                        }*/}
              {/*                    }*/}

              {/*                }}>*/}
              {/*                    <View style={{flexDirection: 'row'}}>*/}
              {/*                        {code &&*/}
              {/*                        <Text*/}
              {/*                            style={styles.textStyle}>{code}</Text>*/}
              {/*                        }*/}
              {/*                        {code &&*/}
              {/*                        <Text>:</Text>*/}
              {/*                        }*/}
              {/*                        <Text style={styles.textStyle1}>{name}</Text>*/}
              {/*                    </View>*/}

              {/*                </TouchableOpacity>*/}
              {/*                <View style={styles.lineStyle}/>*/}
              {/*            </View>*/}
              {/*        )*/}
              {/*    }*/}
              {/*    }*/}
              {/*    keyExtractor={item => item.id.toString()}*/}
              {/*/>*/}
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = {
  modalContainer: {
    marginLeft: 50,
    marginTop: 50,
    marginRight: 50,
    marginBottom: 50,
    backgroundColor: colors.blueColor
  },
  listStyle: {
    backgroundColor: colors.light_grey,
    flex: 1,
    marginTop: 10,
    marginBottom: 10
  },
  imageStyle: {
    width: 22,
    height: 22,
    marginTop: -8,
    marginLeft: -8
  },
  textStyle: {
    color: colors.blueColor,
    flex: 0.2,
    fontSize: 11,
    fontFamily: "CircularStd-Book",
    textAlign: "left",
    padding: 3,
    flexWrap: "wrap",
    marginLeft: 10
  },
  textStyle1: {
    color: colors.textcolor,
    flex: 0.9,
    fontSize: 11,
    marginLeft: 5,
    padding: 3,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    fontFamily: "CircularStd-Book"
  },
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    marginTop: 8,
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 5
  },
  customerBorderStyle: {
    flex: 1,
    borderWidth: 1,
    borderColor: colors.blueColor,
    flexDirection: "row",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 15,
    marginBottom: 8,
    ...Platform.select({
      ios: {
        padding: 10,
        borderRadius: 10,
        marginTop: 10,
        overflow: "hidden"
      },
      android: {
        paddingTop: 5,
        paddingLeft: 5,
        paddingBottom: 5,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        marginTop: 8
      }
    })
  },
  innerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor
  },
  containerStyle: {
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    backgroundColor: "rgba(0,0,0,0.7)",
    ...Platform.select({
      ios: {
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 60,
        paddingBottom: 40
      },
      android: {
        padding: 15
      }
    }),

    flex: 1
  },
  textInputstyle: {
    fontSize: 12,
    fontFamily: "CircularStd-Book",
    flex: 1,
    width: "100%",
    ...Platform.select({
      ios: {
        padding: 5
      },
      android: {
        padding: 0
      }
    })
  }
};
