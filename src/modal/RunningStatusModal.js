import React, { Component } from "react";
import {
  Alert,
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../colors/Colors";
import Snackbar from "react-native-snackbar";
import axios from "axios";
import {
  ADD_RUNNING_STATUS,
  BOOKING_TRIP_History
} from "../constants/Constants";
import { ScrollView } from "react-navigation";
import Card from "../components/Card";
import AsyncStorage from "@react-native-community/async-storage";
import RadioForm from "react-native-simple-radio-button";
import NetInfo from "@react-native-community/netinfo";
import { BarIndicator } from "react-native-indicators";
import SelectInput from "react-native-select-input-ios";

let comment,
  reason_code,
  tripId,
  dataSource = [],
  delayReasonsArr,
  token,
  rightsArray;

class RunningStatusModal extends Component {
  state = {
    successMessage: "",
    reasonCode: "0",
    delayedLayout: false,
    otherReason: "",
    internalReason: "",
    hours: 0,
    minutes: 0,
    status: "",
    duration: 0,
    dataSource: [],
    isLoading: true
  };

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;

    tripId = params.tripId;
    token = params.token;
    rightsArray = params.rightsArray;

    console.log("tripId::::" + params.tripId);

    return {
      // title: navigation.getParam('Title', params ? params.title : ''),
      title: navigation.getParam("Title", tripId),
      //Default Title of ActionBar
      headerStyle: {
        backgroundColor: navigation.getParam(
          "BackgroundColor",
          colors.blueColor
        )
      },
      headerTintColor: navigation.getParam("HeaderTintColor", "#fff"),
      //Text color of ActionBar
      headerTitleStyle: {
        fontWeight: "bold",
        fontSize: 13,
        fontFamily: "CircularStd-Book"
      }
    };
  };

  checkNetworkSettings() {
    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        this.submitRunningStatus();
      } else {
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });
  }

  submitRunningStatus() {
    console.log("Running Status Modal tripId" + tripId);
    console.log("Running Status Modal reasonCode" + this.state.reasonCode);
    console.log("Running Status Modal tripId" + this.state.status);
    console.log("Running Status Modal tripId" + this.state.otherReason);

    let hrs, mins, duration, hrsInt, minsInt, durationInt;
    hrs = this.state.hours;
    mins = this.state.minutes;

    hrsInt = parseInt(hrs);
    minsInt = parseInt(mins);
    duration = parseInt(hrsInt * 60 + minsInt);

    // duration = ((hrs * 60) + mins);
    console.log("Running Status Modal duration" + duration);

    this.setState({ duration: duration }, () => {
      if (this.state.status === "delayed") {
        if (
          this.state.reasonCode === null ||
          this.state.reasonCode === "" ||
          this.state.reasonCode === undefined
        ) {
          return Snackbar.show({
            title: "Please Select Reason ",
            duration: Snackbar.LENGTH_LONG
          });
        } else if (this.state.duration === null || this.state.duration === 0) {
          return Snackbar.show({
            title: "Please Enter Delayed Time",
            duration: Snackbar.LENGTH_LONG
          });
        } else {
          axios
            .post(
              ADD_RUNNING_STATUS,
              {
                trip_code: tripId,
                status: this.state.status,
                duration: this.state.duration,
                reason: this.state.reasonCode,
                internal_reason: this.state.internalReason
              },
              {
                headers: {
                  Authorization: "Token " + token,
                  "Content-Type": "application/json"
                }
              }
            )
            .then(response => {
              console.log(response.data);
              console.log("reject message" + response.data.message);

              if (response.data.error === false) {
                console.log("error === false" + response.data.message[0]);
                this.setState(
                  { successMessage: response.data.message[0] },
                  () => this.showSuccessfullMessage()
                );
              } else {
                console.log("error === true");
                this.setState(
                  { successMessage: response.data.message[0] },
                  () => this.showSuccessfullMessage()
                );
              }
            })
            .catch(error => {
              console.log("catch " + error);
              if (error.response) {
                if (error.response.status === 401) {
                  AsyncStorage.removeItem("token");
                  this.props.navigation.navigate("Auth");
                }
              }
            });
        }
      } else {
        axios
          .post(
            ADD_RUNNING_STATUS,
            {
              trip_code: tripId,
              status: this.state.status,
              duration: this.state.duration,
              reason: this.state.reasonCode,
              comment: this.state.otherReason
            },
            {
              headers: {
                Authorization: "Token " + token,
                "Content-Type": "application/json"
              }
            }
          )
          .then(response => {
            console.log(response.data);
            console.log("reject message" + response.data.message);

            if (response.data.error === false) {
              console.log("error === false" + response.data.message[0]);
              this.setState({ successMessage: response.data.message[0] }, () =>
                this.showSuccessfullMessage()
              );
            } else {
              console.log("error === true");
              this.setState({ successMessage: response.data.message[0] }, () =>
                this.showSuccessfullMessage()
              );
            }
          })
          .catch(error => {
            console.log("catch " + error);
            if (error.response) {
              if (error.response.status === 401) {
                this.props.navigation.navigate("Auth");
              }
            }
          });
      }
    });
  }

  componentDidMount() {
    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        this.getRunningStatus();
      } else {
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });

    // this.getRunningStatus()
  }

  getRunningStatus() {
    const { params } = this.props.navigation.state;
    const { tripId } = params;
    console.log("tripId received::::::::::" + tripId);

    AsyncStorage.getItem("token").then(value => {
      this.setState({ value: value });
      console.log("token:::::::" + value);
      axios
        .post(
          BOOKING_TRIP_History,
          {
            booking_code: null,
            trip_code: tripId,
            entity: "running_status"
          },
          {
            headers: {
              Authorization: "Token " + value,
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          this.setState(
            {
              dataSource: response.data.data,
              isLoading: false
            },
            () =>
              console.log(
                "dataSource setState:: " + this.state.dataSource.length
              )
          );
          console.log("rowData::::: " + response.data.data.length);
        })
        .catch(error => {
          console.log("catch" + error);
          if (error.response) {
            if (error.response.status === 401) {
              this.props.navigation.navigate("Auth");
            }
          }
        });
    });
  }

  showSuccessfullMessage() {
    const { closeModal, refreshing } = this.props;
    console.log(this.state);
    console.log("close Modal Reject::: " + closeModal);
    console.log("success Message" + this.state.successMessage);
    Alert.alert("Message", "" + this.state.successMessage, [
      {
        text: "OK",
        onPress: () => {
          this.clearState();
          this.handleBackPress();
        }
      }
    ]);
  }

  handleBackPress = () => {
    console.log("backPress");
    this.props.navigation.goBack(null);
    return true;
  };

  clearState() {
    // const {closeModal} = this.props;

    this.setState({ reasonCode: "" });
    reason_code = "";
    comment = "";
  }

  getDelayReasons() {
    const { delayReasons = {} } = this.props;
    console.log("delayReasons-----------------------------", delayReasonsArr);
    delayReasonsArr = Object.keys(delayReasons);

    let options = [
      {
        value: "0",
        label: "Select Delay Reason"
      }
    ];

    console.log(
      "rejection reasons ::::::::::::" + JSON.stringify(delayReasons)
    );
    let mappedArr = Object.keys(delayReasons);
    if (mappedArr.length > 0) {
      for (const index of mappedArr) {
        const reason = delayReasons[index] || {};
        const { value } = reason || {};
        if (value) {
          options.push({
            value: value,
            label: value
          });
        }
      }
    }

    return options;
  }

  renderDelayedLayout() {
    if (this.state.delayedLayout === true) {
      return (
        <View style={{ marginLeft: 10, marginRight: 10 }}>
          <View style={styles.BorderStyle}>
            <Text style={styles.phcustomerStyle}>Hours</Text>
            <TextInput
              editable={true}
              placeholder="Hours"
              keyboardType="numeric"
              placeholderTextColor={colors.border_grey}
              style={
                this.state.hours !== "" && this.state.hours !== "Hours"
                  ? [styles.StyleInput, { color: colors.textcolor }]
                  : styles.StyleInput
              }
              onChangeText={text => this.setState({ hours: text })}
            />
          </View>

          <View style={styles.BorderStyle}>
            <Text style={styles.phcustomerStyle}>Minutes</Text>
            <TextInput
              editable={true}
              placeholder="Minutes"
              keyboardType="numeric"
              placeholderTextColor={colors.border_grey}
              style={
                this.state.minutes !== "" && this.state.minutes !== "Minutes"
                  ? [styles.StyleInput, { color: colors.textcolor }]
                  : styles.StyleInput
              }
              onChangeText={text => this.setState({ minutes: text })}
            />
          </View>

          <View style={styles.BorderStyle}>
            <Text style={styles.phcustomerStyle}>Internal Reason</Text>
            <TextInput
              editable={true}
              placeholder="Internal Reason"
              placeholderTextColor={colors.border_grey}
              style={
                this.state.internalReason !== "" &&
                this.state.internalReason !== "Internal Reason"
                  ? [styles.StyleInput, { color: colors.textcolor }]
                  : styles.StyleInput
              }
              onChangeText={text => this.setState({ internalReason: text })}
            />
          </View>

          <View style={styles.reasonBorderStyle}>
            <Text style={styles.phcustomerStyle}>Reason</Text>

            <SelectInput
              labelStyle={[
                {
                  padding: 0,
                  paddingTop: 4,
                  textAlign: "center",
                  margin: 0,
                  color: colors.dark_grey,
                  height: 30,
                  ...Platform.select({
                    ios: {
                      overflow: "hidden",
                      fontSize: 15
                    },
                    android: {
                      paddingBottom: 2,
                      fontSize: 11
                    }
                  })
                },
                this.state.reasonCode === "0"
                  ? colors.border_grey
                  : colors.dark_grey
              ]}
              options={this.getDelayReasons()}
              mode={"dropdown"}
              value={this.state.reasonCode}
              onValueChange={text => this.setState({ reasonCode: text })}
            />

            {/*<Picker mode="dropdown"*/}
            {/*        style={{padding: 0, margin: 0, height: 33}}*/}
            {/*        itemStyle={{height: 33}}*/}
            {/*        selectedValue={this.state.reasonCode}*/}
            {/*        onValueChange={(itemValue, itemIndex) =>*/}
            {/*            this.setState({reasonCode: itemValue}, () => console.log('DelayedReason' + itemValue))}>*/}
            {/*    {delayReasons.map((item) => {*/}
            {/*        const label = delayReasons[item] || {};*/}
            {/*        const {code = "", value = ""} = label;*/}
            {/*        return (*/}
            {/*            <Picker.Item label={value} value={value} key={value}*/}
            {/*                         color={colors.textcolor}/>*/}
            {/*        );*/}
            {/*    })}*/}

            {/*</Picker>*/}
          </View>
        </View>
      );
    } else {
      return null;
    }
  }

  checkStatus(value) {
    return (
      <View style={{ flexDirection: "row" }}>
        <Text style={styles.placeholderStyle}>Status</Text>
        <Text style={styles.textStyle}>{value}</Text>
      </View>
    );
  }

  checkUser(value) {
    return (
      <View style={{ flexDirection: "row", flex: 1 }}>
        <Text style={styles.placeholderStyle}>User</Text>
        <Text style={styles.textStyle}>{value}</Text>
      </View>
    );
  }

  checkDelayDuration(value) {
    return (
      <View style={{ flexDirection: "row" }}>
        <Text style={styles.placeholderStyle}>Delay Duration</Text>
        <Text style={styles.textStyle}>{value}</Text>
      </View>
    );
  }

  checkReason(value) {
    return (
      <View style={{ flexDirection: "row" }}>
        <Text style={styles.placeholderStyle}>Reason</Text>
        <Text style={styles.textStyle}>{value}</Text>
      </View>
    );
  }

  checkTime(value) {
    return (
      <View style={{ flexDirection: "row" }}>
        <Text style={styles.placeholderStyle}>Time</Text>
        <Text style={styles.textStyle}>{value}</Text>
      </View>
    );
  }

  render() {
    let radio_props = [
      { label: "Ontime", value: "ontime" },
      { label: "Delayed", value: "delayed" }
    ];

    return (
      <View style={{ flex: 1, backgroundColor: "#ECF0F1" }}>
        <ScrollView
          style={{ marginTop: 10 }}
          scrollEnabled={true}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
        >
          {rightsArray.includes("add-running-status") ? (
            <Card>
              <Text style={styles.headingStyle}>Add Running Status</Text>

              <RadioForm
                style={styles.radioButtonStyle}
                radio_props={radio_props}
                initial={-1}
                buttonSize={13}
                borderWidth={1}
                buttonOuterSize={15}
                onPress={value =>
                  this.setState({ status: value }, () => {
                    this.state.status === "delayed"
                      ? this.setState({ delayedLayout: true })
                      : this.setState({ delayedLayout: false });
                  })
                }
              />

              {this.renderDelayedLayout()}

              <View style={styles.buttonStyle}>
                <TouchableOpacity onPress={() => this.checkNetworkSettings()}>
                  <Text style={styles.submitTextStyle}>Add Running Status</Text>
                </TouchableOpacity>
              </View>
            </Card>
          ) : null}

          {this.state.isLoading ? (
            <BarIndicator color={colors.blueColor} size={20} />
          ) : (
            <View style={{ backgroundColor: "#ECF0F1" }}>
              {this.state.dataSource.map(rowData => {
                return (
                  <Card key={rowData.id}>
                    {rowData.user !== null && rowData.user !== ""
                      ? this.checkUser(rowData.user)
                      : null}
                    {rowData.status !== null && rowData.status !== ""
                      ? this.checkStatus(rowData.status)
                      : null}
                    {rowData.duration !== null && rowData.duration !== 0
                      ? this.checkDelayDuration(rowData.duration)
                      : null}
                    {rowData.reason !== null && rowData.reason !== ""
                      ? this.checkReason(rowData.reason)
                      : null}
                    {rowData.time !== null && rowData.time !== ""
                      ? this.checkTime(rowData.time)
                      : null}
                  </Card>
                );
              })}
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  imageStyle: {
    width: 22,
    height: 22,
    marginTop: -15,
    marginLeft: -15
  },
  textStyle: {
    color: colors.dark_grey,
    fontFamily: "CircularStd-Book",
    padding: 5,
    flex: 0.8,
    justifyContent: "center"
  },
  reasonBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 15,
    paddingLeft: 5,
    paddingRight: 5
  },
  BorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    paddingLeft: 5,
    paddingRight: 5,
    height: 40
  },
  phcustomerStyle: {
    marginTop: -10,
    color: colors.dark_grey,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  buttonStyle: {
    color: colors.white,
    backgroundColor: colors.blueColor,
    fontSize: 12,
    marginTop: 20,
    marginBottom: 20,
    justifyContent: "flex-end",
    height: 35,
    marginLeft: 10,
    marginRight: 10
  },

  customerStyleInput: {
    fontSize: 12,
    color: colors.border_grey,
    marginLeft: 5,
    padding: 5,
    height: 35,
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
    fontFamily: "CircularStd-Book"
  },
  modalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.7)"
  },
  modalInnerStyle: {
    backgroundColor: "#FFF",
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    marginTop: 15,
    marginBottom: 15,
    padding: 10,
    width: "90%"
  },

  StyleInput: {
    fontSize: 13,
    color: colors.border_grey,
    paddingLeft: 5,
    marginLeft: 5,
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center",
    padding: 0,
    marginTop: 5,
    width: "100%"
  },
  pickerItemStyle: {
    fontSize: 12,
    color: colors.blueColor
  },
  headingStyle: {
    fontSize: 15,
    fontWeight: "bold",
    fontFamily: "CircularStd-Book",
    color: colors.blueColor,
    marginTop: 10,
    padding: 5
  },
  placeholderStyle: {
    color: colors.black,
    fontFamily: "CircularStd-Book",
    fontSize: 12,
    padding: 5,
    flex: 0.2,
    marginLeft: 10,
    justifyContent: "center"
  },
  radioButtonStyle: {
    marginTop: 10,
    padding: 5
  },
  submitTextStyle: {
    color: colors.white,
    textAlign: "center",
    padding: 10
  }
};

export default RunningStatusModal;
