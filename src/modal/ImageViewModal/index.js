import React, { Component, Fragment } from "react";
import { View } from "react-native";
import ImageViewer from "react-native-image-zoom-viewer";
import Modal from "react-native-modal";

// const images = [
//   {
//     source: {
//       // uri:
//       //   "https://cdn.pixabay.com/photo/2017/08/17/10/47/paris-2650808_960_720.jpg"
//     },
//     title: "Doodle Jump",
//     width: 806,
//     height: 720
//   }
// ];

class ImageViewModal extends Component {
  state = {
    imageVisible: this.props.imageViewVisible,
    height: 0,
    width: 0
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { closeModal, isVisible, url, image, screen } = this.props;

    const images = [
      {
        url: url
      }
    ];

    const imagesDirectory = [
      {
        url: image,
        props: {
          resizeMode: "contain"
        }
      }
    ];

    return (
      <Fragment>
        <Modal
          isVisible={this.props.imageViewVisible}
          transparent={true}
          onRequestClose={closeModal}
          onBackButtonPress={closeModal}
          onBackdropPress={closeModal}
          animationIn={"slideInUp"}
          style={{
            position: "absolute",
            margin: 0,
            top: 120,
            bottom: 0,
            left: 0,
            right: 0
          }}
        >
          <View style={styles.modalView}>
            <ImageViewer imageUrls={url ? images : imagesDirectory} />
          </View>
        </Modal>

        {/*<ImageView*/}
        {/*  images={images}*/}
        {/*  isVisible={this.state.imageVisible}*/}
        {/*  onClose={closeModal}*/}
        {/*  isTapZoomEnabled={true}*/}
        {/*  isPinchZoomEnabled={false}*/}
        {/*  isSwipeCloseEnabled={true}*/}
        {/*/>*/}
      </Fragment>
    );
  }
}

const styles = {
  container: {
    margin: 0,
    padding: 0,
    alignItems: "flex-start"
  },
  modalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0)"
  },
  modalInnerStyle: {
    backgroundColor: "#FFF",
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    padding: 10,
    width: "90%"
  },
  modalView: {
    width: "100%",
    flex: 1,
    borderRadius: 4,
    backgroundColor: "#ffffff",
    alignSelf: "center",
    paddingBottom: 30
  }
};

export default ImageViewModal;
