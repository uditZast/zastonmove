import React, { Component } from "react";
import { Platform, Text, TouchableOpacity, View } from "react-native";
import { Button, CheckBox, Icon } from "react-native-elements";
import Modal from "react-native-modal";
import NumberInput from "../../components/common/NumberInput";
import colors from "../../components/common/Colors";
import Snackbar from "react-native-snackbar";
import customStyles from "../../components/common/Styles";
import SelectInput from "react-native-select-input-ios";
import { ADVANCE_TYPES } from "../../../constants";

class AdvanceTripModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: null,
      showError: false,
      errorMessage: null,
      isLoading: false,
      checkValue: false,
      advanceTypeId: 0,
      advanceType: null,
      requestOtp: true,
      verifyOtp: false,
      otp: null,
      showOtpLayout: false,
      deployerSame: false,
      deployer: 0
    };
  }

  closeModal = () => {
    this.setState({ isVisible: false, amount: null });
  };

  updateAmount = amount => {
    this.setState({ amount });
  };

  otpValue = otp => {
    this.setState({ otp });
  };

  checkValue = () => {
    if (this.state.checkValue) {
      this.setState({ checkValue: false, deployer: 1 });
    } else {
      this.setState({ checkValue: true, deployer: 2 });
    }
  };

  submitAdvance = () => {
    this.setState({ isLoading: true });

    const { submitTripAdvance } = this.props;

    const { closeModal } = this.props;
    const { amount, advanceType, advanceTypeId } = this.state;

    const { requestTripAdvance } = this.props;

    const { trips } = requestTripAdvance;

    const { trip_advance } = requestTripAdvance;
    const { balance_remaining } = trip_advance;

    const { basicInfo: { vendor_id, code } = {} } =
      Object.values(trips)[0] || {};

    let requestData = {
      vendor_id: vendor_id,
      trip_code: code,
      amount: amount,
      balance_remaining: balance_remaining,
      advance_type: advanceType
    };

    if (advanceTypeId === 0) {
      this.setState({ isLoading: false });
      Snackbar.show({
        title: "Please select Advance type",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (amount === null) {
      this.setState({ isLoading: false });
      Snackbar.show({
        title: "Please enter amount",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      submitTripAdvance(requestData)
        .then(response => {
          const { message } = response;
          console.log("response -----------------------", response);
          if (response.status === true) {
            closeModal().then(
              Snackbar.show({
                title: "" + message,
                duration: Snackbar.LENGTH_LONG
              })
            );
          } else {
            this.setState({
              isLoading: false,
              showError: true,
              errorMessage: response.message
            });
          }
        })
        .catch(error => {
          this.setState({ isLoading: false });
          console.log("error===============" + error);
        })
        .finally(() => {
          this.setState({ isLoading: false });
        });
    }
  };

  getAdvanceType = () => {
    const options = [];
    ADVANCE_TYPES.forEach((type, index) => {
      options.push({
        label: type,
        value: index
      });
    });

    return options;
  };

  setAdvanceType = id => {
    let name = ADVANCE_TYPES[id];
    this.setState({ advanceTypeId: id, advanceType: name });
  };

  requestOTP = () => {
    // this.setState({requestOtp: false, verifyOtp: true});
    this.setState({ isLoading: true });
    const { requestTripAdvance } = this.props;
    const { trip_advance, trip_charges } = requestTripAdvance;

    const { trip_charges_id } = trip_charges;

    // const {trip_charges_id} = trip_advance;
    console.log("trip_charges --------------", trip_charges);

    const { sendOtpToVendor } = this.props;

    console.log("trip_charges_id --------------------------", trip_charges_id);

    let requestData = {
      trip_charges_id: trip_charges_id
    };

    sendOtpToVendor(requestData)
      .then(response => {
        const { status } = response;
        if (status) {
          this.setState({
            requestOtp: false,
            verifyOtp: true,
            showOtpLayout: true
          });
        } else {
        }
      })
      .catch(error => {
        console.log("error ----", error);
      })
      .finally(() => {
        this.setState({ isLoading: false });
      });
  };

  verifyOTP = () => {
    this.setState({ isLoading: true });
    const { verifyOtpAdvance } = this.props;
    const { requestTripAdvance } = this.props;
    const { trip_advance, trip_charges } = requestTripAdvance;
    const { trip_charges_id } = trip_charges;
    const { first_advance_request_vendor } = trip_advance;
    // const {trip_charges_id, first_advance_request_vendor} = trip_advance;

    const { otp, checkValue } = this.state;

    let requestData = {
      trip_charges_id,
      otp,
      deployer_same: checkValue
    };

    console.log("requestData ----------------------", requestData);
    if (!otp) {
      this.setState({ isLoading: false });
      Snackbar.show({
        title: "Please Enter Otp",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      verifyOtpAdvance(requestData)
        .then(response => {
          const { status, message } = response;
          if (status) {
            this.setState({ verifyOtp: false, showOtpLayout: false });
          } else {
            this.setState({ showError: true, errorMessage: message });
            console.log("---- status false Verify Otp----");
          }
        })
        .catch(error => {
          console.log("error ----", error);
        })
        .finally(() => {
          this.setState({ isLoading: false });
        });
    }
  };

  close = () => {
    const { closeModal } = this.props;
    this.setState({ showError: false, errorMessage: null });
    closeModal();
  };

  render() {
    const { isVisible, requestTripAdvance } = this.props;

    const { trip_advance } = requestTripAdvance;

    const {
      first_advance_request_vendor,
      otp_verified,
      balance_remaining,
      total_trip_charges,
      balance_message,
      trip_code_ref
    } = trip_advance;

    return (
      <Modal
        isVisible={isVisible}
        onBackButtonPress={this.close}
        onBackdropPress={this.close}
      >
        <View style={styles.modalOuterStyle}>
          <View style={styles.modalInnerStyle}>
            <View>
              <TouchableOpacity
                style={styles.imageStyle}
                hitSlop={{ top: 30, bottom: 30, left: 70, right: 70 }}
                onPress={this.close}
              >
                <Icon
                  name="cancel"
                  type="material"
                  size={25}
                  color={colors.blue}
                />
              </TouchableOpacity>

              <View>
                <Text
                  style={{
                    color: colors.blue,
                    fontSize: 14,
                    fontFamily: "CircularStd-Book",
                    padding: 5,
                    alignSelf: "center",
                    justifyContent: "center"
                  }}
                >
                  {"ADD NEW TRIP ADVANCE"} {" (Ref: "}
                  {trip_code_ref}
                  {") "}
                </Text>

                {!otp_verified && (
                  <View
                    style={{
                      justifyContent: "center",
                      marginTop: 10
                    }}
                  >
                    {this.state.showOtpLayout && (
                      <View>
                        <NumberInput
                          numericProp={true}
                          message={"Enter OTP"}
                          updateText={this.otpValue}
                          placeholder={"Enter OTP"}
                          otpLengthProp={true}
                        />
                        {first_advance_request_vendor && (
                          <View
                            style={{
                              flexDirection: "row",
                              marginTop: 10,
                              marginLeft: 10,
                              marginRight: 10
                            }}
                          >
                            <Text
                              style={{
                                fontSize: 13,
                                color: colors.black65,
                                justifyContent: "center",
                                alignSelf: "center"
                              }}
                            >
                              IS THE DEPLOYER AND OWNER SAME
                            </Text>

                            <CheckBox
                              containerStyle={{ padding: 0, margin: 0 }}
                              right
                              checked={this.state.checkValue}
                              iconType="material-community"
                              checkedIcon="checkbox-marked-outline"
                              uncheckedIcon="checkbox-blank-outline"
                              checkedColor={colors.darkblue}
                              uncheckColor={colors.blue}
                              onPress={this.checkValue}
                            />
                          </View>
                        )}
                      </View>
                    )}

                    <View>
                      {this.state.requestOtp && (
                        <Button
                          title={"Send OTP to Vendor"}
                          buttonStyle={{ margin: 10 }}
                          onPress={
                            this.state.isLoading ? null : this.requestOTP
                          }
                          loading={this.state.isLoading}
                        />
                      )}

                      {this.state.verifyOtp && (
                        <Button
                          title={"Verify OTP"}
                          buttonStyle={{ margin: 10 }}
                          onPress={this.state.isLoading ? null : this.verifyOTP}
                          loading={this.state.isLoading}
                        />
                      )}
                    </View>
                  </View>
                )}

                {otp_verified && (
                  <View>
                    <View
                      style={{
                        flexDirection: "row",
                        marginTop: 15,
                        marginLeft: 10,
                        marginRight: 10
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 14,
                          color: colors.black85,
                          flex: 0.3,
                          fontFamily: "CircularStd-Book"
                        }}
                      >
                        Total:{" "}
                      </Text>
                      <Text
                        style={{
                          fontSize: 13,
                          color: colors.black85,
                          flex: 0.7,
                          fontWeight: "bold",
                          fontFamily: "CircularStd-Book"
                        }}
                      >
                        {total_trip_charges}
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: "row",
                        marginTop: 10,
                        marginLeft: 10,
                        marginRight: 10
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 14,
                          color: colors.black85,
                          flex: 0.3,
                          fontFamily: "CircularStd-Book"
                        }}
                      >
                        Balance to Pay:{" "}
                      </Text>
                      <Text
                        style={{
                          fontSize: 13,
                          color: colors.black85,
                          flex: 0.7,
                          fontWeight: "bold",
                          fontFamily: "CircularStd-Book"
                        }}
                      >
                        {balance_remaining}
                      </Text>
                    </View>

                    {balance_message ? (
                      <View
                        style={{
                          flexDirection: "row",
                          marginTop: 10,
                          marginBottom: 10,
                          marginLeft: 10,
                          marginRight: 10
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 14,
                            color: colors.black85,
                            flex: 0.3,
                            fontFamily: "CircularStd-Book"
                          }}
                        >
                          Reason:{" "}
                        </Text>
                        <Text
                          style={{
                            fontSize: 13,
                            color: colors.black85,
                            flex: 0.7,
                            fontFamily: "CircularStd-Book"
                          }}
                        >
                          {balance_message}
                        </Text>
                      </View>
                    ) : null}

                    <NumberInput
                      numericProp={true}
                      message={"Enter Amount"}
                      updateText={this.updateAmount}
                      placeholder={"Enter Amount"}
                    />

                    <View style={{ marginTop: 10, marginBottom: 10 }}>
                      <View style={[customStyles.border]}>
                        <Text style={customStyles.labelText}>Type</Text>
                        <SelectInput
                          style={
                            Platform.OS === "ios"
                              ? customStyles.pickerView
                              : { height: 30 }
                          }
                          labelStyle={{
                            color:
                              this.state.advanceTypeId === 0
                                ? colors.black25
                                : colors.black,
                            ...Platform.select({
                              android: {
                                height: 30
                              },
                              ios: {
                                alignSelf: "flex-start"
                              }
                            })
                          }}
                          options={this.getAdvanceType()}
                          mode={"dropdown"}
                          value={this.state.advanceTypeId}
                          onSubmitEditing={this.setAdvanceType}
                        />
                      </View>
                    </View>

                    <Button
                      title={"Submit"}
                      buttonStyle={{ margin: 10 }}
                      onPress={this.submitAdvance}
                      loading={this.state.isLoading}
                    />
                  </View>
                )}

                {this.state.showError && (
                  <Text
                    style={{
                      fontSize: 12,
                      color: colors.redColor,
                      padding: 10,
                      justifyContent: "center",
                      alignSelf: "center"
                    }}
                  >
                    {this.state.errorMessage}
                  </Text>
                )}
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = {
  imageStyle: {
    width: 23,
    height: 22,
    marginTop: -18,
    marginLeft: -18
  },
  customerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    paddingLeft: 5,
    paddingRight: 5,
    alignItems: "flex-start"
  },
  phcustomerStyle: {
    marginTop: -10,
    color: colors.dark_grey,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  buttonStyle: {
    color: colors.white,
    backgroundColor: colors.blueColor,
    fontSize: 15,
    justifyContent: "flex-end",
    marginTop: 20,
    marginLeft: 2,
    marginRight: 2,
    marginBottom: 10,
    textAlign: "center",
    padding: 10
  },
  customerStyleInput: {
    fontSize: 12,
    color: colors.border_grey,
    padding: 5,
    marginLeft: 5,
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center"
  },
  selectedTextColor: {
    fontSize: 12,
    color: colors.dark_grey,
    marginLeft: 5,
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
    fontFamily: "CircularStd-Book",
    ...Platform.select({
      ios: {
        padding: 5,
        overflow: "hidden"
      },
      android: {
        padding: 10
      }
    })
  },
  modalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0)"
  },
  modalInnerStyle: {
    backgroundColor: "#FFF",
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    padding: 10,
    width: "90%"
  },
  BorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    flex: 1,
    paddingLeft: 5,
    paddingRight: 5
  },
  StyleInput: {
    fontSize: 12,
    color: colors.dark_grey,
    paddingLeft: 5,
    marginLeft: 5,
    width: "100%",
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center",
    ...Platform.select({
      ios: {
        padding: 5,
        overflow: "hidden"
      },
      android: {
        padding: 0
      }
    })
  },
  textStyle: {
    fontSize: 13,
    color: colors.black,
    textAlign: "center",
    marginBottom: 10,
    fontFamily: "CircularStd-Book"
  }
};

export default AdvanceTripModal;
