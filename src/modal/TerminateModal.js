import React, {Component} from 'react';
import RadioForm from 'react-native-simple-radio-button';
import {Alert, Modal, Text, TouchableOpacity, View} from "react-native";
import Snackbar from "react-native-snackbar";
import axios from "axios";
import {TERMINATE_TRIP} from "../constants/Constants";
import colors from "../colors/Colors";
import {Button, Icon} from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";


export default class TerminateModal extends Component {

    constructor(props) {
        super(props);
        this.state = {value: '', comment: '', successMessage: '', isLoading: false}

    }


    submitTerminateReason() {
        console.log("terminate reason id" + this.state.value);
        console.log("terminate trip id" + this.props.tripId);
        console.log("terminate token" + this.props.token);

        if (this.state.value === '') {
            // ToastAndroid.show('Select a Valid Reason', ToastAndroid.SHORT)
            this.setState({isLoading: false});
            return (
                Snackbar.show({
                    title: 'Please Select Trip terminate reason ',
                    duration: Snackbar.LENGTH_LONG,
                })
            );
        } else {
            axios.post(TERMINATE_TRIP, {
                "trip_code": this.props.tripId,
                "reason": this.state.value,
                "comment": this.state.comment,
            }, {
                headers: {
                    'Authorization': 'Token ' + this.props.token,
                    'Content-Type': 'application/json'
                }
            }).then((response) => {
                console.log(response.data);
                console.log("reject message" + response.data.message);

                if (response.data.error === false) {
                    console.log('error === false' + response.data.message[0]);
                    this.setState({
                        successMessage: response.data.message[0],
                        isLoading: false
                    }, () => this.showSuccessfullMessage())

                } else {
                    console.log('error === true');
                    this.setState({
                        successMessage: response.data.message[0],
                        isLoading: false
                    }, () => this.showSuccessfullMessage())

                }
            }).catch((error) => {
                console.log("catch " + error);
                if (error.response) {
                    if (error.response.status === 401) {
                        AsyncStorage.removeItem('token');
                        this.props.navigation.navigate('Auth')
                    }
                }
            });
        }

    }

    showSuccessfullMessage() {
        const {closeModal, back} = this.props;
        console.log(this.state);
        console.log('close Modal Reject::: ' + closeModal);
        console.log('success Message' + this.state.successMessage);
        Alert.alert(
            'Message',
            '' + this.state.successMessage,
            [
                {
                    text: 'OK', onPress: () => {
                        this.clearState()
                        closeModal()
                        back()
                    }
                },
            ]
        );

    }

    clearState() {
        const {closeModal} = this.props;

        this.setState({reasonCode: ''})
        this.setState({value: '', comment: ''});
        closeModal()
    }


    render() {

        let radio_props = [
            {label: 'Vehicle Breakdown', value: 'Vehicle Breakdown'},
            {label: 'Customer Terminated the Trip', value: 'Customer Terminated the Trip'},
            {label: 'Vehicle Unavailable', value: 'Vehicle Unavailable'}
        ];
        const {closeModal, display, tripId, token} = this.props;

        console.log('trip id received ::::::::::' + this.props.tripId)

        return (
            <Modal
                visible={display}
                animationType="slide"
                transparent={true}
                onRequestClose={() => this.clearState()}
                avoidKeyboard={true}>
                <View style={styles.modalOuterStyle}>
                    <View style={styles.modalInnerStyle}>

                        <TouchableOpacity
                            style={styles.imageStyle}
                            hitSlop={{top: 30, bottom: 30, left: 70, right: 70}}
                            onPress={() => this.clearState()}>
                            <Icon
                                name='cancel'
                                type='material'
                                size={25}
                                color={colors.blueColor}/>
                        </TouchableOpacity>

                        <Text style={styles.headingStyle}>Reason for Terminating the Trip</Text>
                        <RadioForm
                            style={styles.radioButtonStyle}
                            radio_props={radio_props}
                            initial={-1}
                            buttonSize={13}
                            borderWidth={1}
                            buttonOuterSize={15}
                            onPress={(value) => this.setState({value: value}, () => console.log('value selected ' + this.state.value))}/>

                        {/*<TouchableOpacity onPress={() => this.submitTerminateReason()}>*/}
                        {/*    <Text style={styles.submitStyle}>Submit</Text>*/}
                        {/*</TouchableOpacity>*/}

                        <Button buttonStyle={{marginTop: 10, marginBottom: 10}}
                                textStyle={{fontFamily: 'CircularStd-Book', fontSize: 14}}
                                title={'Submit'}
                                loading={this.state.isLoading}
                                onPress={() => this.setState({isLoading: true}, () => this.submitTerminateReason())}
                        />
                    </View>
                </View>
            </Modal>
        );
    }
}


const styles = {
    modalOuterStyle: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.7)'
    },
    modalInnerStyle: {
        backgroundColor: '#FFF',
        height: 210,
        marginLeft: 15,
        marginRight: 15,
        borderRadius: 5,
        padding: 10,
        width: '90%'
    },
    radioButtonStyle: {
        marginTop: 20,
    },
    imageStyle: {
        width: 23,
        height: 22,
        marginTop: -15,
        marginLeft: -15
    },
    headingStyle: {
        fontSize: 15,
        fontWeight: 'bold',
        fontFamily: 'CircularStd-Book',
        color: colors.blueColor,
        marginTop: 10
    },
    submitStyle: {
        fontFamily: 'CircularStd-Book',
        fontSize: 15,
        color: colors.white,
        backgroundColor: colors.blueColor,
        alignItems: 'flex-end',
        textAlign: 'center',
        padding: 10,
        marginTop: 15
    }

}