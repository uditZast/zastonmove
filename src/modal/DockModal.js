import React, {Component} from 'react';
import {Alert, Modal, Platform, Text, TouchableOpacity, View} from 'react-native';
import AsyncStorage from "@react-native-community/async-storage";
import axios from "axios";
import {ARRIVED_DESTINATION, ARRIVED_INTERMEDIATE, BEGIN_TRIP, CLOSE_TRIP, DOCK_BOOKING} from "../constants/Constants";
import Snackbar from "react-native-snackbar";
import colors from "../colors/Colors";
import DateTimePicker from "react-native-modal-datetime-picker";
import {Button, Icon} from "react-native-elements";
import NetInfo from "@react-native-community/netinfo";

let lowerBoundDate, maxDate, testDate, testMonth, testYear, lbDate, lbMonth, lbYear, lbHours, lbMins, testHours,
    testMins;

export default class DockModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showdatePicker: false,
            dateTime: '',
            dateTimeArray: [],
            value: '',
            submitMessage: '',
            currentDate: '',
            beginTime: '',
            isLoading: false
        };
    }

    componentDidMount() {
        console.log(new Date().getDate() + '-' + new Date().getMonth() + '-' + new Date().getFullYear())
        this.setState({currentDate: new Date()}, () => maxDate = new Date(this.state.currentDate))

        testDate = new Date().getDate();
        testMonth = new Date().getMonth();
        testYear = new Date().getFullYear();
        testHours = new Date().getHours();
        testMins = new Date().getMinutes();

        console.log('Maximum date :::::' + new Date(testYear, testMonth, testDate, testHours, testMins))
    }

    checkNetworkSettings() {
        NetInfo.fetch().then(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);

            if (state.isConnected) {
                this.submitDockTime();
            } else {
                this.setState({isLoading: false});
                return (
                    Snackbar.show({
                        title: 'Please check Internet Connection',
                        duration: Snackbar.LENGTH_LONG,
                    })
                );

            }

        });

    }


    submitDockTime() {

        const {closeModal, bookingId, touchingFlag} = this.props;

        console.log("Booking Id" + this.props.bookingId);
        console.log("tripId DockModal" + this.props.tripId);
        console.log("Date and Time" + this.state.dateTime);


        if (this.state.dateTime === '') {
            this.setState({isLoading: false});
            return (
                Snackbar.show({
                    title: 'Please select Date and Time',
                    duration: Snackbar.LENGTH_LONG,
                })
            );

        } else {
            let URL, parameters;

            if (this.props.bookingId !== null) {
                console.log('booking Id not null');
                URL = DOCK_BOOKING;
                parameters = {
                    "booking_code": this.props.booking_code,
                    "date_time": this.state.dateTime
                };

            } else {
                console.log('trip Id received' + this.props.tripId);
                console.log('heading' + this.props.heading);
                if (this.props.heading === 'Begin Trip') {
                    console.log('begin tinme condition');
                    URL = BEGIN_TRIP;
                    parameters = {
                        "trip_code": this.props.tripId,
                        "time": this.state.dateTime
                    }

                } else if (this.props.heading === 'Touching Time') {
                    console.log('touching time condition');
                    if (touchingFlag === 'in') {
                        URL = ARRIVED_INTERMEDIATE;
                    } else if (touchingFlag === 'out') {
                        URL = BEGIN_TRIP;
                    } else if (touchingFlag === 'reached') {
                        URL = ARRIVED_DESTINATION;
                    } else {
                        console.log('else')
                    }
                    console.log('touching city ' + this.props.touchingCity);
                    parameters = {
                        "trip_code": this.props.tripId,
                        "time": this.state.dateTime,
                        "city": this.props.touchingCity
                    }
                } else if (this.props.heading === 'Close Time') {
                    console.log('close time ');
                    URL = CLOSE_TRIP;
                    parameters = {
                        "trip_code": this.props.tripId,
                        "time": this.state.dateTime
                    }
                } else {
                    console.log('else')
                }

            }


            AsyncStorage.getItem('token').then(
                (value) => {
                    this.setState({value: value});
                    console.log("URL -------------------" + URL);
                    axios.post(URL, parameters, {
                        headers: {
                            'Authorization': 'Token ' + value,
                            'Content-Type': 'application/json'
                        }
                    }).then((response) => {
                        console.log("dock message" + JSON.stringify(response.data));

                        if (response.data.error === false) {
                            this.setState({
                                submitMessage: response.data.message[0],
                                isLoading: false
                            }, () => this.showSuccessfullMessage())
                            // closeModal()

                        } else {
                            console.log('error === true');
                            this.setState({submitMessage: response.data.message[0], isLoading: false}, () => {
                                this.showSuccessfullMessage()
                            })
                        }

                    }).catch((error) => {
                        console.log("catch" + error);
                        if (error.response) {
                            if (error.response.status === 401) {
                                AsyncStorage.removeItem('token');
                                this.props.navigation.navigate('Auth')
                            } else if (error.response.status === 400) {
                                console.log('' + JSON.stringify(error.response))
                            } else {

                            }
                        }
                    });

                })

        }

    }

    showSuccessfullMessage() {
        const {closeModal} = this.props;

        Alert.alert(
            'Message',
            this.state.submitMessage,
            [
                {text: 'OK', onPress: () => this.clearState()},
            ]
        )

    }

    clearState() {
        const {closeModal, heading, refreshing} = this.props;

        console.log('closeModal Dock modal' + closeModal);

        this.setState({
            showdatePicker: false,
            dateTime: '',
            dateTimeArray: [],
            value: '',
            submitMessage: ''
        }, () => {
            closeModal()
            {
                this.props.refreshing !== undefined && this.props.refreshing !== null ? refreshing() : null
            }
        })

    }

    render() {

        const {lowerBound, closeModal, heading} = this.props;

        console.log('lowerbound date in Date Modal' + lowerBound);

        console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];

        return (
            <Modal
                animationType="slide"
                visible={this.props.display}
                transparent={true}
                onRequestClose={() => {
                    this.setState({
                        showdatePicker: false,
                        dateTime: '',
                        dateTimeArray: [],
                        value: '',
                        submitMessage: ''
                    }, () => closeModal())
                }}>

                <View style={styles.modalOuterStyle}>
                    <View style={styles.modalInnerStyle}>

                        <TouchableOpacity
                            style={styles.imageStyle}
                            hitSlop={{top: 30, bottom: 30, left: 70, right: 70}}
                            onPress={() => {
                                this.setState({
                                    showdatePicker: false,
                                    dateTime: '',
                                    dateTimeArray: [],
                                    value: '',
                                    submitMessage: ''
                                }, () => closeModal())
                            }}>
                            <Icon
                                name='cancel'
                                type='material'
                                size={25}
                                color={colors.blueColor}/>
                        </TouchableOpacity>

                        <View style={{flex: 1}}>

                            <TouchableOpacity onPress={() => this.setState({showdatePicker: true})}>
                                <View style={styles.customerBorderStyle}>
                                    {/*<Text*/}
                                    {/*    style={styles.phcustomerStyle}>{this.props.bookingId !== null ? 'Select Dock Date and time' : 'Select Begin Time'}</Text>*/}
                                    <Text
                                        style={styles.phcustomerStyle}>{this.props.heading}</Text>
                                    <Text
                                        style={this.state.dateTime !== null && this.state.dateTime !== '' ? [styles.customerStyleInput, {color: colors.dark_grey}] : styles.customerStyleInput}>{this.state.dateTime !== null && this.state.dateTime !== '' ? this.state.dateTime : 'Select Date and Time'}</Text>
                                </View>

                            </TouchableOpacity>


                            <DateTimePicker
                                isVisible={this.state.showdatePicker}
                                mode={'datetime'}
                                maximumDate={new Date(testYear, testMonth, testDate, testHours, testMins)}
                                onConfirm={(date) => {
                                    console.log("A date has been picked: ", date);

                                    this.state.dateTimeArray.push(date);

                                    let d;

                                    if (this.state.dateTimeArray.length > 1) {
                                        console.log('dateTimeArray > 1');
                                        d = this.state.dateTimeArray[this.state.dateTimeArray.length - 1];
                                    } else {
                                        console.log('dateTimeArray < 1');
                                        d = this.state.dateTimeArray[0];
                                    }

                                    console.log('0th index::::' + this.state.dateTimeArray[0]);
                                    console.log('d :::::' + d);
                                    let fromDate = new Date(d);

                                    if (lowerBound !== null && lowerBound !== undefined) {
                                        console.log("split lowerbound " + lowerBound.split(" ")[0]);
                                        let lbDateSplit = lowerBound.split(" ")[0];
                                        let lbTimeSplit = lowerBound.split(" ")[1];
                                        console.log(lbDateSplit.split("-"));
                                        lbDate = lbDateSplit.split("-")[0];
                                        lbMonth = lbDateSplit.split("-")[1];
                                        lbYear = lbDateSplit.split("-")[2];

                                        lbHours = lbTimeSplit.split(":")[0];
                                        lbMins = lbTimeSplit.split(":")[1];
                                        console.log(lbDate);
                                        console.log(lbMonth);
                                        console.log(lbYear);
                                        console.log(lbHours);
                                        console.log(lbMins);


                                        let lowerBoundDate = new Date(lbYear, lbMonth, lbDate, lbHours, lbMins);
                                        console.log('lowerbound date object ' + lowerBoundDate);

                                    } else {

                                    }

                                    const monthNames = ["1", "2", "3", "4", "5", "6",
                                        "7", "8", "9", "10", "11", "12"
                                    ];

                                    console.log('picked date fromDate : ' + fromDate.getDate() + " " + monthNames[fromDate.getMonth()]);
                                    console.log('fromDate : ' + fromDate);
                                    console.log('lowerBoundDate : ' + lowerBound);
                                    console.log('lowerBoundDate : ' + lowerBoundDate);

                                    //test code
                                    if (fromDate > new Date()) {
                                        Snackbar.show({
                                            title: 'Future time cannot be selected',
                                            duration: Snackbar.LENGTH_LONG,
                                        });
                                        this.setState({showdatePicker: false});
                                        console.log('selected time is > than current time')
                                    } else if (fromDate < lowerBoundDate) {
                                        console.log('from date is less than lowerbound');
                                        Snackbar.show({
                                            title: 'Selected Date cannot be before Last Update',
                                            duration: Snackbar.LENGTH_LONG,
                                        });
                                        this.setState({showdatePicker: false})

                                    } else {
                                        console.log('selected time is < than current time');
                                        this.setState({
                                            dateTime: fromDate.getDate() + '-' + monthNames[fromDate.getMonth()] + '-' + fromDate.getFullYear() + " " + fromDate.getHours() + ":" + fromDate.getMinutes(),
                                            showdatePicker: false
                                        }, () => console.log("set state from Date : " + this.state.dateTime))

                                    }

                                }}
                                onCancel={() => this.setState({showdatePicker: false})}/>


                        </View>

                        <Button onPress={() => {
                            this.setState({isLoading: true});
                            this.checkNetworkSettings()
                        }}
                                title={heading.split(' ')[0]}
                                loading={this.state.isLoading}
                                buttonStyle={{
                                    margin: 10,
                                }}
                                titleStyle={{
                                    fontSize: 15,
                                    fontFamily: 'CircularStd-Book',
                                }}/>

                        {/*<TouchableOpacity onPress={() => {*/}
                        {/*    this.setState({isLoading: true});*/}
                        {/*    this.submitDockTime()*/}
                        {/*}}>*/}
                        {/*    <Text*/}
                        {/*        style={styles.buttonStyle}>{heading.split(' ')[0]}</Text>*/}
                        {/*</TouchableOpacity>*/}

                        {/*{this.state.isLoading ? <ActivityIndicator/> : null}*/}

                    </View>

                </View>

            </Modal>

        )

    }


}

const styles = {
    imageStyle: {
        width: 23,
        height: 22,
        ...Platform.select({
            ios: {
                marginTop: -10,
                marginLeft: -10,
            },
            android: {
                marginTop: -7,
                marginLeft: -7,
            }
        })

    },
    textStyle: {
        color: colors.textcolor,
        padding: 5
    },
    headingTextStyle: {
        fontSize: 15,
        color: colors.blueColor,
        marginTop: 10,
        padding: 5,
        fontFamily: 'CircularStd-Book',
    },
    modalOuterStyle: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.7)'
    },
    modalInnerStyle: {
        backgroundColor: '#FFF',
        marginLeft: 15,
        marginRight: 15,
        borderRadius: 5,
        padding: 10,
        width: '90%',
        ...Platform.select({
            ios: {
                padding: 5,
                height: 145,
            },
            android: {
                padding: 0,
                height: 140,
            }
        })

    },
    customerBorderStyle: {
        borderWidth: 1,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        borderColor: colors.blueColor,
        marginLeft: 10,
        marginRight: 10,
        paddingLeft: 5,
        paddingRight: 5,
        alignItems: 'flex-start',
        ...Platform.select({
            ios: {
                marginTop: 20,
            },
            android: {
                marginTop: 20,
            }
        })

    },
    phcustomerStyle: {
        marginTop: -10,
        color: colors.dark_grey,
        backgroundColor: colors.white,
        marginLeft: 5,
        paddingLeft: 5,
        paddingRight: 5,
        fontFamily: 'CircularStd-Book',
        alignSelf: 'flex-start',
    },
    customerStyleInput: {
        fontSize: 12,
        color: colors.border_grey,
        marginLeft: 5,
        justifyContent: 'center',
        alignItems: 'center',
        textAlignVertical: 'center',
        fontFamily: 'CircularStd-Book',
        padding: 5
    },
    BorderStyle: {
        borderWidth: 1,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        borderColor: colors.blueColor,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 20,
        flex: 1,
        paddingLeft: 5,
        paddingRight: 5,
    },
    buttonTextStyle: {
        color: colors.white,
        textAlign: 'center',
        padding: 10,
        fontSize: 15,
        fontFamily: 'CircularStd-Book',
        backgroundColor: colors.blueColor,
    },
    buttonStyle: {
        ...Platform.select({
            ios: {
                marginTop: 10,
                marginBottom: 10,
                marginLeft: 10,
                marginRight: 10,
            },
            android: {
                marginTop: 10,
                marginBottom: 20,
                marginLeft: 10,
                marginRight: 10,
            }
        })

    }

};
