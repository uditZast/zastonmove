import React, {Component} from 'react';
import {Alert, Modal, Platform, Text, TextInput, TouchableOpacity, View} from 'react-native';
import colors from "../colors/Colors";
import Snackbar from "react-native-snackbar";
import axios from "axios";
import {ScrollView} from "react-navigation";
import {EDIT_ROUTE, GET_DISTANCE} from "../constants/Constants";
import {Button, Icon} from "react-native-elements";
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from "@react-native-community/async-storage";

export default class EditRouteModal extends Component {

    state = {
        successMessage: '',
        newRoute: this.props.route,
        newTat: '',
        newDistance: '',
        warningText: '',
        editTatInput: false,
        editDistanceInput: false,
        isLoadingCheck: false,
        isLoading: false
    };

    checkNetworkSettings() {
        NetInfo.fetch().then(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);

            if (state.isConnected) {
                this.submitEditRoute();
            } else {
                return (
                    Snackbar.show({
                        title: 'Please check Internet Connection',
                        duration: Snackbar.LENGTH_LONG,
                    })
                );

            }

        });

    }


    submitEditRoute() {

        console.log('new route : ' + this.state.newRoute);

        if (this.state.newRoute === null || this.state.newRoute === '' || this.state.newRoute === undefined) {
            this.setState({isLoading: false});
            return (
                Snackbar.show({
                    title: 'Please Enter new Route',
                    duration: Snackbar.LENGTH_LONG,
                })
            );
        } else if (this.state.newTat === null || this.state.newTat === '') {
            this.setState({isLoading: false});
            return (
                Snackbar.show({
                    title: 'Please Enter new TAT',
                    duration: Snackbar.LENGTH_LONG,
                })
            );
        } else {
            axios.post(EDIT_ROUTE, {
                "trip_code": this.props.tripId,
                "route": this.state.newRoute,
                "tat": this.state.newTat
            }, {
                headers: {
                    'Authorization': 'Token ' + this.props.token,
                    'Content-Type': 'application/json'
                }
            }).then((response) => {
                console.log(response.data);
                console.log("reject message" + response.data.message);

                if (response.data.error === false) {
                    console.log('error === false' + response.data.message[0]);
                    this.setState({
                        successMessage: response.data.message[0],
                        isLoading: false
                    }, () => this.showSuccessfullMessage())

                } else {
                    console.log('error === true');
                    this.setState({
                        successMessage: response.data.message[0],
                        isLoading: false
                    }, () => this.showSuccessfullMessage())

                }
            }).catch((error) => {
                console.log("catch " + error);
                if (error.response) {
                    if (error.response.status === 401) {
                        AsyncStorage.removeItem('token');
                        this.props.navigation.navigate('Auth')
                    }
                }
            });
        }

    }


    showSuccessfullMessage() {
        const {closeModal, refreshing} = this.props;
        console.log(this.state);
        console.log('close Modal Reject::: ' + closeModal);
        console.log('success Message' + this.state.successMessage);
        Alert.alert(
            'Message',
            '' + this.state.successMessage,
            [
                {
                    text: 'OK', onPress: () => {
                        closeModal();
                        this.clearState();
                        refreshing()
                    }
                },
            ]
        );

    }

    clearState() {
        const {closeModal} = this.props;
        this.setState({
            successMessage: '',
            newRoute: this.props.route,
            newTat: '',
            newDistance: '',
            warningText: '',
            editInput: false
        });
        closeModal()
    }


    getDistance() {
        const {tripId,} = this.props;

        console.log('new Route get Distance ' + this.state.newRoute);

        console.log('token ' + this.props.token);
        axios.post(GET_DISTANCE, {
            "trip_code": tripId,
            "route": this.state.newRoute,
        }, {
            headers: {
                'Authorization': 'Token ' + this.props.token,
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            if (response.data.error === false) {
                console.log(response);
                this.setState({isLoadingCheck: false})
                {
                    response.data.distance !== 0 ? this.setState({
                        newDistance: response.data.distance,
                        editDistanceInput: false,
                    }) : this.setState({editDistanceInput: true})
                }

                console.log("new TAT : " + response.data.tat);
                {
                    response.data.tat !== 0 ? this.setState({
                        editTatInput: false,
                        newTat: response.data.tat
                    }) : this.setState({editTatInput: true})
                }

                {
                    response.data.message !== null && response.data.message !== undefined ? this.setState({warningText: response.data.message}) : null
                }

            } else {
            }
        }).catch((error) => {
            console.log("catch" + error);
            if (error.response) {
                if (error.response.status === 401) {
                    this.props.navigation.navigate('Auth')
                } else if (error.response.status === 400) {
                    console.log('error 400:::::::' + JSON.stringify(error.response))
                } else {
                    console.log("error" + error);
                }
            }
        });
    }

    render() {
        const {display, tat, route, distance} = this.props;


        return (
            <Modal
                visible={display}
                animationType="slide"
                transparent={true}
                onRequestClose={() => this.clearState()}
                avoidKeyboard={true}>

                <View style={styles.modalOuterStyle}>
                    <View style={styles.modalInnerStyle}>
                        <TouchableOpacity
                            style={styles.imageStyle}
                            hitSlop={{top: 30, bottom: 30, left: 70, right: 70}}
                            onPress={() => this.clearState()}>
                            <Icon
                                name='cancel'
                                type='material'
                                size={25}
                                color={colors.blueColor}/>
                        </TouchableOpacity>

                        <Text style={styles.headingStyle}>Edit Route</Text>

                        <ScrollView scrollEnabled={false}
                                    showsVerticalScrollIndicator={false}
                                    keyboardShouldPersistTaps='always'>

                            <View>

                                <View style={[styles.BorderStyle, {flex: 0.5, paddingLeft: 5}]}>
                                    <Text style={styles.phcustomerStyle}>Current Route</Text>
                                    <TextInput
                                        editable={false}
                                        style={[styles.StyleInput, {color: colors.textcolor}]}>{route}</TextInput>

                                </View>

                                <View style={[styles.BorderStyle, {flex: 0.5}]}>
                                    <Text style={styles.phcustomerStyle}>Current TAT</Text>
                                    <TextInput
                                        editable={false}
                                        style={[styles.StyleInput, {color: colors.textcolor}]}>{tat}</TextInput>
                                </View>

                                {distance !== null && distance !== undefined ?
                                    <View style={[styles.BorderStyle, {flex: 0.5}]}>
                                        <Text style={styles.phcustomerStyle}>Current Distance</Text>
                                        <TextInput
                                            editable={false}
                                            style={[styles.StyleInput, {color: colors.textcolor}]}>{distance}</TextInput>
                                    </View>
                                    :
                                    <View style={[styles.BorderStyle, {flex: 0.5}]}>
                                        <Text style={styles.phcustomerStyle}>Current Distance</Text>
                                        <TextInput
                                            editable={false}
                                            style={[styles.StyleInput, {color: colors.textcolor}]}>{'Not Added'}</TextInput>
                                    </View>
                                }


                                <View style={styles.BorderStyle}>
                                    <Text style={styles.phcustomerStyle}>New Route</Text>
                                    <TextInput
                                        editable={true}
                                        placeholderTextColor={colors.border_grey}
                                        style={[styles.StyleInput, {color: colors.textcolor}]}
                                        onChangeText={(text) => this.setState({
                                            newRoute: text,
                                            editTatInput: false,
                                            editDistanceInput: false,
                                            newDistance: '',
                                            newTat: '',
                                            warningText: ''
                                        })}>{route}</TextInput>

                                </View>

                                {this.state.editTatInput === true ?
                                    <View style={styles.BorderStyle}>
                                        <Text style={[styles.phcustomerStyle]}>New TAT</Text>
                                        <TextInput
                                            editable={true}
                                            placeholderTextColor={colors.border_grey}
                                            style={[styles.StyleInput, {color: colors.textcolor}]}
                                            keyboardType='numeric'
                                            onChangeText={(text) => this.setState({newTat: text})}>{this.state.newTat}</TextInput>

                                    </View>
                                    :
                                    <View style={styles.BorderStyle}>
                                        <Text style={styles.phcustomerStyle}>New TAT</Text>
                                        <TextInput
                                            editable={false}
                                            placeholderTextColor={colors.border_grey}
                                            style={styles.StyleInput}
                                            onChangeText={(text) => this.setState({newTat: text})}>{this.state.newTat}</TextInput>

                                    </View>
                                }


                                {this.state.editDistanceInput === true ?
                                    <View style={styles.BorderStyle}>
                                        <Text style={[styles.phcustomerStyle]}>New
                                            Distance</Text>
                                        <TextInput
                                            editable={true}
                                            placeholderTextColor={colors.border_grey}
                                            style={[styles.StyleInput, {color: colors.textcolor}]}
                                            keyboardType='numeric'
                                            onChangeText={(text) => this.setState({newDistance: text})}>{this.state.newDistance}</TextInput>
                                    </View>
                                    :
                                    <View style={styles.BorderStyle}>
                                        <Text style={styles.phcustomerStyle}>New Distance</Text>
                                        <TextInput
                                            editable={false}
                                            placeholderTextColor={colors.border_grey}
                                            style={styles.StyleInput}
                                            onChangeText={(text) => this.setState({newDistance: text})}>{this.state.newDistance}</TextInput>
                                    </View>
                                }

                                {this.state.warningText !== null && this.state.warningText !== '' ?
                                    <Text style={styles.warningTextStyle}>{this.state.warningText}</Text>
                                    : null}


                            </View>


                            <View style={{marginTop: 20}}>

                                <Button onPress={() => {
                                    this.setState({isLoadingCheck: true})
                                    this.getDistance()
                                }}
                                        buttonStyle={{marginTop: 10, marginBottom: 10}}
                                        title={'Click to fill TAT and Distance'}
                                        loading={this.state.isLoadingCheck}
                                        textStyle={{fontFamily: 'CircularStd-Book', fontSize: 14}}/>


                                <Button onPress={() => {
                                    this.setState({isLoading: true})
                                    this.checkNetworkSettings()
                                }}
                                        buttonStyle={{marginTop: 10, marginBottom: 10}}
                                        textStyle={{fontFamily: 'CircularStd-Book', fontSize: 14}}
                                        title={'Edit Route'}
                                        loading={this.state.isLoading}/>

                            </View>


                            <Text style={{textAlign: 'center'}}/>
                        </ScrollView>
                    </View>
                </View>
            </Modal>
        );
    }
}


const styles = {
    imageStyle: {
        width: 23,
        height: 22,
        marginTop: -15,
        marginLeft: -15
    },
    textStyle: {
        color: colors.white,
        padding: 5,
        fontFamily: 'CircularStd-Book',
        justifyContent: 'center'
    },
    reasonBorderStyle: {
        borderWidth: 1,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        borderColor: colors.blueColor,
        marginTop: 15,
        paddingLeft: 5,
        paddingRight: 5,
    },
    BorderStyle: {
        borderWidth: 1,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        borderColor: colors.blueColor,
        marginTop: 20,
        paddingLeft: 5,
        paddingRight: 5,
    },
    phcustomerStyle: {
        marginTop: -10,
        color: colors.dark_grey,
        backgroundColor: colors.white,
        marginLeft: 5,
        paddingLeft: 5,
        paddingRight: 5,
        fontFamily: 'CircularStd-Book',
        alignSelf: 'flex-start',
    },
    buttonStyle: {
        color: colors.white,
        backgroundColor: colors.blueColor,
        fontSize: 12,
        marginBottom: 20,
        justifyContent: 'flex-end',
        height: 35,
    },

    customerStyleInput: {
        fontSize: 12,
        color: colors.border_grey,
        marginLeft: 5,
        padding: 5,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        textAlignVertical: 'center',
        fontFamily: 'CircularStd-Book',

    },
    modalOuterStyle: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.7)'
    },
    modalInnerStyle: {
        backgroundColor: '#FFF',
        marginLeft: 15,
        marginRight: 15,
        borderRadius: 5,
        padding: 10,
        width: '90%'
    },
    StyleInput: {
        fontSize: 13,
        color: colors.border_grey,
        paddingLeft: 5,
        marginLeft: 5,
        fontFamily: 'CircularStd-Book',
        textAlignVertical: 'center',
        marginTop: 5,
        width:'100%',
        ...Platform.select({
            ios: {
                padding: 5,
                overflow: 'hidden'
            },
            android: {
                padding: 0,
            }
        })

    },
    pickerItemStyle: {
        fontSize: 12,
        color: colors.blueColor,
    },
    headingStyle: {
        fontSize: 15,
        fontWeight: 'bold',
        fontFamily: 'CircularStd-Book',
        color: colors.blueColor
    },
    warningTextStyle: {
        color: colors.redColor,
        fontSize: 12,
        fontFamily: 'CircularStd-Book',
        marginTop: 10,
        textAlign: 'center',
        justifyContent: 'center'
    },
    submitTextStyle: {
        color: colors.white,
        padding: 10,
        textAlign: 'center'
    }

};


