import React, { Component } from "react";
import {
  Alert,
  Modal,
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../colors/Colors";
import Snackbar from "react-native-snackbar";
import axios from "axios";
import { EDIT_TRIP } from "../constants/Constants";
import VehicleVendorAssignModal from "./VehicleVendorAssignModal";
import { Button, Icon } from "react-native-elements";
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from "@react-native-community/async-storage";

let crossDockData;

class NewTripEditTripModal extends Component {
  state = {
    showList: false,
    message: "",
    selectedValue: "",
    showModal: false,
    vendorName: "",
    vendorId: "",
    vendorCode: "",
    vehicleName: "",
    vehicleId: "",
    title: "",
    filterData: [],
    apFlag: "",
    phone: "",
    engagedByName: "",
    engagedById: "",
    isLoading: false,
    originalData: []
  };

  checkNetworkSettings() {
    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        this.editTrip();
      } else {
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });
  }

  editTrip() {
    console.log(this.state);
    console.log("clicked" + this.props.tripId);
    console.log("vehicle Id" + this.state.vehicleId);
    console.log("Vendor Id" + this.state.vendorId);
    console.log("Engaged by Id" + this.state.engagedById);

    if (this.state.vehicleId === "" || this.state.vehicleId === undefined) {
      this.setState({ isLoading: false });
      Snackbar.show({
        title: "Please Select Vehicle",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (
      this.state.vendorId === "" ||
      this.state.vendorId === undefined
    ) {
      // ToastAndroid.show('Select Vendor', ToastAndroid.SHORT)
      this.setState({ isLoading: false });
      Snackbar.show({
        title: "Please Select Vendor ",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (this.state.apFlag === "0") {
      // ToastAndroid.show('Selected Vendor cannot be assigned', ToastAndroid.SHORT)
      this.setState({ isLoading: false });
      Snackbar.show({
        title: "Selected Vendor cannot be assigned",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (this.state.phone === "" || this.state.phone === undefined) {
      this.setState({ isLoading: false });
      Snackbar.show({
        title: "Please add Driver Phone",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      axios
        .post(
          EDIT_TRIP,
          {
            trip_code: this.props.tripId,
            vehicle: this.state.vehicleId,
            phone: this.state.phone,
            vendor: this.state.vendorId,
            engaged_by: this.state.engagedById,
            type: "vehicle-change"
          },
          {
            headers: {
              Authorization: "Token " + this.props.token,
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          console.log(response.data);
          console.log("reject message" + response.data);
          if (response.data.error === false) {
            this.setState(
              {
                isLoading: false,
                message: response.data.message[0]
              },
              () => this.showErrorMessage()
            );
            // closeModal
          } else {
            this.setState(
              {
                isLoading: false,
                message: response.data.message[0]
              },
              () => this.showErrorMessage()
            );
          }
        })
        .catch(error => {
          console.log("catch" + error);
          if (error.response) {
            if (error.response.status === 401) {
              AsyncStorage.removeItem("token");
              this.props.navigation.navigate("Auth");
            } else if (error.response.status === 400) {
              console.log("error 400:::::::" + error.response);
            } else {
              console.log("error" + error);
            }
          }
        });
    }
  }

  clearState() {
    const { closeModal } = this.props;

    this.setState(
      {
        showList: false,
        message: "",
        selectedValue: "",
        showModal: false,
        vendorName: "",
        vendorId: "",
        vendorCode: "",
        vehicleName: "",
        vehicleId: "",
        title: "",
        filterData: [],
        apFlag: "",
        phone: "",
        engagedByName: "",
        engagedById: ""
      },
      () => closeModal()
    );
  }

  showErrorMessage() {
    const { closeModal, refreshing } = this.props;
    Alert.alert("Message", this.state.message, [
      {
        text: "OK",
        onPress: () => {
          closeModal();
          refreshing();
        }
      }
    ]);
  }

  checkVendor() {
    if (this.state.vehicleId !== null && this.state.vehicleId !== "") {
      this.showModal("vendor");
    } else {
      console.log("vendorCheck Snackbar");
      return Snackbar.show({
        title: "Please Select Vehicle for Vendor Selection",
        duration: Snackbar.LENGTH_LONG
      });
    }
  }

  checkVendorName(value, value1) {
    return (
      <TouchableOpacity onPress={() => this.checkVendor()} activeOpacity={0.9}>
        <View style={styles.customerBorderStyle}>
          <Text style={styles.phcustomerStyle}>Vendor</Text>
          <Text
            label="Vendor"
            style={
              value !== ""
                ? [styles.customerStyleInput, { color: colors.dark_grey }]
                : styles.customerStyleInput
            }
          >
            {value !== null && value !== ""
              ? value + " (" + value1 + " )"
              : "Vendor"}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  checkVehicleName(value) {
    console.log("vehicle id :::::" + this.state.vehicleId);
    console.log("vendor id :::::" + this.state.vendorId);

    return (
      <TouchableOpacity
        onPress={() => {
          this.showModal("vehicle");
        }}
        activeOpacity={0.9}
      >
        <View style={styles.customerBorderStyle}>
          <Text style={styles.phcustomerStyle}>Vehicle</Text>
          <Text
            label="Vehicle"
            style={
              value !== ""
                ? [styles.customerStyleInput, { color: colors.dark_grey }]
                : styles.customerStyleInput
            }
          >
            {value !== null && value !== "" ? value : "Vehicle"}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  checkEngagedBy(value) {
    console.log("engagedby id :::::" + this.state.engagedById);

    return (
      <TouchableOpacity
        onPress={() => {
          this.showModal("engagedby");
        }}
        activeOpacity={0.9}
      >
        <View style={styles.customerBorderStyle}>
          <Text style={styles.phcustomerStyle}>Vehicle Engaged By</Text>
          <Text
            label="Engaged By"
            style={
              value !== ""
                ? [styles.customerStyleInput, { color: colors.dark_grey }]
                : styles.customerStyleInput
            }
          >
            {value !== null && value !== "" ? value : "Engaged By"}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  renderModal() {
    console.log("vehicleId renderModal::::" + this.state.vehicleId);

    console.log(
      "render Modal FilterData" + JSON.stringify(this.state.filterData)
    );

    return (
      <VehicleVendorAssignModal
        display={this.state.showModal}
        closeModal={() => {
          this.setState({ showModal: false });
        }}
        filterData={this.state.filterData}
        title={this.state.title}
        SelectedValue={this.state.selectedValue}
        filterSearch={this.filterSearch.bind(this)}
      />
    );
  }

  showModal(type) {
    const showModal = true;
    let originalData = [];
    let selectedValue = true;
    let tickVisible = false;
    let title = "";

    let mappedArr;

    const { vehicles = {}, vendors = {}, engagedBy = {} } = this.props;
    console.log("Show Modal Assign modal props----------", this.props);

    console.log(
      "show modal vehicles -----------------------------",
      JSON.stringify(vehicles)
    );
    console.log(
      "show modal vendors -----------------------------",
      JSON.stringify(vendors)
    );
    // originalData = Object.keys(vehicles);

    console.log(
      "original data :::::::::::::::::::::::" +
        JSON.stringify(Object.keys(vehicles))
    );
    console.log(
      "original data :::::::::::::::::::::::" +
        JSON.stringify(Object.keys(vendors))
    );

    const { cities = {} } = this.props;
    console.log(
      "cities in show Modal -----------------------------",
      JSON.stringify(cities)
    );
    console.log(
      "cities in show Modal Newtrip Edit props-----------------------------",
      this.props
    );
    originalData = Object.keys(cities);

    console.log(
      "Original data show Modal Newtrip Edit props-----------------------------",
      JSON.stringify(originalData)
    );

    if (type === "vendor") {
      selectedValue = this.VendorSelectedValue;
      title = "Vendors";
      tickVisible = true;
      mappedArr = Object.keys(vendors).map(function(i) {
        return i, vendors[i];
      });
    } else if (type === "vehicle") {
      selectedValue = this.VehicleSelectedValue;
      title = "Vehicle";
      mappedArr = Object.keys(vehicles).map(function(i) {
        return i, vehicles[i];
      });
    } else if (type === "engagedby") {
      selectedValue = this.EngagedBySelectedValue;
      title = "Engaged By";
      mappedArr = Object.keys(engagedBy).map(function(i) {
        return i, engagedBy[i];
      });
    }
    this.setState(
      {
        originalData: mappedArr,
        showModal,
        filterData: mappedArr,
        selectedValue,
        tickVisible,
        title
      },
      () => console.log(this.state)
    );
  }

  filterSearch(text) {
    console.log("filterText" + this.state);

    if (text !== "") {
      const jsonData_filter = this.state.originalData.filter(item => {
        let itemData = "";
        if (this.state.title === "Vehicle") {
          itemData = item.number + item.vehicle_type;
        } else if (this.state.title === "Engaged By") {
          itemData = item.name.toLowerCase();
        } else {
          itemData = item.code.toLowerCase() + item.name.toLowerCase();
        }
        const textData = text.toLowerCase();

        console.log("textData filter" + textData.toLowerCase());
        return itemData.indexOf(textData) > -1;
      });

      this.setState({
        text: text,
        filterData: jsonData_filter
      });
    } else {
      this.setState({
        filterData: this.state.originalData
      });
    }
  }

  VendorSelectedValue = (changeText, selectedId, code, apFlag) => {
    console.log("selected value vendorName" + changeText);
    console.log("selected value vendorId" + selectedId);
    this.setState({
      vendorName: changeText,
      vendorId: selectedId,
      showModal: false,
      vendorCode: code,
      apFlag: apFlag
    });
  };

  VehicleSelectedValue = (changeText, selectedId, code, vehicleTypeId) => {
    console.log(
      "selected value numer" + changeText + "  vehicle id name " + selectedId
    );
    this.setState({
      vehicleName: changeText,
      vehicleId: selectedId,
      showModal: false
    });
  };

  EngagedBySelectedValue = (changeText, selectedId) => {
    console.log(
      "selected value numer" + changeText + "  engaged " + selectedId
    );
    this.setState({
      engagedByName: changeText,
      engagedById: selectedId,
      showModal: false
    });
  };

  render() {
    const { display, booking_code } = this.props;
    console.log("bookingCode Received RejectionReasonsInput" + booking_code);
    // console.log('modal json length' + crossDocData.length);

    return (
      <Modal
        visible={display}
        animationType="slide"
        transparent={true}
        onRequestClose={() => this.clearState()}
      >
        <View style={styles.modalOuterStyle}>
          <View style={styles.modalInnerStyle}>
            <TouchableOpacity
              style={styles.imageStyle}
              hitSlop={{ top: 30, bottom: 30, left: 70, right: 70 }}
              onPress={() => this.clearState()}
            >
              <Icon
                name="cancel"
                type="material"
                size={25}
                color={colors.blueColor}
              />
            </TouchableOpacity>

            {this.renderModal()}

            {this.checkVehicleName(this.state.vehicleName)}

            {this.checkVendorName(this.state.vendorName, this.state.vendorCode)}

            {this.checkEngagedBy(this.state.engagedByName)}

            <View style={styles.customerBorderStyle}>
              <Text style={styles.phcustomerStyle}>Driver Phone</Text>
              <TextInput
                editable={true}
                placeholder="Driver Phone"
                keyboardType="numeric"
                maxLength={10}
                placeholderTextColor={colors.border_grey}
                style={
                  this.state.phone !== ""
                    ? [styles.StyleInput, { color: colors.dark_grey }]
                    : styles.StyleInput
                }
                multiline={false}
                onChangeText={text => this.setState({ phone: text })}
              />
            </View>

            <Button
              onPress={() => {
                this.setState({ isLoading: true });
                this.checkNetworkSettings();
              }}
              title={"Edit Trip"}
              loading={this.state.isLoading}
              buttonStyle={{ marginTop: 10, marginBottom: 10 }}
              textStyle={{
                fontFamily: "CircularStd-Book",
                fontSize: 14
              }}
            />

            {/*<View*/}
            {/*    style={styles.buttonStyle}>*/}
            {/*    <TouchableOpacity onPress={() => this.editTrip()}>*/}
            {/*        <Text style={styles.submitTextStyle}>Edit Trip</Text>*/}
            {/*    </TouchableOpacity>*/}
            {/*</View>*/}
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = {
  imageStyle: {
    width: 23,
    height: 22,
    marginTop: -15,
    marginLeft: -15
  },
  customerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    paddingLeft: 5,
    paddingRight: 5,
    alignItems: "flex-start"
  },
  phcustomerStyle: {
    marginTop: -10,
    color: colors.dark_grey,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  // buttonStyle: {
  //     color: colors.white,
  //     backgroundColor: colors.blueColor,
  //     fontSize: 12,
  //     justifyContent: 'flex-end',
  //     marginTop: 20,
  //     marginLeft: 2,
  //     marginRight: 2,
  //     marginBottom: 20
  // },
  customerStyleInput: {
    fontSize: 12,
    color: colors.border_grey,
    marginLeft: 5,
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center",
    ...Platform.select({
      ios: {
        padding: 5,
        overflow: "hidden"
      },
      android: {
        paddingLeft: 5,
        paddingTop: 5,
        paddingBottom: 5
      }
    })
  },
  modalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.7)"
  },
  modalInnerStyle: {
    backgroundColor: "#FFF",
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    padding: 10,
    width: "90%"
  },
  BorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    flex: 1,
    paddingLeft: 5,
    paddingRight: 5
  },
  StyleInput: {
    fontSize: 12,
    color: colors.border_grey,
    paddingLeft: 5,
    marginLeft: 5,
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center",
    width: "100%",
    ...Platform.select({
      ios: {
        padding: 5,
        overflow: "hidden"
      },
      android: {
        padding: 0
      }
    })
  },
  submitTextStyle: {
    color: colors.white,
    padding: 10,
    textAlign: "center"
  },

  buttonTextStyle: {
    color: colors.white,
    textAlign: "center",
    padding: 10,
    fontSize: 15,
    fontFamily: "CircularStd-Book",
    backgroundColor: colors.blueColor
  },
  buttonStyle: {
    ...Platform.select({
      ios: {
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        overflow: "hidden"
      },
      android: {
        marginTop: 20,
        marginBottom: 20
      }
    })
  }
};

// const mapStateToProps = state => ({
//     state: state,
//     error: state.error,
//     products: state.productsReducer.products,
//     pending: state.productsReducer.pending,
// });
//
//
// export default connect(
//     mapStateToProps,
//     null
// )(NewTripEditTripModal);

export default NewTripEditTripModal;
