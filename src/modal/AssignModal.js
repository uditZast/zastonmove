import React, { Component } from "react";
import {
  Alert,
  Modal,
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../colors/Colors";
import Snackbar from "react-native-snackbar";
import axios from "axios";
import { ASSIGN_BOOKING } from "../constants/Constants";
import { Button, Icon } from "react-native-elements";
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from "@react-native-community/async-storage";
import VehicleVendorAssignModal from "./VehicleVendorAssignModal";

class AssignModal extends Component {
  state = {
    showList: false,
    message: "",
    selectedValue: "",
    showModal: false,
    vendorName: "",
    vendorId: "",
    vendorCode: "",
    vehicleName: "",
    vehicleId: "",
    title: "",
    filterData: [],
    apFlag: "",
    phone: "",
    isLoading: false
  };

  checkNetworkSettings() {
    NetInfo.fetch().then(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);

      if (state.isConnected) {
        this.assignVehicle();
      } else {
        this.setState({ isLoading: false });
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });
  }

  assignVehicle() {
    console.log("clicked" + this.props.booking_code);
    console.log("vehicle Id" + this.state.vehicleId);
    console.log("Vendor Id" + this.state.vendorId);

    if (this.state.vehicleId === "" || this.state.vehicleId === undefined) {
      this.setState({ isLoading: false }, () =>
        console.log("is Loading " + this.state.isLoading)
      );
      Snackbar.show({
        title: "Please Select Vehicle",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (
      this.state.vendorId === "" ||
      this.state.vendorId === undefined
    ) {
      this.setState({ isLoading: false });
      Snackbar.show({
        title: "Please Select Vendor ",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (this.state.apFlag === "0") {
      this.setState({ isLoading: false });
      Snackbar.show({
        title: "Selected Vendor cannot be assigned",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (this.state.phone === "" || this.state.phone === undefined) {
      this.setState({ isLoading: false });
      Snackbar.show({
        title: "Please add Driver Phone",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      axios
        .post(
          ASSIGN_BOOKING,
          {
            booking_code: this.props.booking_code,
            vehicle_id: this.state.vehicleId,
            phone: this.state.phone,
            vendor: this.state.vendorId
          },
          {
            headers: {
              Authorization: "Token " + this.props.token,
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          console.log(response.data);
          console.log("reject message" + response.data);
          if (response.data.error === false) {
            this.setState(
              {
                message: response.data.message[0],
                vehicleId: "",
                vehicleName: "",
                vendorId: "",
                vendorName: "",
                vendorCode: "",
                isLoading: false
              },
              () => this.showErrorMessage()
            );
            // closeModal
          } else {
            this.setState(
              {
                message: response.data.message[0],
                vehicleId: "",
                vehicleName: "",
                vendorId: "",
                vendorName: "",
                vendorCode: "",
                isLoading: false
              },
              () => this.showErrorMessage()
            );
          }
        })
        .catch(error => {
          console.log("catch" + error);
          if (error.response) {
            if (error.response.status === 401) {
              AsyncStorage.removeItem("token");
              this.props.navigation.navigate("Auth");
            } else if (error.response.status === 400) {
              console.log("error 400:::::::" + error.response);
            } else {
              console.log("error" + error);
            }
          }
        });
    }
  }

  clearState() {
    const { closeModal } = this.props;

    this.setState(
      {
        showList: false,
        message: "",
        selectedValue: "",
        showModal: false,
        vendorName: "",
        vendorId: "",
        vendorCode: "",
        vehicleName: "",
        vehicleId: "",
        title: "",
        filterData: [],
        apFlag: "",
        phone: ""
      },
      () => closeModal()
    );
  }

  showErrorMessage() {
    const { closeModal, refreshing } = this.props;
    Alert.alert("Message", this.state.message, [
      {
        text: "OK",
        onPress: () => {
          closeModal();
          refreshing();
        }
      }
    ]);
  }

  checkVendor() {
    if (this.state.vehicleId !== null && this.state.vehicleId !== "") {
      this.showModal("vendor");
    } else {
      console.log("vendorCheck Snackbar");
      return Snackbar.show({
        title: "Please Select Vehicle for Vendor Selection",
        duration: Snackbar.LENGTH_LONG
      });
    }
  }

  checkVendorName(value, value1) {
    return (
      <TouchableOpacity onPress={() => this.checkVendor()} activeOpacity={0.9}>
        <View style={styles.customerBorderStyle}>
          <Text style={styles.phcustomerStyle}>Vendor</Text>
          <Text
            label="Vendor"
            style={
              value !== ""
                ? styles.selectedTextColor
                : styles.customerStyleInput
            }
          >
            {value !== null && value !== ""
              ? value + " (" + value1 + " )"
              : "Vendor"}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  checkVehicleName(value) {
    console.log("selected id :::::" + this.state.selectedValue);
    console.log("vehicle id :::::" + this.state.vehicleId);
    console.log("vendor id :::::" + this.state.vendorId);

    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({ vehicleId: "" });
          this.showModal("vehicle");
        }}
        activeOpacity={0.9}
      >
        <View style={styles.customerBorderStyle}>
          <Text style={styles.phcustomerStyle}>Vehicle</Text>
          <Text
            label="Vehicle"
            style={
              value !== ""
                ? styles.selectedTextColor
                : styles.customerStyleInput
            }
          >
            {value !== null && value !== "" ? value : "Vehicle"}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  renderModal() {
    console.log("vehicleId renderModal::::" + this.state.vehicleId);

    console.log(
      "render modal FilterData :::::::::::" +
        JSON.stringify(this.state.filterData)
    );
    return (
      <VehicleVendorAssignModal
        display={this.state.showModal}
        closeModal={() => {
          this.setState({ showModal: false });
        }}
        filterData={this.state.filterData}
        title={this.state.title}
        SelectedValue={this.state.selectedValue}
        filterSearch={this.filterSearch.bind(this)}
      />
    );
  }

  showModal(type) {
    const showModal = true;
    let originalData = [];
    let selectedValue = true;
    let tickVisible = false;
    let title = "",
      vehicleId = "";
    let mappedArr;

    const { vehicles = {}, vendors = {} } = this.props;
    console.log("Show Modal Assign modal props----------", this.props);

    console.log(
      "show modal vehicles -----------------------------",
      JSON.stringify(vehicles)
    );
    console.log(
      "show modal vendors -----------------------------",
      JSON.stringify(vendors)
    );
    // originalData = Object.keys(vehicles);

    console.log(
      "original data :::::::::::::::::::::::" +
        JSON.stringify(Object.keys(vehicles))
    );
    console.log(
      "original data :::::::::::::::::::::::" +
        JSON.stringify(Object.keys(vendors))
    );

    // var mappedArr = Object.keys(vehicles).map(function (i) {
    //     return i, vehicles[i];
    // });

    if (type === "vendor") {
      originalData = Object.keys(vendors).map(function(i) {
        return i, vendors[i];
      });
      selectedValue = this.VendorSelectedValue;
      title = "Vendors";
      tickVisible = true;
      vehicleId = this.state.vehicleId;
      mappedArr = Object.keys(vendors).map(function(i) {
        return i, vendors[i];
      });
    } else if (type === "vehicle") {
      originalData = Object.keys(vehicles).map(function(i) {
        return i, vendors[i];
      });
      selectedValue = this.VehicleSelectedValue;
      title = "Vehicle";
      mappedArr = Object.keys(vehicles).map(function(i) {
        return i, vehicles[i];
      });
    }

    this.setState({
      originalData: mappedArr,
      showModal,
      filterData: mappedArr,
      selectedValue,
      tickVisible,
      title,
      vehicleId: vehicleId
    });
  }

  filterSearch(text) {
    console.log("filterText" + text);
    if (text !== "") {
      const jsonData_filter = this.state.originalData.filter(item => {
        let itemData = "";
        if (this.state.title === "Vehicle") {
          itemData = item.number + item.vehicle_type;
        } else {
          itemData = item.code.toLowerCase() + item.name.toLowerCase();
        }
        const textData = text.toLowerCase();

        console.log("textData filter" + textData);
        return itemData.indexOf(textData) > -1;
      });

      this.setState({
        text: text,
        filterData: jsonData_filter
      });
    } else {
      this.setState({
        filterData: this.state.originalData
      });
    }
  }

  VendorSelectedValue = (changeText, selectedId, code, apFlag) => {
    console.log("selected value vendorName" + changeText);
    console.log("selected value vendorId" + selectedId);
    this.setState({
      vendorName: changeText,
      vendorId: selectedId,
      showModal: false,
      vendorCode: code,
      apFlag: apFlag
    });
  };

  VehicleSelectedValue = (changeText, selectedId, code, vehicleTypeId) => {
    console.log(
      "selected value numer" + changeText + "  vehicleId " + selectedId
    );
    this.setState({
      vehicleName: changeText,
      vehicleId: selectedId,
      showModal: false
    });
  };

  render() {
    const { closeModal, display, booking_code } = this.props;
    console.log("bookingCode Received RejectionReasonsInput" + booking_code);
    // console.log('modal json length' + JSON.stringify(jsonData[3]))

    return (
      <Modal
        visible={display}
        animationType="slide"
        transparent={true}
        onRequestClose={() => this.clearState()}
      >
        <View style={styles.modalOuterStyle}>
          <View style={styles.modalInnerStyle}>
            <TouchableOpacity
              style={styles.imageStyle}
              hitSlop={{ top: 30, bottom: 30, left: 70, right: 70 }}
              onPress={() => this.clearState()}
            >
              <Icon
                name="cancel"
                type="material"
                size={25}
                color={colors.blueColor}
              />
            </TouchableOpacity>

            {this.renderModal()}

            {this.checkVehicleName(this.state.vehicleName)}

            {this.checkVendorName(this.state.vendorName, this.state.vendorCode)}

            <View style={styles.customerBorderStyle}>
              <Text style={styles.phcustomerStyle}>Driver Phone</Text>
              <TextInput
                editable={true}
                placeholder="Driver Phone"
                keyboardType="numeric"
                placeholderTextColor={colors.border_grey}
                multiline={false}
                maxLength={10}
                onChangeText={text => this.setState({ phone: text })}
                style={
                  this.state.phone !== ""
                    ? [
                        styles.StyleInput,
                        {
                          color: colors.dark_grey,
                          width: "100%"
                        }
                      ]
                    : styles.StyleInput
                }
              />
            </View>

            <View style={{ marginTop: 10, marginBottom: 10 }}>
              <Button
                onPress={() => {
                  this.setState({ isLoading: true });
                  this.checkNetworkSettings();
                }}
                title={"Assign"}
                buttonStyle={{ marginTop: 10, marginBottom: 10 }}
                loading={this.state.isLoading}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = {
  imageStyle: {
    width: 23,
    height: 22,
    marginTop: -18,
    marginLeft: -18
  },
  customerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    paddingLeft: 5,
    paddingRight: 5,
    alignItems: "flex-start"
  },
  phcustomerStyle: {
    marginTop: -10,
    color: colors.dark_grey,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  buttonStyle: {
    color: colors.white,
    backgroundColor: colors.blueColor,
    fontSize: 15,
    justifyContent: "flex-end",
    marginTop: 20,
    marginLeft: 2,
    marginRight: 2,
    marginBottom: 10,
    textAlign: "center",
    padding: 10
  },
  customerStyleInput: {
    fontSize: 12,
    color: colors.border_grey,
    padding: 5,
    marginLeft: 5,
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center"
  },
  selectedTextColor: {
    fontSize: 12,
    color: colors.dark_grey,
    marginLeft: 5,
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
    fontFamily: "CircularStd-Book",
    ...Platform.select({
      ios: {
        padding: 5,
        overflow: "hidden"
      },
      android: {
        padding: 10
      }
    })
  },
  modalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.7)"
  },
  modalInnerStyle: {
    backgroundColor: "#FFF",
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    padding: 10,
    width: "90%"
  },
  BorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    flex: 1,
    paddingLeft: 5,
    paddingRight: 5
  },
  StyleInput: {
    fontSize: 12,
    color: colors.dark_grey,
    paddingLeft: 5,
    marginLeft: 5,
    width: "100%",
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center",
    ...Platform.select({
      ios: {
        padding: 5,
        overflow: "hidden"
      },
      android: {
        padding: 0
      }
    })
  }
};

// const mapStateToProps = state => ({
//     state: state,
//     error: state.error,
//     products: state.productsReducer.products,
//     pending: state.productsReducer.pending,
// });
//
//
// export default connect(
//     mapStateToProps,
//     null
// )(AssignModal);

export default AssignModal;
