import React, { Component } from "react";
import {
  Alert,
  Modal,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../colors/Colors";
import Snackbar from "react-native-snackbar";
import axios from "axios";
import { EDIT_REFERENCE } from "../constants/Constants";
import { ScrollView } from "react-navigation";
import { Button, Icon } from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";

let comment, reason, reason_code;
export default class ReferenceModal extends Component {
  state = { successMessage: "", isLoading: false };

  submitReference() {
    console.log("clicked" + this.props.token);
    console.log("rejection commnet" + comment);

    if (comment === undefined || comment === "") {
      // ToastAndroid.show('Please add some comment', ToastAndroid.SHORT)
      this.setState({ isLoading: false });
      return Snackbar.show({
        title: "Please add Reference",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      axios
        .post(
          EDIT_REFERENCE,
          {
            trip_code: this.props.tripId,
            reference: comment
          },
          {
            headers: {
              Authorization: "Token " + this.props.token,
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          console.log(response.data);
          console.log("reject message" + response.data.message);

          if (response.data.error === false) {
            console.log("error === false" + response.data.message[0]);
            this.setState(
              { successMessage: response.data.message[0], isLoading: false },
              () => this.showSuccessfullMessage()
            );
          } else {
            console.log("error === true");
            this.setState(
              { successMessage: response.data.message[0], isLoading: false },
              () => this.showSuccessfullMessage()
            );
          }
        })
        .catch(error => {
          console.log("catch " + error);
          if (error.response) {
            if (error.response.status === 401) {
              AsyncStorage.removeItem("token");
              this.props.navigation.navigate("Auth");
            }
          }
        });
    }
  }

  showSuccessfullMessage() {
    const { closeModal, refreshing } = this.props;
    console.log(this.state);
    console.log("close Modal Reject::: " + closeModal);
    console.log("success Message" + this.state.successMessage);
    Alert.alert("Message", "" + this.state.successMessage, [
      {
        text: "OK",
        onPress: () => {
          closeModal();
          this.clearState();
        }
      }
    ]);
  }

  clearState() {
    const { closeModal } = this.props;

    comment = "";
    closeModal();
  }

  render() {
    const { closeModal, display, tripId } = this.props;

    console.log("bookingCode Received RejectionReasonsInput" + tripId);

    return (
      <Modal
        visible={display}
        animationType="slide"
        transparent={true}
        onRequestClose={() => this.clearState()}
        avoidKeyboard={true}
      >
        <View style={styles.modalOuterStyle}>
          <View style={styles.modalInnerStyle}>
            <TouchableOpacity
              style={styles.imageStyle}
              hitSlop={{ top: 30, bottom: 30, left: 70, right: 70 }}
              onPress={() => this.clearState()}
            >
              <Icon
                name="cancel"
                type="material"
                size={25}
                color={colors.blueColor}
              />
            </TouchableOpacity>

            <ScrollView
              scrollEnabled={false}
              showsVerticalScrollIndicator={false}
              keyboardShouldPersistTaps="always"
            >
              <View style={styles.BorderStyle}>
                <Text style={styles.phcustomerStyle}>Reference</Text>
                <TextInput
                  editable={true}
                  placeholder="Reference"
                  placeholderTextColor={colors.border_grey}
                  style={styles.StyleInput}
                  onChangeText={text => (comment = text)}
                />
              </View>

              <Button
                title={"Submit"}
                textStyle={{
                  textAlign: "center",
                  fontSize: 11,
                  color: colors.white
                }}
                buttonStyle={{ marginTop: 20 }}
                onPress={() =>
                  this.setState({ isLoading: true }, () =>
                    this.submitReference()
                  )
                }
                loading={this.state.isLoading}
              />

              {/*<View style={styles.buttonStyle}>*/}
              {/*    <TouchableOpacity onPress={() => this.submitReference()}>*/}
              {/*        <Text style={styles.textStyle}>Submit</Text>*/}
              {/*    </TouchableOpacity>*/}

              {/*</View>*/}
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = {
  imageStyle: {
    width: 23,
    height: 22,
    marginTop: -15,
    marginLeft: -15
  },
  textStyle: {
    color: colors.white,
    padding: 5,
    fontFamily: "CircularStd-Book",
    justifyContent: "center"
  },
  reasonBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 15,
    paddingLeft: 5,
    paddingRight: 5
  },
  BorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    paddingLeft: 5,
    paddingRight: 5,
    height: 40
  },
  phcustomerStyle: {
    marginTop: -10,
    color: colors.dark_grey,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  buttonStyle: {
    color: colors.white,
    backgroundColor: colors.blueColor,
    fontSize: 12,
    marginTop: 20,
    marginBottom: 20,
    justifyContent: "flex-end",
    height: 35
  },

  customerStyleInput: {
    fontSize: 12,
    color: colors.border_grey,
    marginLeft: 5,
    padding: 5,
    height: 35,
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
    fontFamily: "CircularStd-Book"
  },
  modalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.7)"
  },
  modalInnerStyle: {
    backgroundColor: "#FFF",
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    padding: 10,
    width: "90%"
  },

  StyleInput: {
    fontSize: 13,
    color: colors.border_grey,
    paddingLeft: 5,
    marginLeft: 5,
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center",
    padding: 0,
    marginTop: 5,
    width: "100%"
  },
  pickerItemStyle: {
    fontSize: 12,
    color: colors.blueColor
  }
};
