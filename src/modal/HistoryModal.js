import React, { Component } from "react";
import {
  View,
  Text,
  Modal,
  FlatList,
  TouchableOpacity,
  Image
} from "react-native";
import close from "../images/cancel.png";
import colors from "../colors/Colors";

const HistoryModal = props => {
  const { closeModal, display, historyArray } = props;

  if (jsonData.length !== 0) {
    return (
      <Modal visible={display} animationType="slide">
        <View style={styles.modalContainer}>
          <TouchableOpacity onPress={closeModal}>
            <Image source={close} style={styles.imageStyle} />
          </TouchableOpacity>

          <FlatList
            data={historyArray}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (
              <View style={{ flex: 1 }}>
                <Text>{item.status}</Text>
              </View>
            )}
            keyboardShouldPersistTaps={"handled"}
            keyExtractor={item => item.time.toString()}
          />
        </View>
      </Modal>
    );
  } else {
    return null;
  }
};

const styles = {
  modalContainer: {
    marginLeft: 15,
    marginTop: 20
  },
  listStyle: {
    backgroundColor: colors.light_grey,
    flex: 1,
    marginTop: 10
  },
  imageStyle: {
    width: 18,
    height: 18,
    marginTop: 10,
    marginLeft: 10
  },
  textStyle: {
    color: colors.textcolor,
    padding: 5,
    fontFamily: "CircularStd-Book"
  },
  lineStyle: {
    borderBottomColor: colors.border_grey,
    borderBottomWidth: 1,
    marginTop: 8,
    marginLeft: 10,
    marginRight: 5,
    marginBottom: 5
  }
};

export default HistoryModal;
