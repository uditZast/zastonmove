import React, { Component } from "react";
import { Modal, StyleSheet, View } from "react-native";
import colors from "../colors/Colors";
import Geocoder from "react-native-geocoder";
import Container from "../components/common/Container";
import Button from "react-native-elements";
import MapView from "react-native-maps";

let lat, lng;

export default class LocationMapModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      markers: [],
      draggedCoordinate: {
        latitude: 20.5937,
        longitude: 78.9629
      },
      address: ""
    };
  }

  async geoCoderFunction() {
    lat = this.state.draggedCoordinate.latitude;
    lng = this.state.draggedCoordinate.longitude;

    console.log("latitude : " + lat + "longitude : " + lng);

    Geocoder.fallbackToGoogle("AIzaSyDjfNsHFVUcBrNM3YkMSULF4y72eRs0o00");
    let ret = await Geocoder.geocodePosition({ lat, lng });
    // debugger;
    this.setState({ address: ret[0].formattedAddress });

    console.log("dragged coordinate " + ret[0].formattedAddress);
  }

  setLocation = () => {};

  render() {
    const { display } = this.props;

    return (
      <Container>
        <Modal visible={display} avoidKeyboard={true}>
          <View style={{ flex: 1 }}>
            <View style={{ backgroundColor: colors.blueColor }} />

            <View style={styles.mapViewStyle}>
              <MapView
                scrollEnabled={false}
                style={styles.map}
                showsUserLocation={false}
                zoomEnabled={true}
                zoomControlEnabled={true}
                initialRegion={{
                  latitude: 20.5937,
                  longitude: 78.9629,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421
                }}
              >
                <MapView.Marker
                  draggable
                  coordinate={this.state.draggedCoordinate}
                  // onDragEnd={(e) => this.setState({
                  //     draggedCoordinate: e.nativeEvent.coordinate,
                  // }, () => this.geoCoderFunction())}
                />
              </MapView>
            </View>

            <Button title={"Confirm Location"} />
          </View>
        </Modal>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  modalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.7)"
  },
  modalInnerStyle: {
    backgroundColor: "#FFF",
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    padding: 10,
    width: "90%"
  },
  map: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    flex: 0.5
  },
  mapViewStyle: {
    flex: 1,
    height: "20%"
  }
});
