import React, { Component } from "react";
import Geocoder from "react-native-geocoder";
import {
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import MapView from "react-native-maps";
import Modal from "react-native-modal";
import { Button, Icon } from "react-native-elements";
import colors from "../colors/Colors";

let lat, lng;
export default class CrossDockLocationModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      markers: [],
      draggedCoordinate: {
        latitude: 20.5937,
        longitude: 78.9629
      },
      address: ""
    };
  }

  async geoCoderFunction() {
    lat = this.state.draggedCoordinate.latitude;
    lng = this.state.draggedCoordinate.longitude;

    console.log("latitude : " + lat + "longitude : " + lng);

    Geocoder.fallbackToGoogle("AIzaSyDjfNsHFVUcBrNM3YkMSULF4y72eRs0o00");
    let ret = await Geocoder.geocodePosition({ lat, lng });
    // debugger;
    this.setState({ address: ret[0].formattedAddress });

    console.log("dragged coordinate " + ret[0].formattedAddress);
  }

  setLocation = () => {};

  closeModalView() {
    console.log("close modal view 1");
    const { closeModal } = this.props;
    console.log("close modal view 2");
    closeModal();
  }

  render() {
    const { display } = this.props;

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Modal
          style={{ margin: 0 }}
          isVisible={display}
          onBackButtonPress={() => this.closeModalView()}
          avoidKeyboard={true}
        >
          <View style={{ backgroundColor: colors.blueColor, padding: 10 }}>
            <TouchableOpacity onPress={() => this.closeModalView()}>
              <View style={{ flexDirection: "row", marginTop: 20 }}>
                <Icon
                  name="arrow-left"
                  type="feather"
                  size={25}
                  color={colors.white}
                />

                <Text
                  style={{
                    marginLeft: 10,
                    color: colors.white,
                    fontSize: 12,
                    padding: 5
                  }}
                >
                  Select Cross Dock Location
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{ marginLeft: 0, marginRight: 0, marginBottom: 10, flex: 1 }}
          >
            <View style={styles.mapViewStyle}>
              <MapView
                style={styles.map}
                scrollEnabled={false}
                showsUserLocation={false}
                zoomEnabled={true}
                zoomControlEnabled={true}
                initialRegion={{
                  latitude: 20.5937,
                  longitude: 78.9629,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421
                }}
              >
                <MapView.Marker
                  draggable
                  coordinate={this.state.draggedCoordinate}
                  onDragEnd={e =>
                    this.setState(
                      {
                        draggedCoordinate: e.nativeEvent.coordinate
                      },
                      () => this.geoCoderFunction()
                    )
                  }
                />
              </MapView>
            </View>

            <Button title={"Confirm Location"}></Button>
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  modalOuterStyle: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.7)"
  },
  modalInnerStyle: {
    backgroundColor: "#FFF"
  },
  map: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  mapViewStyle: {
    flex: 1
  },
  imageStyle: {
    width: 23,
    height: 22,
    marginTop: 10,
    marginLeft: 10
  }
});
