import React, { Component } from "react";
import {
  Alert,
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import colors from "../colors/Colors";
import Snackbar from "react-native-snackbar";
import axios from "axios";
import { EDIT_TRIP, GET_DISTANCE } from "../constants/Constants";
import { ScrollView } from "react-navigation";
import Geocoder from "react-native-geocoder";
import DateTimePicker from "react-native-modal-datetime-picker";
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from "@react-native-community/async-storage";
import VehicleVendorAssignModal from "./VehicleVendorAssignModal";
import SelectInput from "react-native-select-input-ios";
import MapView from "react-native-maps";
import CrossDockLocationModal from "./CrossDockLocationModal";

let tripId,
  tickVisible,
  delayReasons = [],
  id = 0,
  lat,
  lng,
  token,
  idTrip,
  route,
  source,
  vehicleChangeReasonsArr = [],
  vehicleEngagedBy = [];

class CrossDockModal extends Component {
  state = {
    showList: false,
    message: "",
    selectedValue: "",
    showModal: false,
    vendorName: "",
    vendorId: "",
    vendorCode: "",
    vehicleName: "",
    vehicleId: "",
    title: "",
    filterData: [],
    apFlag: "",
    phone: "",
    cityName: "",
    cityId: "",
    distance: "",
    lat: "",
    longi: "",
    a: {
      latitude: 28.4595,
      longitude: 77.0266
    },
    markers: [],
    draggedCoordinate: {
      latitude: 20.5937,
      longitude: 78.9629
    },
    address: "",
    time: "",
    dateTimeArray: [],
    engagedById: "0",
    reasonCode: "0",
    dateTime: "",
    cityCode: "",
    warningText: "",
    distanceEditInput: true,
    originalData: [],
    showMapModal: false
  };

  constructor(props) {
    super(props);

    // let temp_array = this.props.products.products[11]['vehicle_change_reasons'];
    // temp_array.splice(0, 0, {"code": 'NA', "value": 'Select Vehicle Change Reason'});
    // vehicleChangeReasonsArr = temp_array;
    //
    // let temp_array1 = this.props.products.products[13]['employees'];
    // temp_array1.splice(0, 0, {"id": 'N/A', "name": 'Select Vehicle Engaged by'});
    // vehicleEngagedBy = temp_array1;

    const { vehicleChangeReasons = {}, engagedBy = {} } = this.props;

    vehicleChangeReasonsArr = Object.keys(vehicleChangeReasons);
    vehicleEngagedBy = Object.keys(engagedBy);
  }

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;

    tripId = params.tripId;
    tickVisible = params.tickVisible;
    delayReasons = params.delayReasons;
    token = params.token;
    idTrip = params.id;
    route = params.route;

    source = route.split("-");

    return {
      // title: navigation.getParam('Title', params ? tripId : ''),
      headerTitle: "Edit Vehicle",
      headerStyle: {
        backgroundColor: navigation.getParam(
          "BackgroundColor",
          colors.blueColor
        )
      },
      headerTintColor: navigation.getParam("HeaderTintColor", "#fff"),
      //Text color of ActionBar
      headerTitleStyle: {
        fontWeight: "bold",
        fontSize: 13,
        fontFamily: "CircularStd-Book"
      }
    };
  };

  checkNetworkSettings() {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.editVehicle();
      } else {
        return Snackbar.show({
          title: "Please check Internet Connection",
          duration: Snackbar.LENGTH_LONG
        });
      }
    });
  }

  editVehicle() {
    if (this.state.vehicleId === "" || this.state.vehicleId === undefined) {
      // ToastAndroid.show('Select a Vehicle', ToastAndroid.SHORT)
      Snackbar.show({
        title: "Please Select Vehicle",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (
      this.state.vendorId === "" ||
      this.state.vendorId === undefined
    ) {
      // ToastAndroid.show('Select Vendor', ToastAndroid.SHORT)
      Snackbar.show({
        title: "Please Select Vendor ",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (this.state.apFlag === "0") {
      // ToastAndroid.show('Selected Vendor cannot be assigned', ToastAndroid.SHORT)
      Snackbar.show({
        title: "Selected Vendor cannot be assigned",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (this.state.phone === "" || this.state.phone === undefined) {
      Snackbar.show({
        title: "Please add Driver Phone",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (this.state.cityId === "" || this.state.cityId === undefined) {
      Snackbar.show({
        title: "Please Select City",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (this.state.distance === "") {
      Snackbar.show({
        title: "Please add Distance",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (this.state.draggedCoordinate.latitude === "") {
      Snackbar.show({
        title: "Please select location on map ",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (this.state.draggedCoordinate.longitude === "") {
      Snackbar.show({
        title: "Please select location on map ",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (this.state.reasonCode === "") {
      Snackbar.show({
        title: "Please select Reason",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (this.state.engagedById === "") {
      Snackbar.show({
        title: "Please select Engaged By",
        duration: Snackbar.LENGTH_LONG
      });
    } else if (this.state.dateTime === "") {
      Snackbar.show({
        title: "Please select Time",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      axios
        .post(
          EDIT_TRIP,
          {
            trip_code: tripId,
            vehicle: this.state.vehicleId,
            phone: this.state.phone,
            vendor: this.state.vendorId,
            reason: this.state.reasonCode,
            city: this.state.cityId,
            engaged_by: this.state.engagedById,
            lat: this.state.draggedCoordinate.latitude,
            long: this.state.draggedCoordinate.longitude,
            distance: this.state.distance,
            location: this.state.address,
            time: this.state.dateTime,
            type: "cross-dock"
          },
          {
            headers: {
              Authorization: "Token " + token,
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          if (response.data.error === false) {
            this.setState(
              {
                message: response.data.message[0]
              },
              () => this.showErrorMessage()
            );
            // closeModal
          } else {
            this.setState(
              {
                message: response.data.message[0]
              },
              () => this.showErrorMessage()
            );
          }
        })
        .catch(error => {
          console.log("catch" + error);
          if (error.response) {
            if (error.response.status === 401) {
              AsyncStorage.removeItem("token");
              this.props.navigation.navigate("Auth");
            } else if (error.response.status === 400) {
              console.log("error 400:::::::" + JSON.stringify(error.response));
            } else {
              console.log("error" + error);
            }
          }
        });
    }
  }

  // clearState() {
  //     const {closeModal} = this.props;
  //
  //     this.setState({
  //         showList: false,
  //         message: '',
  //         selectedValue: '',
  //         showModal: false,
  //         vendorName: '',
  //         vendorId: '',
  //         vendorCode: '',
  //         vehicleName: '',
  //         vehicleId: '',
  //         title: '',
  //         filterData: [],
  //         apFlag: '',
  //         phone: ''
  //     }, () => closeModal())
  // }

  showErrorMessage() {
    const { closeModal, refreshing } = this.props;
    Alert.alert("Message", this.state.message, [
      {
        text: "OK",
        onPress: () => {
          // closeModal()
          // refreshing()
        }
      }
    ]);
  }

  checkVendor() {
    if (this.state.vehicleId !== null && this.state.vehicleId !== "") {
      this.showModal("vendor");
    } else {
      console.log("vendorCheck Snackbar");
      return Snackbar.show({
        title: "Please Select Vehicle for Vendor Selection",
        duration: Snackbar.LENGTH_LONG
      });
    }
  }

  checkVendorName(value, value1) {
    return (
      <TouchableOpacity onPress={() => this.checkVendor()} activeOpacity={0.9}>
        <View style={styles.customerBorderStyle}>
          <Text style={styles.phcustomerStyle}>Vendor</Text>
          <Text
            label="Vendor"
            style={
              value !== ""
                ? [styles.customerStyleInput, { color: colors.dark_grey }]
                : styles.customerStyleInput
            }
          >
            {value !== null && value !== ""
              ? value + " (" + value1 + " )"
              : "Vendor"}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  checkVehicleName(value) {
    console.log("selected id :::::" + this.state.selectedValue);
    console.log("vehicle id :::::" + this.state.vehicleId);
    console.log("vendor id :::::" + this.state.vendorId);

    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({ vehicleId: "" });
          this.showModal("vehicle");
        }}
        activeOpacity={0.9}
      >
        <View style={styles.customerBorderStyle}>
          <Text style={styles.phcustomerStyle}>Vehicle</Text>
          <Text
            label="Vehicle"
            style={
              value !== ""
                ? [styles.customerStyleInput, { color: colors.dark_grey }]
                : styles.customerStyleInput
            }
          >
            {value !== null && value !== "" ? value : "Vehicle"}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  checkCityName(value) {
    return (
      <TouchableOpacity
        onPress={() => {
          this.showModal("city");
        }}
        activeOpacity={0.9}
      >
        <View style={styles.customerBorderStyle}>
          <Text style={styles.phcustomerStyle}>Cross-Dock City</Text>
          <Text
            style={
              value !== ""
                ? [styles.customerStyleInput, { color: colors.dark_grey }]
                : styles.customerStyleInput
            }
          >
            {value !== null && value !== "" ? value : "City"}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  renderModal() {
    console.log("vehicleId renderModal::::" + this.state.vehicleId);

    return (
      <VehicleVendorAssignModal
        display={this.state.showModal}
        closeModal={() => {
          this.setState({ showModal: false });
        }}
        filterData={this.state.filterData}
        title={this.state.title}
        SelectedValue={this.state.selectedValue}
        filterSearch={this.filterSearch.bind(this)}
      />
    );
  }

  showModal(type) {
    const showModal = true;
    let originalData = [];
    let selectedValue = true;
    let tickVisible = false;
    let title = "";

    let mappedArr;

    const { vehicles = {}, vendors = {}, cities = {} } = this.props;
    console.log("Show Modal Assign modal props----------", this.props);

    console.log(
      "show modal vehicles -----------------------------",
      JSON.stringify(vehicles)
    );
    console.log(
      "show modal vendors -----------------------------",
      JSON.stringify(vendors)
    );
    // originalData = Object.keys(vehicles);

    console.log(
      "original data :::::::::::::::::::::::" +
        JSON.stringify(Object.keys(vehicles))
    );
    console.log(
      "original data :::::::::::::::::::::::" +
        JSON.stringify(Object.keys(vendors))
    );

    if (type === "vendor") {
      selectedValue = this.VendorSelectedValue;
      title = "Vendors";
      tickVisible = true;
      mappedArr = Object.keys(vendors).map(function(i) {
        return i, vendors[i];
      });
    } else if (type === "vehicle") {
      selectedValue = this.VehicleSelectedValue;
      title = "Vehicle";
      mappedArr = Object.keys(vehicles).map(function(i) {
        return i, vehicles[i];
      });
    } else if (type === "city") {
      selectedValue = this.SourcecitySelectedValue;
      title = "City";
      mappedArr = Object.keys(cities).map(function(i) {
        return i, cities[i];
      });
    }
    this.setState({
      originalData: mappedArr,
      showModal,
      filterData: mappedArr,
      selectedValue,
      tickVisible,
      title
    });
  }

  filterSearch(text) {
    console.log("filterText" + text);
    if (text !== "") {
      const jsonData_filter = this.state.originalData.filter(item => {
        let itemData = "";
        if (this.state.title === "Vehicle") {
          itemData = item.number + item.vehicle_type;
        } else {
          itemData = item.code.toLowerCase() + item.name.toLowerCase();
        }
        const textData = text.toLowerCase();

        console.log("textData filter" + textData);
        return itemData.indexOf(textData) > -1;
      });

      this.setState({
        text: text,
        filterData: jsonData_filter
      });
    } else {
      this.setState({
        filterData: this.state.originalData
      });
    }
  }

  VendorSelectedValue = (changeText, selectedId, code, apFlag) => {
    console.log("selected value vendorName" + changeText);
    console.log("selected value vendorId" + selectedId);
    this.setState({
      vendorName: changeText,
      vendorId: selectedId,
      showModal: false,
      vendorCode: code,
      apFlag: apFlag
    });
  };

  VehicleSelectedValue = (changeText, selectedId, code, vehicleTypeId) => {
    console.log(
      "selected value numer" + changeText + "  vehicleId " + selectedId
    );
    this.setState({
      vehicleName: changeText,
      vehicleId: selectedId,
      showModal: false
    });
  };

  SourcecitySelectedValue = (changeText, selectedId, code) => {
    console.log("selected value sourcecity" + changeText);

    this.setState(
      {
        cityName: changeText,
        cityId: selectedId,
        cityCode: code,
        showModal: false
      },
      () => this.getDistance()
    );

    // this.getDistance()
  };

  getDistance() {
    const { tripId } = this.props;

    route = source[0] + "-" + this.state.cityCode;

    console.log("new route ::: " + route);

    axios
      .post(
        GET_DISTANCE,
        {
          trip_code: tripId,
          route: route
        },
        {
          headers: {
            Authorization: "Token " + token,
            "Content-Type": "application/json"
          }
        }
      )
      .then(response => {
        if (response.data.error === false) {
          console.log(response);
          {
            response.data.distance !== null && response.data.distance !== 0
              ? this.setState({
                  distance: response.data.distance,
                  distanceEditInput: false
                })
              : this.setState({ distanceEditInput: true });
          }

          {
            response.data.message !== null &&
            response.data.message !== undefined
              ? this.setState({ warningText: response.data.message })
              : null;
          }
        } else {
        }
      })
      .catch(error => {
        console.log("catch" + error);
        if (error.response) {
          if (error.response.status === 401) {
            this.props.navigation.navigate("Auth");
          } else if (error.response.status === 400) {
            console.log("error 400:::::::" + JSON.stringify(error.response));
          } else {
            console.log("error" + error);
          }
        }
      });
  }

  onMapPress(e) {
    this.setState({
      markers: [
        ...this.state.markers,
        {
          coordinate: e.nativeEvent.coordinate,
          key: id++,
          color: colors.blueColor
        }
      ]
    });
  }

  async geoCoderFunction() {
    lat = this.state.draggedCoordinate.latitude;
    lng = this.state.draggedCoordinate.longitude;

    console.log("latitude : " + lat + "longitude : " + lng);

    Geocoder.fallbackToGoogle("AIzaSyDjfNsHFVUcBrNM3YkMSULF4y72eRs0o00");
    let ret = await Geocoder.geocodePosition({ lat, lng });
    // debugger;
    this.setState({ address: ret[0].formattedAddress });

    console.log("dragged coordinate " + ret[0].formattedAddress);
  }

  getVehicleChangeReasons = () => {
    const { vehicleChangeReasons = {} } = this.props;
    vehicleChangeReasonsArr = Object.keys(vehicleChangeReasons);

    let options = [
      {
        value: "0",
        label: "Select Vehicle Change Reason"
      }
    ];

    console.log(
      "vehicle change reasons reasons ::::::::::::" +
        JSON.stringify(vehicleChangeReasonsArr)
    );
    let mappedArr = Object.keys(vehicleChangeReasons);
    if (mappedArr.length > 0) {
      for (const index of mappedArr) {
        const reason = vehicleChangeReasons[index] || {};
        const { code, value } = reason || {};
        if (code && value) {
          options.push({
            value: code,
            label: value
          });
        }
      }
    }

    return options;
  };

  engagedBy = () => {
    const { engagedBy = {} } = this.props;
    vehicleEngagedBy = Object.keys(engagedBy);

    let options = [
      {
        value: "0",
        label: "Vehicle Engaged By"
      }
    ];

    console.log(
      "vehicle engaged By::::::::::::" + JSON.stringify(vehicleEngagedBy)
    );
    let mappedArr = Object.keys(engagedBy);
    if (mappedArr.length > 0) {
      for (const index of mappedArr) {
        const reason = engagedBy[index] || {};
        const { id, name } = reason || {};
        if (id && name) {
          options.push({
            value: id,
            label: name
          });
        }
      }
    }

    console.log("engaged By mappedArr" + mappedArr);

    return options;
  };

  renderMap() {
    if (this.state.showMapModal) {
      return (
        <CrossDockLocationModal
          display={this.state.showMapModal}
          closeModal={() => {
            this.setState({ showMapModal: false });
          }}
        />
      );
    } else {
    }
  }

  render() {
    // console.log('vehicleChangeReasonsArr : ' + this.state.vehicleChangeReasonsArr.length);
    // console.log('vehicle Engaged By : ' + this.state.vehicleEngagedBy.length);

    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1 }}>
          {this.renderModal()}

          {this.checkVehicleName(this.state.vehicleName)}

          {this.checkVendorName(this.state.vendorName, this.state.vendorCode)}

          {this.checkCityName(this.state.cityName)}

          {this.renderMap()}

          {this.state.warningText !== null && this.state.warningText !== "" ? (
            <Text
              style={[
                styles.phcustomerStyle,
                {
                  textAlign: "center",
                  color: colors.redColor
                }
              ]}
            >
              {this.state.warningText}
            </Text>
          ) : null}

          <View style={styles.customerBorderStyle}>
            <Text style={styles.phcustomerStyle}>Distance</Text>
            <TextInput
              editable={this.state.distanceEditInput}
              placeholder="Distance"
              keyboardType="numeric"
              placeholderTextColor={colors.border_grey}
              style={
                this.state.distance !== ""
                  ? [styles.StyleInput, { color: colors.dark_grey }]
                  : styles.StyleInput
              }
              multiline={false}
              onChangeText={text => this.setState({ distance: text })}
            >
              {this.state.distance}
            </TextInput>
          </View>

          <View style={styles.reasonBorderStyle}>
            <Text style={styles.phcustomerStyle}>Reason</Text>

            <SelectInput
              labelStyle={[
                {
                  padding: 0,
                  paddingTop: 4,
                  textAlign: "center",
                  margin: 0,
                  color: colors.dark_grey,
                  height: 30,
                  ...Platform.select({
                    ios: {
                      overflow: "hidden",
                      fontSize: 15
                    },
                    android: {
                      paddingBottom: 2,
                      fontSize: 11
                    }
                  })
                },
                this.state.reasonCode === "0"
                  ? colors.border_grey
                  : colors.dark_grey
              ]}
              options={this.getVehicleChangeReasons()}
              mode={"dropdown"}
              value={this.state.reasonCode}
              onValueChange={text => this.setState({ reasonCode: text })}
            />
          </View>

          <View style={styles.reasonBorderStyle}>
            <Text style={styles.phcustomerStyle}>Vehicle Engaged by</Text>
            <SelectInput
              labelStyle={[
                {
                  padding: 0,
                  paddingTop: 4,
                  textAlign: "center",
                  margin: 0,
                  color: colors.dark_grey,
                  height: 30,
                  ...Platform.select({
                    ios: {
                      overflow: "hidden",
                      fontSize: 15
                    },
                    android: {
                      paddingBottom: 2,
                      fontSize: 11
                    }
                  })
                },
                this.state.reasonCode === "0"
                  ? colors.border_grey
                  : colors.dark_grey
              ]}
              options={this.engagedBy()}
              mode={"dropdown"}
              value={this.state.engagedById}
              onValueChange={text => this.setState({ engagedById: text })}
            />
          </View>

          <View style={styles.customerBorderStyle}>
            <Text style={styles.phcustomerStyle}>Driver Phone</Text>
            <TextInput
              editable={true}
              placeholder="Driver Phone"
              keyboardType="numeric"
              placeholderTextColor={colors.border_grey}
              style={
                this.state.phone !== ""
                  ? [styles.StyleInput, { color: colors.dark_grey }]
                  : styles.StyleInput
              }
              multiline={false}
              onChangeText={text => this.setState({ phone: text })}
            />
          </View>

          <TouchableOpacity
            onPress={() => this.setState({ showMapModal: true })}
          >
            <View style={styles.customerBorderStyle}>
              <Text style={styles.phcustomerStyle}>Address</Text>
              <TextInput
                editable={true}
                placeholder="Address"
                placeholderTextColor={colors.border_grey}
                style={
                  this.state.address !== ""
                    ? [styles.StyleInput, { color: colors.dark_grey }]
                    : styles.StyleInput
                }
                multiline={false}
                onChangeText={text => this.setState({ address: text })}
              >
                {this.state.address}
              </TextInput>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
            onPress={() => this.setState({ showdatePicker: true })}
          >
            <View style={styles.customerBorderStyle}>
              <Text style={[styles.phcustomerStyle]}>
                CrossDock Date and Time
              </Text>
              <Text
                label="CrossDock Date and Time"
                style={[
                  this.state.dateTime !== null &&
                  this.state.dateTime !== "" &&
                  this.state.dateTime !== "Select Trip Date and Time"
                    ? styles.selectedTextColor
                    : styles.customerStyleInput
                ]}
              >
                {this.state.dateTime !== null && this.state.dateTime !== ""
                  ? this.state.dateTime
                  : "Select Trip Date and Time"}
              </Text>
            </View>
          </TouchableOpacity>

          <DateTimePicker
            isVisible={this.state.showdatePicker}
            mode={"datetime"}
            onConfirm={date => {
              console.log("A date has been picked: ", date);

              this.state.dateTimeArray.push(date);

              let d;

              if (this.state.dateTimeArray.length > 1) {
                console.log("dateTimeArray > 1");
                d = this.state.dateTimeArray[
                  this.state.dateTimeArray.length - 1
                ];
              } else {
                console.log("dateTimeArray < 1");
                d = this.state.dateTimeArray[0];
              }

              console.log("0th index::::" + this.state.dateTimeArray[0]);
              console.log("d :::::" + d);
              let fromDate = new Date(d);

              const monthNames = [
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "10",
                "11",
                "12"
              ];

              console.log(
                "getDate" +
                  fromDate.getDate() +
                  "-" +
                  monthNames[fromDate.getMonth()] +
                  "-" +
                  fromDate.getFullYear() +
                  " " +
                  fromDate.getHours() +
                  ":" +
                  fromDate.getMinutes()
              );
              this.setState({
                dateTime:
                  fromDate.getDate() +
                  "-" +
                  monthNames[fromDate.getMonth()] +
                  "-" +
                  fromDate.getFullYear() +
                  " " +
                  fromDate.getHours() +
                  ":" +
                  fromDate.getMinutes(),
                showdatePicker: false
              });
            }}
            onCancel={() => this.setState({ showdatePicker: false })}
          />

          <View style={styles.buttonStyle}>
            <TouchableOpacity onPress={() => this.checkNetworkSettings()}>
              <Text style={styles.submitTextStyle}>Edit Vehicle</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>

        {/*<View style={styles.mapViewStyle}>*/}
        {/*    <MapView*/}
        {/*        scrollEnabled={false}*/}
        {/*        style={styles.map}*/}
        {/*        showsUserLocation={false}*/}
        {/*        zoomEnabled={true}*/}
        {/*        zoomControlEnabled={true}*/}
        {/*        onPress={e => this.onMapPress(e)}*/}
        {/*        initialRegion={{*/}
        {/*            latitude: 20.5937,*/}
        {/*            longitude: 78.9629,*/}
        {/*            latitudeDelta: 0.0922,*/}
        {/*            longitudeDelta: 0.0421,*/}
        {/*        }}>*/}

        {/*        <MapView.Marker*/}
        {/*            draggable*/}
        {/*            coordinate={this.state.draggedCoordinate}*/}
        {/*            onDragEnd={(e) => this.setState({*/}
        {/*                draggedCoordinate: e.nativeEvent.coordinate,*/}
        {/*            }, () => this.geoCoderFunction())}*/}
        {/*        />*/}
        {/*    </MapView>*/}
        {/*</View>*/}
      </View>
    );
  }
}

const styles = {
  imageStyle: {
    width: 22,
    height: 22,
    marginTop: -15,
    marginLeft: -15
  },
  customerBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    alignItems: "flex-start"
  },
  phcustomerStyle: {
    marginTop: -10,
    color: colors.dark_grey,
    backgroundColor: colors.white,
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    fontFamily: "CircularStd-Book",
    alignSelf: "flex-start"
  },
  buttonStyle: {
    color: colors.white,
    backgroundColor: colors.blueColor,
    fontSize: 12,
    justifyContent: "flex-end",
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 20
  },
  customerStyleInput: {
    fontSize: 12,
    color: colors.border_grey,
    paddingLeft: 5,
    marginLeft: 5,
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center",
    padding: 5
  },
  modalOuterStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.7)"
  },
  modalInnerStyle: {
    backgroundColor: "#FFF",
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    padding: 10,
    width: "90%"
  },
  BorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 20,
    flex: 1,
    paddingLeft: 5,
    paddingRight: 5
  },
  StyleInput: {
    fontSize: 12,
    paddingLeft: 5,
    marginLeft: 5,
    fontFamily: "CircularStd-Book",
    textAlignVertical: "center",
    width: "100%",
    ...Platform.select({
      ios: {
        padding: 5,
        overflow: "hidden"
      },
      android: {
        padding: 0
      }
    })
  },
  reasonBorderStyle: {
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    borderColor: colors.blueColor,
    marginTop: 15,
    marginLeft: 10,
    marginRight: 10
  },
  map: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    flex: 0.5
  },
  mapViewStyle: {
    flex: 1,
    height: "20%"
  },
  mapStyle: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  MainContainer: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: "center",
    justifyContent: "flex-end"
  },
  selectedTextColor: {
    fontSize: 12,
    color: colors.dark_grey,
    marginLeft: 5,
    padding: 5,
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
    fontFamily: "CircularStd-Book"
  },
  pickerItemStyle: {
    height: 33,
    ...Platform.select({
      ios: {
        padding: 5,
        overflow: "hidden"
      },
      android: {
        padding: 0
      }
    })
  },
  submitTextStyle: {
    padding: 10,
    textAlign: "center",
    color: colors.white
  }
};

// const mapStateToProps = state => ({
//     state: state,
//     error: state.error,
//     products: state.productsReducer.products,
//     pending: state.productsReducer.pending,
// });
//
//
// export default connect(
//     mapStateToProps,
//     null
// )(CrossDockModal);
//

export default CrossDockModal;
