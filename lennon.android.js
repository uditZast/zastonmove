import React, { Component } from "react";
import AppContainer from "./src";
import { Platform } from "react-native";
import BackgroundColor from "react-native-background-color";

const prefix = "lennon://";

export default class AppAndroid extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (Platform.OS === "android") {
      BackgroundColor.setColor("#FFFFFF");
    }
  }

  render() {
    return <AppContainer uriPrefix={prefix} />;
  }
}
