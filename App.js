import React, { Component } from "react";
import "react-native-gesture-handler";
import Lennon from "./lennon";

export default class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  toggleDrawer = () => {
    this.props.navigationProps.toggleDrawer();
  };

  render() {
    return (
      <Lennon />
    );
  }
}
