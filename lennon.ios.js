import React, { Component } from "react";
import AppContainer from "./src";

const prefix = "lennon://";

export default class AppIos extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <AppContainer uriPrefix={prefix} />;
  }
}
